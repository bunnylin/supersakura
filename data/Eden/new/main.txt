// Main menu and game definitions for Eden no Kaori / Scent of Eden

$_windowtitle := "Eden no Kaori"
#$s1 := "Joe" # not used?

// 480x296 view in 640x400 frame @ 80,16
viewport.setparams viewport:1 parent:0, ratiox = 480, ratioy = 296
viewport.setparams viewport:2 parent:1, x:0 y:0 w:1.0 h:1.0

$_defaultviewport := 2

#gfx.show FRE55 bkg viewport 1

// Textbox setup
$defaultlang := "Japanese"
$_titlebox := TITLEBOX
$_choicebox := MAINBOX
$_choicepartbox := TITLEBOX
tbox.setparam NULL language $defaultlang
tbox.setparam MAINBOX language $defaultlang
tbox.setparam TITLEBOX language $defaultlang


// ===== Title screen =====
gfx.clearall
mus.stop
$_interrupt := skiptitle
$_escint := skiptitle
gfx.transition crossfade
gfx.show OP_00 bkg
sleep
gfx.transition fade
gfx.show y:10 OP_TT
sleep
goto mainmenu

@skiptitle:
gfx.transition instant
yield
gfx.transition instant
stop

// ===== Main Menu =====
@mainmenu:
event.clear

tbox.setparam box:MAINBOX columns 1 maxrows 5 mincols 1 minsizexy 1
|: textalign 0.5 paddingh 0.016 basecolor 0x678C
tbox.setloc MAINBOX 0.6

choice.reset
choice.set "Start" newgame // はじめから
choice.set "Load" loadgame // ロード
choice.set "Personae" intros // 人物紹介
// settings - 環境設定
choice.set "Gallery" gallery
choice.set "Quit" quitgame // おわる
choice.go

@setuptextboxes:
tbox.reset MAINBOX
return

// ===== New Game =====
@newgame:
gfx.transition
gfx.clearall
sleep
call setuptextboxes
gfx.show TB_008 bkg // the intro scenes are all overlays, needs a parent bkg

$_allowsaving := 1
goto JO0101.

// ===== Load Game =====
@loadgame:
sys.loadstate
sys.restartgame

// ===== Dramatis Personae =====
@intros:
choice.go

// ===== Gallery =====
@gallery:
choice.go

// ===== Quit Game =====
@quitgame:
sys.quit force:1
