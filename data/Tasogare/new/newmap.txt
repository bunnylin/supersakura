# Clickable map. 13 different locations; must fill in $maploc0..$maploc12, where for
# each destination: 1 = enabled, 0 = disabled.
# Also fill $s11..$s23 with the mouseover text legend for each enabled location.
# Returns the 0-based index the user picked.

transition i
gfx.clearkids
tbox.clear
show CHIZU bkg name:M // 480x296 image
//show MS_CUR sprite x:2.0 y:2.0 ax:1.0 // show first frame, pointy hand
// That must be flipped to point right instead of left! Use runtime dynamic gob...
sleep

$_mainbox := ROOMBOX
tbox.reset ROOMBOX
tbox.setloc ROOMBOX x:0.84 ax:0.5 y:0.05 ay:0 // top right corner
tbox.setparam box:ROOMBOX maxrows 1, mincols 8, textalign 0.5
|: padding 0.02, bevel yes, basecolor 0xDA7F

# Tasogare map:         0 - Oni no Amado ruins ("Demon's Heavenly Gate")
# +------------------+  1 - big, friendly two-story house; Ichinose Ryokan
# |      1  A&       |  2 - police station
# |         &        |  3 - walled compound, rich guy Suemori's mansion
# |        &  3      |  4 - river-crossing, roadside Jizou shrine
# | 9    8 &7   2    |  5 - windmill-looking Furusawa shrine
# |    6  &        &&|  6 - local health clinic
# |  C   4       &&  |  7 - pair of houses; Melanie's home, Matsuno manor
# |     &   &&B&&  0 |  8 - school and library
# |   &&&&&&         |  9 - lone house, blue-roofed; Esaka home
# | &&        5      |  A - hound stone
# +------------------+  B - marsh, tiny island in river
#                       C - stone pillar, execution site

# Using mouseoverable virtual gobs for map destinations. Only define gobs for enabled destinations.
# show <image> <type> <name> <parent> <trigger> <mouseon> <mouseoff>
if $maploc0  then (show "" "" "0"  M mgo mon mof w:0.1000 h:0.0676 x:0.8333 y:0.6081) end // 48x20 @ 400,180
if $maploc1  then (show "" "" "1"  M mgo mon mof w:0.0708 h:0.0946 x:0.3333 y:0.0946) end // 34x28 @ 160,28
if $maploc2  then (show "" "" "2"  M mgo mon mof w:0.0333 h:0.0541 x:0.6875 y:0.4189) end // 16x16 @ 330,124
if $maploc3  then (show "" "" "3"  M mgo mon mof w:0.0979 h:0.0608 x:0.5271 y:0.3243) end // 47x18 @ 253,96
if $maploc4  then (show "" "" "4"  M mgo mon mof w:0.0708 h:0.0608 x:0.3458 y:0.5878) end // 34x18 @ 166,174
if $maploc5  then (show "" "" "5"  M mgo mon mof w:0.0458 h:0.0946 x:0.5875 y:0.8581) end // 22x28 @ 282,254
if $maploc6  then (show "" "" "6"  M mgo mon mof w:0.0458 h:0.0541 x:0.2500 y:0.4932) end // 22x16 @ 120,146
if $maploc7  then (show "" "" "7"  M mgo mon mof w:0.0708 h:0.0676 x:0.4521 y:0.4189) end // 34x20 @ 217,124
if $maploc8  then (show "" "" "8"  M mgo mon mof w:0.0708 h:0.0676 x:0.3271 y:0.4189) end // 34x20 @ 157,124
if $maploc9  then (show "" "" "9"  M mgo mon mof w:0.0542 h:0.0642 x:0.0833 y:0.3784) end // 26x19 @ 40,112
if $maploc10 then (show "" "" "10" M mgo mon mof w:0.0292 h:0.0676 x:0.4938 y:0.1149) end // 14x20 @ 237,34
if $maploc11 then (show "" "" "11" M mgo mon mof w:0.0958 h:0.0743 x:0.5208 y:0.6486) end // 46x22 @ 250,192
if $maploc12 then (show "" "" "12" M mgo mon mof w:0.0313 h:0.0608 x:0.1375 y:0.5878) end // 15x18 @ 66,174

$map := -1
waitsig
if $map < 0 then (error "bad map \$map;") end
gfx.remove MS_CUR
$_mainbox := MAINBOX
tbox.remove ROOMBOX
tbox.clear
return $map

@mgo:
event.remove
signal MAIN
stop

@mon:
$map := tonum(param name)
$x := param x
$y := param y
print $(."s" + ($map + 11))
//gfx.setloc MS_CUR x:$x y:$y
stop

@mof:
$map := -1
//gfx.setloc MS_CUR x:2.0 y:2.0
tbox.clear
stop
