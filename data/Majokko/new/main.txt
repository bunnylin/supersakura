// Majokko Paradise
// Main menu and game definitions

$_windowtitle := "Majokko Paradise"
#$s1 := "Rei" # not used?

// 480x296 view in 640x400 frame @ 80,16
viewport.setparams viewport:1 parent:0, ratiox = 480, ratioy = 296
viewport.setparams viewport:2 parent:1, x:0 y:0 w:1.0 h:1.0

$_defaultviewport := 2

#gfx.show FREMI2
#gfx.show JURA
// 480x296 view in 640x400 frame @ 72,16
#sys.viewport 3686, 1311; 24576 x 24248

// Textbox setup
$defaultlang := "Japanese"
tbox.setparam NULL language $defaultlang
tbox.setparam MAINBOX language $defaultlang
tbox.setparam TITLEBOX language $defaultlang

$_titlebox := TITLEBOX
$_choicebox := MAINBOX
$_choicepartbox := TITLEBOX


// Show the Tiare logo before the title screen?
//gfx.transition 9
//gfx.show bkg TIARE_S
// Make the star curve in and land in the middle of the A?
//gfx.show sprite TIARE_P
//gfx.clearall

$_interrupt := ."skiptitle"
$_escint := ."skiptitle"
fiber.start titlescroll
waitsignal
event.clear
goto mainmenu

// ===== Title Screen =====
// The PC98 shows the title by first showing OPB, then scrolling seamlessly
// up to OPA, then back down to OPB, then slapping TAITOL on the lower left,
// and popping up a menu in the bottom right.
//
// In our case, OPA and OPB have been combined into a double-height OPA.
// Fade in the top half, pan downward smoothly. Halfway through, start
// alpha-fading in the game logo.

@titlescroll:
transition f
show OPA bkg
sleep
mus.play mp_17
sleep 400

move OPA y:-1.0 time:3600 style:cos
sleep 1600
transition // make the logo instantly visible...
show TAITOL x:0.0125 y:0.44 parent:""
gfx.setalpha TAITOL alpha:0 time:0 // ... and instantly fully transparent
gfx.setalpha TAITOL alpha:255 1600 // slide to full visibility
sleep
signal MAIN
stop

@skiptitle:
stop titlescroll
gfx.transition 0
move OPA y:-1.0 time:0
gfx.remove TAITOL
show TAITOL x:0.0125 y:0.44 parent:""
mus.play mp_17
signal MAIN
stop

// ===== Main Menu =====
@mainmenu:
tbox.setloc MAINBOX 0.96, 0.96 ax:1.0 ay:1.0
tbox.setparam box:MAINBOX basecolor 0x0008 basefill flat paddingv 0.02 paddingh 0.03 bevel 0
|: textcolor 0xFFFF columns 1 mincols 0

choice.reset
choice.set "\CStart" newgame
choice.set "Load" loadgame
choice.set "Gallery" gallery
choice.set "Quit" quitgame
choice.go

// ===== New Game =====
@newgame:
gfx.clearall
tbox.reset MAINBOX
mus.stop time:400

// The intro is in MP0001.OVL, but that doesn't follow normal script rules...
// So probably best rewrite it. Also, make it show only after the title
// screen, upon choosing New Game.
//runscript INTRO

$_allowsaving := 1
goto MP1101

@loadgame:
sys.loadstate
sys.restartgame

@gallery:
choice.go

@quitgame:
sys.quit force:1
