$_windowtitle := "Jewel BEM Hunter Lime 1"

viewport.setparams viewport:1 parent:0 ratiox:416 ratioy:272
viewport.setparams viewport:2 parent:1, x:0 y:0 w:1.0 h:1.0
$_defaultviewport := 2

@setuptextboxes:
$defaultlang := "Japanese"
tbox.setparam NULL language $defaultlang
tbox.setparam MAINBOX language $defaultlang
tbox.setparam TITLEBOX language $defaultlang

$_choicebox := CHOICEBOX

tbox.setparam box:MAINBOX basecolor 0x248D mincols 52 maxcols 52 padleft 0.028 padright 0.028
tbox.setloc MAINBOX y:0.99

tbox.setparam box:TITLEBOX basecolor 0x359C

tbox.setloc CHOICEBOX x:0.99, ax:1.0, y:0.99, ay:1.0
tbox.setparam box:CHOICEBOX minrows 4, mincols 7, basecolor 0x248E

$choicelist := "null:Look:Talk:Think:Save:Lime:Bass:Puggy:Area 1:Area 2:Area 3:Around:Man:That way:This way:Move"

// --- Title screen
gfx.clearall
transition f
show ."109" bkg
sleep

// --- Main menu
choice.reset
choice.set "\CStart" newgame
choice.set "Load" loadgame
choice.set "Gallery" gallery
choice.set "Quit" quitgame
choice.go

// --- New game
@newgame:
transition
gfx.clearall
sleep
$_interrupt := main.skipfx
$_cancancelchoice := 2
$_allowsaving := 1
show "|0" bkg x:0 y:0 w:1.0 h:1.0 // fake background to start with
goto startup

// --- Load game
@loadgame:
sys.loadstate
sys.restartgame

// --- Gallery
@gallery:
choice.go

// --- Quit game
@quitgame:
sys.quit force:1

@skipfx:
fx.skip
stop
