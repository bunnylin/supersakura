For modifying the title graphics and biography pictures, must poke
updated offsets and sizes into l2_open.exe.

If reconverting with website/filus/makeolh.py, use fpc/fixpng first if necessary,
and make sure it's in hex numbering! Correct any pixel errors, then run makeolh.
Check flag byte in original images.
For L2 bio pages 001.0E through 001.13 neither pal and size are present.
For 001.14 title screen both are.

The new files are hopefully smaller than the originals, so pad them to the same
sizes except 001.13 which should be a bit smaller to allow room for the larger
localised 001.14 title image. On Linux, you can use:
  truncate --size=1234 l2op_001.xx.olh

Poke the new sizes into the exe. See below for the addresses to look for.
The 001.0E offset at least used to be at 19a1.

Concatenate the olh frames into a single graphic, Linux to the rescue:
  cat l2op_001.??.olh>l2op_001.olh

---------------------- 22 Sep 2022 update ----------------

size diff:  a86,  98c,  a62,   ca8,  abb,  b53,  -7d8
old sizes: 2341, 21d1, 23af,  258d, 2452, 258f,  d99f = 1b1ce
new sizes: 18e5, 18cc, 190c,  1955, 1a05, 1ac6,  e177 = 17a54 (377a short)
pad sizes: 2341, 21d1, 23af,  258d, 2452,     ,  e177 = 19417 needs more so 1ac6->1db7 = 1b1ce
old ofs:  24754,26a95,28c66, 2b015,2d5a2,2f9f4, 31f83, next at 3f922
new ofs:  24754,26a95,28c66, 2b015,2d5a2,2f9f4, 317ab, next at 3f922

l2op_001.0E.olh size at 19a1: 4123 -> 4123; ofs at 19a5: 5447....02 -> 5447....02
l2op_001.0F.olh size at 1c77: d121 -> d121; ofs at 1c7b: 956A....02 -> 956A....02
l2op_001.10.olh size at 1f75: af23 -> af23; ofs at 1f79: 668C....02 -> 668C....02
l2op_001.11.olh size at 2260: 8d25 -> 8d25; ofs at 2264: 15B0....02 -> 15B0....02
l2op_001.12.olh size at 2560: 5224 -> 5224; ofs at 2564: A2D5....02 -> A2D5....02
l2op_001.13.olh size at 284B: 8f25 -> b71d; ofs at 284f: F4F9....02 -> F4F9....02
l2op_001.14.olh size at 2988: 9fd9 -> 77e1; ofs at 298d: 831F....03 -> AB17....03

----------------------- old values -----------------------
$2341 l2op_001.0E.olh @ $24754 size 4123 at 19a1; ofs 5447....02 at 19a5
$21D1 l2op_001.0F.olh @ $26A95 size d121 at 1c77; ofs 956A....02 at 1c7b
$23AF l2op_001.10.olh @ $28C66 size af23 at 1f75; ofs 668C....02 at 1f79
$258D l2op_001.11.olh @ $2B015 size 8d25 at 2260; ofs 15B0....02 at 2264
$2452 l2op_001.12.olh @ $2D5A2 size 5224 at 2560; ofs A2D5....02 at 2564
$258F l2op_001.13.olh @ $2F9F4 size 8f25 at 284B; ofs F4F9....02 at 284f
$D99F l2op_001.14.olh @ $31F83 size 9fd9 at 2988; ofs 831F....03 at 298d

size diff:  a86,  98c,  a62,   ca8,  abb,  b53,  -7d8
old sizes: 2341, 21d1, 23af,  258d, 2452, 258f,  d99f = 1b1ce
new sizes: 1918, 18a6, 1928,  1955, 1a03, 1ac6,  e177 = 17a7b (3753 short)
pad sizes: 2341, 21d1, 23af,  258d, 2452, 1db7,  e177 = 19ee0 needs more so 1ac6->1db7 = 1b1ce
old ofs:  24754,26a95,28c66, 2b015,2d5a2,2f9f4, 31f83, next at 3f922
new ofs:  24754,26a95,28c66, 2b015,2d5a2,2f9f4, 317ab, next at 3f922

l2op_001.0E.olh size at 19a1: 4123 -> 4123; ofs at 19a5: 5447....02 -> 5447....02
l2op_001.0F.olh size at 1c77: d121 -> d121; ofs at 1c7b: 956A....02 -> 956A....02
l2op_001.10.olh size at 1f75: af23 -> af23; ofs at 1f79: 668C....02 -> 668C....02
l2op_001.11.olh size at 2260: 8d25 -> 8d25; ofs at 2264: 15B0....02 -> 15B0....02
l2op_001.12.olh size at 2560: 5224 -> 5224; ofs at 2564: A2D5....02 -> A2D5....02
l2op_001.13.olh size at 284B: 8f25 -> b71d; ofs at 284f: F4F9....02 -> F4F9....02
l2op_001.14.olh size at 2988: 9fd9 -> 77e1; ofs at 298d: 831F....03 -> AB17....03


$0EB2 l2op_anm.16.olh @ $80BA size B20E at 0e94; ofs BA80 at 0e98
$10E8 l2op_anm.17.olh @ $8F6C size E810 at 0f0f; ofs 6C8F at 0f13
$177C l2op_anm.18.olh @ $A054 size 7C17 at 1630; ofs 54A0 at 1634

old sizes: 0eb2,10e8,177c
new sizes: 134b,1580,1f3a
old ofs:   80ba,8f6c,a054
new ofs:   80ba,9405,a985 : c8bf

l2op_anm.16.olh size at 0e94: B20E -> 4B13; ofs at 0e98: BA80 -> BA80
l2op_anm.17.olh size at 0f0f: E810 -> 8015; ofs at 0f13: 6C8F -> 0594
l2op_anm.18.olh size at 1630: 7C17 -> 3A1F; ofs at 1634: 54A0 -> 85a9

----------------------------- file sizes ----------------------------
$1A4B l2op_001.00.olh @ $0000		$4624 l2op_anm.00.olh @ $0000
$32F4 l2op_001.01.olh @ $1A4B		$019C l2op_anm.01.olh @ $4624
$2529 l2op_001.02.olh @ $4D3F		$0142 l2op_anm.02.olh @ $47C0
$3E60 l2op_001.03.olh @ $7268		$0142 l2op_anm.03.olh @ $4902
$1C3D l2op_001.04.olh @ $B0C8		$0096 l2op_anm.04.olh @ $4A44
$33BF l2op_001.05.olh @ $CD05		$008B l2op_anm.05.olh @ $4ADA
$14C5 l2op_001.06.olh @ $100C4		$0164 l2op_anm.06.olh @ $4B65
$39E4 l2op_001.07.olh @ $11589		$0181 l2op_anm.07.olh @ $4CC9
$178C l2op_001.08.olh @ $14F6D		$01BC l2op_anm.08.olh @ $4E4A
$2CF8 l2op_001.09.olh @ $166F9		$0319 l2op_anm.09.olh @ $5006
$2B56 l2op_001.0A.olh @ $193F1		$04EB l2op_anm.0A.olh @ $531F
$3103 l2op_001.0B.olh @ $1BF47		$002D l2op_anm.0B.olh @ $580A
$2E81 l2op_001.0C.olh @ $1F04A		$0163 l2op_anm.0C.olh @ $5837
$2889 l2op_001.0D.olh @ $21ECB		$0385 l2op_anm.0D.olh @ $599A
$2341 l2op_001.0E.olh @ $24754*		$04EF l2op_anm.0E.olh @ $5D1F	<< nosize,nopal
$21D1 l2op_001.0F.olh @ $26A95*		$03DC l2op_anm.0F.olh @ $620E	<< nosize,nopal
$23AF l2op_001.10.olh @ $28C66*		$014A l2op_anm.10.olh @ $65EA	<< nosize,nopal
$258D l2op_001.11.olh @ $2B015*		$0291 l2op_anm.11.olh @ $6734	<< nosize,nopal
$2452 l2op_001.12.olh @ $2D5A2*		$03EF l2op_anm.12.olh @ $69C5	<< nosize,nopal
$258F l2op_001.13.olh @ $2F9F4*		$0555 l2op_anm.13.olh @ $6DB4	<< nosize,nopal
$D99F l2op_001.14.olh @ $31F83*		$0721 l2op_anm.14.olh @ $7309
$6912 l2op_001.15.olh @ $3F922		$0690 l2op_anm.15.olh @ $7A2A
$370A l2op_001.16.olh @ $46234		$0EB2 l2op_anm.16.olh @ $80BA* nosize
$6E92 l2op_001.17.olh @ $4993E		$10E8 l2op_anm.17.olh @ $8F6C* nosize,nopal
$32BF l2op_001.18.olh @ $507D0		$177C l2op_anm.18.olh @ $A054* nosize
$527C l2op_001.19.olh @ $53A8F
$2BC3 l2op_001.1A.olh @ $58D0B
$59A5 l2op_001.1B.olh @ $5B8CE
$386D l2op_001.1C.olh @ $61273
$49BA l2op_001.1D.olh @ $64AE0
$161A l2op_001.1E.olh @ $6949A
$AF0C l2op_001.1F.olh @ $6AAB4
