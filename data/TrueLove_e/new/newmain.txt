// True Love
// Main menu and game definitions

$_windowtitle := "True Love"

// ==================================================================
// Viewport setup, viewframe at 8,8 size 512x288

viewport.setparams viewport:1 parent:0, ratiox=640, ratioy=400
viewport.setparams viewport:2 parent:1, x:0.0125 y:0.02 w:0.8 h:0.72

// ===== Title Screen =====
@titlescreen:
viewport.setdefault 1
gfx.clearall
show logo ax:0.5 ay:0.5 x:0.5 y:0.5
sleep 1200
show title

// ===== Main Menu =====
@mainmenu:
event.clear
mus.play m011

tbox.setloc MAINBOX 0.5,0.96 ax:0.5 ay:1.0
tbox.setparam box:MAINBOX basecolor 0 basefill flat textcolor 0x68FF
|: padding 0 textalign 0.5
|: maxrows 8 mincols 13 columns 1

tbox.setparam box:HIGHLIGHT basecolor 0xFFF4
|: paddingv 180 paddingh 480

choice.reset
choice.set "Start" newgame
choice.set "Load" loadgame
choice.set "Gallery" gallery
choice.set "Quit" quitgame
choice.go

@newgame:
event.clear
mus.stop 1000
transition interlaced
gfx.clearall
viewport.setdefault 2 // almost all game graphics go in viewport 2
sleep

tbox.reset

goto MAIN

@loadgame:
sys.loadstate
sys.restartgame

@gallery:
choice.go

@quitgame:
sys.quit force:1
