// Rose Blood
// Main menu and game definitions

$_windowtitle := "Rose Blood"

viewport.setparams viewport:1 parent:0, ratiox=480, ratioy=272
$_defaultviewport := 1

// Textbox setup
$defaultlang := "Japanese"
tbox.setparam NULL language $defaultlang
tbox.setparam MAINBOX language $defaultlang
tbox.setparam TITLEBOX language $defaultlang

$_interrupt := skiptitle
$_escint := skiptitle

transition
gfx.clearall
sleep
transition f
show R
sleep
sleep 640
transition f
gfx.clearall
sleep
transition f
show title_a
sleep
goto mainmenu

@skiptitle:
transition 0
gfx.clearall
goto fiber:MAIN mainmenu
signal MAIN
stop

@mainmenu:
event.clear
transition 0
show title_a

tbox.setparam box:MAINBOX basecolor 0, textcolor 0x556F
|: padding 0, maxrows 4, minsizexy 1, columns 1, textalign 0.5
tbox.setloc MAINBOX y:0.6 ay:0.5

tbox.setparam box:HIGHLIGHT basecolor 0x77AF, blendmode add

choice.reset
choice.set "Start" newgame
choice.set "Load" loadgame
choice.set "Gallery" gallery
choice.set "Quit" quitgame
choice.go

@setuptextboxes:
tbox.reset MAINBOX
tbox.setparam MAINBOX basecolor 0x556D, columns 2
tbox.setparam TITLEBOX basecolor 0x556C
tbox.reset HIGHLIGHT
return

@newgame:
transition 0
gfx.clearall
sleep
call setuptextboxes
$_allowsaving := 1
goto ROSE_OP

@loadgame:
sys.loadstate
sys.restartgame

@gallery:
choice.go

@quitgame:
sys.quit force:1
