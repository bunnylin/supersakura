// Season of the Sakura - Endings
// Before calling, set gvar 512 to the ending number index

mus.play SK_33
transition 4
gfx.clearall
sleep
sleep 400

# Order of endings: bad:reiko:kiyomi:mio:shoko:ruri:aki:seia:meimi

if $v512 > 8 then // bad ending
  show SP_001 bkg
  transition 4
  sleep
  goto credits
end

transition 4
$0 := (select ($v512 - 1) ."193:194:195:197:191:192:196:198")
show AE_$0 bkg name:ENDIMG ofsx:0.5 ax:0.5
sleep
call ."ENDING" + $v512

@credits:
tbox.clear
// In the Japanese credits, the ending image moves left, leaving space for
// the credits along the right edge... but, having tried it, I think leaving
// the closing image where it is looks better.
// gfx.move ENDIMG; 0, 0; 0.05
// sleep

sleep 600

"\C\cB9DF;Season of the Sakura\n\n"
"\cACEF;DIRECTOR\n"
"Taizo the Destroyer\n\n"
"\cACEF;ORIGINAL ARTWORK\n"
"Ishiki Ryotaro\n\n"
"\cACEF;ART DIRECTOR\n"
"Sanagi Oshima\n\n"
"\cACEF;ARTISTS\n"
"Usa Usagi\n"
"Yumirin\n"
"Saya Izawa\n"
"Curly-kun\n\n"
"\cACEF;ASSISTANT\n"
"Oguro\n\n"
"\cACEF;SCENARIO\n"
"Kaoru Shisa\n"
"Professor Compression\n"
"Naozo Konno\n\n"
"\cACEF;MUSIC\n"
"Takanori Nakaigawa\n\n"
"\cACEF;PROGRAM\n"
"Taizo the Destroyer\n\n" // missing from the original English credits
"\cACEF;THANKS TO\n"
"Eiwa Technical Service\n\n"
"\cACEF;ENGLISH TRANSLATION\n"
"Totoro Hunter Leto II\n\n"
"\cACEF;SLAVE IN AMERICA\n"
"Motenai Yoda\n\n"
"\cACEF;PLANNING & DEVELOPMENT\n"
"\cE02F;JAST"
...

"\CThe End"
...

// fade to black
transition 4
gfx.clearall
sleep
sys.restartgame
