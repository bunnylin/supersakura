program SuperSakuraCon;

{                                                                           }
{ SuperSakura engine :: Copyright 2009-2025 :: Kirinn Bunnylin / Mooncore   }
{ Console version! No audio or gamepad support.                             }
{ https://supersakura.net/                                                  }
{ https://gitlab.com/bunnylin/supersakura                                   }
{                                                                           }
{ This program is free software: you can redistribute it and/or modify      }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ This program is distributed in the hope that it will be useful,           }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with this program.  If not, see <https://www.gnu.org/licenses/>.    }
{ ------------------------------------------------------------------------- }
{                                                                           }
{ Targets FPC 3.2.2 for Linux/Win, 32/64-bit.                               }
{                                                                           }
{ Compilation dependencies (included under "lib"):                          }
{ - Various moonlibs (Zlib license)                                         }
{   https://gitlab.com/bunnylin/moonlibs                                    }
{                                                                           }

{$mode objfpc}
{$ifdef WINDOWS}{$apptype console}{$endif}
{$codepage UTF8}
{$iochecks off} // WARNING: if an IO action fails, all subsequent actions fail silently until IOresult is checked
{$scopedEnums on}
{$inline on}
{$librarypath lib/moonlibs} // libpaths clear warnings in win32 compilation
{$unitpath lib/moonlibs}
{$unitpath inc}
{$ifdef WINDOWS}{$resource etc/saku.res}{$endif} // pulls in the app icon

{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

{$define sakucon}

uses
{$ifdef UNIX}cthreads,{$endif}
sysutils, // needed for file traversal etc
math,
paszlib, // standard compression/decompression unit for savegames etc
mccommon,
mcfileio,
mccosine,
minicrt, // console/terminal input/output
mcvarmon, // script variable handling system
mcgloder, // graphics loading and resizing
mcscaler,
mcblitters, // pixel buffer copying functions
mcsassm; // general asset management, streaming stuff from DAT-files

// Sakurascript compiler and types.
{$include inc/sakurascript-compiler.pas}

// Basic structures, helper functions.
{$include inc/saku/common.pas}

// Common visual element class.
{$include inc/saku/elements-implementation.pas}

// Virtual viewport handling.
{$include inc/saku/viewport-implementation.pas}

// Text box functions.
{$include inc/saku/box-con.pas}

// Gob functions.
{$include inc/saku/gobs-implementation.pas}

// All timed effects setup and execution.
{$include inc/saku/effects-implementation.pas}

// Rendering and visual effect functions.
{$include inc/saku/render-con.pas}

// Choicematic functions.
{$include inc/saku/choice-implementation.pas}

// Sakurascript execution, fiber handling system, and helpers.
{$include inc/saku/fiber-implementation.pas}

// Mouseoverables, events, script callbacks.
{$include inc/saku/event-implementation.pas}

// User input handling.
{$include inc/saku/input.pas}

// Console-specific init, main loop, input handling, output display.
{$include inc/saku/base-con.pas}

// ------------------------------------------------------------------

begin
	//MiniCRT_RunTest;
	//exit;
	if HandleCommandlineParams = FALSE then exit;
	InitEverything;
	MainLoop;
	preference.WriteConfig;
end.

