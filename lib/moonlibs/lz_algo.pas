unit lz_algo;

{                                               }
{ LZ decompression variants                     }
{ CC0, 2017-2024 : Kirinn Bunnylin / MoonCore   }
{ Use freely for anything ever!                 }
{                                               }

{$mode objfpc}
{$codepage UTF8}

interface

uses mcfileio; // for bitstream reading

procedure Decompress_LZ77(
	srcp : pointer; srcsize : dword; var outp : pointer; out writesize : dword; topdown : boolean = FALSE);
procedure Decompress_LZSS(
	srcp, srcend : pointer; var outp : pointer; out writesize : dword; distofs : dword;
	topdown : boolean = FALSE; distfirst : boolean = TRUE; xorflag : byte = 0; undertable : byte = 0);
procedure Decompress_LZSS3(
	srcp, srcend : pointer; var outp : pointer; out writesize : dword; distofs : dword;
	topdown : boolean = FALSE; distfirst : boolean = TRUE; xorflag : byte = 0);
procedure Decompress_LZSS4(srcp, srcend : pointer; var outp : pointer; out writesize : dword;
	topdown : boolean = FALSE; xorflag : byte = 0);
procedure Decompress_LZSSbitstream(const src : TFileLoader; var outp : pointer; out writesize : dword);
procedure Decompress_CsLZ16(srcp, srcend : pointer; out outp : pointer; var writesize : dword; lz32 : boolean = FALSE);
procedure Decompress_LZZE(const src : TFileLoader; inbytes : dword; var outp : pointer; var writesize : dword);

implementation

uses sysutils, mccommon;

procedure InitUnder(writep : pointer; table : byte);
// Some LZ algorithms have preset patterns that can be repeated from by trying to copy from before the output buffer.
// Nearly useless for improving compression in practice...
var p : pointer;
	i : dword;
begin
	case table of
		1: // Panda House (Cat's Pro, Melody)
		begin
			p := writep + 13 * $100;
			for i := 0 to $FF do begin
				fillbyte(writep^, 13, i);
				inc(writep, 13);
				byte(p^) := i;
				byte((p + $100)^) := i xor $FF;
				inc(p);
			end;
			inc(p, $100);
			filldword(p^, 32, 0); inc(p, 128);
			fillword(p^, 55, $2020); inc(p, 110);
			fillword(p^, 9, 0);
		end;
	end;
end;

procedure Decompress_LZ77(
	srcp : pointer; srcsize : dword; var outp : pointer; out writesize : dword; topdown : boolean = FALSE);
// Attempts to uncompress a SoftDisk-style LZ77 stream from srcp^. Puts the result in outp^ and writesize.
// Reserves 64k for outp if it is NIL, otherwise caller must have reserved enough.
// Caller is responsible for freeing both buffers, except if there's an exception, this frees outp.
var srcend : pointer;
	writep : ^byte;
	copyfrom : longint;
	flagbyte, bitindex, copylen : byte;
begin
	if outp = NIL then getmem(outp, 65536);
	writep := outp;
	try
		srcend := srcp + srcsize;
		flagbyte := 0; bitindex := 1;
		while srcp < srcend do begin
			dec(bitindex);
			if bitindex = 0 then begin
				flagbyte := byte(srcp^); inc(srcp);
				if topdown then begin // read in reverse order, flip the bits!
					flagbyte := RorByte(flagbyte, 4);
					flagbyte := (byte(flagbyte and $CC) shr 2) or (byte(flagbyte and $33) shl 2);
					flagbyte := (byte(flagbyte and $AA) shr 1) or (byte(flagbyte and $55) shl 1);
				end;
				bitindex := 8;
			end;

			if flagbyte and 1 <> 0 then begin
				// Literal.
				writep^ := byte(srcp^);
				inc(srcp); inc(writep);
			end
			else begin
				// Codeword. AB CD, where B+3 is copy length, CDA is distance.
				copyfrom := LEtoN(word(srcp^)); inc(srcp, 2);
				if copyfrom = 0 then break;
				copylen := (copyfrom and $F) + 3;

				writesize := writep - outp;
				copyfrom := longint(writesize and $FFFFF000) + (copyfrom shr 4) - 1;
				if copyfrom >= longint(writesize) then dec(copyfrom, $1000);

				if copyfrom >= 0 then
					MemCopy(outp + copyfrom, writep, copylen)
				else begin
					// Underflow, fill with zeroes.
					copyfrom := -copyfrom;
					if copylen <= copyfrom then
						fillbyte(writep^, copylen, 0)
					else begin
						fillbyte(writep^, copyfrom, 0);
						inc(writep, copyfrom);
						dec(copylen, copyfrom);
						MemCopy(outp, writep, copylen);
					end;
				end;
				inc(writep, copylen);
			end;

			flagbyte := flagbyte shr 1;
		end;
	except
		freemem(outp); outp := NIL;
		raise;
	end;
	writesize := writep - outp;
end;

procedure Decompress_LZSS(
	srcp, srcend : pointer; var outp : pointer; out writesize : dword; distofs : dword;
	topdown : boolean = FALSE; distfirst : boolean = TRUE; xorflag : byte = 0; undertable : byte = 0);
// Attempts to uncompress an LZSS byte stream from srcp^. Puts the result in outp^ and writesize.
// Reserves 64k for outp if it is NIL, otherwise caller must have reserved enough.
// Caller is responsible for freeing both buffers, except if there's an exception, this frees outp.
// Distofs controls the read distance window offset. Usually 18, sometimes 16. Caller must choose the right one.
// This is used by eg. Eve Burst Error; the algorithm looks very similar to a 1992 Softdisk implementation, except the
// ring buffer can be optimised out.
var writep : ^byte;
	underflow, read_distance : longint;
	flagbyte, bitindex, copylen, underlen : byte;
begin
	flagbyte := 0; bitindex := 1;
	if outp = NIL then getmem(outp, 65536);
	writep := outp;
	// Set up the initial underflow table if desired. Copies from out of bounds read from this.
	if undertable <> 0 then InitUnder(outp, undertable);

	try
		while srcp < srcend do begin
			flagbyte := flagbyte shr 1;
			dec(bitindex);
			if bitindex = 0 then begin
				flagbyte := byte(srcp^); inc(srcp);
				if topdown then begin // read in reverse order, rotate the bits!
					flagbyte := RorByte(flagbyte, 4);
					flagbyte := (byte(flagbyte and $CC) shr 2) or (byte(flagbyte and $33) shl 2);
					flagbyte := (byte(flagbyte and $AA) shr 1) or (byte(flagbyte and $55) shl 1);
				end;
				flagbyte := flagbyte xor xorflag; // optionally reverse flag bit meanings!
				bitindex := 8;
			end;

			if flagbyte and 1 <> 0 then begin
				// Literal.
				writep^ := byte(srcp^);
				inc(srcp); inc(writep);
			end
			else begin
				// If distance first: Codeword AB CD, where CAB is read_distance, D+3 is copylen.
				// If length first: CDA is read_distance, B+3 is copylen.
				if distfirst then begin
					read_distance := byte(srcp^); inc(srcp);
					copylen := byte(srcp^); inc(srcp);
					inc(read_distance, longint((copylen and $F0) shl 4));
				end
				else begin
					copylen := byte(srcp^); inc(srcp);
					read_distance := byte(srcp^); inc(srcp);
					read_distance := dword(read_distance shl 4) + (copylen shr 4);
				end;
				copylen := (copylen and $F) + 3;
				// The algorithm offsets read_distance by distofs, usually 18 bytes.
				writesize := writep - outp;
				read_distance := (writesize - distofs - read_distance) and $FFF;

				underflow := read_distance - writesize;
				if underflow <= 0 then
					MemCopy(writep - read_distance, writep, copylen)
				else begin
					// Special case: copying from before start of output buffer produces 0's or copies from undertable.
					if underflow >= copylen then
						underlen := copylen
					else
						underlen := underflow;

					if undertable = 0 then
						fillbyte(writep^, underlen, 0)
					else
						move((outp + $1000 - underflow - distofs)^, writep^, underlen);

					if underflow < copylen then begin
						// Copy source is only partially before start of output.
						inc(writep, underflow);
						dec(copylen, underflow);
						MemCopy(outp, writep, copylen);
					end;
				end;
				inc(writep, copylen);
			end;
		end;
	except
		freemem(outp); outp := NIL;
		raise;
	end;
	writesize := writep - outp;
end;

procedure Decompress_LZSS3(
	srcp, srcend : pointer; var outp : pointer; out writesize : dword; distofs : dword;
	topdown : boolean = FALSE; distfirst : boolean = TRUE; xorflag : byte = 0);
// Attempts to uncompress an LZSS byte stream from srcp^. Puts the result in outp^ and writesize.
// In this variant, the copy command is 3 bytes instead of the standard 2.
// Reserves 64k for outp if it is NIL, otherwise caller must have reserved enough.
// Caller is responsible for freeing both buffers, except if there's an exception, this frees outp.
// Distofs controls the read distance window offset. Caller must choose the right one.
// This is used by AIL games.
var writep : ^byte;
	copylen, underflow, read_distance : longint;
	flagbyte, bitindex : byte;
begin
	flagbyte := 0; bitindex := 1;
	if outp = NIL then getmem(outp, 65536);
	writep := outp;

	try
		while srcp < srcend do begin
			flagbyte := flagbyte shr 1;
			dec(bitindex);
			if bitindex = 0 then begin
				flagbyte := byte(srcp^); inc(srcp);
				if topdown then begin // read in reverse order, rotate the bits!
					{flagbyte := RorByte(flagbyte, 4);
					flagbyte := (byte(flagbyte and $CC) shr 2) or (byte(flagbyte and $33) shl 2);
					flagbyte := (byte(flagbyte and $AA) shr 1) or (byte(flagbyte and $55) shl 1);}
				end;
				flagbyte := flagbyte xor xorflag; // optionally reverse flag bit meanings!
				bitindex := 8;
			end;

			if flagbyte and 1 <> 0 then begin
				// Literal.
				writep^ := byte(srcp^);
				inc(srcp); inc(writep);
			end
			else begin
				// Codeword AB CD EF, where DAB is read_distance, CEF+3 is copylen.
				if distfirst then begin
					read_distance := byte(srcp^); inc(srcp);
					copylen := byte(srcp^); inc(srcp);
					inc(read_distance, longint((copylen and $F) shl 8));
					copylen := ((copylen and $F0) shl 4) or byte(srcp^); inc(srcp);
				end
				else begin
					{copylen := byte(srcp^); inc(srcp);
					read_distance := byte(srcp^); inc(srcp);
					read_distance := dword(read_distance shl 4) + (copylen shr 4);}
				end;
				inc(copylen, 3);
				// The algorithm offsets read_distance by distofs.
				writesize := writep - outp;
				read_distance := (writesize - distofs - read_distance) and $FFF;

				underflow := read_distance - writesize;
				if underflow <= 0 then
					MemCopy(writep - read_distance, writep, copylen)
				else begin
					// Special case: copying from before start of output buffer produces zeroes.
					if underflow < copylen then
						dec(copylen, underflow)
					else begin
						underflow := copylen;
						copylen := 0;
					end;
					fillbyte(writep^, underflow, 0);
					inc(writep, underflow);
					MemCopy(outp, writep, copylen);
				end;
				inc(writep, copylen);
			end;
		end;
	except
		freemem(outp); outp := NIL;
		raise;
	end;
	writesize := writep - outp;
end;

procedure Decompress_LZSS4(srcp, srcend : pointer; var outp : pointer; out writesize : dword;
	topdown : boolean = FALSE; xorflag : byte = 0);
// Attempts to uncompress an LZSS byte stream from srcp^. Puts the result in outp^ and writesize.
// In this variant, the copy command is straightforward with a len+1 nibble and 3-nibble copy without an offset.
// Reserves 64k for outp if it is NIL, otherwise caller must have reserved enough.
// Caller is responsible for freeing both buffers, except if there's an exception, this frees outp.
// This is used by Sogna's Windows games.
var writep : ^byte;
	copylen, underflow, read_distance : longint;
	flagbyte, bitindex : byte;
	//op : pointer;
begin
	flagbyte := 0; bitindex := 1;
	if outp = NIL then getmem(outp, 65536);
	writep := outp;
	//op := srcp;

	try
		while srcp < srcend do begin
			flagbyte := flagbyte shr 1;
			dec(bitindex);
			if bitindex = 0 then begin
				flagbyte := byte(srcp^); inc(srcp);
				if topdown then begin // read in reverse order, rotate the bits!
					flagbyte := RorByte(flagbyte, 4);
					flagbyte := (byte(flagbyte and $CC) shr 2) or (byte(flagbyte and $33) shl 2);
					flagbyte := (byte(flagbyte and $AA) shr 1) or (byte(flagbyte and $55) shl 1);
				end;
				flagbyte := flagbyte xor xorflag; // optionally reverse flag bit meanings!
				bitindex := 8;
			end;

			if flagbyte and 1 <> 0 then begin
				// Literal.
				writep^ := byte(srcp^);
				inc(srcp); inc(writep);
			end
			else begin
				// Codeword AB CD, where C+1 is copylen, DAB is read_distance.
				read_distance := LEtoN(word(srcp^)); inc(srcp, 2);
				copylen := (read_distance shr 12) + 1;
				read_distance := read_distance and $FFF;

				underflow := read_distance - (writep - outp);
				if underflow <= 0 then
					MemCopy(writep - read_distance, writep, copylen)
				else begin
					// Special case: copying from before start of output buffer produces zeroes. Or invalid?
					//writeln(strcat('SrcOfs: %, DestOfs: %, dist %, len %', [srcp-op, writep-outp, read_distance, copylen]));
					//raise Exception.Create('copy underflow');
					if underflow < copylen then
						dec(copylen, underflow)
					else begin
						underflow := copylen;
						copylen := 0;
					end;
					fillbyte(writep^, underflow, 0);
					inc(writep, underflow);
					MemCopy(outp, writep, copylen);
				end;
				inc(writep, copylen);
			end;
		end;
	except
		freemem(outp); outp := NIL;
		raise;
	end;
	writesize := writep - outp;
end;

procedure Decompress_LZSSbitstream(const src : TFileLoader; var outp : pointer; out writesize : dword);
// Attempts to uncompress an LZSS bitstream from src. Puts the result in outp^ and writesize.
// Reserves memory for outp if it is NIL, otherwise caller must have reserved enough.
// Caller is responsible for freeing both buffers, except if there's an exception, this frees outp.
var writep : ^byte;
	outbufsize, bitsremaining, copydist : dword;
	underflow : longint;
	copylen : byte;
	//op : pointer;
begin
	src.bitSelect := $80;
	outbufsize := src.fullFileSize shl 2;
	if outp = NIL then getmem(outp, outbufsize);
	writep := outp;
	writesize := 0;
	bitsremaining := (src.fullFileSize - src.ofs) shl 3;
	//op := src.readp;

	try
		while bitsremaining <> 0 do begin
			dec(bitsremaining);
			if writep - outp + 256 > longint(outbufsize) then begin
				writesize := writep - outp;
				outbufsize := outbufsize shl 1;
				reallocmem(outp, outbufsize);
				writep := outp + writesize;
			end;

			if src.ReadBit then begin
				// Copy from before.
				if bitsremaining < 16 then break;
				dec(bitsremaining, 16);
				copylen := src.ReadBits(4) + 1;
				copydist := src.ReadBits(12);

				underflow := copydist - (writep - outp);
				if underflow <= 0 then
					MemCopy(writep - copydist, writep, copylen)
				else begin
					// Special case: copying from before start of output buffer produces zeroes. Or invalid?
					//writeln(strcat('SrcOfs: %, DestOfs: %, dist %, len %', [src.readp-op, writep-outp, copydist, copylen]));
					//if longint(copydist) > (writep - outp) then raise Exception.Create('lzssbitstream copy underflow');
					if underflow < copylen then
						dec(copylen, underflow)
					else begin
						underflow := copylen;
						copylen := 0;
					end;
					fillbyte(writep^, underflow, 0);
					inc(writep, underflow);
					MemCopy(outp, writep, copylen);
				end;

				inc(writep, copylen);
			end
			else begin
				// Literal.
				if bitsremaining < 8 then break;
				dec(bitsremaining, 8);
				writep^ := byte(src.ReadBits(8));
				inc(writep);
			end;
		end;
	except
		freemem(outp); outp := NIL;
		raise;
	end;
	writesize := writep - outp;
end;

procedure Decompress_CsLZ16(srcp, srcend : pointer; out outp : pointer; var writesize : dword; lz32 : boolean = FALSE);
// Attempts to uncompress a Himeya/C's/Foster 16/32-bit LZ stream from srcp^. Puts the result in outp^ and writesize.
// Set writesize to 0 to autosize outp, or non-zero if you know what the uncompressed size will be.
// Caller is responsible for freeing both buffers, even if an exception is thrown.
// This is used by the FA1 archive format, eg. DeFaNa, Bacta 2, Zenith, Rabyni.
// Also used in FA2 archives as lz32, eg. KokoRaku 3.
var writep, outendp : ^byte;
	outpsize : dword;
	copydist, copylen, underflow : longint;
	bitselect, flagword : dword;

	function _GetBit : boolean;
	begin
		bitselect := bitselect shr 1;
		if bitselect = 0 then begin
			if lz32 then begin
				flagword := LEtoN(dword(srcp^)); inc(srcp, 4);
				bitselect := $80000000;
			end
			else begin
				flagword := LEtoN(word(srcp^)); inc(srcp, 2);
				bitselect := $8000;
			end;
		end;
		result := (flagword and bitselect) <> 0;
	end;

	function _GetVarLen : longint; inline;
	var a : byte = 0;
	begin
		if _GetBit then // 001b 1
			result := 3
		else if _GetBit then // 001b 01
			result := 4
		else if _GetBit then begin // 001b 001a
			result := 5; a := 1;
		end
		else if _GetBit then begin // 001b 0001aa
			result := 7; a := 2;
		end
		else if _GetBit then begin // 001b 00001aaaa
			result := 11; a := 4;
		end
		else begin // 001b 00000 special
			result := 27 + byte(srcp^); inc(srcp);
			exit;
		end;

		while a <> 0 do begin
			dec(a);
			if _GetBit then inc(result, 1 shl a);
		end;
	end;

	function _GetVarDist : longint; inline;
	var a : byte = 0;
	begin
		// 00 1 b varlen - copy varlen bytes from 2 * xx + b + 1 bytes ago.
		// 00 01 b varlen - 2 * (xx + 256) + b + 1.
		// 00 001 bb varlen - 4 * (xx + 256) + bb + 1.
		// 00 0001 bbb varlen - 8 * (xx + 256) + bbb + 1.
		// 00 0000 bbbb varlen - 16 * (xx + 256) + bbbb + 1.
		// The xx byte must be read after the first 1, before the b's.
		while (a < 4) and (NOT _GetBit) do inc(a);
		result := byte(srcp^); inc(srcp);

		if a = 0 then
			inc(a)
		else
			inc(result, 256);

		result := result shl a;

		// Add the b's.
		while a <> 0 do begin
			dec(a);
			if _GetBit then inc(result, 1 shl a);
		end;
	end;

begin
	outpsize := writesize;
	if outpsize = 0 then outpsize := (srcend - srcp) * 3;
	getmem(outp, outpsize);
	writep := outp;
	outendp := outp + outpsize;
	bitselect := 0;

	try
		while writep < outendp do begin
			if _GetBit then begin
				// 1 - literal.
				writep^ := byte(srcp^);
				inc(srcp); inc(writep);
			end
			else begin // 0x
				if _GetBit then begin // 01x
					// 010 - copy 2 bytes from xx+1 bytes ago.
					// 011bbb - copy 2 bytes from 256 + xx*8 + bbb + 1 ago.
					copylen := 2;
					if _GetBit then begin
						copydist := 0;
						copydist := copydist + 256 + longint(byte(srcp^) shl 3); inc(srcp);
						if _GetBit then inc(copydist, 4);
						if _GetBit then inc(copydist, 2);
						if _GetBit then inc(copydist, 1);

						if copydist >= $8FF then begin
							// Special case - 011111 with max xx: eat an extra bit, output nothing; or quit if lz32.
							if lz32 then break;
							//writeln('[!] lz16 011111 FF');
							if _GetBit then ;//writeln('lz16 extra bit 1');
							continue;
						end;
					end
					else begin copydist := byte(srcp^); inc(srcp); end;
				end
				else begin // 00x - copy 3+ bytes.
					copydist := _GetVarDist;
					copylen := _GetVarLen;
				end;

				inc(copydist);

				if writep + copylen > outendp then
					raise Exception.Create('lz16 out of bounds @ -' + strdec(srcend - srcp));
				underflow := copydist - (writep - outp);

				if underflow <= 0 then begin
					MemCopy(writep - copydist, writep, copylen);
					inc(writep, copylen);
				end
				else begin
					// Special case: copying from before start of output buffer produces zeroes.
					if underflow < copylen then
						dec(copylen, underflow)
					else begin
						underflow := copylen;
						copylen := 0;
					end;
					fillbyte(writep^, underflow, 0);
					inc(writep, underflow);
					MemCopy(outp, writep, copylen);
					inc(writep, copylen);
				end;
			end;
		end;
	finally
		writesize := writep - outp;
	end;
end;

procedure Decompress_LZZE(const src : TFileLoader; inbytes : dword; var outp : pointer; var writesize : dword);
// Attempts to uncompress inbytes worth LZZE bitstream from src. Puts the result in outp^ and writesize.
// Reserves memory for outp if it is NIL, otherwise caller must have reserved enough.
// Caller is responsible for freeing both src and outp.
var writep, inendp, outendp, chunkendp : ^byte;
	i, outbufsize, copydist, copylen : dword;
	f : boolean = FALSE;

	function _ReadNum : dword;
	var l : byte;
	begin
		l := 0;
		while l < 16 do begin
			if src.ReadBit then break;
			inc(l);
		end;
		result := dword(1 shl l) + src.ReadBits(l);
	end;

begin
	if outp = NIL then begin
		outbufsize := inbytes shl 2;
		getmem(outp, outbufsize);
		f := TRUE;
	end
	else outbufsize := writesize;
	writep := outp;
	outendp := outp + outbufsize;
	inendp := src.readp + inbytes;

	try
		while (src.readp < inendp) and (writep < outendp) do begin
			if src.ReadWord <> BEtoN(word($7A65)) then raise Exception.Create('no ze at lzze chunk start');
			i := BEtoN(src.ReadWord);
			chunkendp := writep + i;
			if chunkendp > outendp then raise Exception.Create('lzze chunk oob');
			src.bitSelect := 0;

			while (src.readp < inendp) and (writep < chunkendp) do begin
				// Output literals.
				i := _ReadNum;
				if writep + i > chunkendp then raise Exception.Create('lzze lit oob');
				while i <> 0 do begin
					writep^ := src.ReadBits(8); inc(writep);
					dec(i);
				end;
				if (src.readp >= inendp) or (writep >= chunkendp) then break;
				// Copy from before.
				copydist := _ReadNum;
				copylen := _ReadNum;
				if writep + copylen > chunkendp then raise Exception.Create('lzze rep oob');
				if longint(copydist) > writep - outp then raise Exception.Create('lzze rep dist oob');
				MemCopy(writep - copydist, writep, copylen);
				inc(writep, copylen);
			end;
		end;
	except
		if f then begin freemem(outp); outp := NIL; end;
		raise;
	end;
	writesize := writep - outp;
end;

initialization
finalization
end.

