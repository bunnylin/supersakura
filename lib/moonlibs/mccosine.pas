unit mccosine;
{                                                                           }
{ Mooncore cosine provider                                                  }
{ CC0, 2019 : Kirinn Bunnylin / MoonCore                                    }
{ Use freely for anything ever!                                             }
{                                                                           }

interface

// The below is a legacy precalculated half cosine curve at 16-bit resolution. This is not used by the current implementation.
// In radians, the input goes from cos(0) to cos(pi). The output has been normalised from [1..-1] to [65535..0].
// Notes:
// Cosine's gradient is a phase-shifted cosine... maybe helpful.
//   cos(n)' = cos(n + 1/4 * 2pi)
//   cos[0..max/2]' = cos(max/2 + n)
//   cos[max/2..max]' = cos(max*3/2 - n)
// And, if only expanding 2x at a time, the interpolation simplifies to:
//   = (g1 - g2) / 8 + dy / 2 + y1
// (can't use shifts since the terms can be negative)
// So for cos[1]:
//   g1 = cos[0]', g2 = cos[2]', dy = cos[2] - cos[0], y1 = cos[0]
{const mcg_CosTable : array[256..0] of word = (
65535, 65533, 65525, 65513, 65496, 65473, 65446, 65414,
65377, 65335, 65289, 65237, 65180, 65119, 65053, 64981,
64905, 64825, 64739, 64648, 64553, 64453, 64348, 64238,
64124, 64005, 63881, 63753, 63620, 63482, 63339, 63192,
63041, 62885, 62724, 62559, 62389, 62215, 62036, 61853,
61666, 61474, 61278, 61078, 60873, 60664, 60451, 60234,
60013, 59787, 59558, 59324, 59087, 58845, 58600, 58350,
58097, 57840, 57579, 57315, 57047, 56775, 56499, 56220,
55938, 55652, 55362, 55069, 54773, 54473, 54170, 53864,
53555, 53243, 52927, 52609, 52287, 51963, 51635, 51305,
50972, 50636, 50298, 49957, 49613, 49267, 48919, 48567,
48214, 47858, 47500, 47140, 46777, 46413, 46046, 45678,
45307, 44935, 44560, 44184, 43807, 43427, 43046, 42663,
42279, 41894, 41507, 41119, 40729, 40339, 39947, 39554,
39160, 38765, 38369, 37973, 37575, 37177, 36779, 36379,
35979, 35579, 35178, 34777, 34375, 33974, 33572, 33170,
32767, 32365, 31963, 31561, 31160, 30758, 30357, 29956,
29556, 29156, 28756, 28358, 27960, 27562, 27166, 26770,
26375, 25981, 25588, 25196, 24806, 24416, 24028, 23641,
23256, 22872, 22489, 22108, 21728, 21351, 20975, 20600,
20228, 19857, 19489, 19122, 18758, 18395, 18035, 17677,
17321, 16968, 16616, 16268, 15922, 15578, 15237, 14899,
14563, 14230, 13900, 13572, 13248, 12926, 12608, 12292,
11980, 11671, 11365, 11062, 10762, 10466, 10173, 9883,
9597,  9315,  9036,  8760,  8488,  8220,  7956,  7695,
7438,  7185,  6935,  6690,  6448,  6211,  5977,  5748,
5522,  5301,  5084,  4871,  4662,  4457,  4257,  4061,
3869,  3682,  3499,  3320,  3146,  2976,  2811,  2650,
2494,  2343,  2196,  2053,  1915,  1782,  1654,  1530,
1411,  1297,  1187,  1082,  982,   887,   796,   710,
630,   554,   482,   416,   355,   298,   246,   200,
158,   121,   89,    62,    39,    22,    10,    2,
0);}

// Tables for precalculated cosine values for effects at high resolutions.
// A plain cosine isn't quite dramatic enough; cosine * abs(cosine) has a more distinctive slow-fast-slow slope.
// Call UpdateCoscosTable to ensure the values exist at a suitable resolution. After this, the table can be used directly
// without additional interpolation, with no significant aliasing effects.
// Length(coscos) is guaranteed to be a power of two, plus one, and at least 257.
var coscos : array of word;

procedure UpdateCoscosTable(resolution : dword);

implementation

procedure UpdateCoscosTable(resolution : dword);
// Generate a coscos table suitable for the requested resolution. For visual effects, this should be the visual buffer's width
// or height in pixels, whichever is greater.
var	c, pi_div_highcoscos : valreal;
	i, tabsize : dword;
	shift : byte;
const halfpi = pi * 0.5;
begin
	shift := 0;
	tabsize := 256;
	while tabsize < resolution do begin
		inc(shift);
		tabsize := 256 shl shift;
	end;

	if dword(length(coscos)) <= tabsize then begin
		setlength(coscos, tabsize + 1);
		pi_div_highcoscos := pi / high(coscos);

		for i := high(coscos) downto 0 do begin
			c := cos(i * pi_div_highcoscos);
			c := cos((1.0 - c) * halfpi);
			coscos[i] := round(65535 * 0.5 * (1.0 - c));
		end;

		Assert(coscos[0] = 0);
		Assert(coscos[high(coscos)] = 65535);
	end;
end;

initialization
finalization
end.
