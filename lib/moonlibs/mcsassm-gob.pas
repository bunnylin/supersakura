{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

procedure TGraphicObject.FreeGraphic;
begin
	dec(asman_gfxMemUsage, length(bitmap));
	asman_logger(strcat('freeing %=%', [objIndex, graphicName]));
	setlength(bitmap, 0);
	setlength(framePtr, 0);
	graphicName := '';
	cacheState := 0;
	sourceFile := NIL;
	// detach
	graphicObjects[newerLink].olderLink := olderLink;
	graphicObjects[olderLink].newerLink := newerLink;
end;

procedure TGraphicObject.MarkAsRecentlyUsed;
begin
	// detach
	graphicObjects[newerLink].olderLink := olderLink;
	graphicObjects[olderLink].newerLink := newerLink;
	// reattach
	newerLink := 0;
	olderLink := graphicObjects[0].olderLink;
	graphicObjects[0].olderLink := objIndex;
	graphicObjects[olderLink].newerLink := objIndex;
end;

procedure TGraphicObject.ResizeFrom(origsizeobject : TGraphicObject);
// Resizes the graphic object's frames from the original size object to the expected sizeXP * frameHeightP in this one.
var img : mcg_bitmap;
	framecounter, origframebytes, framebytes : dword;
	srcp, destp : pointer;
begin
	memFormat := origsizeobject.memFormat;
	framecounter := length(origsizeobject.framePtr);
	framebytes := sizeXP * frameHeightP * 4;
	setlength(bitmap, framebytes * framecounter);
	origframebytes := origsizeobject.sizeXP * origsizeobject.frameHeightP * 4;
	origOfsX := origsizeobject.origOfsX;
	origOfsY := origsizeobject.origOfsY;
	origSizeX := origsizeobject.origSizeX;
	origFrameHeight := origsizeobject.origFrameHeight;

	img := mcg_bitmap.Create;
	img.bitmapFormat := MCG_FORMAT_BGRA;
	img.bitDepth := 8;

	srcp := @origsizeobject.bitmap[0] + origframebytes * framecounter;
	destp := @bitmap[0] + length(bitmap);
	while framecounter <> 0 do begin
		dec(framecounter);
		dec(srcp, origframebytes);
		dec(destp, framebytes);
		framePtr[framecounter] := destp;
		img.sizeXP := origsizeobject.sizeXP;
		img.sizeYP := origsizeobject.frameHeightP;
		img.stride := (img.sizeXP * img.bitDepth + 7) shr 3;
		setlength(img.bitmap, origframebytes);
		move(srcp^, img.bitmap[0], origframebytes); {$note todo: resize directly from source bits}
		img.Resize(sizeXP, frameHeightP);
		move(img.bitmap[0], destp^, framebytes);
		setlength(img.bitmap, 0);
	end;
	img.Destroy; img := NIL;

	inc(asman_gfxMemUsage, dword(length(bitmap)));
	asman_logger(strcat(
		'Loaded % %x% memusage=%/%', [graphicName, sizeXP, frameHeightP, asman_gfxMemUsage, asman_gfxMemLimit]));
	cacheState := $FF;
end;

procedure TGraphicObject.FillGradient;
// Generate a shaded rectangle into the graphic object's bitmap.
var gradientcolor, black, white : RGBAquad;
	argh, i : dword;
begin
	asman_logger('Splatting a gradient -> ' + graphicName);
	argh := 0;
	i := length(graphicName);
	while i <> 0 do begin
		argh := argh xor byte(graphicName[i]);
		argh := RorDWord(argh, 13);
		dec(i);
	end;
	with gradientcolor do begin
		b := $40 + $7F and (argh shr 16);
		g := $40 + $7F and (argh shr 8);
		r := $40 + $7F and argh;
		a := $FF;
	end;
	dword(black) := $FF000000;
	dword(white) := $FFFFFFFF;

	i := sizeXP * sizeYP * 4;
	if dword(length(bitmap)) < i then setlength(bitmap, 0);
	setlength(bitmap, i);
	memFormat := MCG_FORMAT_BGRX;
	mcg_FillGradient(black, gradientcolor, gradientcolor, white, @bitmap[0], sizeXP, sizeYP, 0);

	argh := sizeXP * frameHeightP * 4;
	for i := high(framePtr) downto 0 do begin
		framePtr[i] := @bitmap[i * argh];
		dword(framePtr[i]^) := $FFFFFFFF; // white top left
		dword((framePtr[i] + argh - 4)^) := $FF000000; // black low right
	end;

	// Track memory usage.
	inc(asman_gfxMemUsage, dword(length(bitmap)));
	cacheState := $FF;
end;

function TGraphicObject.LoadFromFile : boolean;
// Loads the graphic file directly into this graphicObject at original size. Returns FALSE if anything went wrong.
var content : array of byte = NIL;
	destp : pointer;
	infilu : file;
	bm : mcg_bitmap;
	errcode, i, framesize : dword;
	owningfilehandle : boolean = FALSE;
begin
	result := FALSE;

	// Ensure the file is open, read the data into memory.
	if sourceFile.srcDat <> NIL then begin
		infilu := sourceFile.srcDat.fileObject;
		owningfilehandle := NOT sourceFile.srcDat.loaded;
	end
	else begin
		assign(infilu, sourceFile.srcFilePath);
		owningfilehandle := TRUE;
	end;
	if owningfilehandle then for i := 7 downto 0 do begin
		filemode := 0; reset(infilu, 1); // read-only access
		errcode := IOresult;
		if errcode = 0 then
			break
		else if i = 0 then begin
			asman_logger(strcat('[!] LoadFromFile: % opening %', [errortxt(errcode), graphicName]));
			exit;
		end;
		sleep(100);
	end;

	seek(infilu, sourceFile.srcFileReadOfs);
	errcode := IOresult;
	if errcode <> 0 then begin
		asman_logger(strcat('[!] LoadFromFile: % seeking in %', [errortxt(errcode), graphicName]));
		if owningfilehandle then close(infilu);
		exit;
	end;

	setlength(content, sourceFile.srcFileReadSize);
	blockread(infilu, content[0], sourceFile.srcFileReadSize);
	errcode := IOresult;
	if owningfilehandle then close(infilu);
	if errcode <> 0 then begin
		asman_logger(strcat('[!] LoadFromFile: % reading %', [errortxt(errcode), graphicName]));
		exit;
	end;

	// Unpack the image.
	try
		bm := mcg_bitmap.FromPNG(@content[0], sourceFile.srcFileReadSize, [MCG_FLAG_FORCE32BPP]);
	except
		on e : Exception do begin
			asman_logger('[!] LoadFromFile ' + graphicName + ': ' + e.Message);
			exit;
		end;
	end;
	setlength(content, 0);

	// bm^ now has the RGBX/RGBA bitmap! The byte order is BGRX, or BGRA.
	with bm do if (sizeXP = 0) or (sizeYP = 0) or (length(bitmap) = 0) then begin
		asman_logger('[!] LoadFromFile: invalid bitmap');
		bm.Destroy;
		exit;
	end;

	// If the expected size and actual size differ, this must have been a loose multiframe PNG that needs rearranging.
	// Normally for images in DATs the rearrange was done at build time, but loose files must do it at loadtime.
	with sourceFile do begin
		if origSizeXP <> bm.sizeXP then begin
			Assert(frameCount > 1);
			RearrangeFrames(bm, sourceFile);
		end;
		origOfsX := 0; origSizeX := 0;
		if origResXP <> 0 then begin
			origOfsX := round(32768 * origOfsXP / origResXP);
			origSizeX := round(32768 * origSizeXP / origResXP);
		end;
		origOfsY := 0; origFrameHeight := 0;
		if origResYP <> 0 then begin
			origOfsY := round(32768 * origOfsYP / origResYP);
			origFrameHeight := round(32768 * origFrameHeightP / origResYP);
		end;
	end;

	// Pre-multiply RGB by alpha if applicable.
	if bm.bitmapFormat = MCG_FORMAT_BGRA then
		mcg_PremulRGBA32(@bm.bitmap[0], bm.sizeXP * bm.sizeYP);

	// We've got the image, now just stuff it into this graphicObject!
	memFormat := bm.bitmapFormat;
	bitmap := bm.bitmap; bm.bitmap := NIL;
	bm.Destroy; bm := NIL;

	framesize := sizeXP * frameHeightP * 4;
	destp := @bitmap[0] + framesize * dword(length(framePtr));
	for i := high(framePtr) downto 0 do begin
		dec(destp, framesize);
		framePtr[i] := destp;
	end;

	inc(asman_gfxMemUsage, dword(length(bitmap)));
	asman_logger(strcat('Loaded % memusage=%/%', [graphicName, asman_gfxMemUsage, asman_gfxMemLimit]));
	cacheState := $FF;
	result := TRUE;
end;

procedure ExpandGraphicObjectList;
var oldsize, newsize : dword;
begin
	oldsize := length(graphicObjects);
	newsize := oldsize + 32;
	setlength(graphicObjects, newsize);
	while oldsize < newsize do begin
		graphicObjects[oldsize] := TGraphicObject.Create;
		with graphicObjects[oldsize] do begin
			bitmap := NIL;
			framePtr := NIL;
			objIndex := oldsize;
			cacheState := 0;
		end;
		inc(oldsize);
	end;
end;

function FindFreeGraphicObjectSlot : TGraphicObject;
// Returns a free graphic object. Makes space for more as needed.
var i : dword;
begin
	i := asman_gObjWriteSlot;
	Assert(i <> 0);
	Assert(i < dword(length(graphicObjects)));
	repeat
		dec(i);
		if i = 0 then begin
			i := length(graphicObjects);
			repeat
				dec(i);
				if i = asman_gObjWriteSlot then begin
					asman_logger('FindFreeGObjSlot: gObjs out of space, expanding');
					i := length(graphicObjects);
					ExpandGraphicObjectList;
					break;
				end;
			until graphicObjects[i].cacheState = 0;
			break;
		end;
	until graphicObjects[i].cacheState = 0;
	asman_gObjWriteSlot := i;
	result := graphicObjects[i];
end;

function FindSizedGraphic(const name : UTF8string; rqsizex, rqsizey : dword) : TGraphicObject;
// Returns the most recently used graphicObject that matches the given name and size. Use 0 size to accept any size.
// If no match is found, returns NIL.
// The search is case-sensitive, but graphic names shall be in uppercase and no more than 31 characters.
// The pixel height is the frame height rather than the full bitmap height. (Otherwise frames bleed into each other.)
// graphicObjects[] is sorted from most to least recently used via a linked list, to make it easy to find the newest
// and oldest graphics.
var i : dword;
begin
	i := graphicObjects[0].olderLink; // start from newest
	while i <> 0 do with graphicObjects[i] do begin
		if (cacheState <> 0) and (graphicName = name)
		and ((rqsizex = 0) or (sizeXP = rqsizex)) and ((rqsizey = 0) or (frameHeightP = rqsizey))
		then begin
			result := graphicObjects[i];
			exit;
		end;
		i := olderLink;
	end;
	result := NIL;
end;

function LoadAtSize(_sizexp, _sizeyp : dword; origobject : TGraphicObject; origfile : TGraphicFile) : TGraphicObject;
begin
	result := FindFreeGraphicObjectSlot;

	// Prepare the graphicObject with everything.
	with result do begin
		asman_logger(strcat('Cache % @ size %x%', [origfile.graphicName, _sizexp, _sizeyp]));
		graphicName := origfile.graphicName;
		sourceFile := origfile;

		// Convert the original size to the new resolution, using a direct size/size multiplier.
		sizeXP := _sizexp;
		frameHeightP := _sizeyp;
		sizeYP := frameHeightP * sourceFile.frameCount;
		setlength(framePtr, sourceFile.frameCount);
		origOfsX := 0; origOfsY := 0;
		origSizeX := 0; origFrameHeight := 0;

		newerLink := 0;
		olderLink := graphicObjects[0].olderLink;
		graphicObjects[0].olderLink := objIndex;
		graphicObjects[olderLink].newerLink := objIndex;

		if origObject <> NIL then
			ResizeFrom(origObject)
		else
			if NOT LoadFromFile then FillGradient;
	end;
end;

function GetDynamicGraphicObject(const loadcmd : UTF8string; rqsizex, rqsizey : dword) : TGraphicObject;
// Generates a new graphic object as described in the load command, at the given pixel size.
// The pixel height will be interpreted as the frame height rather than the full height of the bitmap. (Otherwise
// frames would bleed in each other.)
// If the graphic couldn't be loaded, throws an exception.
var i : dword = 2;
	fromofs : dword;

	procedure _Flat;
	// >FLAT color
	var color : RGBAquad;
	begin
		dword(color) := 0;
		while (i <= loadcmd.Length) and (loadcmd[i] = ' ') do inc(i);
		if i <= loadcmd.Length then color.FromRGBA4(valhex(copy(loadcmd, i)));
		with result do begin
			setlength(bitmap, sizeXP * sizeYP shl 2);
			setlength(framePtr, 1);
			framePtr[0] := @bitmap[0];
			if color.a = $FF then
				memFormat := EBitmapFormat.MCG_FORMAT_BGRX
			else begin
				memFormat := EBitmapFormat.MCG_FORMAT_BGRA;
				dword(color) := mcg_PremulRGBA32(dword(color));
			end;
			filldword(bitmap[0], sizeXP * sizeYP, dword(color));
		end;
	end;

	procedure _LinearGradient;
	// >LINEAR-GRADIENT [direction] color-stop color-stop ...
	begin
		raise Exception.Create('not imp');
	end;

	procedure _Copy;
	// >COPY graphic width frameheight [src x ofs] [src y ofs] [framecount]
	var name : UTF8string;
		srcimg : TGraphicObject;
		tempimg : mcg_bitmap = NIL;
		srcp, writep, writep2 : pointer;
		j, srccopywidth, destframebytes : dword;
		w, h, frameheight, ofsxp, ofsyp, framecount : longint;
	begin
		while (i <= loadcmd.Length) and (loadcmd[i] = ' ') do inc(i);
		fromofs := i;
		while (i <= loadcmd.Length) and (NOT (loadcmd[i] in [' ',','])) do inc(i);
		name := copy(loadcmd, fromofs, i - fromofs);

		j := GetGraphicFile(name);
		if j = 0 then raise Exception.Create('GetDynGraphicObj: sourcefile not found: ' + name);
		result.sourceFile := graphicFiles[j];
		with result do begin
			srcimg := FindSizedGraphic(name, sourceFile.origSizeXP, sourceFile.origFrameHeightP);
			if srcimg = NIL then
				srcimg := LoadAtSize(sourceFile.origSizeXP, sourceFile.origFrameHeightP, NIL, sourceFile);
		end;
		Assert(srcimg <> NIL);
		result.memFormat := srcimg.memFormat;

		while (i <= loadcmd.Length) and (loadcmd[i] in [' ',',']) do inc(i);
		w := CutNumberFromString(loadcmd, i);
		frameheight := CutNumberFromString(loadcmd, i);
		ofsxp := CutNumberFromString(loadcmd, i);
		ofsyp := CutNumberFromString(loadcmd, i);
		framecount := CutNumberFromString(loadcmd, i);
		if framecount < 1 then framecount := 1;
		setlength(result.framePtr, framecount);

		// Negative sizes are valid (growing toward top left), just subtract size from source offset and flip size.
		if w < 0 then begin inc(ofsxp, w); w := -w; end;

		h := frameheight * framecount;
		if frameheight < 0 then begin
			inc(ofsyp, h);
			frameheight := -frameheight;
			h := -h;
		end;

		// Source coords out of bounds are an error.
		if (ofsxp < 0) or (ofsxp + w > longint(srcimg.sizeXP))
		or (ofsyp < 0) or (ofsyp + h > longint(srcimg.sizeYP)) then
			raise Exception.Create(
				strcat('GetDynGraphicObj: % src coords %,% %x% out of bounds', [name, ofsxp, ofsyp, w, h]));

		i := srcimg.sizeXP shl 2; // stride
		srccopywidth := w shl 2;
		destframebytes := rqsizex * rqsizey shl 2;
		setlength(result.bitmap, dword(framecount) * destframebytes);
		srcp := @srcimg.bitmap[0] + (ofsyp * longint(srcimg.sizeXP) + ofsxp) shl 2;
		writep := @result.bitmap[0];
		for j := 0 to framecount - 1 do begin
			// Todo: really need a resizer from an arbitrary pointer to avoid this alloc+copy every time...
			tempimg := mcg_bitmap.Init(w, frameheight, srcimg.memFormat, 32);
			try
				writep2 := @tempimg.bitmap[0];
				for h := frameheight - 1 downto 0 do begin
					move(srcp^, writep2^, srccopywidth);
					inc(srcp, i);
					inc(writep2, srccopywidth);
				end;
				tempimg.Resize(rqsizex, rqsizey);
				result.framePtr[j] := writep;
				move(tempimg.bitmap[0], writep^, destframebytes);
				inc(writep, destframebytes);
			finally
				if tempimg <> NIL then tempimg.Destroy; tempimg := NIL;
			end;
		end;
	end;

begin
	Assert(loadcmd[1] = '>');
	result := FindFreeGraphicObjectSlot;
	// Prepare the graphicObject with everything.
	with result do begin
		asman_logger(strcat('Cache % @ size %x%', [loadcmd, rqsizex, rqsizey]));
		graphicName := loadcmd;

		sizeXP := rqsizex;
		sizeYP := rqsizey; frameHeightP := rqsizey;
		origOfsX := 0; origOfsY := 0;
		origSizeX := 0; origFrameHeight := 0;
		Assert(bitmap = NIL);
		framePtr := NIL;
		sourceFile := NIL;

		newerLink := 0;
		olderLink := graphicObjects[0].olderLink;
		graphicObjects[0].olderLink := objIndex;
		graphicObjects[olderLink].newerLink := objIndex;
	end;

	try
		while (i <= loadcmd.Length) and (loadcmd[i] = ' ') do inc(i);
		fromofs := i;
		while (i <= loadcmd.Length) and (NOT (loadcmd[i] in [' ',','])) do inc(i);

		case copy(loadcmd, fromofs, i - fromofs) of
			'FLAT': _Flat;
			'LINEAR-GRADIENT': _LinearGradient;
			'COPY': _Copy;
			else raise Exception.Create('GetDynGraphicObj: No recognised command in "' + loadcmd + '"');
		end;

		// Track memory usage.
		inc(asman_gfxMemUsage, dword(length(result.bitmap)));
		result.cacheState := $FF;
	except
		result.cacheState := 0;
		result.graphicName := '';
		raise;
	end;
end;

procedure GetLoadtimeDynamicSize(const loadcmd : UTF8string; out sizex, sizey : dword; pxsize : boolean = FALSE);
// Parses loadcmd for a source image and pixel size, returns the calculated 32k or pixel size for this dynamic graphic.
// Loadcmd must be in uppercase and start with >. Returns sizey = 0 in case of error.
const _COPY = $59504F43; // "COPY"
var i : dword = 2;
	j : dword;
	l : longint;
begin
	sizex := 0; sizey := 0;
	while (i <= loadcmd.Length) and (loadcmd[i] = ' ') do inc(i);
	if (i + 3 > loadcmd.Length) or (dword((@loadcmd[i])^) <> NtoLE(dword(_COPY))) then exit;
	inc(i, 4);
	// Expecting >COPY graphic width frameheight [src x ofs] [src y ofs] [framecount]
	while (i <= loadcmd.Length) and (loadcmd[i] in [' ',',']) do inc(i);
	j := i;
	while (i <= loadcmd.Length) and (NOT (loadcmd[i] in [' ',','])) do inc(i);
	j := GetGraphicFile(copy(loadcmd, j, i - j));
	if j = 0 then exit; // no such source graphic found

	with graphicFiles[j] do begin
		while (i <= loadcmd.Length) and (loadcmd[i] in [' ',',']) do inc(i);
		l := abs(CutNumberFromString(loadcmd, i));
		if l = 0 then exit;
		if pxsize then
			sizex := l
		else
			sizex := (dword(l shl 15) + origResXP shr 1) div origResXP;

		while (i <= loadcmd.Length) and (loadcmd[i] in [' ',',']) do inc(i);
		l := abs(CutNumberFromString(loadcmd, i));
		if l = 0 then exit;
		if pxsize then
			sizey := l
		else
			sizey := (dword(l shl 15) + origResYP shr 1) div origResYP;
	end;
end;

function GetGraphicObject(const name : UTF8string; rqsizexp, rqsizeyp : dword) : TGraphicObject;
// Call this to get the first graphicObject that matches the given name and size. If the graphic is not cached yet,
// blocks execution until caching finishes.
// The pixel height will be interpreted as the frame height rather than the full height of the bitmap. (Otherwise
// frames would bleed in each other.) Request size 0 to get a non-resized version.
// If the graphic doesn't exist or couldn't be loaded, returns NIL.
var origfile : TGraphicFile = NIL;
	i, j : dword;
begin
	// If the request is for a dynamic runtime graphic, forward the request as a loadtime type.
	Assert(name <> '');
	if name[1] = '|' then exit(GetGraphicObject('>flat ' + copy(name, 2), rqsizexp, rqsizeyp));

	// If requested size 0, substitute the original size.
	if (rqsizexp = 0) or (rqsizeyp = 0) then begin
		if name[1] = '>' then begin
			GetLoadtimeDynamicSize(name, i, j, TRUE);
			if rqsizexp = 0 then rqsizexp := i;
			if rqsizeyp = 0 then rqsizeyp := j;
		end
		else begin
			i := GetGraphicFile(name);
			if i = 0 then begin
				asman_logger('[!] GetGraphicObj: sourcefile not found: ' + name);
				exit(NIL);
			end;
			origfile := graphicFiles[i];
			if rqsizexp = 0 then rqsizexp := origfile.origSizeXP;
			if rqsizeyp = 0 then rqsizeyp := origfile.origFrameHeightP;
		end;
	end;

	// Does the requested graphic already exist in some slot at the right size?
	result := FindSizedGraphic(name, rqsizexp, rqsizeyp);
	if result <> NIL then with result do begin
		// Found, and at the requested size! Sort to most recent, we're done!
		cacheState := $FF;
		MarkAsRecentlyUsed;
		exit;
	end;

	// Not found at the requested size! Is it a loadtime dynamic graphic?
	if name[1] = '>' then
		exit(GetDynamicGraphicObject(name, rqsizexp, rqsizeyp));

	// Asking for just a normal graphic. Does it exist at all?
	if origfile = NIL then begin
		i := GetGraphicFile(name);
		if i = 0 then begin
			asman_logger('[!] GetGraphicObj: sourcefile not found: ' + name);
			exit(NIL);
		end;
		origfile := graphicFiles[i];
	end;

	// Is the graphic already cached at its original size?
	result := FindSizedGraphic(name, origfile.origSizeXP, origfile.origFrameHeightP);

	// Request the original size version, or use existing one.
	if result = NIL then
		result := LoadAtSize(origfile.origSizeXP, origfile.origFrameHeightP, NIL, origfile)
	else
		result.MarkAsRecentlyUsed;

	// Asking for a resized version of the image?
	if ((rqsizexp <> 0) and (rqsizexp <> origfile.origSizeXP))
	or ((rqsizeyp <> 0) and (rqsizeyp <> origfile.origFrameHeightP)) then
		result := LoadAtSize(rqsizexp, rqsizeyp, result, origfile);
end;

procedure ReleaseGfx;
// Frees released graphics starting from the oldest, until total memory usage fits in the allowed budget. This means
// graphics remain re-usable for some time after being released.
var i : dword;
begin
	asman_logger('ReleaseGfx');
	for i := high(graphicObjects) downto 0 do
		with graphicObjects[i] do cacheState := cacheState and 1;

	i := graphicObjects[0].newerLink;
	while (asman_gfxMemUsage > asman_gfxMemLimit) and (i <> 0) do with graphicObjects[i] do begin
		i := newerLink;
		if cacheState <> 0 then FreeGraphic;
	end;
end;

