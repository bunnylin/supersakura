unit mcscriptwriter;

{                                                                           }
{ Mooncore game script writer. Use to easily create a script memory block.  }
{ Copyright 2024 :: Kirinn Bunnylin / MoonCore                              }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

{$mode objfpc}
{$I-}

interface

uses mccommon, mcfileio;

type TScriptWriter = object
	labelList : array of ptruint;
	lineList : TStringBunch;
	jumpList : array of dword;
	finalBuffy : array of byte;
	nonCode : array of dword;
	localsList : array of dword;
	commentList : array of record
		ofs : dword;
		ctxt : UTF8string;
	end;
	lineIndex, finalBufSize, jumpCount, nonCodeCount, localsCount, commentCount : dword;
	// CurrentLine keeps being re-used to minimise number of allocations. WriteBuf writes into it,
	// WriteBufLn copies its content to lineList[lineIndex], increments lineIndex, and resets currentLen.
	currentLine : array[0..32767] of char;
	currentLen : dword;
	m_loader : TFileLoader;

	procedure WriteBuf(const line : UTF8string; linelen : dword = $FFFFFFFF);
	procedure WriteBufLn(const line : UTF8string = '');
	procedure SetLineOfs(ofs : dword);
	procedure AddJump(address : dword);
	procedure AddComment(const _txt : UTF8string; _ofs : dword);
	procedure AddNonCodeSection(startofs : dword);
	function PassNonCode(var ofs : dword) : boolean;
	procedure AddLocalVar(index : dword);
	procedure Init(const loader : TFileLoader);
	procedure GenerateFinal(striplabels : boolean);
end;

var ScriptWriter : TScriptWriter;

implementation

procedure TScriptWriter.WriteBuf(const line : UTF8string; linelen : dword = $FFFFFFFF);
begin
	if line = '' then exit;
	if linelen > line.Length then linelen := line.Length;
	if currentLen + linelen > 32767 then linelen := 32767 - currentLen; // overflow! no string should be this long

	move(line[1], currentLine[currentLen], linelen);
	inc(currentLen, linelen);
end;

procedure TScriptWriter.WriteBufLn(const line : UTF8string = '');
begin
	if line <> '' then begin
		if currentLen = 0 then
			lineList[lineIndex] := line
		else begin
			setlength(lineList[lineIndex], currentLen + line.Length);
			move(currentLine[0], lineList[lineIndex][1], currentLen);
			move(line[1], lineList[lineIndex][currentLen + 1], line.Length);
		end;
	end
	else if currentLen <> 0 then begin
		setlength(lineList[lineIndex], currentLen);
		move(currentLine[0], lineList[lineIndex][1], currentLen);
	end;
	currentLen := 0;

	inc(lineIndex);
	if lineIndex >= dword(length(labelList)) then begin
		// Extrapolate expected total lines from current count based on how far along the bytecode we've gotten.
		// This counts by buffy size rather than file size because sys98 adds buffer padding that counts toward ofs.
		setlength(labelList, length(labelList) * m_loader.buffySize div (m_loader.ofs + 1) + 40);
		setlength(lineList, length(labelList));
	end;
	labelList[lineIndex] := m_loader.ofs;
end;

procedure TScriptWriter.SetLineOfs(ofs : dword);
begin
	labelList[lineIndex] := ofs;
end;

procedure TScriptWriter.AddJump(address : dword);
// Adds the address to the list of possible jump targets, unless it's already listed. Keeps the list sorted.
var i : dword;
	j : longint;
begin
	// Note: For very complex scripts, maybe better change this linear search for a duplicate to a binary search...
	i := jumpCount;
	while i <> 0 do begin
		j := address - jumpList[i - 1];
		if j = 0 then exit; // duplicate, nothing to do
		if j > 0 then break;
		dec(i);
	end;

	if jumpCount >= dword(length(jumpList)) then setlength(jumpList, jumpCount shl 1 + 64);
	if i < jumpCount then
		for j := jumpCount - 1 downto i do
			jumpList[j + 1] := jumpList[j];
	jumpList[i] := address;
	inc(jumpCount);
end;

procedure TScriptWriter.AddComment(const _txt : UTF8string; _ofs : dword);
// Add a comment that will be printed in the final output when the given offset is reached.
// Don't use this for immediate comments, those should be output directly with WriteLine etc.
// Use this for delayed comments, eg. when reading a list of jumps, stash comments at the jump landing points
// describing where the jump came from. Particularly useful with choice outcomes.
var i : dword;
begin
	if commentCount >= dword(length(commentList)) then setlength(commentList, 16 + commentCount shl 1);
	// Keep the comments sorted at all times. Lowest ofs at start of list.
	i := commentCount;
	while i <> 0 do begin
		dec(i);
		if commentList[i].ofs <= _ofs then begin inc(i); break; end;
		commentList[i + 1] := commentList[i];
	end;
	commentList[i].ofs := _ofs;
	commentList[i].ctxt := _txt;
	inc(commentCount);
end;

procedure TScriptWriter.AddNonCodeSection(startofs : dword);
// Add a non-code section start offset into a sorted list of non-code sections. When your read offset reaches the
// lowest listed non-code offset, call PassNonCode to skip ahead to the next code section.
var i, j : dword;
begin
	if nonCodeCount >= dword(length(nonCode)) then setlength(nonCode, nonCodeCount + nonCodeCount shr 1 + 16);
	// Find the index to insert the new ofs.
	i := 0;
	while (i < nonCodeCount) and (nonCode[i] < startofs) do inc(i);

	if i < nonCodeCount then begin
		if nonCode[i] = startofs then exit; // already listed
		// Shift up all above the target index.
		for j := nonCodeCount - 1 downto i do nonCode[j + 1] := nonCode[j];
	end;

	nonCode[i] := startofs;
	inc(nonCodeCount);
end;

function TScriptWriter.PassNonCode(var ofs : dword) : boolean;
// Call this when the read offset has reached a known non-code script section. The next known code section is
// the next nearest jump label. Read offset is moved ahead there, and the non-code entry is discarded.
// Returns TRUE if reached the end of bytecode (so you can stop parsing).
var i, delcount : dword;
begin
	if jumpCount = 0 then exit(TRUE); // no known jumps at all, all remaining is noncode
	i := jumpCount;
	while (i <> 0) and (jumpList[i - 1] > ofs) do dec(i);
	if i = jumpCount then exit(TRUE); // no known jumps after current location, all remaining is noncode
	i := jumpList[i];
	if i >= m_loader.buffySize then exit(TRUE); // jump out of bounds, no more code in this file
	ofs := i;
	delcount := 0;
	while (delcount < nonCodeCount) and (nonCode[delcount] < i) do inc(delcount);
	if (delcount <> 0) and (delcount < nonCodeCount) then
		for i := delcount to nonCodeCount - 1 do nonCode[i - delcount] := nonCode[i];
	dec(nonCodeCount, delcount);
	result := FALSE;
end;

procedure TScriptWriter.AddLocalVar(index : dword);
// Maintains a list of all local variables seen used in this script. These are of the form $v<index>, index in hex.
var i : dword;
begin
	if localsCount <> 0 then for i := localsCount - 1 downto 0 do
		if localsList[i] = index then exit;
	if localsCount >= dword(length(localsList)) then setlength(localsList, localsCount shl 1 + 8);
	localsList[localsCount] := index;
	inc(localsCount);
end;

procedure TScriptWriter.Init(const loader : TFileLoader);
begin
	labelList := NIL; lineList := NIL; jumpList := NIL; finalBuffy := NIL;

	// Typically bytecode scripts generate one line for every 15-20 bytes. This means we can guess how many lines
	// should be allocated just by knowing the input bytecode size.
	// This uses file size, because even if a buffer is padded, the padding doesn't add new lines.
	setlength(labelList, loader.fullFileSize div 15 + 80);
	setlength(lineList, length(labelList));

	jumpList := NIL; // C's CC files don't have any jumps
	nonCode := NIL; // not used at all in some bytecode
	localsList := NIL;
	finalBufSize := 0;
	currentLen := 0;
	currentLine[32767] := #0; // terminating null just in case
	lineIndex := 0;
	jumpCount := 0;
	nonCodeCount := 0;
	localsCount := 0;
	commentCount := 0;
	m_loader := loader;
end;

procedure TScriptWriter.GenerateFinal(striplabels : boolean);
// Concatenates all written lines from lineList into a byte array, finalBuffy. Sets finalBufSize to its byte size.
// Also inserts labels and comments at their intended lines.
var writep : pointer;
	i, l, comm : dword;
	txt : string[16];
	labelinjumplist : boolean;
begin
	// Set up a buffer that can hold all script lines.
	finalBufSize := localsCount * 16;
	if lineIndex <> 0 then for i := lineIndex - 1 downto 0 do
		inc(finalBufSize, lineList[i].Length + 9);
	if commentCount <> 0 then for i := commentCount - 1 downto 0 do
		inc(finalBufSize, commentList[i].ctxt.Length + 4);
	setlength(finalBuffy, finalBufSize);
	writep := @finalBuffy[0];

	// Print reset instructions for local variables.
	if localsCount <> 0 then begin
		for i := localsCount - 1 downto 0 do begin
			txt := '$v' + strhex(localsList[i]) + ':=0'#$A;
			move(txt[1], writep^, length(txt));
			inc(writep, length(txt));
		end;
	end;

	// Build the sakurascript lines into a single memory block.
	l := 0;
	comm := 0;
	if lineIndex <> 0 then for i := 0 to lineIndex - 1 do begin

		// If this line's label address is in jumplist, add a linebreak to visually differentiate code blocks.
		labelinjumplist := (l < jumpCount) and (labelList[i] >= jumpList[l]);
		if labelinjumplist then begin
			byte(writep^) := $A; inc(writep);
			inc(l);
		end;

		// Print the label if this line's label address is in jumpList, or keeping all debug labels, and this label
		// is different from the previous line's label.
		if ((NOT striplabels) or (labelinjumplist))
		and ((i = 0) or (labelList[i] <> labelList[i - 1]))
		then begin
			char(writep^) := '@'; inc(writep);
			txt := strhex(labelList[i], 4);
			dword(writep^) := dword((@txt[1])^); inc(writep, 4);
			char(writep^) := ':'; inc(writep);
			char(writep^) := ' '; inc(writep);
		end;

		// Print stashed comments as their offset comes up.
		while (comm < commentCount) and (commentList[comm].ofs <= labelList[i]) do begin
			word(writep^) := word($2F2F); inc(writep, 2); // two forward-slashes
			char(writep^) := ' '; inc(writep);
			with commentList[comm] do if ctxt <> '' then begin
				move(ctxt[1], writep^, ctxt.Length);
				inc(writep, ctxt.Length);
			end;
			byte(writep^) := $A; inc(writep);
			inc(comm);
		end;

		// Print the generated script line.
		if lineList[i].Length <> 0 then begin
			move(lineList[i][1], writep^, lineList[i].Length);
			inc(writep, lineList[i].Length);
			lineList[i] := '';
		end;

		// Line break after each line.
		byte(writep^) := $A;
		inc(writep);
	end;
	finalBufSize := writep - @finalBuffy[0];
end;
end.

