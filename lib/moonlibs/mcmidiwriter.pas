unit mcmidiwriter;
{                                                                           }
{ Mooncore MIDI data writer. Use to easily create a MIDI file in-memory.    }
{ Copyright 2019 :: Kirinn Bunnylin / MoonCore                              }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

{$mode objfpc}
{$I-}

interface

type TMidiWriter = class; // forward

type TMidiCommand = record
		tick : dword;
		data : array of byte;
	end;

type TMidiTrack = class
	private
		container : TMidiWriter;
		commands : array of TMidiCommand;
		command_count, single_instrument : dword;
		renderbuffy : array of byte;
		procedure Render;

	public
		track_name : UTF8string; // will be saved as a meta text 3 event at start of track
		current_tick : dword;
		midichan : byte;

		procedure AddCommand(at_tick : longint; datalen : byte; dataptr : pointer);
		procedure AddController(ctrlnumber, value : byte; at_tick : longint = -1);
		procedure AddMetaString(metanumber : byte; const txt : UTF8string; at_tick : longint = -1);
		procedure SelectInstrument(instrument : byte; bank : longint = -1; at_tick : longint = -1);
		procedure SetAftertouch(velocity : byte; note : longint = -1; at_tick : longint = -1);
		procedure SetBendRange(total_cents : dword; at_tick : longint = -1);
		procedure SetDetune(cents : longint; at_tick : longint = -1);
		procedure SetExpression(volume : byte; at_tick : longint = -1);
		procedure SetPitchBend(bend : dword; at_tick : longint = -1);
		procedure SetPortamento(enabled : boolean; duration : shortint = -1; at_tick : longint = -1);
		procedure SetTremolo(depth : byte; at_tick : longint = -1);
		procedure SetVibrato(depth : byte; at_tick : longint = -1);
		procedure SetVolume(volume : byte; at_tick : longint = -1);
		procedure NoteOff(note, velocityoff : byte; at_tick : longint = -1);
		procedure NoteOn(note, velocity : byte; at_tick : longint = -1);
		procedure Reset;

		constructor Create(creator : TMidiWriter; midichannel : byte);
	end;

type TMidiWriter = class
	private
		loopstart_tick : dword;
	public
		track : array of TMidiTrack;
		sequence_name : UTF8string; // will be saved as a meta text 3 event at start of first track
		ticks_per_quarter_note : word;

		procedure SetNumberOfTracks(numtracks : byte);
		procedure WriteMetaString(at_tick : longint; metanumber : byte; const txt : UTF8string);
		procedure SetTempo(at_tick : longint; usec_per_beat : dword);
		procedure Finalise(out midibuffer : pointer; out midibuffersize : dword);

		constructor Create;
		destructor Destroy(); override;
end;

// ------------------------------------------------------------------

implementation

uses mccommon;

procedure WriteDelta(delta : dword; var writep : pointer);
begin
	if delta >= $80 then begin
		if delta >= $4000 then begin
			if delta >= $200000 then begin
				byte(writep^) := $80 or byte(delta shr 21); inc(writep);
			end;
			byte(writep^) := $80 or byte(delta shr 14); inc(writep);
		end;
		byte(writep^) := $80 or byte(delta shr 7); inc(writep);
	end;
	byte(writep^) := delta and $7F; inc(writep);
end;

constructor TMidiTrack.Create(creator : TMidiWriter; midichannel : byte);
begin
	container := creator;
	midichan := midichannel;
	current_tick := 0;
	command_count := 0;
	single_instrument := $FFFFFFFF;
	track_name := '';
end;

procedure TMidiTrack.AddCommand(at_tick : longint; datalen : byte; dataptr : pointer);
// Inserts a command in a track's command list sorted by time.
var i : dword;
begin
	if at_tick < 0 then at_tick := current_tick;
	if command_count >= dword(length(commands)) then setlength(commands, length(commands) * 2 + 16);

	i := command_count;
	while i <> 0 do begin
		dec(i);
		if commands[i].tick <= dword(at_tick) then begin inc(i); break; end;
		commands[i + 1].tick := commands[i].tick;
		commands[i + 1].data := commands[i].data;
	end;

	with commands[i] do begin
		tick := at_tick;
		setlength(data, 0);
		setlength(data, datalen);
		move(dataptr^, data[0], datalen);
	end;
	inc(command_count);
end;

procedure TMidiTrack.AddController(ctrlnumber, value : byte; at_tick : longint = -1);
var a : array[0..2] of byte;
begin
	a[0] := $B0 + midichan;
	a[1] := ctrlnumber and $7F;
	a[2] := value and $7F;
	AddCommand(at_tick, 3, @a[0]);
end;

procedure TMidiTrack.AddMetaString(metanumber : byte; const txt : UTF8string; at_tick : longint = -1);
// Saves a meta text string on this track at the given tick count. The allowed meta type numbers are:
//   01 - General text
//   03 - Sequence/track name
//   04 - Instrument name
//   05 - Lyric
//   06 - General marker or loop point
//   07 - Cue point
//   08 - Program name
//   09 - Device name
var a : array of byte = NIL;
	writep : pointer;
begin
	Assert(metanumber in [1,3..15], 'Bad metanumber: ' + strdec(metanumber));

	// Remember the loop start point, all tracks need to reissue their going state at this tick to loop cleanly.
	if (metanumber = 6) and (lowercase(txt) = 'loopstart') then begin
		if at_tick >= 0 then container.loopstart_tick := at_tick else container.loopstart_tick := current_tick;
	end;

	setlength(a, 6 + txt.Length);
	a[0] := $FF;
	a[1] := metanumber;
	writep := @a[2];
	WriteDelta(txt.Length, writep);
	if txt <> '' then begin
		move(txt[1], writep^, txt.Length);
		inc(writep, txt.Length);
	end;
	AddCommand(at_tick, writep - @a[0], @a[0]);
end;

procedure TMidiTrack.SetAftertouch(velocity : byte; note : longint = -1; at_tick : longint = -1);
// Sets a new aftertouch velocity on this track. Polyphonic if note is specified, otherwise channel aftertouch.
var a : array[0..2] of byte;
begin
	Assert(velocity < $80, 'Bad aftertouch velocity: ' + strdec(velocity));
	if note in [0..$7F] then begin
		a[0] := $A0 + midichan;
		a[1] := note;
		a[2] := velocity;
		AddCommand(at_tick, 3, @a[0]);
	end
	else begin
		a[0] := $D0 + midichan;
		a[1] := velocity;
		AddCommand(at_tick, 2, @a[0]);
	end;
end;

procedure TMidiTrack.SetBendRange(total_cents : dword; at_tick : longint = -1);
// Sets a new pitch bend range on this track, to the given total cents.
// Note, that the actual value passed to MIDI is half this, since it expects a +/- value, not the total.
var midirange : dword;
	a : array[0..2] of byte;
begin
	midirange := total_cents shr 1;
	Assert(midirange < $7F * 100, 'Bend range oob: ' + strdec(midirange));
	// Select registered parameter number 0.
	a[0] := $B0 + midichan;
	a[1] := 100;
	a[2] := 0;
	AddCommand(at_tick, 3, @a[0]);
	a[1] := 101;
	AddCommand(at_tick, 3, @a[0]);
	// Coarse data entry. (semitones)
	a[1] := $06;
	a[2] := midirange div 100;
	AddCommand(at_tick, 3, @a[0]);
	// Fine data entry. (cents)
	a[1] := $26;
	a[2] := midirange mod 100;
	AddCommand(at_tick, 3, @a[0]);
	// End controller sequence.
	a[1] := 100;
	a[2] := $7F;
	AddCommand(at_tick, 3, @a[0]);
	a[1] := 101;
	AddCommand(at_tick, 3, @a[0]);
end;

procedure TMidiTrack.SetDetune(cents : longint; at_tick : longint = -1);
// Sets detuning on this track.
var a : array[0..2] of byte;
	i : longint;
begin
	Assert(cents >= -6400, 'Detune too low: ' + strdec(cents));
	Assert(cents <= 6399, 'Detune too high: ' + strdec(cents));
	a[0] := $B0 + midichan;
	a[1] := 0;
	// Select registered parameter number 1, master fine detune.
	i := (cents mod 100) * $4000 div 100 + $2000;
	a[1] := 100;
	a[2] := 0;
	AddCommand(at_tick, 3, @a[0]);
	a[1] := 101;
	a[2] := 1;
	AddCommand(at_tick, 3, @a[0]);
	// Coarse data entry. (cents)
	a[1] := $06;
	a[2] := byte(i shr 7);
	AddCommand(at_tick, 3, @a[0]);
	// Fine data entry. (cents)
	a[1] := $26;
	a[2] := byte(i and $7F);
	AddCommand(at_tick, 3, @a[0]);

	// Select registered parameter number 2, master coarse detune.
	i := (cents div 100) + 64;
	a[1] := 100;
	a[2] := 0;
	AddCommand(at_tick, 3, @a[0]);
	a[1] := 101;
	a[2] := 2;
	AddCommand(at_tick, 3, @a[0]);
	// Coarse data entry. (semitones)
	a[1] := $06;
	a[2] := i;
	AddCommand(at_tick, 3, @a[0]);

	// End controller sequence.
	a[1] := 100;
	a[2] := $7F;
	AddCommand(at_tick, 3, @a[0]);
	a[1] := 101;
	AddCommand(at_tick, 3, @a[0]);
end;

procedure TMidiTrack.SetExpression(volume : byte; at_tick : longint = -1);
begin
	Assert(volume < $80, 'Bad expression: ' + strdec(volume));
	AddController(11, volume, at_tick);
end;

procedure TMidiTrack.SetPitchBend(bend : dword; at_tick : longint = -1);
var a : array[0..2] of byte;
begin
	Assert(bend < $4000, 'Bad bend: $' + strhex(bend));
	a[0] := $E0 + midichan;
	// Unlike most of midi, pitch bends are little-endian!
	a[1] := bend and $7F;
	a[2] := (bend shr 7) and $7F;
	AddCommand(at_tick, 3, @a[0]);
end;

procedure TMidiTrack.SetPortamento(enabled : boolean; duration : shortint = -1; at_tick : longint = -1);
var a : array[0..2] of byte;
begin
	a[0] := $B0 + midichan;
	a[1] := 65;
	a[2] := 0;
	if enabled then a[2] := $FF;
	AddCommand(at_tick, 3, @a[0]);

	if duration >= 0 then begin
		a[1] := 5;
		a[2] := duration;
		AddCommand(at_tick, 3, @a[0]);
	end;
end;

procedure TMidiTrack.SetTremolo(depth : byte; at_tick : longint = -1);
begin
	Assert(depth < $80, 'Bad tremolo: ' + strdec(depth));
	AddController(92, depth, at_tick);
end;

procedure TMidiTrack.SetVibrato(depth : byte; at_tick : longint = -1);
begin
	Assert(depth < $80, 'Bad vibrato: '+ strdec(depth));
	AddController(1, depth, at_tick);
end;

procedure TMidiTrack.SelectInstrument(instrument : byte; bank : longint = -1; at_tick : longint = -1);
var a : array[0..1] of byte;
begin
	if bank >= 0 then AddController(0, bank and $7F, at_tick);
	a[0] := $C0 + midichan;
	a[1] := instrument and $7F;
	AddCommand(at_tick, 2, @a[0]);
	// Keep track of whether only a single instrument has been selected on this channel.
	if single_instrument = $FFFFFFFF then
		single_instrument := instrument
	else if single_instrument <> instrument then
		single_instrument := $FEFEFEFE;
end;

procedure TMidiTrack.SetVolume(volume : byte; at_tick : longint = -1);
begin
	Assert(volume < $80, 'Bad volume: ' + strdec(volume));
	AddController(7, volume, at_tick);
end;

procedure TMidiTrack.NoteOff(note, velocityoff : byte; at_tick : longint = -1);
var a : array[0..2] of byte;
begin
	Assert(note or velocityoff < $80, strcat('Bad note/velocity: %/%', [note, velocityoff]));
	a[0] := $80 + midichan;
	a[1] := note and $7F;
	a[2] := velocityoff and $7F;
	AddCommand(at_tick, 3, @a[0]);
end;

procedure TMidiTrack.NoteOn(note, velocity : byte; at_tick : longint = -1);
var a : array[0..2] of byte;
begin
	Assert(note or velocity < $80, strcat('Bad note/velocity: %/%', [note, velocity]));
	a[0] := $90 + midichan;
	a[1] := note and $7F;
	a[2] := velocity and $7F;
	AddCommand(at_tick, 3, @a[0]);
end;

procedure TMidiTrack.Reset;
// Resets a midi track to neutral defaults: all controllers off and pitch bend centered, using instrument bank 0.
// (Really, the midi player should do all this itself, but some don't, so best write this at the start of all tracks.)
begin
	AddController(121, 0, 0);
	SetBendRange(400, 0);
	SetPitchBend($2000, 0);
	AddController(0, 0, 0);
end;

procedure TMidiTrack.Render;
var writep : pointer;
	i, t, buffysize : dword;
	last : record
		tick, bend : dword;
		command, bank, volume, expression, instrument, vibra, porta_enabled, porta_duration, chanpres : byte;
		rpn_high_byte : byte;
	end;
	com : TMidiCommand;
	loopstart_not_passed : boolean;
begin
	setlength(renderbuffy, 0);
	buffysize := 65536 + track_name.Length;
	if self = container.track[0] then inc(buffysize, container.sequence_name.Length);
	setlength(renderbuffy, buffysize);
	writep := @renderbuffy[0];
	dword(writep^) := BEtoN(dword($4D54726B)); inc(writep, 8); // MTrk

	// Write the sequence/track name. First must always be the sequence name, subsequent are track names.
	if (self = container.track[0]) and ((container.sequence_name <> '') or (track_name <> '')) then begin
		if container.sequence_name.Length > 255 then setlength(container.sequence_name, 255);
		byte(writep^) := 0; inc(writep);
		byte(writep^) := $FF; inc(writep);
		byte(writep^) := 3; inc(writep);
		byte(writep^) := container.sequence_name.Length; inc(writep);
		if container.sequence_name.Length <> 0 then begin
			move(container.sequence_name[1], writep^, container.sequence_name.Length);
			inc(writep, container.sequence_name.Length);
		end;
	end;

	if track_name <> '' then begin
		if track_name.Length > 255 then setlength(track_name, 255);
		byte(writep^) := 0; inc(writep);
		byte(writep^) := $FF; inc(writep);
		byte(writep^) := 3; inc(writep);
		byte(writep^) := track_name.Length; inc(writep);
		move(track_name[1], writep^, track_name.Length); inc(writep, track_name.Length);
	end;

	// When only a single instrument is used, push the selection command up to tick 0. My MIDI editor likes seeing it
	// up front.
	if single_instrument < $FEFEFEFE then begin
		i := command_count;
		while i <> 0 do begin
			dec(i);
			if commands[i].data[0] and $F0 = $C0 then break;
		end;
		com := commands[i];
		while i <> 0 do begin
			dec(i);
			if commands[i].tick = 0 then break;
			commands[i + 1] := commands[i];
		end;
		com.tick := 0;
		commands[i + 1] := com;
	end;

	loopstart_not_passed := TRUE;
	last.tick := 0; // silence a compiler warning
	fillbyte(last, sizeof(last), $FF);
	last.tick := 0;
	i := 0;
	while i < command_count do begin
		with commands[i] do begin
			inc(i);

			// Clear the cached state at loopstart, so when looping back, state will be cleanly set.
			if (loopstart_not_passed) and (tick >= container.loopstart_tick) then begin
				t := last.tick;
				fillbyte(last, sizeof(last), $FF);
				last.tick := t;
				loopstart_not_passed := FALSE;
			end;

			// If a command doesn't change the current state, skip it.
			case data[0] and $F0 of
				$B0: // continuous controllers
				case data[1] of
					0: begin
						if data[2] = last.bank then continue;
						last.bank := data[2];
					end;
					1: begin
						if data[2] = last.vibra then continue;
						last.vibra := data[2];
					end;
					5: begin
						if data[2] = last.porta_duration then continue;
						last.porta_duration := data[2];
					end;
					7: begin
						if data[2] = last.volume then continue;
						last.volume := data[2];
					end;
					11: begin
						if data[2] = last.expression then continue;
						last.expression := data[2];
					end;
					65: begin
						if data[2] = last.porta_enabled then continue;
						last.porta_enabled := data[2];
					end;
					100: last.rpn_high_byte := data[2];
					101: begin
						// Change in bend range, don't optimise out the next pitch bend command.
						if (last.rpn_high_byte = 0) and (data[2] = 0) then last.bend := high(last.bend);
					end;
				end;

				$C0: begin
					if data[1] = last.instrument then continue;
					last.instrument := data[1];
				end;

				$D0: begin
					if data[1] = last.chanpres then continue;
					last.chanpres := data[1];
				end;

				$E0: begin
					if (data[1] = last.bend and $7F) and (data[2] = last.bend shr 7) then continue;
					last.bend := data[1] + (data[2] shl 7);
				end;
			end;

			buffysize := writep - @renderbuffy[0];
			if dword(length(renderbuffy)) < buffysize + dword(length(data)) + 4 then begin
				setlength(renderbuffy, buffysize + dword(length(data)) + 65536);
				writep := @renderbuffy[buffysize];
			end;
			t := tick - last.tick;
			last.tick := tick;
			WriteDelta(t, writep);
			if (data[0] = last.command) and (data[0] and $F0 <> $F0) then begin
				// Same command in [8x..Ex] repeats, use running status.
				move(data[1], writep^, length(data) - 1);
				inc(writep, length(data) - 1);
			end
			else begin
				move(data[0], writep^, length(data));
				inc(writep, length(data));
				last.command := data[0];
			end;
		end;
	end;

	dword(writep^) := $002FFF00; inc(writep, 4); // end of track mark
	buffysize := writep - @renderbuffy[0];
	dword((@renderbuffy[4])^) := NtoBE(dword(buffysize - 8)); // track length
	setlength(renderbuffy, buffysize);
end;

// ------------------------------------------------------------------

constructor TMidiWriter.Create;
begin
	loopstart_tick := high(loopstart_tick);
	ticks_per_quarter_note := 48;
	sequence_name := '';
end;

destructor TMidiWriter.Destroy;
var i : byte;
begin
	if length(track) <> 0 then for i := high(track) downto 0 do track[i].Destroy;
	setlength(track, 0);
	inherited;
end;

procedure TMidiWriter.SetNumberOfTracks(numtracks : byte);
var i : byte;
begin
	inc(numtracks);

	if numtracks = length(track) then exit;
	if numtracks < length(track) then begin
		for i := high(track) downto numtracks do track[i].Destroy;
		setlength(track, numtracks);
	end
	else begin
		i := length(track);
		setlength(track, numtracks);
		while i < numtracks do begin
			track[i] := TMidiTrack.Create(self, i and $F);
			inc(i);
		end;
	end;
end;

procedure TMidiWriter.WriteMetaString(at_tick : longint; metanumber : byte; const txt : UTF8string);
// Saves a meta text string on track 0 at the given tick count.
begin
	track[0].AddMetaString(metanumber, txt, at_tick);
end;

procedure TMidiWriter.SetTempo(at_tick : longint; usec_per_beat : dword);
// Saves a tempo change on track 0 at the given tick count.
var a : array[0..5] of byte;
begin
	a[0] := $FF; // meta-event
	a[1] := $51; // set tempo
	a[2] := 3; // three data bytes
	a[3] := byte(usec_per_beat shr 16); // tempo, MSB-first
	a[4] := byte(usec_per_beat shr 8);
	a[5] := byte(usec_per_beat);
	track[0].AddCommand(at_tick, 6, @a[0]);
end;

procedure TMidiWriter.Finalise(out midibuffer : pointer; out midibuffersize : dword);
// Produces a MIDI file in midibuffer^ from the current tracks. Memory for the buffer is reserved by this call, and it
// is the caller's responsibility to free it.
var writep : pointer;
	i : dword;
begin
	midibuffer := NIL;
	midibuffersize := 14;
	for i := high(track) downto 0 do with track[i] do begin
		Render;
		inc(midibuffersize, dword(length(renderbuffy)));
	end;
	getmem(midibuffer, midibuffersize);
	writep := midibuffer;

	dword(writep^) := BEtoN(dword($4D546864)); inc(writep, 4); // MThd
	dword(writep^) := NtoBE(dword(6)); inc(writep, 4); // header length
	word(writep^) := NtoBE(word(1)); inc(writep, 2); // file type = 1, multitrack synchronous
	word(writep^) := NtoBE(word(length(track))); inc(writep, 2); // number of tracks
	word(writep^) := NtoBE(word(ticks_per_quarter_note)); inc(writep, 2); // ticks per quarter note

	for i := 0 to high(track) do with track[i] do begin
		move(renderbuffy[0], writep^, length(renderbuffy));
		inc(writep, length(renderbuffy));
		setlength(renderbuffy, 0);
	end;
end;

initialization
finalization
end.

