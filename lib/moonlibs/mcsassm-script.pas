{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

destructor TScriptObject.Destroy;
var j : dword;
begin
	setlength(code, 0);
	if length(stringTable) <> 0 then begin
		for j := high(stringTable) downto 0 do
			setlength(stringTable[j].txt, 0);
		setlength(stringTable, 0);
	end;
	inherited;
end;

function GetScript(const labelname : UTF8string) : dword;
// Returns the index of a scriptObject with the given name. If not found, returns 0.
// Can't return the object itself since LoadDat may need to overwrite the index.
// The search is case-sensitive, but all script names shall be in uppercase.
// The scriptObjects[] array must be sorted before calling this.
var comp, min, max : longint;
begin
	if (labelname = '') or (scriptObjectCount < 2) then begin
		result := 0; exit;
	end;
	// binary search
	min := 1; max := scriptObjectCount - 1;
	repeat
		result := (min + max) shr 1;
		comp := CompStr(
			@labelname[1], @scriptObjects[result].labelName[1],
			labelname.Length, length(scriptObjects[result].labelName));
		if comp = 0 then exit;
		if comp > 0 then
			min := result + 1
		else
			max := result - 1;
	until min > max;
	result := 0;
end;

procedure SortScripts(onlylast : boolean);
var i, j : dword;
	tempscr : TScriptObject;
begin
	if scriptObjectCount < 3 then exit; // don't touch index 0, must have at least 2 others to sort
	// Insertion sort.
	i := 2;
	if onlylast then i := scriptObjectCount - 1;
	while i < scriptObjectCount do begin
		tempscr := scriptObjects[i];
		j := i - 1;
		while (j <> 0) and (scriptObjects[j].labelName > tempscr.labelName) do begin
			scriptObjects[j + 1] := scriptObjects[j];
			dec(j);
		end;
		scriptObjects[j + 1] := tempscr;
		inc(i);
	end;
end;

procedure ImportScripts(poku : pointer; blocksize : dword);
// Poku^ must point to an uncompressed SuperSakura script block. This splits the scripts into memory and resorts the
// script array.
// The caller is responsible for freeing poku.
// Throws an exception in case of errors.
var streamp, streamend : pointer;
	templabel : TScriptObject;
	newlabelcount, i, j : dword;
	newlabellist : array of TScriptObject = NIL;
	languagemap : array of byte = NIL;
	lang : ShortString;
begin
	if blocksize < 4 then raise Exception.Create('too tiny sakurascript chunk in DAT');

	// Tempbuffy now contains a series of concatenated script label records.
	// For each, where a label name already exists in memory, it is overwritten; otherwise save as a new script label.
	setlength(newlabellist, 256);
	newlabelcount := 0;
	streamp := poku;
	streamend := streamp + blocksize;

	setlength(languagemap, byte(streamp^)); inc(streamp);
	if length(languagemap) = 0 then raise Exception.Create('no language list in sakurascript chunk');
	for i := 0 to high(languagemap) do begin
		byte(lang[0]) := byte(streamp^);
		inc(streamp);
		if streamp + byte(lang[0]) + 1 >= streamend then
			raise Exception.Create('language descriptor out of bounds');
		move(streamp^, lang[1], byte(lang[0]));
		inc(streamp, byte(lang[0]));

		languagemap[i] := FindOrAddLanguage(lang);
	end;

	while streamp < streamend do begin
		// Bounds check...
		i := byte(streamp^);
		if (i = 0) or (streamp + i + 1 >= streamend) then break;

		templabel := TScriptObject.Create;
		templabel.code := NIL;
		setlength(templabel.stringTable, 0);
		// Get and uppercase the label name.
		templabel.labelName := upcase(ShortString(streamp^));
		inc(streamp, i + 1);

		// Get and uppercase the next label name.
		i := byte(streamp^);
		if streamp + i + 1 > streamend then break;
		templabel.nextLabel := upcase(ShortString(streamp^));
		inc(streamp, i + 1);

		// Get the label language.
		if byte(streamp^) >= length(languagemap) then
			raise Exception.Create('bad language byte $' + strhex(byte(streamp^)));
		templabel.labelLanguage := languagemap[byte(streamp^)];
		inc(streamp);
		defaultScriptLanguage := templabel.labelLanguage;

		// Get the code size.
		if streamp + 4 > streamend then break;
		i := LEtoN(dword(streamp^));
		setlength(templabel.code, i);
		inc(streamp, 4);
		// Get the bytecode.
		if streamp + i > streamend then break;
		if length(templabel.code) <> 0 then begin
			move(streamp^, templabel.code[0], length(templabel.code));
			inc(streamp, length(templabel.code));
		end;

		// Does this label exist yet?
		i := GetScript(templabel.labelName);

		if i <> 0 then begin
			templabel.stringTable := scriptObjects[i].stringTable;
			scriptObjects[i].stringTable := NIL;
			// Label has been previously loaded! Overwrite the old one, but preserve the string table.
			scriptObjects[i].Destroy;
			scriptObjects[i] := templabel;
		end
		else begin
			// Label has not been previously loaded! Append to the new labels list.
			// (The label may already be in the new label list if the dat file has been hacked or incorrectly
			// generated, but in that case the engine will just randomly use one or the other script.)
			if newlabelcount >= dword(length(newlabellist)) then
				setlength(newlabellist, length(newlabellist) + 256);
			newlabellist[newlabelcount] := templabel;
			inc(newlabelcount);

			setlength(templabel.stringTable, languageList.Length);
		end;
		templabel := NIL;
	end;
	if templabel <> NIL then begin
		asman_logger('out of bounds loading script ' + templabel.labelName);
		templabel.Destroy; templabel := NIL;
		raise Exception.Create('out of bounds loading script');
	end;

	// Append newlabellist[] to script[], and re-sort.
	if newlabelcount <> 0 then begin
		setlength(scriptObjects, scriptObjectCount + newlabelcount);
		// Since there are dynamic arrays involved, can't just "move" the memory.
		for j := 0 to newlabelcount - 1 do begin
			scriptObjects[scriptObjectCount] := newlabellist[j];
			inc(scriptObjectCount);
		end;
		setlength(newlabellist, 0);
		SortScripts(FALSE);
	end;
	asman_logger('acquired ' + strdec(newlabelcount) + ' new labels from script block');
end;

