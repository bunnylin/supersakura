unit mcsassm;
{                                                                           }
{ Mooncore Super Asset Manager                                              }
{ Copyright 2013-2024 :: Kirinn Bunnylin / MoonCore                         }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

// This makes available assets from DAT files, as well as loose files.
//
// All used game resource types need to be defined in this unit. The unit cleans up all loaded assets when your main
// program ends or crashes. Be sure to attach a logger procedure to asman_logger.
//
// Usage:
// - Call EnumerateDats to populate availableDats[]; call LoadDat to populate asset lists from available dats; call
//   LoadLooseFile to add override files to asset lists.
// - You need to assign your own script loader function to the asman_CompileScript function pointer.
// - Scripts are sorted by labelName for fast binary searching. Populating the list will change object indexes, except
//   the null script which is always index 0. Script objects remain valid, unless the label is overwritten, which makes
//   the old object reference invalid. You can put custom scripts safely in index 0, and should refetch active script
//   objects if loading new scripts during use. If a currently-executing script is overwritten, handle it gracefully.
// - String tables are tied to scripts, although they are saved separately. Access rules are the same as with scripts.
// - Dat files are sorted by displayName for convenience. Individual dats can't be removed; you can remove all dats
//   with UnloadDats, but that also clears asset lists.
// - Graphic files are sorted by graphicName for fast binary searching. Populating the list changes object indexes,
//   except the null file, always index 0. Graphic files remain valid, unless overwritten by a new load, which makes
//   the old file reference invalid. If you load new files at runtime, manually clear the graphic object list before
//   continuing to ensure no invalid references remain.
// - Graphic objects are not sorted, but are a linked list. Do not edit this directly. Call GetGraphicObject to
//   generate or fetch a previously generated one, based on a defined graphic file. Call ReleaseGfx periodically,
//   whenever the visible set of graphics has changed significantly. At the same time, clear all graphic object
//   references from your structures; they can't be used again without another GetGraphic.
// - If your program does a fullscreen or other window size switch, all graphics probably need to be resized to the new
//   window. Simply call ReleaseGfx and begin requesting assets at their new sizes.

{$mode objfpc}
{$inline on}
{$codepage UTF8}
{$iochecks off} // WARNING: if an IO action fails, all subsequent actions fail silently until IOresult is checked
{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

// On case-sensitive filesystems, user experience can be improved by doing some extra case-insensitive checks.
{$ifndef WINDOWS}{$define caseshenanigans}{$endif}

interface

uses
{$ifdef unix}cthreads,{$endif}
sysutils, mccommon, mcfileio, mcgloder, mcscaler;

type string31 = string[31]; // resource name, must be in uppercase

// Data bundles, sorted by displayName for user experience. 0 is invalid or runtime-generated.
type TDatFile = class
	public
		projectName : UTF8string; // folder and dat filename without suffix, must be lowercase unless creating the dat
		parentName : UTF8string; // if another dat must be loaded before this, set its projectName here
		displayName : UTF8string; // human-readable long string, eg. "Vanishing Point (PC-98)"
		gameVersion : UTF8string; // arbitrary version description, normally uses supersakura version
		mainOverride : UTF8string; // if specified, this will be the game's entry label after DAT load
		baseResX, baseResY : dword;
		loaded : boolean;
	private
		fileObject : file;
		bannerName : UTF8string; // only used when creating a dat, not saved in dat itself
		datFilePath : UTF8string;
		dataStartOfs : dword;

	private
		function ReadDATHeader : boolean;
	public
		procedure LoadDat;
		function LoadBanner(const saveasname : string31) : boolean;
		procedure WriteDat(const filepath : UTF8string);
		destructor Destroy; override;
	end;
var availableDats : array of TDatFile;
	availableDatCount : dword;
	loadedDatList : array of TDatFile;
	mainLabelName : UTF8string; // fully-qualified game entry label name, must be in uppercase

type TStringEntry = record
	txt : TStringBunch;
end;

// Sakurascript code, must be sorted by labelName, must be in uppercase. 0 is invalid or runtime-generated.
type TScriptObject = class
	public
		labelName : string[63]; // label string for this bytecode segment
		nextLabel : string[63];
		code : array of byte;
		// All unique strings for this label: stringTable[stringindex].txt[language]
		stringTable : array of TStringEntry;
		labelLanguage : byte; // index to languageList

		destructor Destroy; override;
	end;
var scriptObjects : array of TScriptObject;
	scriptObjectCount, globalStringCount, totalStringCount : dword;
	languageList : TStringBunch; // loaded language descriptors (eg. "English")
	defaultScriptLanguage : byte;
	asman_CompileScript : function(scriptname : UTF8string; inbuf, inbufend : pointer) : pointer = NIL;

// Requestable graphics, original size, must be sorted by graphicName. 0 is invalid.
type TGraphicFile = class
	public
		graphicName : string31;
		origResXP, origResYP : word;
		origSizeXP, origSizeYP : word; // pixel size in original res
		origOfsXP, origOfsYP : longint; // pixel offset of top left corner in original res
		origFrameHeightP : dword;
		frameCount : dword;
		seqLen : dword; // animation sequence length
		sequence : array of dword; // 16.16: action/frame . param/delay
		srcFilePath : UTF8string; // loose file that contains this image, or copy of dat file name, or empty

		procedure ResetMeta;
		constructor Create(const name : string31);
		function Clone : TGraphicFile;
		destructor Destroy; override;
	private
		srcDat : TDatFile; // dat that contains this image, NIL to use srcFilePath instead
		srcFileReadOfs, srcFileReadSize : dword;
	end;
var graphicFiles : array of TGraphicFile;
	graphicFileCount : dword;
	framesVertically : boolean; // whether to read multiframe image vertically first, or horizontally

// Slots for loaded, resized graphics. Sparse list, not sorted, uses a linked list to keep most recently used on top.
// Call GetGraphicObject to fetch correctly-sized graphics; call ReleaseGfx when done using. (Released graphics remain
// in memory for a while for fast re-getting.)
type TGraphicObject = class
	public
		bitmap : array of byte;
		graphicName : UTF8string; // must be in uppercase, or dynamic generation string
		sourceFile : TGraphicFile; // where on disk this graphic is originally found; can be NIL for runtime-generated
		framePtr : array of pointer; // pointers in bitmap[] for first byte of each frame
		origOfsX, origOfsY : longint; // 32k values, origOfsP / origResP
		origSizeX, origFrameHeight : dword; // 32k values, origFrameHeightP / origResP
		sizeXP, sizeYP, frameHeightP : dword;
		memFormat : EBitmapFormat;
		cacheState : byte; // 0: empty, 1..254: released but valid, 255: valid and in use
	private
		objIndex : dword;
		newerLink, olderLink : dword;

	private
		procedure FreeGraphic;
		procedure MarkAsRecentlyUsed;
		procedure ResizeFrom(origsizeobject : TGraphicObject); // destination object's sizeXYP must be set
		procedure FillGradient;
		function LoadFromFile : boolean;
	end;
var graphicObjects : array of TGraphicObject;
	// LoadThisGraphic increases MemUsage, FreeGraphic decreases, UnloadDats resets to 0.
	asman_gfxMemLimit : dword; // soft limit for graphics cache, in bytes
	asman_gfxMemUsage : dword; // current memory used

// Individual songs of any format. Must be sorted by songName. 0 is invalid.
// MIDI files are always kept fully in memory. Other files may be loaded from disk on demand.
type TMusicObject = class
	public
		songName : string31;
		data : array of byte;
		fileFormat : (MOF_MID);
	end;
var musicObjects : array of TMusicObject;
	musicObjectCount : dword;

var asman_logger : procedure (const logtext : UTF8string);

procedure ValidateIdentifier(const src : UTF8string; ofs, len : dword);
function GetGraphicFile(const name : UTF8string) : dword;
procedure AddOrReplaceGraphicFile(const graphic : TGraphicFile);
function GetOrAddGraphicFile(const name : UTF8string) : TGraphicFile;
function CopyOrCreateGraphicFile(const name : UTF8string) : TGraphicFile;
function GetDynamicGraphicObject(const loadcmd : UTF8string; rqsizex, rqsizey : dword) : TGraphicObject;
procedure GetLoadtimeDynamicSize(const loadcmd : UTF8string; out sizex, sizey : dword; pxsize : boolean = FALSE);
function GetGraphicObject(const name : UTF8string; rqsizexp, rqsizeyp : dword) : TGraphicObject;
procedure ReleaseGfx;
function GetScript(const labelname : UTF8string) : dword;
procedure SortScripts(onlylast : boolean);
procedure ImportScripts(poku : pointer; blocksize : dword);
function FindLanguageIndex(langdesc : UTF8string) : dword;
function AddGlobalString(const s : UTF8string) : dword;
function FindGlobalString(const canonical : UTF8string) : dword;
procedure ImportStringTable(const importbuffy : pointer; bytelen : dword; importcanonicals : boolean);
function ExportStringTable(var exportbuffy : pointer) : dword;
function GetSong(const name : UTF8string) : dword;
function GetDat(const datname : UTF8string) : dword;
procedure SortDats;
function EnumerateDats(const scanpath : UTF8string) : boolean;
procedure UnloadDats(initall : boolean);
function LoadInf(const filepath : UTF8string; baseresonly : boolean) : dword;
function LoadFileTree(const dirpath : UTF8string) : dword;

// ------------------------------------------------------------------

implementation

const asman_signature : dword = $CACABAAB;

var asman_gObjWriteSlot : dword; // rotating index to graphicObjects[]

procedure ValidateIdentifier(const src : UTF8string; ofs, len : dword);
// To easily check if a resource name has stupid characters. Throws an exception if anything looks wrong.
// UTF8strings are 1-based, so ofs should be 1 or more. The offset and length are in bytes, not characters.
begin
	// safety
	if ofs > src.Length then raise Exception.Create('validation start ofs is beyond end of string');
	if ofs = 0 then raise Exception.Create('validation start ofs must be > 0');
	if ofs + len > src.Length + 1 then len := src.Length - ofs + 1;

	while len <> 0 do begin
		if NOT (char(src[ofs]) in ['0'..'9', 'A'..'Z', 'a'..'z', '_', '-', '!', '&', '~', '[', ']'])
			then raise Exception.Create('illegal character in identifier, only alphanumerics and _-!&~[] allowed');

		inc(ofs);
		dec(len);
	end;
end;

// ------------------------------------------------------------------

procedure TGraphicFile.ResetMeta;
begin
	sequence := NIL;
	srcDat := NIL;
	origResXP := 0; origResYP := 0;
	origSizeXP := 0; origSizeYP := 0;
	origOfsXP := 0; origOfsYP := 0;
	origFrameHeightP := 0;
	frameCount := 1;
end;

constructor TGraphicFile.Create(const name : string31);
// The new graphic file name must be in uppercase.
begin
	Assert(name = upcase(name));
	graphicName := name;
	srcFilePath := '';
	ResetMeta;
end;

function TGraphicFile.Clone : TGraphicFile;
begin
	result := TGraphicFile.Create(graphicName);
	result.origResXP := origResXP;
	result.origResYP := origResYP;
	result.origSizeXP := origSizeXP;
	result.origSizeYP := origSizeYP;
	result.origOfsXP := origOfsXP;
	result.origOfsYP := origOfsYP;
	result.origFrameHeightP := origFrameHeightP;
	result.frameCount := frameCount;
	result.seqLen := seqLen;
	result.sequence := NIL;
	if length(sequence) <> 0 then begin
		setlength(result.sequence, length(sequence));
		move(sequence[0], result.sequence[0], 4 * length(sequence));
	end;
	result.srcFilePath := srcFilePath;
	result.srcFileReadOfs := srcFileReadOfs;
	result.srcFileReadSize := srcFileReadSize;
end;

destructor TGraphicFile.Destroy;
var o : TGraphicObject;
begin
	// Any objects pointing at this file also become invalid.
	for o in graphicObjects do
		if (o.cacheState <> 0) and (o.sourceFile = self) then o.FreeGraphic;
	inherited;
end;

function GetGraphicFile(const name : UTF8string) : dword;
// Returns the index of a graphicFile with the given name. If not found, returns 0.
// Can't return the object itself since LoadDat may need to overwrite the index.
// The search is case-sensitive, but all graphicFiles names shall be in uppercase and no more than 31 characters.
// graphicFiles[] is a mostly constant list that must be sorted before calling.
var comp, min, max : longint;
begin
	if (name = '') or (graphicFileCount < 2) then begin
		result := 0; exit;
	end;
	// binary search
	min := 1; max := graphicFileCount - 1;
	repeat
		result := (min + max) shr 1;
		comp := CompStr(
			@name[1], @graphicFiles[result].graphicName[1],
			name.Length, length(graphicFiles[result].graphicName));
		if comp = 0 then exit;
		if comp > 0 then
			min := result + 1
		else
			max := result - 1;
	until min > max;
	result := 0;
end;

procedure SortGraphicFiles(onlylast : boolean);
var i, j : dword;
	tempgra : TGraphicFile;
begin
	if graphicFileCount < 3 then exit; // don't touch index 0, must have at least 2 others to sort
	// Insertion sort.
	i := 2;
	if onlylast then i := graphicFileCount - 1;
	while i < graphicFileCount do begin
		tempgra := graphicFiles[i];
		j := i - 1;
		while (j <> 0) and (graphicFiles[j].graphicName > tempgra.graphicName) do begin
			graphicFiles[j + 1] := graphicFiles[j];
			dec(j);
		end;
		graphicFiles[j + 1] := tempgra;
		inc(i);
	end;
end;

procedure AddGraphicFile(const graphic : TGraphicFile);
// Inserts the given graphic in graphicFiles[]. It must not exist there yet.
begin
	if graphicFileCount >= dword(length(graphicFiles)) then
		setlength(graphicFiles, graphicFileCount + graphicFileCount shr 2 + 8);
	graphicFiles[graphicFileCount] := graphic;
	inc(graphicFileCount);
	SortGraphicFiles(TRUE);
end;

procedure AddOrReplaceGraphicFile(const graphic : TGraphicFile);
// Inserts the given graphic in graphicFiles[], overwriting any existing graphic by the same name.
var i : dword;
begin
	i := GetGraphicFile(graphic.graphicName);
	if i = 0 then
		AddGraphicFile(graphic)
	else begin
		if graphicFiles[i] <> NIL then graphicFiles[i].Destroy;
		graphicFiles[i] := graphic;
	end;
end;

function GetOrAddGraphicFile(const name : UTF8string) : TGraphicFile;
// Returns the given graphic from graphicFiles[]. Adds it first if it doesn't exist yet. Name must be in uppercase.
var i : dword;
begin
	i := GetGraphicFile(name);
	if i <> 0 then
		result := graphicFiles[i]
	else begin
		result := TGraphicFile.Create(name);
		AddGraphicFile(result);
	end;
end;

function CopyOrCreateGraphicFile(const name : UTF8string) : TGraphicFile;
// Returns a value copy of the given item from graphicFiles[] if it exists. Else creates a fresh object without adding
// to graphicFiles[]. Name must be in uppercase.
var i : dword;
begin
	i := GetGraphicFile(name);
	if i = 0 then
		result := TGraphicFile.Create(name)
	else
		result := graphicFiles[i].Clone;
end;

procedure RearrangeFrames(var image : mcg_bitmap; const metadata : TGraphicFile);
// Rearranges an animation frame grid so that frames are stacked vertically. All frames must be the exact same
// dimensions.
var p : array of byte = NIL;
	destp : pointer;
	framebytewidth : dword;
	x, y, srchframes, srcvframes : dword;

	procedure _Move;
	var srcp : pointer;
		scanline : dword;
	begin
		with metadata do begin
			srcp := @image.bitmap[0] + y * origFrameHeightP * image.stride + x * framebytewidth;
			for scanline := origFrameHeightP - 1 downto 0 do begin
				move(srcp^, destp^, framebytewidth);
				inc(srcp, image.stride);
				inc(destp, framebytewidth);
			end;
		end;
	end;

begin
	Assert((image <> NIL) and (metadata <> NIL));
	with metadata do with image do begin
		Assert((origFrameHeightP <> 0) and (origFrameHeightP < origSizeYP));

		framebytewidth := (origSizeXP * bitDepth + 7) shr 3;

		srchframes := sizeXP div origSizeXP;
		srcvframes := sizeYP div origFrameHeightP;
		setlength(p, framebytewidth * origSizeYP);
		destp := @p[0];
		if framesVertically then
			for x := 0 to srchframes - 1 do for y := 0 to srcvframes - 1 do _Move
		else
			for y := 0 to srcvframes - 1 do for x := 0 to srchframes - 1 do _Move;
		setlength(bitmap, 0); bitmap := p; p := NIL;
		sizeXP := origSizeXP;
		sizeYP := origSizeYP;
		stride := (sizeXP * bitDepth + 7) shr 3;
	end;
end;

{$include mcsassm-gob.pas}
{$include mcsassm-string.pas}
{$include mcsassm-script.pas}
{$include mcsassm-music.pas}
{$include mcsassm-datfile.pas}

function LoadScript(const filepath : UTF8string) : dword;
// Tries to compile the given file as a script. Returns number of errors.
var scriptname : UTF8string;
	loader : TFileLoader = NIL;
	errorlist : pointer;
	i : dword;

	procedure _Error(const msg : ShortString);
	begin
		asman_logger(strcat('[!] LoadScript: %: %', [filepath, msg]));
		inc(result);
	end;

begin
	result := 0;
	// The uppercased file base name is used to refer to the script.
	scriptname := ExtractFileName(filepath);
	asman_logger('loading ' + scriptname);
	setlength(scriptname, scriptname.Length - 4); // remove .txt
	if scriptname.Length > 31 then begin
		_Error('file name too long (max 31 bytes)');
		setlength(scriptname, 31);
	end;

	try
		loader := TFileLoader.Open(filepath, -1, 1); // whole file +1 extra byte
	except on e : Exception do begin
		_Error(e.Message);
		exit;
	end; end;

	// Add a linebreak at the end in case there wasn't one before.
	byte((loader.endp - 1)^) := $A;

	// Pack the script into our script and string arrays.
	// (Scripts are saved label by label, and each script file may contain multiple labels, so CompileScript needs to
	// decide which array indexes everything goes to.)
	if asman_CompileScript = NIL then begin
		_Error('asman_CompileScript must be assigned first');
		exit;
	end;
	errorlist := asman_CompileScript(scriptname, loader.readp, loader.endp);
	loader.Destroy; loader := NIL;

	// If we got a null pointer, all went well.
	if errorlist = NIL then exit;

	// Otherwise we got a buffy of error ministrings.
	i := 0;
	while byte((errorlist + i)^) <> 0 do begin
		_Error(ShortString((errorlist + i)^));
		inc(i, byte((errorlist + i)^) + byte(1));
	end;
	freemem(errorlist); errorlist := NIL;
end;

function LoadPng(const filepath : UTF8string) : dword;
var graphicname : UTF8string;
	loader : TFileLoader = NIL;
	workimu : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	i : dword;

	procedure _Error(const msg : ShortString);
	begin
		asman_logger(strcat('[!] LoadPng: %: %', [filepath, msg]));
		inc(result);
	end;

begin
	result := 0;

	// The uppercased file base name is used to refer to the graphic.
	asman_logger('loading ' + ExtractFileName(filepath));
	graphicname := upcase(ExtractFileNameWithoutExt(filepath));
	if graphicname.Length > 31 then begin
		_Error('file name too long (max 31 bytes)');
		setlength(graphicname, 31);
	end;

	try
		loader := TFileLoader.Open(filepath, 32); // only care about the header right now, don't load whole file
	except on e : Exception do begin
		_Error(e.Message);
		exit;
	end; end;

	try
		if loader.buffySize < 21 then begin
			// Absolute minimum recognisable file header is a single IHDR chunk without an appended CRC:
			// 4 + 4 + IHDR content (13) = 21 bytes
			_Error('file too tiny');
			exit;
		end;

		metadata := GetOrAddGraphicFile(graphicname);
		Assert(metadata.frameCount <> 0);

		// Parse the PNG header only.
		try
			workimu := mcg_bitmap.FromPNG(loader.readp, loader.buffySize, [MCG_FLAG_HEADERONLY]);
		except
			on e : Exception do begin
				_Error(e.Message);
				exit;
			end;
		end;

		with metadata do begin
			// Capture the image size from the parsed header.
			origSizeXP := workimu.sizeXP;
			origSizeYP := workimu.sizeYP;
			// Calculate the frame sizes, and if frame rearranging is needed, adjust graphic size to match.
			// The graphic isn't actually loaded at this point, so the rearrange is only done on load.
			if origFrameHeightP = 0 then
				origFrameHeightP := origSizeYP div frameCount
			else begin
				if origFrameHeightp > origSizeYP then begin
					asman_logger('[!] frameheight > image height');
					origFrameHeightP := origSizeYP;
				end
				else if (frameCount <= 1) and (origFrameHeightP < origSizeYP) then
					frameCount := origSizeYP div origFrameHeightP;

				if (frameCount > 1) and (origFrameHeightP * frameCount > origSizeYP) then begin
					//framesv := origSizeYP div origFrameHeightP;
					//framesh := ((frameCount - 1) div framesv) + 1;
					i := origSizeYP div origFrameHeightP;
					origSizeYP := frameCount * origFrameHeightP;
					origSizeXP := origSizeXP div (((frameCount - 1) div i) + 1);
				end;
			end;
			// Remember which file contains this PNG data.
			srcDat := NIL;
			srcFilePath := filepath;

			i := 0;
			if dword(loader.readp^) = BEtoN(dword($89504E47)) then i := 8; // skippable PNG sig
			srcFileReadOfs := i;
			srcFileReadSize := loader.fullFileSize - i;
		end;

	finally
		if loader <> NIL then loader.Destroy;
		if workimu <> NIL then workimu.Destroy;
	end;
end;

function LoadFile(const filepath : UTF8string; filetype : byte) : dword;
var loader : TFileLoader = NIL;

	procedure _Error(const msg : ShortString);
	begin
		asman_logger(strcat('[!] LoadFile: %: %', [filepath, msg]));
		result := 1;
	end;

begin
	result := 0;
	asman_logger('loading ' + ExtractFileName(filepath));
	try
		loader := TFileLoader.Open(filepath);
	except on e : Exception do begin
		_Error(e.Message);
		exit;
	end; end;

	try
		case filetype of
			0: ImportStringTable(loader.readp, loader.buffySize, TRUE);
			1: inc(result, ImportMidiFile(loader.readp, loader.buffySize, upcase(ExtractFileNameWithoutExt(filepath))));
		end;
	except
		on e : Exception do _Error(e.Message);
	end;

	if loader <> NIL then begin loader.Destroy; loader := NIL; end;
end;

function LoadInf(const filepath : UTF8string; baseresonly : boolean) : dword;
// Reads all global and file-specific metadata from the given text file. Returns number of errors, throws an exception
// for criticals.
// This text metadata describes any package-level game information like project name and description or default game
// resolution. A few extra parameters for images can also be set that aren't included in the graphic file itself.
var metadata : TGraphicFile = NIL;
	f : TIniFileLoader;
	bunch : TStringBunch;
	key, value : UTF8string;
	i, j, ofs : dword;
	l : longint;

	procedure _Error(const msg : ShortString);
	begin
		asman_logger(strcat('[!] LoadInf: % (% %): %', [filepath, key, value, msg]));
		inc(result);
	end;

begin
	result := 0;
	asman_logger('loading ' + filepath);
	f := TIniFileLoader.Open(filepath);

	try
		with availableDats[0] do while TRUE do begin
			key := f.ReadKeyValue(value);
			if key = '' then break;
			if value.Length > 255 then setlength(value, 255);

			// Default resolution for all images in this dat.
			if key = 'baseres' then begin
				ofs := 1;
				baseResX := CutNumberFromString(value, ofs);
				baseResY := CutNumberFromString(value, ofs);
				if (baseResX < 2) or (baseResY < 2) then begin
					_Error(strcat('base resolution %x% is invalid!', [baseResX, baseResY]));
					baseResX := 640;
					baseResY := 400;
				end;
				if baseresonly then break;
				continue;
			end;
			if baseresonly then continue;

			case key of
				'banner':
				begin
					bannerName := upcase(value);
					asman_logger('Requested banner image: ' + bannerName);
				end;

				// Human-readable description string for the dat being built.
				'desc', 'description', 'displayname':
				displayName := value;

				'framesvertically':
				framesVertically := TRUE;

				'language':
				defaultScriptLanguage := FindOrAddLanguage(value);

				'mainoverride':
				mainOverride := value;

				'parent':
				begin
					parentName := value;
					asman_logger('Parent dat: ' + parentName);
				end;

				'version':
				gameVersion := value;

				'image':
				begin
					if value = '' then begin
						_Error('empty filename');
						continue;
					end;
					if value.Length > 31 then _Error('filename too long (max 31 bytes)');
					// Get the graphic file for saving this metadata, if one exists.
					metadata := GetOrAddGraphicFile(upcase(value));
				end;

				'source':
				if metadata = NIL then
					_Error('no image specified yet')
				else begin
					if value = '' then begin
						_Error('empty filename');
						continue;
					end;
					if NOT (value[1] in ['/','\']) then value := PathCombine([ExtractFilePath(filepath), value]);
					bunch := FindFiles_caseless(value, FALSE, TRUE);
					if bunch = NIL then begin
						_Error('file not found');
						continue;
					end;
					LoadPng(bunch[0]);
				end;

				// Base location for an image's anchor relative to its containing viewport.
				'ofs', 'loc':
				if metadata = NIL then
					_Error('no image specified yet')
				else begin
					ofs := 1;
					metadata.origOfsXP := CutNumberFromString(value, ofs);
					metadata.origOfsYP := CutNumberFromString(value, ofs);
				end;

				'res':
				if metadata = NIL then
					_Error('no image specified yet')
				else begin
					ofs := 1;
					metadata.origResXP := CutNumberFromString(value, ofs);
					metadata.origResYP := CutNumberFromString(value, ofs);
				end;

				'framecount':
				if metadata = NIL then
					_Error('no image specified yet')
				else begin
					metadata.frameCount := abs(valx(value));
					if metadata.frameCount = 0 then metadata.frameCount := 1;
				end;

				'frameheight':
				if metadata = NIL then
					_Error('no image specified yet')
				else
					metadata.origFrameHeightP := abs(valx(value));

				// Animation frame sequence, pairs of FRAME:DELAY.
				'sequence':
				if metadata = NIL then
					_Error('no image specified yet')
				else begin
					ofs := 1;
					while ofs <= value.Length do with metadata do begin
						// sequence index
						i := abs(CutNumberFromString(value, ofs));
						if i > 8191 then i := 8191;
						if i >= dword(length(sequence)) then
							setlength(sequence, (i + 16) and $FFF0);
						if i >= seqLen then seqLen := i + 1;
						// frame number to display or jump command
						j := 0;
						while (ofs <= value.Length) and (NOT (value[ofs] in ['0'..'9','j','r','v'])) do inc(ofs);
						if value[ofs] = 'j' then begin
							j := $80000000; // jump
							inc(ofs);
						end;
						while (ofs <= value.Length) and (NOT (value[ofs] in ['0'..'9','j','r','v'])) do inc(ofs);
						case value[ofs] of
							'r': j := j or $40000000; // random
							'v': j := j or $20000000; // variable
						end;
						while (ofs <= value.Length) and (NOT (value[ofs] in ['0'..'9','+','-'])) do inc(ofs);
						if (j = $80000000) and (value[ofs] in ['+','-']) then
							l := i + dword(CutNumberFromString(value, ofs)) // relative jump
						else
							l := abs(CutNumberFromString(value, ofs)); // non-relative
						j := j or dword((l and $1FFF) shl 16);
						if j and $80000000 = 0 then begin
							// delay value
							while (ofs <= value.Length) and (NOT (value[ofs] in ['0'..'9','s','r','v'])) do inc(ofs);
							case value[ofs] of
								// stop at this frame
								's': j := j or $FFFF;
								// random delay, top bits 10
								'r': j := j or $8000 or dword(CutNumberFromString(value, ofs) and $3FFF);
								// delay from variable, top bits 11
								'v': j := j or $C000 or dword(CutNumberFromString(value, ofs) and $3FFF);
								// absolute delay value, top bit 0
								else j := j or dword(CutNumberFromString(value, ofs) and $7FFF);
							end;
						end;
						// Save the packed action:frame:delay into sequence[].
						sequence[i] := j;
						// Skip ahead until the next entry on the line.
						while (ofs <= value.Length) and (NOT (value[ofs] in ['0'..'9'])) do inc(ofs);
					end;
				end;

				else _Error('unknown command: ' + key + ' ' + value);
			end;
		end;
	finally
		f.Destroy;
	end;
end;

function LoadFileTree(const dirpath : UTF8string) : dword;
// Loads all files recursively in and under the given directory. Returns number of errors, throws an exception for
// criticals.
// Rules for loading:
// - Loads all .inf files in the directory first, lowercased, in byte order.
// - On case-sensitive systems, the .inf suffix must be in lowercase or it won't be recognised.
// - Then loads all other files in the directory, not ordered.
// - Files are evaluated by suffix; unrecognised file types are ignored.
// - New resources overwrite existing resources by the same name.
// - Then recurses into all subdirectories, lowercased, in byte order.
// - Does not recurse into "override" subdirectory; this is only loaded if named in the initial LoadFileTree call.
var filelist : TStringBunch;
	fileitem : UTF8string;
begin
	result := 0;
	asman_logger('scanning ' + dirpath);

	// First read metadata .inf files, sorted.
	filelist := FindFiles_caseless(PathCombine([dirpath, '*.inf']));
	if filelist.Length <> 0 then begin
		filelist.Sort;
		for fileitem in filelist do inc(result, LoadInf(fileitem, FALSE));
	end;

	// Find and try to process all files that could be recognisable data.
	filelist := FindFiles_caseless(PathCombine([dirpath, '*']));
	for fileitem in filelist do begin
		// Identify and forward file types by suffix.
		case lowercase(ExtractFileExt(fileitem)) of
			'.inf': ;
			'.txt': inc(result, LoadScript(fileitem));
			'.png': inc(result, LoadPng(fileitem));
			'.tsv': inc(result, LoadFile(fileitem, 0));
			'.mid': inc(result, LoadFile(fileitem, 1));
			else asman_logger('ignore ' + ExtractFileName(fileitem));
		end;
	end;

	// Recurse into subdirectories.
	filelist := FindFiles_caseless(PathCombine([dirpath, '*']), TRUE);
	if filelist.Length <> 0 then begin
		filelist.Sort;
		for fileitem in filelist do
			if ExtractFileName(fileitem) <> 'override' then
				inc(result, LoadFileTree(fileitem))
			else
				asman_logger('ignore ' + fileitem);
	end;

	// Remove excess space from resource arrays.
	if dword(length(scriptObjects)) <> scriptObjectCount then setlength(scriptObjects, scriptObjectCount);
	if dword(length(graphicFiles)) <> graphicFileCount then setlength(graphicFiles, graphicFileCount);
	if dword(length(availableDats)) <> availableDatCount then setlength(availableDats, availableDatCount);
end;

// ------------------------------------------------------------------

initialization
	asman_gfxMemLimit := 16777216; asman_gfxMemUsage := 0;
	asman_logger := @NullLogger;
	UnloadDats(TRUE); // inits asset arrays, sets up null items

finalization
	asman_logger('mcsassm is exiting');
	UnloadDats(FALSE);
end.

