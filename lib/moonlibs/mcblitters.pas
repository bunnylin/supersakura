unit mcblitters;

{                                                                           }
{ Mooncore blitter functions unit                                           }
{ CC0, 2019-2023 : Kirinn Bunnylin / MoonCore                               }
{ Use freely for anything ever!                                             }
{                                                                           }

interface

uses mcgloder;

{$note todo: more useful blitstruct names, remove clip items, to be included as Clip params}
type blitstruct = record
		srcp, destp : pointer;
		srcofs, destofs : dword;
		destofsxp, destofsyp : longint;
		copywidth, copyrows : dword;
		srcskipwidth, destskipwidth : dword;
		clipx1p, clipy1p : longint;
		clipx2p, clipy2p : longint;
		clipviewport : dword;
	end;
	pblitstruct = ^blitstruct;

procedure DrawRGBX32(clipdata : pblitstruct);
procedure DrawRGBA32(clipdata : pblitstruct);
procedure DrawRGBA32Alpha(clipdata : pblitstruct; amul : byte);
procedure DrawRGBA32Additive(clipdata : pblitstruct);
procedure DrawRGBA32Difference(clipdata : pblitstruct);
procedure DrawRGBA32Hardlight(clipdata : pblitstruct);
procedure DrawRGBA32Multiply(clipdata : pblitstruct);
procedure DrawRGBA32Wipe(clipdata : pblitstruct; completion : dword; wipein : boolean);
procedure DrawSolid(clipdata : pblitstruct; fillcolor : RGBAquad);
procedure DrawFlat(clipdata : pblitstruct; fillcolor : RGBAquad);

implementation

// These blitters use simple alpha blending, without gamma correction.
// All buffers must come with correctly pre-multiplied alpha.
// I'd be happy to use gamma-correct alpha blending, but 8 bits per channel is not sufficient for that; the premul must
// be done in linear color space, but the result saved in sRGB. This imprecision appears to invariably lead to a fuzzy
// blend calculation, which overflows its color space. To make that work, you'd have to add slow saturation checks in
// the blitters. :(
// Or, increase the bit depth and/or switch to floats, both of which would require rewriting all bitmap handling and
// gobble more memory. Am I just doing it wrong?

procedure DrawRGBX32(clipdata : pblitstruct);
// Copies data from a source bitmap into a destination buffer, assuming the alpha channel is fully opaque.
// The source data must be in BGRA byte order.
var destbufbytewidth : dword;
begin
	with clipdata^ do begin
		// Special case for full-width graphics, faster direct blit.
		if (srcskipwidth or destskipwidth) = 0 then
			move(srcp^, destp^, copyrows * copywidth * 4)
		else begin
			// Normal clipped blit.
			copywidth := copywidth * 4;
			inc(srcskipwidth, copywidth);
			destbufbytewidth := copywidth + destskipwidth;
			while copyrows <> 0 do begin
				move(srcp^, destp^, copywidth);
				inc(srcp, srcskipwidth);
				inc(destp, destbufbytewidth);
				dec(copyrows);
			end;
		end;
	end;
end;

procedure DrawRGBA32(clipdata : pblitstruct);
// Copies data from a source bitmap into a destination buffer, applying alpha blending normally.
// The source data must be in BGRA byte order, with components pre-multiplied by alpha.
var x : dword;
	alpha : byte;
begin
	with clipdata^ do begin
		while copyrows <> 0 do begin
			x := copywidth;
			while x <> 0 do begin
				alpha := byte((srcp + 3)^);
				case alpha of
					// Shortcut for the majority of pixels, totally transparent or opaque.
					0:
					begin
						inc(srcp, 4); inc(destp, 4);
					end;
					$FF:
					begin
						dword(destp^) := dword(srcp^);
						inc(srcp, 4); inc(destp, 4);
					end;
					else begin
						alpha := alpha xor $FF;
						byte(destp^) := alphamixtab[byte(destp^), alpha] + byte(srcp^);
						inc(srcp); inc(destp);
						byte(destp^) := alphamixtab[byte(destp^), alpha] + byte(srcp^);
						inc(srcp); inc(destp);
						byte(destp^) := alphamixtab[byte(destp^), alpha] + byte(srcp^);
						inc(srcp); inc(destp);
						inc(byte(destp^), alphamixtab[byte(destp^) xor $FF, byte(srcp^)]);
						inc(srcp); inc(destp);
					end;
				end;
				dec(x);
			end;
			inc(srcp, srcskipwidth);
			inc(destp, destskipwidth);

			dec(copyrows);
		end;
	end;
end;

procedure DrawRGBA32Alpha(clipdata : pblitstruct; amul : byte);
// Copies 32-bit RGBA data from a source bitmap into a destination buffer, multiplying the source bitmap with an alpha
// value along the way. 255 alpha is fully visible, 0 alpha is fully invisible.
var x : dword;
	alpha : byte;
begin
	with clipdata^ do begin
		while copyrows <> 0 do begin
			x := copywidth;
			while x <> 0 do begin
				alpha := alphamixtab[byte((srcp + 3)^), amul];
				// Shortcut for totally transparent pixels.
				if alpha = 0 then begin
					inc(srcp, 4); inc(destp, 4);
				end
				else begin
					alpha := alpha xor $FF;
					byte(destp^) := alphamixtab[byte(destp^), alpha] + alphamixtab[byte(srcp^), amul];
					inc(srcp); inc(destp);
					byte(destp^) := alphamixtab[byte(destp^), alpha] + alphamixtab[byte(srcp^), amul];
					inc(srcp); inc(destp);
					byte(destp^) := alphamixtab[byte(destp^), alpha] + alphamixtab[byte(srcp^), amul];
					inc(srcp, 2); inc(destp);
					inc(byte(destp^), alphamixtab[byte(destp^) xor $FF, alpha xor $FF]);
					inc(destp);
				end;

				dec(x);
			end;
			inc(srcp, srcskipwidth);
			inc(destp, destskipwidth);

			dec(copyrows);
		end;
	end;
end;

procedure DrawRGBA32Additive(clipdata : pblitstruct);
// Copies data from a source bitmap into a destination buffer with "additive" blending.
// The bitmap data must be in BGRA byte order, with components pre-multiplied by alpha.
var a, x : dword;
begin
	with clipdata^ do begin
		while copyrows <> 0 do begin
			x := copywidth shl 2;
			while x <> 0 do begin
				a := byte(destp^) + byte(srcp^);
				if a > 255 then a := 255;
				byte(destp^) := a;
				inc(srcp); inc(destp);
				dec(x);
			end;
			inc(srcp, srcskipwidth);
			inc(destp, destskipwidth);

			dec(copyrows);
		end;
	end;
end;

procedure DrawRGBA32Difference(clipdata : pblitstruct);
// Copies data from a source bitmap into a destination buffer with "difference" blending.
// The bitmap data must be in BGRA byte order, with components pre-multiplied by alpha.
var x : dword;
	alpha : byte;
begin
	with clipdata^ do begin
		while copyrows <> 0 do begin
			x := copywidth;
			while x <> 0 do begin
				alpha := byte((destp + 3)^);
				if alpha = 255 then begin
					byte(destp^) := abs(byte(destp^) - byte(srcp^));
					inc(srcp); inc(destp);
					byte(destp^) := abs(byte(destp^) - byte(srcp^));
					inc(srcp); inc(destp);
					byte(destp^) := abs(byte(destp^) - byte(srcp^));
					inc(srcp, 2); inc(destp, 2);
				end
				else if (alpha = 0) or (byte((srcp + 3)^) = 0) then begin
					inc(destp, 4); inc(srcp, 4);
				end
				else begin
					// To respect alpha pre-multiplication, the source colors must be multiplied by the destination's
					// alpha, so they're equally scaled. Then take a difference normally. The source alpha is not used
					// here, but it already did its part in the source premultiplication.
					byte(destp^) := abs(byte(destp^) - alphamixtab[byte(srcp^), alpha]);
					inc(srcp); inc(destp);
					byte(destp^) := abs(byte(destp^) - alphamixtab[byte(srcp^), alpha]);
					inc(srcp); inc(destp);
					byte(destp^) := abs(byte(destp^) - alphamixtab[byte(srcp^), alpha]);
					inc(srcp, 2); inc(destp, 2);
				end;

				dec(x);
			end;
			inc(srcp, srcskipwidth);
			inc(destp, destskipwidth);

			dec(copyrows);
		end;
	end;
end;

procedure DrawRGBA32Hardlight(clipdata : pblitstruct);
// Copies data from a source bitmap into a destination buffer with "hard light" blending.
// The source data must be in BGRA byte order, with components pre-multiplied by alpha.
var x : dword;
	alphasrc, halfalphasrcinv, alphadest, halfalphadest : byte;
	i, srcbyte, destbyte, newalpha : byte;
begin
	with clipdata^ do begin
		while copyrows <> 0 do begin
			x := copywidth;
			while x <> 0 do begin
				alphadest := byte((destp + 3)^);

				if alphadest = 0 then begin
					dword(destp^) := dword(srcp^);
					inc(destp, 4); inc(srcp, 4);
				end
				else begin
					alphasrc := byte((srcp + 3)^);
					halfalphasrcinv := (alphasrc xor $FF) shr 1;
					halfalphadest := alphadest shr 1;
					newalpha := alphadest + alphamixtab[alphadest xor $FF, alphasrc];

					// In discussions of blending algorithms, it's usually "B over A".
					// Here it's "src over dest".
					// Hard light uses light src values to lighten dest, but dark src values darken it. The traditional
					// way is:
					//   result = (if b < 0.5 then (a * b) else (1 - (1 - a) * (1 - b))) * 2
					//
					// Since we're using bytes, (1 - a) becomes (255 - a), or (a xor 255).
					// This allows turning the equation branchless.
					//
					// Also, the alpha pre-multiplication means that the current src alpha should be considered the
					// color channels' full output value. For the equation to work, src must be scaled so that its
					// middle value (half of the pixel's alpha) now sits at 0.5; dest must have its alpha
					// premultiplication reversed.

					srcbyte := byte(srcp^) + halfalphasrcinv;
					i := byte(SarShortint(shortint(srcbyte), 7)); // extend top bit to byte
					destbyte := (byte(destp^) * 255 + halfalphadest) div alphadest;
					byte(destp^) := alphamixtab[((srcbyte xor i) * (destbyte xor i) shr 7) xor i, newalpha];
					inc(srcp); inc(destp);

					srcbyte := byte(srcp^) + halfalphasrcinv;
					i := byte(SarShortint(shortint(srcbyte), 7));
					destbyte := (byte(destp^) * 255 + halfalphadest) div alphadest;
					byte(destp^) := alphamixtab[((srcbyte xor i) * (destbyte xor i) shr 7) xor i, newalpha];
					inc(srcp); inc(destp);

					srcbyte := byte(srcp^) + halfalphasrcinv;
					i := byte(SarShortint(shortint(srcbyte), 7));
					destbyte := (byte(destp^) * 255 + halfalphadest) div alphadest;
					byte(destp^) := alphamixtab[((srcbyte xor i) * (destbyte xor i) shr 7) xor i, newalpha];
					inc(srcp, 2); inc(destp);

					byte(destp^) := newalpha; inc(destp);
				end;

				dec(x);
			end;
			inc(srcp, srcskipwidth);
			inc(destp, destskipwidth);

			dec(copyrows);
		end;
	end;
end;

procedure DrawRGBA32Multiply(clipdata : pblitstruct);
// Copies data from a source bitmap into a destination buffer with "multiply" blending.
// The bitmap data must be in BGRA byte order, with components pre-multiplied by alpha.
var x : dword;
begin
	with clipdata^ do begin
		while copyrows <> 0 do begin
			x := copywidth;
			while x <> 0 do begin
				if (byte((destp + 3)^) = 0) or (byte((srcp + 3)^) = 0) then begin
					dword(destp^) := 0;
					inc(destp, 4); inc(srcp, 4);
				end
				else begin
					byte(destp^) := alphamixtab[byte(srcp^), byte(destp^)];
					inc(srcp); inc(destp);
					byte(destp^) := alphamixtab[byte(srcp^), byte(destp^)];
					inc(srcp); inc(destp);
					byte(destp^) := alphamixtab[byte(srcp^), byte(destp^)];
					inc(srcp); inc(destp);
					byte(destp^) := alphamixtab[byte(srcp^), byte(destp^)];
					inc(srcp); inc(destp);
				end;

				dec(x);
			end;
			inc(srcp, srcskipwidth);
			inc(destp, destskipwidth);

			dec(copyrows);
		end;
	end;
end;

procedure DrawRGBA32Wipe(clipdata : pblitstruct; completion : dword; wipein : boolean);
// Copies data from a source bitmap into a destination buffer, applying a sideways wipe effect up to 32k completion
// fraction. If wipein is TRUE, the image will appear wiped in from the left; else the image appears being wiped away
// toward the right.
var atable : array of byte = NIL;
	i, x : dword;
	edgewidthp, leadedgep, trailedgep : dword;
	alpha, alpha2 : byte;
begin
	with clipdata^ do begin
		// Pre-calculate an alpha table... This shows what alpha each pixel column needs to be multiplied with.
		setlength(atable, copywidth + 1);
		// If wiping out, invert completion.
		if wipein = FALSE then completion := 32768 - completion;
		// Soft edge width = WinSizeX / 16
		edgewidthp := (copywidth + (destskipwidth shr 2)) shr 4;
		// Lead edge position.
		leadedgep := ((copywidth + edgewidthp) * completion) shr 15;
		// Trailing edge position.
		trailedgep := 0;
		if leadedgep > edgewidthp then trailedgep := leadedgep - edgewidthp;

		if wipein then begin
			// Fill max alpha before trailing edge.
			if trailedgep <> 0 then fillbyte(atable[copywidth - trailedgep], trailedgep, 255);
			// Fill min alpha after leading edge.
			if leadedgep < copywidth then fillbyte(atable[0], copywidth - leadedgep, 0);
			// Prepare the trailing edge alpha value.
			alpha := 255;
			if leadedgep < edgewidthp then alpha := (255 * leadedgep) div edgewidthp;
			// Prepare the leading edge alpha value.
			alpha2 := 0;
			if leadedgep > copywidth then alpha2 := 255 * (leadedgep - copywidth) div edgewidthp;
		end
		else begin
			// Fill min alpha before trailing edge.
			if trailedgep <> 0 then fillbyte(atable[copywidth - trailedgep], trailedgep, 0);
			// Fill max alpha after leading edge.
			if leadedgep < copywidth then fillbyte(atable[0], copywidth - leadedgep, 255);
			// Prepare the trailing edge alpha value.
			alpha := 0;
			if leadedgep < edgewidthp then alpha := ((255 * leadedgep) div edgewidthp) xor $FF;
			// Prepare the leading edge alpha value.
			alpha2 := 255;
			if leadedgep > copywidth then alpha2 := (255 * (leadedgep - copywidth) div edgewidthp) xor $FF;
		end;
		// Clip lead edge.
		if leadedgep > copywidth then leadedgep := copywidth;
		// Calculate the soft edge linear alpha gradient.
		i := leadedgep - trailedgep;
		for x := i downto 0 do
			atable[copywidth - x - trailedgep] := (alpha2 * x + alpha * dword(i - x)) div i;

		while copyrows <> 0 do begin
			x := copywidth;
			while x <> 0 do begin
				alpha := alphamixtab[byte((srcp + 3)^), atable[x]];
				// Shortcut for totally transparent pixels.
				if alpha = 0 then begin
					inc(srcp, 4); inc(destp, 4);
				end
				else begin
					alpha := alpha xor $FF;
					byte(destp^) := alphamixtab[byte(destp^), alpha] + alphamixtab[byte(srcp^), atable[x]];
					inc(srcp); inc(destp);
					byte(destp^) := alphamixtab[byte(destp^), alpha] + alphamixtab[byte(srcp^), atable[x]];
					inc(srcp); inc(destp);
					byte(destp^) := alphamixtab[byte(destp^), alpha] + alphamixtab[byte(srcp^), atable[x]];
					inc(srcp, 2); inc(destp);
					inc(byte(destp^), alphamixtab[byte(destp^) xor $FF, alpha xor $FF]);
					inc(destp);
				end;

				dec(x);
			end;
			inc(srcp, srcskipwidth);
			inc(destp, destskipwidth);

			dec(copyrows);
		end;
	end;
end;

procedure DrawSolid(clipdata : pblitstruct; fillcolor : RGBAquad);
// Fills a destination buffer with the alpha profile of a source bitmap, using fillcolor.
// Useful for full-screen blackouts, or making a character sprite flash.
// Call ClipRGB first to generate the blitstruct.
var x : dword;
	alpha : byte;
begin
	if fillcolor.a = 0 then exit;

	with clipdata^ do begin
		while copyrows <> 0 do begin
			x := copywidth;
			while x <> 0 do begin
				alpha := alphamixtab[byte((srcp + 3)^), fillcolor.a];
				// Shortcut for totally transparent pixels.
				if alpha = 0 then begin
					inc(srcp, 4); inc(destp, 4);
				end
				else begin
					// Partial alpha mix, using the precalculated alphamixtable.
					// Source is not premultiplied by its own alpha, must be further multiplied by amul.
					// Destination must be multiplied by the inverse of (alpha * amul), then the two are summed.
					byte(destp^) := byte(alphamixtab[alpha xor $FF, byte(destp^)] + alphamixtab[fillcolor.b, alpha]);
					inc(srcp); inc(destp);
					byte(destp^) := byte(alphamixtab[alpha xor $FF, byte(destp^)] + alphamixtab[fillcolor.g, alpha]);
					inc(srcp); inc(destp);
					byte(destp^) := byte(alphamixtab[alpha xor $FF, byte(destp^)] + alphamixtab[fillcolor.r, alpha]);
					inc(srcp); inc(destp);
					byte(destp^) := byte(alphamixtab[alpha xor $FF, byte(destp^)] + alpha);
					inc(srcp); inc(destp);
				end;

				dec(x);
			end;
			inc(srcp, srcskipwidth);
			inc(destp, destskipwidth);

			dec(copyrows);
		end;
	end;
end;

procedure DrawFlat(clipdata : pblitstruct; fillcolor : RGBAquad);
// Blits a rectangle of flat fillcolor into the destination buffer.
// Call ClipRGB first to generate the blitstruct.
var x : dword;
	alpha, ainv, mulb, mulg, mulr : byte;
begin
	if fillcolor.a = 0 then exit;

	with clipdata^ do begin
		if fillcolor.a = $FF then begin
			x := copywidth * 4 + destskipwidth;
			while copyrows <> 0 do begin
				filldword(destp^, copywidth, dword(fillcolor));
				inc(destp, x);
				dec(copyrows);
			end;
			exit;
		end;

		alpha := fillcolor.a;
		ainv := alpha xor $FF;
		mulb := alphamixtab[fillcolor.b, alpha];
		mulg := alphamixtab[fillcolor.g, alpha];
		mulr := alphamixtab[fillcolor.r, alpha];
		while copyrows <> 0 do begin
			x := copywidth;
			while x <> 0 do begin
				// Partial alpha mix, using the precalculated alphamixtable.
				// Every destination pixel is multiplied by the inverse of fillcolor's alpha, then fillcolor multiplied
				// by its own alpha is added to the destination.
				byte(destp^) := byte(alphamixtab[ainv, byte(destp^)] + mulb); inc(destp);
				byte(destp^) := byte(alphamixtab[ainv, byte(destp^)] + mulg); inc(destp);
				byte(destp^) := byte(alphamixtab[ainv, byte(destp^)] + mulr); inc(destp);
				byte(destp^) := byte(alphamixtab[ainv, byte(destp^)] + alpha); inc(destp);

				dec(x);
			end;
			inc(destp, destskipwidth);

			dec(copyrows);
		end;
	end;
end;

initialization
finalization
end.

