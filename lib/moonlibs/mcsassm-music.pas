{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function GetSong(const name : UTF8string) : dword;
// Returns the index of a musicObject with the given name. If not found, returns 0.
// Can't return the object itself since LoadDat may need to overwrite the index.
// The search is case-sensitive, but all song names shall be in uppercase.
// The musicObjects[] array must be sorted before calling this.
var comp, min, max : longint;
begin
	if (name = '') or (musicObjectCount < 2) then begin
		result := 0; exit;
	end;
	// binary search
	min := 1; max := musicObjectCount - 1;
	repeat
		result := (min + max) shr 1;
		comp := CompStr(
			@name[1], @musicObjects[result].songName[1],
			name.Length, length(musicObjects[result].songName));
		if comp = 0 then exit;
		if comp > 0 then
			min := result + 1
		else
			max := result - 1;
	until min > max;
	result := 0;
end;

procedure SortSongs(onlylast : boolean);
var i, j : dword;
	tempmus : TMusicObject;
begin
	if musicObjectCount < 3 then exit; // don't touch index 0, must have at least 2 others to sort
	// Insertion sort.
	i := 2;
	if onlylast then i := musicObjectCount - 1;
	while i < musicObjectCount do begin
		tempmus := musicObjects[i];
		j := i - 1;
		while (j <> 0) and (musicObjects[j].songName > tempmus.songName) do begin
			musicObjects[j + 1] := musicObjects[j];
			dec(j);
		end;
		musicObjects[j + 1] := tempmus;
		inc(i);
	end;
end;

function ImportMidiFile(srcp : pointer; len : dword; name : UTF8string) : dword;
var mob : TMusicObject;
	i : dword;
begin
	result := 0;
	if name.Length > 31 then begin
		asman_logger('[!] midi name too long (max 31 bytes)');
		inc(result);
		setlength(name, 31);
	end;

	i := GetSong(name);
	if i <> 0 then
		mob := musicObjects[i]
	else begin
		mob := TMusicObject.Create;
		if musicObjectCount >= dword(length(musicObjects)) then
			setlength(musicObjects, musicObjectCount + musicObjectCount shr 1 + 20);
		musicObjects[musicObjectCount] := mob;
		inc(musicObjectCount);
	end;

	with mob do begin
		songName := name;
		fileFormat := MOF_MID;
		data := NIL;
		setlength(data, len);
		move(srcp^, data[0], len);
	end;

	if i = 0 then SortSongs(TRUE);
end;

