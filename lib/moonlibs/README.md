Moonlibs
========

This is a set of possibly useful Free Pascal units, mostly for the
benefit of my other projects.

##### minicrt

A small Windows/Linux CRT unit, much like FPC's own CRT unit but smaller.
Does UTF-8 output in consoles.

##### sjisutf8

A unit for converting single Shift-JIS code points to UTF-8 characters.

##### mcmidiwriter

Helper for combining individual MIDI commands into a full MIDI file.

##### mcscriptwriter

Helper for generating a Sakurascript file.

##### mcgloder

Mooncore Graphics Loader. Reads and writes PNG/BMP/DIB files, and has helper
functions for format conversion, bitdepth conversion, gamma adjustment, etc.

##### mcscaler

Mooncore Graphics Scaler. Helper functions for resizing mcgloder bitmaps.

##### mcsassm

Mooncore Super Asset Manager. Reads bundled data files, making resources
smoothly available to a master program.

##### mcvarmon

Mooncore Varmon variable management system. Allows smooth access to named
variables for script engines, with support for multiple simultaneous
languages for string variables.

##### mccommon

Various trivial functions used by my other projects.
