unit mcbtree;

{                                         }
{ Binary tree traverser                   }
{ CC0, 2024 :: Kirinn Bunnylin / MoonCore }
{ Use freely for anything ever!           }
{                                         }

{$mode objfpc}

interface

uses mcfileio, mccommon;

// If left = 0, return right; else read bit; 0 = add left to read pointer; 1 = add right.
type TBinTree = array of word;

// Binary tree specification:
// TBinTree must be an even length, so each node is a pair of words, "left" and "right".
// A pair represents a terminating leaf if "left" == 0; the leaf's return value is "right".
// Otherwise the pair is a normal node. Input bit 0 takes the "left" branch, 1 takes the "right" branch.
// The branch values are the relative byte distance from the start byte of this node to the start byte of the next.
// (p += node_offset is maybe faster than p := @tree[0] + node_offset)

type TBitReader = function() : boolean of object; // should point to one of the BitRead functions from mcfileio
function ReadBinaryCode(const src : TFileLoader; const tree : TBinTree; BitReader : TBitReader) : dword;
{$ifdef dumpbtree}
procedure DumpTree(const tree : TBinTree; node : pointer = NIL; prefix : string = '');
{$endif}

implementation

const b = sizeof(word); // outcome size

function ReadBinaryCode(const src : TFileLoader; const tree : TBinTree; BitReader : TBitReader) : dword;
var p, treetop : pointer;
begin
	{$push}{$R-}
	result := 0;
	p := @tree[0];
	treetop := p + length(tree) * b;
	while TRUE do begin
		if p >= treetop then begin
			//writeln('btree oob @ $' + strhex(src.ofs));
			break;
		end;

		// If "left" value is 0, this is the final node - return the "right" value.
		if word(p^) = 0 then begin
			result := word((p + b)^);
			break;
		end;

		// Branch out to next node!
		// Tried making this branchless, but that performs worse, since the condition turns into an arithmetic
		// dependency that blocks all subsequent actions (sans reordering), equivalent to a 100% branch misprediction.
		// Also, since binary trees have the most common value as the shortest code, the input bits are not entirely
		// chaotic, so a modern branch predictor can on average make decent guesses and get a slight speedup.
		if BitReader() then
			inc(p, word((p + b)^))
		else
			inc(p, word(p^));
	end;
	if src.readp > src.endp then begin
		//writeln('btree out of inputs @ $' + strhex(src.ofs));
		result := 0;
	end;
	{$pop}
end;

{$ifdef dumpbtree}
procedure DumpTree(const tree : TBinTree; node : pointer = NIL; prefix : string = '');
begin
	if node = NIL then node := @tree[0];
	if word(node^) = 0 then
		writeln(prefix, ': ', word((node + 2)^))
	else begin
		DumpTree(tree, node + word(node^), prefix + '0');
		DumpTree(tree, node + word((node + 2)^), prefix + '1');
	end;
end;
{$endif}

initialization
finalization
end.

