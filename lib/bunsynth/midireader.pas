unit midireader;

// Helper for consuming standard midi binary data. This is part of BunnySynth.

{$mode objfpc}
{$ifdef DEBUG}{$ASSERTIONS on}{$endif}
{$codepage UTF8}
{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

{$unitpath ../moonlibs}

interface

type TMidiReader = class
	track : array of record
		startp, readp, endp : pointer;
		nextAtTick : dword;
		runstatus : byte; // the previous midi event, carried over as running status
	end;
	queue : array of dword; // ordered list, track[] indexes in order of lowest nextAtTick first
	currentTick, liveTracks : dword;
	ticksPerQuarterNote : word;
	eof : boolean;

	procedure SortQueue(onlyfirst : boolean);
	function ReadDeltaTime(trackindex : dword) : dword;
	procedure SeekStart;
	procedure Seek(tick : dword);
	function ReadNextEvent(out cmd : byte) : pointer;
	constructor ImportMidiFile(srcp, endp : pointer); // sets up track readers; the buffer must remain in scope
end;

// ------------------------------------------------------------------

implementation

uses sysutils, mccommon;

procedure TMidiReader.SortQueue(onlyfirst : boolean);
var i, j, l, temp : dword;
begin
	l := length(queue);
	if l < 2 then exit;
	// Backward insertion sort.
	i := l - 1;
	if onlyfirst then i := 1;
	while i <> 0 do begin
		dec(i);
		temp := queue[i];
		j := i + 1;
		while (j <> l) and
		((track[queue[j]].nextAtTick < track[temp].nextAtTick) or
		((track[queue[j]].nextAtTick = track[temp].nextAtTick) and (temp > queue[j])))
		do begin
			queue[j - 1] := queue[j];
			inc(j);
		end;
		queue[j - 1] := temp;
	end;
end;

function TMidiReader.ReadDeltaTime(trackindex : dword) : dword;
// Returns the next delta time at the track's readp^, advances readp. If the track has ended, returns -1.
// Delta times use the bottom 7 bits of each byte, end when top bit not set. Ex: $82 04 -> $00000094
// Max delta time allowed by spec is FF FF FF 7F -> 0FFF_FFFF.
var b : byte;
begin
	result := 0;
	with track[trackindex] do begin
		while readp < endp do begin
			b := byte(readp^); inc(readp);
			inc(result, byte(b and $7F));
			if b and $80 = 0 then break;
			result := result shl 7;
		end;
		if readp + 4 > endp then result := high(result);
	end;
end;

procedure TMidiReader.SeekStart;
var i : dword;
begin
	for i := high(track) downto 0 do with track[i] do begin
		readp := startp;
		nextAtTick := ReadDeltaTime(i);
		runstatus := 80;
	end;
	SortQueue(FALSE);
	liveTracks := length(track);
	currentTick := 0;
	eof := FALSE;
end;

procedure TMidiReader.Seek(tick : dword);
var i, d : dword;
begin
	liveTracks := length(track);
	eof := FALSE;
	for i := high(track) downto 0 do with track[i] do begin
		readp := startp;
		nextAtTick := ReadDeltaTime(i);
		runstatus := 80;
		while nextAtTick < tick do begin
			if byte(readp^) >= $80 then begin runstatus := byte(readp^); inc(readp); end;
			case runstatus of
				$F0: while (readp < endp) and (byte(readp^) and $80 = 0) do inc(readp); // sysex
				$F1, $F3: inc(readp); // time code or song select, 1 data byte
				$F2: inc(readp, 2); // song pos pointer, 2 data bytes
				$F4..$FE: ; // undefined or realtime messages, no data
				$FF: begin // meta
					inc(readp);
					inc(readp, byte(readp^) + 1);
				end;

				else // normal midi event, 1 or 2 data bytes
				if runstatus in [$C0..$DF] then inc(readp) else inc(readp, 2);
			end;
			d := ReadDeltaTime(i);
			if d = high(nextAtTick) then begin
				dec(liveTracks);
				nextAtTick := high(nextAtTick);
			end
			else inc(nextAtTick, d);
		end;
	end;
	SortQueue(FALSE);
	currentTick := tick;
end;

function TMidiReader.ReadNextEvent(out cmd : byte) : pointer;
// Finds the next midi event on any track that should play next. Returns a pointer to the first data byte of this
// event, if any. Also places the event command in cmd. Returns NIL for song end.
// The absolute tick of this event relative to song start is this instance's currentTick.
var d : dword;
begin
	if liveTracks = 0 then begin cmd := 0; result := NIL; eof := TRUE; exit; end;

	with track[queue[0]] do begin
		currentTick := nextAtTick;
		if byte(readp^) >= $80 then begin runstatus := byte(readp^); inc(readp); end;
		result := readp;
		cmd := runstatus;
		case runstatus of
			$F0: begin // sysex, read until F7 terminator (or anything with top bit set)
				while (readp < endp) and (byte(readp^) and $80 = 0) do inc(readp);
				if byte(readp^) = $F7 then inc(readp);
			end;
			$F1, $F3: inc(readp); // time code or song select, 1 data byte
			$F2: inc(readp, 2); // song pos pointer, 2 data bytes
			$F4..$FE: ; // undefined or realtime messages, no data
			$FF: begin // meta
				inc(readp);
				inc(readp, byte(readp^) + 1);
			end;

			else // normal midi event, 1 or 2 data bytes
			if runstatus in [$C0..$DF] then inc(readp) else inc(readp, 2);
		end;
		d := ReadDeltaTime(queue[0]);
		if d = high(nextAtTick) then begin
			dec(liveTracks);
			eof := (liveTracks = 0);
			nextAtTick := high(nextAtTick);
		end
		else inc(nextAtTick, d);
	end;
	SortQueue(TRUE);
end;

constructor TMidiReader.ImportMidiFile(srcp, endp : pointer);
var hlen : dword;
	t, format, numtracks : word;
begin
	track := NIL;
	queue := NIL;
	if srcp >= endp then raise Exception.Create('Empty midi file');
	Assert((srcp <> NIL) and (endp <> NIL));
	if endp - srcp < 22 then raise Exception.Create('File too tiny');
	if BEtoN(dword(srcp^)) = $52494646 then raise Exception.Create('RIFF midi not supported yet');

	if (BEtoN(dword(srcp^)) <> $4D546864) then raise Exception.Create('missing MThd sig');
	inc(srcp, 4);
	hlen := BEtoN(dword(srcp^)); inc(srcp, 4);
	if hlen <> 6 then raise Exception.Create('bad header size');
	format := BEtoN(word(srcp^)); inc(srcp, 2);
	if format > 2 then raise Exception.Create('bad format word');
	if format = 2 then raise Exception.Create('async track format not supported yet');
	numtracks := BEtoN(word(srcp^)); inc(srcp, 2);
	if (numtracks = 0) or (numtracks > 999) then raise Exception.Create('unexpected numtracks: ' + strdec(numtracks));
	setlength(track, numtracks);
	setlength(queue, numtracks);
	ticksPerQuarterNote := BEtoN(word(srcp^)); inc(srcp, 2);
	if ticksPerQuarterNote and $8000 <> 0 then raise Exception.Create('smpte tempo not supported yet');

	for t := 0 to numtracks - 1 do begin
		if srcp + 8 > endp then raise Exception.Create('out of bounds reading track ' + strdec(t));
		if BEtoN(dword(srcp^)) <> $4D54726B then raise Exception.Create('missing MTrk sig');
		inc(srcp, 4);
		track[t].endp := srcp + BEtoN(dword(srcp^)) + 4;
		if track[t].endp > endp then raise Exception.Create('out of bounds reading track ' + strdec(t));
		inc(srcp, 4);
		track[t].startp := srcp;
		srcp := track[t].endp;
		queue[t] := t;
	end;

	SeekStart;
end;

end.

