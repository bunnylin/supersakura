unit bunnysound;
{                                                                           }
{ BunnySound and BunnySynth, a software sound synthesizer system            }
{ Copyright 2024 :: Kirinn Bunnylin / MoonCore                              }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{ ------------------------------------------------------------------------- }
{                                                                           }
{ Targets FPC 3.2.2 for Linux/Win, 32/64-bit.                               }
{                                                                           }
{ Compilation dependencies:                                                 }
{ - Pascal translation of SDL2 headers (22-May-2022) (ZLib/MPL license)     }
{   https://github.com/PascalGameDevelopment/SDL2-for-Pascal                }
{ - Pascal translation of FluidSynth headers (ZLib license)                 }
{   https://gitlab.com/bunnylin/pasfluidsynth                               }
{ - Various moonlibs (ZLib license)                                         }
{   https://gitlab.com/bunnylin/moonlibs                                    }
{                                                                           }
{ Runtime dependencies:                                                     }
{ - Simple DirectMedia Library SDL2 (2.0.22)                                }
{   https://libsdl.org/                                                     }
{ - Optional soft dependency: FluidSynth (2.3.3)                            }
{   https://www.fluidsynth.org/                                             }
{                                                                           }

{$mode objfpc}
{$ifdef DEBUG}{$ASSERTIONS on}{$endif}
{$codepage UTF8}
{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 5024 off} // unused parameters
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

{$unitpath ../moonlibs}
{$unitpath ../pasfluidsynth}
{$unitpath ../sdl2}

interface

uses
{$ifdef UNIX}cthreads,{$endif}
{$ifdef WINDOWS}mmsystem,{$endif}
sysutils, mccommon, pasfluidsynth, midireader;

type TBSoundServer = class
	public
	type ETargetSink = (TS_NONE, TS_BUNNY, {$ifdef WINDOWS}TS_WINDOWS,{$endif} TS_FLUID);
	var volume : record
		overall : byte; // caller-tweakable volume level, default 255
	end;
	alwaysLoop : boolean;
	fluidDriverName : PChar; // set to one of possible values for running platform, or empty for default

	private
	fluidSynth : TFluidSynth;
	fluidInitialGain : single;
	targetSink : ETargetSink;
	currentMidiFile, nextMidiFile : TMidiReader;
	ticksPerBeat, midiTempo : dword;
	playbackTick, loopStartTick : dword;
	fadeOutTick : dword; // the projected tick at which fade out is complete
	fadeFromTick : longint; // the past tick at which fade out started
	isPaused : dword;
	playing : boolean; // must only be changed by StartPlayback/StopPlayback

	{$ifdef WINDOWS}
	streamH : HMIDISTRM;
	windowsDevice : longint;
	streamSizeTicks : dword;
	streamBuffy : array[0..32767] of dword;
	streamHeader : array[0..1] of MIDIHDR;
	streamSelect : byte;
	resetGain : boolean;
	procedure ResetWindowsMidi;
	procedure StopWindowsPlayback;
	procedure StartWindowsPlayback;
	procedure InitWindowsMidi;
	procedure StopWindowsMidi;
	{$endif}

	procedure StopFluidPlayback;
	procedure StartFluidPlayback;
	procedure InitFluid;
	procedure StopFluid;
	procedure GetNextMidi;

	public
	procedure SetSink(newsink : ETargetSink);
	function AddFluidSoundFont(const filepath : PChar) : longint;
	procedure Reset;
	procedure StopPlayback;
	procedure StartPlayback;
	procedure PausePlayback(pause : boolean);
	procedure FadeOut(fadeoutms : dword);
	procedure ApplyTempo;
	procedure LoadNewMidi(srcp, endp : pointer);
	procedure PlayNewMidi(srcp, endp : pointer; fadeoutms : dword);

	constructor Create;
	destructor Destroy; override;
end;

// ------------------------------------------------------------------

implementation

uses minimutex;

var lut_volume : array[0..1023] of single;
	//callbackMutex : TRTLCriticalSection;
	callbackMutex : TMiniMutex;

{$include fluidmidi.pas}
{$ifdef WINDOWS}{$include windowsmidi.pas}{$endif}

procedure TBSoundServer.GetNextMidi;
begin
	if nextMidiFile <> NIL then begin
		if currentMidiFile <> NIL then currentMidiFile.Destroy;
		currentMidiFile := nextMidiFile; nextMidiFile := NIL;
	end;
	Reset;
end;

procedure TBSoundServer.SetSink(newsink : ETargetSink);
begin
	case targetSink of
		TS_FLUID: StopFluid;
		{$ifdef WINDOWS}
		TS_WINDOWS: StopWindowsMidi;
		{$endif}
		TS_BUNNY: ;
	end;

	case newsink of
		TS_FLUID: InitFluid;
		{$ifdef WINDOWS}
		TS_WINDOWS: InitWindowsMidi;
		{$endif}
		TS_BUNNY: ;
	end;

	targetSink := newsink;
end;

procedure TBSoundServer.Reset;
// Called by GetNextMidi, which may be called by the player callback, which is already in a critical section.
// Do not call from anywhere else unless the player is stopped.
begin
	midiTempo := 500000;
	ticksPerBeat := 48;
	if currentMidiFile <> NIL then begin
		currentMidiFile.SeekStart;
		ticksPerBeat := currentMidiFile.ticksPerQuarterNote;
		loopStartTick := 0;
	end;
	case targetSink of
		TS_FLUID: with fluidSynth do fluid_synth_system_reset(synth);
		{$ifdef WINDOWS}
		TS_WINDOWS: ResetWindowsMidi;
		{$endif}
	end;
end;

procedure TBSoundServer.StopPlayback;
begin
	MiniMutexLock(callbackMutex);
	playing := FALSE;
	MiniMutexRelease(callbackMutex);
	case targetSink of
		TS_FLUID: StopFluidPlayback;
		{$ifdef WINDOWS}
		TS_WINDOWS: StopWindowsPlayback;
		{$endif}
	end;
end;

procedure TBSoundServer.StartPlayback;
begin
	if playing then exit;

	MiniMutexLock(callbackMutex);
	fadeOutTick := 0;
	playbackTick := 0;
	GetNextMidi;
	playing := TRUE;
	MiniMutexRelease(callbackMutex);

	if isPaused = 0 then case targetSink of
		TS_FLUID: StartFluidPlayback;
		{$ifdef WINDOWS}
		TS_WINDOWS: StartWindowsPlayback;
		{$endif}
	end;
end;

procedure TBSoundServer.PausePlayback(pause : boolean);
// Pauses sound triggering, freezes the target sink if possible or sets its master volume to 0.
// If pause is FALSE, resumes playback where left off.
begin
	if (targetSink = TS_NONE) or (NOT playing) or ((NOT pause) and (isPaused = 0)) then exit;
	if pause then
		inc(isPaused)
	else begin
		dec(isPaused);
		if isPaused <> 0 then exit;
	end;

	if pause then begin
		// Pause playback.
		case targetSink of
			TS_FLUID: StopFluidPlayback;
			{$ifdef WINDOWS}
			TS_WINDOWS: StopWindowsPlayback;
			{$endif}
		end;
	end
	else begin
		// Resume playback.
		case targetSink of
			TS_FLUID: StartFluidPlayback;
			{$ifdef WINDOWS}
			TS_WINDOWS: StartWindowsPlayback;
			{$endif}
		end;
	end;
end;

procedure TBSoundServer.FadeOut(fadeoutms : dword);
// Consider using Master Volume SysEx: F0 7F id 04 01 LSB MSB F7
begin
	if (fadeoutms = 0) or (targetSink = TS_NONE) or (midiTempo = 0) then
		StopPlayback
	else if (playing) and (currentMidiFile <> NIL) and (fadeOutTick = 0) then begin
		MiniMutexLock(callbackMutex);
		fadeFromTick := playbackTick;
		fadeOutTick := playbackTick + fadeoutms * ticksPerBeat * 1000 div midiTempo + 1;
		MiniMutexRelease(callbackMutex);
	end;
end;

procedure TBSoundServer.ApplyTempo;
// Tells the target synthesizer to use a new speed. TicksPerBeat and midiTempo must be set before calling this.
// Quarternote == beat. BPM == quarternotes per minute.
// Beats per microsecond = 1 / tempo; quarternotes per second = 1_000_000 / tempo.
// Ticks per second = ticksPerQuarternote * 1_000_000 / tempo.
//
// Midi files have a ticks per quarternote in their header (default 48), and a microseconds per quarternote
// tempo (default 500_000). Fluidsynth's player can use the midi tempo almost as is, except you have to explicitly
// apply the correct "division" or ticksPerQuarterNote to the tempo, because it's in the midi file header and we never
// pass the file itself to Fluidsynth.
begin
	case targetSink of
		TS_FLUID:
		with fluidSynth do fluid_player_set_tempo(
			player, TFluidSynth.FLUID_PLAYER_TEMPO_EXTERNAL_MIDI, (midiTempo * 48) div ticksPerBeat);
		{$ifdef WINDOWS}
		TS_WINDOWS: ;
		{$endif}
	end;
end;

procedure TBSoundServer.LoadNewMidi(srcp, endp : pointer);
begin
	Assert(endp > srcp);
	MiniMutexLock(callbackMutex);
	if nextMidiFile <> NIL then nextMidiFile.Destroy;
	nextMidiFile := TMidiReader.ImportMidiFile(srcp, endp);
	if currentMidiFile = NIL then begin currentMidiFile := nextMidiFile; nextMidiFile := NIL; end;
	MiniMutexRelease(callbackMutex);
end;

procedure TBSoundServer.PlayNewMidi(srcp, endp : pointer; fadeoutms : dword);
begin
	LoadNewMidi(srcp, endp);
	FadeOut(fadeoutms);
	StartPlayback;
end;

constructor TBSoundServer.Create;
begin
	targetSink := TS_NONE;
	volume.overall := 255;
	fluidSynth := NIL;
	fadeOutTick := 0;
	currentMidiFile := NIL;
	nextMidiFile := NIL;
	playing := FALSE;
	alwaysLoop := FALSE;
	isPaused := 0;
	{$ifdef WINDOWS}
	streamH := 0;
	windowsDevice := 0; // 0 = default, -1 = midi mapper
	streamSizeTicks := 1;
	streamSelect := 0;
	resetGain := TRUE;
	{$endif}
end;

destructor TBSoundserver.Destroy;
begin
	if fluidSynth <> NIL then begin
		StopFluid;
		fluidSynth.Destroy; fluidSynth := NIL;
	end;
	{$ifdef WINDOWS}
	if streamH <> 0 then StopWindowsMidi;
	{$endif}
	if nextMidiFile <> NIL then begin nextMidiFile.Destroy; nextMidiFile := NIL; end;
	if currentMidiFile <> NIL then begin currentMidiFile.Destroy; currentMidiFile := NIL; end;
	inherited;
end;

procedure DoInits;
var i : dword;
begin
	// Precalculate a MIDI volume lookup table. lut_volume[linear_value] = squared_fraction
	for i := high(lut_volume) downto 0 do lut_volume[i] := sqr(i / high(lut_volume));
	callbackMutex := 0;
end;

// ------------------------------------------------------------------

initialization
	DoInits;
finalization
end.

