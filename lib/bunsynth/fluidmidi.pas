// FluidSynth's sequencer is unusable, which means have to use their file player or take over all timing and poke
// events directly into the synth at precise intervals. Their player is quite limiting if asked to just play a whole
// file, while independent direct event input is inefficient due to rapid timer interrupts.
// I'm hoping their player implementation is driven by audio driver callbacks and not timer interrupts, so we can set
// the player in motion with a silent fake midi, then use their tick callback to poke in events every tick.
const fakemidi : RawByteString = 'MThd'#0#0#0#6 + #0#0 + #0#1 + #0#48 + 'MTrk'#0#0#0#7 + #$FF#$FF#$FF#$7F + #$FF#$2F#0;

function FluidTickCallback(data : pointer; tick : longint) : longint;
var eventp : ^byte;
	cmd : byte;
	loopnow : boolean = FALSE;

	procedure _HandleMarker(txt : string);
	begin
		with TBSoundServer(data) do begin
			//writeln('marker ' + txt + ' at tick ', playbackTick);
			case lowercase(txt) of
				'loopstart': loopStartTick := playbackTick;
				'loopend': loopnow := TRUE;
			end;
		end;
	end;

begin
	result := 0;
	MiniMutexLock(callbackMutex);
	try
		with TBSoundServer(data) do with fluidSynth do begin
			if (NOT playing) or (currentMidiFile = NIL) then exit;

			if playbackTick < fadeOutTick then begin
				Assert(longint(playbackTick) >= fadeFromTick, strcat('playbacktick %, fadefrom %, fadeout at %', [playbackTick, fadeFromTick, fadeOutTick]));
				Assert(1023 * (fadeOutTick - playbackTick) div (fadeOutTick - fadeFromTick) <= 1023, strcat('!!playbacktick %, fadefrom %, fadeout at %', [playbackTick, fadeFromTick, fadeOutTick]));
				Assert(1023 * (fadeOutTick - playbackTick) div (fadeOutTick - fadeFromTick) >= 0, strcat('!!!playbacktick %, fadefrom %, fadeout at %', [playbackTick, fadeFromTick, fadeOutTick]));
				fluid_synth_set_gain(synth, fluidInitialGain *
					lut_volume[1023 * (fadeOutTick - playbackTick) div (fadeOutTick - fadeFromTick)]
					);
			end;

			if (currentMidiFile.eof) or ((fadeOutTick <> 0) and (playbackTick >= fadeOutTick)) then begin
				fluid_synth_all_sounds_off(synth, -1);
				if nextMidiFile = NIL then begin
					fadeOutTick := 0;
					playing := FALSE;
					fluid_player_stop(player);
					exit;
				end;
				if playbackTick = fadeOutTick then begin inc(playbackTick); exit; end; // allow one frame to stop sounds
				GetNextMidi;
				fluid_player_seek(player, 0);
				playbackTick := 0;
				fadeOutTick := 0;
				fluid_synth_set_gain(synth, fluidInitialGain);
			end;

			with currentMidiFile do while track[queue[0]].nextAtTick <= playbackTick do begin
				eventp := currentMidiFile.ReadNextEvent(cmd);
				//if (cmd and $F0 = $90) then writeln(strcat('tick %: & &', [playbackTick, cmd, eventp^]));
				case cmd of
					$80..$8F: fluid_synth_noteoff(synth, cmd and $F, eventp^);
					$90..$9F: fluid_synth_noteon(synth, cmd and $F, eventp^, (eventp + 1)^);
					$A0..$AF: fluid_synth_key_pressure(synth, cmd and $F, eventp^, (eventp + 1)^);
					$B0..$BF: fluid_synth_cc(synth, cmd and $F, eventp^, (eventp + 1)^);
					$C0..$CF: fluid_synth_program_change(synth, cmd and $F, eventp^);
					$D0..$DF: fluid_synth_channel_pressure(synth, cmd and $F, eventp^);
					$E0..$EF: fluid_synth_pitch_bend(synth, cmd and $F, eventp^ + (eventp + 1)^ shl 7);
					$F0..$FE: ; // sysex, etc

					$FF: begin // meta
						cmd := eventp^; inc(eventp);
						case cmd of
							6: _HandleMarker(string(pointer(eventp)^));

							$51:
							if eventp^ = 3 then begin // valid new tempo, have to apply ticksPerBeat explicitly
								midiTempo := BEtoN(dword(pointer(eventp)^)) and $FFFFFF;
								fluid_player_set_tempo(
									player, TFluidSynth.FLUID_PLAYER_TEMPO_EXTERNAL_MIDI,
									(miditempo * 48) div ticksPerBeat);
							end;
						end;
					end;

				end;
			end;
			inc(playbackTick);

			// Jump back to loop start after everything at the loop end tick has been processed.
			if (loopnow) or ((alwaysLoop) and (currentMidiFile.eof)) then begin
				currentMidiFile.Seek(loopStartTick);
				if playbackTick < fadeOutTick then begin
					fadeFromTick := loopStartTick - (playbackTick - fadeFromTick);
					fadeOutTick := loopStartTick + (fadeOutTick - playbackTick);
				end;
				playbackTick := loopStartTick;
			end;

		end;
	finally
		MiniMutexRelease(callbackMutex);
	end;
end;

procedure TBSoundServer.InitFluid;
begin
	if fluidSynth = NIL then fluidSynth := TFluidSynth.Create;
	with fluidSynth do begin
		if NOT isLoaded then raise Exception.Create(status);
		//fluid_set_log_function(FLUID_DBG, fluid_default_log_function, NIL);
		settings := new_fluid_settings();
		if settings = NIL then raise Exception.Create('Failed to create fluid settings');
		synth := new_fluid_synth(settings);
		if synth = NIL then raise Exception.Create('Failed to create fluid synth');
		fluidInitialGain := fluid_synth_get_gain(synth);
		// Initing the audio driver begins wave output immediately, so we'll only do that when actually playing.
	end;
end;

procedure TBSoundServer.StopFluid;
begin
	if (fluidSynth = NIL) or (NOT fluidSynth.isLoaded) then exit;
	// Always stop and delete driver first.
	StopPlayback;
	with fluidSynth do begin
		if player <> NIL then begin delete_fluid_player(player); player := NIL; end;
		if synth <> NIL then begin delete_fluid_synth(synth); synth := NIL; end;
		if settings <> NIL then begin delete_fluid_settings(settings); settings := NIL; end;
	end;
end;

function TBSoundServer.AddFluidSoundFont(const filepath : PChar) : longint;
// Adds the given file as a Fluid sound font. Fluid must be selected as target sink.
// Returns the soundfont ID.
begin
	if targetSink <> TS_FLUID then raise Exception.Create('Select Fluid sink before adding soundfont');
	Assert(fluidSynth <> NIL);
	with fluidSynth do begin
		result := fluid_synth_sfload(synth, filepath, 1);
		if result < 0 then raise Exception.Create(fluid_synth_error(synth));
	end;
end;

procedure TBSoundServer.StopFluidPlayback;
begin
	with fluidSynth do begin
		if audioDriver <> NIL then begin delete_fluid_audio_driver(audioDriver); audioDriver := NIL; end;
		if player <> NIL then fluid_player_stop(player);
		if synth <> NIL then fluid_synth_all_sounds_off(synth, -1);
	end;
end;

procedure TBSoundServer.StartFluidPlayback;
begin
	with fluidSynth do begin
		// Have to recreate player every time due to fluidsynth 2.3.3, commit cced26aa2f.
		if player <> NIL then begin delete_fluid_player(player); player := NIL; end;
		player := new_fluid_player(synth);
		if fluid_player_set_tick_callback(player, @FluidTickCallback, self) < 0 then
			raise Exception.Create('Failed to set tick callback');
		if fluid_player_add_mem(player, @fakemidi[1], length(fakemidi)) < 0 then
			raise Exception.Create('Failed to set up fake midi');
		if fluid_player_seek(player, 0) < 0 then raise Exception.Create('Failed to seek');
		fluid_synth_set_gain(synth, fluidInitialGain);
		fluid_player_set_tempo(
			player, TFluidSynth.FLUID_PLAYER_TEMPO_EXTERNAL_MIDI, (midiTempo * 48) div ticksPerBeat);

		if audioDriver = NIL then begin
			if fluidDriverName <> '' then fluid_settings_setstr(settings, 'audio.driver', fluidDriverName);
			audioDriver := new_fluid_audio_driver(settings, synth);
			if audioDriver = NIL then raise Exception.Create('Failed to create fluid audio driver');
		end;
		if fluid_player_play(player) < 0 then raise Exception.Create('Failed to start playing');
	end;
end;

