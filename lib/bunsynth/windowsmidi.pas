// Playback strategy: because I suspect Windows uses timer events to run a midi stream, it's important to minimise the
// frequency of timer triggers by sending large stream chunks. However, to keep the sound rapidly responsive, the
// stream chunks must be as short as possible too. When the BunnySound user asks for a half-second fadeout, the fadeout
// must begin nearly immediately; within 200ms or so at most. Since the stream is double-buffered, each bufferful must
// be 100ms or less. To guarantee exact buffer durations, every buffer must contain a midi event at the buffer's
// calculated final tick; if there's no other such event, a silent metronome tick must be sent.
// Whenever a bufferful is done playing, Windows invokes the callback, which can queue up a new bufferful.
//
// For reference, helpful constants: https://www.rpi.edu/dept/cis/software/g77-mingw32/include/mmsystem.h

procedure WindowsTickCallback(devh : HMIDIOUT; msg : dword; dwInstance, dwParam1, dwParam2 : pointer); stdcall;
var eventp : ^byte;
	writep, endp : pointer;
	ticksleft, delta : dword;
	cmd : byte;
	loopnow : boolean = FALSE;
	res : MMRESULT;

	procedure _HandleMarker(txt : string);
	begin
		with TBSoundServer(dwInstance) do begin
			//writeln('marker ' + txt + ' at tick ', playbackTick);
			case lowercase(txt) of
				'loopstart': loopStartTick := playbackTick;
				'loopend': loopnow := TRUE;
			end;
		end;
	end;

	procedure _SetGain(gain : dword);
	// http://midi.teragonaudio.com/tech/midispec/mastrvol.htm
	begin
		dword(writep^) := delta; inc(writep, 4);
		dword(writep^) := 0; inc(writep, 4);
		dword(writep^) := MEVT_F_LONG or (MEVT_LONGMSG shl 24) or 8; inc(writep, 4);
		dword(writep^) := NtoLE(dword($047F7FF0)); inc(writep, 4);
		dword(writep^) := NtoLE(dword($F7000001 + ((gain and $3F80) shl 9) + ((gain and $7F) shl 8))); inc(writep, 4);
		delta := 0;
	end;

begin
	if msg <> MOM_DONE then exit;
	with TBSoundServer(dwInstance) do begin
		if (NOT playing) or (currentMidiFile = NIL) then exit;
		MiniMutexLock(callbackMutex);
		try try

			streamSelect := streamSelect xor 1;
			writep := streamHeader[streamSelect].lpData;

			endp := writep + 65400;

			delta := 0;
			if resetGain then begin
				_SetGain($3FFF);
				resetGain := FALSE;
			end;

			ticksleft := streamSizeTicks;
			while ticksleft <> 0 do begin
				if playbackTick < fadeOutTick then begin
					Assert(longint(playbackTick) >= fadeFromTick, strcat('playbacktick %, fadefrom %, fadeout at %', [playbackTick, fadeFromTick, fadeOutTick]));
					Assert(1023 * (fadeOutTick - playbackTick) div (fadeOutTick - fadeFromTick) <= 1023, strcat('!!playbacktick %, fadefrom %, fadeout at %', [playbackTick, fadeFromTick, fadeOutTick]));
					Assert(1023 * (fadeOutTick - playbackTick) div (fadeOutTick - fadeFromTick) >= 0, strcat('!!!playbacktick %, fadefrom %, fadeout at %', [playbackTick, fadeFromTick, fadeOutTick]));
					_SetGain(round($3FFF * lut_volume[1023 * (fadeOutTick - playbackTick) div (fadeOutTick - fadeFromTick)] ));
				end;

				if (currentMidiFile.eof) or ((fadeOutTick <> 0) and (playbackTick >= fadeOutTick)) then begin
					//win_synth_all_sounds_off(synth, -1);
					MidiStreamPause(devh);
					playing := FALSE;
					if nextMidiFile = NIL then begin
						fadeOutTick := 0;
						break;
					end;
					//if playbackTick = fadeOutTick then begin inc(playbackTick); break; end; // allow one bufferful to stop sounds
					GetNextMidi;
					playbackTick := 0;
					fadeOutTick := 0;
					_SetGain($3FFF);
					playing := TRUE;
					MidiStreamRestart(devh);
				end;

				with currentMidiFile do while (writep < endp) and (track[queue[0]].nextAtTick <= playbackTick) do begin
					dword(writep^) := delta;
					dword((writep + 4)^) := 0; // streamid

					eventp := currentMidiFile.ReadNextEvent(cmd);
					//if (cmd and $F0 = $90) then writeln(strcat('tick %: & &', [playbackTick, cmd, eventp^]));
					case cmd of
						$80..$EF:
						begin
							dword((writep + 8)^) := cmd + (eventp^ shl 8) + ((eventp + 1)^ shl 16) + MEVT_F_SHORT;
							inc(writep, 12);
							delta := 0;
						end;
						$F0..$FE: ; // sysex, etc

						$FF: begin // meta
							cmd := eventp^; inc(eventp);
							case cmd of
								6: _HandleMarker(string(pointer(eventp)^));

								$51:
								if eventp^ = 3 then begin
									midiTempo := BEtoN(dword(pointer(eventp)^)) and $FFFFFF;
									dword((writep + 8)^) := NtoLE(midiTempo) + MEVT_F_SHORT + (MEVT_TEMPO shl 24);
									inc(writep, 12);
									delta := 0;
									// Ticks per 100ms = ticksPerQuarternote * 100_000 / tempo
									streamSizeTicks := ticksPerBeat * 100000 div midiTempo;
									if streamSizeTicks = 0 then streamSizeTicks := 1;
								end;
							end;
						end;
					end;
				end;
				inc(playbackTick);
				inc(delta);

				// Jump back to loop start after everything at the loop end tick has been processed.
				if (loopnow) or ((alwaysLoop) and (currentMidiFile.eof)) then begin
					currentMidiFile.Seek(loopStartTick);
					if playbackTick < fadeOutTick then begin
						fadeFromTick := loopStartTick - (playbackTick - fadeFromTick);
						fadeOutTick := loopStartTick + (fadeOutTick - playbackTick);
					end;
					playbackTick := loopStartTick;
					loopnow := FALSE;
				end;

				dec(ticksleft);
			end;

			// Generate a metronome event (NOP) to ensure the buffer goes up to the right tick index.
			if delta <> 0 then begin
				dword(writep^) := delta; inc(writep, 4);
				dword(writep^) := 0; inc(writep, 4);
				dword(writep^) := MEVT_F_SHORT + (MEVT_NOP shl 24); inc(writep, 4);
			end;

			with streamHeader[streamSelect] do dwBytesRecorded := writep - lpData;
			res := MidiStreamOut(devh, @streamHeader[streamSelect], sizeof(MIDIHDR));
			if res <> MMSYSERR_NOERROR then writeln('midistreamout failed: ',res);

		except
			on e : Exception do begin
				writeln(e.Message + LineEnding + BacktraceStrFunc(ExceptAddr));
			end;
		end;
		finally
			MiniMutexRelease(callbackMutex);
		end;
	end;
end;

procedure TBSoundServer.ResetWindowsMidi;
var prop : MIDIPROPTIMEDIV;
begin
	MidiOutReset(streamH);
	resetGain := TRUE;
	prop.cbStruct := sizeof(prop);
	prop.dwTimeDiv := ticksPerBeat;
	MidiStreamProperty(streamH, @prop, MIDIPROP_SET or MIDIPROP_TIMEDIV);
	// Ticks per 100ms = ticksPerQuarternote * 100_000 / tempo
	streamSizeTicks := ticksPerBeat * 100000 div midiTempo;
	if streamSizeTicks = 0 then streamSizeTicks := 1;
end;

// Have to redefine this to feed a function pointer to the 4th and 5th arguments.
function MidiStreamOpen_(
	phandle : LPHMIDISTRM; puDeviceID : LPUINT; cMidi : dword; dwCallback, dwInstance : pointer; fdwOpen : dword)
	: MMRESULT;
	stdcall; external 'winmm.dll' name 'midiStreamOpen';

procedure TBSoundServer.InitWindowsMidi;
var res : MMRESULT;
begin
	res := MidiStreamOpen_(@streamH, @windowsDevice, 1, @WindowsTickCallback, self, CALLBACK_FUNCTION);
	if res <> MMSYSERR_NOERROR then begin
		streamH := 0;
		raise Exception.Create('MidiStreamOpen: error ' + strdec(res));
	end;
	//writeln('midi stream opened, handle ' + strdec(streamH));

	fillbyte(streamHeader[0], sizeof(streamHeader[0]), 0);
	fillbyte(streamHeader[1], sizeof(streamHeader[1]), 0);
	streamHeader[0].dwBufferLength := 65532;
	streamHeader[1].dwBufferLength := 65532;
	streamHeader[0].lpData := @streamBuffy[0];
	streamHeader[1].lpData := @streamBuffy[16384];
	res := MidiOutPrepareHeader(streamH, @streamHeader[0], sizeof(MIDIHDR));
	if res <> MMSYSERR_NOERROR then raise Exception.Create('failed to prep header: ' + strdec(res));
	res := MidiOutPrepareHeader(streamH, @streamHeader[1], sizeof(MIDIHDR));
	if res <> MMSYSERR_NOERROR then raise Exception.Create('failed to prep header: ' + strdec(res));
end;

procedure TBSoundServer.StopWindowsMidi;
begin
	if streamH = 0 then exit;
	StopPlayback;
	MiniMutexLock(callbackMutex);
	MidiStreamClose(streamH);
	MidiOutUnprepareHeader(streamH, @streamHeader[0], sizeof(streamHeader[0]));
	MidiOutUnprepareHeader(streamH, @streamHeader[1], sizeof(streamHeader[1]));
	streamH := 0;
	MiniMutexRelease(callbackMutex);
end;

procedure TBSoundServer.StopWindowsPlayback;
begin
	MidiStreamPause(streamH);
end;

procedure TBSoundServer.StartWindowsPlayback;
begin
	resetGain := TRUE;
	if playbackTick = 0 then begin
		// Set up two bufferfuls of midi data for output, unless resuming from paused.
		WindowsTickCallback(streamH, MOM_DONE, self, NIL, NIL);
		WindowsTickCallback(streamH, MOM_DONE, self, NIL, NIL);
	end;
	// Unpause playback!
	MidiStreamRestart(streamH);
end;

