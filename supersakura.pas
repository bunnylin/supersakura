program SuperSakura;

{                                                                           }
{ SuperSakura engine :: Copyright 2009-2025 :: Kirinn Bunnylin / Mooncore   }
{ https://supersakura.net/                                                  }
{ https://gitlab.com/bunnylin/supersakura                                   }
{                                                                           }
{ This program is free software: you can redistribute it and/or modify      }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ This program is distributed in the hope that it will be useful,           }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with this program.  If not, see <https://www.gnu.org/licenses/>.    }
{ ------------------------------------------------------------------------- }
{                                                                           }
{ Targets FPC 3.2.2 for Linux/Win, 32/64-bit.                               }
{                                                                           }
{ Compilation dependencies (included under "lib"):                          }
{ - Pascal translation of SDL2 headers (22-May-2022) (ZLib/MPL license)     }
{   https://github.com/PascalGameDevelopment/SDL2-for-Pascal                }
{ - BunnySynth (ZLib license)                                               }
{   https://gitlab.com/bunnylin/bunnysynth                                  }
{ - Pascal translation of FluidSynth headers (ZLib license)                 }
{   https://gitlab.com/bunnylin/pasfluidsynth                               }
{ - Various moonlibs (ZLib license)                                         }
{   https://gitlab.com/bunnylin/moonlibs                                    }
{                                                                           }
{ Runtime dependencies:                                                     }
{ - Simple DirectMedia Library SDL2 and SDL2_ttf (2.0.22+)                  }
{   https://libsdl.org/                                                     }
{ - Optional soft dependency: FluidSynth (2.2.7+)                           }
{   https://www.fluidsynth.org/                                             }
{                                                                           }

{$mode objfpc}
{$ifdef DEBUG}{$ASSERTIONS on}{$endif}
{$ifdef WINDOWS}{$apptype GUI}{$endif}
{$codepage UTF8}
{$iochecks off} // WARNING: if an IO action fails, all subsequent actions fail silently until IOresult is checked
{$scopedEnums on}
{$inline on}
{$librarypath lib/moonlibs} // libpaths clear warnings in win32 compilation
{$librarypath lib/bunsynth}
{$librarypath lib/sdl2}
{$unitpath lib/moonlibs}
{$unitpath lib/pasfluidsynth}
{$unitpath lib/bunsynth}
{$unitpath lib/sdl2}
{$unitpath inc}
{$ifdef WINDOWS}{$resource etc/saku.res}{$endif} // pulls in the app icon

{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

uses
{$ifdef UNIX}cthreads,{$endif}
{$ifdef WINDOWS}windows,{$endif} // for registry access, to enumerate fonts (TRegistry would bring other dependencies)
sysutils, // needed for file system traversal etc
process, // for shelling out to programs - REPLACE WITH SMALLER IMP
math,
SDL2,
SDL2_ttf,
paszlib,
mccommon,
mcfileio,
mccosine,
mcvarmon, // script variable handling system
mcgloder, // graphics loading and resizing
mcscaler,
mcblitters, // pixel buffer copying functions
mcsassm, // general asset management, streaming stuff from DAT-files
midireader, // used by bunnysound
pasfluidsynth, // used by bunnysound
minimutex, // used by bunnysound
bunnysound;

// Sakurascript compiler and types.
{$include inc/sakurascript-compiler.pas}

// Basic structures, helper functions.
{$include inc/saku/common.pas}

// Common visual element class.
{$include inc/saku/elements-implementation.pas}

// Virtual viewport handling.
{$include inc/saku/viewport-implementation.pas}

// Text box functions.
{$include inc/saku/box-sdl.pas}

// Gob functions.
{$include inc/saku/gobs-implementation.pas}

// All timed effects setup and execution.
{$include inc/saku/effects-implementation.pas}

// Rendering and visual effect functions.
{$include inc/saku/render-sdl.pas}

// Choicematic functions.
{$include inc/saku/choice-implementation.pas}

// Sakurascript execution, fiber handling system, and helpers.
{$include inc/saku/fiber-implementation.pas}

// Mouseoverables, events, script callbacks.
{$include inc/saku/event-implementation.pas}

// User input handling.
{$include inc/saku/input.pas}

// SDL-specific init, main loop, input handling, output display.
{$include inc/saku/base-sdl.pas}

// ------------------------------------------------------------------

begin
	PreserveConsole;
	if NOT HandleCommandlineParams then exit;
	try
		InitEverything;
		MainLoop;
		preference.WriteConfig;
	except
		on e : Exception do begin
			writeln(e.ToString);
			writeln(BacktraceStrFunc(ExceptAddr));
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, 'Error', @e.Message[1], NIL);
		end;
	end;
end.

