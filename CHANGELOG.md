Changelog
---------

### [0.97.0] - ongoing

- MayClub, Nocturn: Fix converter bug
- Sakura: Fix broken animation hack that may have left some resources unconverted
- Sakutool: Nicer result printing
- Sakurascript: Removed a dozen deprecated commands
- Sakurascript: Various small optimisations, compiler 0-10% faster
- PasFluidSynth: Reduced binary size, improved library selector
- Decomp: Support for Atlus' FC graphics in Giten Megami Tensei
- Decomp: Support for Excellents' DCP and P graphics and more archives, thanks to Reyaziel
- Decomp: Support for Fournine's CGL graphics, thanks to Reyaziel

### [0.96.1] - ad8b12b5 - 23-Dec-2024

- Basic sound system with midi output (FluidSynth, Windows midi)
- New commandline tool `sakutool`, which merges and obsoletes `decomp` and `recomp`
- In-game text skip command (Ctrl-F)
- Clickable maps for Eden no Kaori, Tasogare no Kyoukai
- MayClub and Nocturnal Illusion now playable and completable
- Ma Doll now playable and completable, thanks to Alatalo
- Rose Blood, M Hard, and Zest to Fantasy playable and completable, thanks to MindrustUK
- Jewel BEM Hunter Lime episode 1 now playable and completable
- Tenshitachi no Hohoemi now playable
- Blinking animations for Tenshitachi no Gogo Collections 1 and 2
- Basic dynamic graphic objects
- Better internal font management
- Target framerate per second configurable in ini-file
- Engine version displayed on front end
- Program icon added
- SDL2 dll's are now included in the binary release
- Decomp: JAST/Tiare script conversion improvements
- Decomp: Support for JAST's old BRGE images, thanks to Reyaziel
- Decomp: Support for Fairytale's ADA/MDA/PRS/GPC/GPA graphics
- Decomp: Support for Elf's PR6/PR7/PD7/PD8/ANI graphics, and GP4, thanks to Reyaziel
- Decomp: Support for Foster's FA2 archives, C24/C25 graphics
- Decomp: Support for Silence's resources, thanks to Alpharobo
- Sakurascript: Streamlined event system
- Sakurascript: New commands `select`, `fx.skip`, and `tbox.gettext`
- Sakurascript: New keywords `int8`, `int16`, `param`, `tohex`
- Fix sakurascript and asset manager bugs, thanks to MindrustUK
- Fix occasional choppiness in graphic transition effects

### [0.94.4] - 4a73446d - 02-Jun-2022

- Blinking animations for Amy98 and Xenon98, script conversion improvements
- Work around more renderer crashes on *nixes
- Save state fixes

### [0.94.3] - b5f7300c - 29-May-2022

- More powerful game string table manager
- Load TSV string table automatically if it's next to a game DAT
- Improved Choicematic design
- Screen fade effect while paused
- Decomp: Unified Sakurascript writer unit
- Decomp: Save 16-color images correctly as 4bpp indexed
- Decomp: MMD and MDS music conversion support
- Decomp: Ripping support for graphics GGD, CWL, CWP, ZBM
- Decomp: Ripping support for Ikura GDL archives, Himeya Arc2
- Decomp: Partial support for C's Ware/Himeya games
- Decomp: Support Otaku archives and MRS graphics, thanks to MindrustUK
- Decomp: Ripping support for Forest/Foster games, Jast Memorial Collection
- Decomp: Limited support for Foreigner by Great, thanks to Alatalo
- Decomp: Limited support for Lakers 1 by Apple Pie
- Fix pitch in font rendering, was broken due to SDL2 change
- Work around software renderer crash on *nixes
- Use profile directory more consistently when work dir not accessible

### [0.94.2] - d1fe3f27 - 20-Sep-2021

- Various savestate fixes, cleanup
- Calculate textbox margins automatically, clean up beveller and texturer
- Support override directories for easy game resource modification
- Sakurascript: Underscores as silent number separators
- Sakurascript: Allow dots in label declarations
- Sakurascript: Treat x.y as a fixed-point numeric literal for some purposes
- Sakurascript: Call-return supports a returnable value
- Decomp: Fix Nocturne/Mayclub bundle unpacking

### [0.94.1] - e5322180 - 06-Sep-2021

- Savestate support
- Gamepad support
- Right-click popup menu
- Added an About dialog box with version and license information
- Text fade effect and scrollbar in scrollable boxes
- Added margins and linespacing for textboxes, and tiled texturing
- Decomp: Improved PMD to midi conversion, including fuzzy instrument matching
- Decomp: Improved Maki-chan image conversion
- Decomp: Improved Pi image conversion
- Decomp: Support animations in Vanishing Point
- Added a help command in the runtime debug console
- Added an autotest mode that plays any game by rapidly selecting random choices
- Added a truecolor mode to console port
- Implement named textboxes instead of numbered
- Textbox columns can use automatic width
- Friendlier textbox param getting and setting in script code
- Allow connecting script variables to engine variables, eg. _allowsaving or _fullscreen
- Split generic file IO stuff into a mcfileio unit
- Handle null values separately in script code
- Skip frame to GPU push if nothing changed, makes engine use even less CPU
- Various fixes and refactoring

### [0.92] - 83f9f461 - 02-Jul-2017

- Published on Github
- New game-selector frontend, if you run the engine without specifying a dat
- Support for drop-in English translations for Japanese games
- Dropdown console in-game; Ctrl-T to show game text transcript, Ctrl-D to show debug log
  and execute script commands at runtime if debug mode is on
- User-controllable vsync and UI magnification options
- Mouseonly mouseoverables to use with things like sliders
- Added scrollable textboxes, more robust than previous hard paging
- Shell and Python scripts to assist automatic translation
- Dat modding support
- Further sakurascript work, many new commands
- Consolidated screen transitions to just the least tacky effects
- Choices in textboxes can now coexist with non-choosable text
- Other bugfixes

### [0.91]

- Switched to SDL2
- Created a supersakura-con port, which plays the games using character graphics in a console
- Redesigned the script parser; now has dynamically typed named variables, Powershell-style
  commands with dynamic parameters, scripts are split by label instead of by script file,
  and text is UTF-8 encoded
- Decomp: Support for PC-98 Nocturnal Illusion and Mayclub cat and lib files
- Added pause and single-stepping modes to help debugging, and script breakpoints, and
  co-operative multifibering
- Added explicit Z-levels for gobs
- Separated strings from scripts into a separate resource; string tables can be imported
  and exported into tsv spreadsheets for localising
- Redesigned textboxes, now with anchor points, free scrolling, flexible decorations, and
  a UI size multiplier user preference
- Decomp: only print labels for lines that are jumped to within the script, making the new
  label-grouped code more efficient
- Rewrote the graphics loading and resizing, only to find that retaining subpixel accuracy
  under this new model would be a big pain, dropped subpixel optimisation for now
- Moved a bunch of common code into a separate unit, mccommon
- Spun off the asset loader into a separate unit, mcsassm, and disabled the asset precacher
  for now; made the asset loader 100% thread-safe and way sleeker
- MCGLoder is no longer dependent on the Windows unit
- Massive code clean-up and re-organisation

### [0.90b]

- Added commands: gfx.adopt, gfx.orphan, choice.go, choice.get, choice.cancel
- Added an intermediate output buffer for Recomp so the final resource file is built in
  memory first; it's faster and reduces file system fragmentation
- Added a simple alpha rendering optimisation, 10-30% faster
- Made viewports more flexible
- Rewrote the bash effect and made it work on individual gobs
- Merged box styles into the main text box structures, moved box indexes up by one so box 0
  can be a system text box or console
- Smarter drawing region handler, now crops out partial overlaps
- Unified debug output paradigm, allows implementing an in-game debug console later
- The engine crash output now includes recent game script bytecode history to help debugging
- Fixed invalid memory read when drawing many gobs during a single frame

### [0.90]

- Improved support for Deep
- Improved support for Tasogare no Kyoukai, thanks to Zus at SakuraSoftFan Group
- Preliminary support for Muchimuchi Sekushii Pafe, also thanks to Zus
- Added a gfx.solidblit script code command, to draw any graphic object with a solid color,
  handy for fades and making sprites flash
- Added a sys.alreadyplayed command to allow automatic intro skipping
- Expanded script code escape handling, allowing dynamic resource references from strings
  and variables
- Made image area selection by keyboard a bit more intuitive
- Textbox autowaitkey on overflow now preserves text color changes
- Made Runaway fade to black ending look smoother in framed mode
- Decomp: Fixed bytecode 0B-49 (immediate bitmasked multiple choice)
- Fixed handling of overlapping sleep states
- Fixed lacking edge-alpha corner case in scaling algorithm
- Fixed and optimised mipmapped full-view pop-in transition
- Fixed bugs in textbox transition handling

### [0.89]

- Made pretty textboxes for 3sis
- Added tbox.move and tbox.resize script commands
- Added snap to edge textbox style option
- Added retro-style textbox background negation
- Made all swipe transitions use a snazzy fat fuzzy edge
- Rewired all screen transitions to work correctly inside a viewport
- Redesigned textbox properties to allow using a tiled base graphic, as well as improved
  positioning precision
- Made textbox movement and size changes relative to time instead of framerate
- Made the choice selection box correctly ignore escape codes
- Optimised text rendering, enabling anti-aliased in-line glyph printing
- Fixed off-by-1-pixel errors in scaling algorithm, hopefully for the last time

### [0.88]

- Added an always-topmost flag for gobs
- Made mouseoverable areas selectable by keyboard
- Rewrote the bash effect to play nice with everything else
- Separated implicit and explicit gob coordinates for consistency and precision
- Moved implicit waitkeys from SSakura into Decomp for streamlining and automatic
  double-waitkey elimination, which works surprisingly well
- Changed numeric expression handling to use powerful Reverse Polish Notation
- Fixed a textbox garbage bug on fullscreen switch, a bitmap byte order inconsistency,
  and several special effects bugs

### [0.87]

- Made new title screens with clickable options for the prime three games
- Implemented an event system (mouseover, timed, user interrupt)
- Added global subscript call ability
- Rewrote the gamma correction effect for efficiency and compatibility
- Allowed indirect variable references in scriptcode
- Added INC/DEC scriptcode commands as shorthand for equivalent LET
- Fixed gamma slide duration

### [0.86]

- Wrote ending scripts for the three prime games
- Reimplemented the credit roll effect
- Changed output to 32bpp to avoid dib padding issues
- Separated text control codes from printing buffer for consistency
- Improved textbox location precision and autofitting
- Rewrote user choice handling for greater flexibility
- Enhanced script code string handling
- Made the text drop shadow use a proportional size
- Beautify: Added subtle sprite edge smoothing
- Fixed textbox alpha pre-multiplication

### [0.85]

- Decomp: Big compatibility boost! Added translation of bytecodes 0B-04, 0B-37, 0B-46, 0B-49,
  0B-4B, 0E-xx, 11-06, 11-09, and 09-xx-yyzz, a redundant runscript in Transfer Student
- Decomp: Added minimal support for Maririn DX
- Fixed a graphic object adoption bug

### [0.84]

- Enhanced and unified the bash effect
- Added sys.quit script command
- Made sleep after transitions explicit, for better effects control
- Decomp: Added a Maki-chan Graphic reader for Angels' Eve Collection 1
- Fixed viewframe bugs, scripted viewframe logic for all supported games

### [0.83]

- Reimplemented gfx.getsequence, gfx.setsequence and gfx.setframe, for manual animation control
- Added gob movement effect, and gob alpha slide effect
- New screen update paradigm
- Decomp: Improved animation loading logic
- Fixed text alignment issues and a .SC5 midi translation bug

### [0.82]

- Implemented persistent tracking of seen text and graphics
- Implemented "Skip seen messages" command
- Decomp: Restructured graphic handling
- Decomp: Added hardcoded image compositing

### [0.81]

- Reimplemented vanilla image scaler, 4x faster
- Added emoticon glyph printing
- New textbox transitions: instant, fade, and left-to-right swipe
- Implemented 2x supersampling for textbox contents, but it sucked, so removed it
- Fixed a graphics offset bug after fullscreen switch and some textbox bugs

Initial asset reverse engineering started in 2005.
Engine development began in 2009, with the first three games completable by mid-2010,
and the initial engine source and binary release was in 2011.
