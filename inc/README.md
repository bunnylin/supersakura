### Includes

This directory has engine and tool source code.

- `saku/`: The SuperSakura game engine
- `aud/`: Sakutool functions for converting music and sound files
- `gfx/`: Sakutool functions for converting graphic formats
- `scr/`: Sakutool functions for converting game scripts

- `gidtable`: Game ID metadata, for identifying convertable games
- `decomp_core`: Sakutool unit for resource extraction
- `decomp_bundles`: Functions for extracting files from game data archives
- `decomp_exe`: Functions for hardcoded data extraction from binary executables
- `decomp_postproc`: Graphics postprocessing functions (resolution, transparency,
	compositing, hacks)
- `recomp_core`: Sakutool unit for turning extracted resources into a playable SuperSakura DAT
- `sakurascript`: Definitions and bytecode compiler for SuperSakura's scripting language
