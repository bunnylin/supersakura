{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Define this to get various debug output as standard output.
{$define !ssscriptdebugoutput}

{$include sakurascript-definition.pas}

function CompileScript(scriptname : UTF8string; inbuf, inbufend : pointer) : pointer;
// This takes a pointer to UTF-8 text data of size (inbufend - inbuf), compiles it to label-blocks of bytecode, and
// saves them in scriptObjects[]. Strings are saved in scriptObjects[script].stringTable[stringindex].txt[language].
// The input text data buffy must terminate with some kind of linebreak!
//
// Returns null if all went well; otherwise returns a pointer to a buffer containing a series of Pascal shortstrings
// with error messages, terminating with a 0-length string. The caller must free this.
//
// If you call this while script fibers exist, and a new label is added, you MUST update your fibers' script indexes
// before continuing execution, since the script list will be re-sorted to insert the new labels! Existing label code
// is overwritten if the same label is present in the input data. If a fiber was running in that label, you should
// terminate the fiber (with an error message), since whatever it was running no longer exists.
//
// If you're compiling a script at runtime, use an empty scriptname. This will hardcode all strings instead of creating
// permanent string table references which would waste memory. The compiled bytecode is placed in scriptObjects[0],
// which otherwise is an empty script. Existing fibers' script indexes don't need to be updated in this case since
// scriptObjects[0] always exists.
{$push}{$scopedEnums off}
var parserstate : (blankstate, readinglabel, readingcomment, readingdecnum, readinghexnum, readingdotnum,
	readingquostr, readingministr, readingminitrue, readingparam, readingoperator);

	lasttoken : (
		none, parenopen, flowcontrol, cmdtoken, paramtoken, operand, unaryprefixoperator, binaryoperator
		) = none;
	// Parenclosed is remembered as operand.
	// If-then-else-while-do are remembered as flowcontrol.
	// End is remembered as none.

type TStringTypeOverride = (auto = 0, hardcode = 1, dupable = 2, unique = 3);
var stringtypeoverride : TStringTypeOverride = auto;
{$pop}

var	i, j : dword;
	linestart : pointer;
	linenumber : dword;
	errorcount : dword;
	errorlist : array of string = NIL;
	newlabel : TScriptObject = NIL;

	opstack : array of record
		optoken : EScriptToken;
		opprecedence : byte;
	end;
	ifstack : array of dword = NIL;
	currentcmd : array of record
		cmdnum : byte;
		paramarray : array[0..127] of byte;
	end;
	opstackcount, ifstackcount, currentcmdcount : dword;

	labelcount, uniquestringcount : dword;
	// Compiled bytecode is temporarily stored here, and when a label ends, the bytecode is moved to script[].code^.
	// Note: Tried to use codep^ instead of codebuffy[codeofs], but yielded no performance difference.
	codebuffy : array of byte = NIL;
	codeofs : dword = 0;
	// These are for capturing literals as we parse the input buffer.
	strip : string;
	stripUTF8 : array[0..4095] of byte;
	stripp : pointer = NIL;
	stripUTF8len : dword;
	stripnum : dword;
	// Various state trackers.
	quotype : byte = 0;
	ifwhileexpr : byte;
	acceptnamelessparams : boolean; // params on the same line as their command can be nameless
	paramalreadyexplicit : boolean = FALSE; // command params may have an optional : or = after them, but only once

	procedure _Error(const msg : string);
	begin
		if errorcount >= dword(length(errorlist)) then setlength(errorlist, length(errorlist) + 16);
		if scriptname = '' then
			errorlist[errorcount] := strcat('line %,%: %', [linenumber, inbuf - linestart, msg])
		else
			errorlist[errorcount] := strcat('% (%,%): %', [scriptname, linenumber, inbuf - linestart, msg]);
		inc(errorcount);
	end;

	function _StripUTF8ToString : UTF8string;
	begin
		result := ''; setlength(result, stripUTF8len); move(stripUTF8[0], result[1], stripUTF8len);
	end;

	procedure _OpstackPop(pres : byte);
	// Pops the operator stack as long as stack item precedence >= pres. Popped operators are output as bytecode.
	// Command and param tokens come with a companion byte that must be popped at the same time.
	begin
		while (opstackcount <> 0) and (opstack[opstackcount - 1].opprecedence >= pres) do begin
			dec(opstackcount);
			codebuffy[codeofs] := byte(opstack[opstackcount].optoken);
			inc(codeofs);
			// When popping a command, also remove it from the currentcmd stack.
			if opstack[opstackcount].optoken = TOKEN_CMD then begin
				if currentcmdcount = 0 then _Error('internal: pop empty cmdstack');
				dec(currentcmdcount);
				if currentcmdcount = 0 then acceptnamelessparams := FALSE;
			end;
			// Companion byte! Pop it also.
			if opstack[opstackcount].optoken in [TOKEN_CMD, TOKEN_PARAM] then begin
				dec(opstackcount);
				codebuffy[codeofs] := byte(opstack[opstackcount].optoken);
				inc(codeofs);
				// Choice.get/call/goto? Print an implicit choice-react token.
				if (opstack[opstackcount + 1].optoken = TOKEN_CMD)
				and (byte(opstack[opstackcount].optoken) in [CMD_CHOICE_GET, CMD_CHOICE_CALL, CMD_CHOICE_GOTO])
				then begin
					codebuffy[codeofs] := byte(TOKEN_CHOICEREACT);
					inc(codeofs);
				end;
			end;
		end;
	end;

	procedure _OpstackDoPush(newtoken : EScriptToken; precedence : byte);
	begin
		if opstackcount >= dword(length(opstack)) then setlength(opstack, length(opstack) + 16);
		opstack[opstackcount].optoken := newtoken;
		opstack[opstackcount].opprecedence := precedence;
		inc(opstackcount);
	end;

	procedure _OpstackPushCmd(cmdid : byte);
	{$ifdef ssscriptdebugoutput}
	var i : dword;
	begin
		for i := high(ss_cmdlist) downto 0 do
			if ss_cmdlist[i].code = cmdid then break;
		writeln('command: ' + ss_cmdlist[i].namu + ' $' + strhex(cmdid));
	{$else}
	begin
	{$endif}

		// If this is an expression right after if or while, only one such is allowed before a corresponding then or do
		// must be present.
		if (lasttoken <> parenopen) and (ifwhileexpr <> 0) then begin
			if (ifwhileexpr and $1 <> 0) then begin
				if ifwhileexpr and $80 <> 0 then
					_Error('expected do, instead of a command')
				else
					_Error('expected then, instead of a command');
			end
			else inc(ifwhileexpr);
		end;

		// Check for expected element order.
		if lasttoken in [unaryprefixoperator, binaryoperator, paramtoken] then
			_Error('command statement must be in brackets to use its return value');

		// Pop any previous command or other expression, up to the first open bracket.
		_OpstackPop(11);
		lasttoken := cmdtoken;

		// Push the new command on the command stack, to keep track of which parameters will be allowed or mandatory.
		if currentcmdcount >= dword(length(currentcmd)) then setlength(currentcmd, length(currentcmd) + 16);
		currentcmd[currentcmdcount].cmdnum := cmdid;
		filldword(currentcmd[currentcmdcount].paramarray, 32, 0); // 128 bytes
		inc(currentcmdcount);

		// Output a marker for end of command params.
		codebuffy[codeofs] := byte(TOKEN_CMDEND);
		inc(codeofs);

		_OpstackDoPush(EScriptToken(cmdid), 14);
		_OpstackDoPush(TOKEN_CMD, 14);
		// Any params following this command on the same line don't have to have an explicit name. This allows
		// constructs like Print X3 "Bunny" as shorthand for usual commands, in this case Print box=X3 text="Bunny".
		acceptnamelessparams := TRUE;
	end;

	procedure _OpstackPushParam(paramid : byte);
	{$ifdef ssscriptdebugoutput}
	var i : dword;
	{$endif}
	begin
		paramalreadyexplicit := FALSE;
		if currentcmdcount = 0 then begin
			_Error('there is no active command for parameter ' + strip);
			exit;
		end;
		// Check for expected element order.
		if lasttoken in [unaryprefixoperator, binaryoperator, paramtoken] then
			_Error('expected expression, instead of parameter name');

		// Pop any previous param or expression.
		_OpstackPop(16);
		lasttoken := paramtoken;

		// Can't start a new param expression, named or dynamic, if open bracket from previous expression still there.
		if (opstackcount <> 0) and (opstack[opstackcount - 1].optoken = TOKEN_PARENOPEN) then begin
			if paramid <> CMDP_DYNAMIC then
				_Error('expected ) instead of parameter name')
			else
				_Error('expected ) instead of new expression');
		end;

		if paramid = CMDP_DYNAMIC then begin
			{$ifdef ssscriptdebugoutput}
			writeln('cmd dynamic param');
			{$endif}
			_OpstackDoPush(TOKEN_DYNPARAM, 16);
		end
		else begin
			{$ifdef ssscriptdebugoutput}
			for i := high(ss_paramlist) downto 0 do
				if ss_paramlist[i].code = paramid then break;
			writeln(strcat('cmd param: % $&', [ss_paramlist[i].id, paramid]));
			{$endif}

			// Check if named parameter is repeated or invalid for this command.
			if currentcmd[currentcmdcount - 1].paramarray[paramid] <> 0 then
				_Error('cmd parameter ' + strip + ' was already defined')
			else begin
				_OpstackDoPush(EScriptToken(paramid), 16);
				_OpstackDoPush(TOKEN_PARAM, 16);
				currentcmd[currentcmdcount - 1].paramarray[paramid] := 1;
			end;
		end;
	end;

	procedure _StartNewExpr(maybeprint : boolean);
	// If this is on the same line as a command, then generate an implicit dynamic parameter tag; otherwise terminate
	// the current command, if any. If maybeprint is true, an implicit print command is issued.
	begin
		if acceptnamelessparams then begin
			_OpstackPushParam(CMDP_DYNAMIC);
			exit;
		end;

		// If this is an expression right after if or while, only one such is allowed before a corresponding then or do
		// must be present.
		if ifwhileexpr <> 0 then begin
			if (ifwhileexpr and $1 <> 0) then begin
				if ifwhileexpr and $80 <> 0 then
					_Error('expected do, instead of an expression')
				else
					_Error('expected then, instead of an expression');
			end
			else inc(ifwhileexpr);
		end

		else begin
			// Pop any previous command or other expression, up to the first open bracket.
			_OpstackPop(11);
			if opstackcount <> 0 then
				if opstack[opstackcount - 1].optoken = TOKEN_PARENOPEN then begin
					_Error('expected ) instead of new expression');
					dec(opstackcount); // pop the mismatched open bracket
				end;
			if maybeprint then begin
				_OpstackPushCmd(CMD_TBOX_PRINT);
				_OpstackPushParam(CMDP_DYNAMIC);
			end;
		end;
	end;

	procedure _OpstackPush(token : EScriptToken);
	var precedence : byte;
	begin
		case token of
			TOKEN_PARENOPEN:
			// Open bracket: pop nothing, just push the token.
			begin
				if lasttoken in [none, flowcontrol, cmdtoken, operand] then _StartNewExpr(FALSE);
				_OpstackDoPush(TOKEN_PARENOPEN, 10);
				lasttoken := parenopen;
			end;

			TOKEN_PARENCLOSE:
			// Closed bracket: pop the operator stack until open bracket found.
			begin
				if lasttoken = paramtoken then
					_Error('expected expression after parameter name, instead of )')
				else
					if lasttoken in [unaryprefixoperator, binaryoperator] then
						_Error('expected expression after operator, instead of )');
				_OpstackPop(11);
				if (opstackcount = 0) or (opstack[opstackcount - 1].optoken <> TOKEN_PARENOPEN) then
					_Error('mismatched )')
				else
					dec(opstackcount); // pop the open bracket
				lasttoken := operand;
			end;

			else begin
				// Special case: equals-sign after parameter name is fine, skip it.
				//if (token = TOKEN_EQ) and (lasttoken = paramtoken) then exit;
				// Check for expected element order.
				if token in [TOKEN_NOT, TOKEN_NEG, TOKEN_VAR, TOKEN_RND, TOKEN_ABS, TOKEN_TONUM, TOKEN_TOSTR,
					TOKEN_TOHEX, TOKEN_INT8, TOKEN_INT16] then begin
					// Unary prefix operator.
					if lasttoken in [none, flowcontrol, cmdtoken, operand] then _StartNewExpr(FALSE);
					lasttoken := unaryprefixoperator;
				end
				else begin
					// Binary operator.
					if lasttoken in [none, parenopen, flowcontrol, cmdtoken, binaryoperator, unaryprefixoperator] then
						_Error('expected operand, instead of a binary operator')
					else
						if lasttoken = paramtoken then
							_Error('expected expression after parameter name, instead of a binary operator');
					lasttoken := binaryoperator;
				end;

				// Establish this token's precedence priority.
				precedence := 0;
				case token of
					TOKEN_SET, TOKEN_INC, TOKEN_DEC: precedence := 20;
					TOKEN_EQ, TOKEN_LT, TOKEN_GT, TOKEN_LE, TOKEN_GE, TOKEN_NE: precedence := 30;
					TOKEN_OR, TOKEN_XOR, TOKEN_PLUS, TOKEN_MINUS: precedence := 40;
					TOKEN_MUL, TOKEN_DIV, TOKEN_MOD, TOKEN_AND, TOKEN_SHL, TOKEN_SHR: precedence := 50;

					TOKEN_NOT, TOKEN_NEG, TOKEN_VAR, TOKEN_RND, TOKEN_ABS, TOKEN_TONUM, TOKEN_TOSTR, TOKEN_TOHEX,
						TOKEN_INT8, TOKEN_INT16: precedence := 60;
					else _Error('sus token $' + strhex(byte(token)));
				end;

				// Establish this token's associativity, lefty by default.
				// While precedence of topmost in opstack >= this lefty token,
				// or precedence of topmost in opstack > this righty token,
				// keep popping.
				if token in [TOKEN_NOT, TOKEN_NEG, TOKEN_VAR, TOKEN_RND, TOKEN_ABS, TOKEN_TONUM, TOKEN_TOSTR,
					TOKEN_TOHEX, TOKEN_INT8, TOKEN_INT16, TOKEN_SET, TOKEN_INC, TOKEN_DEC] then
					_OpstackPop(precedence + 1) // righty
				else
					_OpstackPop(precedence); // lefty

				// Check for variable assignment validity; the last thing output must have been a variable token.
				if token in [TOKEN_SET, TOKEN_INC, TOKEN_DEC] then begin
					if opstack[opstackcount].optoken <> TOKEN_VAR then
						_Error('left side of assignment must be a variable')
					else
						dec(codeofs);
				end;

				_OpstackDoPush(token, precedence);
			end;
		end;
	end;

	procedure _OpstackPushFlowControl(token : EScriptToken);
	var i : dword;
	begin
		// Check for expected element order.
		if lasttoken in [unaryprefixoperator, binaryoperator] then
			_Error('expected an expression after operator')
		else
			if lasttoken = paramtoken then _Error('expected an expression after a parameter name');
		lasttoken := flowcontrol;

		// Pop everything up to previous open bracket, if any.
		_OpstackPop(11);
		if (opstackcount <> 0) and (opstack[opstackcount - 1].optoken = TOKEN_PARENOPEN) then
			_Error('expected ), no brackets allowed in flow control');
		// Pop everything up to previous flow control.
		_OpstackPop(8);

		if ifstackcount >= dword(length(ifstack)) then setlength(ifstack, length(ifstack) + 16);

		case token of
			TOKEN_IF:
			begin
				// Check for expected flow control order.
				if opstackcount <> 0 then
					if opstack[opstackcount - 1].optoken = TOKEN_IF then begin
						_Error('expected if-expression-then, not if-if');
						exit;
					end
					else
						if opstack[opstackcount - 1].optoken = TOKEN_WHILE then
							_Error('expected while-expression-do, not while-if');

				_OpstackDoPush(TOKEN_IF, 7);
				ifwhileexpr := $40;
			end;

			TOKEN_THEN:
			begin
				// Check for expected flow control order.
				if (opstackcount = 0) or (opstack[opstackcount - 1].optoken <> TOKEN_IF) then begin
					_Error('then is only allowed after if');
					exit;
				end;
				if (ifwhileexpr and 1 = 0) then _Error('expected expression before then');
				ifwhileexpr := 0;
				// Replace if with then on opstack.
				opstack[opstackcount - 1].optoken := TOKEN_THEN;
				// Output if token.
				codebuffy[codeofs] := byte(TOKEN_IF);
				inc(codeofs);
				// Save the current code offset on ifstack.
				ifstack[ifstackcount] := codeofs;
				inc(ifstackcount);
				// Skip a longint in output, we'll fill it in later.
				inc(codeofs, 4);
			end;

			TOKEN_ELSE:
			begin
				// Check for expected flow control order.
				if (opstackcount = 0) or (opstack[opstackcount - 1].optoken <> TOKEN_THEN) then begin
					_Error('else is only allowed after a then-block');
					exit;
				end;
				// Replace then with else on opstack.
				opstack[opstackcount - 1].optoken := TOKEN_ELSE;
				// Pop from ifstack.
				if ifstackcount = 0 then begin
					_Error('internal: pop empty ifstack??');
					exit;
				end;
				dec(ifstackcount);
				i := ifstack[ifstackcount];
				// Output unconditional relative jump token.
				codebuffy[codeofs] := byte(TOKEN_JUMP);
				inc(codeofs);
				// Save the current code offset on ifstack.
				ifstack[ifstackcount] := codeofs;
				inc(ifstackcount);
				// Skip a longint in output, we'll fill it in later.
				inc(codeofs, 4);
				// Fill in the jump address for the previous then.
				longint((@codebuffy[i])^) := codeofs - i;
			end;

			TOKEN_WHILE:
			begin
				// Check for expected flow control order.
				if opstackcount <> 0 then
					if opstack[opstackcount - 1].optoken = TOKEN_IF then
						_Error('expected if-expression-then, not if-while')
					else
						if opstack[opstackcount - 1].optoken = TOKEN_WHILE then begin
							_Error('expected while-expression-do, not while-while');
							exit;
						end;
				_OpstackDoPush(TOKEN_WHILE, 7);
				ifwhileexpr := $80;
				// Save the current code offset on ifstack.
				ifstack[ifstackcount] := codeofs;
				inc(ifstackcount);
			end;

			TOKEN_DO:
			begin
				// Check for expected flow control order.
				if (opstackcount = 0) or (opstack[opstackcount - 1].optoken <> TOKEN_WHILE) then begin
					_Error('do is only allowed after while');
					exit;
				end;
				if (ifwhileexpr and 1 = 0) then _Error('expected expression before do');
				ifwhileexpr := 0;
				// Replace while with do on opstack.
				opstack[opstackcount - 1].optoken := TOKEN_DO;
				// Output if token (in this case it means a conditional relative jump).
				codebuffy[codeofs] := byte(TOKEN_IF);
				inc(codeofs);
				// Save the current code offset on ifstack.
				ifstack[ifstackcount] := codeofs;
				inc(ifstackcount);
				// Skip a longint in output, we'll fill it in later.
				inc(codeofs, 4);
			end;

			TOKEN_END:
			begin
				lasttoken := none;
				// Check for expected flow control order.
				if (opstackcount = 0)
				or (NOT (opstack[opstackcount - 1].optoken in [TOKEN_DO, TOKEN_THEN, TOKEN_ELSE]))
				then begin
					_Error('end is only allowed after a then/else/do-block');
					exit;
				end;
				// Pop the do/then/else from opstack.
				dec(opstackcount);
				// Pop from ifstack.
				if ifstackcount = 0 then begin
					_Error('internal: pop empty ifstack');
					exit;
				end;
				dec(ifstackcount);
				i := ifstack[ifstackcount];
				// Depending on whether this is if or while, the rest is different...
				if opstack[opstackcount].optoken = TOKEN_DO then begin
					// This is while-do-end...
					// Fill in the jump address for conditional jump.
					longint((@codebuffy[i])^) := codeofs - i + 5;
					// Pop from ifstack.
					if ifstackcount = 0 then begin
						_Error('internal: pop empty ifstack');
						exit;
					end;
					dec(ifstackcount);
					i := ifstack[ifstackcount];
					// Output unconditional relative jump token.
					codebuffy[codeofs] := byte(TOKEN_JUMP);
					inc(codeofs);
					// Jump distance back to the start of the while statement.
					longint((@codebuffy[codeofs])^) := i - codeofs;
					inc(codeofs, 4);
				end
				else begin
					// This is if-then-else-end...
					// Fill in the jump address for the end of the "then" block.
					longint((@codebuffy[i])^) := codeofs - i;
				end;
			end;
		end;
	end;

	procedure _OutputNumOp(num : longint);
	begin
		// Number operands are expected after operators and parameter names.
		// Otherwise, this must be the first element of the next expression.
		if lasttoken in [none, flowcontrol, cmdtoken, operand] then _StartNewExpr(FALSE);

		// If the previous operator is a unary negation, discard that and turn this number negative, but only if it
		// saves space. (So only applies to a negated dword, 1+1+4 bytes; a negative longint is smaller, 1+4 bytes.)
		// Negated dwords are nearly unused in practice; this optimisation saves less than the implementation overhead.
		{else if (lasttoken = unaryprefixoperator) and (num >= 65536)
		and (opstackcount <> 0) and (opstack[opstackcount - 1].optoken = TOKEN_NEG)
		then begin
			num := -num;
			dec(opstackcount);
		end;}

		lasttoken := operand;
		{$ifdef ssscriptdebugoutput}
		writeln('num:' + strdec(num));
		{$endif}

		// Number operands 0..32 are saved directly.
		if dword(num) <= 32 then begin
			codebuffy[codeofs] := byte(num);
			inc(codeofs);
		end
		// Number operands 32..255 are saved as a byte. Could go up to 287, but would yield minimal benefit.
		else if dword(num) <= 255 then begin
			codebuffy[codeofs] := byte(TOKEN_BYTE); inc(codeofs);
			codebuffy[codeofs] := byte(num); inc(codeofs);
		end
		// Number operands 256..65535 are saved as a word.
		else if dword(num) <= 65535 then begin
			codebuffy[codeofs] := byte(TOKEN_WORD); inc(codeofs);
			word((@codebuffy[codeofs])^) := num;
			inc(codeofs, 2);
		end
		// Number operands minint32..-1 and 256..maxint32 are saved as a longint.
		else begin
			codebuffy[codeofs] := byte(TOKEN_LONGINT); inc(codeofs);
			longint((@codebuffy[codeofs])^) := num;
			inc(codeofs, 4);
		end;
	end;

	procedure _OutputStrOp;
	// Outputs a string operand, either hardcoded or through the string table.
	var i : dword;

		procedure _addglobalstring;
		var s : UTF8string;
		begin
			s := _StripUTF8ToString;
			if FindGlobalString(s) <> high(dword) then exit;
			AddGlobalString(s);
		end;

		function _adduniquestring : dword;
		begin
			with newlabel do begin
				// Grow the string list if needed.
				if uniquestringcount >= dword(length(stringTable)) then
					setlength(stringTable, uniquestringcount + uniquestringcount shr 1 + 32);
				// Store the string in the next list slot.
				result := uniquestringcount;
				setlength(stringTable[uniquestringcount].txt, languageList.Length);
				stringTable[uniquestringcount].txt[labelLanguage] := _StripUTF8ToString;
				inc(uniquestringcount);
			end;
		end;

	begin
		// Check for expected control flow when outputting string operands.
		// If a non-quoted string appears as the first non-param expression element that's not in a conditional, it's
		// an error.
		if quotype = 0 then
			if (lasttoken = none)
			or (lasttoken = flowcontrol) and (ifwhileexpr and $F0 = 0)
			or (lasttoken in [cmdtoken, operand]) and (NOT acceptnamelessparams)
			then _Error('unknown command: ' + _StripUTF8ToString + '; add quotes if string literal');

		case lasttoken of
			flowcontrol: _StartNewExpr((quotype <> 0) and (ifwhileexpr and $F0 = 0));
			// If this string follows a command on the same line, it'll be a dynamic parameter, or on the next line
			// it's an implicit tbox.print command.
			none, cmdtoken, operand: _StartNewExpr(quotype <> 0);
			// Parenopen, unaryprefixoperator, binaryoperator: no action needed, string is part of current expression.
			// Paramtoken: no action needed, string affiliates with the param.
		end;
		lasttoken := operand;

		if stripUTF8len = 0 then begin
			{$ifdef ssscriptdebugoutput}
			writeln('<empty string>');
			{$endif}
			stringtypeoverride := auto;
			codebuffy[codeofs] := byte(TOKEN_EMPTYSTRING); inc(codeofs);
			exit;
		end;

		// Determine string type, unless already overridden.
		// If compiling a nameless script, you're probably doing it at runtime, so all strings can be hardcoded.
		if scriptname = '' then stringtypeoverride := hardcode;

		if stringtypeoverride = auto then begin
			// The currently active command often suggests a best choice.
			if currentcmdcount <> 0 then begin
				case currentcmd[currentcmdcount - 1].cmdnum of
					CMD_CALL, CMD_CASECALL, CMD_CASEGOTO, CMD_GOTO,
					CMD_LOG,
					CMD_EVENT_CREATE_TIMER, CMD_EVENT_GETTIMER, CMD_EVENT_REMOVE, CMD_EVENT_SETLABEL,
					CMD_EVENT_SETTIMER,
					CMD_FIBER_CALLSTACK, CMD_FIBER_LIST, CMD_FIBER_SIGNAL, CMD_FIBER_START, CMD_FIBER_STOP,
					CMD_FIBER_WAIT,
					CMD_GFX_ADOPT, CMD_GFX_BASH, CMD_GFX_CLEARKIDS, CMD_GFX_FIND, CMD_GFX_FLASH, CMD_GFX_GETFRAME,
					CMD_GFX_GETSEQUENCE, CMD_GFX_LIST, CMD_GFX_MOVE, CMD_GFX_PRECACHE, CMD_GFX_REMOVE,
					CMD_GFX_SETALPHA, CMD_GFX_SETFRAME, CMD_GFX_SETSEQUENCE, CMD_GFX_SETSOLIDBLIT, CMD_GFX_SHOW,
					CMD_GFX_TRANSITION,
					//CMD_MUS_PLAY, CMD_MUS_STOP,
					CMD_SYS_LOADDAT, CMD_SYS_SAVESTATE,
					CMD_SYS_SETCURSOR,
					CMD_TBOX_CLEAR, CMD_TBOX_DECORATE, CMD_TBOX_GETPARAM, CMD_TBOX_GETUSERINPUT, CMD_TBOX_LIST,
					CMD_TBOX_OUTLINE, CMD_TBOX_POPIN, CMD_TBOX_POPOUT, CMD_TBOX_REMOVE, CMD_TBOX_REMOVEDECOR,
					CMD_TBOX_REMOVEOUTLINES, CMD_TBOX_RESET, CMD_TBOX_SCROLL,
					CMD_TBOX_SETLOC, CMD_TBOX_SETPARAM, CMD_TBOX_SETSIZE, CMD_TBOX_SETTEXTURE,
					CMD_TBOX_USERINPUTSTOP,
					CMD_VIEWPORT_SETPARAMS:
					stringtypeoverride := hardcode;

					CMD_CHOICE_GET, CMD_CHOICE_ON, CMD_CHOICE_OFF, CMD_CHOICE_REMOVE:
					stringtypeoverride := dupable;

					CMD_CHOICE_SET:
					if quotype = 0 then
						stringtypeoverride := hardcode
					else
						stringtypeoverride := dupable;

					CMD_TBOX_PRINT, CMD_TBOX_USERINPUTSTART:
					if quotype = 0 then
						stringtypeoverride := hardcode
					else
						stringtypeoverride := unique;
				end;
			end;
			// if no useful command was active...
			if stringtypeoverride = auto then
				// Strings without quotes, that are not overridden, default to hardcoded.
				if quotype = 0 then stringtypeoverride := hardcode
				// Quote-encased strings default to unique.
				else stringtypeoverride := unique;
		end;

		if stringtypeoverride = unique then begin
			// Unique string, saved as a reference into the local string table.
			i := _adduniquestring;
			if i <= 255 then begin
				codebuffy[codeofs] := byte(TOKEN_MINIUNIQUESTRING); inc(codeofs);
				codebuffy[codeofs] := byte(i); inc(codeofs);
				{$ifdef ssscriptdebugoutput}
				write('miniunique-u[' + strdec(i) + ']:');
				{$endif}
			end
			else begin
				codebuffy[codeofs] := byte(TOKEN_LONGUNIQUESTRING); inc(codeofs);
				dword((@codebuffy[codeofs])^) := i; inc(codeofs, 4);
				{$ifdef ssscriptdebugoutput}
				write('longunique-U[' + strdec(i) + ']:');
				{$endif}
			end;
		end
		else begin
			// Hardcoded or global string, both are saved directly into the bytecode.
			// If this string doesn't exist under the global label, add it there.
			if stringtypeoverride = dupable then _addglobalstring;

			if stripUTF8len <= 255 then begin
				if stringtypeoverride = hardcode then begin
					if stripUTF8len <> 1 then begin
						codebuffy[codeofs] := byte(TOKEN_MINILOCALSTRING); inc(codeofs);
						codebuffy[codeofs] := byte(stripUTF8len);
					end
					else codebuffy[codeofs] := byte(TOKEN_CHAR);
					{$ifdef ssscriptdebugoutput}
					write('minilocal-z:');
					{$endif}
				end
				else begin
					codebuffy[codeofs] := byte(TOKEN_MINIGLOBALSTRING); inc(codeofs);
					codebuffy[codeofs] := byte(stripUTF8len);
					{$ifdef ssscriptdebugoutput}
					write('miniglobal-s:');
					{$endif}
				end;
				inc(codeofs);
			end
			else begin
				if stringtypeoverride = hardcode then begin
					codebuffy[codeofs] := byte(TOKEN_LONGLOCALSTRING);
					{$ifdef ssscriptdebugoutput}
					write('longlocal-Z:');
					{$endif}
				end
				else begin
					codebuffy[codeofs] := byte(TOKEN_LONGGLOBALSTRING);
					{$ifdef ssscriptdebugoutput}
					write('longglobal-S:');
					{$endif}
				end;
				inc(codeofs);
				dword((@codebuffy[codeofs])^) := stripUTF8len; inc(codeofs, 4);
			end;

			// Expand code output buffer if we must.
			if codeofs + stripUTF8len >= dword(length(codebuffy)) then
				setlength(codebuffy, (codeofs + stripUTF8len + 65535) and $FFFF0000);
			// Output the string itself.
			move(stripUTF8[0], codebuffy[codeofs], stripUTF8len);
			inc(codeofs, stripUTF8len);
		end;

		{$ifdef ssscriptdebugoutput}
		writeln(_StripUTF8ToString);
		{$endif}

		stringtypeoverride := auto;
	end;

	function _CheckMiniString : boolean;
	// Compares strip to the command list, parameters list, operator keywords, and if-then-else-end.
	// Pushes or outputs the appropriate thing in response.
	// Returns TRUE if the strip string matched something, else FALSE.
	var readp : ^char;
		i : dword;
	begin
		result := TRUE;

		// Is a command parameter possible here?
		if (currentcmdcount <> 0) and (NOT (lasttoken in [unaryprefixoperator, binaryoperator, paramtoken])) then begin
			// Look ahead: if there's a : or =, that indicates this is meant to be a parameter. However, := does not
			// indicate a param. :"stuff" is a global string, but param:"value" is valid too, so treat both as param.
			readp := inbuf;
			while (readp < inbufend) and (readp^ in [#0..#$20]) do inc(readp);
			if readp + 1 < inbufend then
				if (readp^ = ':') and ((readp + 1)^ <> '=')
				or (readp^ = '=') and (NOT ((readp + 1)^ in ['=','>','<']))
				then begin
					i := GetCmdParamIndex(strip);
					if i <> 0 then begin
						i := ss_paramlist[i].code;
						_OpstackPushParam(i);
						inbuf := readp + 1;
						exit;
					end;
				end;
		end;

		// Is it a recognised basic keyword?
		case length(strip) of
			2:
			if strip = 'if' then begin
				_OpstackPushFlowControl(TOKEN_IF);
				exit;
			end
			else if strip = 'do' then begin
				_OpstackPushFlowControl(TOKEN_DO);
				exit;
			end
			else if strip = 'or' then begin
				_OpstackPush(TOKEN_OR);
				exit;
			end;
			3:
			if strip = 'end' then begin
				_OpstackPushFlowControl(TOKEN_END);
				exit;
			end
			else if strip = 'and' then begin
				_OpstackPush(TOKEN_AND);
				exit;
			end
			else if strip = 'xor' then begin
				_OpstackPush(TOKEN_XOR);
				exit;
			end
			else if strip = 'not' then begin
				_OpstackPush(TOKEN_NOT);
				exit;
			end
			else if strip = 'div' then begin
				_OpstackPush(TOKEN_DIV);
				exit;
			end
			else if strip = 'mod' then begin
				_OpstackPush(TOKEN_MOD);
				exit;
			end
			else if strip = 'rnd' then begin
				_OpstackPush(TOKEN_RND);
				exit;
			end
			else if strip = 'abs' then begin
				_OpstackPush(TOKEN_ABS);
				exit;
			end
			else if strip = 'shl' then begin
				_OpstackPush(TOKEN_SHL);
				exit;
			end
			else if strip = 'shr' then begin
				_OpstackPush(TOKEN_SHR);
				exit;
			end;
			4:
			if strip = 'then' then begin
				_OpstackPushFlowControl(TOKEN_THEN);
				exit;
			end
			else if strip = 'else' then begin
				_OpstackPushFlowControl(TOKEN_ELSE);
				exit;
			end
			else if strip = 'int8' then begin
				_OpstackPush(TOKEN_INT8);
				exit;
			end;
			5:
			if strip = 'while' then begin
				_OpstackPushFlowControl(TOKEN_WHILE);
				exit;
			end
			else if strip = 'int16' then begin
				_OpstackPush(TOKEN_INT16);
				exit;
			end
			else if strip = 'tonum' then begin
				_OpstackPush(TOKEN_TONUM);
				exit;
			end
			else if strip = 'tostr' then begin
				_OpstackPush(TOKEN_TOSTR);
				exit;
			end
			else if strip = 'tohex' then begin
				_OpstackPush(TOKEN_TOHEX);
				exit;
			end;
			6:
			if strip = 'random' then begin
				_OpstackPush(TOKEN_RND);
				exit;
			end;
			8:
			if strip = 'tonumber' then begin
				_OpstackPush(TOKEN_TONUM);
				exit;
			end
			else if strip = 'tostring' then begin
				_OpstackPush(TOKEN_TOSTR);
				exit;
			end;
		end;
		// Is it a recognised command?
		i := GetCmdIndex(strip);
		if i <> 0 then begin
			_OpstackPushCmd(ss_cmdlist[i].code);
			exit;
		end;

		// Don't know what it is, must be a ministring, caller can handle it.
		result := FALSE;
	end;

	function _IsStripTooLong : boolean; inline;
	begin
		result := (stripUTF8len >= length(stripUTF8));
		if result then begin
			_Error('string exceeded max len ' + strdec(length(stripUTF8)) + ' bytes');
			stripUTF8len := 0;
		end;
	end;

	procedure _BeginMiniStr;
	begin
		quotype := 0; // non-quote-encased ministrings default to hardcoded
		strip := char(inbuf^);
		stripUTF8[0] := byte(inbuf^);
		stripUTF8len := 1;
		parserstate := readingministr;
	end;

	procedure _EndLabel;
	var i : dword;
	begin
		// Clean up the current label's leftovers.
		_OpstackPop(11);
		if opstackcount <> 0 then
			case opstack[opstackcount - 1].optoken of
				TOKEN_IF: _Error('expected then after if, instead of label');
				TOKEN_THEN: _Error('expected else/end after if-then, instead of label');
				TOKEN_ELSE: _Error('expected end after if-else, instead of label');
				TOKEN_WHILE: _Error('expected do after while, instead of label');
				TOKEN_DO: _Error('expected end after while-do, instead of label');
				TOKEN_PARENOPEN: _Error('expected ), instead of label');
			end;
		// React to the last token...
		case lasttoken of
			unaryprefixoperator, binaryoperator: _Error('expected an operand, instead of label');
			paramtoken: _Error('expected an expression after parameter name, instead of label');
		end;

		// Save the built bytecode.
		Assert(length(newlabel.code) = 0);
		if codeofs <> 0 then begin
			{$ifdef ssscriptdebugoutput}
			writeln(newlabel.labelName + ' BYTECODE DUMP:');
			DumpBuffer(@codebuffy[0], codeofs); // see mccommon unit
			writeln;
			{$endif}
			// Note: It's faster to copy to a new exact-sized label buffy, than renaming and shrinking codebuffy to
			// the label buffy and getting a new same-size codebuffy.
			setlength(newlabel.code, codeofs);
			move(codebuffy[0], newlabel.code[0], codeofs);
			codeofs := 0;
		end;

		// Shrink the string array to a precise size.
		if length(newlabel.stringTable) <> 0 then setlength(newlabel.stringTable, uniquestringcount);

		// Add the new label to the scriptObjects[] array.
		i := GetScript(newlabel.labelName);
		if (i = 0) and (scriptname <> '') then begin
			// Add as a new scriptObjects[] item.
			i := scriptObjectCount;
			if i >= dword(length(scriptObjects)) then setlength(scriptObjects, i + (i shr 1) + 20);
			scriptObjects[i] := newlabel;
			inc(scriptObjectCount);
			SortScripts(TRUE);
		end
		else begin
			// This label already exists, overwrite previous. But if it's a nameless script at runtime, that'll be in
			// index 0 and only uses hardcoded strings, so spare the global string list otherwise found at index 0.
			if i = 0 then begin
				setlength(newlabel.stringTable, 0);
				newlabel.stringTable := scriptObjects[0].stringTable;
				scriptObjects[0].stringTable := NIL;
			end;
			scriptObjects[i].Destroy;
			scriptObjects[i] := newlabel;
		end;
		newlabel := NIL;
	end;

	procedure _InitLabel(nam : string);
	var i : dword;
	begin
		i := pos('.', nam);
		if (i <> 0) and (pos('.', nam, i + 1) <> 0) then begin
			_Error('at most one dot allowed in label: @' + nam);
			nam := '!twodots';
			i := 0;
		end;
		if (i > 32) or (nam.Length >= i + 32) then begin
			_Error('too long label, max 31+31 bytes: @' + nam);
			nam := '!toolong';
			i := 0;
		end;
		if i = 0 then nam := scriptname + '.' + nam;
		if newlabel <> NIL then begin
			// Connect the previous label to this new one.
			newlabel.nextLabel := nam;
			// Save the previous label.
			_EndLabel;
		end;
		// New label starts with a blank slate.
		newlabel := TScriptObject.Create;
		lasttoken := none;
		stringtypeoverride := auto;
		acceptnamelessparams := FALSE;
		paramalreadyexplicit := FALSE;
		codeofs := 0;
		opstackcount := 0;
		ifstackcount := 0;
		currentcmdcount := 0;
		uniquestringcount := 0;
		ifwhileexpr := 0;
		quotype := 0;
		// New label inits.
		newlabel.labelName := nam;
		newlabel.nextLabel := '';
		newlabel.labelLanguage := defaultScriptLanguage;
		setlength(newlabel.code, 0);
		setlength(newlabel.stringTable, 0);
		inc(labelcount);
	end;

	procedure _UnexpectedCharError;
	begin
		_Error(strcat('unexpected character % ($&)', [char(inbuf^), byte(inbuf^)]));
	end;

begin
	Assert((inbuf < inbufend) and (byte((inbufend - 1)^) = $A), 'script buffer must end with 0A');
	errorcount := 0;
	linenumber := 1;
	linestart := inbuf;

	strip := ''; stripnum := 0; stripUTF8len := 0; labelcount := 0;
	opstackcount := 0; currentcmdcount := 0; // silence compiler
	opstack := NIL; currentcmd := NIL;
	setlength(opstack, 64);
	setlength(ifstack, 16);
	setlength(currentcmd, 16);

	// The command params map may need initing.
	if ss_cmdparams[CMD_TBOX_PRINT][CMDP_BOX] = ARG_INVALID then ss_cmdparams_init;

	// Output goes here.
	setlength(codebuffy, 65536);

	parserstate := blankstate;
	// Initialise the implicit script start label.
	scriptname := upcase(scriptname);
	_InitLabel('');

	// Skip byte order mark if present.
	if (byte(inbuf^) = $EF) and (byte((inbuf + 1)^) = $BB) and (byte((inbuf + 2)^) = $BF) then inc(inbuf, 3);

	// Process the input text buffy.
	while inbuf < inbufend do begin

		case parserstate of

			blankstate:
			begin
				// Expand code output buffer as needed.
				if codeofs + 32 >= dword(length(codebuffy)) then
					setlength(codebuffy, length(codebuffy) + length(codebuffy) shr 1 + 65536);

				case char(inbuf^) of

					// Linebreak, LF/VT/FF/CR.
					#$A..#$D:
					begin
						// CR + LF
						if (inbuf + 1 < inbufend)
						and (char((inbuf + 1)^) in [#$A..#$D]) and (char(inbuf^) <> char((inbuf + 1)^)) then inc(inbuf);

						inc(linenumber);
						linestart := inbuf;
						acceptnamelessparams := FALSE;
					end;

					// Whitespace, comma.
					#9, #$20, ',': ;

					// Control chars.
					#0..#8, #$E..#$1F: _UnexpectedCharError;

					// Labels.
					'@':
					begin
						strip := '';
						parserstate := readinglabel;
					end;

					// Numerals.
					'0':
					begin
						stripp := inbuf;
						// If the number starts with 0x or 0X, it's actually a hexadecimal!
						if byte((inbuf + 1)^) or $20 = ord('x') then begin
							inc(inbuf);
							stripnum := 0;
							parserstate := readinghexnum;
						end
						else begin
							stripnum := 0;
							parserstate := readingdecnum;
						end;
					end;
					'1'..'9':
					begin
						stripp := inbuf; // stash number start position in case it's a ministring
						stripnum := byte(inbuf^) - 48;
						parserstate := readingdecnum;
					end;

					// ... is waitkey, ." is string type override, anything else is a ministring.
					'.':
					begin
						if (char((inbuf + 1)^) = '.') and (char((inbuf + 2)^) = '.') then begin
							_OpstackPushCmd(CMD_FIBER_WAITKEY);
							inc(inbuf, 2);
						end
						else if char((inbuf + 1)^) in ['"','''','`'] then
							stringtypeoverride := TStringTypeOverride.hardcode
						else
							_BeginMiniStr;
					end;

					// Quoted string literal.
					'"', '''', '`':
					begin
						// Save the quote character, the string must end with the same char.
						quotype := byte(inbuf^);
						stripUTF8len := 0;
						parserstate := readingquostr;
					end;

					// Comments.
					'#': parserstate := readingcomment;
					'/':
					begin
						if char((inbuf + 1)^) = '/' then begin
							inc(inbuf);
							parserstate := readingcomment;
						end
						else begin
							// If the / isn't followed by another /, it must be an attempt to divide something instead.
							_OpstackPush(TOKEN_DIV);
						end;
					end;

					// Single-character operators and brackets.
					// / % * $ ^ ( )
					'%': _OpstackPush(TOKEN_MOD);
					'*': _OpstackPush(TOKEN_MUL);
					'$': _OpstackPush(TOKEN_VAR);
					'^': _OpstackPush(TOKEN_XOR);
					'(': _OpstackPush(TOKEN_PARENOPEN);
					')': _OpstackPush(TOKEN_PARENCLOSE);

					// Possibly double-character operators (and special AGAIN command).
					// != -= += && || <= >= <> << >> |:
					'!', '-', '+', '&', '|', '<', '>':
					begin
						strip := char(inbuf^);
						parserstate := readingoperator;
					end;

					// Possibly double-character operator == =< =>, except after lone colon after parameter name.
					'=':
					if (lasttoken = paramtoken) and (NOT paramalreadyexplicit) then
						paramalreadyexplicit := TRUE
					else begin
						strip := char(inbuf^);
						parserstate := readingoperator;
					end;

					// Double-character operator :=, or parameter name indicator, or string dupability override.
					':':
					begin
						if char((inbuf + 1)^) = '=' then begin
							inc(inbuf);
							_OpstackPush(TOKEN_SET);
						end

						else if char((inbuf + 1)^) in ['"','''','`'] then
							stringtypeoverride := TStringTypeOverride.dupable

						else if (lasttoken = paramtoken) and (NOT paramalreadyexplicit) then
							paramalreadyexplicit := TRUE
						else _Error('unexpected : here');
					end;

					// Backslash, must be an escape char for a ministring.
					'\':
					begin
						// Since the char after the escape should be processed simultaneously, don't use _BeginMiniStr.
						quotype := 0; // non-quote-encased ministrings default to hardcoded
						strip := '';
						stripUTF8len := 0;
						parserstate := readingministr;
						continue;
					end;

					// String uniqueness override, or ministring.
					else begin
						if (char(inbuf^) = 'u') and (char((inbuf + 1)^) in ['"','''','`']) then
							stringtypeoverride := TStringTypeOverride.unique
						else begin
							_BeginMiniStr;
							if strip[1] in ['A'..'Z'] then byte(strip[1]) := ord(strip[1]) or $20;
						end;
					end;
				end;
			end;

			readinglabel:
			begin
				case char(inbuf^) of
					// Linebreak, LF/VT/FF/CR.
					#$A..#$D:
					begin
						_Error('label @' + strip + ' must end with : on the same line');
						parserstate := readingcomment;
						continue;
					end;
					// Control chars.
					#0..#9, #$E..#$1F:
					begin
						_UnexpectedCharError;
						parserstate := readingcomment;
						continue;
					end;
					// End of label.
					':':
					begin
						if strip = '' then
							_Error('label name cannot be empty')
						else
							_InitLabel(strip);
						parserstate := blankstate;
					end;

					// Anything else is a fair label character.
					else begin
						if strip.Length >= 63 then begin
							_Error('too long label, max 31+31 bytes: @' + strip);
							parserstate := readingcomment;
						end
						else begin
							if char(inbuf^) in ['a'..'z'] then byte(inbuf^) := byte(inbuf^) and $DF; // uppercase
							inc(byte(strip[0]));
							strip[byte(strip[0])] := char(inbuf^);
						end;
					end;
				end;
			end;

			readingcomment:
			case char(inbuf^) of
				// Linebreak, LF/VT/FF/CR.
				#$A..#$D:
				begin
					parserstate := blankstate;
					continue;
				end;
			end;

			readingdecnum:
			case char(inbuf^) of
				'0'..'9': {$push}{$Q-} stripnum := (stripnum * 10 + byte(byte(inbuf^) - 48)) and $7FFFFFFF; {$pop}
				'_': ;
				// Actually this is an unquoted ministring that started with a decimal digit.
				'A'..'Z', 'a'..'z', '\', #$80..#$FF:
				begin
					inbuf := stripp;
					strip := ''; stripUTF8len := 0;
					parserstate := readingminitrue;
					continue;
				end;
				'.':
				begin
					{$push}{$Q-} stripnum := stripnum shl 15; {$pop}
					byte(strip[0]) := 0;
					parserstate := readingdotnum;
				end;
				else begin
					dec(inbuf);
					_OutputNumOp(longint(stripnum));
					parserstate := blankstate;
				end;
			end;

			readingdotnum:
			case char(inbuf^) of
				'0'..'9':
				begin
					i := (byte(inbuf^) - 48) shl 15;
					case byte(strip[0]) of
						0: inc(stripnum, i div 10);
						1: inc(stripnum, i div 100);
						2: inc(stripnum, i div 1000);
						3: inc(stripnum, i div 10000);
					end;
					inc(byte(strip[0]));
				end;
				'_': ;
				// Actually this is an unquoted ministring that started with a decimal digit.
				'A'..'Z', 'a'..'z', '\', #$80..#$FF:
				begin
					inbuf := stripp;
					strip := ''; stripUTF8len := 0;
					parserstate := readingminitrue;
					continue;
				end;
				'.':
				begin
					_Error('more than 1 dot in number');
					parserstate := readingcomment;
				end;
				else begin
					dec(inbuf);
					_OutputNumOp(longint(stripnum));
					parserstate := blankstate;
				end;
			end;

			readinghexnum:
			case char(inbuf^) of
				'0'..'9': stripnum := dword(stripnum shl 4) or byte(byte(inbuf^) - 48);
				'A'..'F': stripnum := dword(stripnum shl 4) or byte(byte(inbuf^) - 55);
				'a'..'f': stripnum := dword(stripnum shl 4) or byte(byte(inbuf^) - 87);
				'_': ;
				// Actually this is an unquoted ministring that started with a hexadecimal digit.
				'G'..'Z', 'g'..'z', '\', #$80..#$FF:
				begin
					inbuf := stripp;
					strip := ''; stripUTF8len := 0;
					parserstate := readingminitrue;
					continue;
				end;
				else begin
					dec(inbuf);
					_OutputNumOp(longint(stripnum));
					parserstate := blankstate;
				end;
			end;

			readingquostr:
			if byte(inbuf^) = quotype then begin
				_OutputStrOp;
				parserstate := blankstate;
			end
			else begin
				case char(inbuf^) of
					// Linebreak, LF/VT/FF/CR.
					#$A..#$D:
					begin
						_Error('expected ' + char(quotype) + ' before end of line');
						parserstate := blankstate;
						continue;
					end;
					// Tab.
					#9:
					begin
						stripUTF8[stripUTF8len] := ord('\');
						inc(stripUTF8len);
						stripUTF8[stripUTF8len] := ord('t');
						inc(stripUTF8len);
					end;
					// Control chars.
					#0..#8, #$E..#$1F: _UnexpectedCharError;
					// Escape char.
					'\':
					begin
						inc(inbuf);

						case char(inbuf^) of
							// Escape + control char not allowed.
							#0..#9, #$E..#$1F: begin
								_UnexpectedCharError;
							end;
							// Ignore linebreaks LF/VT/FF/CR, and swallow the backslash - it's a multiline string.
							#$A..#$D:
							begin
								inc(linenumber);
								inc(inbuf);
								if (inbuf < inbufend)
								and (char((inbuf)^) in [#$A..#$D])
								and (char(inbuf^) <> char((inbuf - 1)^)) // CR+LF pair etc
								then inc(inbuf);

								linestart := inbuf;
								continue;
							end;
							// Anything else can be saved verbatim, except if it's our quote mark, drop the backslash.
							else begin
								if _IsStripTooLong then continue;
								if byte(inbuf^) <> quotype then begin
									stripUTF8[stripUTF8len] := ord('\');
									inc(stripUTF8len);
									if _IsStripTooLong then continue;
								end;
								stripUTF8[stripUTF8len] := byte(inbuf^);
								inc(stripUTF8len);
							end;
						end;
					end;
					// Anything else is a fair string character.
					else begin
						if _IsStripTooLong then continue;
						// Save the escaped char.
						stripUTF8[stripUTF8len] := byte(inbuf^);
						inc(stripUTF8len);
					end;
				end;
			end;

			// Unquoted identifier, could be a string literal, or command or parameter or operator...
			readingministr:
			begin
				case char(inbuf^) of
					// Basic letters and dots. (And accept int8 and int16 operators.)
					'A'..'Z', 'a'..'z', '.', '1','6','8':
					begin
						inc(byte(strip[0]));
						if char(inbuf^) in ['A'..'Z'] then
							byte(strip[byte(strip[0])]) := byte(inbuf^) or $20 // lowercase
						else
							byte(strip[byte(strip[0])]) := byte(inbuf^);
						stripUTF8[stripUTF8len] := byte(inbuf^);
						inc(stripUTF8len);
						// If string is very long, must be a trueministring.
						if stripUTF8len >= 24 then parserstate := readingminitrue;
					end;
					// Whitespace, line break, brackets, comments, comma. Colon or equals-sign.
					#9, ' ', #$A..#$D, '(', ')', '#', '/', ',', ':', '=':
					begin
						// Was the last token a $?
						if (opstackcount <> 0)
						and (opstack[opstackcount - 1].optoken = TOKEN_VAR) and (lasttoken = unaryprefixoperator)
						then begin
							// yes, this must be a ministring; specifically, a variable name.
							parserstate := readingminitrue;
						end

						else if strip = 'param' then begin
							if currentcmdcount <> 0 then _Error('param should not be used inside a command statement');
							parserstate := readingparam;
							strip := '';
						end

						// Is this string snippet a command/parameter/operator/if-then-else-end?
						else begin
							if _CheckMiniString then begin
								// yes, it was; it has been processed, return to blank state.
								parserstate := blankstate;
							end
							else begin
								// no; it must be a plain ministring.
								parserstate := readingminitrue;
							end;
						end;
						continue;
					end;
					// Anything else - can't be command/parameter/operator/if-then-else-end.
					else begin
						parserstate := readingminitrue;
						continue;
					end;
				end;
			end;

			// Unquoted identifier, but it can only be a string literal.
			readingminitrue:
			case char(inbuf^) of
				// Control char, whitespace, brackets, hash comment, comma...
				#0..#$20, '(', ')', '#', ',',
				// ... and various operators that are more important than the string.
				'+','=','!','<','>',':','&','|':
				begin
					_OutputStrOp;
					parserstate := blankstate;
					continue;
				end;
				// Variable dereference is an implicit plus.
				'$':
				begin
					dec(inbuf);
					char(inbuf^) := '+';
					_OutputStrOp;
					parserstate := blankstate;
					continue;
				end;
				// Double-slash comment.
				'/':
				begin
					if char((inbuf + 1)^) = '/' then begin
						_OutputStrOp;
						parserstate := blankstate;
						continue;
					end;

					// Single slash is a fair string char.
					stripUTF8[stripUTF8len] := byte(inbuf^);
					inc(stripUTF8len);
				end;
				// Escape char.
				'\':
				begin
					// Save the backslash
					if _IsStripTooLong then continue;
					stripUTF8[stripUTF8len] := ord('\');
					inc(stripUTF8len);
					inc(inbuf);
					// Escape + control char not allowed.
					if byte(inbuf^) in [0..31] then begin
						_UnexpectedCharError;
						byte(inbuf^) := 32;
					end;
					// Save the escaped char.
					if _IsStripTooLong then continue;
					stripUTF8[stripUTF8len] := byte(inbuf^);
					inc(stripUTF8len);
				end;
				// Anything else is a fair string char.
				else begin
					if _IsStripTooLong then continue;
					stripUTF8[stripUTF8len] := byte(inbuf^);
					inc(stripUTF8len);
				end;
			end;

			// Reading what must be a recognised command parameter name.
			readingparam:
			case char(inbuf^) of
				// Basic letters, the only thing parameter names have.
				'A'..'Z', 'a'..'z':
				if strip.Length < 255 then begin
					inc(byte(strip[0]));
					if char(inbuf^) in ['A'..'Z'] then
						byte(strip[byte(strip[0])]) := byte(inbuf^) or $20 // lowercase
					else
						byte(strip[byte(strip[0])]) := byte(inbuf^);
				end;

				// Whitespace is ignored if before the first parameter name character. Anything else ends param name.
				else if (strip <> '') or (NOT (char(inbuf^) in [#9, #$20])) then begin
					if strip = '' then
						_Error('expected param name')
					else begin
						if (strip = 'any') or (strip = 'dynamic') then
							i := CMDP_DYNAMIC
						else if strip = 'anynumber' then
							i := CMDP_ANYNUMBER
						else if strip = 'anystring' then
							i := CMDP_ANYSTRING
						else begin
							i := ss_paramlist[GetCmdParamIndex(strip)].code;
							if i = 0 then _Error('no such param: ' + strip);
						end;
						codebuffy[codeofs] := byte(TOKEN_GETPARAM); inc(codeofs);
						codebuffy[codeofs] := i; inc(codeofs);
						lasttoken := operand;
					end;
					parserstate := blankstate;
					continue;
				end;
			end;

			readingoperator:
			begin
				// This state is only for checking the second character of potentially double-character operators, so
				// we always exit the state right after.
				parserstate := blankstate;
				// We're watching out for these:
				//   != += -= && || == =< => <= >= <> << >> |:
				// Check the first character
				case strip[1] of
					'!':
					if char(inbuf^) = '=' then
						_OpstackPush(TOKEN_NE)
					else begin
						dec(inbuf);
						_OpstackPush(TOKEN_NOT);
					end;
					'+':
					if char(inbuf^) = '=' then
						_OpstackPush(TOKEN_INC)
					else begin
						dec(inbuf);
						_OpstackPush(TOKEN_PLUS);
					end;
					'-':
					if char(inbuf^) = '=' then
						_OpstackPush(TOKEN_DEC)
					else begin
						// Decide whether it's a binary minus op or unary negator.
						dec(inbuf);
						if lasttoken = operand then
							_OpstackPush(TOKEN_MINUS)
						else
							_OpstackPush(TOKEN_NEG);
					end;
					'&':
					begin
						if char(inbuf^) <> '&' then dec(inbuf);
						_OpstackPush(TOKEN_AND);
					end;
					'|':
					begin
						if char(inbuf^) = ':' then
							_OpstackPushCmd(CMD_AGAIN)
						else begin
							if char(inbuf^) <> '|' then dec(inbuf);
							_OpstackPush(TOKEN_OR);
						end;
					end;
					'=':
					case char(inbuf^) of
						'=': _OpstackPush(TOKEN_EQ);
						'<': _OpstackPush(TOKEN_LE);
						'>': _OpstackPush(TOKEN_GE);
						else begin
							dec(inbuf);
							_OpstackPush(TOKEN_EQ);
						end;
					end;
					'<':
					case char(inbuf^) of
						'=': _OpstackPush(TOKEN_LE);
						'<': _OpstackPush(TOKEN_SHL);
						'>': _OpstackPush(TOKEN_NE);
						else begin
							dec(inbuf);
							_OpstackPush(TOKEN_LT);
						end;
					end;
					'>':
					case char(inbuf^) of
						'=': _OpstackPush(TOKEN_GE);
						'<': _OpstackPush(TOKEN_NE); // should >< have a different meaning?
						'>': _OpstackPush(TOKEN_SHR);
						else begin
							dec(inbuf);
							_OpstackPush(TOKEN_GT);
						end;
					end;
				end;
			end;
		end;

		inc(inbuf);
	end;
	// End of script processing code! Just terminate the final label.
	_EndLabel;

	// Resize the global strings array to a precise size.
	setlength(scriptObjects[0].stringTable, globalStringCount);

	// Return results; nil for all good, otherwise a list of error strings.
	result := NIL;
	if errorcount = 0 then exit;

	getmem(result, errorcount * 256 + 1);
	j := 0;
	for i := 0 to errorcount - 1 do begin
		move(errorlist[i][0], (result + j)^, byte(errorlist[i][0]) + 1);
		inc(j, byte(errorlist[i][0]) + byte(1));
	end;
	byte((result + j)^) := 0;
end;

