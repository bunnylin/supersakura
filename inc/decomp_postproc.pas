{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Image post-processing functions.

// Compositing strategy
// --------------------
//
// Logical graphic layouts:
// - Fullscreen image, or full-viewframe image
// - Character sprite with alpha
// - Simple multiframe animated sprite, like blinking
// - Combined multiframe animated sprite, with separate animated things in the same file
// - Fullscreen overlay to make a modified image, maybe a solid rectangle, maybe a per-pixel diff
// - Sprite overlay with alpha, like a different facial expression
// - Image map, multiple irregularly placed non-animated frames
// - Tile map, or sprite sheet, many regularly placed frames with possibly several animation sequences
//
// Overlays must be positioned pixel-perfectly, so they must be composited at the original resolution or an integer
// multiple. Compositing is a bit slow, so there's a tradeoff - doing it at rip/build time will create larger game
// assets; doing it at runtime may mean more CPU usage while playing, although the smaller image loads faster.
// Since SuperSakura is very efficient, it can afford to do runtime compositing without worries.
//
// Classic PC98 games typically try to minimise their graphic sizes and use more partial overlays, but usually don't
// go as far as full per-pixel diff, so such graphics could be shrunk further. More modern games go to the other
// extreme and have few or no overlays, at the cost of vastly more space used for graphics. The best approach here is
// to pre-composite overlays where possible, then run a custom diff algorithm as a manual step prior to dat build to
// aggressively deduplicate all graphics via the dynamic graphic loading system.
//
// Hi-fi filters are quite slow and can't be applied at runtime; loadtime is possible but iffy. Best do it before or as
// part of dat build. Since hi-fi filters are context-dependent, this introduces a problem where overlays' edge context
// isn't obvious before runtime. The bases for overlays therefore have to be hardcoded as part of post-processing.
// Pre-compositing overlays takes care of this in part, but not entirely; won't resolve animated overlays (blinking).
//
// Frame selection: Ideally, all frames are the same size and use the same location. In this case, they need to be
// stacked vertically (proc RearrangeFrames), and drawing any frame is as easy as selecting the correct start offset.
// With some luck, a tilemap or sprite sheet will fit into this paradigm, although multi-tile graphics may require some
// hackery to meet game scripts' expectations. The rearranging can't be done while ripping, since source graphics don't
// don't have to come through decomp and thus may not be pre-stacked when build starts. It would also make tilemaps
// look awful. Rearrange must be done as part of dat build, but also must be done at loose file load for consistency.
//
// The remaining problem are images with haphazardly placed multiple frames. Most likely the game's script indicates
// which source rectangle to blit from. This shall be accessed in two ways in ssakura: gfx.show commands may leverage
// the dynamic graphic loading system to load a particular portion of the source image, or that definition can be
// placed in an inf file and given a virtual graphic name that can be used like a normal graphic.

var postProcImg : mcg_bitmap = NIL; // persistent object for efficient repeated use
	postProcImgName : UTF8string = '';

procedure LoadPostProcImg(metadata : TGraphicFile; const imgname : UTF8string);
var loader : TFileLoader = NIL;
	srcpath : UTF8string;
begin
	if imgname = postProcImgName then exit;
	Assert(metadata.srcFilePath <> '', 'Set srcFilePath before calling ApplyHacks etc');
	srcpath := PathCombine([ExtractFilePath(metadata.srcFilePath), imgname + '.png']);
	loader := TFileLoader.Open(srcpath);
	if postProcImg <> NIL then begin postProcImg.Destroy; postProcImg := NIL; end;

	try
		postProcImg := mcg_bitmap.FromPNG(loader.readp, loader.fullFileSize, [MCG_FLAG_FORCE8BPP]);
	finally
		if loader <> NIL then loader.Destroy;
	end;
	postProcImgName := imgname;
end;

procedure CompositeGraphics(game : PGameRecord);
{$push}{$scopedEnums off}
type ECompAction = (img1_above_img2, img1_covers_img2, img1_left_of_img2, img1_right_of_img2, img1_below_img2);
{$pop}

	function _Composite(
		const imgname1, imgname2 : UTF8string; destname : UTF8string; action : ECompAction;
		useimg1palette : boolean = FALSE; ofsx : longint = 0; ofsy : longint = 0) : dword;
	// Checks if img1 and img2 are both in newGfxList. If yes, opens both images, combines them according to the action
	// value, and writes the result over destimg in newGfxList and graphicFiles, leaving img2 as is.
	// If destimg is empty, writes over img1. All image names must be in lowercase.
	// Returns 0 for success, or -1 if unsuccessful, or throws an exception.
	var loader : TFileLoader = NIL;
		newfile : pointer = NIL;
		image1, image2 : mcg_bitmap;
		newimg : array of byte = NIL;
		srcp, srcp2, destp : ^byte;
		file1, file2, destfile : TGraphicFile;
		i, j, k, bytesize : dword;
		txt : string[20];
	begin
		result := high(dword);
		Assert((imgname1 = lowercase(imgname1)) and (imgname2 = lowercase(imgname2)) and (destname = lowercase(destname)),
			strcat('not lowercase: %|%|%', [imgname1, imgname2, destname]));

		i := SeekNewGfx(imgname1); if i >= newGfxCount then exit;
		i := SeekNewGfx(imgname2); if i >= newGfxCount then exit;
		i := GetGraphicFile(upcase(imgname1)); if i = 0 then exit;
		file1 := graphicFiles[i];
		i := GetGraphicFile(upcase(imgname2)); if i = 0 then exit;
		file2 := graphicFiles[i];

		if destname = '' then begin
			destname := imgname1;
			destfile := file1;
		end
		else begin
			destfile := file1.Clone;
			with destfile do begin
				graphicName := upcase(destname);
				srcFilePath := PathCombine([ExtractFilePath(srcFilePath), destname + '.png']);
			end;
			AddOrReplaceGraphicFile(destfile);
		end;

		WriteStr(txt, action);
		decomp_logger(strcat('Compositing: % % % -> %', [imgname1, txt, imgname2, destname]));

		image1 := NIL; image2 := NIL;

		try try
			loader := TFileLoader.Open(file1.srcFilePath);
			try
				image1 := mcg_bitmap.FromPNG(loader.readp, loader.fullFileSize, [MCG_FLAG_FORCE8BPP]);
			finally
				if loader <> NIL then begin loader.Destroy; loader := NIL; end;
			end;

			loader := TFileLoader.Open(file2.srcFilePath);
			try
				image2 := mcg_bitmap.FromPNG(loader.readp, loader.fullFileSize, [MCG_FLAG_FORCE8BPP]);
			finally
				if loader <> NIL then begin loader.Destroy; loader := NIL; end;
			end;

			if (action in [img1_above_img2, img1_below_img2]) and (image1.stride <> image2.stride) then
				raise DecompException.Create('images not same width');
			if (action in [img1_left_of_img2, img1_right_of_img2]) and (image1.sizeYP <> image2.sizeYP) then
				raise DecompException.Create('images not same height');

			case action of

				img1_above_img2:
				begin
					i := image1.stride * image1.sizeYP;
					inc(image1.sizeYP, image2.sizeYP);
					setlength(image1.bitmap, image1.stride * image1.sizeYP);
					move(image2.bitmap[0], image1.bitmap[i], image2.stride * image2.sizeYP);
				end;

				img1_below_img2:
				begin
					i := image2.stride * image2.sizeYP;
					inc(image2.sizeYP, image1.sizeYP);
					setlength(image2.bitmap, image2.stride * image2.sizeYP);
					move(image1.bitmap[0], image2.bitmap[i], image1.stride * image1.sizeYP);
					image1.sizeYP := image2.sizeYP;
					setlength(image1.bitmap, 0); image1.bitmap := image2.bitmap; image2.bitmap := NIL;
				end;

				img1_left_of_img2:
				begin
					i := image1.stride + image2.stride;
					setlength(newimg, i * image1.sizeYP);
					srcp := @image1.bitmap[0];
					srcp2 := @image2.bitmap[0];
					destp := @newimg[0];
					for j := image1.sizeYP - 1 downto 0 do begin
						move(srcp^, destp^, image1.stride);
						inc(srcp, image1.stride);
						inc(destp, image1.stride);
						move(srcp2^, destp^, image2.stride);
						inc(srcp2, image2.stride);
						inc(destp, image2.stride);
					end;
					setlength(image1.bitmap, 0);
					image1.bitmap := newimg; newimg := NIL;
					inc(image1.sizeXP, image2.sizeXP);
					image1.stride := (image1.sizeXP * image1.bitDepth + 7) shr 3;
				end;

				img1_right_of_img2: raise DecompException.Create('not implemented yet');

				img1_covers_img2:
				begin
					inc(destfile.origOfsXP, ofsx);
					inc(destfile.origOfsYP, ofsy);
					if (destfile.origOfsXP < file2.origOfsXP)
					or (destfile.origOfsYP < file2.origOfsYP)
					or (destfile.origOfsXP + longint(image1.sizeXP)
						> file2.origOfsXP + longint(image2.sizeXP))
					or (destfile.origOfsYP + longint(image1.sizeYP)
						> file2.origOfsYP + longint(image2.sizeYP))
					then raise DecompException.Create('image1 not entirely inside image2');

					destp := @image2.bitmap[0]
						+ (destfile.origOfsYP - file2.origOfsYP) * longint(image2.stride)
						+ (destfile.origOfsXP - file2.origOfsXP);
					srcp := @image1.bitmap[0];

					for k := image1.sizeYP - 1 downto 0 do begin
						for j := image1.sizeXP - 1 downto 0 do begin
							case image1.palette[srcp^].a of
								0..$3F: ;
								$40..$BF: if (j xor k) and 1 = 1 then destp^ := srcp^; // dither
								$C0..$FF: destp^ := srcp^;
							end;
							inc(srcp); inc(destp);
						end;
						inc(destp, image2.stride - image1.stride);
					end;
					setlength(image1.bitmap, 0);
					image1.bitmap := image2.bitmap; image2.bitmap := NIL;
					image1.sizeXP := image2.sizeXP;
					image1.sizeYP := image2.sizeYP;
					image1.stride := image2.stride;
					destfile.origOfsXP := file2.origOfsXP;
					destfile.origOfsYP := file2.origOfsYP;
					destfile.origSizeXP := file2.origSizeXP;
					destfile.origSizeYP := file2.origSizeYP;
					destfile.origFrameHeightP := file2.origFrameHeightP;
				end;
			end;

			if NOT useimg1palette then image1.palette := image2.palette;

			// Save the result over the first original.
			image1.PackBitDepth(1);
			image1.MakePNG(newfile, bytesize); // build a PNG in newfile^
			SaveFile(destfile.srcFilePath, newfile, bytesize);
			result := 0;
		except
			on e : Exception do LogError(e.Message);
		end;
		finally
			if image1 <> NIL then begin image1.Destroy; image1 := NIL; end;
			if image2 <> NIL then begin image2.Destroy; image2 := NIL; end;
			if newfile <> NIL then begin freemem(newfile); newfile := NIL; end;
		end;
	end;

begin
	// The filenames shall be in lowercase since they'll be matched against newGfxList.
	case game^.g of
		gid.AkaiSuishou:
		begin
			_Composite('mk_01a','mk_01b','',img1_above_img2);
			_Composite('yi_01c','yi_01','',img1_covers_img2);
			_Composite('yi_01b','yi_01c','',img1_covers_img2);
			_Composite('yi_01a','yi_01b','',img1_covers_img2, TRUE);
		end;

		gid.AngelsC1:
		begin
			_Composite('gpp_0c','gpp_0a','',img1_below_img2);
			_Composite('t2_08','t2_09','',img1_above_img2);
			_Composite('t3_14','t3_15','',img1_above_img2);
			_Composite('t3_44','t3_45','',img1_above_img2);
			_Composite('t3_62','t3_63','',img1_above_img2);
		end;

		gid.AngelsC2:
		begin
			_Composite('o2_006','o2_007','',img1_above_img2);
			_Composite('o3_006','o3_007','',img1_above_img2);
			_Composite('t3_025','t3_026','',img1_left_of_img2);
			_Composite('t3_039','t3_040','',img1_above_img2);
			_Composite('t3_041','t3_040','',img1_above_img2);
			_Composite('t3_043','t3_044','',img1_left_of_img2);
			_Composite('t4_076','t4_075','',img1_above_img2);
		end;

		gid.Deep:
		begin
			_Composite('dh_s01_a','dh_s01_b','',img1_above_img2);
			_Composite('dh_s02_a','dh_s02_b','',img1_above_img2);
			_Composite('dh_s03_a','dh_s03_b','',img1_above_img2);
			_Composite('dh_s04_a','dh_s04_b','',img1_above_img2);
			_Composite('dh_s5j_a','dh_s5j_b','',img1_above_img2);
			_Composite('dh_s5k_a','dh_s5k_b','',img1_above_img2);
			_Composite('dh_s5m_a','dh_s5m_b','',img1_above_img2);
			_Composite('dh_s5s_a','dh_s5s_b','',img1_above_img2);
			_Composite('dh_s06_a','dh_s06_b','',img1_above_img2);
			_Composite('dh_s07_a','dh_s07_b','',img1_above_img2);
			_Composite('dh_s08_a','dh_s08_b','',img1_above_img2);
			_Composite('dh_s09_a','dh_s09_b','',img1_above_img2);
		end;

		gid.Desire98:
		begin
			_Composite('070_2','070_1','',img1_covers_img2, TRUE);
			_Composite('070_3','070_1','',img1_covers_img2, TRUE);
			_Composite('213a','213b','',img1_above_img2);
		end;

		gid.EveBE98:
		begin
			_Composite('006c','006','',img1_covers_img2, TRUE);
			_Composite('007b','007','',img1_covers_img2, TRUE);
			_Composite('007c','007','',img1_covers_img2, TRUE);
			_Composite('007d','007','',img1_covers_img2, TRUE);
			_Composite('007e','007','',img1_covers_img2, TRUE);
			_Composite('007f','007','',img1_covers_img2, TRUE);
			_Composite('009b','009','',img1_covers_img2, TRUE);
			_Composite('009c','009','',img1_covers_img2, TRUE);
			_Composite('009d','009','',img1_covers_img2, TRUE);
			_Composite('018b','018','',img1_covers_img2, TRUE);
		end;

		gid.Foreigner:
		_Composite('for04_2','for04_1','',img1_covers_img2);

		gid.FromH:
		begin
			// _Composite('fe_007g','fe_007','',img1_covers_img2); // looks better separately
			_Composite('fe_011g','fe_011','',img1_covers_img2);
			_Composite('fh_010g','fh_010','',img1_covers_img2);
			_Composite('fh_022g','fh_022','',img1_covers_img2);
			// _Composite('fh_029g','fh_029','',img1_covers_img2); // complex, check scripting first
			_Composite('ft_27','fb_007','',img1_covers_img2);
			_Composite('ft_28','fb_007','',img1_covers_img2);
		end;

		gid.Lilith:
		begin
			_Composite('106b','106a','',img1_covers_img2, TRUE);
			_Composite('111b','111a','',img1_covers_img2, TRUE);
			_Composite('113c','113b','',img1_covers_img2, TRUE);
			_Composite('113n','113c','',img1_covers_img2, TRUE);
			_Composite('114n','114','',img1_covers_img2, TRUE);
			_Composite('115n','115','',img1_covers_img2, TRUE);
			_Composite('116n','116','',img1_covers_img2, TRUE);
			_Composite('117n','117','',img1_covers_img2, TRUE);
			_Composite('118n','118','',img1_covers_img2, TRUE);
			_Composite('119b','119a','',img1_covers_img2, TRUE);
			_Composite('119n','119a','',img1_covers_img2, TRUE);
			_Composite('120n','120','',img1_covers_img2, TRUE);
			_Composite('121b','121a','',img1_covers_img2, TRUE);
			_Composite('121c','121a','',img1_covers_img2, TRUE);
			_Composite('121d','121a','',img1_covers_img2, TRUE);
			_Composite('122n','122','',img1_covers_img2, TRUE);
			_Composite('123n','123','',img1_covers_img2, TRUE);
			_Composite('125b','125','',img1_covers_img2, TRUE);
			_Composite('125n','125','',img1_covers_img2, TRUE);
			_Composite('126n','126','',img1_covers_img2, TRUE);
			_Composite('127n','127','',img1_covers_img2, TRUE);
			_Composite('127t','127','',img1_covers_img2, TRUE);
			_Composite('128n','128','',img1_covers_img2, TRUE);
			_Composite('133a','133','',img1_covers_img2, TRUE);
			_Composite('133b','133','',img1_covers_img2, TRUE);
			_Composite('133c','133','',img1_covers_img2, TRUE);
			_Composite('133n','133','',img1_covers_img2, TRUE);
			_Composite('161n','161','',img1_covers_img2, TRUE);
			_Composite('e03a','e03','',img1_covers_img2, TRUE);
			_Composite('e03b','e03a','',img1_covers_img2, TRUE);
			_Composite('e03c','e03b','',img1_covers_img2, TRUE);
			_Composite('e03d','e03c','',img1_covers_img2, TRUE);
			_Composite('e03e','e03d','',img1_covers_img2, TRUE);
			_Composite('e03f','e03e','',img1_covers_img2, TRUE);
		end;

		gid.Majokko:
		begin
			_Composite('opa','opb','',img1_above_img2);
			_Composite('ke_035_1','ke_035_2','',img1_above_img2);
			_Composite('ke_036_1','ke_036_2','',img1_above_img2);
			_Composite('ke_039_1','ke_039_2','',img1_above_img2);
			_Composite('ke_044_1','ke_044_2','',img1_above_img2);
			_Composite('ke_051_1','ke_051_2','',img1_above_img2);
			_Composite('ke_052_1','ke_052_2','',img1_above_img2);
			_Composite('ke_065_1','ke_065_2','',img1_above_img2);
			_Composite('ke_069_1','ke_069_2','',img1_above_img2);
			_Composite('ke_076_1','ke_076_2','',img1_above_img2);
		end;

		gid.Mayclub98, gid.Mayclub_w, gid.Mayclub_e:
		begin
			_Composite('a05b','a05a','',img1_covers_img2);
			_Composite('b11b','b11a','',img1_covers_img2);
			_Composite('b11f','b11a','',img1_covers_img2);
			_Composite('b17b','b17a','',img1_covers_img2);
			_Composite('b17c','b17b','',img1_covers_img2);
			_Composite('bg02','bg01','',img1_covers_img2);
			_Composite('c20b','c20a','',img1_covers_img2);
			_Composite('c20f','c20a','',img1_covers_img2);
			_Composite('c22b','c22a','',img1_covers_img2);
			_Composite('d27c','d27a','',img1_covers_img2);
			_Composite('d27f','d27a','',img1_covers_img2);
			_Composite('d27h','d27a','',img1_covers_img2);
			_Composite('d32b','d32a','',img1_covers_img2);
			_Composite('d33b','d33a','',img1_covers_img2);
			_Composite('e35b','e35a','',img1_covers_img2);
			_Composite('e35e','e35a','',img1_covers_img2);
			_Composite('e35g','e35a','',img1_covers_img2);
			_Composite('e38b','e38a','',img1_covers_img2);
			_Composite('g41b','g41a','',img1_covers_img2);
			_Composite('g41c','g41a','',img1_covers_img2);
			_Composite('g41e','g41a','',img1_covers_img2);
			_Composite('g41g','g41a','',img1_covers_img2);
			_Composite('g41h','g41a','',img1_covers_img2);
			_Composite('g43a','g43z','',img1_covers_img2);
			_Composite('g43b','g43z','',img1_covers_img2);
			_Composite('g43c','g43z','',img1_covers_img2);
			_Composite('h47b','h47a','',img1_covers_img2);
			_Composite('h47c','h47a','',img1_covers_img2);
			_Composite('h47g','h47a','',img1_covers_img2);
			_Composite('h49b','h49a','',img1_covers_img2);
			_Composite('h50b','h50a','',img1_covers_img2);
			_Composite('h50c','h50b','',img1_covers_img2);
			_Composite('i54b','i54a','',img1_covers_img2);
			_Composite('i54c','i54a','',img1_covers_img2);
			_Composite('i54d','i54a','',img1_covers_img2);
			_Composite('i54e','i54a','',img1_covers_img2);
			_Composite('i54f','i54a','',img1_covers_img2);
			_Composite('i54h','i54a','',img1_covers_img2);
			_Composite('i55b','i55a','',img1_covers_img2);
			_Composite('z64n','z64','',img1_covers_img2, TRUE);
			_Composite('z65n','z65','',img1_covers_img2, TRUE);
			_Composite('z65y','z65n','',img1_covers_img2, TRUE);
			_Composite('z68n','z68','',img1_covers_img2, TRUE);
			_Composite('z69n','z69','',img1_covers_img2, TRUE);
			_Composite('z70n','z70','',img1_covers_img2, TRUE);
		end;

		gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
		begin
			_Composite('01n','001','',img1_covers_img2, TRUE);
			_Composite('02n','002','',img1_covers_img2, TRUE);
			_Composite('03n','003','',img1_covers_img2, TRUE);
			_Composite('04n','004','',img1_covers_img2, TRUE);
			_Composite('05n','005','',img1_covers_img2, TRUE);
			_Composite('06n','006','',img1_covers_img2, TRUE);
			_Composite('07b','07a','',img1_covers_img2);
			_Composite('07n','07b','07bn',img1_covers_img2, TRUE);
			_Composite('07n','07a','',img1_covers_img2, TRUE);
			_Composite('08n','008','',img1_covers_img2, TRUE);
			_Composite('09n','009','',img1_covers_img2, TRUE);
			_Composite('10b','10a','',img1_covers_img2);
			_Composite('10n','10a','',img1_covers_img2, TRUE);
			_Composite('11n','011','',img1_covers_img2, TRUE);
			_Composite('12b','012','',img1_covers_img2);
			_Composite('12n','12b','12bn',img1_covers_img2, TRUE);
			_Composite('12n','012','',img1_covers_img2, TRUE);
			_Composite('13n','013','',img1_covers_img2, TRUE);
			_Composite('14n','014','',img1_covers_img2, TRUE);
			_Composite('15n','015','',img1_covers_img2, TRUE);
			_Composite('16b','16a','',img1_covers_img2);
			_Composite('17b','17c','',img1_covers_img2);
			_Composite('17a','17b','',img1_covers_img2);
			_Composite('25b','25a','',img1_covers_img2);
			_Composite('25c','25a','',img1_covers_img2);
			_Composite('25e','25a','',img1_covers_img2);
			_Composite('25g','25a','',img1_covers_img2);
			_Composite('2c1','2a1','',img1_covers_img2);
			_Composite('2c2','2a2','',img1_covers_img2);
			_Composite('2d1','2a1','',img1_covers_img2);
			_Composite('2d2','2a2','',img1_covers_img2);
			_Composite('37b','37a','',img1_covers_img2);
			_Composite('37c','37a','',img1_covers_img2);
			_Composite('43b','43a','',img1_covers_img2);
			_Composite('43c','43a','',img1_covers_img2);
			_Composite('43e','43d','',img1_covers_img2);
			_Composite('49n','049','',img1_covers_img2, TRUE);
			_Composite('52b','52a','',img1_covers_img2);
			_Composite('52c','52a','',img1_covers_img2);
			_Composite('52d','52a','',img1_covers_img2);
			_Composite('52e','52a','',img1_covers_img2);
			_Composite('54b','54a','',img1_covers_img2);
			_Composite('58b','58a','',img1_covers_img2);
			_Composite('60a','60b','',img1_covers_img2);
			_Composite('60c','60a','',img1_covers_img2);
			_Composite('67b','67a','',img1_covers_img2);
			_Composite('67c','67b','',img1_covers_img2);
			_Composite('79b','79a','',img1_covers_img2);
			_Composite('79c','79a','',img1_covers_img2);
			_Composite('79d','79a','',img1_covers_img2);
			_Composite('7b2','7a2','',img1_covers_img2);
			_Composite('7c2','7a2','',img1_covers_img2);
			_Composite('84b','84a','',img1_covers_img2);
			_Composite('84d','84a','',img1_covers_img2);
			_Composite('86b','86a','',img1_covers_img2);
			_Composite('86c','86b','',img1_covers_img2);
			_Composite('86d','86c','',img1_covers_img2);
			_Composite('96b','96a','',img1_covers_img2);
			if game^.g = gid.Nocturn98 then begin
				_Composite('30c','30b','',img1_covers_img2);
				_Composite('30d','30c','',img1_covers_img2);
			end;
		end;

		gid.Setsujuu:
		begin
			_Composite('sh_05s','sh_05','',img1_covers_img2);
			_Composite('sh_09s','sh_09','',img1_covers_img2);
			_Composite('sh_30s','sh_30','',img1_covers_img2);
			_Composite('sh_43s','sh_43','',img1_covers_img2);
			_Composite('sh_48s','sh_48','',img1_covers_img2);
			_Composite('sh_53s','sh_53','',img1_covers_img2);
			_Composite('sh_58s','sh_58','',img1_covers_img2);
			_Composite('sh_63s','sh_63','',img1_covers_img2);
			_Composite('sh_68s','sh_68','',img1_covers_img2);
		end;

		gid.Tasogare:
		begin
			_Composite('yb_012g','yb_012','',img1_covers_img2, TRUE);
		end;

		gid.Transfer98:
		begin
			_Composite('0001_a','0001_b','',img1_above_img2);
			_Composite('th_019','th_020','',img1_above_img2);
			_Composite('th_035','th_036','',img1_above_img2);
			_Composite('th_041','th_042','',img1_left_of_img2);
			_Composite('th_047','th_048','',img1_above_img2);
			_Composite('th_055','th_056','',img1_above_img2);
			_Composite('th_063','th_064','',img1_above_img2);
			_Composite('th_069','th_070','',img1_above_img2);
			_Composite('th_075','th_076','',img1_above_img2);
			_Composite('th_085','th_086','',img1_above_img2);
			_Composite('th_091','th_092','',img1_above_img2);
			_Composite('th_098','th_099','',img1_above_img2);
			_Composite('th_103','th_104','',img1_above_img2);
			_Composite('th_109','th_110','',img1_left_of_img2);
			_Composite('th_127','th_128','',img1_above_img2);
			_Composite('th_133','th_134','',img1_above_img2);
		end;

		gid.TrueLove_e:
		begin
			_Composite('ag030u','ag030d','',img1_above_img2);
			_Composite('ag090mp','ag090m','',img1_covers_img2, FALSE, 288, 192); // these lack embedded offsets
			_Composite('bg110mp','bg110m','',img1_covers_img2, FALSE, 216, 176);
			_Composite('cg090ap','cg090m','',img1_covers_img2, FALSE, 208, 96);
			_Composite('dg110ap','dg110m','',img1_covers_img2, FALSE, 200, 120);
			_Composite('eg100ap','eg100','',img1_covers_img2, FALSE, 232, 136);
			_Composite('fg080bp','fg080m','',img1_covers_img2, FALSE, 112, 32);
			_Composite('gg080ap','gg080','',img1_covers_img2, FALSE, 320, 200);
			_Composite('hg140mp','hg140mb','',img1_covers_img2, FALSE, 208, 0);
			_Composite('hg140mxp','hg140mb','',img1_covers_img2, FALSE, 208, 0);
			_Composite('ig040u','ig040d','',img1_above_img2);
			_Composite('ig091','ig090','',img1_covers_img2);
		end;

		gid.Ukiuki:
		begin
			_Composite('evf_26a','evf_26b','',img1_above_img2);
			_Composite('evf_34a','evf_34b','',img1_above_img2);
		end;

		gid.Vanish:
		begin
			_Composite('vop_002','vop_001','',img1_below_img2);
		end;
	end;
end;

// ------------------------------------------------------------------

procedure CropImage(var image : mcg_bitmap; var metadata : TGraphicFile; left, right, top, bottom : dword);
// Crops the given number of pixels from each edge, updates image metadata accordingly.
begin
	Assert(left + right <= image.sizeXP);
	Assert(top + bottom <= image.sizeYP);
	Assert(metadata.frameCount <= 1);
	if (left or right or top or bottom) = 0 then exit;

	with metadata do begin
		if decomp_param.verbose then decomp_logger(strcat('crop from %x% @ %,% by L=% R=% T=% B=%; ',
			[origSizeXP, origSizeYP, origOfsXP, origOfsYP, left, right, top, bottom]));

		image.Crop(left, right, top, bottom);

		inc(origOfsXP, longint(left));
		inc(origOfsYP, longint(top));
		origSizeXP := image.sizeXP;
		origSizeYP := image.sizeYP;
		origFrameHeightP := origSizeYP;
	end;
end;

procedure CropVoidBorders32(image : mcg_bitmap; metadata : TGraphicFile);
// If an image has plenty of totally transparent space on any side, it can be cropped out, but for a single-pixel wide
// transparent border. This is for BGRA images.
var clipleft, clipright, cliptop, clipbottom : dword;
	i : dword;
	srcp, endp : ^byte;
begin
	Assert(image.bitmapFormat = MCG_FORMAT_BGRA);
	Assert(image.bitDepth = 32);
	cliptop := 0; clipbottom := 0;
	with metadata do with image do begin

		endp := @bitmap[0] + stride * origSizeYP;
		// Bottom: check for transparent rows, scan pixels from end of image until a non-transparent is found.
		srcp := endp - 1;
		while (srcp > @bitmap[0]) and (srcp^ = 0) do dec(srcp, 4);
		// Calculate the total full rows of transparent pixels.
		i := endp - srcp;
		clipbottom := i div stride;

		// Top: check for transparent rows, scan pixels from start of image until a non-transparent is found.
		srcp := @bitmap[3];
		while (srcp < endp) and (srcp^ = 0) do inc(srcp, 4);
		// Calculate the total full rows of transparent pixels.
		i := srcp - @bitmap[0];
		cliptop := i div stride;

		// Is there anything left of the image?
		if cliptop + clipbottom >= origSizeYP then begin
			origSizeXP := 1;
			origSizeYP := 1;
			sizeXP := 1;
			sizeYP := 1;
			origFrameHeightP := 1;
			stride := 4;
			exit;
		end;

		// Left: check for transparent columns...
		clipleft := 0; i := 0;
		repeat
			if i = 0 then begin
				// new column
				srcp := @bitmap[3] + cliptop * stride + clipleft;
				i := origSizeYP - cliptop - clipbottom;
				inc(clipleft, 4);
			end;
			dec(i);
			if srcp >= endp then break;
			if srcp^ <> 0 then break;
			inc(srcp, stride);
		until FALSE;
		dec(clipleft, 4);

		// Right: check for transparent columns...
		clipright := 1; i := 0;
		repeat
			if i = 0 then begin
				// new column
				srcp := endp - clipbottom * stride - clipright;
				i := origSizeYP - cliptop - clipbottom;
				inc(clipright, 4);
			end;
			dec(i);
			if srcp < @bitmap[0] then break;
			if srcp^ <> 0 then break;
			dec(srcp, stride);
		until FALSE;
		dec(clipright, 5);

		// Leave single-byte transparent borders even if cropping the rest.
		clipleft := clipleft shr 2;
		if clipleft > 0 then dec(clipleft);
		clipright := clipright shr 2;
		if clipright > 0 then dec(clipright);
		if cliptop > 0 then dec(cliptop);
		if clipbottom > 0 then dec(clipbottom);

		if (clipleft or clipright or cliptop or clipbottom) = 0 then exit;

		CropImage(image, metadata, clipleft, clipright, cliptop, clipbottom);
	end;
end;

procedure CropVoidBorders(image : mcg_bitmap; metadata : TGraphicFile; transparentindex : dword);
// If an image has plenty of totally transparent space on any side, it can be cropped out, but for a single-pixel wide
// transparent border. This is for indexed-color images.
var clipleft, clipright, cliptop, clipbottom : dword;
	i : dword;
	srcp, endp : ^byte;
	transparentbyte : byte;
begin
	Assert(image.bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA]);
	Assert(image.bitDepth in [4,8]);
	if transparentindex >= dword(length(image.palette)) then exit;
	cliptop := 0; clipbottom := 0;
	with metadata do with image do begin

		transparentbyte := transparentindex;
		if bitDepth = 4 then transparentbyte := transparentbyte * 17;

		endp := @bitmap[0] + stride * origSizeYP;
		// Bottom: check for transparent rows, scan pixels from end of image until a non-transparent is found.
		srcp := endp - 1;
		while (srcp > @bitmap[0]) and (srcp^ = transparentbyte) do dec(srcp);
		// Calculate the total full rows of transparent pixels.
		i := endp - srcp;
		clipbottom := i div stride;

		// Top: check for transparent rows, scan pixels from start of image until a non-transparent is found.
		srcp := @bitmap[0];
		while (srcp < endp) and (srcp^ = transparentbyte) do inc(srcp);
		// Calculate the total full rows of transparent pixels.
		i := srcp - @bitmap[0];
		cliptop := i div stride;

		// Is there anything left of the image?
		if cliptop + clipbottom >= origSizeYP then begin
			origSizeXP := 1;
			origSizeYP := 1;
			sizeXP := 1;
			sizeYP := 1;
			origFrameHeightP := 1;
			stride := (image.bitDepth + 7) shr 3;
			exit;
		end;

		// Left: check for transparent columns...
		clipleft := 0; i := 0;
		repeat
			if i = 0 then begin
				// new column
				srcp := @bitmap[0] + cliptop * stride + clipleft;
				i := origSizeYP - cliptop - clipbottom;
				inc(clipleft);
			end;
			dec(i);
			if srcp >= endp then break;
			if srcp^ <> transparentbyte then break;
			inc(srcp, stride);
		until FALSE;
		dec(clipleft);

		// Right: check for transparent columns...
		clipright := 0; i := 0;
		repeat
			if i = 0 then begin
				// new column
				inc(clipright);
				srcp := endp - clipbottom * stride - clipright;
				i := origSizeYP - cliptop - clipbottom;
			end;
			dec(i);
			if srcp < @bitmap[0] then break;
			if srcp^ <> transparentbyte then break;
			dec(srcp, stride);
		until FALSE;
		dec(clipright);

		// Leave single-byte transparent borders even if cropping the rest, if border indeed transparent.
		if palette[transparentindex].a <> $FF then begin
			if clipleft > 0 then dec(clipleft);
			if clipright > 0 then dec(clipright);
			if cliptop > 0 then dec(cliptop);
			if clipbottom > 0 then dec(clipbottom);
		end;

		if (clipleft or clipright or cliptop or clipbottom) = 0 then exit;

		if bitDepth = 4 then begin
			clipleft := clipleft * 2;
			clipright := clipright * 2;
		end;
		CropImage(image, metadata, clipleft, clipright, cliptop, clipbottom);
	end;
end;

procedure FillFlatAlpha(image : mcg_bitmap; rgb : RGBAquad);
// Replaces every pixel of the matching color in a BGR or BGRX image with a fully transparent pixel.
var readp, endp : pointer;
begin
	Assert(image.bitmapFormat in [MCG_FORMAT_BGR, MCG_FORMAT_BGRX]);
	if image.bitmapFormat = MCG_FORMAT_BGR then image.Force32bpp;
	rgb.a := $FF;

	readp := @image.bitmap[0];
	endp := readp + image.sizeXP * image.sizeYP * 4;
	while readp < endp do begin
		if dword(readp^) = dword(rgb) then byte((readp + 3)^) := 0; // keep color information, only set alpha
		inc(readp, 4);
	end;
	image.bitmapFormat := MCG_FORMAT_BGRA;
end;

procedure ImportAlphaChannel(image : mcg_bitmap; metadata : TGraphicFile; const alphaimage : UTF8string);
var srcp, destp, endp : ^byte;
begin
	LoadPostProcImg(metadata, alphaimage);
	if (postProcImg.sizeXP <> image.sizeXP) or (postProcImg.sizeYP <> image.sizeYP) then
		raise DecompException.Create('images not same size');
	if postProcImg.bitmapFormat <> MCG_FORMAT_INDEXED then raise DecompException.Create('alpha image not indexed');
	Assert(postProcImg.bitDepth = 8);

	if image.bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA] then image.ExpandIndexed(TRUE);
	if image.bitmapFormat = MCG_FORMAT_BGR then image.Force32bpp;
	image.bitmapFormat := MCG_FORMAT_BGRA;

	srcp := @postProcImg.bitmap[0];
	destp := @image.bitmap[0] + 3;
	endp := srcp + image.sizeXP * image.sizeYP;
	while srcp < endp do begin
		destp^ := srcp^;
		inc(srcp);
		inc(destp, 4);
	end;
end;

function ExplodeBits(input : byte) : dword;
// Expands each bit in the input byte to use 4bpp.
// bits 87654321 -> MSB ...8...7 ...6...5 ...4...3 ...2...1 = LSB ...2...1 ...4...3 ...6...5 ...8...7
begin
	// MSB
	{result := (input and 1) or ((input and 2) shl 3)
		or ((input and 4) shl 6) or ((input and 8) shl 9)
		or ((input and 16) shl 12) or ((input and 32) shl 15)
		or ((input and 64) shl 18) or ((input and 128) shl 21);}
	// LSB
	result := ((input and 1) shl 24) or ((input and 2) shl 27)
		or ((input and 4) shl 14) or ((input and 8) shl 17)
		or ((input and 16) shl 4) or ((input and 32) shl 7)
		or ((input and 64) shr 6) or byte((input and 128) shr 3);
end;

{$ifdef bonk}
function ExplodeBitsW(input : byte) : word;
// bits 87654321 -> MSB .8.7 .6.5 .4.3 .2.1 = LSB .2.1 .4.3 .6.5 .8.7
begin
	result := ((input and 1) shl 12) or ((input and 2) shl 13)
		or ((input and 4) shl 6) or ((input and 8) shl 7)
		or (input and 16) or ((input and 32) shl 1)
		or ((input and 64) shr 6) or ((input and 128) shr 5);
end;
{$endif}

function ExplodeBitsW(input : byte) : word;
// bits 4321 -> MSB ...4...3 ...2...1 = LSB ...2...1 ...4...3
begin
	// MSB
	{result := (input and 1) + ((input and 2) shl 3)
		+ ((input and 4) shl 6) + ((input and 8) shl 9);}
	// LSB
	result := ((input and 1) shl 8) + ((input and 2) shl 11)
		+ ((input and 4) shr 2) + ((input and 8) shl 1);
end;

function ExplodeBitsQ(input : byte) : qword;
// Expands each bit in the input byte to use 8bpp. The nibble flip isn't at play here.
// bits 87654321 -> MSB tbd = LSB .......1 .......2 .......3 .......4 .......5 .......6 .......7 .......8
begin
	result := (qword(input and 1) shl 56) or (qword(input and 2) shl 47)
		or (qword(input and 4) shl 38) or (qword(input and 8) shl 29)
		or (qword(input and 16) shl 20) or (qword(input and 32) shl 11)
		or (qword(input and 64) shl 2) or (qword(input and 128) shr 7);
end;

procedure SetupPalette(const img : mcg_bitmap; colors : byte);
var i : byte;
begin
	with img do begin
		palette := NIL;
		setlength(palette, colors);
		palette[0].FromRGBA($FF);
		if colors < 16 then i := 0 else i := 8;
		palette[i + 1].FromRGBA($0000FFFF);
		palette[i + 2].FromRGBA($FF0000FF);
		palette[i + 3].FromRGBA($FF00FFFF);
		palette[i + 4].FromRGBA($00FF00FF);
		palette[i + 5].FromRGBA($00FFFFFF);
		palette[i + 6].FromRGBA($FFFF00FF);
		palette[i + 7].FromRGBA($FFFFFFFF);
		if colors in [9, 17] then dword(palette[high(palette)]) := 0; // transparent
		if colors >= 16 then begin
			for i := 1 to 6 do begin
				palette[i].b := palette[i + 8].b shr 1;
				palette[i].g := palette[i + 8].g shr 1;
				palette[i].r := palette[i + 8].r shr 1;
				palette[i].a := $FF;
			end;
			palette[7].FromRGBA($CCCCCCFF);
			palette[8].FromRGBA($888888FF);
		end;
	end;
end;

procedure ForceUniquePalette(const img : mcg_bitmap);
// For any duplicate palette entries, flips bits at the low end to force each palette entry to be unique.
// Although unnecessary when using extracted graphics with SuperSakura, some other image editors very resolutely
// discard unused colors or merge or confuse duplicate colors. A user will have a better image editing experience if
// all colors are unique.
// Always leaves color 0 unchanged, and the highest color, unless it's the same as color 0. Ignores alpha.
var rgbmask, attempt : dword;
	i, j : byte;
	unique : boolean;
begin
	if NOT (byte(length(img.palette)) in [2..32]) then exit;
	rgbmask := LEtoN($FFFFFF); // alpha is the 4th byte
	with img do begin
		j := high(palette);
		if dword(palette[j]) and rgbmask <> dword(palette[0]) and rgbmask then dec(j);
		for i := j downto 1 do begin
			attempt := 0;
			repeat
				inc(attempt);
				unique := TRUE;
				for j := high(palette) downto 0 do if i <> j then
					if dword(palette[i]) and rgbmask = dword(palette[j]) and rgbmask then begin
						unique := FALSE;
						if attempt and 1 <> 0 then
							palette[i].b := palette[i].b xor (attempt shr 2 + 1)
						else if attempt and 2 <> 0 then
							palette[i].g := palette[i].g xor (attempt shr 2 + 1)
						else
							palette[i].r := palette[i].r xor (attempt shr 2 + 1);
						break;
					end;
			until unique;
			if (attempt > 1) and (decomp_param.verbose) then
				decomp_logger('force unique pal ' + strdec(i) + ': ' + strhex(dword(palette[i]), 8));
		end;
	end;
end;

function FindColorInImage(const image : mcg_bitmap; color : byte) : longint;
// Returns the byte offset of the given color value in the image bitmap, or -1 for not found.
begin
	Assert(image <> NIL);
	Assert(length(image.bitmap) <> 0);
	Assert(image.bitDepth = 8);
	Assert(image.bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA]);
	result := IndexByte(image.bitmap[0], length(image.bitmap), color);
end;

function SelectTransparentIndex(
	imagetype : EConverterName; var metadata : TGraphicFile; var image : mcg_bitmap; gameid : gid) : dword;

	function _MatchCorners(color : dword) : byte;
	// Returns number of image corners (0..4) that have a dwordful of this color.
	var d : dword;
	begin
		result := 0;
		with image do begin
			if (stride < 8) or (sizeYP < 2) then exit;
			case bitDepth of
				4: d := color * $11111111;
				8: d := color * $01010101;
				else begin
					LogError('xparency bad bitdepth ' + strdec(bitDepth));
					exit;
				end;
			end;
			if dword((@bitmap[0])^) = d then inc(result);
			if dword((@bitmap[0] + stride - 4)^) = d then inc(result);
			if dword((@bitmap[0] + length(bitmap) - 4)^) = d then inc(result);
			if dword((@bitmap[0] + length(bitmap) - stride)^) = d then inc(result);
		end;
	end;

begin
	result := high(dword);
	// Common files.
	case imagetype of
		CN_GPC: if (metadata.frameCount > 1) or (_MatchCorners(0) > 2) then result := 0;
		CN_PRS: with image do if (bitmap[0] = 0) or (bitmap[high(bitmap)] = 0) then result := 0;
	end;

	with metadata do case gameid of
		gid.AngelsC1, gid.AngelsC2, gid.Deep, gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Maririn,
		gid.Runaway, gid.Runaway98, gid.Sakura, gid.Sakura98, gid.SanShimai, gid.SanShimai98, gid.Setsujuu,
		gid.Tasogare, gid.Transfer98, gid.Vanish:
		begin
			// Anything animated...
			if (seqLen <> 0)
			// Common names...
			or (graphicName = 'MS_CUR') or (graphicName = 'TIARE_P')
			or (graphicName = 'MARU1') or (graphicName = 'MARU2') or (graphicName = 'MARU3')
			or (graphicName = 'TB_008') // completely transparent graphic!
			then
				result := 8
			else
				if graphicName = 'PUSH2' then result := $F;
		end;

		gid.GaoGao1, gid.GaoGao2, gid.GaoGao3, gid.GaoGao4, gid.Lilith, gid.Mayclub_e, gid.Mayclub98,
		gid.Nocturn_e, gid.Nocturn98:
		result := 0; // nearly always index 0 in these games
	end;

	// Game-specific transparency.
	with metadata do case gameid of
		gid.AmeKise:
		case graphicName of
			'AME_KA', 'AME_SI', 'AME_YJ', 'AME_YK': result := BEtoN(dword($FF));
			else if copy(graphicName, 1, 2) = 'ST' then result := BEtoN(dword($FF));
		end;

		gid.Amy98:
		if graphicName = 'PASI2' then
			result := 0
		else
		if (graphicName = 'WAKU2') or (graphicName[1] = 'K') or (copy(graphicName, 1, 3) = 'TO_')
		or (copy(graphicName, 1, 2) = 'HI') or (copy(graphicName, 1, 2) = 'RI') or (copy(graphicName, 1, 2) = 'SU')
		then result := 8;

		gid.AngelCrisis:
		if (graphicName = 'A_C_ROG') or (graphicName = 'ENJTIP') or (graphicName[3] = 'T')
		then result := 0;

		gid.AngelsC2:
		if (graphicName = 'O4_005') or (graphicName = 'T2_LOGO') or (graphicName = 'T3_LOGO') then result := 8;

		gid.DeadBrain1: with image do if (bitmap[0] = 0) or (bitmap[high(bitmap)] = 0) then result := 0;

		gid.Deep:
		if (copy(graphicName, 1, 3) = 'DC_') and (graphicName[5] = 'T') or (copy(graphicName, 1, 3) = 'DT_')
		then result := 8;

		gid.Desire98:
		if (copy(graphicName, 1, 4) = 'TINA') or (copy(graphicName, 1, 3) = 'REI')
		or (copy(graphicName, 1, 3) = 'MUL') or (copy(graphicName, 1, 3) = 'MAK')
		or (graphicName[1] in ['A','E','G','K','S'])
		or (graphicName = '520B')
		or (graphicName = '070_2') or (graphicName = '070_3')
		then result := 8;

		gid.Eden:
		if (graphicName = 'EE_070') or (graphicName = 'EE_084') or (graphicName = 'EE_087')
		or (graphicName = 'OP_TT') or (copy(graphicName, 1, 2) = 'ET')
		then result := 8;

		gid.EtsuGaku98:
		if (graphicName = '040C') or (graphicName = '050C') then result := 8;

		gid.EveBE98:
		if (graphicName[1] in ['B','S'])
		or (graphicName = '3010B') or (graphicName = '1225') or (graphicName = '0680B') or (graphicName = '0565B')
		or (graphicName = '0290B') or (graphicName = '0140C') or (graphicName = '035B') or (graphicName = '030B')
		or (graphicName = '018B') or (graphicName = '018C') or (graphicName = '0005H') or (graphicName = '0005I')
		or (copy(graphicName, 1, 4) = '2010') and (length(graphicName) > 4)
		or (copy(graphicName, 1, 4) = '1035')
		or (copy(graphicName, 1, 4) = '0490') and (length(graphicName) > 4)
		or (copy(graphicName, 1, 4) = '0450') and (length(graphicName) > 4)
		or (copy(graphicName, 1, 3) = '034') and (length(graphicName) > 3) and (graphicName[4] in ['B'..'D'])
		or (copy(graphicName, 1, 3) = '029') and (length(graphicName) > 3) and (graphicName[4] in ['B'..'C'])
		or (copy(graphicName, 1, 3) = '028') and (length(graphicName) > 3) and (graphicName[4] in ['B'..'E'])
		or (copy(graphicName, 1, 3) = '014') and (length(graphicName) > 3) and (graphicName[4] in ['B'..'D'])
		or (copy(graphicName, 1, 3) = '011') and (length(graphicName) > 3) and (graphicName[4] in ['B'..'H'])
		or (copy(graphicName, 1, 3) = '010') and (length(graphicName) > 3) and (graphicName[4] in ['B'..'D'])
		or (copy(graphicName, 1, 3) = '009') and (length(graphicName) > 3) and (graphicName[4] in ['B'..'D'])
		or (copy(graphicName, 1, 3) = '007') and (length(graphicName) > 3) and (graphicName[4] in ['B'..'G'])
		or (copy(graphicName, 1, 3) = '006') and (length(graphicName) > 3) and (graphicName[4] in ['B'..'C'])
		or (copy(graphicName, 1, 3) = '001') and (length(graphicName) > 3) and (graphicName[4] in ['B'..'C'])
		then result := 8;

		gid.Foreigner: if copy(graphicName, 1, 3) = 'MON' then result := 10;

		gid.FromH:
		if (copy(graphicName, 1, 3) = 'FT_')
		or (graphicName = 'FE_007G') or (graphicName = 'FE_011G') or (graphicName = 'FH_010G')
		or (graphicName = 'FH_022G') or (graphicName = 'FH_029G') or (graphicName = 'FH_029G2')
		or (graphicName = 'FH_029G3') or (graphicName = 'FIN')
		then result := 8
		else if copy(graphicName, 1, 5) = 'OP_00' then result := high(dword);

		gid.Hohoemi:
		if (copy(graphicName, 3, 2) = '_S') or (copy(graphicName, 3, 2) = '_U') or (graphicName = 'OP_000_')
		then result := 8;

		gid.Kadowakashi:
		if (copy(graphicName, 1, 5) = 'CV10P') and (graphicName[6] <> '1') then result := BEtoN(dword($00FF00FF));

		gid.KoukanNikki1, gid.KoukanNikki2, gid.MaDoll: if _MatchCorners($E) > 2 then result := $E;

		gid.Lakers1: if dword(image.palette[7]) = $FF888899 then result := 7;
		gid.Lakers2:
		case graphicName of
			'CHN_001','SG_002','SG_003','SG_004','SG_005','VG_016','VG_070','VG_083': ;
			'L2OP_ANM.1','L2OP_ANM.2','L2OP_ANM.3': result := 14;

			else if copy(graphicName, 1, 2) <> 'L2' then
			if (dword(image.palette[14]) and $CCFFFF = $44AAAA) or (dword(image.palette[14]) = $FF7788BB)
			or (copy(graphicName, 1, 3) = 'KAO') or (graphicName = 'VG_119') or (graphicName = 'BTL_029')
			then result := 14;
		end;

		gid.Lilith:
		case graphicName of
			'069', '076', '088', 'M59', 'M60': result := high(dword);
		end;

		gid.MahaBarata, gid.Skirmish, gid.DualSoul98, gid.InmaSeiGa98, gid.MajoGari98, gid.Kyouhaku98,
		gid.RuriiroNoYuki: if _MatchCorners(11) > 2 then result := 11;

		gid.Majokko:
		if (copy(graphicName, 1, 2) = 'MT') or (copy(graphicName, 1, 2) = 'NO') or (graphicName = 'TAITOL')
		or (graphicName = 'SAKURA')
		then result := 8;

		gid.Mayclub98, gid.Mayclub_w, gid.Mayclub_e:
		if (copy(graphicName, 1, 3) = 'Z01') or (graphicName[1] in ['0'..'9']) then result := high(dword);

		gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
		case graphicName of
			// Everything is potentially transparent except these.
			'023', '031', '088', '101', 'TTL': result := high(dword);
			else if copy(graphicName, 1, 5) = 'END_1' then result := high(dword);
		end;

		gid.Quintia2:
		if (copy(graphicName, 1, 3) = 'BA0')
		or (copy(graphicName, 1, 3) = 'EDK')
		or (copy(graphicName, 1, 2) = 'K-')
		then result := 7;

		gid.MHard98, gid.RoseBlood98:
		if copy(graphicName, 1, 2) = 'B_' then result := 10;

		gid.MHard_w, gid.RoseBlood_w:
		if copy(graphicName, 1, 2) = 'B_' then result := 0;

		gid.Runaway, gid.Runaway98:
		if (copy(graphicName, 1, 3) = 'M8_') or (graphicName = 'OP_013A0') or (graphicName = 'MB07A')
		or (graphicName = 'MB09_1A')
		then result := 8;

		gid.Sakura, gid.Sakura98:
		if (copy(graphicName, 1, 2) = 'CT') or (graphicName = 'AE_007G') or (graphicName = 'HANKO')
		then result := 8
		else if graphicName = 'SAKURA' then result := $F;

		gid.Sanatorium:
		if (graphicName = 'KEITAI')
		or ((graphicName[1] = 'T') and (graphicName[2] <> '_'))
		then result := BEtoN(dword($FF)); // full black is transparent

		gid.SanShimai, gid.SanShimai98:
		if copy(graphicName, 1, 2) = 'ST' then result := 8;

		gid.Setsujuu:
		if (copy(graphicName, 1, 3) = 'ST_') or (graphicName = 'YUKI')
		or (graphicName[length(graphicName)] = 'S')
		and (byte(valx(copy(graphicName, 4, 2))) in [5, 9, 30, 43, 48, 53, 58, 63, 68])
		then result := 8;

		gid.Sensei2_e:
		if (graphicName[1] = 'T') and (graphicName[2] <> 'X')
		and ((graphicName[2] <> 'I') or (graphicName[3] <> 'T'))
		then result := BEtoN(dword($FF)); // full black is transparent

		gid.Slayers:
		if dword(image.palette[2]) = LEtoN(dword($FF00FF00)) then result := 2; // max green index 2 is transparent

		gid.Tasogare:
		if (copy(graphicName, 1, 3) = 'MAP') or (copy(graphicName, 1, 3) = 'YO_')
		or (copy(graphicName, 1, 3) = 'YT_') and (graphicName[length(graphicName)] <> 'E')
		or (copy(graphicName, 1, 6) = 'YE_004') and (length(graphicName) = 7)
		or (graphicName = 'YE_006G') or (graphicName = 'YE_028')
		or (graphicName = 'YE_029') or (graphicName = 'YB_012G')
		then result := 8;

		gid.Transfer98:
		if (copy(graphicName, 1, 3) = 'TT_') or (graphicName = 'TI_135A') or (graphicName = 'ROGOL2')
		then result := 8
		else if graphicName = 'TB_149A' then result := 7;

		gid.TrueLove_e:
		if (copy(graphicName, 1, 2) = 'NG')
		or ((copy(graphicName, 2, 4) = 'G010') and ((length(graphicName) = 5) or (graphicName[5] <> 'A')))
		then result := 14
		else case graphicName of
			'AG020M','AG030','DG011','EG030','FG020','LG020': result := 14;
			'HG140MXP': result := 0;
			'IG091': result := 3;
		end;

		gid.Vanish:
		if (copy(graphicName, 1, 3) = 'MC_') or (copy(graphicName, 1, 3) = 'MJ_')
		or (copy(graphicName, 1, 3) = 'MT_') or (copy(graphicName, 1, 2) = 'VM')
		or (copy(graphicName, 1, 3) = 'VT_')
		or (graphicName = 'V_LOGO2')
		then result := 8;

		gid.Xenon98:
		if (copy(graphicName, length(graphicName) - 3, 4) = 'BAST')
		or (copy(graphicName, length(graphicName) - 1, 2) = '_B')
		or (copy(graphicName, 1, 5) = 'RA&TO') and (length(graphicName) = 8)
		or (graphicName = '0110') or (graphicName = '0120') or (graphicName = '0040B')
		then result := 8;

		gid.WakuWakuMP2:
		if _MatchCorners(0) >= 2 then result := 0;
	end;

	if image <> NIL then with image do
		if (palette <> NIL) and (result < dword(length(palette))) then begin
			palette[result].a := 0;
			// If format somehow truecolor, don't change.
			if bitmapFormat = MCG_FORMAT_INDEXED then bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
		end;
end;

procedure MarkAnimations(var image : mcg_bitmap; var metadata : TGraphicFile; gameid : gid);
var l, framewidth : dword;
begin
	framewidth := 0;
	with metadata do begin
		// Common files.
		case gameid of
			gid.AngelsC1, gid.AngelsC2, gid.Deep, gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Maririn,
			gid.Runaway, gid.Runaway98, gid.Sakura, gid.Sakura98, gid.SanShimai, gid.SanShimai98, gid.Setsujuu,
			gid.Tasogare, gid.Transfer98, gid.Vanish:
			begin
				if graphicName = 'MS_CUR' then begin
					// Hack: all cursors are 32x32 pixels, use proper frame division.
					framewidth := 32;
					origFrameHeightP := 32;
				end
				else if graphicName = 'PUSH' then begin
					// Hack: all push anims are 16x16 pixels.
					framewidth := 16;
					origFrameHeightP := 16;
				end;
			end;
		end;

		case gameid of
			gid.AmeKise:
			if (graphicName[1] = 'A') and (graphicName[2] = 'C') then begin
				framewidth := 640;
				origFrameHeightP := 480;
			end;

			gid.Amy98:
			if graphicName[length(graphicName)] = 'M' then frameCount := 3;

			gid.AngelsC1:
			begin
				if graphicName = 'TENGO_NO' then begin
					framewidth := 32;
					origFrameHeightP := 25;
					origOfsXP := 552;
					origOfsYP := 276;
				end;
			end;

			gid.Desire98:
			if (graphicName = '213A') or (graphicName = '213B') then begin
				framewidth := 320; origFrameHeightP := 200; frameCount := 4;
			end;

			gid.FromH:
			case graphicName of
				'FIN': begin
					framewidth := 152;
					origFrameHeightP := 96;
					seqLen := 16;
					setlength(sequence, seqLen);
					for l := 0 to 15 do sequence[l] := (l shl 16) or $50;
					sequence[14] := sequence[14] or $200;
					sequence[15] := sequence[15] or $FFFF;
				end;
				'OP_001A0': begin
					framewidth := 160;
					origFrameHeightP := 200;
					origOfsXP := (640 - 160) shr 1; // make it centered
					origOfsYP := (400 - 200) shr 1 - 12; // a bit above center
					seqLen := 16;
					setlength(sequence, seqLen);
					sequence[0] := $00000190; // frame 0 for 400 ms
					sequence[1] := $00010190; // frame 1 for 400 ms, rep x2
					for l := 2 to 11 do sequence[l] := sequence[l and 1];
					// frames 2+3 for 400 ms, rep x3
					for l := 6 to 11 do sequence[l] := sequence[l] or $20000;
					sequence[12] := $00040100; // frame 4 for 256 ms
					sequence[13] := $00050100; // frame 5 for 256 ms
					sequence[14] := $00060100; // frame 6 for 256 ms
					sequence[15] := $0007FFFF; // frame 7, stop
				end;
				'PUSH': begin
					framewidth := 64;
					origFrameHeightP := 120;
					origOfsXP := 544;
					origOfsYP := 280;
					seqLen := 11;
					setlength(sequence, seqLen);
					for l := 0 to 8 do sequence[l] := (l shl 16) or $100; // 256 ms
					sequence[9] := $00097FFF; // frame 9 for 32767 ms
					sequence[10] := $0009BFFF; // frame 9 for random(16383) ms
				end;
			end;

			gid.Hohoemi:
			case graphicName of
				'FIN':
				begin
					framewidth := 152;
					origFrameHeightP := 96;
					seqLen := 16;
					setlength(sequence, seqLen);
					for l := 0 to 15 do sequence[l] := (l shl 16) or $50;
					sequence[14] := sequence[14] or $200;
					sequence[15] := sequence[15] or $FFFF;
				end;
			end;

			gid.Majokko:
			case graphicName of
				'JURA':
				begin
					framewidth := 48;
					origFrameHeightP := 70;
					origOfsXP := 552;
					origOfsYP := 322;
					seqLen := 11;
					setlength(sequence, seqLen);
					sequence[0] := $0000BFFF; // frame 0 for random(16383) ms
					sequence[1] := $00002008; // frame 0 for 8200 ms
					sequence[2] := $00010100; // frame 1 for 256 ms
					sequence[3] := $00020100; // frame 2 for 256 ms
					sequence[4] := $00030100; // frame 3 for 256 ms
					sequence[5] := $00020100; // frame 2 for 256 ms
					sequence[6] := $00010100; // frame 1 for 256 ms
					sequence[7] := $00020100; // frame 2 for 256 ms
					sequence[8] := $00030100; // frame 3 for 256 ms
					sequence[9] := $00020100; // frame 2 for 256 ms
					sequence[10] := $C0030000; // jump to sequence index random(3)
				end;
			end;

			gid.MHard_w, gid.RoseBlood_w, gid.ZestFan_w:
			begin
				if graphicName[1] = 'A' then begin framewidth := 160; origFrameHeightP := 240; end
				else if copy(graphicName, 1, 6) = 'TITLE2' then origFrameHeightP := 160;
			end;

			gid.Sakura, gid.Sakura98:
			case graphicName of
				'SAKURA':
				begin
					framewidth := 16;
					origFrameHeightP := 16;
				end;
			end;

			gid.Setsujuu:
			case graphicName of
				'SH_16A0':
				begin
					framewidth := 64;
					origFrameHeightP := 144;
				end;
				'SH_42A0':
				begin
					framewidth := 80;
					origFrameHeightP := 152;
				end;
			end;

			gid.Tasogare:
			case graphicName of
				'OP_MOJI':
				begin
					framewidth := 80;
					origFrameHeightP := 400;
				end;

				'OP_XXA':
				begin
					framewidth := 64;
					origFrameHeightP := 168;
					origOfsXP := 144;
					origOfsYP := 168;
					seqLen := 15;
					setlength(sequence, seqLen);
					// Show each frame for 100 msec.
					for l := 0 to 13 do sequence[l] := (l shl 16) or $64;
					sequence[14] := $000EFFFF; // frame 14, stop
				end;
			end;

			gid.TrueLove_e:
			case graphicName of
				'BLACK','CGMODE','CGMODE2','CG_R2': ;
				//'LOGO','LOGO2': begin framewidth := 208; origFrameHeightP := 200; end;
				else with image do begin
					if (sizeYP < 100) and ((graphicName[length(graphicName)] = 'A')
					or ((pos('A', graphicName) > 1) and (graphicName[length(graphicName)] <> 'P')))
					then begin
						case graphicName of
							'BG030A': frameCount := 8;
							'EG050A','HG080A': frameCount := 2;
							'EG090MA','HG130A': frameCount := 4;
							else if sizeXP < 400 then frameCount := 3 else frameCount := 1; // 6 really but bad widths
						end;
						framewidth := sizeXP div frameCount;
					end;
				end;
			end;

			gid.Xenon98, gid.Xenon98cd:
			begin
				// Hardcode animation frame counts.
				if (graphicName[length(graphicName)] = 'M') and (graphicName <> 'R&M&M') then frameCount := 3;
				// BAST_M0x are big combined blinkies. Needs multiple sequences for the same graphic.
				if copy(graphicName, 1, 4) = 'BAST' then begin
					framewidth := 104;
					origFrameHeightP := 56;
					dec(origOfsYP, 24);
				end;
			end;
		end;

		// Common files again.
		case gameid of
			gid.Amy98, gid.EtsuGaku98, gid.EveBE98, gid.KinKetsu98, gid.Rabyni, gid.Xenon98, gid.Xenon98cd:
			if (frameCount > 1) then begin
				origFrameHeightP := origSizeYP div frameCount;
				// HACK: Minimal blinking animation, unsure where actual timings are found.
				setlength(sequence, 5);
				seqLen := 5;
				sequence[0] := $00009900; // frame 0, random delay $1900 ms
				sequence[1] := $00010040; // frame 1, delay $40 ms
				sequence[2] := $00020055; // frame 2, delay $55 ms
				sequence[3] := $00010040; // frame 3, delay $40 ms
				sequence[4] := $80000000; // jump back to start
			end;
		end;

		Assert((framewidth <= origSizeXP) and (origFrameHeightP <= origSizeYP),
			strcat('% bad framesize % > % | % > %', [graphicName, framewidth, origSizeXP, origFrameHeightP, origSizeYP]));

		// If frame size is unspecified, assume full image size.
		if framewidth = 0 then framewidth := origSizeXP;
		if origFrameHeightP = 0 then origFrameHeightP := origSizeYP;
		// If frame size is full image but count says multiple frames, assume vertical frames and derive frame height.
		if (frameCount > 1) and (framewidth = origSizeXP) and (origFrameHeightP = origSizeYP) then
			origFrameHeightP := origSizeYP div frameCount;
		// Given the specified or calculated frame size, l := max number of regular frames that can fit in this image.
		l := (origSizeXP div framewidth) * (origSizeYP div origFrameHeightP);
		if frameCount <= 1 then
			frameCount := l
		else if frameCount > l then
			raise DecompException.Create(strcat('framecount % > max at %x%', [l, framewidth, origFrameHeightP]));
	end;
end;

procedure SelectImageResolution(var metadata : TGraphicFile; gameid : gid);
// PC98 originals are all 640x400, but a good chunk of that is wasted in a viewframe. Graphics that are displayed
// inside the viewframe should use the viewport's size as a resolution; that way SuperSakura can scale them to full
// window size. As a rule, anything bigger than the viewport (baseres) cannot fit inside the frame, and so should
// use a 640x400 resolution. There are also some smaller graphics, such as falling sakura petals, meant to be used
// in a 640x400 context.
// Windows games largely stopped using viewframes, so all images most likely use the base resolution.
var bigres : boolean = FALSE;

	procedure _UseFullRes;
	var s : string31;
	begin
		with metadata do case gameid of
			// Windows ports with PC98-res graphics.
			gid.Mayclub_w, gid.Mayclub_e, gid.Nocturn_w, gid.Nocturn_e:
			begin origResXP := 640; origResYP := 400; end;

			else
			if ((origSizeXP = 640) and ((origFrameHeightP = 400) or (origFrameHeightP = 480)))
			or ((origSizeXP = 800) and (origFrameHeightP = 600))
			or ((origSizeXP = 1024) and (origFrameHeightP = 768))
			or ((origSizeXP = 1280) and (origFrameHeightP = 1024))
			then begin
				origResXP := origSizeXP; origResYP := origFrameHeightP;
			end
			else begin
				origResXP := 640; origResYP := 400;
				s := ShortName(gameid);
				if length(s) > 1 then begin
					s := copy(s, length(s) - 1);
					if (s = '_w') or (s = '_e') then origResYP := 480;
				end;
			end;
		end;
	end;

begin
	with metadata do begin
		if frameCount > 1 then
			bigres := (origFrameHeightP > gameConst.baseResYP)
		else if (origSizeXP = gameConst.baseResXP) and (origSizeYP = gameconst.baseResYP shl 1) then
			bigres := FALSE // exactly double-height images are likely pannable within the viewframe
		else
			bigres := (origSizeXP > gameConst.baseResXP) or (origSizeYP > gameConst.baseResYP);

		// Common files.
		if NOT bigres then case gameid of
			gid.AngelsC1, gid.AngelsC2, gid.Deep, gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Maririn,
			gid.Runaway, gid.Runaway98, gid.Sakura, gid.Sakura98, gid.SanShimai, gid.SanShimai98, gid.Setsujuu,
			gid.Tasogare, gid.Transfer98, gid.Vanish:
				bigres :=
					(graphicName = 'TIARE_P')
					or (graphicName = 'MARU1') or (graphicName = 'MARU2') or (graphicName = 'MARU3');

			gid.Amy98, gid.Desire98, gid.EtsuGaku98, gid.EveBE98, gid.KinKetsu98, gid.Xenon98:
				bigres :=
					(graphicName = 'ROGO') or (copy(graphicName, 1, 4) = 'WAKU')
					or (copy(graphicName, 1, 4) = 'MENU') or (copy(graphicName, 1, 3) = 'TIT');
		end;

		// Game-specific exceptions.
		if bigres then case gameid of // fix stuff that was falsely marked as bigres
			gid.Lime1:
			if (graphicName[1] = '1') and (graphicName <> '101S') and (graphicName <> '109') then bigres := FALSE;
		end
		else case gameid of // fix stuff that still needs to be bigres
			gid.Amy98: bigres := (graphicName = 'WAKU2') or (copy(graphicName, 1, 3) = 'EYE');
			gid.AngelsC1:
				bigres := (graphicName = 'TENGO_NO') or (graphicName = 'T1_00E0') or (graphicName = 'T3_00E0');
			gid.Desire98: bigres := (copy(graphicName, 1, 3) = '213');
			gid.EveBE98: bigres := (copy(graphicName, 1, 3) = 'EVE');
			gid.FromH: bigres := (graphicName[1] = 'O') or (graphicName = 'PUSH');
			gid.Lilith: bigres := (graphicName = 'WIN');
			gid.Majokko: bigres := (graphicName = 'JURA');
			gid.Mayclub98, gid.Mayclub_w, gid.Mayclub_e:
				bigres := (graphicName = 'PRS') or (graphicName[1] in ['0'..'9']);

			gid.MHard98, gid.RoseBlood98, gid.RoseBlood_w:
			bigres := (copy(graphicName, 1, 5) = 'TITLE') or (copy(graphicName, 1, 2) = 'SR') or (graphicName = 'R');

			gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
			bigres := (copy(graphicName, 1, 3) = 'END');

			gid.Sakura, gid.Sakura98:
			bigres := (graphicName = 'HANKO') or (graphicName = 'SAKURA') or (copy(graphicName, 1, 5) = 'AE_19');

			gid.Setsujuu: bigres := (copy(graphicName, 1, 3) = 'SET');
			gid.Runaway, gid.Runaway98: bigres := (graphicName = 'OP_013A0');
			gid.Tasogare: bigres := (graphicName = 'OP_XXA');
			gid.Transfer98: bigres := (graphicName = 'ROGOL2');
			gid.Xenon98: bigres := (graphicName = 'XENON2') or (graphicName = 'MUSIC');
			gid.ZestFan98, gid.ZestFan_w: bigres := (origSizeYP = 176) or (origSizeYP = 248) or (graphicName = 'R');
		end;

		if (NOT bigres) or (gameConst.baseResYP >= 400) then begin
			origResXP := gameConst.baseResXP;
			origResYP := gameConst.baseResYP;
		end
		else _UseFullRes;
	end;
end;

{$ifdef enable_decomp_hacks}
procedure ApplyGraphicsHacks(
	var metadata : TGraphicFile; var image : mcg_bitmap; gameid : gid; transparentindex : dword);
var l, m, clippedw, clipleft : dword;
	meta2 : TGraphicFile = NIL;
	pal2 : TSRGBPalette = NIL;
	p : pointer = NIL;
begin
	clipleft := 0; clippedw := 0;
	with metadata do begin

		case gameid of
			gid.AkaiSuishou:
			begin
				// Hack: In-viewframe graphics have an extra inner border that can be cropped.
				if origSizeYP = 304 then begin
					if origSizeXP = 512 then
						CropImage(image, metadata, 2, 2, 2, 2)
					else
						CropImage(image, metadata, 0, 0, 2, 2);
				end;

				case graphicName of
					// Hack: Fix mosaic overflow.
					'YI_01':
					with image do begin
						l := 331 * origSizeXP + 424;
						word((@bitmap[l])^) := $0D0D;
						fillbyte(bitmap[l + 2], 11, $0C);
						dword((@bitmap[l + 13])^) := BEtoN(dword($0D0E0F01));
						l := 332 * origSizeXP + 417;
						word((@bitmap[l])^) := $0D0D;
						fillbyte(bitmap[l + 2], 7, $0C);
						dword((@bitmap[l + 9])^) := BEtoN(dword($0D0E0F01));
						fillbyte(bitmap[l + 13], 12, $01);
						l := 333 * origSizeXP + 412;
						bitmap[l] := $C;
						fillbyte(bitmap[l + 1], 5, 0);
						dword((@bitmap[l + 6])^) := BEtoN(dword($0C0D0E0F));
						fillbyte(bitmap[l + 10], 18, $01);

						word((@bitmap[317 * origSizeXP + 439])^) := $0F0F;
						bitmap[316 * origSizeXP + 438] := $F;
						bitmap[315 * origSizeXP + 436] := $F;
						word((@bitmap[314 * origSizeXP + 436])^) := BEtoN(word($0F0E));
						word((@bitmap[313 * origSizeXP + 435])^) := $0F0F;
						dword((@bitmap[312 * origSizeXP + 434])^) := $0F0F0F0F;
						dword((@bitmap[311 * origSizeXP + 433])^) := $0F0F0F0F;
						bitmap[310 * origSizeXP + 433] := $F;
					end;
				end;
			end;

			gid.Amy98:
			begin
				// Hack: Most graphics include the viewframe's offset, which can be discounted.
				if (origResXP = gameConst.baseResXP) and (origResYP = gameConst.baseResYP) then begin
					dec(origOfsXP, 32);
					dec(origOfsYP, 8);
				end;

				case graphicName of
					// Hack: Fix bogus animation offsets.
					'130_M': origOfsYP := 40 - 8;
					'140_M': origOfsYP := 176 - 8;
					'190B_M': begin origOfsXP := 56 - 32; origOfsYP := 96 - 8; end;
					'510_M': begin origOfsXP := 520 - 32; origOfsYP := 136 - 8; end;
					'610_M': origOfsYP := 184 - 8;
					'MENU_M': begin origOfsXP := 24; origOfsYP := 144; end;
				end;
			end;

			gid.AngelCrisis:
			begin
				// Hack: Remove a stray opaque pixel.
				if graphicName = 'ACT02A' then image.bitmap[466 * image.stride + 637] := 0;
			end;

			//gid.AngelsC1, gid.AngelsC2: ;

			gid.Desire98:
			begin
				// Hack: Crop graphic with big black borders.
				case graphicName of
					'100B': CropImage(image, metadata, 56, 320, 24, 144);
					'150B': CropImage(image, metadata, 80, 72, 0, 120);
					'420D': CropImage(image, metadata, 80, 80, 16, 120);
				end;
				if (origSizeXP = gameConst.baseResXP) and (origSizeYP = gameconst.baseResYP) then begin
					origResXP := gameConst.baseResXP;
					origResYP := gameConst.baseResYP;
				end;
				// Hack: Most graphics include the viewframe's offset, which can be discounted.
				if (origResXP = gameConst.baseResXP) and (origResYP = gameConst.baseResYP) then begin
					dec(origOfsXP, 80);
					dec(origOfsYP, 16);
				end;
			end;

			gid.DiviDead, gid.DiviDead_e:
			with image do begin
				// Hack: remove garbage pixels.
				if graphicName = 'B04_0' then begin
					word((@bitmap[43 + 121 * image.sizeXP])^) := 0;
					word((@bitmap[43 + 122 * image.sizeXP])^) := 0;
				end;
				// Hack: import alpha channel from a previous image.
				if (graphicName[1] = 'B') and (length(graphicName) > 5) and (graphicName[length(graphicName)] <> '0')
				then begin
					l := pos('_', graphicName);
					if l > 1 then ImportAlphaChannel(image, metadata, copy(graphicName, 1, l) + '0');
				end
				else if bitmapFormat = MCG_FORMAT_BGR then begin
					// Hack: transparency for event images.
					l := LEtoN(dword((@bitmap[0])^));
					if (graphicName = 'I_17A') or (graphicName = 'I_17B')
					or (l and $FFFFFF00 = $00FF00)
					or (LEtoN(dword((@bitmap[sizeXP * sizeYP * 3 - 3])^)) and $FFFFFF00 = $00FF00)
					then FillFlatAlpha(image, RGBAquad(l));
				end;
			end;

			gid.Eden:
			begin
				// Hack: many animations have an incorrect palette, blanket fix.
				if copy(graphicName, length(graphicName) - 1, 2) = 'A0' then
					with image do begin
						palette[1].r := $55; palette[2].r := $88; palette[3].r := $BB; palette[6].b := $DD;
						palette[8].r := $99; palette[8].b := $55;
						palette[12].FromRGBA4($A76F);
						palette[13].r := $DD;
					end;
				// Hack: erase garbage pixels at 244,60 and 253,66.
				if (graphicName = 'EE_013') or (graphicName = 'EE_014') then begin
					image.bitmap[60*480 + 244] := 1;
					image.bitmap[66*480 + 253] := 1;
				end;
			end;

			gid.EtsuGaku98:
			begin
				// Hack: Most graphics include the viewframe's offset, which can be discounted.
				if (origResXP = gameConst.baseResXP) and (origResYP = gameConst.baseResYP) then begin
					dec(origOfsXP, 64);
					dec(origOfsYP, 8);
				end;
				case graphicName of
					// Hack: Remove duplicated background pixels from square popup.
					'040C': CropImage(image, metadata, 0, 7, 0, 0);
					// Hack: Remove duplicated background pixels from popup.
					'050C': CropImage(image, metadata, 0, 7, 0, 0);
				end;
			end;

			gid.EveBE98:
			begin
				// Hack: Most graphics include the viewframe's offset, which can be discounted.
				if (origResXP = gameConst.baseResXP) and (origResYP = gameConst.baseResYP) then begin
					dec(origOfsXP, 64);
					dec(origOfsYP, 8);
				end;

				case graphicName of
					// Hack: Use nighttime palette
					'007G': with image do begin
						palette[$C] := RGBAquad(LEtoN(dword($FF001155)));
						palette[$D] := RGBAquad(LEtoN(dword($FF223377)));
						palette[$E] := RGBAquad(LEtoN(dword($FF6677AA)));
					end;
				end;
			end;

			gid.FromH:
			case graphicName of
				// Hack: Cut out garbage pixels.
				'FT_09', 'FT_10', 'FT_11': clippedw := 176;
				'FT_14': clippedw := 183;
				'FT_15': clippedw := 191;
				'FT_16': clippedw := 177;
			end;

			gid.GaoGao1, gid.GaoGao2, gid.GaoGao4:
			case graphicName of
				// Hack: Some graphics actually use different dimensions than they declare.
				'RS023H', 'RS037AH', 'RS037AL', 'RS037H', 'RS037L', 'RS073H', 'RS073L', 'RS103H', 'RS103L',
				'PW004H', 'PW004L', 'PW021H', 'PW021L', 'PW042H', 'PW042M', 'PW042L', 'PW045H', 'PW045M', 'PW045L',
				'PW056H', 'PW056L', 'PW060H', 'PW060L', 'PW104H', 'PW104L',
				'BG12H', 'BG12L', 'E020H', 'E020L', 'E029H', 'E029L', 'E29BH', 'E29BL', 'E033H', 'E033L',
				'E050H', 'E050L', 'S028H', 'S028L', 'S030H', 'S030L', 'S034H', 'S034L', 'S040H', 'S040L':
				with image do begin
					sizeXP := 480;
					sizeYP := length(bitmap) div 480;
					stride := sizeXP;
					origFrameHeightP := 0;
				end;
			end;

			gid.GitenMegaTen:
			case graphicName of
				// Hack: Unnecessary transparency.
				'FC5090_9.0': with image do begin
					setlength(palette, 16);
					bitmapFormat := MCG_FORMAT_INDEXED;
				end;
			end;

			gid.Hohoemi:
			begin
				// Hack: Some backgrounds actually need an extra offset to fit the viewframe.
				if (graphicName[1] = 'B') or (graphicName[2] = 'T') then inc(origOfsXP, 96)
				// Hack: Align title graphic better.
				else if graphicName = 'OP_000_' then origOfsXP := -14;
			end;

			gid.KinKetsu98:
			begin
				// Hack: Most graphics include the viewframe's offset, which can be discounted.
				if (origSizeXP = gameConst.baseResXP) and (origSizeYP = gameConst.baseResYP) then
					dec(origOfsXP, 64);
			end;

			gid.Kurayami:
			case graphicName of
				// Hack: Remove garbage pixel.
				'Y13B': begin clipleft := 200; clippedw := 240; end;
			end;

			gid.Kyouhaku98:
			// Hack: Enforce palette.
			with image do if dword(palette[0]) = 0 then begin
				palette[0].FromRGBA($FF);
				palette[1].FromRGBA($FFCC77FF);
				palette[2].FromRGBA($FFAA99FF);
				palette[3].FromRGBA($774444FF);
				palette[4].FromRGBA($AABBFFFF);
				palette[5].FromRGBA($1144BBFF);
				palette[6].FromRGBA($112277FF);
				palette[7].FromRGBA($FF7799FF);
				palette[8].FromRGBA($559966FF);
				palette[9].FromRGBA($BBBBCCFF);
				palette[10].FromRGBA($888899FF);
				palette[11].FromRGBA($66666600);
				palette[12].FromRGBA($DD0044FF);
				palette[13].FromRGBA($BB8888FF);
				palette[14].FromRGBA($FFDDCCFF);
				dword(palette[15]) := $FFFFFFFF;
			end;

			gid.Lakers2:
			// Hack: Enforce palette.
			if graphicName = 'L2OP_ANM.0' then with image do begin
				palette[0].FromRGBA($FF);
				palette[1].FromRGBA($775544FF);
				palette[2].FromRGBA($EE8866FF);
				palette[3].FromRGBA($FFBB99FF);
				palette[4].FromRGBA($FFDDBBFF);
				palette[5].FromRGBA($FFBB55FF);
				palette[6].FromRGBA($BBDDFFFF);
				palette[7].FromRGBA($5566AAFF);
				palette[8].FromRGBA($66CC99FF);
				palette[9].FromRGBA($006600FF);
				palette[10].FromRGBA($FF99DDFF);
				palette[11].FromRGBA($CC4444FF);
				palette[12].FromRGBA($888899FF);
				palette[13].FromRGBA($555566FF);
				palette[14].FromRGBA($55AAAAFF);
				dword(palette[15]) := $FFFFFFFF;
			end;

			gid.Lilith:
			with metadata do begin
				case graphicName of
					// Hack: Remove unnecessary transparent margin.
					'065','065A','065B','065C','104':
					begin
						image.Crop(0, 160, 0, 112);
						image.bitmapFormat := MCG_FORMAT_INDEXED;
						origResXP := gameConst.baseResXP;
						origResYP := gameConst.baseResYP;
					end;
					// Hack: Crop some more borders and fix resolution choice based on that.
					else if origResXP = 640 then begin
						CropVoidBorders(image, metadata, 0);
						if (origOfsXP or origOfsYP) <> 0 then begin
							origResXP := gameConst.baseResXP;
							origResYP := gameConst.baseResYP;
						end;
					end;
				end;
			end;

			gid.Lixus:
			with image do begin
				// Hack: Crop some garbage out.
				if (graphicName = 'CG12') then
					image.Crop(0, 40, 0, 0)
				else if (graphicName = 'CG54') then
					image.Crop(0, 109, 0, 102)
				// Hack: If a fullscreen image has a specific spacer pattern, crop to viewframe size.
				else if (sizeXP = 640) and (BEtoN(qword((@bitmap[278])^)) = $01010101010F0000) then
					image.Crop(0, 104, 0, 100)
				// Hack: If it's a monster image at 360 width, crop 40 on the right, it's always garbage.
				else if (sizeXP = 360) and (copy(graphicName, 1, 4) = 'MON_') then
					image.Crop(0, 40, 0, 0);
				metadata.origSizeXP := sizeXP;
				metadata.origSizeYP := sizeYP;
				// Hack: There may be an unnecessary margin of color 0, crop it out. But it's not transparent.
				CropVoidBorders(image, metadata, 0);
			end;

			gid.Majokko:
			case graphicName of
				// Hack: cut out garbage pixels.
				'MT01FU': clippedw := 173;

				// Hack: extrapolate a missing pixel row by copying from one up left.
				'KE_069_1':
				begin
					l := origSizeXP * origSizeYP;
					for m := (origSizeXP shr 2) - 1 downto 0 do begin
						dec(l, 4);
						dword((@image.bitmap[l])^) := dword((@image.bitmap[l] - origSizeXP - 1)^);
					end;
					// Fix minor remaining discontinuity.
					l := origSizeXP * (origSizeYP - 1);
					dword((@image.bitmap[l] + 203)^) := $0E0D0C00; // lt leg
					dword((@image.bitmap[l] + 296)^) := $0F000C0F; // rt leg
					dword((@image.bitmap[l] + 362)^) := $0F050404; // rt hand
					inc(l, 247); // inside legs
					move((@image.bitmap[l] - origSizeXP)^, image.bitmap[l], 9);
				end;
			end;

			gid.Mayclub98, gid.Mayclub_e:
			case graphicName of
				// Hack: cut out garbage pixels.
				'D27A': clippedw := 182;
				// Hack: due to some palette shenanigan, the original game shows this more orange than expected.
				'Z65': image.palette[14].b := $77;
				// Hack: fix too big images.
				'G46','Z65Y': begin
					origResXP := 480; origResYP := 288;
					clippedw := origResXP; image.sizeYP := origResYP;
				end;
			end;

			gid.MHard98:
			begin
				// Hack: Most graphics include the viewframe's offset, which can be discounted.
				if (origSizeXP = gameConst.baseResXP) and (origSizeYP = gameConst.baseResYP) then begin
					dec(origOfsXP, 80);
					dec(origOfsYP, 8);
				end;
			end;

			gid.MHard_w:
			begin
				// Hack: Cut unnecessary menu entry frames.
				if graphicName = 'TITLE2' then clippedw := 640;
				// Hack: Crop void borders, unused graphic.
				if graphicName = 'H_07' then begin
					image.Crop(80, 80, 8, 120);
					origResXP := gameConst.baseResXP;
					origResYP := gameConst.baseResYP;
				end;
			end;

			gid.RoseBlood98, gid.RoseBlood_w:
			begin
				// Hack: Most graphics include the viewframe's offset, which can be discounted.
				if (gameid = gid.RoseBlood98)
				and (origSizeXP = gameConst.baseResXP) and (origSizeYP = gameConst.baseResYP) then begin
					dec(origOfsXP, 80);
					dec(origOfsYP, 8);
				end;

				// Hack: Cut unnecessary menu entry frames.
				if (gameid = gid.RoseBlood_w) and (graphicName = 'TITLE') then clippedw := 640;

				// Hack: Generate an extra night-palette graphic for most backgrounds. (H_10 or H_11 already dark.)
				if (graphicName[1] = 'H') and ((graphicName[3] = '0') or (graphicName[4] = '2')) then begin
					try
						meta2 := metadata.Clone;
						meta2.graphicName := meta2.graphicName + '_N';
						meta2.srcFilePath := copy(meta2.srcFilePath, 1, meta2.srcFilePath.Length - 4) + '_n.png';

						setlength(pal2, length(image.palette));
						move(image.palette[0], pal2[0], sizeof(pal2[0]) * length(pal2)); // stash original palette
						for l := high(image.palette) downto 0 do
							// Give everything a dark blue tint, except leave bright whites bright (lamps etc).
							if (dword(image.palette[l]) and $F0F0F0F0 <> $F0F0F0F0)
							or ((gameid = gid.RoseBlood_w) and (graphicName <> 'H_03')) // most _w gfx too smudged :(
							then with image.palette[l] do begin
								b := b shr 1;
								g := g div 3;
								r := r div 3;
							end;
						image.MakePNG(p, l);
						SaveFile(meta2.srcFilePath, p, l);
						image.palette := NIL; image.palette := pal2; pal2 := NIL; // restore original palette

						AddOrReplaceGraphicFile(meta2);
						meta2 := NIL;
					finally
						if meta2 <> NIL then begin meta2.Destroy; meta2 := NIL; end;
						if p <> NIL then begin freemem(p); p := NIL; end;
					end;
				end;
			end;

			gid.Runaway, gid.Runaway98:
			begin
				// Hack: set garbage pixel in bottom left corner to transparent.
				if graphicName = 'M8_012' then image.bitmap[origSizeXP * (origSizeYP - 1)] := transparentindex;
			end;

			gid.Sakura, gid.Sakura98:
			case graphicName of
				// Hack: cut out unnecessary space caused by garbage pixels.
				'CT02S': clippedw := 155;
				'CT03D': dec(image.sizeYP, 202);
				'CT05S': clippedw := 149;
				'CT11D': dec(image.sizeYP, 162);
				'CT14M': clippedw := 283;

				// Hack: add offset to title bar. (Should just draw centered in the main title script instead!)
				'SAKU_T':
				begin
					inc(origOfsXP, 64);
					inc(origOfsYP, 112);
				end;
			end;

			//gid.SanShimai, gid.SanShimai98: ;

			gid.Setsujuu:
			case graphicName of
				// Hack: cut out garbage pixels.
				{'SH_30S': begin clipleft := 9; clippedw := 414; image.sizeYP := 223; end;
				'SH_58S': begin clipleft := 277; clippedw := 219; image.sizeYP := 164; end;
				'SH_63S': begin clipleft := 91; clippedw := 86; image.sizeYP := 129; end;
				'SH_68S': begin clipleft := 134; clippedw := 306; end;}
				'ST_05': begin clipleft := 215; clippedw := 246; end;
			end;

			gid.Tasogare:
			case graphicName of
				// Hack: cut out garbage pixels.
				'YT_10': clippedw := 262;
			end;

			gid.Transfer98:
			case graphicName of
				// Hack: fix the palette.
				'TI_135A':
				with image do begin
					palette[0].FromRGBA4($F);
					palette[1].FromRGBA4($43AF);
					palette[2].FromRGBA4($46CF);
					palette[3].FromRGBA4($BCEF);
					palette[4].FromRGBA4($89DF);
					palette[5].FromRGBA4($C87F);
					palette[6].FromRGBA4($FCBF);
					palette[7].FromRGBA4($041F);
					palette[8].FromRGBA4($855F);
					palette[9].FromRGBA4($533F);
					palette[10].FromRGBA4($800F);
					palette[11].FromRGBA4($84BF);
					palette[12].FromRGBA4($517F);
					palette[13].FromRGBA4($C34F);
					palette[14].FromRGBA4($B7FF);
					palette[15].FromRGBA4($FFFF);
				end;

				// Hack: cut out garbage pixels.
				'TT_20': begin clipleft := 101; clippedw := 247; end;
				'TH_042','TH_110': clippedw := 296;
			end;

			gid.TrueLove_e:
			case graphicName of
				'BLACK','CGMODE','CGMODE2','CG_R2': ;
				// Hack: todo: Remove garbage pixels.
				//'CG010M': ;
				// Hack: cut out empty space.
				'MG180': CropVoidBorders(image, metadata, 10);
				//'BG050','NG041': CropVoidBorders(image, metadata, 0);
				else with image do if copy(graphicName, 1, 3) <> 'SD_' then begin
					if frameCount <= 1 then begin
						// Hack: cut out empty space from character sprite images.
						if (length(palette) = 16) and (palette[14].a = 0) then
							CropVoidBorders(image, metadata, 0)
						else begin
							// Hack: cut out empty space from bkg and events. Because these have occasional garbage
							// pixels in the area being cut, have to target a hard pixel size. But if there's plenty of
							// valid image data in the area being cut and not just black, then leave it.
							if (origSizeXP = 640) and (origSizeYP = 400)
							and (dword((@bitmap[640 * 400 - 4])^) = 0) // bottom right corner is black
							and (qword((@bitmap[640 * 360])^) = 0) // bottom left almost corner is black
							and (qword((@bitmap[640 * 8 - 8])^) = 0) // top right almost corner is black
							then image.Crop(0, 128, 0, 112);
						end;
					end;
				end;
			end;

			gid.Ukiuki:
			case graphicName of
				// Hack: Restore corrupted last row.
				'EVW_94':
				with image do if dword((@bitmap[sizeXP * sizeYP - 4])^) = 0 then begin
					l := sizeXP * (sizeYP - 1);
					MemCopy(@bitmap[l - sizeXP * 2], @bitmap[l], 108);
					inc(l, 108);
					MemCopy(@bitmap[l - sizeXP * 2 - 2], @bitmap[l], 149);
					inc(l, 149);
					MemCopy(@bitmap[l - sizeXP - 2], @bitmap[l], sizeXP - 108 - 149);
					// Fix the leftover hair discontinuities.
					dword((@bitmap[l + 249])^) := NtoBE($00040505);
					dword((@bitmap[l + 74])^) := $04040404;
					dword((@bitmap[l + 70])^) := NtoBE($07070404);
				end;
			end;

			gid.Vanish:
			case graphicName of
				// Hack: cut out garbage pixels, fix one incorrect transparent pixel.
				'VT_006': begin clipleft := 106; clippedw := 228; image.bitmap[295*480 + 300] := 4; end;
			end;

			gid.Xenon98:
			begin
				// Hack: Most graphics include a slice of the viewframe unnecessarily, crop it out.
				if (origSizeXP = 560) and (origSizeYP = 296) then begin
					CropImage(image, metadata, 8, 8, 4, 4);
					origResXP := origSizeXP; origResYP := origSizeYP;
				end;

				// Hack: Most graphics include the viewframe's offset, which can be discounted.
				if (origResXP = gameConst.baseResXP) and (origResYP = gameConst.baseResYP) then begin
					dec(origOfsXP, 48);
					dec(origOfsYP, 8);
				end;

				case graphicName of
					// Hack: Move pod terminal cutaway higher on the screen so it's not covered by the main box.
					'0040B': metadata.origOfsYP := metadata.origOfsYP shr 1;
					// Hack: Remove non-transparent column.
					'TO_BA_B': CropImage(image, metadata, 0, 8, 0, 0);
				end;
			end;

			gid.ZestFan98:
			begin
				// Hack: Most graphics include the viewframe's offset, which can be discounted.
				if (origSizeXP = 480) and (origSizeYP = 288) then begin
					dec(origOfsXP, 80);
					dec(origOfsYP, 8);
				end;
				// Hack: Fix resolutions for fullscreen images.
				if (graphicName[1] in ['A'..'D']) and (graphicName[2] = '_') then begin
					origResXP := origSizeXP;
					origResYP := origSizeYP;
				end;
			end;

			gid.ZestFan_w:
			begin
				// Hack: Cut unnecessary menu entry frames.
				if graphicName = 'TITLE' then clippedw := 640;

				case graphicName[1] of
					// Hack: Add missing viewframe-relative offsets.
					'0':
					begin
						origOfsXP := 32;
						origOfsYP := 256;
					end;
					'H':
					begin
						origOfsXP := 104;
						origOfsYP := 8;
					end;
					// Hack: Fix resolutions for fullscreen images.
					'A'..'D':
					if graphicName[2] = '_' then begin
						origResXP := origSizeXP;
						origResYP := origSizeYP;
					end;
				end;
			end;
		end;

		if (clippedw or clipleft) <> 0 then image.Crop(clipleft, image.sizeXP - clippedw - clipleft, 0, 0);
		// If bitmap was cropped, copy new size to metadata.
		origSizeXP := image.sizeXP;
		origSizeYP := image.sizeYP;
		inc(origOfsXP, longint(clipleft));
		if (origFrameHeightP = 0) or (origFrameHeightP > origSizeYP) then origFrameHeightP := origSizeYP;

		// If it's not an animation, but has transparency, empty space at the edges can be cropped out, but for
		// a one-pixel transparent border.
		// Hack: Leave JAST/Tiare TB_008 alone, used as a full-viewport transparent layer for the graphic stash.
		if graphicName = 'TB_008' then exit;
		if (frameCount <= 1) and (origFrameHeightP = origSizeYP) then case image.bitmapFormat of
			MCG_FORMAT_INDEXEDALPHA: CropVoidBorders(image, metadata, transparentindex);
			MCG_FORMAT_BGRA: CropVoidBorders32(image, metadata);
		end;

		if (image.bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA])
		and (image.palette <> NIL) and (length(image.palette) <= 1 shl (image.bitDepth - 1)) then
			image.PackBitDepth(1);
	end;
end;
{$endif enable_decomp_hacks}

// ------------------------------------------------------------------

procedure ApplyFilter(const imgname : UTF8string);
// Checks if the given graphic file has an associated DTL-file; if not, creates a default one for it.
// Calls Beautify to auto-process the given graphic file.
var //image : mcg_bitmap = NIL;
	//loader : TFileLoader;
	//i, j, k, l : dword;
	gfxindex, fileindex : dword;
begin
	gfxindex := SeekNewGfx(lowercase(imgname)); if gfxindex >= newGfxCount then exit;
	fileindex := GetGraphicFile(upcase(imgname)); if fileindex = 0 then exit;

	{$ifdef bonk}
	loader := TFileLoader.Open(graphicFiles[fileindex].srcFilePath);
	try
		image := mcg_bitmap.FromPNG(loader.readp, loader.buffySize, [MCG_FLAG_FORCE8BPP]);
	finally
		if loader <> NIL then begin loader.Destroy; loader := NIL; end;
	end;

	InitBeautify(imgname);

	// Adjust default parameters for close-up images, nominated manually.
	i := 0;
	case game of
		gid.Deep: if (copy(namu, 1, 4) = 'DH_S') or (copy(namu, 1, 3) = 'DI_') then inc(i);
		gid.Eden: if copy(namu, 1, 3) = 'EE_' then inc(i);
		gid.FromH: if (copy(namu, 1, 3) = 'FE_') or (copy(namu, 1, 3) = 'FH_') or (copy(namu, 1, 3) = 'GRO') then inc(i);
		gid.Majokko: if (copy(namu, 1, 3) = 'KE_') or (copy(namu, 1, 2) = 'OP') then inc(i);
		gid.Maririn: if copy(namu, 1, 3) = 'BC' then inc(i);
		gid.Runaway, gid.Runaway98: if copy(namu, 1, 3) = 'MH_' then inc(i);
		gid.Sakura, gid.Sakura98: if copy(namu, 1, 3) = 'AE_' then inc(i);
		gid.SanShimai, gid.SanShimai98: if copy(namu, 1, 3) = 'SE_' then inc(i);
		gid.Setsujuu: if (copy(namu, 1, 3) = 'SH_') or (copy(namu, 1, 3) = 'SI_') then inc(i);
		gid.Transfer98: if (copy(namu, 1, 3) = 'TH_') or (copy(namu, 1, 3) = 'TI_') then inc(i);
		gid.Tasogare: if (copy(namu, 1, 3) = 'YE_') or (copy(namu, 1, 3) = 'YH_') then inc(i);
		gid.Vanish: if (copy(namu, 1, 3) = 'VE_') or (copy(namu, 1, 3) = 'VH_') then inc(i);
	end;
	if i <> 0 then begin
		gammacorrect := 113; // closeups generally look better darker
		processHVlines := FALSE; // ... and generally don't have orthogonal lines
	end;

	// Figure out the transparent color index.
	xparency := $FF;
	if image.bitmapFormat = MCG_FORMAT_INDEXEDALPHA then
		for i := high(image.palette) downto 0 do
			if image.palette[i].a = 0 then xparency := i;
	// Transparent things usually don't have horizontal/vertical lines.
	if xparency <> $FF then processHVlines := FALSE;

	// Image frames need to be pushed apart to avoid color leaking. Do this by injecting four transparent pixel rows
	// between all frames.
	if PNGlist[gfxi].framecount > 1 then begin
		if xparency >= dword(length(PNGlist[gfxi].pal)) then begin
			LogError('Transparent color must be hardcoded in decomp_g!');
		end
		else begin
			i := image.sizeYP + (PNGlist[gfxi].framecount - 1) * 4;
			getmem(loader, image.sizeXP * i);
			l := image.sizeXP * PNGlist[gfxi].frameheight;
			j := PNGlist[gfxi].framecount;
			while j <> 0 do begin
				dec(j);
				move((image.bitmap + j * l)^, (loader + j * image.sizex * (PNGlist[gfxi].frameheight + 4))^, l);
				if j + 1 <> PNGlist[gfxi].framecount then
					fillbyte((loader + j * image.sizeXP * (PNGlist[gfxi].frameheight + 4) + l)^, image.sizeXP * 4, xparency);
			end;
			freemem(image.bitmap); image.bitmap := loader; loader := NIL;
			image.sizeYP := i;
		end;
	end;

	setlength(detaillist, 0);
	Beautify(@image);

	// Image frames should be put together again.
	if PNGlist[gfxi].framecount > 1 then begin
		l := image.sizeXP * (3 + image.memformat and 1); // image row byte width
		i := l * PNGlist[gfxi].frameheight; // frame byte size
		j := l * (PNGlist[gfxi].frameheight + 4); // frame byte size + 4 rows
		for k := 1 to PNGlist[gfxi].framecount - 1 do
			move((image.bitmap + j * k)^, (image.bitmap + i * k)^, i);
		image.sizey := PNGlist[gfxi].frameheight * PNGlist[gfxi].framecount;

		// Make animation frame edges completely transparent to reduce artifacts.
		if image.memformat = 1 then
			for k := PNGlist[gfxi].framecount - 1 downto 0 do begin
				j := i * k; // j = offset to frame's top left corner
				fillbyte((image.bitmap + j)^, l, 0); // top edge
				fillbyte((image.bitmap + j + i - l)^, l, 0); // bottom edge
			end;
		j := 0; i := (3 + image.memformat and 1);
		for k := image.sizeYP - 1 downto 0 do begin
			fillbyte((image.bitmap + j)^, i, 0); // left edge
			inc(j, l);
			fillbyte((image.bitmap + j - i)^, i, 0);
		end;
	end;

	// Save the result over the original.
	j := mcg_MemorytoPNG(@image, @loader, @i); // build a PNG in loader^
	mcg_ForgetImage(@image);
	if j <> 0 then begin
		LogError(mcg_errortxt); exit;
	end;
	assign(filu, projectdir + 'gfx' + DirectorySeparator + namu + '.png');
	filemode := 1; rewrite(filu, 1); // write-only
	j := IOresult;
	if j <> 0 then begin
		LogError('IO error ' + strdec(j) + ' trying to write ' + namu);
		exit;
	end;
	blockwrite(filu, loader^, i);
	close(filu);
	{$endif}
end;

procedure BeautifyGraphics;
// Converted graphics have been stored under gfx/ as 16-color indexed PNGs.
// This batch-beautifies them using a reverse-dithering filter.
var i : dword;
begin
	decomp_logger('Applying reverse-dithering filter on ' + strdec(newGfxCount) + ' images...');
	for i := newGfxCount - 1 downto 0 do begin
		decomp_logger(strdec(i) + ': ' + newGfxList[i]);
		ApplyFilter(newGfxList[i]);
	end;
end;

// ------------------------------------------------------------------

procedure PostProcessGraphic(var image : mcg_bitmap; var metadata : TGraphicFile; gameid : gid; transparentindex : dword);
// After an individual graphic has been converted, call this to round out its metadata and apply fixes.
begin
	if (metadata.origFrameHeightP = 0) or (metadata.frameCount = 0) then MarkAnimations(image, metadata, gameid);
	SelectImageResolution(metadata, gameid);
	{$ifdef enable_decomp_hacks}
	if decomp_param.doHacks then
		ApplyGraphicsHacks(metadata, image, gameid, transparentindex);
	ForceUniquePalette(image);
	{$endif}
end;

procedure PostProcess(game : PGameRecord);
// After all input files for a game have been converted, call this to apply extra upgrades on all graphics.
begin
	if newGfxCount = 0 then exit;
	if decomp_param.doHacks then CompositeGraphics(game);
	if decomp_param.doBeautify then BeautifyGraphics;
end;

