{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// These would look nice in a byte enum, but those can't be used as array indexes without casting. :(
// Todo: change to 1-byte enum after ss_cmdparams is removed.
const
	CMDP_DYNAMIC = 0; // autofit to next appropriate parameter type
	CMDP_ANYNUMBER = 1; // unnamed numeric parameter
	CMDP_ANYSTRING = 2; // unnamed string parameter
	CMDP_ALPHA = 3;
	CMDP_ANCHORX = 6;
	CMDP_ANCHORY = 7;
	CMDP_ANGLE = 8;
	CMDP_BOX = 9;
	CMDP_BY = 11;
	CMDP_COLOR = 14;
	CMDP_COUNT = 15;
	CMDP_FORCE = 17;
	CMDP_FRAME = 18;
	CMDP_FREQ = 20;
	CMDP_GAMMA = 22;
	CMDP_GOB = 24;
	CMDP_INDEX = 26;
	CMDP_LABEL = 28;
	CMDP_LOCX = 30;
	CMDP_LOCY = 31;
	CMDP_MOUSEOFF = 33;
	CMDP_MOUSEON = 34;
	CMDP_MOUSEONLY = 35;
	CMDP_NAME = 37;
	CMDP_NOCLEAR = 39;
	CMDP_NOPRINT = 40;
	CMDP_NOWAIT = 41;
	CMDP_PARENT = 42;
	CMDP_RATIOX = 44;
	CMDP_RATIOY = 45;
	CMDP_SIZEX = 47;
	CMDP_SIZEY = 48;
	CMDP_STYLE = 50;
	CMDP_TEXT = 52;
	CMDP_THICKNESS = 54;
	CMDP_TIME = 55;
	CMDP_TYPE = 57;
	CMDP_VALUE = 58;
	CMDP_VAR = 59;
	CMDP_VIEWPORT = 61;
	CMDP_ZLEVEL = 63;

// String -> command parameter mappings. Must be arranged in ascending byte order!
const ss_paramlist : array of record
		id : string[9];
		code : byte;
	end = (
// CMDP_DYNAMIC and CMDP_ANY* have special handling, so they're not listed among these.
(id : ''; code : 0),
(id : 'alpha'; code : CMDP_ALPHA),
(id : 'amp'; code : CMDP_BY),
(id : 'amplitude'; code : CMDP_BY),
(id : 'anchorx'; code : CMDP_ANCHORX),
(id : 'anchory'; code : CMDP_ANCHORY),
(id : 'angle'; code : CMDP_ANGLE),
(id : 'ax'; code : CMDP_ANCHORX),
(id : 'ay'; code : CMDP_ANCHORY),
(id : 'box'; code : CMDP_BOX),
(id : 'by'; code : CMDP_BY),
(id : 'color'; code : CMDP_COLOR),
(id : 'count'; code : CMDP_COUNT),
(id : 'fiber'; code : CMDP_NAME),
(id : 'force'; code : CMDP_FORCE),
(id : 'frame'; code : CMDP_FRAME),
(id : 'freq'; code : CMDP_FREQ),
(id : 'gamma'; code : CMDP_GAMMA),
(id : 'gob'; code : CMDP_GOB),
(id : 'h'; code : CMDP_SIZEY),
(id : 'height'; code : CMDP_SIZEY),
(id : 'index'; code : CMDP_INDEX),
(id : 'label'; code : CMDP_LABEL),
(id : 'locx'; code : CMDP_LOCX),
(id : 'locy'; code : CMDP_LOCY),
(id : 'mouseoff'; code : CMDP_MOUSEOFF),
(id : 'mouseon'; code : CMDP_MOUSEON),
(id : 'mouseonly'; code : CMDP_MOUSEONLY),
(id : 'name'; code : CMDP_NAME),
(id : 'noclear'; code : CMDP_NOCLEAR),
(id : 'noprint'; code : CMDP_NOPRINT),
(id : 'nowait'; code : CMDP_NOWAIT),
(id : 'ofsx'; code : CMDP_LOCX),
(id : 'ofsy'; code : CMDP_LOCY),
(id : 'parent'; code : CMDP_PARENT),
(id : 'ratiox'; code : CMDP_RATIOX),
(id : 'ratioy'; code : CMDP_RATIOY),
(id : 'sizex'; code : CMDP_SIZEX),
(id : 'sizey'; code : CMDP_SIZEY),
(id : 'style'; code : CMDP_STYLE),
(id : 'text'; code : CMDP_TEXT),
(id : 'thickness'; code : CMDP_THICKNESS),
(id : 'time'; code : CMDP_TIME),
(id : 'type'; code : CMDP_TYPE),
(id : 'value'; code : CMDP_VALUE),
(id : 'var'; code : CMDP_VAR),
(id : 'viewport'; code : CMDP_VIEWPORT),
(id : 'vp'; code : CMDP_VIEWPORT),
(id : 'w'; code : CMDP_SIZEX),
(id : 'width'; code : CMDP_SIZEX),
(id : 'x'; code : CMDP_LOCX),
(id : 'y'; code : CMDP_LOCY),
(id : 'z'; code : CMDP_ZLEVEL),
(id : 'zlevel'; code : CMDP_ZLEVEL)
);

function GetCmdParamIndex(const name : string) : dword;
// Scans ss_paramlist for an exact match to the input string. Returns index if found, otherwise returns 0.
// The input string should be in lowercase, like all parameters.
var i, min, max : longint;
begin
	// binary search (tried boundless and monobound, no improvement)
	min := 1; max := high(ss_paramlist);
	repeat
		result := (min + max) shr 1;
		i := CompStr(@name[1], @ss_paramlist[result].id[1], length(name), length(ss_paramlist[result].id));
		if i = 0 then exit;
		if i > 0 then
			min := result + 1
		else
			max := result - 1;
	until min > max;
	result := 0;
end;

// These would look nice in a byte enum, but those can't be used as array indexes without casting. :(
// Todo: change to 1-byte enum after ss_cmdparams is removed.
const
	CMD_NOP = 0;
	CMD_LOG = 1;
	CMD_PEEK = 4;
	CMD_POKE = 5;
	CMD_ERROR = 6;
	CMD_AGAIN = 159; // special: repeat previous command

	CMD_MUS_PLAY = 10;
	CMD_MUS_STOP = 11;
	CMD_MUS_PAUSE = 12;
	CMD_MUS_RESUME = 13;

	CMD_META_ENTER = 30;
	CMD_META_EXIT = 31;

	CMD_SELECT = 34;
	CMD_CALL = 35;
	CMD_CASECALL = 36;
	CMD_CASEGOTO = 37;
	CMD_GOTO = 38;
	CMD_RETURN = 39;
	CMD_SCRIPTRET = 158;

	CMD_CHOICE_CALL = 40;
	CMD_CHOICE_CANCEL = 41;
	CMD_CHOICE_GET = 44;
	CMD_CHOICE_GETHIGHLIGHT = 45;
	CMD_CHOICE_GOTO = 46;
	CMD_CHOICE_LIST = 43;
	CMD_CHOICE_OFF = 47;
	CMD_CHOICE_ON = 48;
	CMD_CHOICE_REMOVE = 51;
	CMD_CHOICE_RESET = 52;
	CMD_CHOICE_SET = 53;

	CMD_GFX_ADOPT = 80;
	CMD_GFX_BASH = 81;
	CMD_GFX_CLEARALL = 82;
	CMD_GFX_CLEARANIMS = 83;
	CMD_GFX_CLEARBKG = 84;
	CMD_GFX_CLEARKIDS = 85;
	CMD_GFX_FIND = 100;
	CMD_GFX_FLASH = 86;
	CMD_GFX_GETFRAME = 87;
	CMD_GFX_GETSEQUENCE = 88;
	CMD_GFX_LIST = 89;
	CMD_GFX_MOVE = 90;
	CMD_GFX_PRECACHE = 91;
	CMD_GFX_REMOVE = 92;
	CMD_GFX_SETALPHA = 93;
	CMD_GFX_SETFRAME = 94;
	CMD_GFX_SETSEQUENCE = 95;
	CMD_GFX_SETSOLIDBLIT = 96;
	CMD_GFX_SETSOURCE = 79;
	CMD_GFX_SHOW = 97;
	CMD_GFX_TRANSITION = 99;

	CMD_FX_SKIP = 111;

	CMD_TBOX_CLEAR = 120;
	CMD_TBOX_DECORATE = 122;
	CMD_TBOX_GETPARAM = 123;
	CMD_TBOX_GETTEXT = 125;
	CMD_TBOX_GETUSERINPUT = 124;
	CMD_TBOX_LIST = 126;
	CMD_TBOX_OUTLINE = 127;
	CMD_TBOX_POPIN = 128;
	CMD_TBOX_POPOUT = 129;
	CMD_TBOX_PRINT = 130;
	CMD_TBOX_REMOVE = 131;
	CMD_TBOX_REMOVEDECOR = 132;
	CMD_TBOX_REMOVEOUTLINES = 133;
	CMD_TBOX_RESET = 135;
	CMD_TBOX_SCROLL = 136;
	CMD_TBOX_SETLOC = 142;
	CMD_TBOX_SETPARAM = 146;
	CMD_TBOX_SETSIZE = 148;
	CMD_TBOX_SETTEXTURE = 150;
	CMD_TBOX_USERINPUTSTART = 153;
	CMD_TBOX_USERINPUTSTOP = 154;

	CMD_FIBER_CALLSTACK = 160;
	CMD_FIBER_GETNAME = 161;
	CMD_FIBER_LIST = 162;
	CMD_FIBER_SIGNAL = 163;
	CMD_FIBER_START = 164;
	CMD_FIBER_STOP = 165;
	CMD_FIBER_WAIT = 166;
	CMD_FIBER_WAITKEY = 167;
	CMD_FIBER_WAITSIG = 168;
	CMD_FIBER_WAITTYPING = 169;
	CMD_FIBER_YIELD = 170;

	CMD_EVENT_CREATE_TIMER = 194;
	CMD_EVENT_GETTIMER = 195;
	CMD_EVENT_REMOVE = 196;
	CMD_EVENT_SETLABEL = 199;
	CMD_EVENT_SETTIMER = 200;

	CMD_VIEWPORT_LIST = 201;
	CMD_VIEWPORT_SETGAMMA = 203;
	CMD_VIEWPORT_SETPARAMS = 204;

	CMD_SYS_GETKEYDOWN = 231;
	CMD_SYS_GETKEYLEFT = 232;
	CMD_SYS_GETKEYRIGHT = 233;
	CMD_SYS_GETKEYUP = 234;
	CMD_SYS_GETMOUSEX = 235;
	CMD_SYS_GETMOUSEY = 236;
	CMD_SYS_LOADSTATE = 244;
	CMD_SYS_LOADDAT = 245;
	CMD_SYS_PAUSE = 247;
	CMD_SYS_RESTARTGAME = 248;
	CMD_SYS_SAVESTATE = 249;
	CMD_SYS_SETCURSOR = 250;
	CMD_SYS_SHOWTEXTLOG = 254;
	CMD_SYS_QUIT = 255;
	CMD_INT = 69;

// String -> command mappings. Must be arranged in ascending byte order!
const ss_cmdlist : array of record
		id : string[19];
		code : byte;
	end = (
(id : ''; code : CMD_NOP),
(id : 'call'; code : CMD_CALL),
(id : 'casecall'; code : CMD_CASECALL),
(id : 'casegoto'; code : CMD_CASEGOTO),
(id : 'choice.call'; code : CMD_CHOICE_CALL),
(id : 'choice.cancel'; code : CMD_CHOICE_CANCEL),
(id : 'choice.clear'; code : CMD_CHOICE_RESET),
(id : 'choice.disable'; code : CMD_CHOICE_OFF),
(id : 'choice.enable'; code : CMD_CHOICE_ON),
(id : 'choice.get'; code : CMD_CHOICE_GET),
(id : 'choice.gethighlight'; code : CMD_CHOICE_GETHIGHLIGHT),
(id : 'choice.go'; code : CMD_CHOICE_GOTO),
(id : 'choice.goto'; code : CMD_CHOICE_GOTO),
(id : 'choice.jump'; code : CMD_CHOICE_GOTO),
(id : 'choice.list'; code : CMD_CHOICE_LIST),
(id : 'choice.off'; code : CMD_CHOICE_OFF),
(id : 'choice.on'; code : CMD_CHOICE_ON),
(id : 'choice.remove'; code : CMD_CHOICE_REMOVE),
(id : 'choice.reset'; code : CMD_CHOICE_RESET),
(id : 'choice.set'; code : CMD_CHOICE_SET),
(id : 'error'; code : CMD_ERROR),
(id : 'event.clear'; code : CMD_EVENT_REMOVE),
(id : 'event.create.timer'; code : CMD_EVENT_CREATE_TIMER),
(id : 'event.gettimer'; code : CMD_EVENT_GETTIMER),
(id : 'event.remove'; code : CMD_EVENT_REMOVE),
(id : 'event.remove.all'; code : CMD_EVENT_REMOVE),
(id : 'event.setlabel'; code : CMD_EVENT_SETLABEL),
(id : 'event.settimer'; code : CMD_EVENT_SETTIMER),
(id : 'fiber.callstack'; code : CMD_FIBER_CALLSTACK),
(id : 'fiber.getname'; code : CMD_FIBER_GETNAME),
(id : 'fiber.list'; code : CMD_FIBER_LIST),
(id : 'fiber.signal'; code : CMD_FIBER_SIGNAL),
(id : 'fiber.start'; code : CMD_FIBER_START),
(id : 'fiber.stop'; code : CMD_FIBER_STOP),
(id : 'fiber.wait'; code : CMD_FIBER_WAIT),
(id : 'fiber.waitkey'; code : CMD_FIBER_WAITKEY),
(id : 'fiber.waitsig'; code : CMD_FIBER_WAITSIG),
(id : 'fiber.waitsignal'; code : CMD_FIBER_WAITSIG),
(id : 'fiber.waittyping'; code : CMD_FIBER_WAITTYPING),
(id : 'fiber.yield'; code : CMD_FIBER_YIELD),
(id : 'fx.skip'; code : CMD_FX_SKIP),
(id : 'gfx.adopt'; code : CMD_GFX_ADOPT),
(id : 'gfx.bash'; code : CMD_GFX_BASH),
(id : 'gfx.clearall'; code : CMD_GFX_CLEARALL),
(id : 'gfx.clearanims'; code : CMD_GFX_CLEARANIMS),
(id : 'gfx.clearbkg'; code : CMD_GFX_CLEARBKG),
(id : 'gfx.clearkids'; code : CMD_GFX_CLEARKIDS),
(id : 'gfx.create'; code : CMD_GFX_SHOW),
(id : 'gfx.find'; code : CMD_GFX_FIND),
(id : 'gfx.flash'; code : CMD_GFX_FLASH),
(id : 'gfx.getframe'; code : CMD_GFX_GETFRAME),
(id : 'gfx.getsequence'; code : CMD_GFX_GETSEQUENCE),
(id : 'gfx.list'; code : CMD_GFX_LIST),
(id : 'gfx.move'; code : CMD_GFX_MOVE),
(id : 'gfx.precache'; code : CMD_GFX_PRECACHE),
(id : 'gfx.remove'; code : CMD_GFX_REMOVE),
(id : 'gfx.removeall'; code : CMD_GFX_CLEARALL),
(id : 'gfx.removeanims'; code : CMD_GFX_CLEARANIMS),
(id : 'gfx.removebkg'; code : CMD_GFX_CLEARBKG),
(id : 'gfx.removekids'; code : CMD_GFX_CLEARKIDS),
(id : 'gfx.setalpha'; code : CMD_GFX_SETALPHA),
(id : 'gfx.setframe'; code : CMD_GFX_SETFRAME),
(id : 'gfx.setloc'; code : CMD_GFX_MOVE),
(id : 'gfx.setparent'; code : CMD_GFX_ADOPT),
(id : 'gfx.setsequence'; code : CMD_GFX_SETSEQUENCE),
(id : 'gfx.setsolidblit'; code : CMD_GFX_SETSOLIDBLIT),
(id : 'gfx.setsource'; code : CMD_GFX_SETSOURCE),
(id : 'gfx.show'; code : CMD_GFX_SHOW),
(id : 'gfx.transition'; code : CMD_GFX_TRANSITION),
(id : 'goto'; code : CMD_GOTO),
(id : 'int'; code : CMD_INT),
(id : 'jump'; code : CMD_GOTO),
(id : 'log'; code : CMD_LOG),
(id : 'meta.enter'; code : CMD_META_ENTER),
(id : 'meta.exit'; code : CMD_META_EXIT),
(id : 'move'; code : CMD_GFX_MOVE),
(id : 'mus.pause'; code : CMD_MUS_PAUSE),
(id : 'mus.play'; code : CMD_MUS_PLAY),
(id : 'mus.resume'; code : CMD_MUS_RESUME),
(id : 'mus.stop'; code : CMD_MUS_STOP),
(id : 'nop'; code : CMD_NOP),
(id : 'pause'; code : CMD_SYS_PAUSE),
(id : 'peek'; code : CMD_PEEK),
(id : 'poke'; code : CMD_POKE),
(id : 'print'; code : CMD_TBOX_PRINT),
(id : 'return'; code : CMD_RETURN),
(id : 'scr.return'; code : CMD_SCRIPTRET),
(id : 'select'; code : CMD_SELECT),
(id : 'show'; code : CMD_GFX_SHOW),
(id : 'signal'; code : CMD_FIBER_SIGNAL),
(id : 'sleep'; code : CMD_FIBER_WAIT),
(id : 'start'; code : CMD_FIBER_START),
(id : 'stop'; code : CMD_FIBER_STOP),
(id : 'sys.getkeydown'; code : CMD_SYS_GETKEYDOWN),
(id : 'sys.getkeyleft'; code : CMD_SYS_GETKEYLEFT),
(id : 'sys.getkeyright'; code : CMD_SYS_GETKEYRIGHT),
(id : 'sys.getkeyup'; code : CMD_SYS_GETKEYUP),
(id : 'sys.getmousex'; code : CMD_SYS_GETMOUSEX),
(id : 'sys.getmousey'; code : CMD_SYS_GETMOUSEY),
(id : 'sys.loaddat'; code : CMD_SYS_LOADDAT),
(id : 'sys.loadstate'; code : CMD_SYS_LOADSTATE),
(id : 'sys.pause'; code : CMD_SYS_PAUSE),
(id : 'sys.quit'; code : CMD_SYS_QUIT),
(id : 'sys.restartgame'; code : CMD_SYS_RESTARTGAME),
(id : 'sys.savestate'; code : CMD_SYS_SAVESTATE),
(id : 'sys.setcursor'; code : CMD_SYS_SETCURSOR),
(id : 'sys.showtextlog'; code : CMD_SYS_SHOWTEXTLOG),
(id : 'tbox.addoutline'; code : CMD_TBOX_OUTLINE),
(id : 'tbox.clear'; code : CMD_TBOX_CLEAR),
(id : 'tbox.decorate'; code : CMD_TBOX_DECORATE),
(id : 'tbox.getparam'; code : CMD_TBOX_GETPARAM),
(id : 'tbox.gettext'; code : CMD_TBOX_GETTEXT),
(id : 'tbox.getuserinput'; code : CMD_TBOX_GETUSERINPUT),
(id : 'tbox.list'; code : CMD_TBOX_LIST),
(id : 'tbox.move'; code : CMD_TBOX_SETLOC),
(id : 'tbox.outline'; code : CMD_TBOX_OUTLINE),
(id : 'tbox.popin'; code : CMD_TBOX_POPIN),
(id : 'tbox.popout'; code : CMD_TBOX_POPOUT),
(id : 'tbox.print'; code : CMD_TBOX_PRINT),
(id : 'tbox.remove'; code : CMD_TBOX_REMOVE),
(id : 'tbox.removedecor'; code : CMD_TBOX_REMOVEDECOR),
(id : 'tbox.removeoutlines'; code : CMD_TBOX_REMOVEOUTLINES),
(id : 'tbox.reset'; code : CMD_TBOX_RESET),
(id : 'tbox.scroll'; code : CMD_TBOX_SCROLL),
(id : 'tbox.setloc'; code : CMD_TBOX_SETLOC),
(id : 'tbox.setparam'; code : CMD_TBOX_SETPARAM),
(id : 'tbox.setsize'; code : CMD_TBOX_SETSIZE),
(id : 'tbox.settexture'; code : CMD_TBOX_SETTEXTURE),
(id : 'tbox.userinputstart'; code : CMD_TBOX_USERINPUTSTART),
(id : 'tbox.userinputstop'; code : CMD_TBOX_USERINPUTSTOP),
(id : 'transition'; code : CMD_GFX_TRANSITION),
(id : 'viewport.list'; code : CMD_VIEWPORT_LIST),
(id : 'viewport.setgamma'; code : CMD_VIEWPORT_SETGAMMA),
(id : 'viewport.setparams'; code : CMD_VIEWPORT_SETPARAMS),
(id : 'viewport.transition'; code : CMD_GFX_TRANSITION),
(id : 'wait'; code : CMD_FIBER_WAIT),
(id : 'waitkey'; code : CMD_FIBER_WAITKEY),
(id : 'waitsig'; code : CMD_FIBER_WAITSIG),
(id : 'waitsignal'; code : CMD_FIBER_WAITSIG),
(id : 'waittyping'; code : CMD_FIBER_WAITTYPING),
(id : 'yield'; code : CMD_FIBER_YIELD)
);

function GetCmdIndex(const name : string) : dword;
// Scans ss_cmdlist for an exact match to the input string. Returns index if found, otherwise returns 0.
// The input string should be in lowercase, like all commands.
var i, min, max : longint;
begin
	// binary search (tried boundless and monobound, no improvement)
	min := 1; max := high(ss_cmdlist);
	repeat
		result := (min + max) shr 1;
		i := CompStr(@name[1], @ss_cmdlist[result].id[1], length(name), length(ss_cmdlist[result].id));
		if i = 0 then exit;
		if i > 0 then
			min := result + 1
		else
			max := result - 1;
	until min > max;
	result := 0;
end;

// Command parameter type enums.
{$push}{$scopedEnums off}
type ESSArgt = (ARG_INVALID = 0, ARG_NUM = 1, ARG_STR = 2);
{$pop}

// Parameter -> Command associations.
// Before use, you MUST call ss_cmdparams_init!
// To use, query ss_cmdparams[COMMANDCODE][PARAMCODE].
// Invalid parameters are 0; valid parameters have the parameter type enum.
// The command implementation (sakufibercmds) is responsible for selecting which parameters to use dynamically, if the
// parameter name is omitted in script code. Each command can pull the parameters in its preferred order.
var ss_cmdparams : array[0..255] of array[0..63] of ESSArgt;
	ss_paramtype : array[0..63] of ESSArgt;
// todo: essargt = byte explicitly, fill ss_cmdparams without for
// ... actually...
// The essargt in cmdparams is actually never used except to check it's not ARG_INVALID, 0. Actual type verification
// is always just checked against ss_paramtype. This is since in bytecode the params are kind of separate from the
// commands and so are evaluated on their own merits.
// Cmdparams is actually nearly unused, only found at:
// - compiler: if ss_cmdparams[CMD_TBOX_PRINT][CMDP_BOX] = ARG_INVALID then ss_cmdparams_init; (redundant)
// - base-all: if ss_cmdparams[CMD_TBOX_PRINT][CMDP_BOX] = ARG_INVALID then ss_cmdparams_init; (redundant)
// - fiberhub-imp: for i := 0 to high(ss_cmdparams[c]) do ... (printing help for a command!!)
// At one point this was used to reject inappropriate params at compile time, but that caused some unpleasant side
// effect that I forget now, so I removed the check. Inappropriate params are just ignored now.
// Since the final arbiter of which params are used is fibercmds.pas, and params aren't always consumed in the same
// order and can have varying defaults, it seems the cmd-param associations are better documented in fibercmds.pas
// right beside the implementations.

procedure ss_cmdparams_init;
var i : dword;
begin
	// Explicitly zero out everything.
	for i := high(ss_cmdparams) downto 0 do
		filldword(ss_cmdparams[i][0], length(ss_cmdparams[0]) shr 2, 0);
	filldword(ss_paramtype, length(ss_paramtype) shr 2, 0);

	// Set up the argument types for all parameters.
	ss_paramtype[CMDP_ANYNUMBER] := ARG_NUM;
	ss_paramtype[CMDP_ANYSTRING] := ARG_STR;
	ss_paramtype[CMDP_ALPHA] := ARG_NUM;
	ss_paramtype[CMDP_ANCHORX] := ARG_NUM;
	ss_paramtype[CMDP_ANCHORY] := ARG_NUM;
	ss_paramtype[CMDP_ANGLE] := ARG_NUM;
	ss_paramtype[CMDP_BOX] := ARG_STR;
	ss_paramtype[CMDP_BY] := ARG_NUM;
	ss_paramtype[CMDP_COLOR] := ARG_NUM;
	ss_paramtype[CMDP_COUNT] := ARG_NUM;
	ss_paramtype[CMDP_FORCE] := ARG_NUM;
	ss_paramtype[CMDP_FRAME] := ARG_NUM;
	ss_paramtype[CMDP_FREQ] := ARG_NUM;
	ss_paramtype[CMDP_GAMMA] := ARG_NUM;
	ss_paramtype[CMDP_GOB] := ARG_STR;
	ss_paramtype[CMDP_INDEX] := ARG_NUM;
	ss_paramtype[CMDP_LABEL] := ARG_STR;
	ss_paramtype[CMDP_LOCX] := ARG_NUM;
	ss_paramtype[CMDP_LOCY] := ARG_NUM;
	ss_paramtype[CMDP_MOUSEOFF] := ARG_STR;
	ss_paramtype[CMDP_MOUSEON] := ARG_STR;
	ss_paramtype[CMDP_MOUSEONLY] := ARG_NUM;
	ss_paramtype[CMDP_NAME] := ARG_STR;
	ss_paramtype[CMDP_NOCLEAR] := ARG_NUM;
	ss_paramtype[CMDP_NOPRINT] := ARG_NUM;
	ss_paramtype[CMDP_NOWAIT] := ARG_NUM;
	ss_paramtype[CMDP_PARENT] := ARG_STR;
	ss_paramtype[CMDP_RATIOX] := ARG_NUM;
	ss_paramtype[CMDP_RATIOY] := ARG_NUM;
	ss_paramtype[CMDP_SIZEX] := ARG_NUM;
	ss_paramtype[CMDP_SIZEY] := ARG_NUM;
	ss_paramtype[CMDP_STYLE] := ARG_STR;
	ss_paramtype[CMDP_TEXT] := ARG_STR;
	ss_paramtype[CMDP_THICKNESS] := ARG_NUM;
	ss_paramtype[CMDP_TIME] := ARG_NUM;
	ss_paramtype[CMDP_TYPE] := ARG_STR;
	ss_paramtype[CMDP_VALUE] := ARG_NUM;
	ss_paramtype[CMDP_VAR] := ARG_STR;
	ss_paramtype[CMDP_VIEWPORT] := ARG_NUM;
	ss_paramtype[CMDP_ZLEVEL] := ARG_NUM;

	// === Various jump commands === (CMDP_name defaults to current script, CMDP_label defaults to empty/fail)
	ss_cmdparams[CMD_CALL][CMDP_LABEL] := ss_paramtype[CMDP_LABEL];
	ss_cmdparams[CMD_CALL][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: 0, first index
	ss_cmdparams[CMD_CASECALL][CMDP_INDEX] := ss_paramtype[CMDP_INDEX];
	ss_cmdparams[CMD_CASEGOTO][CMDP_INDEX] := ss_paramtype[CMDP_INDEX];
	ss_cmdparams[CMD_CASECALL][CMDP_LABEL] := ss_paramtype[CMDP_LABEL];
	ss_cmdparams[CMD_CASEGOTO][CMDP_LABEL] := ss_paramtype[CMDP_LABEL];
	ss_cmdparams[CMD_GOTO][CMDP_LABEL] := ss_paramtype[CMDP_LABEL];
	ss_cmdparams[CMD_GOTO][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: index -1, empty string
	ss_cmdparams[CMD_SELECT][CMDP_INDEX] := ss_paramtype[CMDP_INDEX];
	ss_cmdparams[CMD_SELECT][CMDP_TEXT] := ss_paramtype[CMDP_TEXT];

	// === System commands ===
	// default: empty string
	ss_cmdparams[CMD_LOG][CMDP_TEXT] := ss_paramtype[CMDP_TEXT];
	// default: "error"
	ss_cmdparams[CMD_ERROR][CMDP_TEXT] := ss_paramtype[CMDP_TEXT];
	// default: default viewport
	ss_cmdparams[CMD_SYS_GETMOUSEX][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	ss_cmdparams[CMD_SYS_GETMOUSEY][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	// default: empty name, loaddat fails
	ss_cmdparams[CMD_SYS_LOADDAT][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: index 0
	ss_cmdparams[CMD_SYS_LOADSTATE][CMDP_INDEX] := ss_paramtype[CMDP_INDEX];
	// default: force 0 disabled
	ss_cmdparams[CMD_SYS_QUIT][CMDP_FORCE] := ss_paramtype[CMDP_FORCE];
	// default: index 0
	ss_cmdparams[CMD_SYS_SAVESTATE][CMDP_INDEX] := ss_paramtype[CMDP_INDEX];
	// default: "Save with no name"
	ss_cmdparams[CMD_SYS_SAVESTATE][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: empty gob name = no cursor override
	ss_cmdparams[CMD_SYS_SETCURSOR][CMDP_GOB] := ss_paramtype[CMDP_GOB];

	// === Textbox commands === (CMDP_box defaults to gamevar.defaulttextbox)
	// default: clear all boxes
	ss_cmdparams[CMD_TBOX_CLEAR][CMDP_BOX] := ss_paramtype[CMDP_BOX];

	ss_cmdparams[CMD_TBOX_DECORATE][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: empty gob name, decorate fails
	ss_cmdparams[CMD_TBOX_DECORATE][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: 0,0
	ss_cmdparams[CMD_TBOX_DECORATE][CMDP_LOCX] := ss_paramtype[CMDP_LOCX];
	ss_cmdparams[CMD_TBOX_DECORATE][CMDP_LOCY] := ss_paramtype[CMDP_LOCY];
	// default: 0,0
	ss_cmdparams[CMD_TBOX_DECORATE][CMDP_SIZEX] := ss_paramtype[CMDP_SIZEX];
	ss_cmdparams[CMD_TBOX_DECORATE][CMDP_SIZEY] := ss_paramtype[CMDP_SIZEY];
	// default: 0,0
	ss_cmdparams[CMD_TBOX_DECORATE][CMDP_ANCHORX] := ss_paramtype[CMDP_ANCHORX];
	ss_cmdparams[CMD_TBOX_DECORATE][CMDP_ANCHORY] := ss_paramtype[CMDP_ANCHORY];
	// default: 0
	ss_cmdparams[CMD_TBOX_DECORATE][CMDP_FRAME] := ss_paramtype[CMDP_FRAME];

	// default: default game text box
	ss_cmdparams[CMD_TBOX_GETPARAM][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: empty string, getparam fails
	ss_cmdparams[CMD_TBOX_GETPARAM][CMDP_NAME] := ss_paramtype[CMDP_NAME];

	// default: game text box
	ss_cmdparams[CMD_TBOX_GETTEXT][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: game text box
	ss_cmdparams[CMD_TBOX_GETUSERINPUT][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: all boxes
	ss_cmdparams[CMD_TBOX_LIST][CMDP_BOX] := ss_paramtype[CMDP_BOX];

	ss_cmdparams[CMD_TBOX_OUTLINE][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: black
	ss_cmdparams[CMD_TBOX_OUTLINE][CMDP_COLOR] := ss_paramtype[CMDP_COLOR];
	// default: 256
	ss_cmdparams[CMD_TBOX_OUTLINE][CMDP_THICKNESS] := ss_paramtype[CMDP_THICKNESS];
	// default: 0,0
	ss_cmdparams[CMD_TBOX_OUTLINE][CMDP_LOCX] := ss_paramtype[CMDP_LOCX];
	ss_cmdparams[CMD_TBOX_OUTLINE][CMDP_LOCY] := ss_paramtype[CMDP_LOCY];
	// default: 0
	ss_cmdparams[CMD_TBOX_OUTLINE][CMDP_ALPHA] := ss_paramtype[CMDP_ALPHA];

	ss_cmdparams[CMD_TBOX_POPIN][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	ss_cmdparams[CMD_TBOX_POPIN][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	ss_cmdparams[CMD_TBOX_POPIN][CMDP_STYLE] := ss_paramtype[CMDP_STYLE];
	ss_cmdparams[CMD_TBOX_POPOUT][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	ss_cmdparams[CMD_TBOX_POPOUT][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	ss_cmdparams[CMD_TBOX_POPOUT][CMDP_STYLE] := ss_paramtype[CMDP_STYLE];

	// default: game text box
	ss_cmdparams[CMD_TBOX_PRINT][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: empty string
	ss_cmdparams[CMD_TBOX_PRINT][CMDP_TEXT] := ss_paramtype[CMDP_TEXT];

	ss_cmdparams[CMD_TBOX_REMOVE][CMDP_BOX] := ss_paramtype[CMDP_BOX];

	ss_cmdparams[CMD_TBOX_REMOVEDECOR][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: empty gob name, removes all decorations
	ss_cmdparams[CMD_TBOX_REMOVEDECOR][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	ss_cmdparams[CMD_TBOX_REMOVEOUTLINES][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: all boxes
	ss_cmdparams[CMD_TBOX_RESET][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: default game text box
	ss_cmdparams[CMD_TBOX_SCROLL][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: bottom of content
	ss_cmdparams[CMD_TBOX_SCROLL][CMDP_LOCY] := ss_paramtype[CMDP_LOCY];
	// default: 0 msec
	ss_cmdparams[CMD_TBOX_SCROLL][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	// default: "linear"
	ss_cmdparams[CMD_TBOX_SCROLL][CMDP_STYLE] := ss_paramtype[CMDP_STYLE];
	// default: false, capped to content area
	ss_cmdparams[CMD_TBOX_SCROLL][CMDP_FORCE] := ss_paramtype[CMDP_FORCE];

	ss_cmdparams[CMD_TBOX_SETLOC][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: current location or 0,0
	ss_cmdparams[CMD_TBOX_SETLOC][CMDP_LOCX] := ss_paramtype[CMDP_LOCX];
	ss_cmdparams[CMD_TBOX_SETLOC][CMDP_LOCY] := ss_paramtype[CMDP_LOCY];
	// default: 0 msec
	ss_cmdparams[CMD_TBOX_SETLOC][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	// default: current anchor or 0,0
	ss_cmdparams[CMD_TBOX_SETLOC][CMDP_ANCHORX] := ss_paramtype[CMDP_ANCHORX];
	ss_cmdparams[CMD_TBOX_SETLOC][CMDP_ANCHORY] := ss_paramtype[CMDP_ANCHORY];
	// default: "linear"
	ss_cmdparams[CMD_TBOX_SETLOC][CMDP_STYLE] := ss_paramtype[CMDP_STYLE];

	// default: default game text box
	ss_cmdparams[CMD_TBOX_SETPARAM][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	ss_cmdparams[CMD_TBOX_SETPARAM][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	ss_cmdparams[CMD_TBOX_SETPARAM][CMDP_VALUE] := ss_paramtype[CMDP_VALUE];
	ss_cmdparams[CMD_TBOX_SETPARAM][CMDP_TEXT] := ss_paramtype[CMDP_TEXT];

	ss_cmdparams[CMD_TBOX_SETSIZE][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: current size or 0,0
	ss_cmdparams[CMD_TBOX_SETSIZE][CMDP_SIZEX] := ss_paramtype[CMDP_SIZEX];
	ss_cmdparams[CMD_TBOX_SETSIZE][CMDP_SIZEY] := ss_paramtype[CMDP_SIZEY];
	// default: 0 msec
	ss_cmdparams[CMD_TBOX_SETSIZE][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	// default: "linear"
	ss_cmdparams[CMD_TBOX_SETSIZE][CMDP_STYLE] := ss_paramtype[CMDP_STYLE];

	ss_cmdparams[CMD_TBOX_SETTEXTURE][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: empty texture graphic name
	ss_cmdparams[CMD_TBOX_SETTEXTURE][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: "normal"
	ss_cmdparams[CMD_TBOX_SETTEXTURE][CMDP_STYLE] := ss_paramtype[CMDP_STYLE];
	// default: "stretched"
	ss_cmdparams[CMD_TBOX_SETTEXTURE][CMDP_TYPE] := ss_paramtype[CMDP_TYPE];
	// default: frame 0
	ss_cmdparams[CMD_TBOX_SETTEXTURE][CMDP_FRAME] := ss_paramtype[CMDP_FRAME];

	// default: game text box
	ss_cmdparams[CMD_TBOX_USERINPUTSTART][CMDP_BOX] := ss_paramtype[CMDP_BOX];
	// default: empty initial text
	ss_cmdparams[CMD_TBOX_USERINPUTSTART][CMDP_TEXT] := ss_paramtype[CMDP_TEXT];

	// default: empty, disables user input in all boxes
	ss_cmdparams[CMD_TBOX_USERINPUTSTOP][CMDP_BOX] := ss_paramtype[CMDP_BOX];

	// === Choice commands ===
	ss_cmdparams[CMD_CHOICE_CANCEL][CMDP_NOCLEAR] := ss_paramtype[CMDP_NOCLEAR];
	// default: empty, starts at root choice level
	ss_cmdparams[CMD_CHOICE_CALL][CMDP_PARENT] := ss_paramtype[CMDP_PARENT];
	ss_cmdparams[CMD_CHOICE_GET][CMDP_PARENT] := ss_paramtype[CMDP_PARENT];
	ss_cmdparams[CMD_CHOICE_GOTO][CMDP_PARENT] := ss_paramtype[CMDP_PARENT];
	// default: noclear=0, choicebox is cleared before printing choices
	ss_cmdparams[CMD_CHOICE_CALL][CMDP_NOCLEAR] := ss_paramtype[CMDP_NOCLEAR];
	ss_cmdparams[CMD_CHOICE_GET][CMDP_NOCLEAR] := ss_paramtype[CMDP_NOCLEAR];
	ss_cmdparams[CMD_CHOICE_GOTO][CMDP_NOCLEAR] := ss_paramtype[CMDP_NOCLEAR];
	// default: noprint=0, choices are automatically printed in the box
	ss_cmdparams[CMD_CHOICE_CALL][CMDP_NOPRINT] := ss_paramtype[CMDP_NOPRINT];
	ss_cmdparams[CMD_CHOICE_GET][CMDP_NOPRINT] := ss_paramtype[CMDP_NOPRINT];
	ss_cmdparams[CMD_CHOICE_GOTO][CMDP_NOPRINT] := ss_paramtype[CMDP_NOPRINT];
	// default: nowait=0, blocks until user has made a choice
	ss_cmdparams[CMD_CHOICE_GET][CMDP_NOWAIT] := ss_paramtype[CMDP_NOWAIT];

	// default: empty string, all choices
	ss_cmdparams[CMD_CHOICE_OFF][CMDP_TEXT] := ss_paramtype[CMDP_TEXT];
	ss_cmdparams[CMD_CHOICE_ON][CMDP_TEXT] := ss_paramtype[CMDP_TEXT];
	// default: value=1, enable choice; pass value=0 to disable instead
	ss_cmdparams[CMD_CHOICE_ON][CMDP_VALUE] := ss_paramtype[CMDP_VALUE];
	ss_cmdparams[CMD_CHOICE_REMOVE][CMDP_TEXT] := ss_paramtype[CMDP_TEXT];
	// default: first free index
	ss_cmdparams[CMD_CHOICE_SET][CMDP_INDEX] := ss_paramtype[CMDP_INDEX];
	// default: empty string, choice set fails
	ss_cmdparams[CMD_CHOICE_SET][CMDP_TEXT] := ss_paramtype[CMDP_TEXT];
	// default: undefined labels
	ss_cmdparams[CMD_CHOICE_SET][CMDP_LABEL] := ss_paramtype[CMDP_LABEL];
	// default: undefined tracking var
	ss_cmdparams[CMD_CHOICE_SET][CMDP_VAR] := ss_paramtype[CMDP_VAR];
	// default: initial value is 0 enabled
	ss_cmdparams[CMD_CHOICE_SET][CMDP_VALUE] := ss_paramtype[CMDP_VALUE];

	// === Event commands === (CMDP_label defaults to empty, no trigger)
	// default: empty string, create event fails
	ss_cmdparams[CMD_EVENT_CREATE_TIMER][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	ss_cmdparams[CMD_EVENT_CREATE_TIMER][CMDP_LABEL] := ss_paramtype[CMDP_LABEL];
	// default: 1000 msec, negative value makes it a one-shot
	ss_cmdparams[CMD_EVENT_CREATE_TIMER][CMDP_FREQ] := ss_paramtype[CMDP_FREQ];
	// default: current time value=0
	ss_cmdparams[CMD_EVENT_CREATE_TIMER][CMDP_TIME] := ss_paramtype[CMDP_TIME];

	// default: empty string
	ss_cmdparams[CMD_EVENT_GETTIMER][CMDP_NAME] := ss_paramtype[CMDP_NAME];

	// default: empty string, remove all events
	ss_cmdparams[CMD_EVENT_REMOVE][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	ss_cmdparams[CMD_EVENT_REMOVE][CMDP_GOB] := ss_paramtype[CMDP_GOB];

	// default: empty string
	ss_cmdparams[CMD_EVENT_SETLABEL][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	ss_cmdparams[CMD_EVENT_SETLABEL][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: no change
	ss_cmdparams[CMD_EVENT_SETLABEL][CMDP_LABEL] := ss_paramtype[CMDP_LABEL];
	ss_cmdparams[CMD_EVENT_SETLABEL][CMDP_MOUSEON] := ss_paramtype[CMDP_MOUSEON];
	ss_cmdparams[CMD_EVENT_SETLABEL][CMDP_MOUSEOFF] := ss_paramtype[CMDP_MOUSEOFF];
	// default: mouseonly=0
	ss_cmdparams[CMD_EVENT_SETLABEL][CMDP_MOUSEONLY] := ss_paramtype[CMDP_MOUSEONLY];
	// default: current state value=0 (not overed)
	ss_cmdparams[CMD_EVENT_SETLABEL][CMDP_VALUE] := ss_paramtype[CMDP_VALUE];

	// default: empty string, set fails
	ss_cmdparams[CMD_EVENT_SETTIMER][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: current time value=0
	ss_cmdparams[CMD_EVENT_SETTIMER][CMDP_TIME] := ss_paramtype[CMDP_TIME];

	// === Fiber commands ===
	// default: "MAIN" fiber
	ss_cmdparams[CMD_FIBER_CALLSTACK][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: all fibers
	ss_cmdparams[CMD_FIBER_LIST][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: empty string, all fibers get a signal
	ss_cmdparams[CMD_FIBER_SIGNAL][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: empty string, fiber start fails
	ss_cmdparams[CMD_FIBER_START][CMDP_LABEL] := ss_paramtype[CMDP_LABEL];
	// default: same name as label string
	ss_cmdparams[CMD_FIBER_START][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: stop self
	ss_cmdparams[CMD_FIBER_STOP][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: sleep self
	ss_cmdparams[CMD_FIBER_WAIT][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: 0 msec, fiber waits for all effects to end, or signal
	ss_cmdparams[CMD_FIBER_WAIT][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	// default: noclear = 0, box is cleared
	ss_cmdparams[CMD_FIBER_WAITKEY][CMDP_NOCLEAR] := ss_paramtype[CMDP_NOCLEAR];

	// === Graphics commands === (CMDP_gob defaults to empty, fail)
	// default: empty string, gfx.adopt fails
	ss_cmdparams[CMD_GFX_ADOPT][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: empty string, gob is orphaned
	ss_cmdparams[CMD_GFX_ADOPT][CMDP_PARENT] := ss_paramtype[CMDP_PARENT];

	// default: default viewport's background gob
	ss_cmdparams[CMD_GFX_BASH][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: 1024 msec
	ss_cmdparams[CMD_GFX_BASH][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	// default: 32768 = 1 Hz
	ss_cmdparams[CMD_GFX_BASH][CMDP_FREQ] := ss_paramtype[CMDP_FREQ];
	// default: amplitude 8192, 25% of gob size
	ss_cmdparams[CMD_GFX_BASH][CMDP_BY] := ss_paramtype[CMDP_BY];
	// default: random angle
	ss_cmdparams[CMD_GFX_BASH][CMDP_ANGLE] := ss_paramtype[CMDP_ANGLE];

	// default: default viewport
	ss_cmdparams[CMD_GFX_CLEARALL][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	ss_cmdparams[CMD_GFX_CLEARANIMS][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	ss_cmdparams[CMD_GFX_CLEARBKG][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];

	ss_cmdparams[CMD_GFX_CLEARKIDS][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	ss_cmdparams[CMD_GFX_CLEARKIDS][CMDP_GOB] := ss_paramtype[CMDP_GOB];

	// default find: topmost that matches defined values
	ss_cmdparams[CMD_GFX_FIND][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	ss_cmdparams[CMD_GFX_FIND][CMDP_TYPE] := ss_paramtype[CMDP_TYPE];
	ss_cmdparams[CMD_GFX_FIND][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	ss_cmdparams[CMD_GFX_FIND][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];

	// default: default viewport
	ss_cmdparams[CMD_GFX_FLASH][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	// default: 384 msec
	ss_cmdparams[CMD_GFX_FLASH][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	// default: FFFE
	ss_cmdparams[CMD_GFX_FLASH][CMDP_COLOR] := ss_paramtype[CMDP_COLOR];
	// default: linear
	ss_cmdparams[CMD_GFX_FLASH][CMDP_STYLE] := ss_paramtype[CMDP_STYLE];
	// default: 120
	ss_cmdparams[CMD_GFX_FLASH][CMDP_ZLEVEL] := ss_paramtype[CMDP_ZLEVEL];
	// default: 1 time
	ss_cmdparams[CMD_GFX_FLASH][CMDP_COUNT] := ss_paramtype[CMDP_COUNT];

	ss_cmdparams[CMD_GFX_GETFRAME][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	ss_cmdparams[CMD_GFX_GETSEQUENCE][CMDP_GOB] := ss_paramtype[CMDP_GOB];

	// default: all gobs
	ss_cmdparams[CMD_GFX_LIST][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	ss_cmdparams[CMD_GFX_MOVE][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: current location
	ss_cmdparams[CMD_GFX_MOVE][CMDP_LOCX] := ss_paramtype[CMDP_LOCX];
	ss_cmdparams[CMD_GFX_MOVE][CMDP_LOCY] := ss_paramtype[CMDP_LOCY];
	// default: current anchor or 0,0
	ss_cmdparams[CMD_GFX_MOVE][CMDP_ANCHORX] := ss_paramtype[CMDP_ANCHORX];
	ss_cmdparams[CMD_GFX_MOVE][CMDP_ANCHORY] := ss_paramtype[CMDP_ANCHORY];
	// default: 0 msec
	ss_cmdparams[CMD_GFX_MOVE][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	// default: "linear"
	ss_cmdparams[CMD_GFX_MOVE][CMDP_STYLE] := ss_paramtype[CMDP_STYLE];

	// default: empty string, gfx precache fails
	ss_cmdparams[CMD_GFX_PRECACHE][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: default viewport
	ss_cmdparams[CMD_GFX_PRECACHE][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	ss_cmdparams[CMD_GFX_REMOVE][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	ss_cmdparams[CMD_GFX_SETALPHA][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: alpha 255, fully opaque
	ss_cmdparams[CMD_GFX_SETALPHA][CMDP_ALPHA] := ss_paramtype[CMDP_ALPHA];
	// default: 0 msec
	ss_cmdparams[CMD_GFX_SETALPHA][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	ss_cmdparams[CMD_GFX_SETFRAME][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: frame 0
	ss_cmdparams[CMD_GFX_SETFRAME][CMDP_FRAME] := ss_paramtype[CMDP_FRAME];
	ss_cmdparams[CMD_GFX_SETSEQUENCE][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: index 0
	ss_cmdparams[CMD_GFX_SETSEQUENCE][CMDP_INDEX] := ss_paramtype[CMDP_INDEX];
	ss_cmdparams[CMD_GFX_SETSOLIDBLIT][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: 0, disables solid blit effect
	ss_cmdparams[CMD_GFX_SETSOLIDBLIT][CMDP_COLOR] := ss_paramtype[CMDP_COLOR];
	// default: empty string, gfx.setsource fails
	ss_cmdparams[CMD_GFX_SETSOURCE][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: empty string, becomes an invisible virtual gob
	ss_cmdparams[CMD_GFX_SETSOURCE][CMDP_NAME] := ss_paramtype[CMDP_NAME];

	// default: empty string, gfx show fails
	ss_cmdparams[CMD_GFX_SHOW][CMDP_GOB] := ss_paramtype[CMDP_GOB];
	// default: sprite, or anim if an animation is defined for this graphic
	ss_cmdparams[CMD_GFX_SHOW][CMDP_TYPE] := ss_paramtype[CMDP_TYPE];
	// default: same name as graphic named in the "gob" parameter
	ss_cmdparams[CMD_GFX_SHOW][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: empty string, auto-adopt
	ss_cmdparams[CMD_GFX_SHOW][CMDP_PARENT] := ss_paramtype[CMDP_PARENT];
	// default: 0,0
	ss_cmdparams[CMD_GFX_SHOW][CMDP_LOCX] := ss_paramtype[CMDP_LOCX];
	ss_cmdparams[CMD_GFX_SHOW][CMDP_LOCY] := ss_paramtype[CMDP_LOCY];
	// default: gamevar.defaultviewport
	ss_cmdparams[CMD_GFX_SHOW][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	// default: frame 0
	ss_cmdparams[CMD_GFX_SHOW][CMDP_FRAME] := ss_paramtype[CMDP_FRAME];
	// default: 0
	ss_cmdparams[CMD_GFX_SHOW][CMDP_ZLEVEL] := ss_paramtype[CMDP_ZLEVEL];
	// default: 0,0
	ss_cmdparams[CMD_GFX_SHOW][CMDP_ANCHORX] := ss_paramtype[CMDP_ANCHORX];
	ss_cmdparams[CMD_GFX_SHOW][CMDP_ANCHORY] := ss_paramtype[CMDP_ANCHORY];
	// default: 0,0 = use backing graphic's size
	ss_cmdparams[CMD_GFX_SHOW][CMDP_SIZEX] := ss_paramtype[CMDP_SIZEX];
	ss_cmdparams[CMD_GFX_SHOW][CMDP_SIZEY] := ss_paramtype[CMDP_SIZEY];
	// default: alpha 255, fully opaque
	ss_cmdparams[CMD_GFX_SHOW][CMDP_ALPHA] := ss_paramtype[CMDP_ALPHA];
	// default: empty string, no trigger labels
	ss_cmdparams[CMD_GFX_SHOW][CMDP_LABEL] := ss_paramtype[CMDP_LABEL];
	ss_cmdparams[CMD_GFX_SHOW][CMDP_MOUSEON] := ss_paramtype[CMDP_MOUSEON];
	ss_cmdparams[CMD_GFX_SHOW][CMDP_MOUSEOFF] := ss_paramtype[CMDP_MOUSEOFF];

	// default: instant
	ss_cmdparams[CMD_GFX_TRANSITION][CMDP_STYLE] := ss_paramtype[CMDP_STYLE];
	// default: 0 for instant (index is ignored if style is defined)
	ss_cmdparams[CMD_GFX_TRANSITION][CMDP_INDEX] := ss_paramtype[CMDP_INDEX];
	// default: 768 msec
	ss_cmdparams[CMD_GFX_TRANSITION][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	// default: gamevar.defaultviewport
	ss_cmdparams[CMD_GFX_TRANSITION][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	// default: 100
	ss_cmdparams[CMD_GFX_TRANSITION][CMDP_ZLEVEL] := ss_paramtype[CMDP_ZLEVEL];

	// === Viewport commands ===
	// default: all viewports
	ss_cmdparams[CMD_VIEWPORT_LIST][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	// default: gamma 1.0
	ss_cmdparams[CMD_VIEWPORT_SETGAMMA][CMDP_GAMMA] := ss_paramtype[CMDP_GAMMA];
	// default: viewport 0
	ss_cmdparams[CMD_VIEWPORT_SETGAMMA][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	// default: 0 msec
	ss_cmdparams[CMD_VIEWPORT_SETGAMMA][CMDP_TIME] := ss_paramtype[CMDP_TIME];

	// default: viewport 1, can't modify viewport 0
	ss_cmdparams[CMD_VIEWPORT_SETPARAMS][CMDP_VIEWPORT] := ss_paramtype[CMDP_VIEWPORT];
	// default: current parent
	ss_cmdparams[CMD_VIEWPORT_SETPARAMS][CMDP_PARENT] := ss_paramtype[CMDP_PARENT];
	// default: current location
	ss_cmdparams[CMD_VIEWPORT_SETPARAMS][CMDP_LOCX] := ss_paramtype[CMDP_LOCX];
	ss_cmdparams[CMD_VIEWPORT_SETPARAMS][CMDP_LOCY] := ss_paramtype[CMDP_LOCY];
	// default: current size
	ss_cmdparams[CMD_VIEWPORT_SETPARAMS][CMDP_SIZEX] := ss_paramtype[CMDP_SIZEX];
	ss_cmdparams[CMD_VIEWPORT_SETPARAMS][CMDP_SIZEY] := ss_paramtype[CMDP_SIZEY];
	// default: current ratio
	ss_cmdparams[CMD_VIEWPORT_SETPARAMS][CMDP_RATIOX] := ss_paramtype[CMDP_RATIOX];
	ss_cmdparams[CMD_VIEWPORT_SETPARAMS][CMDP_RATIOY] := ss_paramtype[CMDP_RATIOY];

	// === Sound commands ===
	// default: empty song name, stops music
	ss_cmdparams[CMD_MUS_PLAY][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: 1600 msec, stop previous after a fadeout
	ss_cmdparams[CMD_MUS_PLAY][CMDP_TIME] := ss_paramtype[CMDP_TIME];
	// default: 0 msec, stop instantly
	ss_cmdparams[CMD_MUS_STOP][CMDP_TIME] := ss_paramtype[CMDP_TIME];

	// === Variable commands === (CMDP_by defaults to 1)
	// default: MAIN
	ss_cmdparams[CMD_PEEK][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: -1, all
	ss_cmdparams[CMD_PEEK][CMDP_INDEX] := ss_paramtype[CMDP_INDEX];
	// default: MAIN
	ss_cmdparams[CMD_POKE][CMDP_NAME] := ss_paramtype[CMDP_NAME];
	// default: 0
	ss_cmdparams[CMD_POKE][CMDP_INDEX] := ss_paramtype[CMDP_INDEX];
	// default: 0
	ss_cmdparams[CMD_POKE][CMDP_VALUE] := ss_paramtype[CMDP_VALUE];
end;

// Output token enums, used in compiled sakurascript bytecode.
{$push}{$scopedEnums off}
{$WARN 3031 OFF} // values in enumeration types have to be ascending, but it's really more of a guideline
type EScriptToken = (
	// unary operators
	TOKEN_NOT = '!', // NOT !
	TOKEN_NEG = '_', // unary prefix negation
	TOKEN_ABS = 'a', // absolute value
	TOKEN_VAR = '$',
	TOKEN_RND = 'r', // random value
	TOKEN_TONUM = 't', // ToNumber
	TOKEN_TOSTR = 'T', // ToString
	TOKEN_TOHEX = 'H', // to hexadecimal string
	TOKEN_INT8 = 'x', // sign-extend int8
	TOKEN_INT16 = 'X', // sign-extend int16
	TOKEN_GETPARAM = 'G', // followed by a 1-byte command parameter enum
	// binary operators (right-side operand is popped first, left-side second)
	TOKEN_PLUS = '+',
	TOKEN_MINUS = '-',
	TOKEN_MUL = '*',
	TOKEN_DIV = '/', // / DIV
	TOKEN_MOD = '%', // % MOD
	TOKEN_AND = '&', // & AND &&
	TOKEN_OR = '|', // | OR ||
	TOKEN_XOR = '^', // ^ XOR
	TOKEN_EQ = '=', // = ==
	TOKEN_LT = '<',
	TOKEN_GT = '>',
	TOKEN_LE = 'l', // <= =<
	TOKEN_GE = 'g', // >= =>
	TOKEN_NE = 'n', // != <>
	TOKEN_SET = ':', // :=
	TOKEN_INC = 'i', // +=
	TOKEN_DEC = 'd', // -=
	TOKEN_SHL = 'L', // << SHL
	TOKEN_SHR = 'R', // >> SHR
	// function-like operators
	TOKEN_CMD = 'C', // followed by a 1-byte command enum
	TOKEN_PARAM = 'p', // followed by a 1-byte command parameter enum
	TOKEN_DYNPARAM = 'P',
	TOKEN_CMDEND = ';', // marks the end of command parameters (but is actually output first)
	TOKEN_JUMP = 'j', // unconditional relative jump by following longint from the first byte of said longint
	TOKEN_IF = '?', // pop topmost stack, if 0, relative jump by following longint
	TOKEN_CHOICEREACT = 'c', // react to result of choice.get/goto/call
	TOKEN_UNIQUEOFS = 'o', // followed by dword that gets added to all subsequent unique string indexes in this label
	// flow operators (only during processing, these are output as ifs and jumps)
	TOKEN_THEN = '{',
	TOKEN_ELSE = '\',
	TOKEN_END = '}',
	TOKEN_WHILE = 'W',
	TOKEN_DO = 'D',
	// brackets (only during processing, these are not output)
	TOKEN_PARENOPEN = '(',
	TOKEN_PARENCLOSE = ')',
	// operands
	TOKEN_LONGUNIQUESTRING = 'U', // followed by dword local string table index
	TOKEN_LONGGLOBALSTRING = 'S', // followed by length dword + immediate string
	TOKEN_LONGLOCALSTRING = 'Z', // followed by length dword + immediate string
	TOKEN_MINIUNIQUESTRING = 'u', // followed by byte local string table index
	TOKEN_MINIGLOBALSTRING = 's', // followed by length byte + immediate string
	TOKEN_MINILOCALSTRING = 'z', // followed by length byte + immediate string
	TOKEN_CHAR = '''', // followed by a single byte immediate string
	TOKEN_EMPTYSTRING = '"',
	TOKEN_BYTE = 'b', // followed by byte
	TOKEN_WORD = 'w', // followed by word
	TOKEN_LONGINT = '#' // followed by longint
	// Additionally, numbers 0..32 can be saved as direct values.
);
{$pop}

