const supersakura_version = '0.97.0';

function GetSuperSakuraVersion : UTF8string;
// The version format is: major.minor.patch.timestamp.commit
// - Major, minor, and patch are set manually above this.
// - The latest commit timestamp is yyyyMMdd.
// - Commit is the latest git commit in the local repository, HEAD -> master. We use the abbreviated form.
// Commit ID and timestamp are passed in as a temporary environment variable, handled by comp.sh or comp.bat.
var commit : string;
begin
	commit := {$include %COMMITID%};
	if commit = '' then commit := '0';
	result := supersakura_version + '.' + commit;
end;

