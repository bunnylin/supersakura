unit decomp_core;

{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// This unit takes a variety of resources from classic VN games, and converts them into standard formats usable by
// modern tools and the SuperSakura engine.

{$mode objfpc}
{$codepage UTF8}
{$iochecks off} // WARNING: if an IO action fails, all subsequent actions fail silently until IOresult is checked
{$inline on}
{$unitpath ../lib/moonlibs}
{$librarypath ../lib/moonlibs}

{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

// Hacks fix a wide variety of things in the original resources. Some bugs are merely cosmetic errors, but a few will
// break SuperSakura. Turn this off only if you want virgin resource conversion for your own purposes.
{$define enable_decomp_hacks}

// On case-sensitive filesystems, user experience can be improved by doing extra case-insensitive checks.
{$ifndef WINDOWS}{$define caseshenanigans}{$endif}

interface

uses
sysutils, math, paszlib,
mccommon, mcfileio, gidtable, lz_algo, mcscriptwriter, mcmidiwriter, midi_instrument_matcher, mcbtree, mcgloder,
sjisutf8, mcsassm;

type DecompException = class(Exception);
type SkipException = class(Exception);

type ETransitionType = (Instant = 0, WipeFromLeft = 1, RaggedWipe = 2, Interlaced = 3, Crossfade = 4);

var decomp_param : record
		inputPaths : TStringBunch;
		outputDir : UTF8string; // full path to active project directory
		profileDir : UTF8string;
		fileFilter : UTF8string;
		gidOverride : PGameRecord;
		outputOverride, onlyId : boolean;
		keepTemp, tempOnly, keepInt : boolean;
		doBeautify, doHacks : boolean;
		debugLabels : boolean;
		verbose : boolean;
	end;

	decomp_stats : record // stats for entire process lifetime, may cover multiple game conversions
		numFiles : dword;
		numConverted : dword;
		numSkipped : dword;
		numErrors : dword;
	end;

procedure Decomp_ScanFiles;
var decomp_logger : procedure (const logtext : UTF8string);

implementation

var datainfpath, audpath, gfxpath, scrpath : UTF8string;
	newGfxList : TStringBunch; // graphic base filenames, must be lowercase
	newGfxCount : dword;
	gameConst : record
		hardBytes : array of array of byte;
		hardcodedTxt : TStringBunch;
		songList : array of string[15];
		baseResXP, baseResYP : word;
	end;


procedure LogError(const t : UTF8string);
// Logs an error, increases the error counter.
begin
	decomp_logger('[error] ' + t);
	inc(decomp_stats.numErrors);
end;

// ------------------------------------------------------------------

procedure ResetMemResources;
var i : dword;
begin
	// Forget all graphics except the null item.
	if graphicFileCount > 1 then begin
		for i := graphicFileCount - 1 downto 1 do graphicFiles[i].Destroy;
		setlength(graphicFiles, 1);
		graphicFileCount := 1;
	end;

	setlength(newGfxList, 0);
	newGfxCount := 0;

	with gameConst do begin
		setlength(hardBytes, 0);
		setlength(songList, 0);
		setlength(hardcodedTxt, 0);
	end;
end;

function SeekNewGfx(const name : UTF8string) : dword;
// Returns the index of the given graphic in NewGfxList[], or -1 if not found. The name must be in lowercase.
begin
	result := newGfxCount;
	while result <> 0 do begin
		dec(result);
		if newGfxList[result] = name then exit;
	end;
	result := high(dword);
end;

procedure WriteInf(currentgame : PGameRecord);
// Writes data.inf.
var f : TBufferedFileWriter;
	i, j : dword;
	txt : UTF8string;
	hasofs, hasres, hasframes, hasseq : boolean;
begin
	Assert(currentgame <> NIL);
	decomp_logger('Writing ' + datainfpath);
	f := TBufferedFileWriter.Create(datainfpath, NIL, 65536);

	try
		f.WriteLine('# This file is automatically generated. Don''t edit manually!');
		// Must propagate baseres from newdata to data.inf since data.inf is read first and baseres must be known.
		with gameConst do f.WriteLine(strcat('baseres %x%', [baseResXP, baseResYP]));

		if currentgame^.g <> gid.unknown then f.WriteLine('desc ' + currentgame^.desc);

		case currentgame^.g of
			gid.Setsujuu:
			f.WriteLine('framesvertically');
		end;

		if graphicFileCount > 1 then
			for i := 1 to graphicFileCount - 1 do with graphicFiles[i] do begin
				hasofs := (origOfsXP or origOfsYP) <> 0;
				hasres := ((origResXP or origResYP) <> 0) and
					((origResXP <> gameConst.baseResXP) or (origResYP <> gameConst.baseResYP));
				hasframes := (origFrameHeightP <> 0) and (origFrameHeightP * frameCount <> origSizeYP);
				hasseq := seqLen <> 0;

				if (NOT (hasofs or hasres or hasframes or hasseq)) and (frameCount <= 1) then continue;

				f.WriteLine(LineEnding + 'image ' + graphicName);
				if hasofs then f.WriteLine(strcat('ofs %,%', [origOfsXP, origOfsYP]));
				if hasres then f.WriteLine(strcat('res %x%', [origResXP, origResYP]));
				if frameCount > 1 then f.WriteLine('framecount ' + strdec(frameCount));
				if hasframes then begin
					Assert((origSizeYP = 0) or (origFrameHeightP <= origSizeYP),
						strcat('% framehp % > sizeyp %', [graphicName, origFrameHeightP, origSizeYP]));
					f.WriteLine('frameheight ' + strdec(origFrameHeightP));
				end;
				if hasseq then begin

					// Print the unpacked animation sequence.
					txt := 'sequence';
					for j := 0 to seqLen - 1 do begin
						if length(txt) >= 70 then begin
							f.WriteLine(txt);
							txt := 'sequence';
						end;
						// index
						txt := txt + ' ' + strdec(j) + ':';
						if sequence[j] and $80000000 <> 0 then txt := txt + 'jump ';
						// frame
						if sequence[j] and $40000000 <> 0 then txt := txt + 'r';
						if sequence[j] and $20000000 <> 0 then txt := txt + 'v';
						txt := txt + strdec((sequence[j] shr 16) and $1FFF);
						// delay
						if sequence[j] and $80000000 = 0 then begin
							txt := txt + ',';
							case (sequence[j] and $FFFF) of
								$8000..$BFFF:
								txt := txt + 'r'+ strdec(sequence[j] and $3FFF);

								$C000..$FFFE:
								txt := txt + 'v'+ strdec(sequence[j] and $3FFF);

								$FFFF:
								txt := txt + 'stop';

								else txt := txt + strdec(sequence[j] and $7FFF);
							end;
						end;
						txt := txt + ';';
					end;
					f.WriteLine(txt);

				end;
			end;
	finally
		f.Destroy;
	end;
end;

procedure XorBuffer(startp, endp : pointer; value : byte);
var d, i : dword;
	v : ptruint;
begin
	if endp <= startp then exit;
	d := endp - startp;
	{$ifdef CPU64}
	i := d shr 3;
	v := value * qword($101010101010101);
	while i <> 0 do begin
		qword(startp^) := qword(startp^) xor v;
		inc(startp, 8);
		dec(i);
	end;
	{$else}
	i := d shr 2;
	v := value * $1010101;
	while i <> 0 do begin
		dword(startp^) := dword(startp^) xor v;
		inc(startp, 4);
		dec(i);
	end;
	{$endif}
	while startp < endp do begin
		byte(startp^) := byte(startp^) xor value;
		inc(startp);
	end;
end;

// ------------------------------------------------------------------

// Decompilers may need to redispatch bundle contents etc.
procedure DispatchFile(const srcfile : UTF8string; game : PGameRecord; loader : TFileLoader); forward;

{$push}{$scopedEnums off}
type EConverterName = (
	CN_NULL,
	CN_ACH, CN_ADA, CN_AIC, CN_ANM, CN_AS2, CN_ATLUSFC, CN_BMP, CN_C24, CN_CGL, CN_CWL, CN_CWP, CN_DA1, CN_EWBMP,
	CN_GREATPAC, CN_GGD, CN_GP4, CN_GPC, CN_HDF, CN_MAKICHAN, CN_MDA, CN_MRS, CN_MSV, CN_OLH, CN_OT, CN_OZM, CN_PI,
	CN_PR6, CN_PR7, CN_PRS, CN_PRSMC, CN_TIFF, CN_VDF,
	CN_ELFM, CN_LEAFFM, CN_MDS, CN_MID, CN_MLO, CN_MMD, CN_MUAP98, CN_PMD, CN_RECOMPOSER,
	CN_WAV, CN_MP3, CN_OGG);
{$pop}

// Image compositing/beautifying functions.
{$include decomp_postproc.pas}

// Decompiling functions for specific input file types.
{$include scr/ange-adv.pas}
{$include scr/applepie.pas}
{$include scr/csware-ab.pas}
{$include scr/csware-cc.pas}
{$include scr/cuisse-sin.pas}
{$include scr/elf-mes.pas}
{$include scr/fairydust-scp.pas}
{$include scr/fairydust-scr.pas}
{$include scr/great-sd.pas}
{$include scr/isf.pas}
{$include scr/jast-ovl.pas}
{$include scr/leaf-dat.pas}
{$include scr/panda-mdr.pas}
{$include scr/parsley-txb.pas}
{$include scr/sgs-sil.pas}
{$include scr/sys98-s.pas}

{$include gfx/aic.pas}
{$include gfx/as2.pas}
{$include gfx/atlusfc.pas}
{$include gfx/bmp.pas}
{$include gfx/cgl.pas}
{$include gfx/da1.pas}
{$include gfx/ggd.pas}
{$include gfx/gp4.pas}
{$include gfx/ada.pas} // <-- may forward to gp4
{$include gfx/gpc.pas}
{$include gfx/greatpac.pas}
{$include gfx/jast-img-brge.pas}
{$include gfx/makichan.pas}
{$include gfx/mrs.pas}
{$include gfx/msv.pas}
{$include gfx/ot.pas}
{$include gfx/ozm.pas}
{$include gfx/pi.pas}
{$include gfx/png.pas}
{$include gfx/prs.pas}
{$include gfx/vdf.pas}

{$include aud/pmd.pas}
{$include aud/elfm.pas}
{$include aud/mlo.pas}
{$include aud/muap98.pas}
{$include aud/midi.pas}
{$include aud/wav.pas}

{$include decomp_exe.pas}
{$include decomp_bundles.pas}

// ------------------------------------------------------------------

{$push}{$scopedEnums off}
var converterlist : array of record
		TryConvert : function(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
		nextLink : longint;
		intoType : (CT_AUD, CT_PNG);
		name : EConverterName;
	end =
(
	(TryConvert: NIL; nextLink: -1; intoType: CT_PNG; name: CN_NULL),
	(TryConvert: @Decomp_Makichan; nextLink: 0; intoType: CT_PNG; name: CN_MAKICHAN),
	(TryConvert: @Decomp_Pi; nextLink: 0; intoType: CT_PNG; name: CN_PI),
	(TryConvert: @Decomp_DA1; nextLink: 0; intoType: CT_PNG; name: CN_DA1),
	(TryConvert: @Decomp_AIC; nextLink: 0; intoType: CT_PNG; name: CN_AIC),
	(TryConvert: @Decomp_AS2; nextLink: 0; intoType: CT_PNG; name: CN_AS2),
	(TryConvert: @Decomp_AtlusFC; nextLink: 0; intoType: CT_PNG; name: CN_ATLUSFC),
	(TryConvert: @Decomp_GreatPAC; nextLink: 0; intoType: CT_PNG; name: CN_GREATPAC),
	(TryConvert: @Decomp_GGD; nextLink: 0; intoType: CT_PNG; name: CN_GGD),
	(TryConvert: @Decomp_GPC; nextLink: 0; intoType: CT_PNG; name: CN_GPC),
	(TryConvert: @Decomp_VDF; nextLink: 0; intoType: CT_PNG; name: CN_VDF),
	(TryConvert: @Decomp_MRS; nextLink: 0; intoType: CT_PNG; name: CN_MRS),
	(TryConvert: @Decomp_SilenceMSV; nextLink: 0; intoType: CT_PNG; name: CN_MSV),
	(TryConvert: @Decomp_SognaANM; nextLink: 0; intoType: CT_PNG; name: CN_ANM),
	(TryConvert: @Decomp_OT; nextLink: 0; intoType: CT_PNG; name: CN_OT),
	(TryConvert: @Decomp_OZM; nextLink: 0; intoType: CT_PNG; name: CN_OZM),
	(TryConvert: @Decomp_OLH; nextLink: 0; intoType: CT_PNG; name: CN_OLH),
	(TryConvert: @Decomp_CWP; nextLink: 0; intoType: CT_PNG; name: CN_CWP),
	(TryConvert: @Decomp_BMP; nextLink: 0; intoType: CT_PNG; name: CN_BMP),
	(TryConvert: @Decomp_CWL; nextLink: 0; intoType: CT_PNG; name: CN_CWL),
	(TryConvert: @Decomp_CGL; nextLink: 0; intoType: CT_PNG; name: CN_CGL),
	(TryConvert: @Decomp_HDF; nextLink: 0; intoType: CT_PNG; name: CN_HDF),
	(TryConvert: @Decomp_GP4; nextLink: 0; intoType: CT_PNG; name: CN_GP4),
	(TryConvert: @Decomp_PRS; nextLink: 0; intoType: CT_PNG; name: CN_PRS),
	(TryConvert: @Decomp_PRSMC; nextLink: 0; intoType: CT_PNG; name: CN_PRSMC),
	(TryConvert: @Decomp_PR7; nextLink: 0; intoType: CT_PNG; name: CN_PR7),
	(TryConvert: @Decomp_PR6; nextLink: 0; intoType: CT_PNG; name: CN_PR6),
	(TryConvert: @Decomp_MDA; nextLink: 0; intoType: CT_PNG; name: CN_MDA),
	(TryConvert: @Decomp_ADA; nextLink: 0; intoType: CT_PNG; name: CN_ADA),
	(TryConvert: @Decomp_C24; nextLink: 0; intoType: CT_PNG; name: CN_C24),
	(TryConvert: @Decomp_EWBMP; nextLink: 0; intoType: CT_PNG; name: CN_EWBMP),
	(TryConvert: @Decomp_ACH; nextLink: 0; intoType: CT_PNG; name: CN_ACH),
	(TryConvert: @Decomp_TIFF; nextLink: 0; intoType: CT_PNG; name: CN_TIFF),
	(TryConvert: @Decomp_MMD; nextLink: 0; intoType: CT_AUD; name: CN_MMD),
	(TryConvert: @Decomp_RCP; nextLink: 0; intoType: CT_AUD; name: CN_RECOMPOSER),
	(TryConvert: @Decomp_MDS; nextLink: 0; intoType: CT_AUD; name: CN_MDS),
	(TryConvert: @Decomp_MID; nextLink: 0; intoType: CT_AUD; name: CN_MID),
	(TryConvert: @Decomp_MLO; nextLink: 0; intoType: CT_AUD; name: CN_MLO),
	(TryConvert: @Decomp_MUAP98; nextLink: 0; intoType: CT_AUD; name: CN_MUAP98),
	(TryConvert: @Decomp_PMD; nextLink: 0; intoType: CT_AUD; name: CN_PMD),
	(TryConvert: @Decomp_ElfM; nextLink: 0; intoType: CT_AUD; name: CN_ELFM),
	(TryConvert: @Decomp_LeafFM; nextLink: 0; intoType: CT_AUD; name: CN_LEAFFM),
	(TryConvert: @Decomp_OGG; nextLink: 0; intoType: CT_AUD; name: CN_OGG),
	(TryConvert: @Decomp_MP3; nextLink: 0; intoType: CT_AUD; name: CN_MP3),
	(TryConvert: @Decomp_WAV; nextLink: 0; intoType: CT_AUD; name: CN_WAV)
);
{$pop}
var lastconvertedsuffix : UTF8string = '';

procedure DispatchFile(const srcfile : UTF8string; game : PGameRecord; loader : TFileLoader);
// Forwards the given file to an appropriate conversion routine.
// If loader is NIL, srcfile is loaded into it. Loader will be freed before return even if there's an error.
// Each converter may raise a DecompException if conversion failed badly, otherwise returns TRUE for success.
var basename, suffix, fullname : UTF8string;
	converted : boolean = FALSE;

	procedure _InitLinks;
	var i : dword;
	begin
		for i := high(converterlist) - 1 downto 0 do
			converterlist[i].nextLink := i + 1;
	end;

	procedure _DeletePath(const subdir : UTF8string);
	var delpath, delfile : UTF8string;
		f : file;
		err : dword;
	begin
		if decomp_param.keepTemp then exit;
		Assert(IOresult = 0);
		delpath := PathCombine([decomp_param.outputDir, subdir]);
		if NOT DirectoryExists(delpath) then exit;
		if decomp_param.verbose then decomp_logger('Cleaning up temp files...');
		for delfile in FindFiles_caseless(PathCombine([delpath, '*'])) do begin
			assign(f, delfile);
			erase(f);
			err := IOresult;
			if err <> 0 then decomp_logger(strcat('Failed to delete %: %', [delfile, errortxt(err)]));
		end;
		rmdir(delpath);
		err := IOresult;
		if err <> 0 then raise Exception.Create(strcat('Failed to delete %: %', [delpath, errortxt(err)]));
	end;

	procedure _MoveToFront(converter : EConverterName);
	var i, prev : dword;
	begin
		i := 0; prev := 0; // [0] is null item, points to foremost converter; last converter points to 0
		while converterlist[i].name <> converter do begin
			prev := i; i := converterlist[i].nextLink;
			Assert((i <> 0) and (i <> prev) and (i < dword(length(converterlist))), 'bad converter');
		end;
		converterlist[prev].nextLink := converterlist[i].nextLink;
		converterlist[i].nextLink := converterlist[0].nextLink;
		converterlist[0].nextLink := i;
	end;

	function _TryDynamicDispatch : boolean;
	// Moves the most likely converter to the front of the list, then tries every converter until one works.
	// If the current file has the same suffix as the previous one, assume the current list order is already optimal.
	var outdir : UTF8string;
		conv : EConverterName = CN_NULL;
		i : dword;
	begin
		result := FALSE;
		Assert((suffix = '') or (suffix[1] <> '.'));
		if suffix <> lastconvertedsuffix then begin
			case suffix of
				// === Graphics ===
				'ach': conv := CN_ACH;

				'ada':
				if game^.g = gid.DragoonArmor then conv := CN_PR6
				else if (LEtoN(loader.ReadWordFrom(2)) = loader.fullFileSize - 1) and (byte((loader.endp - 1)^) = $1A) then conv := CN_GP4
				else if loader.ReadByteFrom(1) >= $C0 then conv := CN_ADA
				else conv := CN_PRS;

				'ani':
				begin
					_MoveToFront(CN_PR7);
					if (game^.g <> gid.DEJA1) and (game^.g <> gid.DK2) then conv := CN_PR6;
				end;

				'anm':
				begin
					_MoveToFront(CN_ANM);
					conv := CN_MSV;
				end;

				'aic': conv := CN_AIC;
				'as2': conv := CN_AS2;
				'bmp', 'zbm': conv := CN_BMP;
				'c24','c25': conv := CN_C24;
				'cgl': conv := CN_CGL;
				'cwl': conv := CN_CWL;
				'cwp': conv := CN_CWP;
				'da1', 'gdt': conv := CN_DA1;
				'ew': conv := CN_EWBMP;
				'fc': conv := CN_ATLUSFC;

				'g':
				case game^.g of
					gid.Mayclub98, gid.Mayclub_e, gid.Nocturn98, gid.Nocturn_e:
					conv := CN_PI;
				end;

				'gg','ggd','ggp','gg0','gg1','gg2','gg3','gad','gan','drg': conv := CN_GGD;
				'gp4': conv := CN_GP4;
				'gpc','gpa': conv := CN_GPC;

				'gra':
				case game^.g of
					gid.SanShimai, gid.SanShimai98, gid.AngelsC1, gid.AngelsC2, gid.Deep, gid.Eden, gid.FromH,
					gid.Hohoemi, gid.Majokko, gid.Maririn, gid.Runaway, gid.Runaway98, gid.Sakura, gid.Sakura98,
					gid.Setsujuu, gid.Tasogare, gid.Transfer98, gid.Vanish:
					conv := CN_PI;
				end;

				'hdf': conv := CN_HDF;
				'mag', 'max', 'mki', 'sgv': conv := CN_MAKICHAN;
				'mda': conv := CN_MDA;
				'mrs','vrs','xmg': conv := CN_MRS;
				'msv','scr': conv := CN_MSV;
				'olh': conv := CN_OLH;
				'ot': conv := CN_OT;
				'ozm': conv := CN_OZM;
				'pac': conv := CN_GREATPAC;
				'pi','p','dpc': conv := CN_PI;
				'pic': conv := CN_PR6;
				'pr6': if game^.g = gid.DeJa1 then conv := CN_PR7 else conv := CN_PR6;
				'pr7','pa7','pd7','pd8': conv := CN_PR7;
				'prs': conv := CN_PRS;
				'vdf': conv := CN_VDF;

				// === Audio ===
				'fm': conv := CN_LEAFFM;
				'm': if LEtoN(word(loader.readp^)) = 6 then conv := CN_ELFM else conv := CN_PMD;
				'm2': conv := CN_PMD;
				'mds': conv := CN_MDS;
				'mid': conv := CN_MID;
				'mlo': conv := CN_MLO;
				'mmd','gmd': conv := CN_MMD;
				'mu': conv := CN_ELFM;
				'o': conv := CN_MUAP98;
				'sc5', 'rcp': conv := CN_RECOMPOSER;

				'mp3': conv := CN_MP3;
				'ogg': conv := CN_OGG;
				'wav': conv := CN_WAV;
			end;
			lastconvertedsuffix := suffix;
			if conv <> CN_NULL then _MoveToFront(conv);
		end;

		i := 0;
		repeat
			loader.ofs := 0; // reset in case previous converter moved read pointer
			i := converterlist[i].nextLink;
			if i = 0 then exit; // no converter worked, return false

			try
				case converterlist[i].intoType of
					CT_AUD: outdir := audpath;
					CT_PNG: outdir := gfxpath;
				end;
				result := converterlist[i].TryConvert(loader, outdir, basename, game^.g);
				if result then begin
					// This converter worked! Bump to front of list if not yet there in case next file is same.
					if dword(converterlist[0].nextLink) <> i then _MoveToFront(converterlist[i].name);

					if decomp_param.verbose then
						decomp_logger(strenum(typeinfo(EConverterName), @converterlist[i].name));

					break;
				end;
			except on e : DecompException do if decomp_param.verbose then begin
				WriteStr(outdir, converterlist[i].name); // <-- hack, using outdir as scratch string
				decomp_logger(outdir + ': ' + e.Message);
			end; end;
		until FALSE;

		if converterlist[i].intotype = CT_PNG then begin
			// Keep track of newly-added graphics. The converter already added its metadata to graphicFiles.
			if newGfxCount >= newGfxList.Length then
				setlength(newGfxList, newGfxCount + newGfxCount shr 2 + 64);
			newGfxList[newGfxCount] := basename;
			inc(newGfxCount);
		end;
		result := TRUE;
	end;

	function _TryArchives : boolean;
	var cleanable : boolean = TRUE;
		sig : dword;
	begin
		result := FALSE;
		// These pass through the PGameRecord instead of plain gid because need to Dispatch further...
		try
			try
				// First try to use an obvious signature, if possible.
				sig := BEtoN(dword(loader.readp^));
				case sig of
					$444C421A: result := Decomp_DesArc(loader, decomp_param.outputDir, game);
					$5347532E: result := Decomp_SGSDat(loader, decomp_param.outputDir, game);
					$61726333: result := Decomp_CsArc3(loader, decomp_param.outputDir, game);
				end;
				if result then exit;
			except on e : DecompException do begin
				if decomp_param.verbose then decomp_logger('sigarc: ' + e.Message);
				result := FALSE;
			end; end;

			try
				// Use the file suffix if the signature-match didn't work.
				case suffix of
					'dat':
					case game^.g of
						gid.DK1:
						if basename = 'item' then begin
							with loader do fileName := copy(fileName, 1, fileName.Length - 3) + 'pr6';
							result := Decomp_Split(loader, decomp_param.outputDir, game, 252);
						end;

						gid.Eroden, gid.Izayoi, gid.PrettyParfait, gid.SexyParfait, gid.TalesNights, gid.TenshiNingyou:
						result := Decomp_TiareDAT(loader, basename + '.hed', decomp_param.outputDir, game);

						gid.Mayclub98, gid.Mayclub_e, gid.Nocturn_e:
						result := Decomp_ExcellentDAT(loader, basename + '.lst', decomp_param.outputDir, game);

						gid.Gloria_w, gid.Gloria_e:
						result := Decomp_CsArc(loader, decomp_param.outputDir, game);

						gid.MajoGari98, gid.Kyouhaku98, gid.RuriiroNoYuki:
						result := Decomp_AILArc(loader, decomp_param.outputDir, game);

						gid.WakuWakuMP2:
						result := Decomp_WakuDat(loader, decomp_param.outputDir, game);
					end;
					'lst':
					case game^.g of
						gid.Mayclub_e, gid.Nocturn_e:
						raise SkipException.Create('used automatically with ' + basename + '.dat');
					end;
					'hed':
					case game^.g of
						gid.Eroden, gid.Izayoi, gid.PrettyParfait, gid.SexyParfait, gid.TalesNights, gid.TenshiNingyou:
						raise SkipException.Create('used automatically with ' + basename + '.dat');
					end;

					'lib':
					case game^.g of
						gid.GaoGao3, gid.GaoGao4, gid.Lilith, gid.Mayclub98, gid.Nocturn98:
						result := Decomp_ExcellentLib(loader, basename + '.cat', decomp_param.outputDir, game);
					end;
					'cat':
					case game^.g of
						gid.GaoGao3, gid.GaoGao4, gid.Lilith, gid.Mayclub98, gid.Nocturn98:
						raise SkipException.Create('used automatically with ' + basename + '.lib');
					end;

					'000','001','002':
					case game^.g of
						gid.Yuugiri: result := Decomp_DesArc(loader, decomp_param.outputDir, game);
					end;

					'a2', 'adt', 'cma', 'sca', 'ewa', 'wva':
					result := Decomp_CsArc(loader, decomp_param.outputDir, game);

					'anm':
					case game^.g of
						gid.DualSoul98, gid.InmaSeiGa98, gid.MajoGari98, gid.Kyouhaku98, gid.RuriiroNoYuki:
						result := Decomp_AILArc(loader, decomp_param.outputDir, game);
					end;

					'arc':
					case game^.g of
						gid.WakuWakuMP2:
						result := Decomp_DesArc(loader, decomp_param.outputDir, game);
					end;

					'bin':
					case game^.g of
						gid.GitenMegaTen:
						result := Decomp_AtlusBin(loader, decomp_param.outputdir, basename, game);
					end;

					'dad':
					result := Decomp_DAD(loader, basename, decomp_param.outputDir, game^.g);

					'dac','dlb':
					result := Decomp_DesArc(loader, decomp_param.outputDir, game);

					'dl1','fl1':
					result := Decomp_DL1(loader, decomp_param.outputDir, game);

					'fa1':
					result := Decomp_FA1(loader, decomp_param.outputDir, game);

					'fa2':
					result := Decomp_FA2(loader, decomp_param.outputDir, game);

					'fl2':
					result := Decomp_FL2(loader, decomp_param.outputDir, game);

					'gra', 'grp':
					case game^.g of
						gid.DualSoul98, gid.InmaSeiGa98:
						result := Decomp_AILArc(loader, decomp_param.outputDir, game);
					end;

					'pck':
					if loader.ReadDwordFrom(0) = BEtoN(dword($A8212590)) then
						result := Decomp_MAXPCK(loader, decomp_param.outputDir, game)
					else case game^.g of
						gid.Hohoemi_w, gid.JastMemo, gid.Majokko_w:
						result := Decomp_JastPCK(loader, decomp_param.outputDir, game);
					end;

					'm':
					case game^.g of
						gid.Skirmish, gid.DualSoul98, gid.InmaSeiGa98, gid.MajoGari98, gid.Kyouhaku98:
						if NOT (basename[basename.Length] in ['0'..'9']) then
							result := Decomp_Split(loader, decomp_param.outputDir, game);

						gid.RuriiroNoYuki:
						if LEtoN(word(loader.readp^)) = $F then
							result := Decomp_AILArc(loader, decomp_param.outputDir, game);
					end;

					'mio':
					result := Decomp_MIO(loader, decomp_param.outputDir, game);

					'mmd':
					if game^.g = gid.RuriiroNoYuki then
						if LEtoN(word(loader.readp^)) = $F then
							result := Decomp_AILArc(loader, decomp_param.outputDir, game);

					'apg', 'agx', 'mcg', 'bix', 'wax', 'se', 'six', 'bgm', 'bmx':
					case game^.g of
						gid.BountyHunterLudy_w, gid.ChaosQueen1, gid.ChaosQueen2, gid.ChaosQueen3, gid.DeliLunchPack_w,
						gid.ImmoralStudy1_w, gid.ImmoralStudy1_e, gid.ImmoralStudy2_w, gid.ImmoralStudy2_e,
						gid.ImmoralStudy3_w, gid.MahouShoujoBko_w, gid.OjousamaoNerae, gid.SchoolGirls_w:
						result := Decomp_ScooPX(loader, decomp_param.outputDir, game);
					end;

					'snl':
					case game^.g of
						gid.DualSoul98, gid.InmaSeiGa98, gid.MajoGari98, gid.Kyouhaku98, gid.RuriiroNoYuki:
						if word(loader.readp^) <> 0 then
							result := Decomp_AILArc(loader, decomp_param.outputDir, game);
					end;

					'szh':
					result := Decomp_SZH(loader, decomp_param.outputDir, game);

					'vol':
					result := Decomp_MegatechVOL(loader, decomp_param.outputDir, game);

					'':
					begin
						if (basename = 'save') or (basename = 'end') then
							raise SkipException.Create('nothing to extract');
						if (game^.g = gid.TrueLove_e) and (basename = 'datg') then
							raise SkipException.Create('German script file');

						try
							if NOT result then result := Decomp_SM2MPX10(loader, decomp_param.outputDir, game);
						except on e : DecompException do begin
							if decomp_param.verbose then decomp_logger(e.Message);
							result := FALSE;
						end; end;

						if NOT result then begin
							loader.ofs := 0;
							result := Decomp_OtakuArc(loader, decomp_param.outputDir, game);
						end;
					end;

					else cleanable := FALSE;
				end;

			except on e : DecompException do begin
				if decomp_param.verbose then decomp_logger(e.Message);
				result := FALSE;
			end; end;

		finally
			if (cleanable) and (NOT decomp_param.keepTemp) then _DeletePath('temp');
			if NOT result then loader.ofs := 0;
		end;
	end;

	procedure _ExpandSZDD;
	// SZDD is a generic compression format for a single file. This decompresses it in loader.
	var p : pointer = NIL;
		finalsize : dword;
	begin
		if decomp_param.verbose then decomp_logger('expanding SZDD');
		inc(loader.readp, 4);
		if BEtoN(loader.ReadDword) <> $88F02733 then raise DecompException.Create('bad szdd sig2');
		if loader.ReadByte <> $41 then raise DecompException.Create('bad szdd mode');
		// If dealing with a *.da_ file, replace underscore with original character stashed here.
		if (byte(loader.readp^) <> 0) and (loader.fileName[loader.fileName.Length] = '_') then begin
			loader.fileName[loader.fileName.Length] := char(loader.readp^);
			suffix[suffix.Length] := char(loader.readp^);
		end;
		inc(loader.readp);
		finalsize := LEtoN(loader.ReadDword);

		getmem(p, finalsize);
		try
			Decompress_LZSS(loader.readp, loader.endp, p, finalsize, 16);
			loader.Destroy;
			loader := TFileLoader.FromMemory(p, finalsize, TRUE);
		except on e : Exception do begin
			freemem(p);
			LogError('_ExpandSZDD: ' + e.Message);
		end; end;
		p := NIL;
	end;

begin
	Assert(NOT decomp_param.onlyId);
	decomp_logger(srcfile);
	if converterlist[0].nextLink = -1 then _InitLinks;
	inc(decomp_stats.numFiles);

	suffix := copy(lowercase(ExtractFileExt(srcfile)), 2); // no dot
	basename := lowercase(ExtractFileNameWithoutExt(srcfile));
	fullname := basename + '.' + suffix;
	if decomp_param.gidOverride <> NIL then game := decomp_param.gidOverride;
	if game = NIL then game := @GameRecordTable[0];

	try
		try
			if ((suffix = 'sys') and ((basename = 'msdos') or (basename = 'megdos') or (basename = 'qdos')
				or (basename = 'config') or (basename = 'io') or (basename = 'io98')))
			or (fullname = 'command.com')
			or (fullname = 'autoexec.bat')
			or (fullname = 'autorun.inf')
			or (basename = 'readme')
			or (suffix = 'dat') and ((basename = 'memory') or (basename.StartsWith('save')))
			or (basename.StartsWith('savdat'))
			then raise SkipException.Create('nothing to extract');

			if loader = NIL then loader := TFileLoader.Open(srcfile);
			if loader.fullFileSize = 0 then raise SkipException.Create('empty file');
			if loader.fullFileSize < 100 then
				if (basename.StartsWith('disk'))
				or (basename = 'id')
				or ((basename[1] in ['a'..'k']) and
					((basename.Length = 5) and (CompareByte(basename[2], 'disk', 4) = 0))
					or (basename.Length = 4) and (CompareByte(basename[2], 'dsk', 3) = 0))
				then raise SkipException.Create('nothing to extract');
			if loader.fullFileSize <= 1024 then
				if (basename.StartsWith('flag'))
				or (suffix = 'suf') and (dword(loader.readp^) = BEtoN(dword($5B537461))) // Ikura startupinfo files
				then raise SkipException.Create('nothing to extract');

			// Handle generic SZDD compression.
			if (loader.fullFileSize > $F) and (BEtoN(dword(loader.readp^)) = $535A4444) then _ExpandSZDD;

			if _TryArchives then begin
				decomp_logger('[archive ' + basename + '.' + suffix + ' ok]');
				inc(decomp_stats.numConverted);
				exit;
			end;
			with decomp_param do begin
				if tempOnly then raise SkipException.Create('not an archive');
				if (fileFilter <> '') and (NOT WildcardMatch(fileFilter, srcfile)) then
					raise SkipException.Create('filter miss');
			end;
			converted := FALSE;

			// === Tricky resources ===
			case game^.g of
				gid.AngelsEve5, gid.AngelsEve3H, gid.AngelsSpecial2:
				begin
					case suffix of
						'g','r','e','gc','rc','ec':
							raise SkipException.Create('used automatically with .b');
						'b','bc':
							converted := Decomp_BRGE(loader, gfxpath, basename, game^.g);
					end;
				end;
			end;

			if NOT converted then case suffix of
				'bat','doc','rgb','sav': raise SkipException.Create('nothing to extract');

				// === Executables ===
				'exe', 'com':
				begin
					converted := Decomp_Exe(loader, basename, game^.g);
					if NOT converted then raise SkipException.Create('nothing to extract');
				end;

				// === Scripts ===
				// todo: move the scrpath/basename combine to each script converter, same as gfx/mus converters
				'':
				begin
					// HPlus has Des98/System98 scripts without a suffix.
					if (loader.fullFileSize > 700)
					and (dword(loader.readp^) = 0) and (dword((loader.readp + 4)^) = 0) and (dword((loader.readp + $FC)^) = 0)
					and (byte((loader.readp + 256)^) in [1..$69]) then
					converted := Decomp_Sys98_S(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'ab':
				case game^.g of
					gid.Enomoto, gid.Gloria98, gid.Gloria_w, gid.Gloria_e, gid.Guardian, gid.Kokoro1, gid.KouteiHeika,
					gid.LAG, gid.LoftyForm, gid.MHard98, gid.MHard_w, gid.RabidHelix, gid.RoseBlood98, gid.RoseBlood_w,
					gid.ZestFan98, gid.ZestFan_w:
					converted := Decomp_CsWareAB(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'adv':
				case game^.g of
					gid.Like, gid.KaiketsuNikki, gid.SchoolFest:
					converted := Decomp_AngeADV(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'bin':
				case game^.g of
					gid.Lakers1:
					converted := Decomp_ApplePieBin(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'cc':
				case game^.g of
					gid.Amy98, gid.Desire98, gid.EtsuGaku98, gid.EveBE98, gid.Xenon98:
					converted := Decomp_CsWareCC(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'cmd':
				case game^.g of
					gid.KinKetsu98:
					converted := Decomp_CsWareCc(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'cmf': converted := Decomp_PandaCMF(loader, PathCombine([scrpath, basename + '.txt']), game^.g);

				'dat':
				case game^.g of
					gid.Shizuku98:
					converted := Decomp_LeafDat(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'isf': converted := Decomp_ISF(loader, PathCombine([scrpath, basename + '.txt']), game^.g);

				'fn1', 'lsp':
				begin
					if BEtoN(dword(loader.readp^)) shr 8 = $001A00 then
						converted := Decomp_PMD(loader, audpath, basename, game^.g)
					else if (LEtoN(loader.ReadWordFrom(2)) < LEtoN(loader.ReadWordFrom(6)))
						and (LEtoN(loader.ReadWordFrom(6)) < LEtoN(loader.ReadWordFrom(10)))
						and (LEtoN(loader.ReadWordFrom(10)) < loader.fullFileSize) then
						converted := Decomp_MMD(loader, audpath, basename, game^.g)
					else if dword(loader.readp^) = 0 then
						converted := Decomp_Sys98_S(loader, PathCombine([scrpath, basename + '.txt']), game^.g)
					else
						converted := Decomp_Pi(loader, gfxpath, basename, game^.g);
				end;

				'mdr': converted := Decomp_PandaMDR(loader, PathCombine([scrpath, basename + '.txt']), game^.g);

				'mes':
				case game^.g of
					gid.AngelHearts, gid.DeJa1, gid.DK1, gid.DK2, gid.Foxy1, gid.Pinky1, gid.Pinky2, gid.Pinky3,
					gid.RayGun, gid.RunRunConcerto:
					converted := Decomp_MES(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'ovl':
				case game^.g of
					gid.AngelsC1, gid.AngelsC2, gid.Deep, gid.Eden, gid.Eroden, gid.FromH, gid.Hohoemi, gid.Izayoi,
					gid.Majokko, gid.Maririn, gid.PrettyParfait, gid.Runaway, gid.Runaway98, gid.Sakura, gid.Sakura98,
					gid.SanShimai, gid.SanShimai98, gid.Setsujuu, gid.SexyParfait, gid.TalesNights, gid.Tasogare,
					gid.TenshiNingyou, gid.Transfer98, gid.Vanish:
					converted := Decomp_JastOvl(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				's':
				case game^.g of
					gid.GaoGao4, gid.Lilith, gid.Mayclub98, gid.Mayclub_e, gid.Nocturn98, gid.Nocturn_e:
					converted := Decomp_Sys98_S(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'scp':
				case game^.g of
					gid.AkaiSuishou, gid.GoJikanme, gid.Ukiuki:
					converted := Decomp_FairyDustSCP(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'scr':
				case game^.g of
					gid.KoukanNikki1, gid.KoukanNikki2, gid.MaDoll, gid.MaDoll_w:
					converted := Decomp_FairyDustSCR(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'sd':
				case game^.g of
					gid.Foreigner, gid.Quintia2:
					converted := Decomp_GreatSD(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'sil': converted := Decomp_SognaSIL(loader, PathCombine([scrpath, basename + '.txt']), game^.g);

				'sin':
				case game^.g of
					gid.MoreAndMore:
					converted := Decomp_CuisseSIN(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;

				'fmb','txb':
				case game^.g of
					gid.TrueLove98, gid.Reno98:
					converted := Decomp_ParsleyTXB(loader, PathCombine([scrpath, basename + '.txt']), game^.g);
				end;
			end;

			if (NOT converted) and (NOT decomp_param.tempOnly) then converted := _TryDynamicDispatch;

			// Report result.
			if NOT converted then
				decomp_logger('[skip] unknown file type')
			else
				inc(decomp_stats.numConverted);

		except
			on e : SkipException do begin
				inc(decomp_stats.numSkipped);
				decomp_logger('[skip] ' + e.Message);
			end;
			on e : DecompException do LogError(e.Message);
		end;

	finally
		if loader <> NIL then begin loader.free; loader := NIL; end;
	end;
end;

// ------------------------------------------------------------------

function SelectGame(newgame : PGameRecord) : PGameRecord;
var i : dword = 0;
	newinfpath : UTF8string;
begin
	with decomp_param do if (gidOverride <> NIL) and (gidOverride <> newgame) then begin
		decomp_logger('Autodetected ' + ShortName(newgame^.g) + ', override with ' + ShortName(gidOverride^.g));
		newgame := gidOverride;
	end;
	result := newgame;
	decomp_logger(strcat('Game: % [%]', [newgame^.desc, ShortName(newgame^.g)]));
	if decomp_param.onlyId then begin
		inc(decomp_stats.numConverted);
		exit;
	end;

	// Decide on a suitable project output directory.
	if decomp_param.outputOverride then begin
		if NOT DirectoryExists(decomp_param.outputDir) then begin
			if decomp_param.verbose then decomp_logger('mkdir ' + decomp_param.outputDir);
			mkdir(decomp_param.outputDir);
			i := IOresult;
		end;
	end
	else begin
		decomp_param.outputDir := PathCombine(['data', ShortName(newgame^.g)]);
		if NOT DirectoryExists(decomp_param.outputDir) then begin
			if decomp_param.verbose then decomp_logger('mkdir ' + decomp_param.outputDir);
			i := 0;
			if NOT DirectoryExists('data') then begin
				mkdir('data');
				i := IOresult;
			end;
			if i = 0 then begin
				mkdir(decomp_param.outputDir);
				i := IOresult;
			end;
			if i = 5 then begin
				decomp_logger(errortxt(i) + ' creating ' + decomp_param.outputDir);
				// access denied? Try putting the output under the user's profile...
				decomp_param.outputDir := PathCombine([decomp_param.profileDir, ShortName(newgame^.g)]);
				if NOT DirectoryExists(decomp_param.outputDir) then begin
					if decomp_param.verbose then decomp_logger('mkdir ' + decomp_param.outputDir);
					mkdir(decomp_param.outputDir);
					i := IOresult;
				end;
			end;
		end;
	end;
	if i <> 0 then raise Exception.Create(errortxt(i) + ' creating ' + decomp_param.outputDir);

	decomp_logger('Output directory: ' + decomp_param.outputDir);

	ResetMemResources;

	datainfpath := PathCombine([decomp_param.outputDir, 'data.inf']);
	if FileExists(datainfpath) then
		LoadInf(datainfpath, FALSE)
	else
		decomp_logger(datainfpath + ' doesn''t exist yet');

	// Combining the resource subdirs here once saves having to combine them repeatedly later.
	audpath := PathCombine([decomp_param.outputDir, 'aud']);
	gfxpath := PathCombine([decomp_param.outputDir, 'gfx']);
	scrpath := PathCombine([decomp_param.outputDir, 'scr']);

	// Grab the game's base resolution from newdata. Should be in both data and newdata, but newdata has priority.
	newinfpath := PathCombine([decomp_param.outputDir, 'newdata.inf']);
	if FileExists(newinfpath) then begin
		LoadInf(newinfpath, TRUE);
		i := GetDat(ShortName(newgame^.g));
		Assert(i < availableDatCount, 'i=' + strdec(i));
		gameConst.baseResXP := availableDats[i].baseResX;
		gameConst.baseResYP := availableDats[i].baseResY;
	end;
end;

procedure FinaliseGame(game : PGameRecord);
// After all files for a game have been converted, call this to do post-processing on the results and print a report.
// Overwrites the game's data.inf afterward.
begin
	if decomp_param.onlyId then exit;
	PostProcess(game);
	WriteInf(game);
end;

function AutoIdByFile(const filepath : UTF8string; dispatch : boolean) : PGameRecord;
var crc : dword;
begin
	crc := ChibiCRC(filepath);
	if decomp_param.verbose then decomp_logger(strcat('[ChibiCRC] %: &', [filepath, strhex(crc, 8)]));
	result := MatchCRC(crc);
	if result <> NIL then begin
		result := SelectGame(result);
		if (dispatch) and (NOT decomp_param.onlyId) then DispatchFile(filepath, result, NIL);
	end;
end;

function AutoIdByPath(const scanpath : UTF8string) : PGameRecord;
var f, ext : UTF8string;
begin
	for f in FindFiles_caseless(PathCombine([scanpath, '*'])) do begin
		ext := lowercase(ExtractFileExt(f));
		if (ext <> '.com') and (ext <> '.exe') then continue;
		result := AutoIdByFile(f, TRUE);
		if result <> NIL then exit;
	end;
	result := NIL;
end;

function ScanPath(const currentsearch : UTF8string; detectedgame : PGameRecord; recursed : boolean) : PGameRecord;
var filesr : TSearchRec;
	exelist, comlist, dirlist, filelist : TStringBunch;
	g : PGameRecord;
	i, execount, comcount, dircount, filecount : dword;
	curdir, filesuffix : UTF8string;
	searchresult : longint;
begin
	exelist := NIL; comlist := NIL; dirlist := NIL; filelist := NIL;
	execount := 0; comcount := 0; dircount := 0; filecount := 0;
	// Figure out what directory the current search is in.
	curdir := ExtractFilePath(currentsearch);

	// Build a list of files and directories matched by the current search.
	searchresult := FindFirst(currentsearch, faReadOnly or faDirectory, filesr);
	while searchresult = 0 do begin
		// directories...
		if (filesr.Attr and faDirectory <> 0) then begin
			if (filesr.Name <> '.') and (filesr.Name <> '..') then begin
				if dircount >= dirlist.Length then setlength(dirlist, dirlist.Length shl 1 + 4);
				dirlist[dircount] := filesr.Name;
				inc(dircount);
			end;
		end
		else begin
			// executables and other files...
			filesuffix := lowercase(ExtractFileExt(filesr.Name));
			if filesuffix = '.exe' then begin
				if execount >= exelist.Length then setlength(exelist, exelist.Length shl 1 + 4);
				exelist[execount] := filesr.Name;
				inc(execount);
			end
			else if filesuffix = '.com' then begin
				if comcount >= comlist.Length then setlength(comlist, comlist.Length shl 1 + 4);
				comlist[comcount] := filesr.Name;
				inc(comcount);
			end
			else begin
				if filecount >= filelist.Length then setlength(filelist, filelist.Length shl 1 + 16);
				filelist[filecount] := filesr.Name;
				inc(filecount);
			end;
		end;

		searchresult := FindNext(filesr);
	end;
	FindClose(filesr);

	if (execount or comcount or dircount or filecount) = 0 then decomp_logger('No hits in ' + currentsearch);
	// If user-given search path is just a single directory, treat the contents of that directory as the true search.
	// This avoids mistaking the single directory's parent directory as the search base.
	if ((execount or comcount or filecount) = 0) and (dircount = 1) and (NOT recursed) then begin
		g := ScanPath(PathCombine([curdir, dirlist[0], '*']), detectedgame, FALSE);
		if (g <> detectedgame) and (g <> @GameRecordTable[0]) then FinaliseGame(g);
		result := NIL;
		exit;
	end;

	// Examine the executables for hardcoded data. Some of the hardcoded stuff like song lists is needed for script
	// conversion, so this needs to be done first.
	if execount <> 0 then for i := execount - 1 downto 0 do begin
		exelist[i] := PathCombine([curdir, exelist[i]]);
		if detectedgame = NIL then detectedgame := AutoIdByFile(exelist[i], FALSE);
		if NOT decomp_param.onlyId then DispatchFile(exelist[i], detectedgame, NIL);
	end;

	// Try to autodetect and pick up metadata if game not yet autodetected.
	if detectedgame = NIL then begin
		detectedgame := AutoIdByPath(curdir);
		// Hack: LipstickAdv1, Can Can Bunny 3, Kimi Dake have no executable, only a sys.
		if detectedgame = NIL then begin
			exelist := FindFiles_caseless(PathCombine([curdir, 'diskx.sys']), FALSE, TRUE);
			if exelist = NIL then exelist := FindFiles_caseless(PathCombine([curdir, '*', 'adv98v.ovl']), FALSE, TRUE);
			if exelist <> NIL then detectedgame := AutoIdByFile(exelist[0], FALSE);
		end;
		if (detectedgame = NIL) and (NOT recursed) then
			detectedgame := AutoIdByPath(PathCombine([curdir, '..']));
		if detectedgame = NIL then detectedgame := @GameRecordTable[0];
	end;

	if NOT decomp_param.onlyId then begin
		// Dispatch com files and other files.
		if comcount <> 0 then for i := comcount - 1 downto 0 do
			DispatchFile(PathCombine([curdir, comlist[i]]), detectedgame, NIL);
		if filecount <> 0 then begin
			setlength(filelist, filecount);
			filelist.Sort;
			for i := filecount - 1 downto 0 do
				DispatchFile(PathCombine([curdir, filelist[i]]), detectedgame, NIL);
		end;
	end;

	// Scan subdirectories, if any. If a game was auto-id'd in a subdirectory, then finalise game on return from call.
	if dircount <> 0 then begin
		setlength(dirlist, dircount);
		dirlist.Sort;
		repeat
			dec(dircount);
			g := detectedgame;
			if g = @GameRecordTable[0] then g := NIL; // try to re-autodetect in subdir, if nothing found in current
			g := ScanPath(PathCombine([curdir, dirlist[dircount], '*']), g, TRUE);
			if (g <> detectedgame) and (g <> @GameRecordTable[0]) then FinaliseGame(g);
		until dircount = 0;
	end;

	result := detectedgame;
end;

procedure Decomp_ScanFiles;
// Decomp_param.inputPaths should be filled in before calling this, with each entry run through ExpandFileName.
var inputfile, inputpath, cached_inputpath : UTF8string;
	cached_id : PGameRecord = NIL;
begin
	if decomp_param.inputPaths.Length = 0 then exit;

	ResetMemResources;
	cached_inputpath := '';
	if decomp_param.gidOverride <> NIL then
		SelectGame(decomp_param.gidOverride)
	else if NOT decomp_param.outputOverride then begin
		decomp_param.outputDir := PathCombine(['data', 'unknown']);
		audpath := PathCombine([decomp_param.outputDir, 'aud']); // should rather do SelectGame unknown...
		gfxpath := PathCombine([decomp_param.outputDir, 'gfx']);
		scrpath := PathCombine([decomp_param.outputDir, 'scr']);
	end;

	for inputfile in decomp_param.inputPaths do begin
		// If consecutive input path items are in the same directory, carry over the autodetected game.
		inputpath := lowercase(ExtractFilePath(inputfile));
		if inputpath <> cached_inputpath then begin
			cached_inputpath := inputpath;
			if (cached_id <> NIL) and (cached_id <> @GameRecordTable[0]) then
				FinaliseGame(cached_id);
			cached_id := NIL;
		end;

		cached_id := ScanPath(inputfile, cached_id, FALSE);
	end;

	if (cached_id <> NIL) and (cached_id <> @GameRecordTable[0]) then
		FinaliseGame(cached_id);
end;

// Test cases:
// - decomp sanshimai/SK_101.OVL: game is autodetected once, no errors
// - decomp "sanshimai/SK_10?.OVL": game is autodetected only once
// - decomp "sanshimai/SK_101.OVL" "sanshimai/SK_102.OVL": game is autodetected only once
// - decomp "sanshimai/*.EXE": game is autodetected from the scanned file
// - decomp "sanshimai": game is autodetected once from the scanned file, all resources convert fine
// - run all of the above in 3sis/OVL: same outcome
// - run all of the above with gid override: still autodetected once, but gid override is applied so errors crop up
// - with 2+ games under same directory, decomp the holding directory: both games autodetected once and convert ok,
//   postprocessing ran successfully once for each game
// - decomp "Angels*": both games correctly autodetected and post-processed once
// - decomp "AngelsCollection1" "AngelsCollection2": both games correctly autodetected and post-processed once
// - decomp 2+ images in same folder not part of an autodetectable game: autodetect is only done once, no game detected
// - decomp TrueLove_e: only DATE gets processed, others dats are ignored

// Scanning logic:
// - One or more input paths passed in
//   + May point to individual files, individual directories, or wildcarded combos of both
//   + All encountered directories are scanned recursively
//   + ScanPath() processes one given input path entirely
//
// - All encountered files are dispatched to converter routines
//   + Dispatcher makes a best guess at which converter may work based on file suffix and current game id
//   + Try every converter until one works in case it's a supported file with a non-standard suffix
//   + Script files are highly game-specific and must have standard suffixes
//   + Bundles etc requiring more than one file combined must have standard suffixes, don't send to all converters
//   + Exe and com files are dispatched first, since they may contain hardcoded data referenced by other resources
//   + Other encountered files are then dispatched in bytewise order (unless prioritised, see below)
//   + Subdirectories are then sent back to ScanPath() in bytewise order
//
// - Prioritisation
//   + In rare cases, the same-named resource exists more than once among a game's data files; eg. DATE and DATG in
//     TrueLove, or hifi and lofi animations in HornyBun
//   + Have a list by game ID of such potential duplicates, exact files identified by exact size and then ChibiCRC
//   + All duplicate versions of a particular file have an index number
//   + Encountered files in each one directory are checked against the list; if they're on it at any priority, delay
//     dispatching them until the end
//   + If duplicates sharing an index are found, the lower priority one gets skipped
//   + Matched files are remembered for the duration of the detected game
//   + If in a later directory another duplicate is found, it overwrites the previous one or is ignored, by priority
//
// - Game id is needed for finetuned resource conversion
//   + Assume that if a game is detected in a directory, it's the only game present in and under that path
//   + Games can be detected by comparing the ChibiCRC of their exe or com files against gidtable
//   + If game id override is given, the auto-id value is ignored but must still be attempted to pick up metadata
//   + Detected auto-id value is passed down recursive ScanPath() calls
//   + If auto-id is null at start of ScanPath(), try to auto-id
//   + Auto-id runs ChibiCRC on all exe and com files in current path and parent directory, until match found
//     (only look in parent directory when ScanPath() is not recursed)
//   + To avoid unnecessary auto-id when input paths have several files in same directory, cache last auto-id result
//   + Cached auto-id result starts as null, since no game detected is a valid result
//
// - Some post-processing can only be done after all files have been converted, such as compositing
//   + If multiple input paths in same directory, only trigger after last path scanned
//   + Trigger post-proc after exiting ScanPath() call that did a successful auto-id
//   + ScanPath() returns used game id, so caller can determine if post-proc should trigger
//
// - Graphics metadata
//   + At start of Decomp_ScanFiles and when auto-id'd, the graphics list is reset and inited from the game's data.inf
//   + Newly converted graphics are added to the graphic files list and to a converted files list
//   + Post-proc will composite and beautify the converted files list only
//   + After triggering post-proc, write current graphics list as data.inf

initialization
finalization
	if postProcImg <> NIL then postProcImg.Destroy;
end.

