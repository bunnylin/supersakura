{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

procedure TEventmatic.AddAreaEvent( // deprecated!
	const name : UTF8string; inviewport : dword; locx, locy, sizex, sizey : longint;
	const _triggerlabel, _mouseonlabel, _mouseofflabel : UTF8string; _mouseonly, initialstate : boolean);
begin
	Assert(name = upcase(name));
	if sizex < 0 then begin inc(locx, sizex); sizex := -sizex; end;
	if sizey < 0 then begin inc(locy, sizey); sizey := -sizey; end;
	setlength(eventArea, length(eventArea) + 1);
	with eventArea[high(eventArea)] do begin
		eventName := name;
		areaInViewport := inviewport;
		areaLoc.left := locx;
		areaLoc.right := locx + sizex;
		areaLoc.top := locy;
		areaLoc.bottom := locy + sizey;

		with Viewportmatic.viewport[inviewport] do begin
			areaLoc.DerivePixelsFrom32k(viewportSizeP);
			inc(areaLoc.leftp, viewportLoc.leftp);
			inc(areaLoc.rightp, viewportLoc.leftp);
			inc(areaLoc.topp, viewportLoc.topp);
			inc(areaLoc.bottomp, viewportLoc.topp);
		end;
		centerPoint.x := (areaLoc.leftp + areaLoc.rightp) div 2;
		centerPoint.y := (areaLoc.topp + areaLoc.bottomp) div 2;

		triggerLabel := _triggerlabel;
		mouseOnLabel := _mouseonlabel;
		mouseOffLabel := _mouseofflabel;
		eventState := 0;
		if initialstate then eventState := 1;
		mouseOnly := _mouseonly;
	end;
end;

function TEventmatic.CreateElementEvent(const _ele : TElement) : PElementEvent;
// Creates and returns an event associated with the given element. Don't call this if element already has an event.
begin
	Assert(_ele <> NIL);
	if numElementEvents >= dword(length(elementEvent)) then setlength(elementEvent, numElementEvents + 4);
	result := @elementEvent[numElementEvents];
	result^.element := _ele;
	inc(numElementEvents);
end;

function TEventmatic.FindElementEvent(const _ele : TElement) : PElementEvent;
// Returns the element event associated with the given element, or NIL none found.
var i : dword;
begin
	if numElementEvents <> 0 then for i := numElementEvents - 1 downto 0 do
		if elementEvent[i].element = _ele then exit(@elementEvent[i]);
	result := NIL;
end;

function TEventmatic.FindOrCreateElementEvent(const _ele : TElement) : PElementEvent;
// Returns the element event associated with the given element. Creates a new event for it if necessary.
begin
	result := FindElementEvent(_ele);
	if result = NIL then result := CreateElementEvent(_ele);
end;

procedure TEventmatic.AddTimerEvent(
	const name : UTF8string; _triggerperiod : longint; const _triggerlabel : UTF8string; starttime : dword);
begin
	Assert(name = upcase(name));
	setlength(timerEvent, length(timerEvent) + 1);
	with timerEvent[high(timerEvent)] do begin
		eventName := name;
		triggerPeriod := _triggerperiod;
		timercounter := starttime;
		triggerLabel := _triggerlabel;
	end;
end;

procedure TEventmatic.PushOverableParams(const ele : TElement; const fib : TFiber);
begin
	with fib.namedparam do begin
		defined[CMDP_NAME] := TRUE;
		defined[CMDP_LOCX] := TRUE;
		defined[CMDP_LOCY] := TRUE;
		setlength(pStrValue[CMDP_NAME], languageList.Length);
		pStrValue[CMDP_NAME].SetAll(ele.eleName);
		pNumValue[CMDP_LOCX] := ele.eleLoc.x;
		pNumValue[CMDP_LOCY] := ele.eleLoc.y;
	end;
end;

procedure TEventmatic.CheckOverables;
// Checks if the current mouse cursor location is over any overable areas or elements. Spawns mouseon and mouseoff
// fibers appropriately. All due mouseoffs are spawned first, in the same order the overables were defined, then all
// due mouseons.
var i : dword;
	overnewarea : boolean = FALSE;
	overnewelement : boolean = FALSE;
	_fiber : TFiber;
begin
	with sysvar do begin
		// Check mouseoverable areas. Deprecated!
		if length(eventArea) <> 0 then
			for i := 0 to high(eventArea) do with eventArea[i] do begin
				if (mouseLocP.x >= areaLoc.leftp) and (mouseLocP.x < areaLoc.rightp)
				and (mouseLocP.y >= areaLoc.topp) and (MouseLocP.y < areaLoc.bottomp)
				then begin
					// Area is being overed!
					if eventState = 0 then begin
						// It wasn't overed before, so prepare to trigger mouseon.
						eventState := 1;
						if mouseOnLabel <> '' then begin
							overnewarea := TRUE;
							eventState := 2;
						end;
					end;
				end
				else begin
					// Area is not being overed!
					if eventState <> 0 then begin
						// It was overed before, so trigger mouseoff.
						eventState := 0;
						if mouseOffLabel <> '' then TFiber.Create(mouseOffLabel, eventName);
					end;
				end;
			end;

		// Check mouseoverable elements.
		if numElementEvents <> 0 then
			for i := 0 to numElementEvents - 1 do
				with elementEvent[i] do with element do with Viewportmatic.viewport[eleViewport] do begin
					// Check if the cursor is over the element and within the element's viewport.
					if (mouseLocP.x >= viewportLoc.leftp) and (mouseLocP.x < viewportLoc.rightp)
					and (mouseLocP.y >= viewportLoc.topp) and (mouseLocP.y < viewportLoc.bottomp)
					and (mouseLocP.x >= eleLoc.xp) and (mouseLocP.x < eleLoc.xp + longint(eleSizeP.w))
					and (mouseLocP.y >= eleLoc.yp) and (mouseLocP.y < eleLoc.yp + longint(eleSizeP.h))
					then begin
						// Element is being overed!
						if eventState = 0 then begin
							// It wasn't overed before, so prepare to trigger mouseon.
							eventState := 1;
							if mouseOnLabel <> '' then begin
								overnewelement := TRUE;
								eventState := 2;
							end;
						end;
					end
					else begin
						// Element is not being overed!
						if eventState <> 0 then begin
							// It was overed before, so trigger mouseoff.
							eventState := 0;
							if mouseOffLabel <> '' then begin
								_fiber := TFiber.Create(mouseOffLabel, 'off' + eleName);
								PushOverableParams(element, _fiber);
							end;
						end;
					end;
				end;
	end;

	// Trigger mouseon labels. Must be a separate step afterward to ensure all mouseoffs have been spawned first.
	if overnewelement then
		for i := 0 to high(elementEvent) do with elementEvent[i] do
			if eventState = 2 then begin
				eventState := 1;
				_fiber := TFiber.Create(mouseOnLabel, 'on' + element.eleName);
				PushOverableParams(element, _fiber);
			end;
	if overnewarea then // deprecated
		for i := 0 to high(eventArea) do with eventArea[i] do
			if eventState = 2 then begin
				eventState := 1;
				TFiber.Create(mouseOnLabel, eventName);
			end;
end;

function TEventmatic.TriggerOverables(usingmouse : boolean) : dword;
// Spawns a trigger fiber for every element currently in a mouseovered state (ie. the overable was clicked).
// If multiple elements are overed simultaneously, fibers are spawned in the same order as the overables were defined.
// Returns number of overables triggered. Some overables can be mouse-only, so if triggering via keyboard/gamepad, call
// with usingmouse = false to ignore those overables.
var i : dword;
	_fiber : TFiber;
begin
	result := 0;

	if numElementEvents <> 0 then
		for i := 0 to numElementEvents - 1 do with elementEvent[i] do
			if (eventState <> 0) and (triggerLabel <> '') and ((usingmouse) or (NOT mouseOnly)) then begin
				_fiber := TFiber.Create(triggerLabel, element.eleName);
				PushOverableParams(element, _fiber);
				inc(result);
			end;

	if length(eventArea) <> 0 then // deprecated
		for i := 0 to high(eventArea) do with eventArea[i] do
			if (eventState <> 0) and (triggerLabel <> '') and ((usingmouse) or (NOT mouseOnly)) then begin
				TFiber.Create(triggerLabel, eventName);
				inc(result);
			end;
end;

procedure TEventMatic.RemoveTimer(index : dword);
var j : dword;
begin
	Assert(index < dword(length(timerEvent)));
	if longint(index) < high(timerEvent) then
		for j := index to high(timerEvent) do
			timerEvent[j] := timerEvent[j + 1];
	setlength(timerEvent, high(timerEvent));
end;

procedure TEventmatic.AdvanceTimers(tickcount : dword);
// Advances all defined timer events by tickcount and triggers any that have reached their trigger period. Timers can
// trigger more than once during this call if the period is short or tickcount is high.
var i, period : dword;
begin
	if length(timerEvent) = 0 then exit;
	i := 0;
	while i < dword(length(timerEvent)) do with timerEvent[i] do begin
		inc(i);
		if triggerPeriod <> 0 then begin
			period := abs(triggerPeriod);
			inc(timerCounter, tickcount);
			while timerCounter >= dword(period) do begin
				TFiber.Create(triggerLabel, eventName);
				// Negative frequency = one-shot timer
				if triggerPeriod < 0 then begin
					dec(i);
					RemoveTimer(i);
					break;
				end;
				dec(timerCounter, period);
			end;
		end;
	end;
end;

procedure TEventmatic.Reset;
begin
	setlength(eventArea, 0);
	setlength(elementEvent, 0);
	numElementEvents := 0;
	setlength(timerEvent, 0);
	interruptLabel := '';
	escInterruptLabel := '';
end;

procedure TEventmatic.Remove(const name : UTF8string; element : boolean = TRUE; timer : boolean = TRUE);
// Removes all area, element, and timer events by the given name. Name must be uppercased before calling.
var i, j : dword;
begin
	Assert(name = upcase(name));
	if length(eventArea) <> 0 then for i := high(eventArea) downto 0 do // deprecated
		if eventArea[i].eventName = name then begin
			j := high(eventArea);
			if i < j then eventArea[i] := eventArea[j];
			setlength(eventArea, j);
		end;

	if element then if numElementEvents <> 0 then for i := numElementEvents - 1 downto 0 do
		if elementEvent[i].element.eleName = name then begin
			dec(numElementEvents);
			if i < numElementEvents then
				for j := i to numElementEvents - 1 do
					elementEvent[j] := elementEvent[j + 1];
			if numElementEvents shl 2 < dword(length(elementEvent)) then
				setlength(elementEvent, numElementEvents shl 1);
		end;

	if timer then if length(timerEvent) <> 0 then for i := high(timerEvent) downto 0 do
		if timerEvent[i].eventName = name then RemoveTimer(i);
end;

function TEventmatic.Serialise : TStringBunch;
var i, j : dword;
	opt : string;
begin
	result := NIL;
	setlength(result, length(eventArea) + longint(numElementEvents) + length(timerEvent) + 2);
	j := 0;
	if length(eventArea) <> 0 then for i := 0 to high(eventArea) do with eventArea[i] do begin // deprecated
		opt := '';
		if triggerLabel <> '' then opt := ' label:`' + triggerLabel + '`';
		if mouseOnLabel <> '' then opt := opt + ' mouseon:`' + mouseOnLabel + '`';
		if mouseOffLabel <> '' then opt := opt + ' mouseoff:`' + mouseOffLabel + '`';
		if mouseOnly then opt := opt + ' mouseonly:1';
		if eventState <> 0 then opt := opt + ' value:1';
		result[j] := strcat(
			'event.create.area `%` viewport:% x:% y:% sizex:% sizey:%%',
			[eventName, areaInViewport, areaLoc.left, areaLoc.top,
			areaLoc.right - areaLoc.left, areaLoc.bottom - areaLoc.top, opt]);
		inc(j);
	end;
	if numElementEvents <> 0 then for i := 0 to numElementEvents - 1 do with elementEvent[i] do begin
		opt := '';
		if triggerLabel <> '' then opt := ' label:`' + triggerLabel + '`';
		if mouseOnLabel <> '' then opt := opt + ' mouseon:`' + mouseOnLabel + '`';
		if mouseOffLabel <> '' then opt := opt + ' mouseoff:`' + mouseOffLabel + '`';
		if mouseOnly then opt := opt + ' mouseonly:1';
		if eventState <> 0 then opt := opt + ' value:1';
		result[j] := strcat('event.setlabel `%`%', [element.eleName, opt]);
		inc(j);
	end;
	if length(timerEvent) <> 0 then for i := 0 to high(timerEvent) do with timerEvent[i] do begin
		opt := '';
		if triggerLabel <> '' then opt := ' label:`' + triggerLabel + '`';
		if timerCounter <> 0 then opt := opt + ' time:' + strdec(timerCounter);
		result[j] := strcat('event.create.timer `%`% freq:%', [eventName, opt, triggerPeriod]);
		inc(j);
	end;

	setlength(result, j);
end;

