{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

procedure TGob.AutoAdopt;
// Compare the screen location of this gobs to all other visible gobs at or below its Z-level in the same viewport.
// The first that completely contains the kid is set as the parent. If no parent is found, the viewport's background
// or topmost overlay is used; if that also failed, the element is left orphan.
// Autoadopt is only for gob elements as both parent and child. Other elements set their parents explicitly.
var i : dword;
	parent : TElement;
	kidloc, parentloc : TEdgeCoord32k;
begin
	// safety
	if eleViewport >= dword(length(Viewportmatic.viewport)) then begin
		LogError(strcat('Element adopt (%) bad viewport: %', [eleName, eleViewport]));
		exit;
	end;
	//log('Adopting gob ' + eleName);
	kidloc.left := eleLoc.x;
	kidloc.right := kidloc.left + longint(eleSize.w);
	kidloc.top := eleLoc.y;
	kidloc.bottom := kidloc.top + longint(eleSize.h);

	with EleHub do for i := numElements - 1 downto 0 do begin
		if (NOT (ele[i] is TGob))
		or (ele[i].eleZLevel > eleZLevel)
		or (ele[i] = self)
		// Parent must be in the same viewport, and already visible or about to become visible.
		or (ele[i].eleViewport <> eleViewport)
		or (NOT (ele[i].eleIsVisible or ele[i].eleRenderNow))
		then continue;

		// The parent candidate's own parents must meet the same criteria.
		parent := ele[i].eleParent;
		while parent <> NIL do begin
			if (parent = self) or (NOT (parent.eleIsVisible or parent.eleRenderNow)) then break;
			parent := parent.eleParent;
		end;
		if parent <> NIL then continue;

		// Backgrounds and overlays are automatic adopters, whether the kid fully overlaps them or not.
		if (ele[i] as TGob).GobType in [EGobType.Bkg, EGobType.Overlay] then begin
			eleParent := ele[i];
			eleDisplacement := eleParent.eleDisplacement;
			exit;
		end;

		// Finally, does the parent candidate rect contain the kid rect?
		parentloc.left := ele[i].eleLoc.x;
		parentloc.right := parentloc.left + longint(ele[i].eleSize.w);
		parentloc.top := ele[i].eleLoc.y;
		parentloc.bottom := parentloc.top + longint(ele[i].eleSize.h);
		// Check for containment... (award +2 to fuzziness to allow for rounding error)
		if (parentloc.left - 2 <= kidloc.left) and (parentloc.right + 2 >= kidloc.right)
		and (parentloc.top - 2 <= kidloc.top) and (parentloc.bottom + 2 >= kidloc.bottom)
		then begin
			eleParent := ele[i];
			eleDisplacement := eleParent.eleDisplacement;
			exit;
		end;
	end;

	// Failed to adopt.
	eleParent := NIL;
end;

procedure TGob.RefreshFrameP; inline;
begin
	Assert(cachedObject <> NIL);
	Assert(gobFrame < dword(length(cachedObject.framePtr)));
	eleFrameP := cachedObject.framePtr[gobFrame];
	Assert(eleFrameP >= @cachedObject.bitmap[0]);
	Assert(eleFrameP + eleSizeP.w * eleSizeP.h shl 2 - @cachedObject.bitmap[0] <= length(cachedObject.bitmap));
end;

function TGob.EnsureCached : boolean;
begin
	cachedObject := GetGraphicObject(gfxListName, eleSizeP.w, eleSizeP.h);
	result := (cachedObject <> NIL) and (cachedObject.bitmap <> NIL);
	if result then RefreshFrameP else eleIsVisible := FALSE;
end;

function TGob.ToString : UTF8string;
var _parent : string = '';
begin
	if eleParent <> NIL then _parent := eleParent.eleName;
	result := strcat('%:%  %  gfx:%  parent:%  vp:%  loc:%,%  locp:%,%  anchor:%,%  sizep:%  frame:%  z:%',
		[longint(GetCurrentIndex), eleName, strenum(typeinfo(GobType), @GobType), gfxListName, _parent,
		eleViewport, eleLoc.x / 32768, eleLoc.y / 32768, eleLoc.xp, eleLoc.yp,
		eleAnchor.x / 32768, eleAnchor.y / 32768, eleSizeP.ToString, longint(gobFrame), eleZLevel]);
end;

constructor TGob.Create(
	listname : UTF8string; _eleName : string31; _parent : string31; _gobtype : EGobType;
	inviewport, locx, locy, zlevel, anchorx, anchory, sizex, sizey : longint);
// Creates a gob, set to be drawn immediately on the next Renderer pass.
// Listname must be an available graphic object or dynamic loader string.
// eleName is used to refer to the element in script code; it may be empty, in which case eleName defaults to listname.
// If an existing gob has the same name, z-level, viewport, default parent, and compatible type, the existing one is
// immediately replaced by the new one, inheriting some attributes.
// Initial 32k x and y coordinates must be set, relative to the containing viewport. A z-level is used to place this
// element relative to other elements in this viewport.
var i, _frame : dword;
	index : longint = -1;
	_alpha : byte = $FF;
	_visible : boolean = FALSE;
begin
	Assert(listname = upcase(listname));
	Assert(_eleName = upcase(_eleName));
	Assert(_parent = upcase(_parent));
	// Sanitise the given names, validate parameters.
	gfxListName := listname;
	if gfxlistname = '' then gfxListName := '|0';
	runtimeDynamic := (gfxListName[1] = '|');
	loadtimedynamic := (gfxListName[1] = '>');
	if _eleName = '' then _eleName := gfxListName;

	if inviewport < 0 then inviewport := EleHub.defaultViewport;
	if inviewport >= length(Viewportmatic.viewport) then begin
		LogError(strcat('TGob.Create: bad viewport % for %', [inviewport, eleName]));
		inviewport := 0;
	end;
	eleViewport := inviewport;
	cachedObject := NIL;
	_frame := 0;

	// Each viewport can only have one background at a time. Creating a new one will replace the old background,
	// importing some of its characteristics, most importantly retaining its child gobs.
	// For example 3sis sk_123 draws sprites before the background, so the sprites have to carry over.
	if _gobtype = EGobType.Bkg then with EleHub do begin
		dword(index) := Viewportmatic.viewport[inviewport].GetBackgroundIndex;
		if dword(index) < numElements then begin
			Assert(ele[index] is TGob);

			if (zlevel = ele[index].eleZLevel)
			or ((zlevel < ele[index].eleZLevel) and ((index = 0) or (ele[index - 1].eleZLevel < zlevel)))
			or ((zlevel > ele[index].eleZLevel) and ((index = numElements - 1) or (ele[index + 1].eleZLevel >= zlevel)))
			then begin
				if (_parent = '') and (ele[index].eleParent <> NIL) then _parent := ele[index].eleParent.eleName;
				{_frame := (ele[index] as TGob).gobFrame;
				_alpha := ele[index].eleAlpha;
				_visible := ele[index].eleIsVisible;}

				for i := numElements - 1 downto 0 do
					if ele[i].eleParent = ele[index] then ele[i].eleParent := self;
				{with EffectHub do if fxCount <> 0 then for i := fxCount - 1 downto 0 do
					if fx[i].fxElement = ele[index] then fx[i].fxElement := self;
				with Eventmatic do if numElementEvents <> 0 then for i := numElementEvents - 1 downto 0 do
					if elementEvent[i].element = ele[index] then elementEvent[i].element := self;}
			end;
			ele[index].Destroy;
			index := -1;
		end;
		// In any case, insert this bkg at the bottom of its z-level group.
		index := 0;
		while (dword(index) < numElements) and (zlevel > ele[index].eleZLevel) do inc(index);
	end;

	inherited Create(_eleName, _parent, inviewport, locx, locy, zlevel, anchorx, anchory, sizex, sizey, index);

	GobType := _gobtype;
	animTimer := $FFFFFFFF; // non-animating
	animSeqP := 0;
	gobFrame := _frame;
	eleAlpha := _alpha;
	eleIsVisible := _visible;
	if runtimeDynamic then
		solidBlit.FromRGBA4(valhex(gfxListName))
	else if (eleSize.w = 0) or (eleSize.h = 0) then begin
		if loadtimeDynamic then begin
			// If a loadtime gob doesn't isn't specified with size values, extract the requested pixel size from the
			// name string, convert it to 32k, and use that as the gob size.
			GetLoadtimeDynamicSize(gfxListName, i, _frame);
			if _frame = 0 then raise Exception.Create('Failed to parse size from "' + gfxListName + '"');
			if eleSize.w = 0 then eleSize.w := i;
			if eleSize.h = 0 then eleSize.h := _frame;
		end
		else begin
			i := GetGraphicFile(gfxListName);
			if i = 0 then raise Exception.Create('Graphic not found: ' + gfxListName);
			with graphicFiles[i] do begin
				eleSize.w := (origSizeXP shl 15 + origResXP shr 1) div origResXP;
				eleSize.h := (origFrameHeightP shl 15 + origResYP shr 1) div origResYP;
				// Can't apply each image's built-in offset from the inf directly to element coordinates here, they'd
				// get re-applied on load state.
			end;
		end;
	end;
	// Must be after overwrites to know visibility, must be before autoadopt since it needs the size.
	// This also calls CacheGraphic at the calculated size, so cachedObject will be valid.
	UpdateSizep;
	Assert((runtimeDynamic) or (cachedObject <> NIL));

	if GobType = EGobType.Anim then with cachedObject do begin
		if (runtimeDynamic) or ((sourceFile <> NIL) and (sourceFile.sequence = NIL)) then
			GobType := EGobType.Sprite
		else if (sourceFile <> NIL) and (sourceFile.sequence <> NIL) and (sourceFile.sequence[0] and $FFFF <> $FFFF) then
			animTimer := 0 // enable animating; todo: switch to anim snippets
		else
			animtimer := $FFFFFFFF; // freeze on first displayed frame
	end;

	log(strcat('show % as "%" vp=%', [gfxListName, eleName, inviewport]));
end;

constructor TGob.Clone(src : TGob);
begin
	eleName := src.eleName;
	gfxListName := src.gfxListName;
	cachedObject := src.cachedObject;

	eleParent := src.eleParent;
	eleViewport := src.eleViewport;

	eleAnchor := src.eleAnchor;
	eleLoc := src.eleLoc;
	eleSize := src.eleSize;
	eleSizeP := src.eleSizeP;
	eleSizeMultiplier := src.eleSizeMultiplier;

	eleFrameP := src.eleFrameP;
	gobFrame := src.gobFrame;
	animSeqP := src.animSeqP;
	animTimer := src.animTimer;

	solidBlit := src.solidBlit;
	eleZLevel := src.eleZLevel;
	GobType := src.GobType;
	eleAlpha := src.eleAlpha;

	eleRenderNow := src.eleRenderNow;
	eleIsVisible := src.eleIsVisible;
	runtimeDynamic := src.runtimeDynamic;
	loadtimeDynamic := src.loadtimeDynamic;
end;

