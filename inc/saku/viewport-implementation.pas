{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function TViewport.GetBackgroundIndex : dword;
// Returns the lowest background element in this viewport, visible or not. If none found, returns maxuint.
begin
	result := high(dword); // silence compiler
	with EleHub do if numElements <> 0 then for result := 0 to numElements - 1 do
		if (ele[result] is TGob)
		and (ele[result].eleViewport = viewportIndex)
		and ((ele[result] as TGob).gobType = EGobType.Bkg) then exit;
	result := high(dword);
end;

procedure TViewport.Update;
// If you've changed any viewport parameters, call this afterward to recalculate the viewport's pixel values and
// propagate the changes to any child viewports. This also adds a screen refresh region.
// This also needs to be called for viewport 0 on window size change.
var newportsizep : TSizeP;
	{$ifndef sakucon} letterboxsizep : TSizeP; {$endif}
	newportlocp : TCoordP;
	i : dword;
begin
	if viewportIndex = 0 then begin
		// Viewport 0 is always equal to the current game window.
		viewportSizeP.w := sysvar.windowSize.w;
		viewportSizeP.h := sysvar.windowSize.h;
		with viewportLoc do begin
			leftp := 0; topp := 0;
			rightp := viewportSizeP.w;
			bottomp := viewportSizeP.h;
		end;
		viewportRatio.w := viewportSizeP.w;
		viewportRatio.h := viewportSizeP.h;
		UpdateCoscosTable(sysvar.windowSize.w + sysvar.windowSize.h);
		// All previous screen refresh rects can be dropped, as they are now mis-sized.
		Rendermatic.numfresh := 0;
	end
	else begin
		if viewportParent >= viewportIndex then begin
			LogError(strcat('UpdateViewport: viewport % parent % can''t be below child', [viewportIndex, viewportParent]));
			exit;
		end;

		// Mark the previous viewport pixel position for redrawing.
		AddRefresh;

		// Start from the parent's pixel coords.
		with Viewportmatic.viewport[viewportParent] do begin
			newportlocp.x := viewportLoc.leftp;
			newportlocp.y := viewportLoc.topp;
			newportsizep := viewportSizeP;
		end;
		log(strcat('Viewport % parent=%: locp=% sizep=%',
			[viewportIndex, viewportParent, newportlocp.ToString, newportsizep.ToString]));

		{$ifndef sakucon}
		// Apply letterboxing. (In console mode, skip this as unworkable.)
		if (viewportRatio.w <> 0) and (viewportRatio.h <> 0) then begin
			// To transform the current viewport to the required aspect ratio:
			//   new width = cury * reqx / reqy
			//   new height = curx * reqy / reqx
			letterboxsizep.w := (newportsizep.h * viewportRatio.w + viewportRatio.h shr 1) div viewportRatio.h;
			letterboxsizep.h := (newportsizep.w * viewportRatio.h + viewportRatio.w shr 1) div viewportRatio.w;
			// One will require enlarging the current viewport, the other shrinking it. We have to stay within the
			// parent, so we can only shrink.
			if letterboxsizep.w < newportsizep.w then begin
				inc(newportlocp.x, (newportsizep.w - letterboxsizep.w) shr 1); // center horizontally
				newportsizep.w := letterboxsizep.w;
			end;
			if letterboxsizep.h < newportsizep.h then begin
				inc(newportlocp.y, (newportsizep.h - letterboxsizep.h) shr 1); // center vertically
				newportsizep.h := letterboxsizep.h;
			end;
			log(strcat('Letterbox ratio %:% -> locp + %', [viewportRatio.w, viewportRatio.h, newportlocp.ToString]));
		end;
		{$endif}
		// Apply 32k coords within the new box to get final values.
		if viewportLoc.right < viewportLoc.left then viewportLoc.right := viewportLoc.left;
		if viewportLoc.bottom < viewportLoc.top then viewportLoc.bottom := viewportLoc.top;

		viewportLoc.DerivePixelsFrom32k(newportsizep);
		inc(viewportLoc.leftp, newportlocp.x);
		inc(viewportLoc.rightp, newportlocp.x);
		inc(viewportLoc.topp, newportlocp.y);
		inc(viewportLoc.bottomp, newportlocp.y);
		viewportSizeP.w := viewportLoc.rightp - viewportLoc.leftp;
		viewportSizeP.h := viewportLoc.bottomp - viewportLoc.topp;

		log(strcat('Viewport %: loc %,% to %,%;  locp %,% to %,% (%)',
			[viewportIndex, viewportLoc.left, viewportLoc.top, viewportLoc.right, viewportLoc.bottom,
			viewportLoc.leftp, viewportLoc.topp, viewportLoc.rightp, viewportLoc.bottomp,
			viewportSizeP.ToString]));
	end;

	// Mark the updated viewport pixel position for redrawing.
	AddRefresh;

	// Refresh all children viewports of this viewport.
	i := viewportIndex + 1;
	if Viewportmatic <> NIL then with Viewportmatic do while i < dword(length(viewport)) do begin
		if (viewport[i] <> NIL) and (viewport[i].viewportParent = viewportIndex) then viewport[i].Update;
		inc(i);
	end;

	// Refresh all other things that exist relative to this viewport.
	if EleHub <> NIL then with EleHub do begin
		if numElements <> 0 then for i := numElements - 1 downto 0 do
			if ele[i].eleViewport = viewportIndex then ele[i].UpdateSizeP;
	end;

	if BoxHub <> NIL then with BoxHub do for i := high(textbox) downto 0 do with textbox[i] do
		if boxInViewport = viewportIndex then parametersNeedUpdate := TRUE;

	if Eventmatic <> NIL then with Eventmatic do
		if length(eventArea) <> 0 then for i := high(eventArea) downto 0 do with eventArea[i] do
			if areaInViewport = viewportIndex then begin
				areaLoc.DerivePixelsFrom32k(viewportSizeP);
				inc(areaLoc.leftp, viewportLoc.leftp);
				inc(areaLoc.rightp, viewportLoc.leftp);
				inc(areaLoc.topp, viewportLoc.topp);
				inc(areaLoc.bottomp, viewportLoc.topp);
				centerPoint.x := (areaLoc.leftp + areaLoc.rightp) div 2;
				centerPoint.y := (areaLoc.topp + areaLoc.bottomp) div 2;
			end;
end;

procedure TViewport.AddRefresh; inline;
// Marks the viewport's entire area for redrawing at the next renderer pass.
begin
	Rendermatic.AddRefresh(viewportLoc.leftp, viewportLoc.topp, viewportLoc.rightp, viewportLoc.bottomp, 0);
end;

procedure TViewport.Reset;
// Resets a viewport to basically zero values at almost everything. This should probably refresh all elements in the
// viewport too, but if this is only called during bootup, then it's not important.
begin
	viewportParent := 0;
	viewportRatio.w := 0; viewportRatio.h := 0;
	fillbyte(viewportLoc, sizeof(viewportLoc), 0);
	viewportLoc.right := 32768;
	viewportLoc.bottom := 32768;
	fillbyte(viewportSizeP, sizeof(viewportSizeP), 0);
	Update;
end;

function TViewport.ToString : UTF8string;
begin
	result := strcat('Viewport %: parent:%  ratio:%  loc:%,%  locp:%,%  sizep:%',
		[viewportIndex, viewportParent, viewportRatio.ToString, viewportLoc.left / 32768, viewportLoc.top / 32768,
		viewportLoc.leftp, viewportLoc.topp, viewportSizeP.ToString]);
end;

constructor TViewport.Create(viewnum : dword; const clonefrom : TViewport);
begin
	Viewportmatic.viewport[viewnum] := self;
	viewportIndex := viewnum;
	if clonefrom = NIL then begin
		Reset;
		exit;
	end;
	viewportParent := clonefrom.viewportParent;
	viewportRatio := clonefrom.viewportRatio;
	viewportLoc := clonefrom.viewportLoc;
	viewportSizeP := clonefrom.viewportSizeP;
end;

// ------------------------------------------------------------------

procedure TViewportmatic.SetNumViewports(newcount : dword);
var i, j : dword;
begin
	if newcount = dword(length(viewport)) then exit;
	if newcount < dword(length(viewport)) then begin
		for i := length(viewport) - 1 downto newcount do if viewport[i] <> NIL then viewport[i].Destroy;
		setlength(viewport, newcount);
	end
	else begin
		i := length(viewport);
		setlength(viewport, newcount);
		for j := i to newcount - 1 do viewport[j] := NIL;
		while i < newcount do begin
			TViewport.Create(i, NIL);
			inc(i);
		end;
	end;
end;

procedure TViewportmatic.InheritState(const src : TViewportmatic);
var i : dword;
begin
	SetNumViewports(0);
	setlength(viewport, length(src.viewport));
	for i := 0 to high(viewport) do
		TViewport.Create(i, src.viewport[i]);
end;

procedure TViewportmatic.Reset;
begin
	SetNumViewports(1);
end;

function TViewportmatic.Serialise : TStringBunch;
var i : dword;
begin
	result := NIL;
	setlength(result, length(viewport) - 1);
	for i := 1 to high(viewport) do with viewport[i] do
		result[i - 1] := strcat(
			'viewport.setparams viewport:% parent:% ratiox:% ratioy:% x:% y:% sizex:% sizey:%',
			[i, viewportParent, viewportRatio.w, viewportRatio.h, viewportLoc.left, viewportLoc.top,
			viewportLoc.right - viewportLoc.left, viewportLoc.bottom - viewportLoc.top]);
end;

destructor TViewportmatic.Destroy;
begin
	SetNumViewports(0);
	inherited;
end;

