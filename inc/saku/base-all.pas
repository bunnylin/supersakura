{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Override "supersakura-whatever" with "ssakura" for conciseness.
// This is used by GetAppConfigDir to decide on a good config directory.
function truename : ansistring;
begin
	truename := 'ssakura';
end;

procedure SakuExit;
// Procedure called automatically on program exit.
var logfileavail : boolean;
begin
	logfileavail := TextRec(LogFile).mode <> fmClosed;
	if logfileavail then log('Quitting...');
	sysvar.quit := TRUE;

	{$ifndef sakucon}
	// Release SDL resources.
	if sdl_GamepadH <> NIL then SDL_GameControllerClose(sdl_GamepadH);
	if Rendermatic.SDL.rendererH <> NIL then SDL_DestroyRenderer(Rendermatic.SDL.rendererH);
	if Rendermatic.SDL.mainTextureH <> NIL then SDL_DestroyTexture(Rendermatic.SDL.mainTextureH);
	if Rendermatic.SDL.mainWindowH <> NIL then SDL_DestroyWindow(Rendermatic.SDL.mainWindowH);
	if logfileavail then log('SDL resources released');
	if soundserver <> NIL then soundserver.Destroy;
	{$endif}

	// Free whatever other memory was reserved.
	Rendermatic.Destroy;
	Fontmatic.Reset;
	//Savematic.Destroy; // doesn't do anything at this time

	// Print out the error message if exiting unnaturally.
	if (erroraddr <> NIL) or (exitcode <> 0) then begin
		// Also print the script code history.
		if logfileavail then begin
			LogError(errortxt(exitcode));
			{$ifdef bonk}
			if scr <> NIL then begin
				log('Script history:');
				for i := 15 downto 0 do begin
					if scr^.historyindex <> 0 then
						dec(scr^.historyindex)
					else
						scr^.historyindex := 15;
					log(strdec(i) + ': ' + strdec(scr^.history[scr^.historyindex]));
				end;
			end;
			{$endif}
		end;
	end;

	if HubStack <> NIL then HubStack.Destroy;
	if Varmon <> NIL then Varmon.Destroy;
	{$ifdef sakucon}
	GotoXY(0, sysvar.windowSize.h - 1);
	{$else}
	// Quitting TTF or SDL cleans up and invalidates any remaining SDL handles and resources.
	// Any references to them must have been released or dropped before calling these.
	if TTF_WasInit then TTF_Quit;
	SDL_Quit;
	{$endif}
end;

procedure InitEverything;
var i : dword;
	txt, profiledir : UTF8string;
begin
	OnGetApplicationName := @truename;
	// Get the current directory and executable name!
	// The executable name is used for the config file, and the current directory is used for default file IO.
	with sakuparam do begin
		basePath := paramstr(0);
		appName := ExtractFileName(basePath);
		i := pos('.', appName);
		if i <> 0 then setlength(appName, i - 1);
		basePath := ExtractFilePath(basePath);

		// If the base directory is write-protected, fall back to an app-specific directory under the user's profile.
		profiledir := GetAppConfigDir(FALSE);

		// Set up a log file. Try the current directory first...
		logPath := PathCombine([basePath, 'saku.log']);
		assign(LogFile, logPath);
		filemode := 1; rewrite(LogFile); // write-only
		i := IOresult;
		if i = 5 then begin
			// Access denied! Try the user's profile directory...
			mkdir(profiledir);
			while IOresult <> 0 do ;
			logPath := PathCombine([profiledir, 'saku.log']);
			assign(LogFile, logPath);
			filemode := 1; rewrite(LogFile); // write-only
			i := IOresult;
		end;
		if i <> 0 then raise Exception.Create(errortxt(i) + ' trying to create ' + logPath);
	end;

	logbuffers.transcriptBufIndex := 0;
	logbuffers.debugBufIndex := 0;
	logbuffers.errorMsg := '';
	log('--- SuperSakura --- ' + GetSuperSakuraVersion + ' ---');
	asman_logger := @Log;
	asman_CompileScript := @CompileScript;

	THubStack.Create; // saves self as variable HubStack

	// Install the exit handler proc.
	AddExitProc(@sakuexit);

	// The Mesa software renderer throws a SIGFPE floating point exception, invalid operation or divide by zero, on
	// init or renderer creation. https://github.com/PascalGameDevelopment/SDL2-for-Pascal/issues/56
	// Additionally, the radeon hardware renderer throws FPE's as well. There are probably others. This is due to the
	// exception mask which hides all those errors by default when using C, but enables and trips over some of them
	// when using Pascal. A fundamental inconsistency...
	// If calling badly-behaved external floating point code, FPE errors must all be hidden as their code expects.
	SetExceptionMask(GetExceptionMask + [exInvalidOp,exDenormalized,exZeroDivide,exOverflow,exUnderflow,exPrecision]);

	// Basic variable init. Sysvars carry over even when returning to a game's main script. Some of these get saved in
	// a configuration file.
	BoxHub.allBoxesHidden := FALSE;
	fillbyte(sysvar, sizeof(sysvar), 0);
	with sysvar do begin
		skipText := ESkipState.No;
		autoChoice := FALSE;
		fullscreen := FALSE; // meaningless on consoles
		maxFiberSteps := 32000;
		debugAllowed := 0;
		dropdownConsoleState := EDDCState.None;
		keysPolled := FALSE;
		restart := TRUE;
	end;
	preference.Reset;

	// The command param mappings may need initing.
	if ss_cmdparams[CMD_TBOX_PRINT][CMDP_BOX] = ARG_INVALID then ss_cmdparams_init;

	{$ifndef sakucon}
	InitSDL;
	with sysvar do begin
		mutedAudio := FALSE;
	end;
	{$else}
	with sysvar do begin
		windowSize.w := 80; windowSize.h := 25;
	end;

	// Hide the console cursor. Set the console colors to an expected default.
	CrtShowCursor(FALSE);
	SetColor($0007);
	Rendermatic.InitPalette;
	{$endif}

	// Read the user configuration, first from the base directory, then from the user's local profile directory.
	txt := sakuparam.appName + '.ini';
	try
		preference.ReadConfig(PathCombine([sakuparam.basePath, txt]));
	except
		on e : Exception do log(e.Message);
	end;
	try
		preference.ReadConfig(PathCombine([profiledir, txt]));
	except
		on e : Exception do log(e.Message);
	end;
	{$ifndef sakucon}
	InitAudio;
	{$endif}
	InitVarmon;

	// Load the front end dat, and game dat if binary has been renamed. The front end dat also has other common GUI
	// scripts (metamenu, dialogs) that dats loaded afterward will inherit.
	LoadDatCommon(frontEndName);
	txt := lowercase(sakuparam.appName);
	if copy(txt, 1, 11) <> frontEndName then LoadDatCommon(txt);

	// Load the other dats given on the commandline.
	with sakuparam do if cmdlineDats.Length <> 0 then begin
		for txt in cmdlineDats do LoadDatCommon(txt);
		// Clean up. The dat names are now in availableDats[].
		setlength(cmdlineDats, 0);
	end;

	if GetScript(mainLabelName) = 0 then raise Exception.Create('Main script not found: ' + mainLabelName);

	SpawnWindow;
end;

procedure MainLoop;
var tickcount, tickmark : dword;
	{$ifndef sakucon}
	evd : TSDL_event;
	{$endif}
begin
	tickmark := dword(GetMsecTime);
	while sysvar.restart do begin
		sysvar.restart := FALSE;
		sysvar.quit := FALSE;
		ResetDefaults;
		TFiber.Create(mainLabelName, 'MAIN');

		while TRUE do begin

			if sakuparam.frameDelay = 0 then begin
				if sysvar.pauseState <> EPauseState.Paused then begin
					// Automatic frame limiter... wait a bit, to achieve regular frame times.
					{$push}{$R-}
					longint(tickcount) := preference.restTime - dword(dword(GetMsecTime) - tickmark);
					if longint(tickcount) > 0 then Delay(tickcount);
					{$pop}

					// How many msec since the last frame?
					tickcount := tickmark;
					tickmark := dword(GetMsecTime);
					tickcount := (tickmark - tickcount) and $FFFF;
					// If we are skipping text, run engine at double speed.
					if sysvar.skipText <> ESkipState.No then tickcount := tickcount shl 1;
				end
				else begin
					// Game time does not pass while paused. Use a large fixed frame delay to save CPU while paused.
					tickcount := 0;
					tickmark := dword(GetMsecTime); // have to keep track of current time to avoid jump on unpause
					Delay(100);
				end;
			end
			else begin
				// Frame limiter disabled, no delay between frames. Max CPU used, treat each frame as n msec long.
				tickcount := sakuparam.frameDelay;
			end;

			// User input etc.
			Eventmatic.hasTriggeredInterrupt := FALSE;
			{$ifdef sakucon}
			sysvar.keysDown := [];
			while KeyPressed do HandleConEvent(ReadKey);
			{$else}
			sysvar.swallowRemainingUserInput := FALSE;
			while SDL_PollEvent(@evd) <> 0 do HandleSDLevent(@evd);
			{$endif}

			if sysvar.pauseState <> EPauseState.Paused then begin
				{$ifndef sakucon}
				with sysvar do begin
					if (stickRepeatAfterMsecs <> 0) or ((gamepadLeftStick.x or gamepadLeftStick.y) <> 0) then
						UserInput_GamepadLeftStick(tickcount);
					if (gamepadRightStick.x or gamepadRightStick.y) <> 0 then
						UserInput_GamepadRightStick(tickcount);
					keysPolled := FALSE;
				end;
				{$endif}
				if sysvar.skipText <> ESkipState.No then UserInput_Auto;

				// If we just entered single-stepping mode, override elapsed time with an exact single-frame duration.
				if sysvar.pauseState = EPauseState.Single then tickcount := preference.restTime;

				{$ifndef sakucon}
				// Repeating gamepad direction buttons.
				if sysvar.keyRepeatAfterMsecs <> 0 then HandleKeyRepeat(tickcount);
				{$endif}

				// Process timer events, if any.
				if tickcount <> 0 then Eventmatic.AdvanceTimers(tickcount);

				// Script logic.
				if sysvar.pauseState <> EPauseState.Paused then FiberHub.RunFibers;

				// Deferred save, with user input and fibers entirely done for this frame.
				with Savematic do if saveNow then
					SaveStateToFile(sysvar.activeProjectName + '.' + strdec(saveNowIndex) + '.sav', saveNowDesc);

				if sysvar.quit then break;

				// Update various effects. Instant effects can still act even with tickcount 0.
				with EffectHub do if fxCount <> 0 then Update(tickcount);

				// Update textbox data and prepare to draw them a little later.
				BoxHub.TextBoxer(tickcount);

				// Update display structures, animation frames.
				with EleHub do if numElements <> 0 then UpdateAll(tickcount);
			end
			// While paused, skip most everything, but do quit when needed.
			else if sysvar.quit then break;

			// Render anything that's changed and push out to display.
			Rendermatic.RenderEverything;

			// Deferred error message from whatever went wrong in this frame. Changes metastate.
			// Should this be before RenderEverything?
			with logBuffers do if errorMsg <> '' then begin
				SummonMessageBox(errorMsg);
				errorMsg := '';
			end;

			// If single-stepping, our step is done, so re-pause the game.
			if sysvar.pauseState = EPauseState.Single then SetPauseState(EPauseState.Paused);
		end;

		// Disable autoplay on main loop exit (going to restart or quit).
		sysvar.autoChoice := FALSE;
		sakuparam.frameDelay := 0;
	end;
end;

