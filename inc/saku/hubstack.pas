{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// See doc/uidesign.md, section Dialogs and metastates, for some additional information.
//
// Game state is tracked and controlled by Hub and Matic components, each administering specific engine functionality.
// A hub contains an array of things (eg. array of TTextBox in BoxHub), a matic does not. It's a bit fuzzy.
// There are 3 defined EMetaStates. Some components are shared between metastates, others are state-local.
//
// - Local components are created anew and guaranteed reset to defaults on metalevel entry.
//    * boxhub, choicematic, effecthub, eventmatic, fiberhub
// - Inherited components are a unique copy from the previous level for continuity, but changes to them don't affect
//   the previous level. The copy is scrapped on state pop.
//    * elehub, viewportmatic
// - Global components are singletons, the same instance used in all metastates.
//    * fontmatic, rendermatic, varmon, savematic
//
// The local/inherited components are in a stack structure, the HubStack. Changes in metastate push and pop that stack.

type THubLevel = record
	boxHubLevel : TBoxHub;
	choicematicLevel : TChoicematic;
	effectHubLevel : TEffectHub;
	eleHubLevel : TEleHub;
	eventmaticLevel : TEventMatic;
	fiberHubLevel : TFiberHub;
	viewportmaticLevel : TViewportmatic;
	metaState : EMetaState;
end;

type THubStack = class
private
	procedure SelectHubLevel(index : dword);
	procedure ResetLevel(index : dword);
	procedure CreateLevel(index : dword);

public
	stackIndex : dword; // only hubstack may change this value; any can read
	hubLevel : array of THubLevel;

	procedure Push(inherit : boolean);
	function Pop : boolean;
	procedure ResetToTopmost;
	constructor Create;
	destructor Destroy; override;
end;

var HubStack : THubStack = NIL;

// The rest of the engine mostly only needs these object pointers to the currently active hubs.
var BoxHub : TBoxHub;
	Choicematic : TChoicematic;
	EffectHub : TEffectHub;
	EleHub : TEleHub;
	Eventmatic : TEventmatic;
	FiberHub : TFiberHub;
	Viewportmatic : TViewportmatic;

// ------------------------------------------------------------------

procedure THubStack.SelectHubLevel(index : dword);
begin
	// If level being departed had active text input, disable it.
	if (BoxHub <> NIL) and (BoxHub.activeTextInput <> 0) then
	{$ifdef sakucon}
		CrtShowCursor(FALSE);
	{$else}
		SDL_StopTextInput;
	{$endif}

	with hubLevel[index] do begin
		BoxHub := boxHubLevel;
		Choicematic := choicematicLevel;
		EffectHub := effectHubLevel;
		Eventmatic := eventmaticLevel;
		FiberHub := fiberHubLevel;
		EleHub := eleHubLevel;
		Viewportmatic := viewportmaticLevel;
	end;

	// If level being entered has active text input, enable it. (BoxHub can still be NIL if level creation failed.)
	if (BoxHub <> NIL) and (BoxHub.activeTextInput <> 0) then
	{$ifdef sakucon}
		CrtShowCursor(TRUE);
	{$else}
		SDL_StartTextInput;
	{$endif}

	stackIndex := index;
end;

procedure THubStack.ResetLevel(index : dword);
begin
	if stackIndex <> index then SelectHubLevel(index);
	Choicematic.Init;
	EffectHub.Reset;
	BoxHub.ResetHub; // boxhub depends on choicematic and effecthub
	EleHub.Reset;
	Viewportmatic.Reset; // viewportmatic depends on boxhub
	Eventmatic.Reset;
	FiberHub.Reset;
end;

procedure THubStack.CreateLevel(index : dword);
begin
	if index >= dword(length(hubLevel)) then setlength(hubLevel, index + 1);
	with hubLevel[index] do begin
		choicematicLevel := TChoicematic.Create;
		effectHubLevel := TEffectHub.Create;
		boxHubLevel := TBoxHub.Create;
		eventmaticLevel := TEventMatic.Create;
		fiberHubLevel := TFiberHub.Create;
		eleHubLevel := TEleHub.Create;
		viewportmaticLevel := TViewportmatic.Create;
	end;
	ResetLevel(index);
end;

procedure THubStack.Push(inherit : boolean);
begin
	if sysvar.dropdownConsoleState <> EDDCState.None then BoxHub.DropdownSlideUp;
	hubLevel[stackIndex].metaState := sysvar.metaState;
	// FiberHub must not attempt to run further fibers this frame, since the fibers will be invalid.
	FiberHub.exitRunFibersNow := TRUE;

	if stackIndex < dword(high(hubLevel)) then
		SelectHubLevel(stackIndex + 1)
	else
		CreateLevel(stackIndex + 1); // also resets + selects

	with hubLevel[stackindex - 1].choicematicLevel do if (isActive) and (highlightBox <> NIL) then highlightBox.Clear;
	if inherit then begin
		Viewportmatic.InheritState(hubLevel[stackindex - 1].viewportmaticLevel);
		EleHub.InheritState(hubLevel[stackindex - 1].eleHubLevel);
		BoxHub.InheritState(hubLevel[stackindex - 1].boxHubLevel);
	end;
	// If the push invoker is already running fibers, such as using LogError in sakufibercmds, the new FiberHub must
	// also exit immediately to avoid executing invalid memory. Any use of LogError in sakufibercmds must exit
	// the command invoke immediately afterward.
	FiberHub.exitRunFibersNow := TRUE;
end;

function THubStack.Pop : boolean;
// Resets and pops the current hub level; the previous level becomes active. If already on lowest level, resets all
// levels including current, but doesn't pop it. Returns TRUE if popped level was not lowest, which allows resetting
// the full game state with "while Pop do nothing".
var i : longint;
begin
	result := stackIndex <> 0;
	if result = FALSE then begin
		for i := high(hubLevel) downto 0 do ResetLevel(i);
		exit;
	end;

	FiberHub.exitRunFibersNow := TRUE; // must be done before pushing new so the -previous- fiberhub exits
	ResetLevel(stackIndex);
	if sysvar.dropdownConsoleState <> EDDCState.None then BoxHub.DropdownSlideUp;
	SelectHubLevel(stackIndex - 1);
	sysvar.metaState := hubLevel[stackIndex].metaState;
	Rendermatic.AddRefresh(0, 0, high(longint), high(longint), 0);
	// Re-highlight current choice if choicematic is live.
	with Choicematic do if isActive then HighlightChoice(highlightIndex, EMoveType.Instant);

	{$ifdef sakucon}
	with BoxHub do for i := high(textbox) downto 1 do
		with textbox[i] do if (boxState <> EBoxState.Null) and (isHidden = FALSE) then boxNeedsRedraw := TRUE;
	{$endif}
end;

procedure THubStack.ResetToTopmost;
// Discards all hub levels except the topmost, which becomes the new base level.
var templevel : THubLevel;
	i : byte;
begin
	Assert(stackindex <> 0);
	templevel := hubLevel[stackindex];
	hubLevel[stackindex] := hubLevel[0];
	hubLevel[0] := templevel;
	stackindex := 0;
	for i := high(hubLevel) downto 1 do ResetLevel(i);
	SelectHubLevel(0);
end;

constructor THubStack.Create;
begin
	HubStack := self;
	stackIndex := high(dword);
	CreateLevel(0);
end;

destructor THubStack.Destroy;
var i : byte;
begin
	log('destroying hubstack');
	for i := high(hubLevel) downto 0 do with hubLevel[i] do begin
		SelectHubLevel(i); // if any destructor references another hub, must be sure they refer to the same level
		// Order of destruction is slightly important, some components softly depend on others existing.
		try if FiberHub <> NIL then FiberHub.Destroy; fiberHubLevel := NIL; except on e : Exception do LogError('HubStack.Destroy', e, ExceptAddr); end;
		try if EleHub <> NIL then EleHub.Destroy; eleHubLevel := NIL; except on e : Exception do LogError('HubStack.Destroy', e, ExceptAddr); end;
		try if BoxHub <> NIL then BoxHub.Destroy; boxHubLevel := NIL; except on e : Exception do LogError('HubStack.Destroy', e, ExceptAddr); end;
		try if EffectHub <> NIL then EffectHub.Destroy; effectHubLevel := NIL; except on e : Exception do LogError('HubStack.Destroy', e, ExceptAddr); end;
		try if Choicematic <> NIL then Choicematic.Destroy; choicematicLevel := NIL; except on e : Exception do LogError('HubStack.Destroy', e, ExceptAddr); end;
		try if Viewportmatic <> NIL then Viewportmatic.Destroy; viewportmaticLevel := NIL; except on e : Exception do LogError('HubStack.Destroy', e, ExceptAddr); end;
		try if EventMatic <> NIL then Eventmatic.Destroy; eventmaticLevel := NIL; except on e : Exception do LogError('HubStack.Destroy', e, ExceptAddr); end;
	end;
	BoxHub := NIL; Choicematic := NIL; EffectHub := NIL; EleHub := NIL; Eventmatic := NIL;
	FiberHub := NIL; Viewportmatic := NIL;
	{$if declared(useHeapTrace)}CheckHeap;{$endif}
	inherited;
end;

