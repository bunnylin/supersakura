{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

type TEffect = class abstract
	fxIndex : dword;

	fxFiber : TFiber; // fiber that spawned this effect, or nil
	fxBox : TTextBox; // reference if effect uses a box, else nil
	fxElement : TElement; // reference if effect uses an element, else nil
	fxViewport : dword; // ID if viewport is referenced, else -1

	// Common effect lifetime tracking.
	age, ageLimit : dword;
	infinite, destroyOnFiberLoss : boolean;

	procedure Update; virtual;
	function GetProgress(style : EMoveType) : single;
	function Serialise : UTF8string; virtual;
	constructor Create(const owningfiber : TFiber; msecs : longint);
	destructor Destroy; override;
end;

// The parent class's constructor and destructor take care of bookkeeping. The specialised effect classes are
// responsible for starting and ending the actual effect.

type TEffectBoxMove = class(TEffect)
	fromLoc, toLoc : TCoord32k; // relative to viewport
	fromAnchor, toAnchor : TCoord32k;
	moveStyle : EMoveType;

	procedure Update; override;
	function Serialise : UTF8string; override;
	constructor Create(
		const owningfiber : TFiber; msecs : longint;
		_box : TTextBox; locx, locy, anchorx, anchory : longint; style : EMoveType);
	destructor Destroy; override;
end;

type TEffectBoxScroll = class(TEffect)
	fromOfs, toOfs : longint;
	scrollStyle : EMoveType;

	procedure Update; override;
	function Serialise : UTF8string; override;
	constructor Create(
		const owningfiber : TFiber; msecs : longint; _box : TTextBox; _toofs : longint; style : EMoveType);
	destructor Destroy; override;
end;

type TEffectBoxSize = class(TEffect)
	fromSize, toSize : TSize32k;
	sizeStyle : EMoveType;

	procedure Update; override;
	function Serialise : UTF8string; override;
	constructor Create(
		const owningfiber : TFiber; msecs : longint; _box : TTextBox; sizex, sizey : dword; style : EMoveType);
	destructor Destroy; override;
end;

type TEffectAlphaSlide = class(TEffect)
	fromAlpha, toAlpha : byte;

	procedure Update; override;
	function Serialise : UTF8string; override;
	constructor Create(const owningfiber : TFiber; msecs : longint; _ele : TElement; _toalpha : byte);
	destructor Destroy; override;
end;

type TEffectBash = class(TEffect)
	rotationsPerMsec : single;
	directionUnitVector : TCoordfloat;
	displacement : TCoord32k;

	procedure Update; override;
	function Serialise : UTF8string; override;
	constructor Create(
		const owningfiber : TFiber; msecs : longint; _ele : TElement; direction32k, freq32k, amp32k : longint);
	destructor Destroy; override;
end;

type TEffectFlash = class(TEffect)
	color, count, flashtime : dword;
	style : EMoveType;
	fromAlpha : byte;

	procedure Update; override;
	function Serialise : UTF8string; override;
	constructor Create(
		const owningfiber : TFiber; msecs, _count : longint; _viewport, _color : dword; _style : EMoveType; z : longint);
	destructor Destroy; override;
end;

type TEffectMove = class(TEffect)
	fromLoc, toLoc : TCoord32k;
	fromAnchor, toAnchor : TCoord32k;
	moveStyle : EMoveType;

	procedure Update; override;
	function Serialise : UTF8string; override;
	constructor Create(
		const owningfiber : TFiber; msecs : longint;
		_ele : TElement; locx, locy, anchorx, anchory : longint; style : EMoveType);
	constructor Create(
		const owningfiber : TFiber; msecs : longint; _ele : TElement; locx, locy : longint; style : EMoveType);
	destructor Destroy; override;
end;

type TEffectSleep = class(TEffect)
	function Serialise : UTF8string; override;
	constructor Create(const owningfiber : TFiber; msecs : longint);
	destructor Destroy; override;
end;

type TEffectTransition = class(TEffect)
	data : pointer;
	transitionStyle : ETransitionType;
	width : dword;
	startDelay : byte;

	procedure Update; override;
	function Serialise : UTF8string; override;
	constructor Create(const owningfiber : TFiber; msecs : longint; _viewport : dword; style : ETransitionType);
	destructor Destroy; override;
end;

// The Effect Hub controls all timed effects, changing engine state in response to elapsed milliseconds.
type TEffectHub = class
	fx : array of TEffect;
	fxCount : dword;
	transitionsActive : dword;

	procedure Reset;
	procedure Update(tickcount : dword);
	procedure RemoveOwnedEffects(const ownerfiber : TFiber);
	procedure RemoveOwnedEffects(const ownerbox : TTextBox);
	procedure RemoveTimed;
	function Serialise : TStringBunch;
	destructor Destroy; override;
end;

