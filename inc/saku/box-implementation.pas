{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

procedure TTextBox.Reset;
begin
	boxState := EBoxState.Null;
	content.txtContent := NIL;
	content.escapeList := NIL;
	content.choiceList := NIL;
	style.outline := NIL;
	style.decorList := NIL;
	style.texture.texName := '';
	snapToBox := NIL;
	content.exportToBox := NIL;
	with buffers do begin
		if baseP <> NIL then begin freemem(baseP); baseP := NIL; end;
		if scratchP <> NIL then begin freemem(scratchP); scratchP := NIL; end;
	end;

	if boxName = 'NULL' then begin
		fillbyte(boxFont, sizeof(boxFont), 0);
		fillbyte(contentWin, sizeof(contentWin), 0);
		fillbyte(boxRect, sizeof(boxRect), 0);
		fillbyte(buffers, sizeof(buffers), 0);
		fillbyte(content, sizeof(content), 0);
		fillbyte(style, sizeof(style), 0);

		content.caretPos := -1;
		boxInViewport := 0;
		boxZLevel := 0;
		snapToBoxName := ''; snapToAnchor.x := 0; snapToAnchor.y := 0;
		parametersNeedUpdate := TRUE;

		boxFont.origFontHeight := 1200; // ~15px/400px
		contentWin.linespacing := 9200; // 28% of fontheight
		contentWin.fitMaxCols := high(longint);
		contentWin.fitMaxRows := high(longint);
		contentWin.paddedMaxSize.w := high(longint);
		contentWin.paddedMaxSize.h := high(longint);
		with boxRect.padding do begin
			left := 768; right := 768;
			top := 320; bottom := 320;
		end;
		contentWin.columnCount := 1;
		contentWin.columnPadding := -32768; // 1 ex padding between columns, if any defined
		with style do begin
			textColor.FromRGBA4($FFFF);
			baseColor[0].FromRGBA4($76ED);
			dword(baseColor[1]) := dword(baseColor[0]);
			dword(baseColor[2]) := dword(baseColor[0]);
			dword(baseColor[3]) := dword(baseColor[0]);
			baseFill := EFillMode.Flat;
			popTimeMsec := 384;
			doBevel := 1;
			scrollbar.capColor.FromRGBA4($FFF6);
			scrollbar.troughColor.FromRGBA4($FFF3);
			scrollbar.thumbColor.FromRGBA4($FFF8);
			autoVanish := TRUE;
			alwaysInViewport := TRUE;
		end;
		exit;
	end;

	CloneFrom(BoxHub.textbox[0], FALSE);

	case boxName of
		#1:
		begin
			// Dropdown console or system box.
			boxFont.origFontHeight := 768;
			contentWin.paddedMinSize.w := 32768;
			contentWin.paddedMaxSize.w := 32768;
			contentWin.paddedMinSize.h := 14000;
			contentWin.paddedMaxSize.h := 14000;
			boxZLevel := high(longint);
			with boxRect do begin
				boxLoc.x := 16384; boxLoc.y := 0;
				boxAnchor.x := 16384; boxAnchor.y := 32768;
			end;
			with style do begin
				textColor.FromRGBA4($F);
				baseColor[0].FromRGBA4($CCCC);
				baseColor[1].FromRGBA4($CCCC);
				baseColor[2].FromRGBA4($AAAF);
				baseColor[3].FromRGBA4($999F);
				popType := EPopType.Fade;
				popTimeMsec := 512;
				freeScrollable := TRUE;
				autoVanish := FALSE;
				alwaysInViewport := FALSE;
			end;
		end;

		'MAINBOX':
		begin
			with boxRect do begin
				boxLoc.x := 16384; boxLoc.y := 32000;
				boxAnchor.x := 16384; boxAnchor.y := 32768;
				padding.left := 1024; padding.right := 1024;
			end;
			contentWin.fitMinRows := 4;
			contentWin.fitMaxRows := 4;
			contentWin.fitMinCols := 64;
			contentWin.fitMaxCols := 64;
			contentWin.paddedMaxSize.w := 30000;
			contentWin.columnCount := 4;
			style.autoWaitkey := TRUE;
			with BoxHub do if defaultTextBox = NIL then defaultTextbox := self;
		end;

		'HIGHLIGHT':
		begin
			boxZLevel := 100;
			with boxRect.padding do begin
				left := 310; right := 310;
				top := 90; bottom := 90;
			end;
			style.baseColor[0].FromRGBA4($FFF7);
			style.baseColor[1].FromRGBA4($FFF7);
			style.baseColor[2].FromRGBA4($FFF7);
			style.baseColor[3].FromRGBA4($FFF7);
			with Choicematic do if highlightBox = NIL then highlightBox := self;
		end;

		'TITLEBOX':
		begin
			with boxRect.padding do begin
				left := 600; right := 600;
				top := 210; bottom := 210;
			end;
			boxRect.boxAnchor.y := 32768;
			snapToBoxName := 'MAINBOX';
			snapToAnchor.x := 2000;
			contentWin.fitMinRows := 1; contentWin.fitMaxRows := 1;
			contentWin.fitMinCols := 11;
			with BoxHub do if dialogueTitleBox = NIL then dialogueTitleBox := self;
		end;

		'CHOICEBOX':
		begin
			with Choicematic do if choiceBox = NIL then choiceBox := self;
		end;

		'CHOICEPARTBOX':
		begin
			with Choicematic do if choicePartBox = NIL then choicePartBox := self;
		end;
	end;
end;

procedure TTextBox.AddRefresh; inline;
begin
	with boxRect do Rendermatic.AddRefresh(
		renderLocP.x,
		renderLocP.y,
		renderLocP.x + longint(renderSizeP.w),
		renderLocP.y + longint(renderSizeP.h),
		boxInViewport);
end;

constructor TTextBox.Create(const name : string31);
var i : dword;
begin
	fillbyte(buffers, sizeof(buffers), 0);
	boxName := name;
	Assert(name = upcase(name));
	//log(strcat('create box % level %', [boxName, HubStack.stackIndex]));

	Reset;

	with BoxHub do begin
		i := GetBoxIndex(name);
		if i < dword(high(textbox)) then textbox[i].Destroy;

		if name <> 'NULL' then
			InsertBoxInList(self)
		else begin
			if length(textbox) = 0 then setlength(textbox, 1);
			textbox[0] := self;
		end;
	end;
end;

destructor TTextBox.Destroy;
var i : dword;
begin
	//log(strcat('destroy box % level %', [boxName, HubStack.stackIndex]));
	StopTextInput;
	with buffers do begin
		if baseP <> NIL then begin freemem(baseP); baseP := NIL; end;
		if scratchP <> NIL then begin freemem(scratchP); scratchP := NIL; end;
	end;

	if (boxState <> EBoxState.Null) and (isHidden = FALSE) then begin
		AddRefresh;
		{$ifdef sakucon}
		if sysvar.quit = FALSE then RefreshOverlappingBoxes;
		{$endif}
	end;

	EffectHub.RemoveOwnedEffects(self);

	with BoxHub do begin
		RemoveBoxFromList(self);
		if length(textbox) <> 0 then for i := high(textbox) downto 0 do begin
			if textbox[i].snapToBox = self then textbox[i].snapToBox := NIL;
			if textbox[i].content.exportToBox = self then textbox[i].content.exportToBox := NIL;
		end;
		if defaultTextbox = self then defaultTextBox := NIL;
		if dialogueTitleBox = self then dialogueTitleBox := NIL;
	end;

	with Choicematic do begin
		if choiceBox = self then choiceBox := NIL;
		if choicePartBox = self then choicePartBox := NIL;
		if highlightBox = self then highlightBox := NIL;
	end;

	inherited;
end;

// ------------------------------------------------------------------

function TTextBox.ToString : UTF8string;
begin
	if boxFont.font <> NIL then result := strdec(boxFont.font.fontHeightP) else result := '?';
	result := strcat(
		'%: vp:%  fonth:%/%p  linehp:%  fullhp:%  winsizep:%  scrollp:%  loc:%,%  paddedsizep:%  txtlen:%  state:%',
		[boxName, boxInViewport, boxFont.origFontHeight, result, contentWin.lineHeightP, content.fullHeightP,
		contentWin.contentWinSizeP.ToString, contentwin.scrollOfsP, boxRect.boxLoc.x / 32768, boxRect.boxLoc.y / 32768,
		boxRect.paddedSizeP.ToString, content.txtLength, strenum(typeinfo(boxState), @boxState)]);
end;

function TTextBox.GetUTF8LongestFit(startp, endp : pointer; segmentwidthp, maxwidthp : longint) : dword;
// Given UTF8 bytes [startp..endp - 1], returns the number of bytes that can be printed without exceeding maxwidthp.
// Segmentwidthp can be set to the initial pixelwidth of the full span, or a negative number to autocalculate it.
begin
	if maxwidthp = 0 then begin result := 0; exit; end;
	Assert(startp <= endp);

	result := endp - startp;
	if segmentwidthp < 0 then segmentwidthp := GetUTF8Size(startp, result);
	if segmentwidthp <= maxwidthp then exit;
	// Cut the segment to an approximately fitting length.
	result := longint(result) * maxwidthp div segmentwidthp;

	// Find the nearest prior UTF-8 char initial byte.
	endp := startp + result;
	while (endp > startp) and (byte(endp^) and $C0 = $80) do dec(endp);
	result := endp - startp;

	// Check the approximated segment's size.
	segmentwidthp := GetUTF8Size(startp, result);

	// Scan forward to first non-fitting.
	while segmentwidthp <= maxwidthp do begin
		repeat
			inc(endp);
		until (byte(endp^) and $C0 <> $80);
		segmentwidthp := GetUTF8Size(startp, endp - startp);
	end;

	// Scan backward to first fit.
	repeat
		repeat
			dec(endp);
		until (endp = startp) or (byte(endp^) and $C0 <> $80);
		result := endp - startp;
		segmentwidthp := GetUTF8Size(startp, result);
	until segmentwidthp <= maxwidthp;
end;

procedure TTextBox.Clear(deleteuserinput : boolean = TRUE);
// Clears a textbox's contents, updates all relevant variables.
begin
	with content do begin
		// Shorten the txt buffer if it's quite long and the text being cleared was quite short.
		if (txtLength < 3333) and (length(txtContent) > 9999) then setlength(txtContent, length(txtContent) shr 1);
		txtLength := 0;
		escapeCount := 0;
		choiceCount := 0;
		if length(choiceList) > 16 then choiceList := NIL;
		if deleteuserinput then begin
			if caretPos > 0 then caretPos := 0;
			userInputLen := 0;
		end;
	end;
	contentWin.scrollOfs := 0;
	contentWin.scrollOfsP := 0;
	contentNeedsRender := TRUE;

	// If text input is active, add back the caret position escape code. Otherwise make box ready to vanish.
	with content do if caretPos >= 0 then begin
		escapeList[0].escapeOfs := 0;
		escapeList[0].escapeCode := 1;
		inc(escapeCount);
	end
	else case boxState of
		EBoxState.Null, EBoxState.Empty, EBoxState.Vanishing:;
		EBoxState.Appearing: if style.autoVanish then boxState := EBoxState.Empty;
		else boxState := EBoxState.Empty;
	end;
end;

procedure TTextBox.CloneFrom(srcbox : TTextBox; clonecontent : boolean);
// Copies all box appearance and style from the source box to this one.
var i : dword;
begin
	with srcbox do if boxState <> EBoxState.Null then AddRefresh;
	// Clean up any previous reservations in this box.
	with buffers do begin
		if baseP <> NIL then begin freemem(baseP); baseP := NIL; end;
		if scratchP <> NIL then begin freemem(scratchP); scratchP := NIL; end;
	end;

	fillbyte(buffers, sizeof(buffers), 0);

	boxFont := srcbox.boxFont;
	contentWin := srcbox.contentWin;
	style := srcbox.style;
	style.decorList := NIL;
	setlength(style.decorList, length(srcbox.style.decorList));
	if length(style.decorList) <> 0 then for i := high(style.decorList) downto 0 do
		style.decorList[i] := srcbox.style.decorList[i];
	style.outline := NIL;
	setlength(style.outline, length(srcbox.style.outline));
	if length(style.outline) <> 0 then for i := high(style.outline) downto 0 do
		style.outline[i] := srcbox.style.outline[i];

	boxInViewport := srcbox.boxInViewport;
	boxRect.boxAnchor := srcbox.boxRect.boxAnchor;
	boxRect.boxLoc := srcbox.boxRect.boxLoc;
	boxRect.padding := srcbox.boxRect.padding;
	snapToBoxName := srcbox.snapToBoxName;
	snapToBox := NIL;
	snapToAnchor := srcbox.snapToAnchor;
	boxZLevel := srcbox.boxZLevel;
	isHidden := srcbox.isHidden;
	boxState := srcbox.boxState;

	content.txtContent := NIL;
	content.escapeList := NIL;
	fillbyte(content, sizeof(content), 0);

	if clonecontent then begin
		content := srcbox.content;
		content.exportToBox := NIL;

		content.txtContent := NIL;
		setlength(content.txtContent, length(srcbox.content.txtContent));
		if content.txtLength <> 0 then move(srcbox.content.txtContent[0], content.txtContent[0], content.txtLength);
		content.escapeList := NIL;
		setlength(content.escapeList, length(srcbox.content.escapeList));
		if content.escapeCount <> 0 then
			move(srcbox.content.escapeList[0], content.escapeList[0], sizeof(content.escapeList[0]) * content.escapeCount);
		content.choiceList := NIL;
		setlength(content.choiceList, length(srcbox.content.choiceList));
		if content.choiceCount <> 0 then
			move(srcbox.content.choiceList[0], content.choiceList[0], sizeof(content.choiceList[0]) * content.choiceCount);
	end;
	content.caretPos := -1;

	parametersNeedUpdate := TRUE;
end;

procedure TTextBox.Hide(makehidden : boolean);
begin
	isHidden := makehidden;
	boxNeedsRedraw := NOT makehidden;
	if boxState <> EBoxState.Null then AddRefresh;
end;

procedure TTextBox.MoveCaret(numchars : dword; backward : boolean; ctrl : boolean);
// Moves the user text input caret by numchars UTF-8 characters (not bytes). Movement is capped at userInput bounds.
// If ctrl is true, moves to the next space.
var userInputPtr : ^byte;
	i : dword;
	findspace : boolean;
begin
	with content do begin
		Assert(caretPos >= 0);
		if numchars = 0 then exit;
		userInputPtr := @txtContent[txtLength - userInputLen];
		if numchars >= userInputLen then begin
			if backward then caretPos := 0 else caretPos := userInputLen;
		end
		else if ctrl then begin
			findspace := FALSE;
			while TRUE do begin
				if backward then begin
					if caretPos = 0 then break;
					UTF8NextChar(userInputPtr, dword(caretPos), -1);
				end
				else begin
					if dword(caretPos) >= userInputLen then break;
					UTF8NextChar(userInputPtr, dword(caretPos), 1);
				end;
				if (dword(caretPos) < userInputLen) and (findspace = ((userInputPtr + caretPos)^ = $20)) then begin
					if findspace then begin
						inc(caretPos);
						break;
					end;
					findspace := TRUE;
				end;
			end;
		end
		else begin
			while numchars <> 0 do begin
				dec(numchars);
				if backward then begin
					if caretPos = 0 then break;
					UTF8NextChar(userInputPtr, dword(caretPos), -1);
				end
				else begin
					if dword(caretPos) >= userInputLen then break;
					UTF8NextChar(userInputPtr, dword(caretPos), 1);
				end;
			end;
		end;

		i := txtLength - userInputLen + dword(caretPos);
		while (escapeCount <> 0) and (escapeList[escapeCount - 1].escapeCode <> 1) do dec(escapeCount);
		if escapeCount <> 0 then escapeList[escapeCount - 1].escapeOfs := i;
		while (escapeCount >= 2) and (escapeList[escapeCount - 2].escapeOfs > i) do begin
			dec(escapeCount);
			escapeList[escapeCount - 1].escapeCode := 1;
			escapeList[escapeCount - 1].escapeOfs := i;
		end;
		contentNeedsRender := TRUE;
	end;
end;

procedure TTextBox.StartTextInput(initialtxt : UTF8string; initialpos : longint);
var newlen : dword;
begin
	with content do begin
		if caretPos >= 0 then exit;

		if initialtxt <> '' then begin
			dec(txtLength, userInputLen);
			userInputLen := initialtxt.Length;
			newlen := txtLength + userInputLen;
			if newlen >= dword(length(txtContent)) then setlength(txtContent, newlen + 64);
			move(initialtxt[1], txtContent[txtLength], userInputLen);
			inc(txtLength, userInputLen);
			// Auto-appear box if inserting initial text.
			case boxState of
				EBoxState.Null, EBoxState.Vanishing: boxState := EBoxState.Appearing;
				EBoxState.Empty: boxState := EBoxState.ShowText;
			end;
		end;
		if (initialpos < 0) or (dword(initialpos) > userInputLen) then initialpos := userInputLen;
		caretPos := initialpos;

		// Add an escape code for the caret position.
		if (escapeCount = 0) or (escapeList[escapeCount - 1].escapeCode <> 1) then begin
			if escapeCount >= dword(length(escapeList)) then setlength(escapeList, escapeCount + 4);
			escapeList[escapeCount].escapeOfs := initialpos;
			escapeList[escapeCount].escapeCode := 1;
			inc(escapeCount);
		end;
		contentNeedsRender := TRUE;

		if BoxHub.activeTextInput = 0 then
		{$ifdef sakucon}
			CrtShowCursor(TRUE);
		{$else}
			SDL_StartTextInput;
		{$endif}
		inc(BoxHub.activeTextInput);
	end;
end;

procedure TTextBox.StopTextInput;
begin
	with content do begin
		if caretPos < 0 then exit;
		caretPos := -1;
		while (escapeCount <> 0) and (escapeList[escapeCount - 1].escapeCode <> 1) do dec(escapeCount);
		if escapeCount <> 0 then dec(escapeCount);
		contentNeedsRender := TRUE;

		if BoxHub.activeTextInput <> 0 then begin
			dec(BoxHub.activeTextInput);
			if BoxHub.activeTextInput = 0 then
			{$ifdef sakucon}
				CrtShowCursor(FALSE);
			{$else}
				SDL_StopTextInput;
			{$endif}
		end;
	end;
end;

procedure TTextBox.InsertTextInput(const newtxt : pchar);
// Inserts the given null-terminated multibyte UTF-8 character at the user caret position, moving the caret forward.
// Scrolls the box to bottom.
var i, j, newlen : dword;
begin
	with content do begin
		newlen := length(newtxt);
		if (newlen = 0) or (newlen > 8) then exit;
		Assert(caretPos >= 0);
		i := txtLength + newlen;
		if i >= dword(length(txtContent)) then setlength(txtContent, i + 64);

		j := 0;
		while j < userInputLen - dword(caretPos) do begin
			inc(j);
			txtContent[i - j] := txtContent[txtLength - j];
		end;

		move(newtxt[0], txtContent[txtLength - j], newlen);
		txtLength := i;
		inc(userInputLen, newlen);
		inc(dword(caretPos), newlen);

		while (escapeCount <> 0) and (escapeList[escapeCount - 1].escapeCode <> 1) do dec(escapeCount);
		if escapeCount <> 0 then inc(escapeList[escapeCount - 1].escapeOfs, newlen);
		contentNeedsRender := TRUE;

		ScrollTo(high(longint));
	end;
end;

procedure TTextBox.DeleteTextInput(numchars : dword; backspace : boolean);
// Removes numchars characters after or before the current caret position. Stops early if runs out of characters.
var userInputPtr : ^byte;
	i, right, spanbytes : dword;
begin
	with content do begin
		Assert(caretPos >= 0);
		if (numchars = 0)
		or ((caretPos = 0) and (backspace))
		or ((dword(caretPos) >= userInputLen) and (NOT backspace))
		then exit;

		right := caretPos;
		userInputPtr := @txtContent[txtLength - userInputLen];

		while numchars <> 0 do begin
			dec(numchars);
			if backspace then begin
				if caretPos = 0 then break;
				UTF8NextChar(userInputPtr, dword(caretPos), -1);
			end
			else begin
				if right >= userInputLen then break;
				UTF8NextChar(userInputPtr, right, 1);
			end;
		end;

		MemCopy(userInputPtr + right, userInputPtr + caretPos, userInputLen - right);
		spanbytes := right - caretPos;
		dec(txtLength, spanbytes);
		dec(userInputLen, spanbytes);

		while (escapeCount <> 0) and (escapeList[escapeCount - 1].escapeCode <> 1) do dec(escapeCount);
		if backspace then begin
			if escapeCount <> 0 then dec(escapeList[escapeCount - 1].escapeOfs, spanbytes);
			i := escapeList[escapeCount - 1].escapeOfs;
			while (escapeCount >= 2) and (escapeList[escapeCount - 2].escapeOfs > i) do begin
				dec(escapeCount);
				escapeList[escapeCount - 1].escapeCode := 1;
				escapeList[escapeCount - 1].escapeOfs := i;
			end;
		end;
		contentNeedsRender := TRUE;
	end;
end;

function TTextBox.ScrollTo(
	_ofs : longint; msecs : longint = -1; movetype : EMoveType = EMoveType.Halfcos; capToContent : boolean = TRUE;
	owningfiber : TFiber = NIL) : longint;
// Sets up an instant or gradual scroll effect for the box, from its current scroll position to the given new one. The
// scroll position is 32k per lineheightp, feeding to contentWin.scrollOfs. Use capToContent to prevent scrolling past
// start or end of content. Any ongoing scroll effect is immediately replaced by this, continuing to scroll to the
// target value from whatever value the offset is right now.
// If calling from a script, set the owning fiber so the fiber is able to wait for its effects to complete.
// Returns the capped 32k offset.
var i : longint;
begin
	// If a scroll effect on this box is already live, replace it.
	with EffectHub do begin
		if fxCount <> 0 then for i := fxCount - 1 downto 0 do
			if (fx[i] is TEffectBoxScroll) and (fx[i].fxBox = self) then begin fx[i].Destroy; break; end;
	end;

	if capToContent then with contentWin do begin
		i := round(((content.fullHeightP - contentWinSizeP.h) shl 15) / lineHeightP);
		if _ofs > i then _ofs := i;
		if _ofs < 0 then _ofs := 0; // not else-if, i may be negative so must double-cap
	end;
	result := _ofs;
	if _ofs = contentWin.scrollOfs then exit;

	// Scrolling an active choicebox should hide the highlight box to avoid having to reposition it.
	if (Choicematic.choiceBox = self) and (Choicematic.isActive) and (Choicematic.highlightBox <> NIL) then
		with Choicematic.highlightBox do begin
			popRunTimeMsec := 0;
			boxState := EBoxState.Vanishing;
		end;

	if (msecs = 0) or (movetype = EMoveType.Instant) then begin
		contentWin.scrollOfs := _ofs;
		contentWin.scrollOfsP := SarLongint(contentWin.scrollOfs * longint(contentWin.lineHeightP), 15);
		finalNeedsRender := TRUE;
		exit;
	end;

	if msecs < 0 then msecs := 160;
	TEffectBoxScroll.Create(owningfiber, msecs, self, _ofs, movetype);
end;

procedure TTextBox.Snap;
// Sets the rendering location of this box pixel-perfectly next to another box. Specifically, the anchor pixel
// coordinates of this box are set equal to the snap-to-anchor point relative to the padded target box.
// You shouldn't snap to boxes that themselves snap to something, since the once-removed snap may lag by one frame.
// The null box can't be snapped to.
var targetlocp : TCoordP;
	targetsizep : TSizeP;
begin
	if snapToBox = NIL then begin
		snapToBox := BoxHub.GetBox(snapToBoxName);
		if snapToBox = NIL then begin
			LogError(strcat('bad snap-to: % -> %', [boxName, snapToBoxName]));
			snapToBoxName := '';
			exit;
		end;
	end;

	targetlocp := snapToBox.boxRect.paddedLocP;
	targetsizep := snapToBox.boxRect.paddedSizeP;

	if snapToAnchor.x >= 0 then
		boxRect.paddedLocP.x := targetlocp.x + (longint(targetsizep.w) * snapToAnchor.x + 16384) shr 15
	else
		boxRect.paddedLocP.x := targetlocp.x - (-(longint(targetsizep.w) * snapToAnchor.x + 16384)) shr 15;

	if snapToAnchor.y >= 0 then
		boxRect.paddedLocP.y := targetlocp.y + (longint(targetsizep.h) * snapToAnchor.y + 16384) shr 15
	else
		boxRect.paddedLocP.y := targetlocp.y - (-(longint(targetsizep.h) * snapToAnchor.y + 16384)) shr 15;
end;

function TTextBox.FlowContent : boolean;
// Builds a list of all implicit and explicit linebreaks in the box's text, while calculating the best box content
// dimensions. The box's text is assumed to be valid UTF-8.
// Returns TRUE for ok, FALSE if it was impossible to fit the text in the box.
var escindex : dword = 0;
	dontlinebreak : longint = 0;
	column : record
		widthp : array of dword;
		index, fromrowwidthp : dword;
	end;
	row : record // 1 or more spans, from start of row to known breakable escape
		widthp : dword;
	end;
	span : record // 1 or more snippets, from known breakable offset or row start to current read offset
		fromescindex, fromofs, len, widthp : dword;
	end;
	snippet : record // from most recent escape or row start to current read offset
		fromofs, endofs, len, widthp : dword;
	end;
	i, j : dword;

	{$ifdef sakucon}
	rowalign : longint;

	procedure _SaveRowSize;
	var l : dword;
	begin
		with buffers do begin
			// The scratch buffer must be suitably-sized. It needs to store the left alignment-padding for each row.
			l := (content.fullRowCount + 1) shl 2;
			if (scratchP = NIL) or (l > contentAllocSize) then begin
				// Adjust size in 16 byte chunks, with a bit of headroom.
				contentAllocSize := (l + 20) and $FFFFFFF0;
				reallocmem(scratchP, contentAllocSize);
			end;
			dword((scratchP + content.fullRowCount * 4)^) := word(row.widthp) + (word(rowalign) shl 16);
		end;
	end;
	{$endif}

	procedure _FinaliseAutoColumns;
	var l, rowindex, colindex, cellpad : dword;
	begin
		rowindex := 0;
		row.widthp := 0;
		colindex := 0;
		for l := 0 to content.escapeCount - 1 do with content.escapeList[l] do begin
			if (escapeCode in [10, byte('n')]) then begin
				inc(rowindex);
				colindex := 0;
			end
			else if (escapeCode = byte('t')) and (colindex + 1 < contentWin.columnCount) then begin
				// EscapeData is the pixel width of the tab cell. This needs to be padded to the widest cell width in
				// this column, plus column padding.
				cellpad := column.widthp[colindex] - escapeData + contentWin.columnPaddingP;
				escapeData := cellpad;
				inc(colindex);
			end;
		end;

		cellpad := contentWin.columnPaddingP * (contentWin.columnCount - 1);
		for l in column.widthp do inc(cellpad, l);
		if cellpad > contentWin.contentWinSizeP.w then contentWin.contentWinSizeP.w := cellpad;
		//log(strcat('% autosizecolumns: contentsizep=%', [boxName, contentWin.contentWinSizeP.ToString]));
	end;

	procedure _AddSoftBreak;
	// Places a soft linebreak at the current span's fromescindex.
	var l, m : dword;
	begin
		{$ifdef sakucon} _SaveRowSize; {$endif}
		with content do begin
			if escapeCount >= dword(length(escapeList)) then setlength(escapeList, length(escapeList) + 16);
			l := span.fromescindex;
			if escapeCount = 0 then begin
				inc(escapeCount);
				inc(escindex);
			end
			else begin
				// If there's a tab right before this linebreak, set it to 0 width to right-trim empty space.
				if l <> 0 then with escapeList[l - 1] do
					if (escapeOfs = span.fromofs) and (escapeCode = byte('t')) then
						escapeData := 0;
				// If there's a nop escape, replace it instead of pushing escapes up. (Soft linebreaks turn into nops
				// on cleanup, so repeated soft breaks in the same place (eg. when switching to fullscreen and back
				// repeatedly) can re-use code indexes.
				if (escapeList[l].escapeOfs <> span.fromofs) or (escapeList[l].escapeCode <> 0) then begin
					// No nop, push everything up.
					if l < escapecount then
						for m := escapeCount - 1 downto l do
							escapeList[m + 1] := escapeList[m];
					inc(escapeCount);
					inc(escindex);
				end;
			end;

			with escapeList[l] do begin
				escapeOfs := span.fromofs;
				escapeCode := 10;
				//log(strcat('esc[%] @ % := %', [l, escapeOfs, escapeCode]));
			end;
		end;
	end;

	procedure _FinaliseRow;
	// Resets accumulated row data, tracking the appropriate variables. Any span that should remain on this row must
	// have been merged before calling.
	var l : dword;
	begin
		{$ifdef sakucon} _SaveRowSize; {$endif}
		if contentWin.contentWinSizeP.w < row.widthp then contentWin.contentWinSizeP.w := row.widthp;

		if column.index <> 0 then begin
			l := row.widthp - column.fromrowwidthp;
			if column.widthp[column.index] < l then column.widthp[column.index] := l;
		end;

		row.widthp := 0;
		inc(content.fullRowCount);
		inc(content.fullHeightP, contentWin.lineHeightP);
		column.index := 0;
		column.fromrowwidthp := 0;
	end;

	procedure _AddSpanToRow;
	begin
		inc(row.widthp, span.widthp);
		span.widthp := 0;
		span.fromofs := snippet.fromofs;
		span.len := 0;
		span.fromescindex := escindex + 1;
	end;

	procedure _HandleEscapes;
	// Processes all escapes at snippet.fromofs, sets snippet.endofs to an appropriate offset + 1.
	var l, m : dword;
	begin
		with content do begin
			snippet.endofs := txtLength;

			while (escindex < escapeCount) and (snippet.fromofs = escapeList[escindex].escapeOfs) do begin
				{with escapeList[escindex] do
					log(strcat('Flow esc[%] at % = %', [escindex, escapeOfs, ByteToAscii(escapeCode)]));}
				with escapeList[escindex] do case escapeCode of
					{$ifdef sakucon}
					byte('L'): rowalign := 0;
					byte('C'): rowalign := 16384;
					byte('R'): rowalign := 32768;
					{$else}
					byte('B'): TTF_SetFontStyle(boxFont.font.sdlHandle, TTF_STYLE_BOLD);
					byte('b'): TTF_SetFontStyle(boxFont.font.sdlHandle, 0);

					// Emote.
					byte('&'):
					begin
						// Assume square emotes, so width is same as font pixel height.
						_AddSpanToRow;
						if row.widthp + boxFont.font.fontHeightP >= contentWin.flowMaxWidthP then begin
							dec(span.fromescindex); // insert linebreak before glyph
							_AddSoftBreak;
							_FinaliseRow;
							inc(span.fromescindex);
						end;
						inc(span.widthp, boxFont.font.fontHeightP);
					end;

					// Text entry caret!
					1:
					begin
						m := boxFont.font.fontHeightP shr 4 + 1;
						if row.widthp + span.widthp + m > contentWin.flowMaxWidthP then begin
							_AddSoftBreak;
							_FinaliseRow;
						end;
						inc(span.widthp, m);
					end;
					{$endif}

					// Horizontal divider line! EscapeData is divider pixel height including padding.
					byte('-'):
					begin
						_AddSpanToRow;
						if row.widthp <> 0 then begin
							_AddSoftBreak;
							_FinaliseRow;
						end;
						{$ifdef sakucon}
						escapeData := 1;
						{$else}
						escapeData := boxFont.font.fontHeightP shr 3 + 3;
						{$endif}
						inc(fullHeightP, escapeData);
					end;

					// Choice item!
					byte('?'): inc(dontlinebreak);
					byte('.'): dec(dontlinebreak);

					// Column break!
					byte('t'):
					if style.autoSizeColumns then begin
						_AddSpanToRow;

						if column.index + 1 >= contentWin.columnCount then begin
							_AddSoftBreak;
							_FinaliseRow;
						end
						else begin
							// EscapeData is the preceding column's pixel width. This is later converted to pixel skip.
							// Remember the widest pixel width seen in this column.
							// Not applied to the last column; it can terminate by any linebreak or content end.
							escapeData := row.widthp - column.fromrowwidthp;
							if escapeData > column.widthp[column.index] then column.widthp[column.index] := escapeData;

							column.fromrowwidthp := row.widthp;
							inc(column.index);
						end;
					end
					else begin
						// Fixed width columns: jump ahead to start of next column pixel offset.

						// If we're inside a choice string and encounter a tab, and the tab goes past max width, it'll
						// be necessary to linebreak at the start of the choice (start of span) to avoid splitting the
						// string across rows. If linebreaking is allowed or we're already on a new row, then anything
						// up to end of this span can be merged on this row.
						if (dontlinebreak = 0) or (row.widthp = 0) then _AddSpanToRow;

						m := contentWin.columnFixedWidthP + contentWin.columnPaddingP;
						l := row.widthp + span.widthp + m + contentWin.columnPaddingP - 1;
						dec(l, l mod m);
						{log(strcat(
							'Flowtab: colFixWidthP=%, colPaddP=%, row+spanwidthP=%, flowmaxwidthP=%, width tabs to: %',
							[contentWin.columnFixedWidthP, contentWin.columnPaddingP, row.widthp + span.widthp,
							contentWin.flowMaxWidthP, l]));}

						if l >= contentWin.flowMaxWidthP then begin
							_AddSoftBreak;
							_FinaliseRow;
							// If this tab is within a non-breaking sequence, then try to insert again on the new row.
							if span.len <> 0 then continue;
						end
						else begin
							// Add column break to this row. EscapeData is the number of pixels to skip.
							escapeData := l - row.widthp - span.widthp;
							row.widthp := l;
						end;
					end;

					// Hard line break!
					byte('n'):
					begin
						_AddSpanToRow;
						_FinaliseRow;
					end;

					// Soft line break from before, cancel into a nop!
					10: escapeCode := 0;
				end;
				inc(escindex);
			end;

			if escindex < escapeCount then snippet.endofs := escapeList[escindex].escapeOfs;
		end;
	end;

	function _FindSnippetBreak(maxlen : dword) : dword;
	// Returns the offset of the furthest linebreak within the current snippet. Returns 0 if none found.
	var startp, breakp : ^byte;
		charvalue : dword;
		charwidth : byte;
	begin
		// Scan backward until the first soft linebreaking character. Breaks are allowed after a normal space, and
		// after any CJK symbol. This could be refined, since not all CJK symbols are actually linebreak-friendly.
		startp := @content.txtContent[snippet.fromofs];
		breakp := startp + maxlen;
		while breakp > startp do begin
			charwidth := 0;
			repeat
				dec(breakp); inc(charwidth);
			until breakp^ and $C0 <> $80;

			case charwidth of
				1: charvalue := breakp^;
				2: charvalue := breakp^ shl 8 + (breakp + 1)^;
				3: charvalue := breakp^ shl 16 + (breakp + 1)^ shl 8 + (breakp + 2)^;
				else charvalue := BEtoN(dword(pointer(breakp)^));
			end;
			// 2E80..D7AF   = E2BA80..ED9EAF (mishmash of all basic CJK)
			// FF01..FF60   = EFBC81..EFBDA0 (doublewidth ascii characters)
			// FFE0..FFE6   = EFBFA0..EFBFA6 (extra CJK punctuation)
			// 20000..2CEAF = F0A08080..F0ACBAAF (tons of extended ideographs)
			if (charvalue = $20)
			or ((charvalue >= $E2BA80) and (charvalue <= $ED9EAF))
			or ((charvalue >= $F0A08080) and (charvalue <= $F0ACBAAF))
			then begin
				inc(breakp, charwidth);
				break;
			end;
		end;
		result := breakp - startp;
	end;

begin
	result := TRUE;
	contentWin.contentWinSizeP.w := contentWin.contentMinSizeP.w;
	{$ifdef sakucon}
	rowalign := style.textAlign;
	{$else}
	TTF_SetFontStyle(boxFont.font.sdlHandle, 0);
	{$endif}

	Assert(contentWin.columnCount > 0);

	with contentWin do begin
		column.index := 0;
		column.fromrowwidthp := 0;
		column.widthp := NIL;
		if style.autoSizeColumns then begin
			setlength(column.widthp, columnCount);
			filldword(column.widthp[0], columnCount, 0);
		end;
	end;

	fillbyte(row, sizeof(row), 0);
	fillbyte(span, sizeof(span), 0);

	// Scan the entire box text content and insert implicit linebreaks wherever necessary.
	with contentWin do with content do begin
		fullRowCount := 0;
		fullHeightP := 0;
		snippet.endofs := 0;

		repeat
			snippet.fromofs := snippet.endofs;
			_HandleEscapes;

			// End of content?
			if snippet.fromofs >= txtLength then break;

			// Evaluate snippet fitness at most 200 characters at a time. User-friendly textboxes should have < 100.
			if snippet.endofs > snippet.fromofs + 200 then begin
				snippet.endofs := snippet.fromofs + 200;
				// Find the nearest prior UTF-8 char initial byte, so the evaluation is done on only whole characters.
				UTF8NextChar(@txtContent[0], snippet.endofs, -1);
			end;

			snippet.len := snippet.endofs - snippet.fromofs;
			snippet.widthp := GetUTF8Size(@txtContent[snippet.fromofs], snippet.len);
			{with snippet do log(strcat('Snippet [%] ofs=% len=% widthp=% flowmaxwidthp=% row,span=%,%',
				[copy(PChar(@txtContent[fromofs]), 1, len), fromofs, len, widthp, flowMaxWidthP, row.widthp, span.widthp]));}

			if row.widthp + span.widthp + snippet.widthp <= flowMaxWidthP then begin
				// The whole snippet fits on the current row. Find the furthest linebreaking offset in the snippet.
				i := 0;
				if (dontlinebreak = 0) or (row.widthp = 0) then i := _FindSnippetBreak(snippet.len);
				if i = snippet.len then begin
					// Snippet ends on a break: add whole span and snippet to the row.
					inc(row.widthp, span.widthp + snippet.widthp);
					span.fromofs := snippet.endofs;
					span.len := 0;
					span.widthp := 0;
					span.fromescindex := escindex;
				end
				else if i <> 0 then begin
					// Break found: add whole span and everything to the left of snippet break to the row, and
					// everything to the right of snippet break is the new span.
					// (Must ensure the sum width of both sides still adds up to the right total.)
					j := GetUTF8Size(@txtContent[snippet.fromofs], i);
					inc(row.widthp, span.widthp + j);
					span.fromofs := snippet.fromofs + i;
					span.len := snippet.len - i;
					span.widthp := snippet.widthp - j; //GetUTF8Size(@txtContent[span.fromofs], span.len);
					span.fromescindex := escindex;
				end
				else begin
					// Break not found: the whole snippet is added to the span.
					inc(span.len, snippet.len);
					inc(span.widthp, snippet.widthp);
				end;
				//log(strcat('Snippet added; row widthp=%; span len=% widthp=%', [row.widthp, span.len, span.widthp]));
				continue;
			end;

			// It doesn't fit; a soft linebreak will be required somewhere in or before this snippet.

			// What's the absolute most bytes that would fit on this line?
			j := GetUTF8LongestFit(
				@txtContent[snippet.fromofs], @txtContent[snippet.endofs],
				snippet.widthp,
				flowMaxWidthP - row.widthp - span.widthp);

			// Choices may only get soft linebreaks if they occupy the entire row, to avoid having half of the choice
			// aligned to the right edge and half to the left. Highlighting such a choice would require more than one
			// highlight box, or covering lots of unrelated text with a full-box-width highlight. (That latter still
			// happens if there's further text immediately after a choice that's longer than one line, but that happens
			// less often.)

			// Find the furthest linebreaking offset in the remaining snippet.
			i := 0;
			if (dontlinebreak = 0) or (row.widthp = 0) then i := _FindSnippetBreak(j);

			if i <> 0 then begin
				// Break found: add whole span and everything to the left of snippet break to the row; end the row;
				// everything to the right of snippet break is the next snippet.
				inc(row.widthp, span.widthp);
				inc(row.widthp, GetUTF8Size(@txtContent[snippet.fromofs], i));
				snippet.endofs := snippet.fromofs + i;
				span.fromofs := snippet.endofs;
				span.len := 0;
				span.widthp := 0;
				span.fromescindex := escindex;
			end
			else if row.widthp <> 0 then begin
				// No break in snippet, but there's already stuff on this row: push span and snippet to the next row.
				snippet.endofs := snippet.fromofs;
			end
			else if span.len + j = 0 then begin
				// No break in snippet, and a fresh full row can't accommodate even a single character: bail out.
				log(strcat('[!] box % too small for [%]', [boxName, PChar(@txtContent[0])]));
				{$ifndef sakucon}
				buffers.contentFullP := NIL;
				{$endif}
				contentNeedsRender := FALSE;
				result := FALSE;
				break;
			end
			else begin
				// No break in snippet, and span and snippet don't fully fit on a fresh full row: add as much of the
				// span and snippet to the current row as possible, then end the row.
				row.widthp := flowMaxWidthP;
				snippet.endofs := snippet.fromofs + j;
				span.fromofs := snippet.endofs;
				span.len := 0;
				span.widthp := 0;
				span.fromescindex := escindex;
			end;
			_AddSoftBreak;
			_FinaliseRow;

		until FALSE;
	end;

	// End of content is also end of final row.
	inc(row.widthp, span.widthp);
	_FinaliseRow;

	if (style.autoSizeColumns) and (content.escapeCount <> 0) then _FinaliseAutoColumns;
	{$ifndef sakucon}
	if style.underlineAllRows then inc(content.fullHeightP, content.fullRowCount * (boxFont.font.fontHeightP shr 4 + 1));
	{$endif}

	{$ifdef sakucon}
	// Save the left padding caused by text alignment for the last row.
	for i := 0 to content.fullRowCount - 1 do begin
		rowalign := longint((buffers.scratchP + i * 4)^);
		row.widthp := rowalign and $FFFF;
		rowalign := rowalign shr 16;
		{log(strcat('% row %: widthp=% align=% contentWin.w=%',
			[boxName, i, curwidthp, rowalign, contentWin.contentWinSizeP.w]));}
		dword((buffers.scratchP + i * 4)^) := ((contentWin.contentWinSizeP.w - row.widthp) * rowalign) shr 15;
	end;
	{$endif}

	{$ifdef bonk}
	writeln('box ' + boxName + ' postflow:');
	j := 0; i := 0;
	while TRUE do with content do begin
		while (i < escapeCount) and (escapeList[i].escapeOfs = j) do begin
			write('\' + char(escapeList[i].escapeCode));
			inc(i);
		end;
		if j >= txtLength then break;
		write(char(txtContent[j]));
		inc(j);
	end;
	writeln;
	{$endif}

	// Enforce content window min and max height.
	with contentWin do with boxRect do begin
		contentWinSizeP.h := content.fullHeightP;
		if contentWinSizeP.h > contentMaxSizeP.h then contentWinSizeP.h := contentMaxSizeP.h;
		if contentWinSizeP.h < contentMinSizeP.h then contentWinSizeP.h := contentMinSizeP.h;

		// Update the full box size. The pixel location will be updated by TextBoxer.
		paddedSizeP.w := contentWinSizeP.w + padding.leftp + padding.rightp;
		paddedSizeP.h := contentWinSizeP.h + padding.topp + padding.bottomp;

		DeriveMargins;
		refreshLocP := TRUE;

		{log(strcat('%: locxy=% contentsizep=% paddedsizep=% marginedsizep=%',
			[boxName, boxLoc.ToString, contentWinSizeP.ToString, paddedSizeP.ToString, marginedSizeP.ToString]));}
	end;
end;

procedure TTextBox.PrintFast(srcp : pointer; srclen : dword);
// Adds the given string to the box's text content. Ignores escape codes.
// Srcp should point to the first byte of a UTF8string. Srclen should be the string's byte length.
begin
	Assert(boxName <> 'NULL');
	if srclen = 0 then exit;

	with content do begin
		if txtLength + srclen + 8 >= dword(length(txtContent)) then
			setlength(txtContent, (txtLength + srclen) * 2 + 64);
		move(srcp^, txtContent[txtLength], srclen);
		inc(txtLength, srclen);
	end;
end;

procedure TTextBox.Print(const newtxt : UTF8string);
// Adds the given string to the box's text content. Separates escape codes from displayable text first, and immediately
// dereferences variables. Color changing, emote printing, and variable dereferencing may all use inner dereferences
// too, but the contents of such deref'd variables are not scanned for further references.
// Example: $blue := "00FF"; $mycolor := "blue"; print "\c$$mycolor;;;this text uses my color"
var i, j : dword;
	readp, endp, writep : pointer;
	overrideofs : dword = 0;
	escapecodestring : string[99] = '';
	useoverride : boolean = FALSE;
	empty : boolean = TRUE;
	stash : UTF8string = '';

	procedure _GrabExtraData(code : char);
	var dollarindex : array[0..7] of byte;
		dollarcount : byte = 0;
		writeofs : byte = 0;
	begin
		dollarindex[0] := 0; // silence compiler warning
		while readp < endp do begin
			inc(readp);
			case char(readp^) of
				';':
				begin
					if dollarcount = 0 then break;
					dec(dollarcount);
					byte(escapecodestring[dollarindex[dollarcount]]) := writeofs - dollarindex[dollarcount] - 1;
					with Varmon do begin
						stash := GetStrVar(string((@escapecodestring[dollarindex[dollarcount]])^), style.boxLanguage);
						if writeofs + stash.Length > length(escapecodestring) then begin
							LogError('print: unpacked deref overflow in \' + code + ': ' + escapecodestring);
							break;
						end;
						move(stash[1], escapecodestring[dollarindex[dollarcount]], stash.Length);
						writeofs := dollarindex[dollarcount] + stash.Length;
					end;
				end;

				'$':
				begin
					if dollarcount >= high(dollarindex) then begin
						LogError('print: too many deref in \' + code + ': ' + escapecodestring);
						break;
					end;
					dollarindex[dollarcount] := writeofs;
					inc(dollarcount);
				end;

				else begin
					inc(writeofs);
					if writeofs > 99 then begin
						byte(escapecodestring[0]) := 99;
						LogError('print: too long data for \' + code + ': ' + escapecodestring);
						break;
					end;
					escapecodestring[writeofs] := char(readp^);
				end;
			end;
		end;
		if dollarcount <> 0 then LogError('print: missing ; in \' + code + ': ' + escapecodestring);
		byte(escapecodestring[0]) := writeofs;
	end;

	procedure _AddChoice;
	begin
		with content do begin
			if choiceCount >= dword(length(choiceList)) then
				setlength(choiceList, choiceCount + 4 + choiceCount shr 1);
			fillbyte(choiceList[choiceCount], sizeof(choiceList[choiceCount]), 0);
			inc(choiceCount);
		end;
	end;

	procedure _UTFErr;
	begin
		LogError('Print into ' + boxName + ' has bad UTF8'); // don't print bad UTF8 in error box, that's also an error
		Log(strcat('in: [%]:%', [newtxt, readp - @newtxt[1]]));
	end;

begin
	Assert(boxName <> 'NULL');
	if newtxt = '' then exit;

	with content do begin
		// Expand the txtContent to fit estimated input string size.
		if txtLength + newtxt.Length + 8 >= dword(length(txtContent)) then
			setlength(txtContent, txtLength + newtxt.Length + 64);
		writep := @txtContent[txtLength];
		readp := @newtxt[1] - 1;
		endp := readp + newtxt.Length;

		// If the textbox is empty, emit the default text color code first.
		if (txtLength = 0) and (escapeCount = 0) then begin
			if escapeCount >= dword(length(escapeList)) then setlength(escapeList, length(escapeList) + 16);
			escapeList[0].escapeCode := byte('c');
			escapeList[0].escapeOfs := 0;
			RGBAquad(escapeList[0].escapeData) := style.textColor;
			inc(escapeCount);
		end;

		// Parse newtxt.
		while readp < endp do begin
			inc(readp);
			// Handle normal character.
			if (char(readp^) <> '\') and (char(readp^) >= ' ') then begin
				// Get character's UTF-8 byte count.
				// 1: 0xxxxxxx
				// 2: 110xxxxx 10xxxxxx
				// 3: 1110xxxx 10xxxxxx 10xxxxxx
				// 4: 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
				case byte(readp^) of
					$20..$7F:
					begin
						byte(writep^) := byte(readp^); inc(writep); inc(txtLength);
					end;
					$C0..$DF:
					begin
						if (readp + 1 > endp) or (byte((readp + 1)^) and $C0 <> $80) then
							begin _UTFErr; break; end;
						word(writep^) := word(readp^); inc(writep, 2); inc(readp); inc(txtLength, 2);
					end;
					$E0..$EF:
					begin
						if (readp + 2 > endp) or (word((readp + 1)^) and $C0C0 <> $8080) then
							begin _UTFErr; break; end;
						word(writep^) := word(readp^); inc(writep, 2); inc(readp, 2);
						byte(writep^) := byte(readp^); inc(writep); inc(txtLength, 3);
					end;
					$F0..$F7:
					begin
						if (readp + 3 > endp) or (BEtoN(dword(readp^)) and $C0C0C0 <> $808080) then
							begin _UTFErr; break; end;
						dword(writep^) := dword(readp^); inc(writep, 4); inc(readp, 3); inc(txtLength, 4);
					end;
					else begin
						_UTFErr; break;
					end;
				end;
				continue;
			end;

			// Handle escape code.
			if (readp < endp) and (char(readp^) = '\') then inc(readp);

			// Special handling for control characters, needed for multiline error messages at least.
			j := byte(readp^);
			if j < $20 then case byte(j) of
				$9: j := byte('t');
				$D: j := byte('0');
				$A: j := byte('n');
				else j := byte(' ');
			end;

			if char(j) in ['t','n','&','-'] then empty := FALSE;

			case byte(j) of
				byte('0'): ; // \0 empty char
				byte(':'): // \: dialogue title end
				begin
					Clear;
					with BoxHub do if dialogueTitleBox <> NIL then begin
						if dialogueTitleBox <> defaultTextbox then dialogueTitleBox.Clear;
						dialogueTitleBox.Print(copy(newtxt, 1, readp - @newtxt[2]));
						if dialogueTitleBox = defaultTextbox then begin
							// Treat \: in main text box as a newline. Could use various different title printing styles...
							if escapeCount >= dword(length(escapeList)) then
								setlength(escapeList, length(escapeList) + 16);
							escapeList[escapeCount].escapeOfs := txtLength;
							escapeList[escapeCount].escapeCode := ord('n');
							inc(escapeCount);
						end;
					end;
					writep := @txtContent[txtLength];
				end;

				byte('@'):
				begin
					_GrabExtraData('@');
					useoverride := TRUE;
					overrideofs := valx(escapecodestring);
				end;
				byte('#'):
				begin
					_GrabExtraData('#');
					j := 1;
					i := dword(CutNumberFromString(escapecodestring, j));
					if i < choiceCount then
						choiceList[i].index := CutNumberFromString(escapecodestring, j)
					else
						LogError('\#' + strdec(i) + ' > choiceCount ' + strdec(choiceCount));
				end;

				byte('n'), // \n newline
				byte('t'), // \t column break
				byte('-'), // \- horizontal divider line
				byte('?'), byte('.'), // \? \. choice string start/end
				byte('b'), byte('B'), // \b \B bold font off/on
				byte('d'), // \d restore default text color
				byte('L'), byte('C'), byte('R'), // \L \C \R text align left/center/right
				byte('&'), // print emoji \&emoname:frame;
				byte('c'): // set text color \c48BF;
				begin
					// Expand the escape list if needed, save the escape.
					if escapeCount >= dword(length(escapeList)) then setlength(escapeList, length(escapeList) + 16);
					if NOT useoverride then
						escapeList[escapeCount].escapeOfs := txtLength
					else begin
						escapeList[escapeCount].escapeOfs := overrideofs;
						useoverride := FALSE;
					end;
					escapeList[escapeCount].escapeCode := j;
					case byte(j) of
						byte('d'):
						begin
							escapeList[escapeCount].escapeCode := byte('c');
							escapeList[escapeCount].escapeData := dword(style.textColor);
						end;
						byte('c'):
						begin
							_GrabExtraData('c');
							RGBAquad(escapeList[escapeCount].escapeData).FromRGBA4(dword(valhex(escapecodestring)));
						end;
						byte('&'):
						begin
							_GrabExtraData('&');
							escapeList[escapeCount].escapeData := dword(valx(escapecodestring));
						end;
						byte('?'): _AddChoice;
					end;
					inc(escapeCount);
				end;

				byte('$'): // variable dereference \$varname;
				begin
					// These should already be expanded as soon as the string is read from bytecode.
					// But in case something slips through (eg. incomplete var ref), keep this still in place.
					_GrabExtraData('$');
					stash := Varmon.GetStrVar(escapecodestring, style.boxLanguage);
					Print(stash);
					writep := @txtContent[txtLength];
				end;

				$80..$FF:
				begin
					LogError('Print ' + boxName + ' bad escape code in: ' + newtxt);
					break;
				end;

				else begin
					byte(writep^) := byte(readp^); inc(writep); inc(txtLength);
				end;
			end;
		end;

		{$ifdef bonk}
		writeln('box ' + boxName + ' post-print:');
		j := 0; i := 0;
		while TRUE do begin
			while (i < escapeCount) and (escapeList[i].escapeOfs = j) do begin
				write('\' + char(escapeList[i].escapeCode));
				inc(i);
			end;
			if j >= txtLength then break;
			write(char(txtContent[j]));
			inc(j);
		end;
		writeln;
		{$endif}

		if (empty) and (txtLength = 0) then exit; // box is still empty, don't appear
	end;

	contentNeedsRender := TRUE;
	case boxState of
		EBoxState.Null, EBoxState.Vanishing: boxState := EBoxState.Appearing;
		EBoxState.Empty: boxState := EBoxState.ShowText;
	end;
end;

procedure TTextBox.PrintNested(const txt : TStringBunch);
var i : dword;
begin
	// Print string in this box's preferred language. Unless it's a bad language index, or the localised string is
	// empty, in which case print language 0.
	i := style.boxLanguage;
	if (i >= dword(length(txt))) or (txt[i] = '') then i := 0;
	Print(txt[i]);

	if sysvar.metaState = EMetaState.Normal then LogTranscript(txt[i]);

	// Print in export target boxes, in their preferred languages.
	with content do if exportToBoxName <> '' then begin
		if exportToBox = NIL then begin
			exportToBox := BoxHub.GetBox(exportToBoxName);
			if exportToBox = NIL then begin
				LogError('no such export box: ' + exportToBoxName);
				exportToBoxName := '';
				exit;
			end;
		end;
		exportToBox.PrintNested(txt);
	end;
end;

procedure TTextBox.PrintNested(const txt : UTF8string);
begin
	Print(txt);

	if sysvar.metaState = EMetaState.Normal then LogTranscript(txt);

	// Print in export target boxes, in their preferred languages.
	with content do if exportToBoxName <> '' then begin
		if exportToBox = NIL then begin
			exportToBox := BoxHub.GetBox(exportToBoxName);
			if exportToBox = NIL then begin
				LogError('no such export box: ' + exportToBoxName);
				exportToBoxName := '';
				exit;
			end;
		end;
		exportToBox.PrintNested(txt);
	end;
end;

procedure TTextBox.RemoveDecoration(decornamu : UTF8string);
var i, j : dword;
begin
	if decornamu = '' then
		setlength(style.decorList, 0)
	else with style do begin
		decornamu := upcase(decornamu);
		i := length(decorList);
		while i <> 0 do begin
			dec(i);
			if decorList[i].decorName = decornamu then begin
				j := i + 1;
				while j < dword(length(decorList)) do begin
					decorList[j - 1] := decorList[j];
					inc(j);
				end;
				setlength(decorList, length(decorList) - 1);
			end;
		end;
	end;

	parametersNeedUpdate := TRUE;
end;

procedure TTextBox.SetLocation(x, y, anchorx, anchory : longint);
// Sets a box immediately to a new 32k location, relative to its viewport. The pixel location is updated by TextBoxer.
begin
	with boxRect do begin
		boxLoc.x := x; boxAnchor.x := anchorx;
		boxLoc.y := y; boxAnchor.y := anchory;
	end;
	refreshLocP := TRUE;
	boxNeedsRedraw := TRUE;
end;

procedure TTextBox.SetSize(w, h : dword);
// Sets a box immediately to a new 32k size, relative to its viewport. Implicitly disables content-fitting dynamic
// sizing. If either dimension is high(dword), doesn't set that dimension.
begin
	if w <> high(dword) then with contentWin do begin
		paddedMinSize.w := w;
		paddedMaxSize.w := w;
	end;
	if h <> high(dword) then with contentWin do begin
		paddedMinSize.h := h;
		paddedMaxSize.h := h;
	end;
	if NOT parametersNeedUpdate then begin
		DeriveSizeP;
		contentNeedsRender := TRUE;
	end;
end;

function TTextBox.GetBoxParam(const bpname : UTF8string; out strvalue : UTF8string) : longint;
// Returns the named parameter value of this box. Bpname must be in lowercase.
// If strvalue is NIL, use result longint, else use strvalue.
begin
	result := 0;
	strvalue := '';
	if bpname = '' then begin
		LogError(strcat('GetBoxParam % no param name', [boxName]));
		exit;
	end;

	case bpname of
		'viewport': result := boxInViewport;
		'fontheight': result := boxFont.origFontHeight;
		'linespacing': result := contentWin.linespacing;
		'textalign': result := style.textAlign;

		'mincols': result := contentWin.fitMinCols;
		'minrows': result := contentWin.fitMinRows;
		'maxcols': result := contentWin.fitMaxCols;
		'maxrows': result := contentWin.fitMaxRows;
		'minsizex': result := contentWin.paddedMinSize.w;
		'minsizey': result := contentWin.paddedMinSize.h;
		'maxsizex': result := contentWin.paddedMaxSize.w;
		'maxsizey': result := contentWin.paddedMaxSize.h;
		'zlevel': result := boxZLevel;

		'padleft', 'paddingleft': result := boxRect.padding.left;
		'padright', 'paddingright': result := boxRect.padding.right;
		'padtop', 'paddingtop': result := boxRect.padding.top;
		'padbottom', 'paddingbottom': result := boxRect.padding.bottom;
		// Can't get paddingv, paddingh, or plain padding.

		'columns': result := contentWin.columnCount;
		'columnpadding': result := contentWin.columnPadding;

		'snaptobox': strvalue := snapToBoxName;
		'exporttobox': strvalue := content.exportToBoxName;
		'textcolor': result := style.textColor.ToRGBA4;
		'basecolor': result := style.baseColor[0].ToRGBA4;
		'basecolor0', 'basecolor1', 'basecolor2', 'basecolor3':
			result := style.baseColor[ord(bpname[10]) - 48].ToRGBA4;
		'basefill': result := longint(style.baseFill);
		'blendmode': result := longint(style.boxBlendMode);
		'poptype': result := byte(style.popType);
		'poptime': result := style.popTimeMsec;
		'scrollcapcolor': result := style.scrollbar.capColor.ToRGBA4;
		'scrolltroughcolor': result := style.scrollbar.troughColor.ToRGBA4;
		'scrollthumbcolor': result := style.scrollbar.thumbColor.ToRGBA4;
		'texleftedge': result := style.texture.texTileMarginsP_original.left;
		'texrightedge': result := style.texture.texTileMarginsP_original.right;
		'textopedge': result := style.texture.texTileMarginsP_original.top;
		'texbottomedge': result := style.texture.texTileMarginsP_original.bottom;
		'bevel': result := style.doBevel;

		'alwaysinviewport': if style.alwaysInViewport then result := 1 else result := 0;
		'autosizecolumns': if style.autoSizeColumns then result := 1 else result := 0;
		'freescrollable': if style.freeScrollable then result := 1 else result := 0;
		'autowaitkey': if style.autoWaitkey then result := 1 else result := 0;
		'autovanish': if style.autoVanish then result := 1 else result := 0;
		'underlineallrows': if style.underlineAllRows then result := 1 else result := 0;

		'language': strvalue := languageList[style.boxLanguage];

		else LogError(strcat('GetBoxParam % unknown param: %', [boxName, bpname]));
	end;
end;

procedure TTextBox.SetBoxParam(const bpname : UTF8string; const bpstr : UTF8string; bpval : longint; usedefault : boolean);
// Use this to cleanly change textbox characteristics. Bpname is the case-insensitive parameter name, bpstr and bpval
// are the values to set it to depending on parameter type. If usedefault is TRUE, a default value is used instead of
// the given value.
var l : longint;

	procedure _Error(const errtxt : UTF8string);
	begin
		if bpstr <> '' then
			LogError(strcat('SetBoxParam % [%=%]: %', [boxName, bpname, bpstr, errtxt]))
		else
			LogError(strcat('SetBoxParam % [%=%]: %', [boxName, bpname, bpval, errtxt]));
	end;

	function _MinMaxDefault(min, max : longint) : longint;
	var unused : UTF8string;
	begin
		if usedefault then
			result := BoxHub.textbox[0].GetBoxParam(bpname, unused)
		else begin
			if bpstr <> '' then case lowercase(bpstr) of
				'yes', 'true', 'on', 'enabled': bpval := 1;
				'no', 'false', 'off', 'disabled': bpval := 0;
				else begin
					if (bpstr.Length > 2) and (bpstr[1] = '0') and (bpstr[2] in ['x','X']) then
						bpval := longint(valhex(bpstr))
					else
						bpval := longint(valx(bpstr));
				end;
			end;
			if (bpval >= min) and (bpval <= max) then
				result := bpval
			else begin
				_Error('must be in range ' + strdec(min) + '..' + strdec(max));
				result := min;
			end;
		end;
	end;

	function _StrDefault(digitok : boolean) : UTF8string;
	begin
		if usedefault then
			BoxHub.textbox[0].GetBoxParam(bpname, result)
		else if (bpstr = '') and (digitok) then
			result := strdec(bpval)
		else
			result := bpstr;
	end;

begin
	if bpname = '' then begin
		_Error('no param name');
		exit;
	end;

	parametersNeedUpdate := TRUE;

	case lowercase(bpname) of
		'viewport': boxInViewport := _MinMaxDefault(0, high(Viewportmatic.viewport));
		'fontheight': boxFont.origFontHeight := _MinMaxDefault(1, 99999);
		'linespacing': contentWin.linespacing := _MinMaxDefault(0, 99999);
		'textalign': style.textAlign := _MinMaxDefault(0, 32768);

		'mincols': contentWin.fitMinCols := _MinMaxDefault(0, 99999);
		'minrows': contentWin.fitMinRows := _MinMaxDefault(0, 99999);
		'maxcols': contentWin.fitMaxCols := _MinMaxDefault(0, high(longint));
		'maxrows': contentWin.fitMaxRows := _MinMaxDefault(0, high(longint));
		'minsizex': contentWin.paddedMinSize.w := _MinMaxDefault(0, 99999);
		'minsizey': contentWin.paddedMinSize.h := _MinMaxDefault(0, 99999);
		'minsizexy': with contentWin.paddedMinSize do begin l := _MinMaxDefault(0, 99999); w := l; h := l; end;
		'maxsizex': contentWin.paddedMaxSize.w := _MinMaxDefault(0, high(longint));
		'maxsizey': contentWin.paddedMaxSize.h := _MinMaxDefault(0, high(longint));
		'maxsizexy': with contentWin.paddedMaxSize do begin l := _MinMaxDefault(0, high(longint)); w := l; h := l; end;
		'zlevel': begin
			l := _MinMaxDefault(low(longint), high(longint));
			if l <> boxZLevel then begin
				BoxHub.RemoveBoxFromList(self);
				boxZLevel := l;
				BoxHub.InsertBoxInList(self);
			end;
		end;

		'padleft', 'paddingleft': boxRect.padding.left := _MinMaxDefault(0, 99999);
		'padright', 'paddingright': boxRect.padding.right := _MinMaxDefault(0, 99999);
		'padtop', 'paddingtop': boxRect.padding.top := _MinMaxDefault(0, 99999);
		'padbottom', 'paddingbottom': boxRect.padding.bottom := _MinMaxDefault(0, 99999);
		'padh', 'paddingh':
		begin l := _MinMaxDefault(0, 99999); boxRect.padding.left := l; boxRect.padding.right := l; end;
		'padv', 'paddingv':
		begin l := _MinMaxDefault(0, 99999); boxRect.padding.top := l; boxRect.padding.bottom := l; end;
		'padding':
		with boxRect do begin
			l := _MinMaxDefault(0, 99999);
			padding.top := l; padding.bottom := l; padding.left := l; padding.right := l;
		end;

		'columns': contentWin.columnCount := _MinMaxDefault(1, 99999);
		'columnpadding': contentWin.columnPadding := _MinMaxDefault(low(longint), high(longint));

		'snaptobox': begin snapToBoxName := _StrDefault(FALSE); snapToBox := NIL; end;
		'snaptoanchorx': snapToAnchor.x := _MinMaxDefault(low(longint), high(longint));
		'snaptoanchory': snapToAnchor.y := _MinMaxDefault(low(longint), high(longint));
		'exporttobox': begin content.exportToBoxName := _StrDefault(FALSE); content.exportToBox := NIL; end;
		'textcolor': style.textColor.FromRGBA4(_MinMaxDefault(0, $FFFF));
		'basecolor': begin
			style.baseFill := EFillMode.Flat;
			style.baseColor[0].FromRGBA4(_MinMaxDefault(0, $FFFF));
			dword(style.baseColor[1]) := dword(style.baseColor[0]);
			dword(style.baseColor[2]) := dword(style.baseColor[0]);
			dword(style.baseColor[3]) := dword(style.baseColor[0]);
		end;
		'basecolor0', 'basecolor1', 'basecolor2', 'basecolor3': begin
			style.baseFill := EFillMode.XGradient;
			style.baseColor[ord(bpname[10]) - 48].FromRGBA4(_MinMaxDefault(0, $FFFF));
		end;
		'basefill': style.baseFill := EFillModeFromString(_StrDefault(TRUE));
		'blendmode': style.boxBlendMode := EBlendModeFromString(_StrDefault(TRUE));
		'poptype': style.popType := EPopTypeFromString(_StrDefault(TRUE));
		'poptime': style.popTimeMsec := _MinMaxDefault(0, high(longint));
		'scrollcapcolor': style.scrollbar.capColor.FromRGBA4(_MinMaxDefault(0, $FFFF));
		'scrolltroughcolor': style.scrollbar.troughColor.FromRGBA4(_MinMaxDefault(0, $FFFF));
		'scrollthumbcolor': style.scrollbar.thumbColor.FromRGBA4(_MinMaxDefault(0, $FFFF));
		'bevel': style.doBevel := _MinMaxDefault(0, 1);

		'texleftedge': style.texture.texTileMarginsP_original.left := _MinMaxDefault(0, 99999);
		'texrightedge': style.texture.texTileMarginsP_original.right := _MinMaxDefault(0, 99999);
		'textopedge': style.texture.texTileMarginsP_original.top := _MinMaxDefault(0, 99999);
		'texbottomedge': style.texture.texTileMarginsP_original.bottom := _MinMaxDefault(0, 99999);
		'texedgeh': with style.texture.texTileMarginsP_original do begin
			l := _MinMaxDefault(0, 99999);
			left := l; right := l;
		end;
		'texedgev': with style.texture.texTileMarginsP_original do begin
			l := _MinMaxDefault(0, 99999);
			top := l; bottom := l;
		end;
		'texedge': with style.texture.texTileMarginsP_original do begin
			l := _MinMaxDefault(0, 99999);
			left := l; right := l; top := l; bottom := l;
		end;

		'alwaysinviewport': style.alwaysInViewport := _MinMaxDefault(low(longint), high(longint)) <> 0;
		'autosizecolumns': style.autoSizeColumns := _MinMaxDefault(low(longint), high(longint)) <> 0;
		'freescrollable': style.freeScrollable := _MinMaxDefault(low(longint), high(longint)) <> 0;
		'autowaitkey': style.autoWaitkey := _MinMaxDefault(low(longint), high(longint)) <> 0;
		'autovanish': style.autoVanish := _MinMaxDefault(low(longint), high(longint)) <> 0;
		'negatebkg': style.boxBlendMode := EBlendMode.Difference; // hack, deprecated
		'underlineallrows': style.underlineAllRows := _MinMaxDefault(low(longint), high(longint)) <> 0;

		'language': if usedefault then style.boxLanguage := BoxHub.textbox[0].style.boxLanguage
		else begin
			if bpstr <> '' then dword(bpval) := FindLanguageIndex(bpstr);
			if dword(bpval) >= languageList.Length then
				_Error('language not found')
			else
				style.boxLanguage := dword(bpval);
		end;

		else _Error('unknown param');
	end;
end;

procedure TTextBox.DeriveLocP;
// Calculates paddedLocP with snap/anchor/alwaysinviewport, and marginedLocP. PaddedSizeP must be already calculated.
begin
	with boxRect do begin
		if snapToBoxName <> '' then
			Snap
		else with Viewportmatic.viewport[boxInViewport] do begin
			if boxLoc.x >= 0 then
				paddedLocP.x := (boxLoc.x * longint(viewportSizeP.w) + 16384) shr 15 + viewportLoc.leftp
			else
				paddedLocP.x := -((-boxLoc.x * longint(viewportSizeP.w) + 16384) shr 15) + viewportLoc.leftp;
			if boxLoc.y >= 0 then
				paddedLocP.y := (boxLoc.y * longint(viewportSizeP.h) + 16384) shr 15 + viewportLoc.topp
			else
				paddedLocP.y := -((-boxLoc.y * longint(viewportSizeP.h) + 16384) shr 15) + viewportLoc.topp;
		end;

		// Apply own anchor.
		dec(paddedLocP.x, (longint(paddedSizeP.w) * boxAnchor.x + 16384) shr 15);
		dec(paddedLocP.y, (longint(paddedSizeP.h) * boxAnchor.y + 16384) shr 15);

		if style.alwaysInViewport then with Viewportmatic.viewport[boxInViewport] do begin
			// If the box is outside its viewport, the box is moved to be inside. Applies against the padded box.
			// If the box is too big to fit, it is not resized.
			if paddedLocP.x + longint(paddedSizeP.w) >= viewportLoc.rightp then
				paddedLocP.x := viewportLoc.rightp - longint(paddedSizeP.w);
			if paddedLocP.x < viewportLoc.leftp then paddedLocP.x := viewportLoc.leftp;

			if paddedLocP.y + longint(paddedSizeP.h) >= viewportLoc.bottomp then
				paddedLocP.y := viewportLoc.bottomp - longint(paddedSizeP.h);
			if paddedLocP.y < viewportLoc.topp then paddedLocP.y := viewportLoc.topp;
		end;

		marginedLocP.x := paddedLocP.x - marginP.left;
		marginedLocP.y := paddedLocP.y - marginP.top;
	end;
	//refreshLocP := FALSE; // caller's responsibility, see TextBoxer
end;

procedure TTextBox.DeriveSizeP;
var i, j : dword;
begin
	with contentWin do with boxRect do begin
		// Calculate the content window bounds.
		contentMinSizeP.w := 0;
		contentMinSizeP.h := 0;
		contentMaxSizeP.w := $FFFFFFFF;
		contentMaxSizeP.h := $FFFFFFFF;

		if fitMinRows < $FFFF then contentMinSizeP.h := fitMinRows * lineHeightP;
		if fitMaxRows < $FFFF then contentMaxSizeP.h := fitMaxRows * lineHeightP;
		i := style.outlineMarginP.left + style.outlineMarginP.right;
		if boxFont.font = NIL then raise Exception.Create('fuketyfuk');
		Assert(boxFont.font <> NIL);
		if fitMinCols < $FFFF then contentMinSizeP.w := fitMinCols * boxFont.font.exWidthP + i;
		if fitMaxCols < $FFFF then contentMaxSizeP.w := fitMaxCols * boxFont.font.exWidthP + i;

		j := padding.leftp + padding.rightp;
		if paddedMinSize.w < $FFFF then begin
			i := (paddedMinSize.w * Viewportmatic.viewport[boxInViewport].viewportSizeP.w + 16384) shr 15;
			if i < j then i := j;
			dec(i, j);
			if (paddedMinSize.w <> 0) and (i = 0) then i := 1; // guarantee non-zero if min size 32k is non-zero
			if i > contentMinSizeP.w then contentMinSizeP.w := i;
		end;
		if paddedMaxSize.w < $FFFF then begin
			i := (paddedMaxSize.w * Viewportmatic.viewport[boxInViewport].viewportSizeP.w + 16384) shr 15;
			if i < j then i := j;
			dec(i, j);
			if i < contentMaxSizeP.w then contentMaxSizeP.w := i;
		end;

		j := padding.topp + padding.bottomp;
		if paddedMinSize.h < $FFFF then begin
			i := (paddedMinSize.h * Viewportmatic.viewport[boxInViewport].viewportSizeP.h + 16384) shr 15;
			if i < j then i := j;
			dec(i, j);
			if (paddedMinSize.h <> 0) and (i = 0) then i := 1; // guarantee non-zero if min size 32k is non-zero
			if i > contentMinSizeP.h then contentMinSizeP.h := i;
		end;
		if paddedMaxSize.h < $FFFF then begin
			i := (paddedMaxSize.h * Viewportmatic.viewport[boxInViewport].viewportSizeP.h + 16384) shr 15;
			if i < j then i := j;
			dec(i, j);
			if i < contentMaxSizeP.h then contentMaxSizeP.h := i;
		end;

		if contentMinSizeP.w > contentMaxSizeP.w then contentMinSizeP.w := contentMaxSizeP.w;
		if contentMinSizeP.h > contentMaxSizeP.h then contentMinSizeP.h := contentMaxSizeP.h;

		flowMaxWidthP := $FFFF;
		if (NOT style.autoSizeColumns) or (columnCount = 1) then begin
			flowMaxWidthP := contentMaxSizeP.w - style.outlineMarginP.left - style.outlineMarginP.right;
			if flowMaxWidthP > $FFFF then flowMaxWidthP := $FFFF;
		end;

		if (columnPadding >= 0) and (NOT style.autoSizeColumns) then
			columnPaddingP := (flowMaxWidthP * dword(columnPadding) + 16384) shr 15
		else
			columnPaddingP := (boxFont.font.exWidthP * dword(abs(columnPadding)) + 16384) shr 15;

		columnFixedWidthP := 0;
		if NOT style.autoSizeColumns then
			columnFixedWidthP := (flowMaxWidthP - columnPaddingP * dword(columnCount - 1)) div columnCount;
	end;
end;

procedure TTextBox.DeriveMargins;
// Calculates decor position and margins after a box's paddedSizeP is known.
var i, h : dword;
	j : longint;
begin
	with style do with boxRect do begin
		with marginP do begin
			left := 0; right := 0;
			top := 0; bottom := 0;
		end;
		// Make sure the rescaled texture fits, squash further if necessary.
		with texture do if (texType <> ETextureType.None) and (texName <> '') then begin
		end;

		// Calculate box decoration positioning.
		if length(decorList) <> 0 then for i := high(decorList) downto 0 do with decorList[i] do begin
			decorLocP.x := SarLongint((decorLoc.x * longint(boxRect.paddedSizeP.w) - longint(decorSize.wp) * decorAnchor.x), 15);
			decorLocP.y := SarLongint((decorLoc.y * longint(boxRect.paddedSizeP.h) - longint(decorSize.hp) * decorAnchor.y), 15);

			// Calculate box margins to accommodate any decor going outside the box.
			if -decorLocP.x > boxRect.marginP.left then boxRect.marginP.left := -decorLocP.x;
			j := decorLocP.x + longint(decorSize.wp) - boxRect.paddedSizeP.w;
			if j > boxRect.marginP.right then boxRect.marginP.right := j;
			if -decorLocP.y > boxRect.marginP.top then boxRect.marginP.top := -decorLocP.y;
			j := decorLocP.y + longint(decorSize.hp) - boxRect.paddedSizeP.h;
			if j > boxRect.marginP.bottom then boxRect.marginP.bottom := j;
		end;

		i := paddedSizeP.w + dword(marginP.left + marginP.right);
		h := paddedSizeP.h + dword(marginP.top + marginP.bottom);
		if (i <> marginedSizeP.w) or (h <> marginedSizeP.h) then begin
			baseNeedsRender := TRUE;
			marginedSizeP.w := i;
			marginedSizeP.h := h;
		end;

		// Adjust decor pixel positions to be relative to margined box.
		if length(decorList) <> 0 then for i := high(decorList) downto 0 do with decorList[i] do begin
			inc(decorLocP.x, marginP.left);
			inc(decorLocP.y, marginP.top);
		end;
	end;
end;

procedure TTextBox.UpdateParameters;
// Converts all box-related 32k values to pixel values as needed. Must be called before rendering content or base.
var i, j, userfontheight : dword;
	gfxfile : TGraphicFile;
	gfxob : TGraphicObject;
begin
	with style do begin
		// Check the texture image, if any.
		with texture do if (texType <> ETextureType.None) and (texName <> '') then begin
			gfxob := GetGraphicObject(texName, 0, 0);
			if gfxob = NIL then begin
				LogError(strcat('bad tex in box %: %', [boxName, texName]));
				texName := '';
				texType := ETextureType.None;
			end
			else with Viewportmatic.viewport[boxInViewport] do begin
				gfxfile := gfxob.sourceFile;
				texFrameIndex := texFrameIndex mod gfxfile.frameCount;
				if (texTileMarginsP_original.left + texTileMarginsP_original.right > gfxob.sizeXP)
				or (texTileMarginsP_original.top + texTileMarginsP_original.bottom > gfxob.frameHeightP)
				then begin
					LogError(strcat('% texMargins % exceeds texSize %x%',
						[texName, texTileMarginsP_original.ToString, gfxob.sizeXP, gfxob.frameHeightP]));
					texName := '';
				end
				else begin
					// Calculate the PNG margins fitted to the box's viewport, and the texture's size in the viewport.
					j := gfxfile.origResXP shr 1;
					texTileMarginsP_ideal.left :=
						(texTileMarginsP_original.left * viewportSizeP.w + j) div gfxfile.origResXP;
					texTileMarginsP_ideal.right :=
						(texTileMarginsP_original.right * viewportSizeP.w + j) div gfxfile.origResXP;
					texSizeP.w := (gfxob.sizeXP * viewportSizeP.w + j) div gfxfile.origResXP;

					j := gfxfile.origResYP shr 1;
					texTileMarginsP_ideal.top :=
						(texTileMarginsP_original.top * viewportSizeP.h + j) div gfxfile.origResYP;
					texTileMarginsP_ideal.bottom :=
						(texTileMarginsP_original.bottom * viewportSizeP.h + j) div gfxfile.origResYP;
					texSizeP.h := (gfxob.frameHeightP * viewportSizeP.h + j) div gfxfile.origResYP;
				end;
			end;
		end;

		// Update font outline pixel sizes, if any.
		outlineMarginP.top := 0; outlineMarginP.bottom := 0;
		outlineMarginP.left := 0; outlineMarginP.right := 0;
		{$ifndef sakucon}
		if length(outline) <> 0 then for i := high(outline) downto 0 do begin
			outline[i].thicknessP :=
				(outline[i].thickness * Viewportmatic.viewport[boxInViewport].viewportSizeP.h + 16384) shr 15;
			outline[i].offset.DerivePixelsFrom32k(Viewportmatic.viewport[boxInViewport].viewportSizeP);

			longint(j) := longint(outline[i].thicknessP) - outline[i].offset.xp;
			if longint(j) > longint(outlineMarginP.left) then outlineMarginP.left := j;
			longint(j) := longint(outline[i].thicknessP) + outline[i].offset.xp;
			if longint(j) > longint(outlineMarginP.right) then outlineMarginP.right := j;
			longint(j) := longint(outline[i].thicknessP) - outline[i].offset.yp;
			if longint(j) > longint(outlineMarginP.top) then outlineMarginP.top := j;
			longint(j) := longint(outline[i].thicknessP) + outline[i].offset.yp;
			if longint(j) > longint(outlineMarginP.bottom) then outlineMarginP.bottom := j;
		end;
		{$endif sakucon}

		// Calculate box decoration sizes. Location can only be calculated after paddedSizeP is known.
		if length(decorList) <> 0 then for i := high(decorList) downto 0 do with decorList[i] do begin
			j := GetGraphicFile(decorName); {$note todo: allow loadtime as decor}
			if j = 0 then begin
				LogError('BuildBoxBase: decor graphic not found: ' + decorName);
				setlength(style.decorList, 0);
				break;
			end;
			gfxfile := graphicFiles[j];
			if decorSize.w = 0 then
				decorSize.wp := gfxfile.origSizeXP * Viewportmatic.viewport[boxInViewport].viewportSizeP.w
				div gfxfile.origResXP
			else
				decorSize.wp := (decorSize.w * Viewportmatic.viewport[boxInViewport].viewportSizeP.w + 16384) shr 15;
			if decorSize.h = 0 then
				decorSize.hp := gfxfile.origFrameHeightP * Viewportmatic.viewport[boxInViewport].viewportSizeP.h
				div gfxfile.origResYP
			else
				decorSize.hp := (decorSize.h * Viewportmatic.viewport[boxInViewport].viewportSizeP.h + 16384) shr 15;
		end;
	end;

	userfontheight := (boxFont.origFontHeight * preference.uiMagnification + 16384) shr 15;
	i := (userfontheight * Viewportmatic.viewport[boxInViewport].viewportSizeP.h + 16384) shr 15;
	boxFont.font := Fontmatic.GetFont('', languageList[style.boxLanguage], i);

	with contentWin do with boxRect do begin
		// Calculate the line height.
		i := style.outlineMarginP.top + style.outlineMarginP.bottom;
		j := (boxFont.font.requestedHeightP * linespacing + 16384) shr 15;
		if boxFont.font.extraLinespaceP >= j then j := 0 else dec(j, boxfont.font.extraLinespaceP);
		if i > j then
			lineHeightP := boxFont.font.fontHeightP + i
		else
			lineHeightP := boxfont.font.fontHeightP + j;
		{log(strcat('%: outlinemarg top=% bottom=%, fonthp_req=%, linesp=% xlinesp=%, i=% j=%, linehp=%, scroll=%',
			[boxName, style.outlineMarginP.top, style.outlineMarginP.bottom, boxFont.font.fontHeightP_requested,
			linespacing, boxFont.font.extraLinespaceP, i, j, lineHeightP, scrollOfs]));}

		// Refresh scroll pixel offset, unless position is maxint (scroll to tail), which is handled by TextBoxer.
		if scrollOfs < high(scrollOfs) then
			scrollOfsP := SarLongint(scrollOfs * longint(lineHeightP), 15);

		// Calculate the padding. (Guarantee 1 pixel of horizontal padding if 32k value is not zero.)
		padding.DerivePixelsFrom32k(Viewportmatic.viewport[boxInViewport].viewportSizeP);
		if (padding.left <> 0) and (padding.leftp = 0) then padding.leftp := 1;
		if (padding.right <> 0) and (padding.rightp = 0) then padding.rightp := 1;

		{log(strcat('%: paddingP=%,%,%,% marginP=%',
			[boxName, padding.leftp, padding.rightp, padding.topp, padding.bottomp, marginP.ToString]));}

		DeriveSizeP;
	end;

	parametersNeedUpdate := FALSE;
	contentNeedsRender := TRUE;
	baseNeedsRender := TRUE;
end;

