{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// SuperSakura text box functions.

function TTextBox.GetUTF8Size(poku : pointer; slen : dword) : dword;
// Returns the size in character cells of the given UTF-8 string. Tries to account for double-width CJK.
// Poku must point to a valid UTF-8 byte sequence, and slen is the byte length of the sequence.
var endp : pointer;
	utfcode : dword;
begin
	result := 0;
	endp := poku + slen;
	while poku < endp do begin
		inc(result); // each character takes 1 cell by default
		// 1: 0xxxxxxx
		// 2: 110xxxxx 10xxxxxx
		// 3: 1110xxxx 10xxxxxx 10xxxxxx
		// 4: 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
		case byte(poku^) of
			$00..$7F:
			begin
				inc(poku); continue;
			end;

			$C0..$DF:
			begin
				utfcode := byte(poku^) shl 8 + byte((poku + 1)^);
				inc(poku, 2);
			end;

			$E0..$EF:
			begin
				utfcode := byte(poku^) shl 16 + byte((poku + 1)^) shl 8 + byte((poku + 2)^);
				inc(poku, 3);
			end;

			$F0..$F7:
			begin
				utfcode := byte(poku^) shl 24 + byte((poku + 1)^) shl 16 + byte((poku + 2)^) shl 8 + byte((poku + 3)^);
				inc(poku, 4);
			end;

			else begin
				LogError('GetUTF8Size: invalid UTF8 first byte: $' + strhex(byte(poku^)));
				exit;
			end;
		end;
		// Double-width characters:
		// 2E80..D7AF   = E2BA80..ED9EAF (mishmash of all basic CJK)
		// FF01..FF60   = EFBC81..EFBDA0 (doublewidth ascii characters)
		// FFE0..FFE6   = EFBFA0..EFBFA6 (extra CJK punctuation)
		// 20000..2CEAF = F0A08080..F0ACBAAF (tons of extended ideographs)
		if utfcode >= $E2BA80 then begin
			if (utfcode <= $ED9EAF)
			or (utfcode >= $EFBC81) and (utfcode <= $EFBDA0)
			or (utfcode >= $EFBFA0) and (utfcode <= $EFBFA6)
			or (utfcode >= $F0A08080) and (utfcode <= $F0ACBAAF)
			then inc(result);
		end;
	end;
end;

// ------------------------------------------------------------------

procedure TTextBox.RenderContent;
// The actual rendering is done just-in-time in BlitzBox.
begin
	contentNeedsRender := FALSE;
	finalNeedsRender := TRUE;
end;

procedure TTextBox.RenderBase; begin baseNeedsRender := FALSE; boxNeedsRedraw := TRUE; end;
procedure TTextBox.RenderFinal; begin finalNeedsRender := FALSE; boxNeedsRedraw := TRUE; end;

// ------------------------------------------------------------------

procedure TTextBox.BlitzBox;
// Entirely reprints a box in a console.
var i : dword;
	j, l, contentdrawrightedge : longint;
	termpos, contentpos, contentdrawsize : TCoordP;
	distfromedge, boxclipping, contentpad, contentclip : TEdgeCoordP;
	boxdrawsize : TSizeP;
	textcolor : RGBAquad;
	txtofs, escindex, choiceindex : dword;
	rowinfo : pointer;
	txt : UTF8string;
	scrollbar : array of record
		ccolor : dword;
		ccode : string[3];
	end;
	drawscrollbar : boolean = FALSE;
	invertcolors : boolean = FALSE;
	runchoice : boolean = FALSE;
	backpal : byte;

	procedure _PrepareScrollbar;
	var c : RGBAquad;
		sy : dword;
	begin
		// Reduce the box width and right padding width by one, make room for the scrollbar.
		dec(boxdrawsize.w);
		dec(contentpad.right);
		setlength(scrollbar, boxRect.renderSizeP.h);
		// Add the up and down arrows, or = if unscrollable.
		c := style.scrollbar.capColor;
		scrollbar[0].ccolor := (Rendermatic.palette[c.r shr 4][c.g shr 4][c.b shr 4].flatcolor) + backpal shl 4;
		if contentWin.scrollOfsP > 0 then
			scrollbar[0].ccode := #$E2#$86#$91
		else
			scrollbar[0].ccode := '=';
		scrollbar[high(scrollbar)].ccolor := scrollbar[0].ccolor;
		if contentWin.scrollOfsP + longint(contentWin.contentWinSizeP.h) < longint(content.fullHeightP) then
			scrollbar[high(scrollbar)].ccode := #$E2#$86#$93
		else
			scrollbar[high(scrollbar)].ccode := '=';

		// Add the trough, using a checkerboard block.
		c := style.scrollbar.troughColor;
		dword(c) := (Rendermatic.palette[c.r shr 4][c.g shr 4][c.b shr 4].flatcolor) + backpal shl 4;
		sy := boxRect.renderSizeP.h - 2;
		while sy <> 0 do begin
			scrollbar[sy].ccolor := dword(c);
			scrollbar[sy].ccode := #$E2#$96#$92;
			dec(sy);
		end;

		// Add the thumb, using a simple space.
		if boxRect.renderSizeP.h >= 3 then begin
			sy := content.fullHeightP - contentWin.contentWinSizeP.h;
			if contentWin.scrollOfsP > 0 then
				sy := ((boxRect.renderSizeP.h - 3) * contentWin.scrollOfsP + sy shr 1) div sy + 1
			else
				sy := 1;
			if sy >= dword(high(scrollbar)) then sy := dword(high(scrollbar)) - 1;
			c := style.scrollbar.thumbColor;
			scrollbar[sy].ccolor := (Rendermatic.palette[c.r shr 4][c.g shr 4][c.b shr 4].flatcolor) shl 4 + backpal;
			scrollbar[sy].ccode := ' ';
		end;

		drawscrollbar := TRUE;
	end;

	procedure _NewColor(c : RGBAquad; isvisible, force, allowinvert : boolean);
	var textpal : byte;
	begin
		if dword(textcolor) = dword(c) then isvisible := FALSE;
		dword(textcolor) := dword(c);
		if (isvisible) or (force) then begin
			if sakuparam.palettemode = EPaletteMode.Truecolor then begin
				if (allowinvert) and (invertcolors) then
					write(#27'[48;2;', textcolor.r, ';', textcolor.g, ';', textcolor.b, 'm',
						#27'[38;2;', style.baseColor[0].r, ';', style.baseColor[0].g, ';', style.baseColor[0].b, 'm')
				else
					write(#27'[38;2;', textcolor.r, ';', textcolor.g, ';', textcolor.b, 'm',
						#27'[48;2;', style.baseColor[0].r, ';', style.baseColor[0].g, ';', style.baseColor[0].b, 'm');
			end
			else begin
				textpal := Rendermatic.palette[textcolor.r shr 4][textcolor.g shr 4][textcolor.b shr 4].flatcolor;
				if (allowinvert) and (invertcolors) then
					SetColor(backpal + textpal shl 4)
				else
					SetColor(textpal + backpal shl 4);
			end;
		end;
	end;

	procedure _DrawScrollbar;
	var index : longint;
	begin
		index := termpos.y - boxRect.renderLocP.y;
		SetColor(dword(scrollbar[index].ccolor));
		UTF8Write(scrollbar[index].ccode);
		_NewColor(textcolor, TRUE, TRUE, FALSE);
	end;

	procedure _ProcessContent(maxwidthp : dword; isvisible : boolean);
	// Processes the given cell count of text content, or until the next linebreak or end of content.
	// Prints the text if isvisible is true, otherwise moves on without printing.
	var endindex, totalwidthp, spanwidthp, spanlength : dword;
	begin
		// Reinforce inverted color if highlighted choice covers more than one line.
		if (isvisible) and (invertcolors) then _NewColor(textcolor, TRUE, TRUE, TRUE);

		totalwidthp := 0;
		with content do begin
			repeat
				// Check for escape codes at current txt offset.
				while (escindex < escapeCount) and (escapeList[escindex].escapeOfs <= txtofs) do begin
					with escapeList[escindex] do case escapeCode of
						1: sysvar.caretLocP := termpos;

						10, byte('n'):
						begin
							// Only process end of line if max maxwidth requested.
							if maxwidthp = high(dword) then begin
								inc(rowinfo, 4);
								contentpos.x := longint(rowinfo^);
								inc(contentpos.y);
								inc(escindex);
							end;
							exit;
						end;

						byte('t'):
						if isvisible then begin
							spanwidthp := escapeData;
							UTF8Write(space(spanwidthp));
							inc(dword(termpos.x), spanwidthp);
							inc(dword(contentpos.x), spanwidthp);
							inc(totalwidthp, spanwidthp);
						end;

						byte('-'):
						begin
							if maxwidthp = high(dword) then begin
								contentpos.x := longint(rowinfo^);
								inc(contentpos.y);
								inc(escindex);
							end
							else begin
								UTF8Write(StringOfChar('-', maxwidthp));
								inc(dword(termpos.x), maxwidthp);
								inc(contentpos.x, longint(maxwidthp));
							end;
							exit;
						end;

						byte('c'): _NewColor(RGBAquad(escapeData), isvisible, FALSE, TRUE);

						byte('?'):
						if choiceindex < content.choiceCount then begin
							if choiceindex = dword(Choicematic.highlightIndex) then begin
								invertcolors := TRUE;
								_NewColor(textcolor, isvisible, isvisible, TRUE);
							end;
							with content.choiceList[choiceindex] do begin
								showLocP.left := contentpos.x;
								showLocP.top := contentpos.y;
							end;
							runchoice := TRUE;
						end;

						byte('.'):
						if (runchoice) and (choiceindex < content.choiceCount) then begin
							if choiceindex = dword(Choicematic.highlightIndex) then begin
								invertcolors := FALSE;
								_NewColor(textcolor, isvisible, isvisible, TRUE);
							end;
							with content.choiceList[choiceindex] do begin
								showLocP.right := contentpos.x;
								showLocP.bottom := contentpos.y + 1;
							end;
							inc(choiceindex);
							runchoice := FALSE;
						end;
					end;
					inc(escindex);
				end;

				// Is this the end of the text, or as wide as we're allowed to go during this call?
				if (txtofs = txtLength) or (totalwidthp >= maxwidthp) then break;

				// Process characters up to the end of content, end of row, or next escape code.
				endindex := txtLength;
				if (escindex < escapecount) and (escapeList[escindex].escapeOfs < endindex) then
					endindex := escapeList[escindex].escapeOfs;

				// Or if the current span exceeds maximum cell width, process characters up to that point.
				spanlength := endindex - txtofs;
				spanwidthp := GetUTF8Size(@txtContent[txtofs], spanlength);
				if totalwidthp + spanwidthp > maxwidthp then begin
					spanlength := GetUTF8LongestFit(@txtContent[txtofs], @txtcontent[endindex], spanwidthp, maxwidthp);
					spanwidthp := GetUTF8Size(@txtContent[txtofs], spanlength);
					endindex := txtofs + spanlength;
				end;
				if (isvisible) and (spanlength <> 0) then begin
					if dword(length(txt)) < spanlength then setlength(txt, 0);
					setlength(txt, spanlength);
					move(txtContent[txtofs], txt[1], spanlength);
					UTF8Write(txt);
					inc(dword(termpos.x), spanwidthp);
				end;
				inc(totalwidthp, spanwidthp);
				inc(dword(contentpos.x), spanwidthp);
				txtofs := endindex;
			until FALSE;
		end;
	end;

begin
	dword(textcolor) := 0; // silence compiler
	scrollbar := NIL;
	// Figure out where the box is relative to its viewport. This may involve clipping any or none of its sides.
	// Ignore margins, since we're not drawing any decorations; they're always 0 in console mode.
	with Viewportmatic.viewport[boxInViewport] do begin
		distfromedge.left := boxRect.renderLocP.x - viewportLoc.leftp;
		distfromedge.right := viewportLoc.rightp - boxRect.renderLocP.x - longint(boxRect.renderSizeP.w);
		distfromedge.top := boxRect.renderLocP.y - viewportLoc.topp;
		distfromedge.bottom := viewportLoc.bottomp - boxRect.renderLocP.y - longint(boxRect.renderSizeP.h);
	end;

	boxclipping.left := 0; boxclipping.right := 0; boxclipping.top := 0; boxclipping.bottom := 0;
	if distfromedge.left < 0 then boxclipping.left := -distfromedge.left;
	if distfromedge.right < 0 then boxclipping.right := -distfromedge.right;
	if distfromedge.top < 0 then boxclipping.top := -distfromedge.top;
	if distfromedge.bottom < 0 then boxclipping.bottom := -distfromedge.bottom;

	// Box completely out of bounds?
	if (boxclipping.left + boxclipping.right >= longint(boxRect.renderSizeP.w))
	or (boxclipping.top + boxclipping.bottom >= longint(boxRect.renderSizeP.h)) then exit;

	// Calculate the drawable location and size of the box, and prepare to draw.
	with style.baseColor[0] do
		backpal := Rendermatic.palette[r shr 4][g shr 4][b shr 4].flatcolor;
	_NewColor(style.textColor, TRUE, TRUE, FALSE);

	inc(distfromedge.left, boxclipping.left);
	inc(distfromedge.right, boxclipping.right);
	inc(distfromedge.top, boxclipping.top);
	inc(distfromedge.bottom, boxclipping.bottom);

	with Viewportmatic.viewport[boxInViewport] do begin
		boxdrawsize.w := viewportSizeP.w - distfromedge.left - distfromedge.right;
		boxdrawsize.h := viewportSizeP.h - distfromedge.top - distfromedge.bottom;
		termpos.x := viewportLoc.leftp + distfromedge.left;
		termpos.y := viewportLoc.topp + distfromedge.top;
	end;

	// If the box is not showing text, print the whole thing as a single block.
	if boxState <> EBoxState.ShowText then begin
		for i := boxdrawsize.h - 1 downto 0 do begin
			GotoXY(termpos.x, termpos.y);
			write(space(boxdrawsize.w));
			inc(termpos.y);
		end;
		exit;
	end;

	// Calculate the content window's visible dimensions. Subtract clipping from the visible box regions.
	contentpad.left := boxRect.padding.leftp; contentpad.right := boxRect.padding.rightp;
	contentpad.top := boxRect.padding.topp; contentpad.bottom := boxRect.padding.bottomp;
	contentclip.left := 0; contentclip.top := 0;
	contentdrawsize.x := contentWin.contentWinSizeP.w;
	contentdrawsize.y := contentWin.contentWinSizeP.h;

	dec(contentpad.left, boxclipping.left);
	if contentpad.left < 0 then begin
		inc(contentdrawsize.x, contentpad.left);
		contentpad.left := 0;
		if contentdrawsize.x < 0 then begin
			inc(contentpad.right, contentdrawsize.x);
			contentdrawsize.x := 0;
		end;
		contentclip.left := contentWin.contentWinSizeP.w - contentdrawsize.x;
	end;
	dec(contentpad.right, boxclipping.right);
	if contentpad.right < 0 then begin
		inc(contentdrawsize.x, contentpad.right);
		contentpad.right := 0;
		if contentdrawsize.x < 0 then begin
			inc(contentpad.left, contentdrawsize.x);
			contentdrawsize.x := 0;
		end;
	end;
	dec(contentpad.top, boxclipping.top);
	if contentpad.top < 0 then begin
		inc(contentdrawsize.y, contentpad.top);
		contentpad.top := 0;
		if contentdrawsize.y < 0 then begin
			inc(contentpad.bottom, contentdrawsize.y);
			contentdrawsize.y := 0;
		end;
		contentclip.top := contentWin.contentWinSizeP.h - contentdrawsize.y;
	end;
	dec(contentpad.bottom, boxclipping.bottom);
	if contentpad.bottom < 0 then begin
		inc(contentdrawsize.y, contentpad.bottom);
		contentpad.bottom := 0;
		if contentdrawsize.y < 0 then begin
			inc(contentpad.top, contentdrawsize.y);
			contentdrawsize.y := 0;
		end;
	end;

	contentdrawrightedge :=
		Viewportmatic.viewport[boxInViewport].viewportLoc.rightp - distfromedge.right - contentpad.right;
	txtofs := 0; escindex := 0; choiceindex := 0; contentpos.y := 0; txt := '';
	rowinfo := buffers.scratchP;
	contentpos.x := longint(rowinfo^);

	if (style.freescrollable) or ((Choicematic.isActive) and (Choicematic.choiceBox = self)) then
		if (contentpad.right <> 0) and (boxRect.renderSizeP.h >= 2)
		and (content.fullHeightP > contentWin.contentWinSizeP.h)
		then _PrepareScrollbar;

	// If the content is scrolled and/or clipped from top, skip a suitable number of content rows.
	j := contentWin.scrollOfsP + contentclip.top;
	if j > 0 then begin
		for i := j - 1 downto 0 do _ProcessContent(high(dword), FALSE);
		_NewColor(textcolor, TRUE, TRUE, FALSE);
	end
	else begin
		// A negative scroll offset is treated as extra padding.
		j := -j;
		if j > contentdrawsize.y then j := contentdrawsize.y;
		dec(contentdrawsize.y, j);
		inc(contentpad.top, j);
	end;

	// Print the top padding.
	if contentpad.top <> 0 then
		for i := contentpad.top - 1 downto 0 do begin
			GotoXY(termpos.x, termpos.y);
			write(space(boxdrawsize.w));
			if drawscrollbar then _DrawScrollbar;
			inc(termpos.y);
			dec(boxdrawsize.h);
		end;

	if contentdrawsize.y <> 0 then
		for i := contentdrawsize.y - 1 downto 0 do begin
			GotoXY(termpos.x, termpos.y);

			// Print the left padding.
			write(space(contentpad.left));
			inc(termpos.x, contentpad.left);

			// Calculate the number of spaces to add less clipping on the left due to text alignment.
			// If it's negative, the content gets clipped on the left.
			l := longint(rowinfo^) - contentclip.left;
			if l >= 0 then begin
				write(space(l));
				inc(termpos.x, l);
			end
			else _ProcessContent(-l, FALSE);

			// Print the visible part of this row.
			_ProcessContent(contentdrawrightedge - termpos.x, TRUE);
			// Skip whatever content is left on this row.
			_ProcessContent(high(dword), FALSE);

			// Print spaces to fill the rest of the row, which includes box padding on the right.
			//if invertcolors then begin invertcolors := FALSE; _NewColor(textcolor, TRUE, TRUE); end;
			_NewColor(textcolor, TRUE, TRUE, FALSE);
			write(space(contentdrawrightedge - termpos.x + contentpad.right));
			if drawscrollbar then _DrawScrollbar;

			termpos.x := Viewportmatic.viewport[boxInViewport].viewportLoc.leftp + distfromedge.left;
			inc(termpos.y);
			dec(boxdrawsize.h);

			// No more content? Fill the rest of the box with empty space.
			if dword(contentpos.y) > content.fullRowCount then break;
		end;

	// Make sure the rest of the content is scanned for choice rects.
	while (txtofs < content.txtLength) or (escindex < content.escapeCount) do _ProcessContent(high(dword), FALSE);

	// Fill the rest of the box with spaces to cover the bottom padding and any unused content space.
	while boxdrawsize.h <> 0 do begin
		GotoXY(termpos.x, termpos.y);
		write(space(boxdrawsize.w));
		if drawscrollbar then _DrawScrollbar;
		inc(termpos.y);
		dec(boxdrawsize.h);
	end;

	{with Choicematic do if (self = choiceBox) and (showCount <> 0) then begin
		for i := 0 to showCount - 1 do
			log(strcat('showlist %: %: %', [i, showList[i].showLocP.ToString, showlist[i].showTxt[0]]));
		log('highlighted ' + strdec(highlightIndex));
	end;}
end;

function TTextBox.RefreshOverlappingBoxes : boolean;
var i : dword;
	box2 : TTextBox;
begin
	result := FALSE;
	with BoxHub do for i := high(textbox) downto 0 do if textbox[i] <> self then begin
		box2 := textbox[i];
		// This is called from box destructor to ensure boxes overlapped by boxes being destroyed get redrawn, but when this is
		// done as part of setnumboxes, some destroyed boxes may be null already.
		if (box2 <> NIL) and (box2.boxState <> EBoxState.Null) and (NOT box2.isHidden) and (NOT box2.boxNeedsRedraw)
		and (box2.boxRect.renderLocP.x < boxRect.renderLocP.x + longint(boxRect.renderSizeP.w))
		and (box2.boxRect.renderLocP.x + longint(box2.boxRect.renderSizeP.w) >= boxRect.renderLocP.x)
		and (box2.boxRect.renderLocP.y < boxRect.renderLocP.y + longint(boxRect.renderSizeP.h))
		and (box2.boxRect.renderLocP.y + longint(box2.boxRect.renderSizeP.h) >= boxRect.renderLocP.y)
		then begin
			box2.boxNeedsRedraw := TRUE;
			result := TRUE;
		end;
	end;
end;

{$include boxhub-implementation.pas}
