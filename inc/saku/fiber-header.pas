{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Stopping = not running, can be removed
// Normal = executing script
// Waitkey = pause until keypress not eaten by a more important box/event, or fiber is signalled
// Waitclear = deprecated
// Waitchoice = pause until user selects a new choice in choicematic, or choice is cancelled
// Waitsignal = pause until signalled by another fiber
// Waitsleep = pause until thread's sleep effect expires, or fiber is signalled
// Waitfx = pause until thread's fx refcount is 0, or fiber is signalled
// Waittyping = pause until user enters a line in choicematic, or fiber is signalled
type EFiberState = (
	Stopping = 0, Normal = 1, WaitKey = 2, WaitClear = 3,
	WaitChoice = 4, WaitSignal = 5, WaitSleep = 6, WaitFx = 7, WaitTyping = 8);
// The numbers are in save states, so changes will break compatibility.

type TFiberStates = set of EFiberState;

// Each fiber's data stack contains EStackTokens and their associated values.
type EStackToken = (
	EndParams = 0,
	Number = 10, // paired with longint value
	SingleStr = 11, // paired with 1 longstring
	MultiStr = 12, // paired with lang x longstrings
	Null = 255,
	Param = 256); // supplements param enum, see sakurascript-compiler.pas

const
	FIBER_STACK_SIZE = 2048; // this many dwords, must be ^2
	CALLSTACK_HIGH = 15; // must be ^2 - 1, soft max 255

type EPoppedToken = (Int, Str, Null);

type TFiber = class
	fiberName : UTF8string;
	labelName : UTF8string;
	labelObject: TScriptObject;
	codeOfs : dword; // current offset in labelObject.code^

	// Every timed effect spawned by this fiber increases fxRefCount.
	// Any effect belonging to this fiber decreases it on expiry.
	// This allows efficiently waiting for all timed effects to complete.
	fxRefCount : dword;

	// Fiber-local variables, accessible by index only.
	locals : array of longint;
	localCount : dword;

	// When a command token is encountered in bytecode, the param list is wiped, and params are read from bytecode
	// until the params end token. These are volatile and not saved in save states. Normally only used within fiber
	// execution, the only exception being parameters set on label spawn.
	namedparam : record
		defined : array[0..63] of boolean;
		pNumValue : array[0..63] of longint;
		pStrValue : array[0..63] of TStringBunch;
	end;
	dynamicparam : array[0..31] of record
		pStrValue : TStringBunch;
		pNumValue : longint;
		pType : EPoppedToken;
	end;
	dparamcount : byte;

	fiberState : EFiberState;
	callstackIndex : byte; // rolling counter, points at next free slot

	// Stack used to execute sakurascript. Script consists of tokenised operands and operations; operands get pushed on
	// the stack, and operations pop them off to do things with them, possibly pushing the result back. The stack loops
	// around, since some commands leave unpopped leftovers. DataCount tracks how many valid poppable dwords are
	// available, to catch stack underflows.
	dataStack : array[0..FIBER_STACK_SIZE - 1] of dword;
	dataIndex, dataCount : dword;

	// The furthest-back callstack level must always be zeroed out, so any attempt to return further back will show an
	// error.
	callstack : array[0..CALLSTACK_HIGH] of record
		labelName : UTF8string;
		ofs : dword;
	end;

	function GetCurrentId : longint;
	function ExpandRelativeLabel(const labelref : UTF8string) : UTF8string;
	procedure ScriptGoto(const _labelname : UTF8string);
	procedure ScriptCall(const _labelname : UTF8string);
	procedure ScriptReturn(onelevel : boolean);
	procedure ScriptCase(const _labels : TStringBunch; caseindex : dword; docall : boolean);
	procedure SetLocal(index : dword; value : longint);
	procedure PushInt(num : longint);
	procedure PushInt(token : EStackToken); inline;
	procedure PushBytes(srcp : pointer; numbytes : dword);
	procedure PushString(const s : UTF8string);
	procedure PushStringBunch(const a : TStringBunch);
	function PopInt : longint;
	function PopString : UTF8string;
	procedure Run(yieldnow : boolean);
	function ToString : UTF8string; override;

	constructor Create(_labelname, _fibername : UTF8string);
	destructor Destroy; override;
end;

// ------------------------------------------------------------------

type TFiberHub = class
	fiber : array of TFiber;
	fiberCount, executingId : dword;
	exitRunFibersNow, exitOnAnyError : boolean;
	private const varEndian : char = {$ifdef ENDIAN_LITTLE}'v'{$else}'V'{$endif};

	public
	function GetFiber(const name : UTF8string) : dword;
	procedure SignalFibers(_fibername : UTF8string; const states : TFiberStates);
	procedure StopFibers(_fibername : UTF8string);
	function ClearWaitKeys : boolean;
	procedure Reset;
	procedure RunDebugCommand;
	procedure RunFibers;
	procedure UpdateScriptIndexes;
	function Serialise : TStringBunch;
	procedure Deserialise(buf, bufend : pointer);
	procedure LogCallstack(const _fibername : UTF8string);
	destructor Destroy; override;
end;

