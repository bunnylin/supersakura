{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// A sprite is a basic static image, an anim is the same but with active animation.
// A background is always the lowest gob in a viewport regardless of Z-level, and there can only be one background in
// each viewport. Expected to be a full-viewport image.
// An overlay is also a full-viewport image, but does respect Z-level. These are used for temporary special scenes.
// Backgrounds and overlays have no parent, while sprites normally use the topmost bkg/overlay as parent.
type EGobType = (Unknown = 0, Bkg = 1, Overlay = 2, Sprite = 3, Anim = 4);

function EGobTypeFromString(const instr : UTF8string) : EGobType;
begin
	case lowercase(instr) of
		'0'..'4': result := EGobType(ord(instr[1]) - 48);
		'bkg','background': result := EGobType.Bkg;
		'overlay': result := EGobType.Overlay;
		'sprite': result := EGobType.Sprite;
		'anim','animation','default','': result := EGobType.Anim;
		else result := EGobType.Unknown;
	end;
end;

// The basic gob is a bitmap of some sort, with or without a disk-backed asset.
type TGob = class(TElement)
	gfxListName : UTF8string;
	cachedObject : TGraphicObject;

	gobFrame : dword; // which frame of the graphic to display
	animSeqP : dword; // current frame: gfxlist[].sequence[animseqp]
	animTimer : dword; // next frame after x msecs, -1 if not animating
	gobType : EGobType;
	runtimeDynamic : boolean; // if true, gob is a flat rectangle of solidBlit color, no backing graphic object
	loadtimeDynamic : boolean; // if true, gfxListName is a generation command, no backing file on disk

	procedure AutoAdopt;
    procedure RefreshFrameP; inline;
	function EnsureCached : boolean;
	function ToString : UTF8string; override;
	constructor Create(
		listname : UTF8string; _elename : string31; _parent : string31; _gobtype : EGobType;
		inviewport, locx, locy, zlevel, anchorx, anchory, sizex, sizey : longint);
	constructor Clone(src : TGob);
end;

