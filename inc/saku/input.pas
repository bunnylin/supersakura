{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function PollKey(arrow : EArrows) : longint;
// Returns non-zero if the given key is held down, or 0 if it is not.
// Checks cursor keys first, then gamepad direction buttons, then gamepad left stick.
// On the console port, only checks the cursor keys.
// The non-zero return value will be 32767 for digital buttons, and in the range 1..32768 for the analog left stick.
begin
	sysvar.keysPolled := TRUE;
	result := 0;

	{$ifdef sakucon}
	if arrow in sysvar.keysdown then result := 32767;
	{$else}

	case arrow of
		EArrows.Down:
		if (byte((sdl_KeyState + SDL_SCANCODE_DOWN)^) <> 0)
		or (sdl_GamepadH <> NIL) and (SDL_GameControllerGetButton(sdl_GamepadH, SDL_CONTROLLER_BUTTON_DPAD_DOWN) <> 0)
		then
			result := 32767
		else
			result := sysvar.gamepadLeftStick.y;

		EArrows.Up:
		if (byte((sdl_KeyState + SDL_SCANCODE_UP)^) <> 0)
		or (sdl_GamepadH <> NIL) and (SDL_GameControllerGetButton(sdl_GamepadH, SDL_CONTROLLER_BUTTON_DPAD_UP) <> 0)
		then
			result := 32767
		else
			result := -sysvar.gamepadLeftStick.y;

		EArrows.Left:
		if (byte((sdl_KeyState + SDL_SCANCODE_LEFT)^) <> 0)
		or (sdl_GamepadH <> NIL) and (SDL_GameControllerGetButton(sdl_GamepadH, SDL_CONTROLLER_BUTTON_DPAD_LEFT) <> 0)
		then
			result := 32767
		else
			result := -sysvar.gamepadLeftStick.x;

		EArrows.Right:
		if (byte((sdl_KeyState + SDL_SCANCODE_RIGHT)^) <> 0)
		or (sdl_GamepadH <> NIL) and (SDL_GameControllerGetButton(sdl_GamepadH, SDL_CONTROLLER_BUTTON_DPAD_RIGHT) <> 0)
		then
			result := 32767
		else
			result := sysvar.gamepadLeftStick.x;
	end;

	if result < 0 then result := 0;
	{$endif}
end;

function StopSkipping : boolean;
// Turns off text skipping, returns TRUE if it used to be on.
begin
	result := sysvar.skipText <> ESkipState.No;
	sysvar.skipText := ESkipState.No;
	sysvar.autoChoice := FALSE;
end;

procedure UserInput_CtrlB; inline;
begin
	// If not in normal metastate, ignore it.
	if sysvar.metaState <> EMetaState.Normal then exit;
	BoxHub.HideAllBoxes(NOT BoxHub.allBoxesHidden);
end;

procedure UserInput_CtrlD;
// Toggles the dropdown console in debug mode.
begin
	// User must type Ctrl-XYZZY to allow debug mode.
	if sysvar.debugAllowed < 5 then exit;

	// Dropdown not allowed when in confirm quit mode.
	if sysvar.metaState = EMetaState.ConfirmQuit then exit;

	with BoxHub.textbox[high(BoxHub.textbox)] do begin
		// If the dropdown console is hidden, show it, and don't stop at this step.
		if isHidden then Hide(FALSE);

		// If the dropdown console is already in debug mode, remove the box.
		if sysvar.dropdownConsoleState = EDDCState.Debug then begin
			BoxHub.DropdownSlideUp;
			exit;
		end;

		// If the dropdown console is not displayed or is vanishing, slide it in.
		if boxState in [EBoxState.Null, EBoxState.Vanishing] then BoxHub.DropdownSlideDown;

		// Box is visible or appearing, and not in debug mode yet: go to debug mode.
		sysvar.dropdownConsoleState := EDDCState.Debug;
		StartTextInput('', -1);
		sysvar.dropdownContentChanged := TRUE;
	end;
end;

procedure UserInput_CtrlF; inline;
begin
	// If metastate is not normal, do nothing.
	if sysvar.metaState <> EMetaState.Normal then exit;

	// Enable or disable skip seen text mode.
	if NOT StopSkipping then sysvar.skipText := ESkipState.Seen;
end;

procedure UserInput_CtrlI;
// Shows the game and engine info box.
begin
	// Can only be displayed when in normal metastate.
	if sysvar.metaState <> EMetaState.Normal then exit;
	SummonInfoBox;
end;

procedure UserInput_CtrlL;
begin
	// If not in normal metastate, do nothing.
	if sysvar.metaState <> EMetaState.Normal then exit;

	// If skip seen text mode is enabled, disable it, and don't stop at this step.
	StopSkipping;

	try
		log('Quickloading...');
		Savematic.EnumerateSaves(FALSE);
		if length(Savematic.availableSaveList) = 0 then
			SummonMessageBox('No savefile found.')
		else begin
			if Savematic.LoadState(Savematic.availableSaveList[0]) then log('Load ok.');
		end;
	except
		on e : Exception do LogError('Failed to load', e, ExceptAddr);
	end;
end;

procedure UserInput_CtrlM;
// Toggles the autotest mode, if debugging is enabled.
begin
	if sysvar.debugAllowed < 5 then exit;
	sysvar.autoChoice := NOT sysvar.autoChoice;
	if sysvar.autoChoice then
		sysvar.skipText := ESkipState.All
	else
		sysvar.skipText := ESkipState.No;
end;

procedure UserInput_CtrlP;
begin
	if (sysvar.pauseState = EPauseState.Normal) and (sysvar.metaState <> EMetaState.ConfirmQuit) then
		SetPauseState(EPauseState.Paused)
	else
		SetPauseState(EPauseState.Normal);
end;

procedure UserInput_CtrlS;
begin
	// If not in normal metastate, do nothing.
	if sysvar.metaState <> EMetaState.Normal then exit;
	if NOT sysvar.allowSaving then exit;

	// If skip seen text mode is enabled, disable it, and don't stop at this step.
	StopSkipping;

	with Savematic do begin
		saveNow := TRUE;
		saveNowIndex := 0;
		saveNowDesc := 'quicksave';
	end;
end;

procedure UserInput_CtrlT;
// Toggles the dropdown console in transcript mode.
begin
	// If not in normal metastate, do nothing.
	if sysvar.metaState <> EMetaState.Normal then exit;

	// If skip seen text mode is enabled, disable it, and don't stop at this step.
	StopSkipping;

	with BoxHub.textbox[high(BoxHub.textbox)] do begin
		// If the dropdown console is hidden, show it, and don't stop at this step.
		if isHidden then Hide(FALSE);

		// If the dropdown console is already in transcript mode, remove the box.
		if sysvar.dropdownConsoleState = EDDCState.Log then begin
			BoxHub.DropdownSlideUp;
			exit;
		end;

		// If the dropdown console is in debug mode, leave debug mode and don't stop at this step.
		if sysvar.dropdownConsoleState = EDDCState.Debug then StopTextInput;

		// If the dropdown console is not displayed or is vanishing, slide it in.
		if boxState in [EBoxState.Null, EBoxState.Vanishing] then BoxHub.DropdownSlideDown;

		// Box is visible or appearing, and not in transcript mode yet: go to transcript mode.
		sysvar.dropdownConsoleState := EDDCState.Log;
		sysvar.dropdownContentChanged := TRUE;
	end;
end;

procedure UserInput_CtrlV;
// Pastes text from clipboard, or shows the game and engine info box.
begin
	UserInput_CtrlI;
end;

// ------------------------------------------------------------------

procedure UserInput_Mouse(musx, musy : longint; button : byte);
// The coordinates are a pixel value from the game window's top left.
// The button is 0 if this is just the mouse moving around; 1 if it's a left-click and 3 if it's a right-click.
var i : dword;
	x, y : longint;
	overvalidchoice : boolean;
begin
	sysvar.mouseLocP.x := musx;
	sysvar.mouseLocP.y := musy;
	overvalidchoice := FALSE;

	// Check if mouseovering choices in an active choiceBox.
	with Choicematic do if (isActive) and (choiceBox <> NIL) then
		with choiceBox do with boxRect do if (boxState <> EBoxState.Null) and (NOT isHidden) then begin
			if (musx >= paddedLocP.x) and (musx < paddedLocP.x + longint(paddedSizeP.w))
			and (musy >= paddedLocP.y) and (musy < paddedLocP.y + longint(paddedSizeP.h))
			then with content do begin
				// Calculate the cursor's location relative to the full content buffer.
				x := musx - paddedLocP.x - longint(padding.leftp);
				y := musy - paddedLocP.y - longint(padding.topp) + contentWin.scrollOfsP;
				// Check if the cursor is over any choice rect.
				i := choiceCount;
				while i <> 0 do begin
					dec(i);
					if (x >= longint(choiceList[i].showLocP.left))
					and (x < longint(choiceList[i].showLocP.right))
					and (y >= longint(choiceList[i].showLocP.top))
					and (y < longint(choiceList[i].showLocP.bottom))
					then begin
						overvalidchoice := TRUE;
						if i <> dword(highlightindex) then HighlightChoice(i);
						break;
					end;
				end;
			end;
		end;

	// React to the mouse cursor moving on or off any overable areas or elements.
	Eventmatic.CheckOverables;

	// Handle mouse clicks...

	// Left-click!
	if button = 1 then begin

		// If skip seen text mode is enabled, disable it.
		if StopSkipping then exit;

		// If textboxes are hidden, make them visible.
		if BoxHub.allBoxesHidden then begin
			BoxHub.HideAllBoxes(FALSE);
			exit;
		end;

		// If clicking over a choice rectangle in the choice box, select the currently highlighted choice.
		with Choicematic do if (isActive) and (overvalidchoice) then begin
			SelectChoice(highlightindex);
			exit;
		end;

		// If clicking over any box, page boxes ahead and resume waitkeys.
		i := BoxHub.GetBoxAt(musx, musy);
		if i < dword(length(BoxHub.textbox)) then begin
			if BoxHub.CheckPageableBoxes then exit;
			FiberHub.ClearWaitKeys;
			exit;
		end;

		// If over any mouseoverable, trigger them.
		if Eventmatic.TriggerOverables(TRUE) <> 0 then exit;

		// Page boxes ahead and resume waitkeys, even if not clicking a box.
		if BoxHub.CheckPageableBoxes then exit;
		if FiberHub.ClearWaitKeys then exit;

		// If a normal interrupt is defined and an interrupt hasn't happened yet in this frame, trigger it.
		with Eventmatic do if (interruptLabel <> '') and (NOT hasTriggeredInterrupt) then begin
			hasTriggeredInterrupt := TRUE;
			TFiber.Create(interruptLabel, '');
		end;

	end
	else

		// Right-click!
		if button = 3 then begin

			// If skip seen text mode is enabled, disable it.
			if StopSkipping then exit;

			// If the dropdown console is in transcript mode, slide out the box.
			if sysvar.dropdownConsoleState = EDDCState.Log then begin
				UserInput_CtrlT;
				exit;
			end;

			if NOT BoxHub.allBoxesHidden then begin
				// If boxes are visible and choicematic is active, and not on the topmost choice level,
				// cancel out of choicematic if user-cancellable at sub-level, or revert toward the top level.
				with Choicematic do if (isActive) and (selectedNode > 0) then begin
					if userCanCancel and 2 <> 0 then
						Deactivate(TRUE, clearOnDeactivate)
					else
						RevertChoice;
					exit;
				end;

				// If metastate is normal and boxes are visible and mouse is over any displayed box, hide all boxes.
				if sysvar.metaState = EMetaState.Normal then
					if BoxHub.GetBoxAt(musx, musy) < dword(length(BoxHub.textbox)) then begin
						BoxHub.HideAllBoxes(TRUE);
						exit;
					end;

				// If boxes are visible and choicematic is active, and on top level, and is user-cancellable at
				// top level, cancel it.
				with Choicematic do if (isActive) and (selectedNode = 0) and (userCanCancel and 1 <> 0) then begin
					Deactivate(TRUE, clearOnDeactivate);
					exit;
				end;
			end;

			// If textboxes are hidden, make them visible.
			if BoxHub.allBoxesHidden then begin
				BoxHub.HideAllBoxes(FALSE);
				exit;
			end;

			// If esc-interrupt is defined and an interrupt hasn't happened yet in this frame, trigger it.
			with Eventmatic do if (escInterruptLabel <> '') and (NOT hasTriggeredInterrupt) then begin
				hasTriggeredInterrupt := TRUE;
				TFiber.Create(escInterruptLabel, '');
				exit;
			end;

			// If metastate is normal, enter the metamenu metastate.
			if sysvar.metaState = EMetaState.Normal then SummonMetaMenu;
		end;
end;

{$ifndef sakucon}
procedure UserInput_Wheel(y : longint);
// Input positive numbers to scroll up/away, negative to scroll down/toward.
var i, j : dword;
	newpos : longint;
begin
	with BoxHub do begin
		// If textboxes are hidden, ignore.
		if allBoxesHidden then exit;

		// If mouseovering a scrollable box, try to scroll that box. Otherwise scroll topmost scrollable box.
		// The choice box is always considered scrollable.
		i := GetBoxAt(sysvar.mouseLocP.x, sysvar.mouseLocP.y);
		if (i >= dword(length(textbox)))
		or ((NOT textbox[i].style.freeScrollable)
		and ((Choicematic.choiceBox <> textbox[i]) or (NOT Choicematic.isActive)))
		then begin
			i := length(textbox);
			while i <> 0 do begin
				dec(i);
				if (textbox[i].boxState <> EBoxState.Null) and (NOT textbox[i].isHidden) then
				if (textbox[i].style.freeScrollable)
				or ((Choicematic.choiceBox = textbox[i]) and (Choicematic.isActive))
				then break;
			end;
			// Nothing scrollable.
			if i = 0 then exit;
		end;

		with textbox[i] do begin
			newpos := contentWin.scrollOfs;
			// Check for an existing scroll effect.
			with EffectHub do begin
				if fxCount <> 0 then for j := fxCount - 1 downto 0 do
					if (fx[j] is TEffectBoxScroll) and (fx[j].fxBox = BoxHub.textbox[i]) then begin
						// Found one. Import its target scroll offset. This allows multiple sequential wheel ticks to
						// add up naturally. Also triple the new delta for an acceleration effect.
						newpos := (fx[j] as TEffectBoxScroll).toOfs - (y shl 16);
						break;
					end;
			end;

			dec(newpos, y shl 15);
			ScrollTo(newpos);
		end;
	end;
end;
{$endif}

// ------------------------------------------------------------------

function MoveToMouseoverable(vecx, vecy : longint) : boolean;
// Attempts to find the closest mouseoverable area or element center point in the approximate direction of the given
// vector from the current mouse cursor position. If one was found, teleports the cursor there and returns TRUE. If no
// overable found, returns FALSE.
var i, dist, bestdist : dword;
	x, y, bestx, besty : longint;
	moveangle : float;

	procedure _TryNewBest();
	var anglediff : float;
	begin
		if (x or y) = 0 then exit;
		anglediff := abs(arctan2(y, x) - moveangle);
		// Handle wraparound on the left side.
		if anglediff > 3.1416 then anglediff := 6.2832 - anglediff;
		// Desired angle must be within 45 deg = pi/4 = ~0.7854
		if anglediff < 0.7854 then begin
			dist := x * x + y * y;
			if dist < bestdist then begin
				bestx := x; besty := y;
				bestdist := dist;
			end;
		end;
	end;

begin
	result := FALSE;
	if (vecx or vecy) = 0 then exit;
	// To allow approximate directions, use a +/- 45 degree arc around the vector direction, then find the closest
	// overable in that sector. The obvious solution is trigonometric, although it could also be done geometrically...
	// Arctan2 treats -> as the 0-angle, and the return value is in [-pi..+pi].
	// X increases toward the right, and Y increases downward; a positive return value is clockwise, negative is
	// counterclockwise. FPC docs say input X shouldn't be 0, but that produces the correct result, so it's fine.
	moveangle := arctan2(vecy, vecx);

	bestx := 0; besty := 0; bestdist := high(dword);

	with Eventmatic do begin
		if length(eventArea) <> 0 then for i := high(eventArea) downto 0 do // deprecated
			with eventArea[i] do
				if NOT mouseOnly then begin
					x := centerPoint.x - sysvar.mouseLocP.x;
					y := centerPoint.y - sysvar.mouseLocP.y;
					_TryNewBest;
				end;

		if numElementEvents <> 0 then for i := numElementEvents - 1 downto 0 do
			with elementEvent[i] do
				if NOT mouseOnly then with element do begin
					x := eleLoc.xp + longint(eleSizeP.w shr 1) - sysvar.mouseLocP.x;
					y := eleLoc.yp + longint(eleSizeP.h shr 1) - sysvar.mouseLocP.y;
					_TryNewBest;
				end;
	end;

	result := bestdist <> high(dword);
	if result then UserInput_Mouse(sysvar.mouseLocP.x + bestx, sysvar.mouseLocP.y + besty, 0);
end;

procedure UserInput_Enter;
begin
	// If skip seen text mode is enabled, disable it.
	if StopSkipping then exit;

	with BoxHub do begin
		// If textboxes are hidden, make them visible.
		if allBoxesHidden then begin
			HideAllBoxes(FALSE);
			exit;
		end;

		// If the dropdown console in debug mode contains user input, execute the last line.
		if (sysvar.dropdownConsoleState = EDDCState.Debug) and (textbox[high(textbox)].content.userInputLen <> 0) then begin
			FiberHub.RunDebugCommand;
			exit;
		end;

		// Check boxes for pageble content. Any box that has more to display and is not freely scrollable but does have
		// autowaitkey enabled, will scroll ahead by a page, swallowing the keystroke.
		if CheckPageableBoxes then exit;
	end;

	// Select mouseoverables that are highlighted and have trigger labels.
	if Eventmatic.TriggerOverables(FALSE) <> 0 then exit;

	// If choicematic is active, select the highlighted choice.
	with Choicematic do if isActive then begin
		SelectChoice(highlightIndex);
		exit;
	end;

	// If a box is accepting text input, resume any waittyping fibers.
	with BoxHub do if (activeTextInput <> 0) and (CompleteTextInput) then exit;

	// Resume any fibers in waitkey state.
	if FiberHub.ClearWaitKeys then exit;

	// If a normal interrupt is defined and an interrupt hasn't happened yet in this frame, trigger it.
	with Eventmatic do if (interruptLabel <> '') and (NOT hasTriggeredInterrupt) then begin
		hasTriggeredInterrupt := TRUE;
		TFiber.Create(interruptLabel, '');
	end;
end;

procedure UserInput_Esc;
begin
	// If skip seen text mode is enabled, disable it.
	if StopSkipping then exit;

	// If textboxes are hidden, make them visible.
	with BoxHub do if allBoxesHidden then begin
		HideAllBoxes(FALSE);
		exit;
	end;

	// If the dropdown console is in transcript mode, slide out the box.
	if sysvar.dropdownConsoleState = EDDCState.Log then begin
		UserInput_CtrlT;
		exit;
	end;

	with Choicematic do if isActive then begin
		// If choicematic is user-cancellable at current level, cancel.
		if ((selectedNode = 0) and (userCanCancel and 1 <> 0))
		or ((selectedNode > 0) and (userCanCancel and 2 <> 0))
		then begin
			Deactivate(TRUE, clearOnDeactivate);
			exit;
		end;
		// If choicematic is not on top level, revert toward top level.
		if selectedNode > 0 then begin
			RevertChoice;
			exit;
		end;
	end;

	// If esc-interrupt is defined and an interrupt hasn't happened yet in this frame, trigger it.
	with Eventmatic do if (escInterruptLabel <> '') and (NOT hasTriggeredInterrupt) then begin
		hasTriggeredInterrupt := TRUE;
		TFiber.Create(escInterruptLabel, '');
		exit;
	end;

	// If metastate is normal, enter the metamenu metastate; otherwise return to previous metastate.
	if sysvar.metaState = EMetaState.Normal then
		SummonMetaMenu
	else
		HubStack.Pop;
end;

procedure UserInput_Menu;
// That's the "context menu" key beside right ctrl on a common 105-key keyboard. Shift-F10 does the same.
begin
	// If skip seen text mode is enabled, disable it.
	if StopSkipping then exit;

	// If an esc-interrupt is defined, ignore.
	if Eventmatic.escInterruptLabel <> '' then exit;

	// If textboxes are hidden, make them visible, and don't stop at this step.
	with BoxHub do if allBoxesHidden then HideAllBoxes(FALSE);

	// If the dropdown console is in transcript mode, pop out the box, and don't stop yet.
	if sysvar.dropdownConsoleState = EDDCState.Log then UserInput_CtrlT;

	// If metastate is normal, summon the right-click popup menu.
	if sysvar.metaState = EMetaState.Normal then SummonMetaMenu;

	// Else metastate is not normal: ignore.
end;

procedure UserInput_TextInput(const instr : pchar);
// Localised keyboard input comes in as snippets of UTF8.
var i, j : dword;
begin
	if instr = '' then exit;
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then

			// If the box accepts text input, add the snippet there.
			if content.caretPos >= 0 then begin
				InsertTextInput(instr);
				exit;
			end;

	// If choicematic is active, scan from the current highlight to find and select the next item that begins with this
	// snippet.
	with Choicematic do if (isActive) and (choiceBox <> NIL) and (choiceBox.boxState <> EBoxState.Null)
	and (NOT choiceBox.isHidden) and (choiceBox.content.choiceCount <> 0) then with choiceBox.content do begin
		Assert(highlightIndex >= 0);
		for i := choiceCount - 1 downto 0 do begin
			highlightIndex := dword(highlightIndex + 1) mod choiceCount;
			with choiceList[highlightIndex] do begin
				j := index;
				if j = 0 then j := highlightIndex + 1; // ad-hoc
				if j < choiceNodeCount then with choiceNodes[j] do begin
					if (choiceTxt[activeLanguage].StartsWith(instr))
					or (instr[0] in ['A'..'Z','a'..'z']) and (byte(choiceTxt[activeLanguage][1]) xor $20 = byte(instr[0]))
					then begin
						HighlightChoice(highlightIndex);
						exit;
					end;
				end;
			end;
		end;
	end;
end;

procedure UserInput_Backspace;
var i : dword;
begin
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then begin

			// If the text box accepts text input, delete a character there.
			if content.caretPos >= 0 then begin
				DeleteTextInput(1, TRUE);
				exit;
			end;

			// If choicematic is active in this box and not on top level, revert to top level.
			// If user-cancellable at sub-level, cancel instead.
			with Choicematic do if (isActive) and (choiceBox = BoxHub.textbox[i]) and (selectedNode > 0) then begin
				if userCanCancel and 2 <> 0 then
					Deactivate(TRUE, clearOnDeactivate)
				else
					RevertChoice;
				exit;
			end;
		end;
end;

procedure UserInput_Delete;
var i : dword;
begin
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then begin
			// If the text box accepts text input, delete a character there.
			if content.caretPos >= 0 then begin
				DeleteTextInput(1, FALSE);
				exit;
			end;
		end;
end;

procedure UserInput_Home(ctrl : boolean);
var i : dword;
begin
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then begin

			if ctrl = FALSE then begin
				// If the text box accepts text input, move the caret.
				if content.caretPos >= 0 then begin
					MoveCaret(high(dword), TRUE, FALSE);
					exit;
				end;

				// If choicematic is active in this box, move the highlight.
				with Choicematic do if (isActive) and (choiceBox = BoxHub.textbox[i]) then begin
					HighlightChoice(0);
					exit;
				end;
			end;

			// If the box is freely scrollable, scroll the box.
			if style.freeScrollable then begin
				if contentWin.scrollOfsP > 0 then ScrollTo(0);
				exit;
			end;
		end;

	// Move to closest mouseoverable.
	if NOT ctrl then MoveToMouseoverable(-16, -16);
end;

procedure UserInput_End(ctrl : boolean);
var i : dword;
begin
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then begin

			if ctrl = FALSE then begin
				// If the text box accepts text input, move the caret.
				if content.caretPos >= 0 then begin
					MoveCaret(high(dword), FALSE, FALSE);
					exit;
				end;

				// If choicematic is active in this box, move the highlight.
				with Choicematic do if (isActive) and (choiceBox = BoxHub.textbox[i]) then begin
					HighlightChoice(content.choiceCount - 1);
					exit;
				end;
			end;

			// If the box is freely scrollable, scroll the box.
			if style.freeScrollable then begin
				ScrollTo(high(longint));
				exit;
			end;
		end;

	// Move to closest mouseoverable.
	if NOT ctrl then MoveToMouseoverable(-16, 16);
end;

procedure UserInput_PageUp;
var i : dword;
	j : longint;
begin
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then begin

			// If choicematic is active in this box, move the highlight.
			with Choicematic do if (isActive) and (choiceBox = BoxHub.textbox[i]) then begin
				MoveHighlightUp(contentWin.contentWinSizeP.h div contentWin.lineHeightP);
				exit;
			end;

			// If the box is freely scrollable, scroll the box.
			if style.freeScrollable then with contentWin do begin
				j := ((contentWinSizeP.h - lineHeightP) shl 15) div lineHeightP;
				ScrollTo(scrollOfs - j);
				exit;
			end;
		end;

	// Move to closest mouseoverable.
	MoveToMouseoverable(16, -16);
end;

procedure UserInput_PageDown;
var i : dword;
	j : longint;
begin
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then begin

			// If choicematic is active in this box, move the highlight.
			with Choicematic do if (isActive) and (choiceBox = BoxHub.textbox[i]) then begin
				MoveHighlightDown(contentWin.contentWinSizeP.h div contentWin.lineHeightP);
				exit;
			end;

			// If the box is freely scrollable, scroll the box.
			if style.freeScrollable then with contentWin do begin
				j := ((contentWinSizeP.h - lineHeightP) shl 15) div lineHeightP;
				ScrollTo(scrollOfs + j);
				exit;
			end;
		end;

	// Move to closest mouseoverable.
	MoveToMouseoverable(16, 16);
end;

procedure UserInput_Left(ctrl : boolean);
var i : dword;
begin
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then begin

			// If the text box accepts text input, move the caret.
			if content.caretPos >= 0 then begin
				MoveCaret(1, TRUE, ctrl);
				exit;
			end;

			// If choicematic is active in this box and has more than one column, move the highlight.
			if NOT ctrl then with Choicematic do
				if (isActive) and (choiceBox = BoxHub.textbox[i]) and (choiceBox.contentWin.columnCount > 1) then begin
					MoveHighlightLeft;
					exit;
				end;
		end;

	{$ifdef sakucon}
	Include(sysvar.keysdown, EArrows.Left);
	{$endif}

	// Move to closest mouseoverable.
	if NOT ctrl then MoveToMouseoverable(-16, 0);
end;

procedure UserInput_Right(ctrl : boolean);
var i : dword;
begin
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then begin

			// If the text box accepts text input, move the caret.
			if content.caretPos >= 0 then begin
				MoveCaret(1, FALSE, ctrl);
				exit;
			end;

			// If choicematic is active in this box and has more than one column, move the highlight.
			if NOT ctrl then with Choicematic do
				if (isActive) and (choiceBox = BoxHub.textbox[i]) and (choiceBox.contentWin.columnCount > 1) then begin
					MoveHighlightRight;
					exit;
				end;
		end;

	{$ifdef sakucon}
	Include(sysvar.keysdown, EArrows.Right);
	{$endif}

	// Move to closest mouseoverable.
	if NOT ctrl then MoveToMouseoverable(16, 0);
end;

procedure UserInput_Up(ctrl : boolean);
var i : dword;
begin
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// If the debug console is visible, fetch commands from command history.

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then begin

			// If choicematic is active in this box, move the highlight.
			if NOT ctrl then with Choicematic do if (isActive) and (choiceBox = BoxHub.textbox[i]) then begin
				MoveHighlightUp;
				exit;
			end;

			// If the box is freely scrollable, scroll the box.
			if style.freeScrollable then begin
				ScrollTo(contentWin.scrollOfs - 32768);
				exit;
			end;
		end;

	{$ifdef sakucon}
	Include(sysvar.keysdown, EArrows.Up);
	{$endif}

	// Move to closest mouseoverable.
	if NOT ctrl then MoveToMouseoverable(0, -16);
end;

procedure UserInput_Down(ctrl : boolean);
var i : dword;
begin
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// If the debug console is visible, fetch commands from command history.

	// For each box from topmost...
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Vanishing) and (boxState <> EBoxState.Null) and (NOT isHidden) then begin

			// If choicematic is active in this box, move the highlight.
			if NOT ctrl then with Choicematic do if (isActive) and (choiceBox = BoxHub.textbox[i]) then begin
				MoveHighlightDown;
				exit;
			end;

			// If the box is freely scrollable, scroll the box.
			if style.freeScrollable then begin
				ScrollTo(contentWin.scrollOfs + 32768);
				exit;
			end;
		end;

	{$ifdef sakucon}
	Include(sysvar.keysdown, EArrows.Down);
	{$endif}

	// Move to closest mouseoverable.
	if NOT ctrl then MoveToMouseoverable(0, 16);
end;

// ------------------------------------------------------------------

{$ifndef sakucon}
procedure UserInput_GamepadCancel;
begin
	// If skip seen text mode is enabled, disable it.
	if StopSkipping then exit;

	// If textboxes are hidden, make them visible.
	if BoxHub.allBoxesHidden then begin
		BoxHub.HideAllBoxes(FALSE);
		exit;
	end;

	// If the dropdown console is visible and in log mode, slide out the box.
	if sysvar.dropdownConsoleState = EDDCState.Log then begin
		UserInput_CtrlT;
		exit;
	end;

	// If choicematic is active, and is user-cancellable at the current level, cancel it.
	// If choicematic is active, and not on top choice level, go up a level.
	with Choicematic do if isActive then begin
		if ((selectedNode = 0) and (userCanCancel and 1 <> 0))
		or ((selectedNode > 0) and (userCanCancel and 2 <> 0))
		then begin
			Deactivate(TRUE, clearOnDeactivate);
			exit;
		end;
		if selectedNode > 0 then begin
			RevertChoice;
			exit;
		end;
	end;

	// Check boxes for pageble content. Any box that has more to display and is not freely scrollable but does have
	// autowaitkey enabled, will scroll ahead by a page, swallowing the keystroke.
	if BoxHub.CheckPageableBoxes then exit;

	// Resume any fibers in waitkey state.
	if FiberHub.ClearWaitKeys then exit;

	with Eventmatic do begin
		// If a normal interrupt is defined and an interrupt hasn't happened yet in this frame, trigger it.
		if (interruptLabel <> '') and (NOT hasTriggeredInterrupt) then begin
			hasTriggeredInterrupt := TRUE;
			TFiber.Create(interruptLabel, '');
			exit;
		end;

		// If esc-interrupt is defined and an interrupt hasn't happened yet in this frame, trigger it.
		if (escInterruptLabel <> '') and (NOT hasTriggeredInterrupt) then begin
			hasTriggeredInterrupt := TRUE;
			TFiber.Create(escInterruptLabel, '');
			exit;
		end;
	end;

	// If metastate is not normal, return to previous metastate.
	if sysvar.metaState <> EMetaState.Normal then HubStack.Pop;
end;

procedure UserInput_GamepadMenu;
begin
	// If skip seen text mode is enabled, disable it.
	if StopSkipping then exit;

	// If textboxes are hidden, make them visible.
	if BoxHub.allBoxesHidden then begin
		BoxHub.HideAllBoxes(FALSE);
		exit;
	end;

	// If the dropdown console is visible and in log mode, slide out the box.
	if sysvar.dropdownConsoleState = EDDCState.Log then begin
		UserInput_CtrlT;
		exit;
	end;

	// If esc-interrupt is defined and an interrupt hasn't happened yet in this frame, trigger it.
	with Eventmatic do if (escInterruptLabel <> '') and (NOT hasTriggeredInterrupt) then begin
		hasTriggeredInterrupt := TRUE;
		TFiber.Create(escInterruptLabel, '');
		exit;
	end;

	// If choicematic is active, and user-cancellable at current level, cancel it.
	with Choicematic do if isActive then
		if ((selectedNode = 0) and (userCanCancel and 1 <> 0))
		or ((selectedNode > 0) and (userCanCancel and 2 <> 0))
		then begin
			Deactivate(TRUE, clearOnDeactivate);
			exit;
		end;

	// If metaState is normal, enter the metamenu.
	if sysvar.metaState = EMetaState.Normal then SummonMetaMenu;
end;

procedure UserInput_GamepadButtonDown(bnum : TSDL_GameControllerButton);
begin
	case bnum of
		SDL_CONTROLLER_BUTTON_DPAD_UP:
		begin
			sysvar.keyRepeatAfterMsecs := 400;
			Include(sysvar.keysdown, EArrows.Up);
			UserInput_Up(FALSE);
		end;

		SDL_CONTROLLER_BUTTON_DPAD_DOWN:
		begin
			sysvar.keyRepeatAfterMsecs := 400;
			Include(sysvar.keysdown, EArrows.Down);
			UserInput_Down(FALSE);
		end;

		SDL_CONTROLLER_BUTTON_DPAD_LEFT:
		begin
			sysvar.keyRepeatAfterMsecs := 400;
			Include(sysvar.keysdown, EArrows.Left);
			UserInput_Left(FALSE);
		end;

		SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
		begin
			sysvar.keyRepeatAfterMsecs := 400;
			Include(sysvar.keysdown, EArrows.Right);
			UserInput_Right(FALSE);
		end;

		SDL_CONTROLLER_BUTTON_BACK: UserInput_CtrlB;

		// button A/cross: low position
		SDL_CONTROLLER_BUTTON_A: UserInput_Enter;
		// button B/circle: right position
		SDL_CONTROLLER_BUTTON_B: UserInput_GamepadCancel;
		// button Y/triangle: top position
		SDL_CONTROLLER_BUTTON_Y: UserInput_GamepadMenu;
		// button X/square: left position
		SDL_CONTROLLER_BUTTON_X: UserInput_CtrlT;
	end;
end;

procedure UserInput_GamepadButtonUp(bnum : TSDL_GameControllerButton);
begin
	with sysvar do case bnum of
		SDL_CONTROLLER_BUTTON_DPAD_UP: Exclude(keysdown, EArrows.Up);
		SDL_CONTROLLER_BUTTON_DPAD_DOWN: Exclude(keysdown, EArrows.Down);
		SDL_CONTROLLER_BUTTON_DPAD_LEFT: Exclude(keysdown, EArrows.Left);
		SDL_CONTROLLER_BUTTON_DPAD_RIGHT: Exclude(keysdown, EArrows.Right);
	end;
end;

procedure UserInput_GamepadAxis(anum : TSDL_GameControllerAxis; value : longint);
begin
	if abs(value) < preference.ctrlDeadZone then value := 0;
	// Stick ranges: -32768..32767, where negatives are up/left.
	with sysvar do case anum of
		SDL_CONTROLLER_AXIS_LEFTX: gamepadLeftStick.x := value;
		SDL_CONTROLLER_AXIS_LEFTY: gamepadLeftStick.y := value;
		SDL_CONTROLLER_AXIS_RIGHTX: gamepadRightStick.x := value;
		SDL_CONTROLLER_AXIS_RIGHTY: gamepadRightStick.y := value;
	end;
end;

procedure UserInput_GamepadLeftStick(tickcount : dword);
var repeatdelay : dword;
begin
	with sysvar do begin
		if (gamepadLeftStick.x or gamepadLeftStick.y) = 0 then begin
			stickRepeatAfterMsecs := 0;
			exit;
		end;

		// If textboxes are hidden, ignore.
		if BoxHub.allBoxesHidden then exit;

		// Some stick actions force a brief cooldown.
		repeatdelay := 400;
		if stickRepeatAfterMsecs <> 0 then begin
			dec(stickRepeatAfterMsecs, tickcount);
			if stickRepeatAfterMsecs > 0 then exit;
			stickRepeatAfterMsecs := 0;
			repeatdelay := 160;
		end;

		with Choicematic do if (isActive) and (choiceBox <> NIL) then
			with choiceBox do if (boxState <> EBoxState.Null) and (NOT isHidden) then begin
				// Regardless of choice columns, if the stick points up/down, move highlight.
				if abs(gamepadLeftStick.x) <= abs(gamepadLeftStick.y) then begin
					stickRepeatAfterMsecs := repeatdelay;
					if gamepadLeftStick.y >= 0 then
						MoveHighlightDown
					else
						MoveHighlightUp;
					exit;
				end
				// If more than one choice column present and stick points sideways, move highlight.
				else if contentWin.columnCount > 1 then begin
					stickRepeatAfterMsecs := repeatdelay;
					if gamepadLeftStick.x >= 0 then
						MoveHighlightRight
					else
						MoveHighlightLeft;
					exit;
				end;
			end;

		// Move to closest mouseoverable in pointed direction.
		if MoveToMouseoverable(gamepadLeftStick.x, gamepadLeftStick.y) then begin
			stickRepeatAfterMsecs := repeatdelay;
			exit;
		end;

		// If a fiber polled the stick position in the previous frame, stop here; it's obviously controlling something.
		if keysPolled then exit;

		// Scroll topmost scrollable box.
		BoxHub.ScrollUsingStick(gamepadLeftStick.y, tickcount);
	end;
end;

procedure UserInput_GamepadRightStick(tickcount : dword);
begin
	if sysvar.gamepadRightStick.y = 0 then exit; // actions only defined for y-axis
	// If textboxes are hidden, ignore.
	if BoxHub.allBoxesHidden then exit;

	// Scroll topmost scrollable box.
	BoxHub.ScrollUsingStick(sysvar.gamepadRightStick.y, tickcount);
end;
{$endif !sakucon}

// ------------------------------------------------------------------

procedure UserInput_Auto;
// This special user input handler generates automatic keypresses depending on current game state, for skipping seen
// text and for autotesting.
var i, o : dword;
	stash : ESkipState;
	auto : boolean;
begin
	o := 0; // silence compiler
	auto := sysvar.autoChoice;
	stash := sysvar.skipText;
	sysvar.skipText := ESkipState.No;

	if auto then with Eventmatic do begin
		o := dword(length(eventArea)) + numElementEvents;
		if o <> 0 then MoveToMouseoverable(random(17) - 8, random(17) - 8);
	end;

	i := FiberHub.fiberCount;
	while i <> 0 do begin
		dec(i);
		with FiberHub.fiber[i] do begin
			case fiberState of
				EFiberState.WaitKey, EFiberState.WaitClear: UserInput_Enter;
				EFiberState.WaitSignal:
				if NOT auto then
					stash := ESkipState.No
				else if (o <> 0) and (random(2) = 0) then
					UserInput_Enter;

				EFiberState.WaitChoice:
				if auto then begin
					if labelName.StartsWith(mainLabelName) then
						UserInput_Enter
					else
						if random(2) = 0 then
							UserInput_Enter
						else with Choicematic do
							HighlightChoice(random(choiceBox.content.choiceCount));
				end
				else stash := ESkipState.No;
			end;
		end;
	end;

	sysvar.autoChoice := auto;
	sysvar.skipText := stash;
end;

