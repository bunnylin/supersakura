{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function TFontmatic.GetFont(namepath : UTF8string; lang : UTF8string; heightp : dword) : TFont;
{$ifndef sakucon}
var foundpath, txt : UTF8string;
	bunch : TStringBunch;
	h : pointer;
	i, j : dword;
	minx, maxx : longint;

	function _NewSize(index : dword) : TFont;
	begin
		with font[index] do begin
			Assert(path <> '');
			h := TTF_OpenFont(@path[1], heightp);
			if h = NIL then begin
				LogError(strcat('Failed to open font % at size %: %', [path, heightp, TTF_GetError]));
				// Failed, so return the first loaded font as an emergency fallback.
				if length(font[0].instances) <> 0 then exit(font[0].instances[0]);
				raise Exception.Create('Total font failure');
			end;

			setlength(instances, length(instances) + 1);
			result := TFont.Create;
			instances[high(instances)] := result;
			with result do begin
				sdlHandle := h;
				requestedHeightP := heightp;
				fontHeightP := TTF_FontHeight(h);
				if fontHeightP > heightp then
					extraLinespaceP := fontHeightP - heightp
				else
					extraLinespaceP := 0;
				// Lineskip is sometimes greater than font height, sometimes less. If it is less, rows overlap each
				// other, a rendering nuisance. Ignore this value, we'll use a script-customisable linespacing instead.
				//skip := TTF_FontLineSkip(h);
				minx := 0; maxx := 0;
				if TTF_GlyphMetrics(h, ord('x'), @minx, @maxx, NIL, NIL, NIL) = 0 then
					exWidthP := maxx - minx
				else
					exWidthP := fontHeightP;
				log(strcat('Loaded font % (%) %px -> % %px, ex-width %..%',
					[namepath, lang, heightp, TTF_FontFaceFamilyName(h), fontHeightP, minx, maxx]));
			end;
		end;
	end;

{$endif}
begin
	if (heightp = 0) or (heightp > 999999) then
		raise Exception.Create(strcat('bad % % font size: %', [namepath, lang, heightp]));
	namepath := lowercase(namepath);

	{$ifdef sakucon}
	// For the console port, every pixel is basically one character, so conversely font size is always 1.
	if length(font) = 0 then begin
		setlength(font, 1);
		with font[0] do begin
			setlength(instances, 1);
			instances[0] := TFont.Create;
			with instances[0] do begin
				requestedHeightP := 1;
				fontHeightP := 1;
				exWidthP := 1;
				extraLinespaceP := 0;
			end;
		end;
	end;
	exit(font[0].instances[0]);
	{$else}

	// If name/path not given, default to the user's font preference for this language.
	if namepath = '' then with preference do begin
		lang := lowercase(lang);
		if length(fonts) <> 0 then begin
			for i := 0 to high(fonts) do
				if lowercase(fonts[i].fontLang) = lang then begin
					namepath := fonts[i].fontMatch;
					break;
				end;
		end;
	end;

	// If name/path not specified by script or user, use a virtual language tag for the name and fall back to defaults.
	if namepath = '' then namepath := #1 + lang;

	// Check cache for an existing match.
	if length(font) <> 0 then for i := 0 to high(font) do with font[i] do
		if namepath = match then begin
			if length(instances) <> 0 then for j := 0 to high(instances) do
				if instances[j].requestedHeightP = heightp then
					exit(instances[j]);

			// Have the file but not an instance of the right size: add new size.
			exit(_NewSize(i));
		end;

	// A file has not been found yet for this font name/path: try to find it.
	// If name/path was specified by the script, or it's the user's font preference, try to find that exact one.
	foundpath := '';
	if namepath[1] >= ' ' then begin
		foundpath := FindFontFile(namepath);
		if foundpath = '' then LogError('Could not load font "' + namepath + '"');
	end
	else begin
		// Name/path not specified by script or user, try various likely default fonts.
		// Fonts listed first are subjectively nicer-looking. (Todo: Should perhaps review list at some point.)
		// These should all be at least somewhat common fonts. The user is free to select any other font too.

		case lang of

			'english': bunch := [
				// serifed
				'FPL Neu*', 'Book Antiqua*', 'Palatino*', 'DejaVu*Serif*',
				// sans
				'Tahoma*', 'Liberation*Sans*', 'Droid*Sans*', 'Roboto*', 'Noto*Sans*',
				// basic
				'Arial*', 'Courier*'];

			'japanese': bunch := [
				'Yu*Min*', 'Hana*Min*', 'IPA*Mincho*',
				'Source*Han*Serif*JP', 'Noto*Sans*CJK*JP*', 'Meiryo*', 'MS*Gothic*'];

			else bunch := NIL;

		end;
		for txt in bunch do begin
			foundpath := FindFontFile(txt);
			if foundpath <> '' then break;
		end;
		if foundpath = '' then LogError('Could not load any default font');
	end;

	if foundpath = '' then begin
		// Failed, return the first loaded font as an emergency fallback if possible.
		if (length(font) = 0) or (length(font[0].instances) = 0) then
			raise Exception.Create('Total font failure');
		foundpath := font[0].path;
	end;
	setlength(font, length(font) + 1);
	with font[high(font)] do begin
		match := namepath;
		path := foundpath;
	end;
	result := _NewSize(high(font));
	{$endif}
end;

{$ifndef sakucon}
function TFontmatic.FindFontFile(namepath : UTF8string) : UTF8string;
// Attempts to locate a .ttf/otf or other such file for the given font match. Returns the exact font path if found,
// otherwise an empty string.
var sysfontdir : TStringBunch = NIL;
	i : dword;
	b : TStringBunch;
	filep, namep : ^char;
	{$ifdef WINDOWS}
	j, l, res : longint;
	key : HKEY;
	{$else}
	readp : ^char;
	a : ansistring;
	{$endif}

	procedure _ScanDir(dirnamu : UTF8string; const filter : UTF8string);
	var foundlist : TStringBunch;
		f : UTF8string;
	begin
		foundlist := FindFiles_caseless(dirnamu + filter, FALSE, FALSE, TRUE); // onlynames
		for f in foundlist do
			case lowercase(ExtractFileExt(f)) of
				'.ttf', '.otf', '.otb', '.ttc', '.fon':
				begin
					log('found ' + f);
					if (result = '') or (upcase(f) < upcase(result)) then begin
						result := dirnamu + f;
						// Not breaking immediately because want the best match, if pattern matches multiple files.
					end;
				end;
			end;

		// Also check sub-directories.
		foundlist := FindFiles_caseless(dirnamu + '*', TRUE);
		for f in foundlist do begin
			if ExtractFileName(f)[1] <> '.' then
				_ScanDir(f + DirectorySeparator, filter);
		end;
	end;

begin
	Assert(namepath <> '');
	log('Trying to match font: ' + namepath);

	// First, assume we're looking for a face name. Query the operating system for known fonts.
	{$ifdef WINDOWS}
	// Windows fonts should be in the registry.
	//'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts'
	// Unprivileged fonts win10 onward are in HKCU, same folder.
	getmem(namep, 256);
	getmem(filep, 259);
	key := 0;
	res := dword(RegOpenKey(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts', key));
	try
		if res <> 0 then
			log('Failed to open fonts list in HKLM: ' + strhex(res))
		else begin
			i := 0;
			repeat
				// Get key name+value in one call, fetching value separately would be a pain without RegGetValue.
				j := 255; l := 258; // secret +1 space for null
				res := RegEnumValue(key, i, namep, @j, NIL, NIL, @filep[0], @l);
				if j <> 0 then namep[j] := #0;
				if res <> 0 then begin
					if res = ERROR_NO_MORE_ITEMS then break;
					log('Failed to enumerate fonts from HKLM: ' + strhex(res));
				end
				else begin
					if WildcardMatch(namepath, PChar(namep)) then begin
						if l <> 0 then filep[l] := #0;
						namepath := PChar(filep);
						log('Match, now find ' + namepath);
						break;
					end;
				end;
				inc(i);
			until FALSE;
		end;
	finally
		freemem(namep); namep := NIL;
		freemem(filep); filep := NIL;
		RegCloseKey(key);
	end;
	{$else}
	// Fontconfig-match returns results delimited by newlines, in the form: filename.ttf: "Face name" "Style".
	// It seems to return the "Regular" style ones higher in the list, which is what we want.
	// Let's use a custom format to find the correct file without having to scan directories.
	RunCommand('fc-match', ['-a', '--format', '%{family}\t%{file}\n'], a);

	if a <> '' then begin
		readp := @a[1];
		namep := readp;
		filep := readp;
		repeat
			case readp^ of
				#0: break;
				#9: begin
					readp^ := #0;
					filep := readp + 1;
				end;
				#$A: begin
					readp^ := #0;
					if WildcardMatch(namepath, PChar(namep)) then begin
						result := PChar(filep);
						log('Got it, using ' + result);
						exit;
					end;
					namep := readp + 1;
				end;
			end;
			inc(readp);
		until FALSE;
	end;
	{$endif}

	// If not found, assume namepath was a filename to begin with.
	// Is it a full path?
	b := FindFiles_caseless(namepath, FALSE, TRUE, FALSE);
	if b.Length <> 0 then exit(b[0]);

	// Look for the filename in usual font locations.
	{$ifdef WINDOWS}
		setlength(sysfontdir, 2);
		sysfontdir[0] := GetEnvironmentVariable('SystemRoot');
		if sysfontdir[0] = '' then sysfontdir[0] := GetEnvironmentVariable('windir');
		if sysfontdir[0] = '' then sysfontdir[0] := 'C:\WINDOWS';
		sysfontdir[0] := sysfontdir[0] + '\Fonts\';
		// On new windowses, unprivileged fonts install to local appdata.
		sysfontdir[1] := GetEnvironmentVariable('LOCALAPPDATA') + '\Microsoft\Windows\Fonts\';
	{$else}
		setlength(sysfontdir, 4);
		sysfontdir[3] := '~/.local/share/fonts/';
		sysfontdir[2] := '~/.fonts/';
		sysfontdir[1] := '/usr/local/share/fonts/';
		sysfontdir[0] := '/usr/share/fonts/';
	{$endif}
	result := '';
	for i := high(sysfontdir) downto 0 do begin
		log('Looking in ' + sysfontdir[i]);
		_ScanDir(sysfontdir[i], namepath);
		if result <> '' then begin
			log('Using ' + result);
			exit;
		end;
	end;
	log('No match');
end;
{$endif !sakucon}

procedure TFontmatic.Reset;
var f : TFont;
	i : dword;
begin
	if length(font) = 0 then exit;
	for i := high(font) downto 0 do with font[i] do
		if length(instances) <> 0 then begin
			for f in instances do begin
				{$ifndef sakucon}
				TTF_CloseFont(f.sdlHandle);
				{$endif}
				f.Destroy;
			end;
			setlength(instances, 0);
		end;
	setlength(font, 0);
end;

