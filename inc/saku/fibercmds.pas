{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Note: When fetching parameters, you have to put them in temporary variables first if there are more than one!
// Parameter order matters for dynamic parameters in script code, so you have to fetch them in the right order.
// Don't fetch them directly as parameters of further function calls; FPC doesn't resolve them left-to-right, so it
// gets confusing.
// For minor errors, use Log; for major errors, use _FiberError(txt, false); for criticals, use _FiberError(txt, true).
// After any call to _FiberError you must exit the invoke code immediately so the meta error box can take over.

procedure Invoke_CALL; inline;
var _labelname, _fibername : TStringBunch;
	i : dword;
begin
	if NOT FetchParam(CMDP_LABEL, _labelname) then begin
		_FiberError('call without label name', TRUE);
		exit;
	end;
	_labelname[labelObject.labelLanguage] := upcase(_labelname[labelObject.labelLanguage]);

	if NOT FetchParam(CMDP_NAME, _fibername) then
		ScriptCall(_labelname[labelObject.labelLanguage])
	else begin
		i := FiberHub.GetFiber(upcase(_fibername[labelObject.labelLanguage]));
		if i >= FiberHub.fiberCount then
			_FiberError('call with bad fiber name: ' + _fibername[labelObject.labelLanguage], TRUE)
		else
			FiberHub.fiber[i].ScriptCall(_labelname[labelObject.labelLanguage]);
	end;
end;

procedure Invoke_CASECALL; inline;
var _labelname : TStringBunch = NIL;
	i : longint;
begin
	i := FetchParamOrDefault(CMDP_INDEX, 0);
	if i < 0 then begin
		_FiberError('casegoto with index ' + strdec(i));
		exit;
	end;
	with labelObject do begin
		if NOT FetchParam(CMDP_LABEL, _labelname) or (_labelname[labelLanguage] = '') then begin
			_FiberError('casecall without label names', TRUE);
			exit;
		end;

		_labelname[labelLanguage] := upcase(_labelname[labelLanguage]);
		ScriptCase(_labelname[labelLanguage].Split(':', FALSE), i, TRUE);
	end;
end;

procedure Invoke_CASEGOTO; inline;
var _labelname : TStringBunch = NIL;
	i : longint;
begin
	i := FetchParamOrDefault(CMDP_INDEX, 0);
	if i < 0 then begin
		_FiberError('casegoto with index ' + strdec(i));
		exit;
	end;
	with labelObject do begin
		if NOT FetchParam(CMDP_LABEL, _labelname) or (_labelname[labelLanguage] = '') then begin
			_FiberError('casegoto without label names', TRUE);
			exit;
		end;

		_labelname[labelLanguage] := upcase(_labelname[labelLanguage]);
		ScriptCase(_labelname[labelLanguage].Split(':', FALSE), i, FALSE);
	end;
end;

procedure Invoke_CHOICE_INIT; // not inlined, shared by call/get/goto
var _startparent : UTF8String;
	noclear, noprint, nowait : longint;
begin
	_startparent := upcase(FetchParamOrDefault(CMDP_PARENT, '')[labelObject.labelLanguage]);
	while (_startparent <> '') and (_startparent[_startparent.Length] = ':') do // trim extra colons from end
		setlength(_startparent, _startparent.Length - 1);
	noclear := FetchParamOrDefault(CMDP_NOCLEAR, 0);
	noprint := FetchParamOrDefault(CMDP_NOPRINT, 0);
	nowait := FetchParamOrDefault(CMDP_NOWAIT, 0);

	Choicematic.Activate(self, _startparent, noclear = 0, noprint = 0, nowait = 0);
	yieldNow := TRUE;
end;

procedure Invoke_CHOICE_CANCEL; inline;
var noclear : longint;
begin
	noclear := FetchParamOrDefault(CMDP_NOCLEAR, 0);
	Choicematic.Deactivate(TRUE, noclear = 0);
end;

procedure Invoke_CHOICE_GETHIGHLIGHT; inline;
begin
	PushInt(Choicematic.highlightIndex);
	PushInt(EStackToken.Number);
end;

procedure Invoke_CHOICE_LIST; inline;
begin
	log(Choicematic.StateToString);
end;

procedure Invoke_CHOICE_OFF; inline;
begin
	Choicematic.LockChoice(FetchParamOrDefault(CMDP_TEXT, '')[labelObject.labelLanguage], FALSE);
end;

procedure Invoke_CHOICE_ON; inline;
var _choicetxt : UTF8string;
	i : longint;
begin
	_choicetxt := FetchParamOrDefault(CMDP_TEXT, '')[labelObject.labelLanguage];
	i := FetchParamOrDefault(CMDP_VALUE, 1);
	Choicematic.LockChoice(_choicetxt, i <> 0);
end;

procedure Invoke_CHOICE_REMOVE; inline;
begin
	Choicematic.RemoveChoice(FetchParamOrDefault(CMDP_TEXT, '')[labelObject.labelLanguage]);
end;

procedure Invoke_CHOICE_RESET; inline;
begin
	Choicematic.ResetChoices;
end;

procedure Invoke_CHOICE_SET; inline;
var i, lockvalue : longint;
	txt : TStringBunch;
	_labelname, _varname : UTF8string;
begin
	i := FetchParamOrDefault(CMDP_INDEX, -1);

	if NOT FetchParam(CMDP_TEXT, txt) then begin
		_FiberError('choice.set without text');
		exit;
	end;

	_labelname := upcase(FetchParamOrDefault(CMDP_LABEL, '')[labelObject.labelLanguage]);
	_varname := FetchParamOrDefault(CMDP_VAR, '')[labelObject.labelLanguage];
	lockvalue := FetchParamOrDefault(CMDP_VALUE, 0);

	Choicematic.AddChoice(txt, i, _labelname, _varname, lockvalue <> 0, labelObject.labelLanguage);
end;

procedure Invoke_ERROR; inline;
var txt : TStringBunch;
	i : longint = 0;
begin
	if FetchParam(CMDP_TEXT, txt) then
		_FiberError(txt[labelObject.labelLanguage])
	else if FetchParam(CMDP_VALUE, i) then
		_FiberError(strdec(i))
	else
		_FiberError('error');
end;

procedure Invoke_EVENT_CREATE_TIMER; inline;
var _eventname : TStringBunch;
	destlabel : UTF8string;
	freq, starttime : longint;
begin
	if NOT FetchParam(CMDP_NAME, _eventname) then begin
		_FiberError('event.create timer without name');
		exit;
	end;
	_eventname[labelObject.labelLanguage] := upcase(_eventname[labelObject.labelLanguage]);

	destlabel := FetchParamOrDefault(CMDP_LABEL, '')[labelObject.labelLanguage];
	destlabel := ExpandRelativeLabel(upcase(destlabel));
	freq := FetchParamOrDefault(CMDP_FREQ, 1000);
	starttime := FetchParamOrDefault(CMDP_TIME, 0);
	Eventmatic.AddTimerEvent(_eventname[labelObject.labelLanguage], freq, destlabel, starttime);
end;

procedure Invoke_EVENT_GETTIMER; inline;
var _eventname : TStringBunch;
	i : dword;
begin
	if NOT FetchParam(CMDP_NAME, _eventname) then begin
		_FiberError('gettimer without event name');
		exit;
	end;
	_eventname[labelObject.labelLanguage] := upcase(_eventname[labelObject.labelLanguage]);

	with Eventmatic do
		if length(timerEvent) <> 0 then for i := high(timerEvent) downto 0 do
			if timerEvent[i].eventName = _eventname[labelObject.labelLanguage] then begin
				PushInt(longint(timerEvent[i].timerCounter));
				PushInt(EStackToken.Number);
				exit;
			end;
	PushInt(-1);
	PushInt(EStackToken.Number);
end;

procedure Invoke_EVENT_REMOVE; inline;
var _eventname : UTF8string;
begin
	_eventname := upcase(FetchParamOrDefault(CMDP_NAME, '')[labelObject.labelLanguage]);
	if _eventname = '' then begin
		_eventname := upcase(FetchParamOrDefault(CMDP_GOB, '')[labelObject.labelLanguage]);
		if _eventname = '' then
			Eventmatic.Reset
		else
			Eventmatic.Remove(_eventname, TRUE, FALSE);
	end
	else
		Eventmatic.Remove(_eventname);
end;

procedure Invoke_EVENT_SETLABEL; inline;
var _eventname, _labelname, mouseon, mouseoff : TStringBunch;
	_ele : TElement;
	i, _value : longint;
	elementonly, _mouseonly : boolean;
	hastrig, hason, hasoff, hasmouso, hasvalue : boolean;
begin
	elementonly := FALSE;
	if NOT FetchParam(CMDP_NAME, _eventname) then begin
		elementonly := TRUE;
		if NOT FetchParam(CMDP_GOB, _eventname) then begin
			_FiberError('setlabel without event name');
			exit;
		end;
	end;
	_eventname[0] := upcase(_eventname[labelObject.labelLanguage]);
	_ele := EleHub.GetElement(_eventname[0]);
	if (elementonly) and (_ele = NIL) then begin
		_FiberError('setlabel: no such element: ' + _eventname[0]);
		exit;
	end;

	hastrig := FetchParam(CMDP_LABEL, _labelname);
	hason := FetchParam(CMDP_MOUSEON, mouseon);
	hasoff := FetchParam(CMDP_MOUSEOFF, mouseoff);
	hasmouso := FetchParam(CMDP_MOUSEONLY, i);
	_mouseonly := i <> 0;
	hasvalue := FetchParam(CMDP_VALUE, _value);
	if _value <> 0 then _value := 1;
	if NOT (hastrig or hason or hasoff or hasmouso or hasvalue) then exit;

	with labelObject do begin
		if hastrig then _labelname[labelLanguage] := ExpandRelativeLabel(upcase(_labelname[labelLanguage]));
		if hason then mouseon[labelLanguage] := ExpandRelativeLabel(upcase(mouseon[labelLanguage]));
		if hasoff then mouseoff[labelLanguage] := ExpandRelativeLabel(upcase(mouseoff[labelLanguage]));
	end;

	with Eventmatic do begin
		if (NOT elementonly) and (length(eventArea) <> 0) then for i := high(eventArea) downto 0 do
			if eventArea[i].eventName = _eventname[0] then begin
				if hastrig then eventArea[i].triggerLabel := _labelname[labelObject.labelLanguage];
				if hason then eventArea[i].mouseOnLabel := mouseon[labelObject.labelLanguage];
				if hasoff then eventArea[i].mouseOffLabel := mouseoff[labelObject.labelLanguage];
				if hasmouso then eventArea[i].mouseOnly := _mouseonly;
				if hasvalue then eventArea[i].eventState := _value;
			end;

		if _ele <> NIL then with FindOrCreateElementEvent(_ele)^ do begin
			if hastrig then triggerLabel := _labelname[labelObject.labelLanguage];
			if hason then mouseOnLabel := mouseon[labelObject.labelLanguage];
			if hasoff then mouseOffLabel := mouseoff[labelObject.labelLanguage];
			if hasmouso then mouseOnly := _mouseonly;
			if hasvalue then eventState := _value;

			// Should auto-destroy element event if all labels wiped? But maybe script wants to reuse it later.
			{if (triggerLabel = '') and (mouseOnLabel = '') and (mouseOffLabel = '') then
				Remove(_eventname[0], TRUE, FALSE);}
		end;

		if (numElementEvents <> 0) then for i := numElementEvents - 1 downto 0 do
			with elementEvent[i] do if eventName = _eventname[0] then begin
				if hastrig then triggerLabel := _labelname[labelObject.labelLanguage];
				if hason then mouseOnLabel := mouseon[labelObject.labelLanguage];
				if hasoff then mouseOffLabel := mouseoff[labelObject.labelLanguage];
				if hasmouso then mouseOnly := _mouseonly;
				if hasvalue then eventState := _value;
			end;

		if (hastrig) and (NOT elementonly) and (length(timerEvent) <> 0) then for i := high(timerEvent) downto 0 do
			with timerEvent[i] do if eventName = _eventname[0] then
				triggerLabel := _labelname[labelObject.labelLanguage];
	end;
end;

procedure Invoke_EVENT_SETTIMER; inline;
var _eventname : TStringBunch;
	i : dword;
begin
	if NOT FetchParam(CMDP_NAME, _eventname) then begin
		_FiberError('settimer without event name');
		exit;
	end;
	_eventname[labelObject.labelLanguage] := upcase(_eventname[labelObject.labelLanguage]);

	with Eventmatic do
		if length(timerEvent) <> 0 then for i := high(timerEvent) downto 0 do
			if timerEvent[i].eventName = _eventname[labelObject.labelLanguage] then
				timerEvent[i].timerCounter := FetchParamOrDefault(CMDP_TIME, 0);
end;

procedure Invoke_FIBER_CALLSTACK; inline;
begin
	FiberHub.LogCallstack(upcase(FetchParamOrDefault(CMDP_NAME, 'MAIN')[labelObject.labelLanguage]));
end;

procedure Invoke_FIBER_GETNAME; inline;
begin
	PushString(fiberName);
	PushInt(EStackToken.SingleStr);
end;

procedure Invoke_FIBER_LIST; inline;
var _fibername : TStringBunch;
	i : dword;
begin
	with FiberHub do if fiberCount <> 0 then begin
		if NOT FetchParam(CMDP_NAME, _fibername) then
			for i := 0 to fiberCount - 1 do log(fiber[i].ToString)
		else begin
			_fibername[labelObject.labelLanguage] := upcase(_fibername[labelObject.labelLanguage]);
			for i := 0 to fiberCount - 1 do
				if fiber[i].fiberName = _fibername[labelObject.labelLanguage] then log(fiber[i].ToString);
		end;
	end;
end;

procedure Invoke_FIBER_SIGNAL; inline;
begin
	FiberHub.SignalFibers(FetchParamOrDefault(CMDP_NAME, '')[labelObject.labelLanguage], []);
end;

procedure Invoke_FIBER_START; inline;
var _labelname : TStringBunch;
	_fibername : UTF8string;
begin
	if NOT FetchParam(CMDP_LABEL, _labelname) then begin
		_FiberError('fiber.start without label name');
		exit;
	end;

	with labelObject do begin
		_fibername := FetchParamOrDefault(CMDP_NAME, '')[labelLanguage];
		if _fibername = '' then _fibername := _labelname[labelLanguage];
		_labelname[labelLanguage] := ExpandRelativeLabel(upcase(_labelname[labelLanguage]));
		TFiber.Create(_labelname[labelLanguage], _fibername);
	end;
end;

procedure Invoke_FIBER_STOP; inline;
var _fibername : TStringBunch;
begin
	// Stop all fibers by this name, or all fibers if name is empty; or stop self if there's no name parameter.
	if FetchParam(CMDP_NAME, _fibername) then
		FiberHub.StopFibers(_fibername[labelObject.labelLanguage])
	else
		fiberState := EFiberState.Stopping;
	if fiberState = EFiberState.Stopping then yieldNow := TRUE;
end;

procedure Invoke_FIBER_WAIT; inline;
var _fiber : TFiber;
	_fibername : TStringBunch;
	time : longint;
	i : dword;
begin
	_fiber := self;
	if FetchParam(CMDP_NAME, _fibername) then begin
		i := FiberHub.GetFiber(upcase(_fibername[labelObject.labelLanguage]));
		if i >= FiberHub.fiberCount then begin
			_FiberError('fiber.wait no such fiber: ' + _fibername[labelObject.labelLanguage]);
			exit;
		end;
		_fiber := FiberHub.fiber[i];
	end;

	time := FetchParamOrDefault(CMDP_TIME, 0);
	// If a time was specified and non-zero, put the fiber to sleep for so long. (Negative time = infinite sleep)
	// For a zero-wait, wait for all of this fiber's effects to expire. (And yield even if no active effects.)
	if time <> 0 then
		TEffectSleep.Create(_fiber, time)
	else
		if _fiber.fxRefCount <> 0 then _fiber.fiberState := EFiberState.WaitFx;
	if _fiber = self then yieldNow := TRUE;
end;

procedure Invoke_FIBER_WAITKEY; inline;
begin
	fiberState := EFiberState.WaitKey;
	BoxHub.delayedClear := FetchParamOrDefault(CMDP_NOCLEAR, 0) = 0;
	yieldNow := TRUE;
end;

procedure Invoke_FIBER_WAITSIG; inline;
begin
	fiberState := EFiberState.WaitSignal;
	yieldNow := TRUE;
end;

procedure Invoke_FIBER_WAITTYPING; inline;
begin
	fiberstate := EFiberState.WaitTyping;
	yieldNow := TRUE;
end;

procedure Invoke_FIBER_YIELD; inline;
begin
	yieldNow := TRUE;
end;

procedure Invoke_FX_SKIP; inline;
begin
	EffectHub.RemoveTimed;
end;

procedure Invoke_GFX_ADOPT; inline;
var _elename : TStringBunch;
	_ele : TElement;
begin
	if NOT FetchParam(CMDP_GOB, _elename) then begin
		_FiberError('gfx.adopt without element');
		exit;
	end;

	with EleHub do begin
		_ele := GetElement(upcase(_elename[labelObject.labelLanguage]));
		if _ele = NIL then begin
			_FiberError('gfx.adopt no such element: ' + _elename[labelObject.labelLanguage]);
			exit;
		end;
		_ele.SetParent(upcase(FetchParamOrDefault(CMDP_PARENT, '')[labelObject.labelLanguage]));
	end;
end;

procedure Invoke_GFX_BASH; inline;
var _elename : TStringBunch;
	_ele : TElement;
	time, freq, by, angle : longint;
	i : dword;
begin
	if FetchParam(CMDP_GOB, _elename) then
		_ele := EleHub.GetElement(upcase(_elename[labelObject.labelLanguage]))
	else begin
		i := Viewportmatic.viewport[EleHub.defaultViewport].GetBackgroundIndex;
		_ele := NIL;
		if i < EleHub.numElements then _ele := EleHub.ele[i];
	end;
	if _ele = NIL then begin
		_FiberError('gfx.bash no such element: ' + _elename[labelObject.labelLanguage]);
		exit;
	end;

	time := FetchParamOrDefault(CMDP_TIME, 1024);
	freq := FetchParamOrDefault(CMDP_FREQ, 32768);
	by := FetchParamOrDefault(CMDP_BY, 8192);
	angle := FetchParamOrDefault(CMDP_ANGLE, random(32768));

	TEffectBash.Create(self, time, _ele, angle, freq, by);
end;

procedure Invoke_GFX_CLEARALL; inline;
// Remove all elements in the viewport.
var i, vp : dword;
begin
	with EleHub do if numElements <> 0 then begin
		vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, defaultViewport));
		for i := numElements - 1 downto 0 do with ele[i] do
			if (vp >= dword(length(Viewportmatic.viewport))) or (eleViewport = vp) then
				Destroy;
	end;
end;

procedure Invoke_GFX_CLEARANIMS; inline;
// Remove all actively animating elements in the viewport.
var i, vp : dword;
begin
	with EleHub do if numElements <> 0 then begin
		vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, defaultViewport));
		for i := numElements - 1 downto 0 do if ele[i] is TGob then with ele[i] as TGob do
			if (gobType = EGobType.Anim)
			and ((vp >= dword(length(Viewportmatic.viewport))) or (eleViewport = vp)) then
				Destroy;
	end;
end;

procedure Invoke_GFX_CLEARBKG; inline;
// Remove the background in the viewport.
var i, vp : dword;
begin
	with EleHub do with Viewportmatic do if (numElements <> 0) and (length(viewport) <> 0) then begin
		vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, defaultViewport));
		if vp < dword(length(viewport)) then begin
			i := viewport[vp].GetBackgroundIndex;
			if i < numElements then ele[i].Destroy;
		end
		else for i := numelements - 1 downto 0 do
			if ele[i] is TGob then with ele[i] as TGob do
				if gobType = EGobType.Bkg then Destroy;
	end;
end;

procedure Invoke_GFX_CLEARKIDS; inline;
// Remove all children of the given element, or all children of the viewport's background if no element specified.
var _elename : TStringBunch;
	i, vp, parent : dword;
begin
	with EleHub do if numElements <> 0 then begin
		if FetchParam(CMDP_GOB, _elename) then begin
			parent := GetElementIndex(upcase(_elename[labelObject.labelLanguage]));
			if parent >= numElements then begin
				_FiberError('gfx.clearkids no such element: ' + _elename[labelObject.labelLanguage]);
				exit;
			end;
		end
		else begin
			vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, defaultViewport));
			if vp >= dword(length(Viewportmatic.viewport)) then begin
				_FiberError('gfx.clearkids bad viewport: ' + strdec(vp));
				exit;
			end;
			parent := Viewportmatic.viewport[vp].GetBackgroundIndex;
			if parent >= numElements then exit;
		end;

		for i := numElements - 1 downto parent + 1 do
			if ele[i].eleParent = ele[parent] then ele[i].Destroy;
	end;
end;

procedure Invoke_GFX_FIND; inline;
var _graphic, _name : UTF8string;
	_type : EGobType = EGobType(0); // silence compiler
	_typesb : TStringBunch = NIL;
	_viewport, i : dword;
	hastype : boolean;
begin
	_name := upcase(FetchParamOrDefault(CMDP_NAME, '')[labelObject.labelLanguage]);
	hastype := FetchParam(CMDP_TYPE, _typesb);
	if hastype then _type := EGobTypeFromString(lowercase(_typesb[labelObject.labelLanguage]));
	_graphic := upcase(FetchParamOrDefault(CMDP_GOB, '')[labelObject.labelLanguage]);
	_viewport := dword(FetchParamOrDefault(CMDP_VIEWPORT, -1));

	with EleHub do if numElements <> 0 then for i := numElements - 1 downto 0 do begin
		if ((_name = '') or (ele[i].eleName = _name))
		and ((_graphic = '') or ((ele[i] is TGob) and ((ele[i] as TGob).gfxListName = _graphic)))
		and ((NOT hastype) or ((ele[i] is TGob) and (_type = (ele[i] as TGob).gobType)))
		and ((_viewport >= dword(length(Viewportmatic.viewport))) or (_viewport = ele[i].eleViewport))
		then begin
			PushString(ele[i].eleName);
			PushInt(EStackToken.SingleStr);
			exit;
		end;
	end;
	PushInt(EStackToken.Null);
end;

procedure Invoke_GFX_FLASH; inline;
var vp, color : dword;
	count, time, z : longint;
	style : EMoveType;
begin
	count := FetchParamOrDefault(CMDP_COUNT, 1);
	time := FetchParamOrDefault(CMDP_TIME, 384);
	color := dword(FetchParamOrDefault(CMDP_COLOR, $FFFE));
	vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, EleHub.defaultViewport));
	if vp >= dword(length(Viewportmatic.viewport)) then begin
		_FiberError('gfx.flash bad viewport: ' + strdec(longint(vp)));
		exit;
	end;
	style := EMoveTypeFromString(FetchParamOrDefault(CMDP_STYLE, '1')[labelObject.labelLanguage]);
	z := FetchParamOrDefault(CMDP_ZLEVEL, 120);

	TEffectFlash.Create(self, time, count, vp, color, style, z);
end;

procedure Invoke_GFX_GETFRAME; inline;
var _elename : TStringBunch;
	_ele : TElement;
begin
	if NOT FetchParam(CMDP_GOB, _elename) then begin
		_FiberError('gfx.getframe without element');
		exit;
	end;

	_ele := EleHub.GetElement(upcase(_elename[labelObject.labelLanguage]));
	if _ele = NIL then
		_FiberError('gfx.getframe: no such element: ' + _elename[labelObject.labelLanguage])
	else if _ele is TGob then begin
		PushInt((_ele as TGob).gobFrame);
		PushInt(EStackToken.Number);
	end
	else
		_FiberError('gfx.getframe: element != bmp: ' + _ele.eleName);
end;

procedure Invoke_GFX_GETSEQUENCE; inline;
var _elename : TStringBunch;
	_ele : TElement;
begin
	if NOT FetchParam(CMDP_GOB, _elename) then begin
		_FiberError('gfx.getsequence without element');
		exit;
	end;

	_ele := EleHub.GetElement(upcase(_elename[labelObject.labelLanguage]));
	if _ele = NIL then
		_FiberError('gfx.getsequence: no such element: ' + _elename[labelObject.labelLanguage])
	else if _ele is TGob then begin
		PushInt((_ele as TGob).animSeqP);
		PushInt(EStackToken.Number);
	end
	else
		_FiberError('gfx.getsequence: element != bmp: ' + _ele.eleName);
end;

procedure Invoke_GFX_LIST; inline;
var _elename : TStringBunch;
	i : dword;
begin
	with EleHub do if numElements <> 0 then begin
		if NOT FetchParam(CMDP_GOB, _elename) then
			for i := 0 to numElements - 1 do log(ele[i].ToString)
		else begin
			_elename[labelObject.labelLanguage] := upcase(_elename[labelObject.labelLanguage]);
			for i := 0 to numElements - 1 do
				if ele[i].eleName = _elename[labelObject.labelLanguage] then Log(ele[i].ToString);
		end;
	end;
end;

procedure Invoke_GFX_MOVE; inline;
var _elename : TStringBunch;
	txt : string;
	_ele : TElement;
	newloc, newanchor : TCoord32k;
	movestyle : EMoveType;
	time : longint;
begin
	if NOT FetchParam(CMDP_GOB, _elename) then begin
		_FiberError('gfx.move without element');
		exit;
	end;

	_ele := EleHub.GetElement(upcase(_elename[labelObject.labelLanguage]));
	if _ele = NIL then begin
		_FiberError('gfx.move: no such element: ' + _elename[labelObject.labelLanguage]);
		exit;
	end;

	newloc.x := FetchParamOrDefault(CMDP_LOCX, _ele.eleLoc.x);
	newloc.y := FetchParamOrDefault(CMDP_LOCY, _ele.eleLoc.y);
	newanchor.x := FetchParamOrDefault(CMDP_ANCHORX, _ele.eleAnchor.x);
	newanchor.y := FetchParamOrDefault(CMDP_ANCHORY, _ele.eleAnchor.y);
	time := FetchParamOrDefault(CMDP_TIME, 0);
	txt := FetchParamOrDefault(CMDP_STYLE, '')[labelObject.labelLanguage];
	if (time <> 0) and (txt = '') then txt := '1'; // if non-zero time defined but no style, assume linear style
	movestyle := EMoveTypeFromString(txt);

	TEffectMove.Create(self, time, _ele, newloc.x, newloc.y, newanchor.x, newanchor.y, movestyle);
end;

procedure Invoke_GFX_PRECACHE; inline;
var _elename : TStringBunch;
	_ele : TGraphicObject;
	getsize : TSizeP;
	vp : dword;
begin
	if NOT FetchParam(CMDP_GOB, _elename) then begin
		_FiberError('gfx.precache without element');
		exit;
	end;

	vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, EleHub.defaultViewport));
	if vp >= dword(length(Viewportmatic.viewport)) then begin
		_FiberError('gfx.precache no such viewport: ' + strdec(vp));
		exit;
	end;

	_ele := GetGraphicObject(_elename[labelObject.labelLanguage], 0, 0);
	if _ele = NIL then begin
		_FiberError('gfx.precache no such graphic: ' + _elename[labelObject.labelLanguage]);
		exit;
	end;

	with _ele do with Viewportmatic.viewport[vp] do begin
		getsize.w := (origSizeX * viewportSizeP.w + 16384) shr 15;
		getsize.h := (origFrameHeight * viewportSizeP.h + 16384) shr 15;
	end;

	if GetGraphicObject(_elename[labelObject.labelLanguage], getsize.w, getsize.h) = NIL then
		_FiberError('gfx.precache cachegfx failed');
end;

procedure Invoke_GFX_REMOVE; inline;
var _elename : TStringBunch;
	i : dword;
begin
	if NOT FetchParam(CMDP_GOB, _elename) then begin
		_FiberError('gfx.remove without element');
		exit;
	end;

	with EleHub do if numElements <> 0 then begin
		_elename[labelObject.labelLanguage] := upcase(_elename[labelObject.labelLanguage]);
		for i := numElements - 1 downto 0 do
			if ele[i].eleName = _elename[labelObject.labelLanguage] then ele[i].Destroy;
	end;
end;

procedure Invoke_GFX_SETALPHA; inline;
var _elename : TStringBunch;
	_ele : TElement;
	alpha, time : longint;
begin
	if NOT FetchParam(CMDP_GOB, _elename) then begin
		_FiberError('gfx.setalpha without element');
		exit;
	end;

	_ele := EleHub.GetElement(upcase(_elename[labelObject.labelLanguage]));
	if _ele = NIL then begin
		_FiberError('gfx.setalpha: no such element: ' + _elename[labelObject.labelLanguage]);
		exit;
	end;

	alpha := FetchParamOrDefault(CMDP_ALPHA, $FF);
	time := FetchParamOrDefault(CMDP_TIME, 0);
	TEffectAlphaSlide.Create(self, time, _ele, byte(alpha));
end;

procedure Invoke_GFX_SETFRAME; inline;
var _elename : TStringBunch;
	_ele : TElement;
	frame, framecount : longint;
begin
	if NOT FetchParam(CMDP_GOB, _elename) then begin
		_FiberError('gfx.setframe without element');
		exit;
	end;

	_ele := EleHub.GetElement(upcase(_elename[labelObject.labelLanguage]));
	if _ele = NIL then begin
		_FiberError('gfx.setframe: no such element: ' + _elename[labelObject.labelLanguage]);
		exit;
	end;
	if NOT (_ele is TGob) then begin
		_FiberError('gfx.setframe: element != bmp: ' + _ele.eleName);
		exit;
	end;

	with _ele as TGob do begin
		if cachedObject = NIL then if NOT EnsureCached then begin
			_FiberError('gfx.setframe: failed to cache ' + _ele.eleName);
			exit;
		end;
		framecount := length(cachedObject.framePtr);
		frame := FetchParamOrDefault(CMDP_FRAME, 0);
		if frame >= 0 then
			gobFrame := frame mod framecount
		else
			gobFrame := framecount + ((frame + 1) mod framecount) - 1;
		// (Mod on a negative number returns the same as positive but sign-flipped.)
		RefreshFrameP;
		if eleIsVisible then AddRefresh;
	end;
end;

procedure Invoke_GFX_SETSEQUENCE; inline;
begin
	log('gfx.setsequence not implemented'); // needs new animation specs first
end;

procedure Invoke_GFX_SETSOLIDBLIT; inline;
var _elename : TStringBunch;
	_ele : TElement;
	c : RGBAquad;
begin
	if NOT FetchParam(CMDP_GOB, _elename) then begin
		_FiberError('gfx.setsolidblit without element');
		exit;
	end;

	_ele := EleHub.GetElement(upcase(_elename[labelObject.labelLanguage]));
	if _ele = NIL then begin
		_FiberError('gfx.setsolidblit: no such element: ' + _elename[labelObject.labelLanguage]);
		exit;
	end;

	c.FromRGBA4(FetchParamOrDefault(CMDP_COLOR, 0));
	_ele.SetSolidBlit(dword(c));
end;

procedure Invoke_GFX_SETSOURCE; inline;
var _elename : TStringBunch;
	_ele : TElement;
begin
	if NOT FetchParam(CMDP_GOB, _elename) then begin
		_FiberError('gfx.setsource without element');
		exit;
	end;

	_ele := EleHub.GetElement(upcase(_elename[labelObject.labelLanguage]));
	if _ele = NIL then begin
		_FiberError('gfx.setsource: no such element: ' + _elename[labelObject.labelLanguage]);
		exit;
	end;

	if NOT (_ele is TGob) then begin
		_FiberError('gfx.setsource: element not a gob: ' + _elename[labelObject.labelLanguage]);
		exit;
	end;

	with _ele as TGob do begin
		gfxListName := upcase(FetchParamOrDefault(CMDP_NAME, '')[labelObject.labelLanguage]);
		if gfxlistname = '' then gfxListName := '|0';
		runtimeDynamic := (gfxListName[1] = '|');
		loadtimedynamic := (gfxListName[1] = '>');
		if runtimeDynamic then solidBlit.FromRGBA4(valhex(gfxListName));
		log('gob ' + eleName + ' source=' + gfxListName);
		cachedObject := NIL;
		UpdateSizeP;
	end;
end;

procedure Invoke_GFX_SHOW; inline;
var n, _parent, onlabel, offlabel : TStringBunch;
	_gob : TGob;
	_gobname, t : UTF8string;
	_type : EGobType;
	gobloc, gobanchor : TCoord32k;
	frame, vp, zlevel, sizex, sizey, alpha : longint;
	hasparent, hasframe, hastrig, hason, hasoff : boolean;
begin
	if NOT FetchParam(CMDP_GOB, n) then begin
		_FiberError('gfx.show without graphic name');
		exit;
	end;

	t := FetchParamOrDefault(CMDP_TYPE, '')[labelObject.labelLanguage];
	_type := EGobTypeFromString(lowercase(t));
	if _type = EGobType.Unknown then begin
		_FiberError('gfx.show bad gob type: ' + t);
		exit;
	end;

	_gobname := upcase(FetchParamOrDefault(CMDP_NAME, '')[labelObject.labelLanguage]);
	t := '';
	hasparent := FetchParam(CMDP_PARENT, _parent);
	if hasparent then t := upcase(_parent[labelObject.labelLanguage]);
	gobloc.x := FetchParamOrDefault(CMDP_LOCX, 0);
	gobloc.y := FetchParamOrDefault(CMDP_LOCY, 0);
	hasframe := FetchParam(CMDP_FRAME, frame);
	vp := FetchParamOrDefault(CMDP_VIEWPORT, -1);
	zlevel := FetchParamOrDefault(CMDP_ZLEVEL, 0);
	gobanchor.x := FetchParamOrDefault(CMDP_ANCHORX, 0);
	gobanchor.y := FetchParamOrDefault(CMDP_ANCHORY, 0);
	sizex := FetchParamOrDefault(CMDP_SIZEX, 0);
	sizey := FetchParamOrDefault(CMDP_SIZEY, 0);

	_gob := TGob.Create(upcase(n[labelObject.labelLanguage]), _gobname, t, _type,
		vp, gobloc.x, gobloc.y, zlevel, gobanchor.x, gobanchor.y, sizex, sizey);
	if (NOT hasparent) and (_gob.gobType <> EGobType.Bkg) then _gob.AutoAdopt;
	if hasframe then begin
		_gob.gobFrame := frame;
		if NOT _gob.runtimeDynamic then _gob.RefreshFrameP;
	end;
	if FetchParam(CMDP_ALPHA, alpha) then _gob.eleAlpha := alpha;
	hastrig := FetchParam(CMDP_LABEL, n);
	hason := FetchParam(CMDP_MOUSEON, onlabel);
	hasoff := FetchParam(CMDP_MOUSEOFF, offlabel);
	if (hastrig or hason or hasoff) then with Eventmatic.CreateElementEvent(_gob)^ do begin
		if hastrig then triggerLabel := ExpandRelativeLabel(upcase(n[labelObject.labelLanguage]));
		if hason then mouseOnLabel := ExpandRelativeLabel(upcase(onlabel[labelObject.labelLanguage]));
		if hasoff then mouseOffLabel := ExpandRelativeLabel(upcase(offlabel[labelObject.labelLanguage]));
	end;
end;

procedure Invoke_GFX_TRANSITION; inline;
var _style : TStringBunch;
	i, time : longint;
	vp : dword;
	xstyle : ETransitionType;
begin
	xstyle := ETransitionType.Instant; i := 0;
	if FetchParam(CMDP_STYLE, _style) then
		xstyle := ETransitionTypeFromString(_style[labelObject.labelLanguage])

	else if FetchParam(CMDP_INDEX, i) then begin
		if dword(i) <= 4 then
			xstyle := ETransitionType(i)
		else begin
			log('[!] bad transition style: ' + strdec(i));
			xstyle := ETransitionType.Instant;
		end;
	end;

	time := FetchParamOrDefault(CMDP_TIME, -1);
	vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, EleHub.defaultViewport));

	// There can be only one transition active per viewport. If a new transition starts while a previous one in the
	// same viewport is running, then if both are owned by the same fiber, the new transition merges with the old one.
	// If owned by different fibers, the old transition is destroyed, then a new one created.
	with EffectHub do if transitionsActive <> 0 then for i := fxCount - 1 downto 0 do
		if fx[i] is TEffectTransition then with TEffectTransition(fx[i]) do if fxViewport = vp then begin
			if fxFiber = self then begin
				// Merge.
				transitionStyle := xstyle;
				if time >= 0 then ageLimit := time else ageLimit := 768;
				exit;
			end;
			Destroy;
			break;
		end;

	if time < 0 then time := 768; // default duration 768ms
	TEffectTransition.Create(self, time, vp, xstyle);

	// Delayed textbox clear kicks in on any transition or print.
	with BoxHub do if delayedClear then begin
		delayedClear := FALSE;
		if defaultTextbox <> NIL then defaultTextbox.Clear;
		if dialogueTitleBox <> NIL then dialogueTitleBox.Clear;
	end;
end;

procedure Invoke_GOTO; inline;
var _labelname, _fibername : TStringBunch;
	i : dword;
begin
	if NOT FetchParam(CMDP_LABEL, _labelname) then begin
		_FiberError('goto without label name', TRUE);
		exit;
	end;
	_labelname[labelObject.labelLanguage] := upcase(_labelname[labelObject.labelLanguage]);

	if NOT FetchParam(CMDP_NAME, _fibername) then
		ScriptGoto(_labelname[labelObject.labelLanguage])
	else begin
		i := FiberHub.GetFiber(upcase(_fibername[labelObject.labelLanguage]));
		if i >= FiberHub.fiberCount then
			_FiberError('goto with bad fiber name: ' + _fibername[labelObject.labelLanguage])
		else
			FiberHub.fiber[i].ScriptGoto(_labelname[labelObject.labelLanguage]);
	end;
end;

procedure Invoke_INT; inline;
var i : dword;
	l : longint;
	bannername : string31;
begin
	l := FetchParamOrDefault(CMDP_ANYNUMBER, 0);
	case l of

		1: // start performance tracking
		begin
			sakuParam.frameDelay := 1;
			PushInt(longint(GetMsecTime));
			PushInt(EStackToken.Number);
		end;

		2: // stop performance tracking
		begin
			sakuParam.frameDelay := 0;
			PushInt(longint(GetMsecTime));
			PushInt(EStackToken.Number);
		end;

		3: PrintInfoBox; // engine info and license

		4: // enumerate all dats and get number of known game data files
		begin
			FindDat('*.dat');
			NiceSortDats;
			PushInt(availableDatCount - 2); // ignore dat 0 and frontend dat which should be last in list
			PushInt(EStackToken.Number);
		end;

		5,6: // get dat name/description from enumerated list
		begin
			i := dword(FetchParamOrDefault(CMDP_INDEX, 0)) + 1; // ignore null dat
			if i >= availableDatCount then
				PushInt(0) // empty string
			else if l = 5 then
				PushString(availableDats[i].projectName)
			else
				PushString(availableDats[i].displayName);
			PushInt(EStackToken.SingleStr);
		end;

		7: // load banner graphic from enumerated dat
		begin
			i := dword(FetchParamOrDefault(CMDP_INDEX, 0)) + 1; // ignore null dat
			if i >= dword(length(availableDats)) then begin
				log('[!] loaddatbanner: bad dat index ' + strdec(i));
				PushInt(0);
			end
			else begin
				bannername := upcase(FetchParamOrDefault(CMDP_NAME, '_BANNER')[labelObject.labelLanguage]);
				if availableDats[i].LoadBanner(bannername) then
					PushInt(1)
				else
					PushInt(0);
			end;
			PushInt(EStackToken.Number);
		end;

		13: // engine version
		begin
			PushString(GetSuperSakuraVersion);
			PushInt(EStackToken.SingleStr);
		end;

		else _FiberError('bad int');
	end;
end;

procedure Invoke_LOG; inline;
var txt : TStringBunch;
	i : longint = 0;
begin
	{$note todo: deref vars for these, maybe other strings}
	if FetchParam(CMDP_TEXT, txt) then
		log(txt[labelObject.labelLanguage])
	else if FetchParam(CMDP_VALUE, i) then
		log(strdec(i))
	else
		log('');
end;

procedure Invoke_META_ENTER; inline;
begin
	if sysvar.metaState <> EMetaState.Normal then begin
		_FiberError('can''t enter metastate', TRUE);
		exit;
	end;
	HubStack.Push(TRUE);
	sysvar.metaState := EMetaState.Meta;
	yieldnow := TRUE;
end;

procedure Invoke_META_EXIT; inline;
var i : longint;
begin
	// The current fiber must carry to the coming metastate so it can make changes to normal state before exiting.
	// Extract it from FiberHub's fiber list.
	with FiberHub do begin
		dec(fiberCount);
		if executingId < fiberCount then begin
			fiber[executingId] := fiber[fiberCount];
			fiber[fiberCount] := NIL;
		end;
	end;

	HubStack.Pop;

	// Insert this fiber at the start of FiberHub's fiber list so it resumes first next frame.
	with FiberHub do begin
		if dword(length(fiber)) >= fiberCount then setlength(fiber, fiberCount + 2);
		if fiberCount <> 0 then for i := fiberCount - 1 downto 0 do fiber[i + 1] := fiber[i];
		inc(fiberCount);
		fiber[0] := self;
	end;

	yieldnow := TRUE;
end;

procedure Invoke_MUS_PAUSE; inline;
begin
	{$ifndef sakucon}
	soundserver.PausePlayback(TRUE);
	{$endif}
end;

procedure Invoke_MUS_PLAY; inline;
var songname, txt : UTF8string;
	time : longint;
	{$ifndef sakucon}
	i : dword;
	{$endif}
begin
	songname := FetchParamOrDefault(CMDP_NAME, '')[labelObject.labelLanguage];
	{$ifndef sakucon}
	if sysvar.mutedAudio then
		time := 0
	else
	{$endif}
	if NOT FetchParam(CMDP_TIME, time) then
		time := 1600
	else
		if time < 0 then time := 0;
	txt := upcase(songname);
	{$ifndef sakucon}
	if songname = '' then
		soundserver.FadeOut(time)
	else begin
		if sysvar.currentSong = txt then exit;
		i := GetSong(txt);
		if i = 0 then begin
			i := GetSong(txt + '.M');
			if i = 0 then begin
				_FiberError('song not found: ' + songname);
				exit;
			end;
		end;
		with musicObjects[i] do soundserver.PlayNewMidi(@data[0], @data[0] + length(data), time);
	end;
	{$endif}
	sysvar.currentSong := txt;
end;

procedure Invoke_MUS_RESUME; inline;
begin
	{$ifndef sakucon}
	soundserver.PausePlayback(FALSE);
	{$endif}
end;

procedure Invoke_MUS_STOP; inline;
{$ifdef sakucon}
begin
	sysvar.currentSong := '';
end;
{$else}
var time : longint = 0;
begin
	if NOT sysvar.mutedAudio then begin
		time := FetchParamOrDefault(CMDP_TIME, 0);
		if time < 0 then time := 0;
	end;
	soundserver.FadeOut(time);
	sysvar.currentSong := '';
end;
{$endif}

procedure Invoke_PEEK; inline;
var _fibername : UTF8string;
	_fiber : TFiber;
	i : longint;
begin
	_fibername := FetchParamOrDefault(CMDP_NAME, 'MAIN')[labelObject.labelLanguage];
	i := longint(FiberHub.GetFiber(upcase(_fibername)));
	if dword(i) >= FiberHub.fiberCount then begin
		_FiberError('no such peek fiber: ' + _fibername);
		exit;
	end;
	_fiber := FiberHub.fiber[i];

	if FetchParam(CMDP_INDEX, i) then begin
		if dword(i) >= _fiber.localCount then
			PushInt(EStackToken.Null)
		else begin
			PushInt(_fiber.locals[i]);
			PushInt(EStackToken.Number);
		end;
		exit;
	end;

	// If no index specified, dump all non-zero local variables from the chosen fiber, one line each.
	if _fiber.localCount <> 0 then
		for i := 0 to _fiber.localCount - 1 do
			if _fiber.locals[i] <> 0 then
				log(strcat('$% (&) = % (0x&)', [i, i, _fiber.locals[i], _fiber.locals[i]]));
end;

procedure Invoke_POKE; inline;
var _fibername : UTF8string;
	f, i, v : longint;
begin
	_fibername := FetchParamOrDefault(CMDP_NAME, 'MAIN')[labelObject.labelLanguage];
	f := longint(FiberHub.GetFiber(upcase(_fibername)));
	if dword(f) >= FiberHub.fiberCount then begin
		_FiberError('no such poke fiber: ' + _fibername);
		exit;
	end;
	i := FetchParamOrDefault(CMDP_INDEX, 0);
	v := FetchParamOrDefault(CMDP_VALUE, 0);
	FiberHub.fiber[f].SetLocal(i, v);
end;

procedure Invoke_RETURN(onelevel : boolean); inline;
var strvalue : TStringBunch = NIL;
	value : longint;
begin
	case FetchParamDynamic(strvalue, value) of
		EPoppedToken.Null: PushInt(EStackToken.Null);
		EPoppedToken.Int:
		begin
			PushInt(value);
			PushInt(EStackToken.Number);
		end;
		EPoppedToken.Str: PushStringBunch(strvalue);
	end;
	ScriptReturn(onelevel);
end;

procedure Invoke_SELECT; inline;
var txt : TStringBunch = NIL;
	output : TStringBunch = NIL;
	i, j, l : longint;
	fromp, readp, endp : ^char;
begin
	i := FetchParamOrDefault(CMDP_INDEX, -1);
	with labelObject do begin
		if (i < 0) or (NOT FetchParam(CMDP_TEXT, txt)) then begin
			PushInt(EStackToken.Null);
			exit;
		end;

		Assert(txt.Length <> 0);
		setlength(output, txt.Length);
		for j := high(txt) downto 0 do if txt[j] <> '' then begin

			readp := @txt[j][1];
			endp := readp + txt[j].Length;
			l := 0;
			while (i <> l) and (readp < endp) do begin
				if readp^ = ':' then inc(l);
				inc(readp);
			end;
			fromp := readp;
			while (readp < endp) and (readp^ <> ':') do inc(readp);

			if fromp <> readp then begin
				l := readp - fromp;
				setlength(output[j], l);
				move(fromp^, output[j][1], l);
			end;
		end;

		PushStringBunch(output);
	end;
end;

procedure Invoke_SYS_GETKEYDOWN; inline;
begin
	PushInt(PollKey(EArrows.Down));
	PushInt(EStackToken.Number);
end;

procedure Invoke_SYS_GETKEYLEFT; inline;
begin
	PushInt(PollKey(EArrows.Left));
	PushInt(EStackToken.Number);
end;

procedure Invoke_SYS_GETKEYRIGHT; inline;
begin
	PushInt(PollKey(EArrows.Right));
	PushInt(EStackToken.Number);
end;

procedure Invoke_SYS_GETKEYUP; inline;
begin
	PushInt(PollKey(EArrows.Up));
	PushInt(EStackToken.Number);
end;

procedure Invoke_SYS_GETMOUSEX; inline;
var vp : dword;
begin
	vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, EleHub.defaultViewport));
	if vp >= dword(length(Viewportmatic.viewport)) then begin
		_FiberError('getmousexy: bad viewport: ' + strdec(vp));
		exit;
	end;
	with Viewportmatic.viewport[vp] do
		PushInt((sysvar.mouseLocP.x - viewportLoc.leftp) * 32768 div longint(viewportSizeP.w));
	PushInt(EStackToken.Number);
end;

procedure Invoke_SYS_GETMOUSEY; inline;
var vp : dword;
begin
	vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, EleHub.defaultViewport));
	if vp >= dword(length(Viewportmatic.viewport)) then begin
		_FiberError('getmousexy: bad viewport: ' + strdec(vp));
		exit;
	end;
	with Viewportmatic.viewport[vp] do
		PushInt((sysvar.mouseLocP.y - viewportLoc.topp) * 32768 div longint(viewportSizeP.h));
	PushInt(EStackToken.Number);
end;

procedure Invoke_SYS_LOADDAT; inline;
var _datname : TStringBunch;
begin
	if NOT FetchParam(CMDP_NAME, _datname) then
		_FiberError('sys.loaddat: no dat name')
	else
		LoadDatCommon(_datname[labelObject.labelLanguage]);
end;

procedure Invoke_SYS_LOADSTATE; inline;
var i : dword;
begin
	i := dword(FetchParamOrDefault(CMDP_INDEX, 0));
	Savematic.EnumerateSaves(FALSE); // <-- this goes in a separate command
	if length(Savematic.availableSaveList) = 0 then begin
		SummonMessageBox('No savefile found.');
		yieldnow := TRUE;
	end
	else begin
		Savematic.LoadState(Savematic.availableSaveList[i]);
		FiberHub.exitRunFibersNow := TRUE;
	end;
end;

procedure Invoke_SYS_PAUSE; inline;
begin
	SetPauseState(EPauseState.Paused);
	yieldNow := TRUE;
end;

procedure Invoke_SYS_QUIT; inline;
begin
	if (sysvar.metaState = EMetaState.ConfirmQuit) or (FetchParamOrDefault(CMDP_FORCE, 0) <> 0) then begin
		sysvar.quit := TRUE;
		FiberHub.exitRunFibersNow := TRUE;
	end
	else begin
		SummonConfirmQuit;
		yieldNow := TRUE;
	end;
end;

procedure Invoke_SYS_RESTARTGAME; inline;
begin
	log('Restarting...');
	sysvar.quit := TRUE;
	sysvar.restart := TRUE;
	FiberHub.exitRunFibersNow := TRUE;
end;

procedure Invoke_SYS_SAVESTATE; inline;
begin
	with Savematic do begin
		saveNow := TRUE;
		saveNowIndex := dword(FetchParamOrDefault(CMDP_INDEX, 0));
		saveNowDesc := FetchParamOrDefault(CMDP_NAME, '')[labelObject.labelLanguage];
	end;
	yieldNow := TRUE;
end;

procedure Invoke_SYS_SETCURSOR; inline;
var _gfxname : TStringBunch;
begin
	if FetchParam(CMDP_GOB, _gfxname) then
		log('set cursor to ' + _gfxname[labelObject.labelLanguage])
	else
		log('remove cursor override');
end;

procedure Invoke_SYS_SHOWTEXTLOG; inline;
begin
	if sysvar.dropdownConsoleState = EDDCState.Debug then exit;

	with BoxHub.textbox[high(BoxHub.textbox)] do begin
		if isHidden then Hide(FALSE);
		if boxState in [EBoxState.Null, EBoxState.Vanishing] then BoxHub.DropdownSlideDown;
		sysvar.dropdownConsoleState := EDDCState.Log;
		sysvar.dropdownContentChanged := TRUE;
	end;
end;

procedure Invoke_TBOX_CLEAR; inline;
var boxname : TStringBunch;
	_box : TTextBox;
	i : dword;
begin
	if FetchParam(CMDP_BOX, boxname) then begin
		boxname[labelObject.labelLanguage] := upcase(boxname[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(boxname[labelObject.labelLanguage]);
		if _box = NIL then _box := TTextBox.Create(boxname[labelObject.labelLanguage]);
		with Choicematic do if (isActive) and (_box = choiceBox) then Deactivate(TRUE, FALSE);
		_box.Clear;
	end
	else with BoxHub do begin
		with Choicematic do if isActive then Deactivate(TRUE, FALSE); // with choice contents lost, must deactivate
		for i := high(textbox) - 1 downto 0 do textbox[i].Clear;
	end;
end;

procedure Invoke_TBOX_DECORATE; inline;
var _gfxname, boxname : TStringBunch;
	_box : TTextBox;
	decornum : dword;
	eleloc, eleanchor : TCoord32k;
	elesize : TSize32k;
	frame : longint;
begin
	_box := BoxHub.defaultTextbox;
	if FetchParam(CMDP_BOX, boxname) then begin
		boxname[labelObject.labelLanguage] := upcase(boxname[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(boxname[labelObject.labelLanguage]);
		if _box = NIL then _box := TTextBox.Create(boxname[labelObject.labelLanguage]);
	end;
	Assert(_box <> NIL);

	if NOT FetchParam(CMDP_GOB, _gfxname) then begin
		_FiberError('decorate box without graphic name');
		exit;
	end;

	eleloc.x := FetchParamOrDefault(CMDP_LOCX, 0);
	eleloc.y := FetchParamOrDefault(CMDP_LOCY, 0);
	elesize.w := FetchParamOrDefault(CMDP_SIZEX, 0);
	elesize.h := FetchParamOrDefault(CMDP_SIZEY, 0);
	eleanchor.x := FetchParamOrDefault(CMDP_ANCHORX, 0);
	eleanchor.y := FetchParamOrDefault(CMDP_ANCHORY, 0);
	frame := FetchParamOrDefault(CMDP_FRAME, 0);

	with _box do begin
		decornum := length(style.decorList);
		setlength(style.decorList, decornum + 1);
		with style.decorList[decornum] do begin
			decorName := upcase(_gfxname[labelObject.labelLanguage]);
			decorFrameIndex := frame;
			decorAnchor := eleanchor;
			decorLoc := eleloc;
			decorSize.w := elesize.w;
			decorSize.h := elesize.h;
		end;
		baseNeedsRender := TRUE;
	end;
end;

procedure Invoke_TBOX_GETPARAM; inline;
var paramname, boxname : TStringBunch;
	strvalue : UTF8string;
	_box : TTextBox;
	value : longint;
begin
	_box := BoxHub.defaultTextbox;
	if FetchParam(CMDP_BOX, boxname) then begin
		boxname[labelObject.labelLanguage] := upcase(boxname[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(boxname[labelObject.labelLanguage]);
		if _box = NIL then begin
			_FiberError('tbox.getparam bad box: ' + boxname[labelObject.labelLanguage]);
			exit;
		end;
	end;
	if _box = NIL then begin
		_FiberError('tbox.getparam bad default box');
		exit;
	end;

	if NOT FetchParam(CMDP_NAME, paramname) then begin
		_FiberError('tbox.setparam ' + _box.boxName + ' without param name');
		exit;
	end;

	value := _box.GetBoxParam(lowercase(paramname[labelObject.labelLanguage]), strvalue);
	if strvalue = '' then begin
		PushInt(value);
		PushInt(EStackToken.Number);
	end
	else begin
		PushString(strvalue);
		PushInt(EStackToken.SingleStr);
	end;
end;

procedure Invoke_TBOX_GETTEXT; inline;
var boxname : TStringBunch;
	_box : TTextBox;
	txt : UTF8string = '';
begin
	if FetchParam(CMDP_BOX, boxname) then
		_box := BoxHub.GetBox(upcase(boxname[labelObject.labelLanguage]))
	else
		_box := BoxHub.defaultTextbox;
	if _box = NIL then begin
		_FiberError('gettext bad box: ' + boxname[labelObject.labelLanguage]);
		exit;
	end;

	with _box.content do begin
		setlength(txt, txtLength);
		if txtLength <> 0 then
			move(txtContent[0], txt[1], txtLength);
		PushString(txt);
		PushInt(EStackToken.SingleStr);
	end;
end;

procedure Invoke_TBOX_GETUSERINPUT; inline;
var boxname : TStringBunch;
	_box : TTextBox;
	userinput : UTF8string = '';
begin
	if FetchParam(CMDP_BOX, boxname) then
		_box := BoxHub.GetBox(upcase(boxname[labelObject.labelLanguage]))
	else
		_box := BoxHub.defaultTextbox;
	if _box = NIL then begin
		_FiberError('bad getuserinput box: ' + boxname[labelObject.labelLanguage]);
		exit;
	end;

	with _box.content do begin
		setlength(userinput, userInputLen);
		if userInputLen <> 0 then
			move(txtContent[txtLength - userInputLen], userinput[1], userInputLen);
		PushString(userinput);
		PushInt(EStackToken.SingleStr);
	end;
end;

procedure Invoke_TBOX_LIST; inline;
var boxname : TStringBunch;
	_box : TTextBox = NIL;
begin
	if FetchParam(CMDP_BOX, boxname) then _box := BoxHub.GetBox(upcase(boxname[labelObject.labelLanguage]));
	if _box <> NIL then
		log(_box.ToString)
	else
		for _box in BoxHub.textbox do
			log(_box.ToString);
end;

procedure Invoke_TBOX_OUTLINE; inline;
var boxname : TStringBunch;
	_box : TTextBox;
	outlinenum, color : dword;
	_thickness, alpha : longint;
	outlineloc : TCoord32k;
begin
	_box := BoxHub.defaultTextbox;
	if FetchParam(CMDP_BOX, boxname) then begin
		boxname[labelObject.labelLanguage] := upcase(boxname[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(boxname[labelObject.labelLanguage]);
		if _box = NIL then _box := TTextBox.Create(boxname[labelObject.labelLanguage]);
	end;
	Assert(_box <> NIL);

	color := dword(FetchParamOrDefault(CMDP_COLOR, $000F));
	if color > $FFFF then begin
		_FiberError('outline color not in 0..$FFFF');
		exit;
	end;

	_thickness := FetchParamOrDefault(CMDP_THICKNESS, 256);
	outlineloc.x := FetchParamOrDefault(CMDP_LOCX, 0);
	outlineloc.y := FetchParamOrDefault(CMDP_LOCY, 0);
	alpha := FetchParamOrDefault(CMDP_ALPHA, 0);

	with _box do begin
		outlinenum := length(style.outline);
		setlength(style.outline, outlinenum + 1);
		with style.outline[outlinenum] do begin
			outlinecolor.FromRGBA4(color);
			thickness := abs(_thickness);
			offset.x := outlineloc.x;
			offset.y := outlineloc.y;
			alphaFade := (alpha <> 0);
		end;
		// Outlines may change content buffer size since they affect font size.
		parametersNeedUpdate := TRUE;
	end;
end;

procedure Invoke_TBOX_POPIN; inline;
var boxname : TStringBunch;
	_box : TTextBox;
	_style : TStringBunch;
	time : longint;
begin
	if FetchParam(CMDP_BOX, boxname) then
		_box := BoxHub.GetBox(upcase(boxname[labelObject.labelLanguage]))
	else
		_box := BoxHub.defaultTextbox;
	if _box = NIL then begin
		_FiberError('bad popin box: ' + boxname[labelObject.labelLanguage]);
		exit;
	end;

	if FetchParam(CMDP_TIME, time) then begin
		if time < 0 then time := 0;
		_box.style.popTimeMsec := time;
	end;
	if FetchParam(CMDP_STYLE, _style) then _box.style.popType := EPopTypeFromString(_style[labelObject.labelLanguage]);

	_box.boxState := EBoxState.Appearing;
end;

procedure Invoke_TBOX_POPOUT; inline;
var boxname : TStringBunch;
	_box : TTextBox;
	_style : TStringBunch;
	time : longint;
begin
	if FetchParam(CMDP_BOX, boxname) then
		_box := BoxHub.GetBox(upcase(boxname[labelObject.labelLanguage]))
	else
		_box := BoxHub.defaultTextbox;
	if _box = NIL then begin
		_FiberError('bad popout box: ' + boxname[labelObject.labelLanguage]);
		exit;
	end;

	if FetchParam(CMDP_TIME, time) then begin
		if time < 0 then time := 0;
		_box.style.popTimeMsec := time;
	end;
	if FetchParam(CMDP_STYLE, _style) then _box.style.popType := EPopTypeFromString(_style[labelObject.labelLanguage]);

	if _box.boxState = EBoxState.ShowText then _box.popRunTimeMsec := _box.style.popTimeMsec;
	_box.boxState := EBoxState.Vanishing;
end;

procedure Invoke_TBOX_PRINT; inline;
var txt1, txt2 : TStringBunch;
	_box : TTextBox;
	i : longint;
	hasbox, hastext : boolean;
begin
	txt1 := NIL; txt2 := NIL;
	// Standard form: print "<box name>" "<text>"
	hasbox := FetchParam(CMDP_BOX, txt1);
	hastext := FetchParam(CMDP_TEXT, txt2);
	// If only one string parameter, it must be print [default box] "<text>".
	if hasbox AND (NOT hastext) then begin
		txt2 := txt1; txt1 := NIL;
		hastext := TRUE;
	end;
	// If no string parameters, it must be print [default box] <number>.
	if NOT (hasbox or hastext) then if FetchParam(CMDP_VALUE, i) then begin
		setlength(txt2, languageList.Length);
		txt2.SetAll(strdec(i));
	end;
	// If no parameters, it must be print [default box] <empty string>.
	if (txt2 = NIL) or (txt2[labelObject.labelLanguage] = '') then exit;

	if txt1 = NIL then begin
		_box := BoxHub.defaultTextbox;
	end
	else begin
		txt1[0] := upcase(txt1[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(txt1[0]);
		if _box = NIL then _box := TTextBox.Create(txt1[0]);
	end;

	// Delayed textbox clear kicks in on any print into the main or title box, or on transition.
	with BoxHub do if (delayedClear) and ((_box = defaultTextbox) or (_box = dialogueTitleBox)) then begin
		delayedClear := FALSE;
		if defaultTextbox <> NIL then defaultTextbox.Clear;
		if dialogueTitleBox <> NIL then dialogueTitleBox.Clear;
	end;

	Assert(_box <> NIL);
	_box.PrintNested(txt2);
end;

procedure Invoke_TBOX_REMOVE; inline;
var boxname : TStringBunch;
	_box : TTextBox = NIL;
begin
	if FetchParam(CMDP_BOX, boxname) then _box := BoxHub.GetBox(upcase(boxname[labelObject.labelLanguage]));
	if _box = NIL then begin
		_FiberError('bad remove box: ' + boxname[labelObject.labelLanguage]);
		exit;
	end;

	_box.Destroy;
end;

procedure Invoke_TBOX_REMOVEDECOR; inline;
var boxname : TStringBunch;
	_box : TTextBox;
begin
	if FetchParam(CMDP_BOX, boxname) then
		_box := BoxHub.GetBox(upcase(boxname[labelObject.labelLanguage]))
	else
		_box := BoxHub.defaultTextbox;
	if _box = NIL then begin
		_FiberError('bad removedecor box: ' + boxname[labelObject.labelLanguage]);
		exit;
	end;

	_box.RemoveDecoration(FetchParamOrDefault(CMDP_GOB, '')[labelObject.labelLanguage]);
end;

procedure Invoke_TBOX_REMOVEOUTLINES; inline;
var boxname : TStringBunch;
	_box : TTextBox;
begin
	if FetchParam(CMDP_BOX, boxname) then
		_box := BoxHub.GetBox(upcase(boxname[labelObject.labelLanguage]))
	else
		_box := BoxHub.defaultTextbox;
	if _box = NIL then begin
		_FiberError('bad removeoutlines box: ' + boxname[labelObject.labelLanguage]);
		exit;
	end;

	with _box do begin
		setlength(style.outline, 0);
		// Outlines may change content buffer size since they affect font size.
		parametersNeedUpdate := TRUE;
	end;
end;

procedure Invoke_TBOX_RESET; inline;
var boxname : TStringBunch;
	_box : TTextBox;
begin
	if NOT FetchParam(CMDP_BOX, boxname) then
		BoxHub.ResetHub
	else begin
		boxname[0] := upcase(boxname[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(boxname[0]);
		if _box = NIL then _box := TTextBox.Create(boxname[0]);
		_box.Reset;
		with Choicematic do if (isActive) and (_box = choiceBox) then Deactivate(TRUE, FALSE);
	end;
end;

procedure Invoke_TBOX_SCROLL; inline;
var _style, boxname : TStringBunch;
	_box : TTextBox;
	newloc, time : longint;
	scrollstyle : EMoveType;
	force : boolean;
begin
	_box := BoxHub.defaultTextbox;
	if FetchParam(CMDP_BOX, boxname) then begin
		boxname[labelObject.labelLanguage] := upcase(boxname[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(boxname[labelObject.labelLanguage]);
		if _box = NIL then _box := TTextBox.Create(boxname[labelObject.labelLanguage]);
	end;
	Assert(_box <> NIL);

	with _box do begin
		newloc := FetchParamOrDefault(CMDP_LOCY, high(longint));
		time := FetchParamOrDefault(CMDP_TIME, 0);
		if time < 0 then time := 0;
		scrollstyle := EMoveType.Linear;
		if FetchParam(CMDP_STYLE, _style) then scrollstyle := EMoveTypeFromString(_style[labelObject.labelLanguage]);
		force := FetchParamOrDefault(CMDP_FORCE, 0) <> 0;

		ScrollTo(newloc, time, scrollstyle, force, self);
	end;
end;

procedure Invoke_TBOX_SETLOC; inline;
var _style, boxname : TStringBunch;
	_box : TTextBox;
	time : longint;
	newloc, newanchor : TCoord32k;
	movestyle : EMoveType;
begin
	_box := BoxHub.defaultTextbox;
	if FetchParam(CMDP_BOX, boxname) then begin
		boxname[labelObject.labelLanguage] := upcase(boxname[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(boxname[labelObject.labelLanguage]);
		if _box = NIL then _box := TTextBox.Create(boxname[labelObject.labelLanguage]);
	end;
	Assert(_box <> NIL);

	with _box do with boxRect do begin
		newloc.x := FetchParamOrDefault(CMDP_LOCX, boxLoc.x);
		newloc.y := FetchParamOrDefault(CMDP_LOCY, boxLoc.y);
		newanchor.x := FetchParamOrDefault(CMDP_ANCHORX, boxAnchor.x);
		newanchor.y := FetchParamOrDefault(CMDP_ANCHORY, boxAnchor.y);

		time := FetchParamOrDefault(CMDP_TIME, 0);
		if time < 0 then time := 0;

		movestyle := EMoveType.Linear;
		if FetchParam(CMDP_STYLE, _style) then movestyle := EMoveTypeFromString(_style[labelObject.labelLanguage]);

		TEffectBoxMove.Create(self, time, _box, newloc.x, newloc.y, newanchor.x, newanchor.y, movestyle);
	end;
end;

procedure Invoke_TBOX_SETPARAM; inline;
var paramname, strvalue, boxname : TStringBunch;
	_box : TTextBox;
	value : longint = 0;
begin
	strvalue := NIL; // silence compiler
	_box := BoxHub.defaultTextbox;
	if FetchParam(CMDP_BOX, boxname) then begin
		boxname[labelObject.labelLanguage] := upcase(boxname[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(boxname[labelObject.labelLanguage]);
		if _box = NIL then _box := TTextBox.Create(boxname[labelObject.labelLanguage]);
	end;
	Assert(_box <> NIL);

	if NOT FetchParam(CMDP_NAME, paramname) then begin
		_FiberError('tbox.setparam ' + _box.boxName + ' without param name');
		exit;
	end;

	if namedparam.defined[CMDP_VALUE] then
		_box.SetBoxParam(paramname[labelObject.labelLanguage], '', namedparam.pNumValue[CMDP_VALUE], FALSE)
	else if namedparam.defined[CMDP_TEXT] then
		_box.SetBoxParam(paramname[labelObject.labelLanguage], namedparam.pStrValue[CMDP_TEXT][labelObject.labelLanguage], 0, FALSE)
	else begin
		case FetchParamDynamic(strvalue, value) of
			EPoppedToken.Null: _box.SetBoxParam(paramname[labelObject.labelLanguage], '', 0, TRUE);
			EPoppedToken.Int: _box.SetBoxParam(paramname[labelObject.labelLanguage], '', value, FALSE);
			EPoppedToken.Str: _box.SetBoxParam(paramname[labelObject.labelLanguage], strvalue[labelObject.labelLanguage], 0, FALSE);
		end;
	end;

	// Extra key:values are allowed with unnamed dynamic params.
	while dparamcount <> 0 do begin
		if NOT FetchParam(CMDP_ANYSTRING, paramname) then begin
			_FiberError('tbox.setparam extra key-values must start with param name');
			exit;
		end;
		case FetchParamDynamic(strvalue, value) of
			EPoppedToken.Null: _box.SetBoxParam(paramname[labelObject.labelLanguage], '', 0, TRUE);
			EPoppedToken.Int: _box.SetBoxParam(paramname[labelObject.labelLanguage], '', value, FALSE);
			EPoppedToken.Str: _box.SetBoxParam(paramname[labelObject.labelLanguage], strvalue[labelObject.labelLanguage], 0, FALSE);
		end;
	end;
end;

procedure Invoke_TBOX_SETSIZE; inline;
var _style, boxname : TStringBunch;
	_box : TTextBox;
	time : longint;
	newsize : TSize32k;
	sizestyle : EMoveType;
begin
	_box := BoxHub.defaultTextbox;
	if FetchParam(CMDP_BOX, boxname) then begin
		boxname[labelObject.labelLanguage] := upcase(boxname[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(boxname[labelObject.labelLanguage]);
		if _box = NIL then _box := TTextBox.Create(boxname[labelObject.labelLanguage]);
	end;
	Assert(_box <> NIL);

	with _box do begin
		newsize.w := dword(FetchParamOrDefault(CMDP_SIZEX, -1));
		newsize.h := dword(FetchParamOrDefault(CMDP_SIZEY, -1));

		time := FetchParamOrDefault(CMDP_TIME, 0);
		if time < 0 then time := 0;

		sizestyle := EMoveType.Linear;
		if FetchParam(CMDP_STYLE, _style) then sizestyle := EMoveTypeFromString(_style[labelObject.labelLanguage]);

		TEffectBoxSize.Create(self, time, _box, newsize.w, newsize.h, sizestyle);
	end;
end;

procedure Invoke_TBOX_SETTEXTURE; inline;
var _gfxname, _textype, _texstyle, boxname : TStringBunch;
	_box : TTextBox;
	frame : longint = 0;
begin
	_box := BoxHub.defaultTextbox;
	if FetchParam(CMDP_BOX, boxname) then begin
		boxname[labelObject.labelLanguage] := upcase(boxname[labelObject.labelLanguage]);
		_box := BoxHub.GetBox(boxname[labelObject.labelLanguage]);
		if _box = NIL then _box := TTextBox.Create(boxname[labelObject.labelLanguage]);
	end;
	Assert(_box <> NIL);

	with _box do begin
		style.texture.texName := '';
		if FetchParam(CMDP_GOB, _gfxname) then style.texture.texName := upcase(_gfxname[labelObject.labelLanguage]);

		if style.texture.texName = '' then
			style.texture.texType := ETextureType.None
		else begin
			style.texture.texType := ETextureType.Stretched;
			if FetchParam(CMDP_TYPE, _textype) then
				style.texture.texType := ETextureTypeFromString(_textype[labelObject.labelLanguage]);
		end;

		style.texture.texBlendMode := EBlendMode.Normal;
		if FetchParam(CMDP_STYLE, _texstyle) then
			style.texture.texBlendMode := EBlendModeFromString(_texstyle[labelObject.labelLanguage]);

		style.texture.texFrameIndex := 0;
		if FetchParam(CMDP_FRAME, frame) then begin
			if frame < 0 then frame := 0;
			style.texture.texFrameIndex := frame;
		end;
		parametersNeedUpdate := TRUE;
	end;
end;

procedure Invoke_TBOX_USERINPUTSTART; inline;
var initialtxt, boxname : TStringBunch;
	_box : TTextBox;
	i : longint;
begin
	if FetchParam(CMDP_BOX, boxname) then
		_box := BoxHub.GetBox(upcase(boxname[labelObject.labelLanguage]))
	else
		_box := BoxHub.defaultTextbox;
	if _box = NIL then begin
		_FiberError('bad userinput box: ' + boxname[labelObject.labelLanguage]);
		exit;
	end;

	initialtxt := FetchParamOrDefault(CMDP_TEXT, '');
	i := FetchParamOrDefault(CMDP_INDEX, -1);
	with _box do begin
		if content.caretPos >= 0 then
			log('[!] user input already enabled in box ' + boxname[labelObject.labelLanguage]);
		StartTextInput(initialtxt[_box.style.boxLanguage], i);
	end;
end;

procedure Invoke_TBOX_USERINPUTSTOP; inline;
var boxname : TStringBunch;
	_box : TTextBox;
	userinput : UTF8string = '';
	i : dword;
begin
	if FetchParam(CMDP_BOX, boxname) then begin
		_box := BoxHub.GetBox(upcase(boxname[labelObject.labelLanguage]));
		if _box = NIL then begin
			_FiberError('bad userinput box: ' + boxname[labelObject.labelLanguage]);
			exit;
		end;
		with _box do with content do begin
			StopTextInput;
			setlength(userinput, userInputLen);
			if userInputLen <> 0 then
				move(txtContent[txtLength - userInputLen], userinput[1], userInputLen);
			PushString(userinput);
			PushInt(EStackToken.SingleStr);
		end;
	end
	else with BoxHub do
		for i := high(textbox) - 1 downto 1 do textbox[i].StopTextInput;
end;

procedure Invoke_VIEWPORT_LIST; inline;
var vp : dword;
begin
	vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, -1));
	with Viewportmatic do begin
		if vp < dword(length(viewport)) then
			log(viewport[vp].ToString)
		else
			for vp := 0 to high(viewport) do Log(viewport[vp].ToString);
	end;
end;

procedure Invoke_VIEWPORT_SETGAMMA; inline;
begin
	log('viewport.setgamma not implemented');
end;

procedure Invoke_VIEWPORT_SETPARAMS; inline;
var _parent : TStringBunch;
	vp : dword;
	newparent : ptrint;
	i : longint = 0;
begin
	vp := dword(FetchParamOrDefault(CMDP_VIEWPORT, 1));
	if vp = 0 then begin
		_FiberError('can''t set params on fixed viewport 0');
		exit;
	end;
	if vp > 99999 then begin
		_FiberError('too big viewport: ' + strdec(vp));
		exit;
	end;
	if vp >= dword(length(Viewportmatic.viewport)) then Viewportmatic.SetNumViewports(vp + 1);

	with Viewportmatic.viewport[vp] do begin
		// Parent is a string for most purposes but we need a number here...
		if FetchParam(CMDP_PARENT, _parent) then begin
			newparent := valx(_parent[labelObject.labelLanguage]);
			if newparent < 0 then begin
				_FiberError('bad viewport ' + strdec(newparent));
				exit;
			end;
			if ptruint(newparent) >= vp then begin
				_FiberError('can''t set viewport ' + strdec(vp) + ' parent >= itself');
				exit;
			end;
			viewportParent := newparent;
		end;

		if FetchParam(CMDP_LOCX, i) then begin
			inc(viewportLoc.right, i - viewportLoc.left);
			viewportLoc.left := i;
		end;
		if FetchParam(CMDP_LOCY, i) then begin
			inc(viewportLoc.bottom, i - viewportLoc.top);
			viewportLoc.top := i;
		end;

		if FetchParam(CMDP_SIZEX, i) then
			viewportLoc.right := viewportLoc.left + i;
		if FetchParam(CMDP_SIZEY, i) then
			viewportLoc.bottom := viewportLoc.top + i;

		if FetchParam(CMDP_RATIOX, i) then viewportRatio.w := i;
		if FetchParam(CMDP_RATIOY, i) then viewportRatio.h := i;

		Update;
	end;
end;

// ------------------------------------------------------------------

procedure InvokeCommand(cmdtoken : byte);
begin
	//log(strcat('Fiber %, label %: cmd %', [fiberName, labelName, cmdtoken]));
	case cmdtoken of
		CMD_NOP: ;

		CMD_SELECT: Invoke_SELECT;
		CMD_CALL: Invoke_CALL;
		CMD_CASECALL: Invoke_CASECALL;
		CMD_CASEGOTO: Invoke_CASEGOTO;
		CMD_GOTO: Invoke_GOTO;
		CMD_RETURN: Invoke_RETURN(TRUE);
		CMD_SCRIPTRET: Invoke_RETURN(FALSE);

		CMD_LOG: Invoke_LOG;
		CMD_ERROR: Invoke_ERROR;

		CMD_CHOICE_CALL: Invoke_CHOICE_INIT;
		CMD_CHOICE_CANCEL: Invoke_CHOICE_CANCEL;
		CMD_CHOICE_GET: Invoke_CHOICE_INIT;
		CMD_CHOICE_GETHIGHLIGHT: Invoke_CHOICE_GETHIGHLIGHT;
		CMD_CHOICE_GOTO: Invoke_CHOICE_INIT;
		CMD_CHOICE_LIST: Invoke_CHOICE_LIST;
		CMD_CHOICE_OFF: Invoke_CHOICE_OFF;
		CMD_CHOICE_ON: Invoke_CHOICE_ON;
		CMD_CHOICE_REMOVE: Invoke_CHOICE_REMOVE;
		CMD_CHOICE_RESET: Invoke_CHOICE_RESET;
		CMD_CHOICE_SET: Invoke_CHOICE_SET;

		CMD_EVENT_CREATE_TIMER: Invoke_EVENT_CREATE_TIMER;
		CMD_EVENT_GETTIMER: Invoke_EVENT_GETTIMER;
		CMD_EVENT_REMOVE: Invoke_EVENT_REMOVE;
		CMD_EVENT_SETLABEL: Invoke_EVENT_SETLABEL;
		CMD_EVENT_SETTIMER: Invoke_EVENT_SETTIMER;

		CMD_FIBER_CALLSTACK: Invoke_FIBER_CALLSTACK;
		CMD_FIBER_GETNAME: Invoke_FIBER_GETNAME;
		CMD_FIBER_LIST: Invoke_FIBER_LIST;
		CMD_FIBER_SIGNAL: Invoke_FIBER_SIGNAL;
		CMD_FIBER_START: Invoke_FIBER_START;
		CMD_FIBER_STOP: Invoke_FIBER_STOP;
		CMD_FIBER_WAIT: Invoke_FIBER_WAIT;
		CMD_FIBER_WAITKEY: Invoke_FIBER_WAITKEY;
		CMD_FIBER_WAITSIG: Invoke_FIBER_WAITSIG;
		CMD_FIBER_WAITTYPING: Invoke_FIBER_WAITTYPING;
		CMD_FIBER_YIELD: Invoke_FIBER_YIELD;

		CMD_FX_SKIP: Invoke_FX_SKIP;

		CMD_GFX_ADOPT: Invoke_GFX_ADOPT;
		CMD_GFX_BASH: Invoke_GFX_BASH;
		CMD_GFX_CLEARALL: Invoke_GFX_CLEARALL;
		CMD_GFX_CLEARANIMS: Invoke_GFX_CLEARANIMS;
		CMD_GFX_CLEARBKG: Invoke_GFX_CLEARBKG;
		CMD_GFX_CLEARKIDS: Invoke_GFX_CLEARKIDS;
		CMD_GFX_FIND: Invoke_GFX_FIND;
		CMD_GFX_FLASH: Invoke_GFX_FLASH;
		CMD_GFX_GETFRAME: Invoke_GFX_GETFRAME;
		CMD_GFX_GETSEQUENCE: Invoke_GFX_GETSEQUENCE;
		CMD_GFX_LIST: Invoke_GFX_LIST;
		CMD_GFX_MOVE: Invoke_GFX_MOVE;
		CMD_GFX_PRECACHE: Invoke_GFX_PRECACHE;
		CMD_GFX_REMOVE: Invoke_GFX_REMOVE;
		CMD_GFX_SETALPHA: Invoke_GFX_SETALPHA;
		CMD_GFX_SETFRAME: Invoke_GFX_SETFRAME;
		CMD_GFX_SETSEQUENCE: Invoke_GFX_SETSEQUENCE;
		CMD_GFX_SETSOLIDBLIT: Invoke_GFX_SETSOLIDBLIT;
		CMD_GFX_SETSOURCE: Invoke_GFX_SETSOURCE;
		CMD_GFX_SHOW: Invoke_GFX_SHOW;
		CMD_GFX_TRANSITION: Invoke_GFX_TRANSITION;

		CMD_INT: Invoke_INT;

		CMD_META_ENTER: Invoke_META_ENTER;
		CMD_META_EXIT: Invoke_META_EXIT;

		CMD_MUS_PLAY: Invoke_MUS_PLAY;
		CMD_MUS_STOP: Invoke_MUS_STOP;
		CMD_MUS_PAUSE: Invoke_MUS_PAUSE;
		CMD_MUS_RESUME: Invoke_MUS_RESUME;

		CMD_PEEK: Invoke_PEEK;
		CMD_POKE: Invoke_POKE;

		CMD_TBOX_CLEAR: Invoke_TBOX_CLEAR;
		CMD_TBOX_DECORATE: Invoke_TBOX_DECORATE;
		CMD_TBOX_GETPARAM: Invoke_TBOX_GETPARAM;
		CMD_TBOX_GETTEXT: Invoke_TBOX_GETTEXT;
		CMD_TBOX_GETUSERINPUT: Invoke_TBOX_GETUSERINPUT;
		CMD_TBOX_LIST: Invoke_TBOX_LIST;
		CMD_TBOX_OUTLINE: Invoke_TBOX_OUTLINE;
		CMD_TBOX_POPIN: Invoke_TBOX_POPIN;
		CMD_TBOX_POPOUT: Invoke_TBOX_POPOUT;
		CMD_TBOX_PRINT: Invoke_TBOX_PRINT;
		CMD_TBOX_REMOVE: Invoke_TBOX_REMOVE;
		CMD_TBOX_REMOVEDECOR: Invoke_TBOX_REMOVEDECOR;
		CMD_TBOX_REMOVEOUTLINES: Invoke_TBOX_REMOVEOUTLINES;
		CMD_TBOX_RESET: Invoke_TBOX_RESET;
		CMD_TBOX_SCROLL: Invoke_TBOX_SCROLL;
		CMD_TBOX_SETLOC: Invoke_TBOX_SETLOC;
		CMD_TBOX_SETPARAM: Invoke_TBOX_SETPARAM;
		CMD_TBOX_SETSIZE: Invoke_TBOX_SETSIZE;
		CMD_TBOX_SETTEXTURE: Invoke_TBOX_SETTEXTURE;
		CMD_TBOX_USERINPUTSTART: Invoke_TBOX_USERINPUTSTART;
		CMD_TBOX_USERINPUTSTOP: Invoke_TBOX_USERINPUTSTOP;

		CMD_SYS_GETKEYDOWN: Invoke_SYS_GETKEYDOWN;
		CMD_SYS_GETKEYLEFT: Invoke_SYS_GETKEYLEFT;
		CMD_SYS_GETKEYRIGHT: Invoke_SYS_GETKEYRIGHT;
		CMD_SYS_GETKEYUP: Invoke_SYS_GETKEYUP;
		CMD_SYS_GETMOUSEX: Invoke_SYS_GETMOUSEX;
		CMD_SYS_GETMOUSEY: Invoke_SYS_GETMOUSEY;
		CMD_SYS_LOADDAT: Invoke_SYS_LOADDAT;
		CMD_SYS_LOADSTATE: Invoke_SYS_LOADSTATE;
		CMD_SYS_PAUSE: Invoke_SYS_PAUSE;
		CMD_SYS_QUIT: Invoke_SYS_QUIT;
		CMD_SYS_RESTARTGAME: Invoke_SYS_RESTARTGAME;
		CMD_SYS_SAVESTATE: Invoke_SYS_SAVESTATE;
		CMD_SYS_SETCURSOR: Invoke_SYS_SETCURSOR;
		CMD_SYS_SHOWTEXTLOG: Invoke_SYS_SHOWTEXTLOG;

		CMD_VIEWPORT_LIST: Invoke_VIEWPORT_LIST;
		CMD_VIEWPORT_SETGAMMA: Invoke_VIEWPORT_SETGAMMA;
		CMD_VIEWPORT_SETPARAMS: Invoke_VIEWPORT_SETPARAMS;

		else begin
			_FiberError('bad cmd token: ' + strdec(cmdtoken), TRUE);
			exit;
		end;
	end;
end;
