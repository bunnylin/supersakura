{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function TBoxHub.GetBoxIndex(const name : string31) : dword;
// Returns the textbox[] index of a box with the given name, or -1 if not found.
begin
	if (lastGetBox < dword(length(textbox))) and (textbox[lastGetBox].boxName = name) then begin
		result := lastGetBox;
		exit;
	end;
	if length(textbox) <> 0 then for result := high(textbox) downto 0 do
		if textbox[result].boxName = name then begin
			lastGetBox := result;
			exit;
		end;
	result := high(dword);
end;

function TBoxHub.GetBox(const name : string31) : TTextBox;
// Returns a box object with the given name, or NIL if not found.
var i : dword;
begin
	result := NIL;
	i := GetBoxIndex(name);
	if i < dword(length(textbox)) then result := textbox[i];
end;

procedure TBoxHub.InsertBoxInList(const newbox : TTextBox);
var i, j : dword;
begin
	Assert(length(textbox) <> 0); // at least null box must exist already
	i := 1;
	while i < dword(length(textbox)) do begin
		if textbox[i].boxZLevel > newbox.boxZLevel then break;
		inc(i);
	end;
	setlength(textbox, length(textbox) + 1);
	for j := high(textbox) - 1 downto i do
		textbox[j + 1] := textbox[j];
	textbox[i] := newbox;
end;

procedure TBoxHub.RemoveBoxFromList(const removable : TTextBox);
// Only called from box destructor or on Z-level change.
var i : dword;
begin
	Assert(length(textbox) <> 0);
	i := length(textbox);
	while i <> 0 do begin
		dec(i);
		if textbox[i] = removable then begin
			while i < dword(high(textbox)) do begin
				textbox[i] := textbox[i + 1];
				inc(i);
			end;
			setlength(textbox, length(textbox) - 1);
			exit;
		end;
	end;
	LogError('bad box remove: ' + removable.boxName);
end;

// The topmost box is always the dropdown transcript/debug console. It slides down to visibility and up to vanish.
procedure TBoxHub.DropdownSlideDown;
begin
	with textbox[high(textbox)] do begin
		style.boxLanguage := textbox[0].style.boxLanguage; // reset language to current default
		boxState := EBoxState.Appearing;
		style.popType := EPopType.Fade;
		SetSize(32768, 14000);
		SetLocation(16384, 0, 16384, 32768);
		TEffectBoxMove.Create(NIL, 768, textbox[high(textbox)], 16384, 0, 16384, 0, EMoveType.Halfcos);
	end;
end;

procedure TBoxHub.DropdownSlideUp;
begin
	with textbox[high(textbox)] do begin
		StopTextInput;
		sysvar.dropdownConsoleState := EDDCState.None;
		boxState := EBoxState.Vanishing;
		style.popType := EPopType.Fade;
		TEffectBoxMove.Create(NIL, 768, textbox[high(textbox)], 16384, 0, 16384, 32768, EMoveType.Halfcos);
	end;
end;

procedure TBoxHub.HideAllBoxes(dohide : boolean);
var i : dword;
begin
	for i := high(textbox) downto 1 do with textbox[i] do
		if isHidden <> dohide then Hide(dohide);
	allBoxesHidden := dohide;
end;

function TBoxHub.CheckPageableBoxes : boolean;
// Checks all boxes for undisplayed content, where the box is not freely scrollable and has autowaitkey enabled. If any
// such box exists, it is paged ahead, and the function returns TRUE. If none found, returns FALSE.
var i : dword;
begin
	result := FALSE;
	for i := high(textbox) - 1 downto 1 do with textbox[i] do begin
		if (boxState = EBoxState.ShowText) and (NOT style.freescrollable) and (style.autoWaitKey)
		and ((NOT Choicematic.isActive) or (Choicematic.choiceBox <> textbox[i]))
		and (contentWin.scrollOfsP + longint(contentWin.contentWinSizeP.h) < longint(content.fullHeightP))
		then begin
			ScrollTo(
				contentWin.scrollOfs + longint((contentWin.contentWinSizeP.h shl 15) div contentWin.lineHeightP),
				-1, EMoveType.Halfcos, FALSE, NIL);
			result := TRUE;
		end;
	end;
end;

function TBoxHub.CompleteTextInput : boolean;
// Finds the topmost box with active user text input, and if any fibers are waiting for typing, resumes them and
// returns TRUE. If no box has active text input or fibers aren't waiting for it, returns FALSE.
// User input in the box is not deactivated and the box is not cleared.
var userinput : UTF8string = '';
	i, f : dword;
begin
	result := FALSE;
	Assert(length(textbox) <> 0);
	if FiberHub.fiberCount = 0 then exit;
	for i := high(textbox) downto 1 do with textbox[i].content do if caretPos >= 0 then begin
		setlength(userinput, userInputLen);
		if userInputLen <> 0 then move(txtContent[txtLength - userInputLen], userinput[1], userInputLen);
		for f := FiberHub.fiberCount - 1 downto 0 do with FiberHub.fiber[f] do
			if fiberState = EFiberState.WaitTyping then begin
				fiberState := EFiberState.Normal;
				PushString(userinput);
				PushInt(EStackToken.SingleStr);
				result := TRUE;
			end;
		break;
	end;
end;

function TBoxHub.GetBoxAt(xp, yp : longint) : dword;
// Returns the index of the topmost visible textbox at the given pixel coordinate relative to the game window.
// Compares against the padded area of the box. Returns -1 if no textbox occupies that pixel.
begin
	for result := high(textbox) downto 1 do with textbox[result] do
		if (boxState <> EBoxState.Null) and (isHidden = FALSE) then with boxRect do begin
			if (paddedLocP.x <= xp) and (paddedLocP.x + longint(paddedSizeP.w) > xp)
			and (paddedLocP.y <= yp) and (paddedLocP.y + longint(paddedSizeP.h) > yp)
			then exit;
		end;
	result := high(dword);
end;

function TBoxHub.ScrollUsingStick(stickvalue : longint; tickcount : dword) : boolean;
// Finds the topmost scrollable box, if any, and scrolls it by the analog stick value (range -32768..32767), taking
// into account the number of millisecond ticks since last frame.
// Returns TRUE if found a scrollable box, even if it wasn't scrollable further.
var i : dword;
	j : longint;
begin
	// Scroll topmost scrollable box.
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Null) and (NOT isHidden) and (style.freeScrollable) then with contentWin do begin
			// For best precision, standard practice is to square the stick value, but still keep the 32k range.
			j := SarLongint(stickvalue * abs(stickvalue), 15);
			// Multiply by tickcount to be framerate-independent. The shift-right scales it to a comfortable range.
			j := scrollOfs + SarLongint(j * longint(tickcount), 5);

			ScrollTo(j, tickcount);
			exit(TRUE);
		end;
	result := FALSE;
end;

procedure TBoxHub.PrintDebugBuffer;
// Prints the debug buffer in the dropdown console textbox.
var i : dword;
	comstash : UTF8string = '';
begin
	with textbox[high(textbox)] do with content do begin
		// Stash the current debug command line, if any.
		if userInputLen <> 0 then begin
			setlength(comstash, userInputLen);
			move(txtContent[txtLength - userInputLen], comstash[1], userInputLen);
		end;
		Clear(FALSE);

		with logbuffers do begin
			// Make sure the box's escape codes list is big enough for us.
			if length(debugBuffer) + 4 >= length(escapeList) then setlength(escapeList, length(debugBuffer) + 4);
			// Print the debug ring buffer.
			i := debugBufIndex;
			repeat
				if debugBuffer[i] <> '' then begin
					PrintFast(@debugBuffer[i][1], length(debugBuffer[i]));
					// Add a hard newline at the end of every line.
					escapeList[escapeCount].escapeOfs := txtLength;
					escapeList[escapeCount].escapeCode := byte('n');
					inc(escapeCount);
				end;
				i := (i + 1) and high(debugBuffer);
			until i = debugBufIndex;
		end;

		// Change text color for the debug command line.
		escapeList[escapeCount].escapeOfs := txtLength;
		escapeList[escapeCount].escapeCode := byte('c');
		escapeList[escapeCount].escapedata := $FFFFFFFF;
		inc(escapeCount);

		// Add an escape code for the caret position.
		escapeList[escapeCount].escapeOfs := txtLength + dword(caretPos);
		escapeList[escapeCount].escapeCode := 1;
		inc(escapeCount);

		// Reprint the current debug command line, if any.
		if userInputLen <> 0 then PrintFast(@comstash[1], userInputLen);

		// Mark the box for redrawing, scroll to the bottom.
		if boxState = EBoxState.Empty then boxState := EBoxState.ShowText;
		contentWin.scrollOfs := high(longint);
	end;
end;

procedure TBoxHub.PrintTranscriptBuffer;
// Prints the game transcript in the dropdown console textbox.
var i : dword;
begin
	with textbox[high(textbox)] do with content do begin
		Clear;

		with logbuffers do begin
			// Make sure the box's escape codes list is big enough for us.
			if length(transcriptBuffer) + 4 >= dword(length(escapeList)) then
				setlength(escapeList, length(transcriptBuffer) + 4);
			// Print the debug ring buffer.
			i := transcriptBufindex;
			repeat
				if transcriptBuffer[i] <> '' then begin
					PrintFast(@transcriptBuffer[i][1], length(transcriptBuffer[i]));
					// Add a hard newline at the end of every line.
					escapeList[escapeCount].escapeOfs := txtLength;
					escapeList[escapeCount].escapeCode := byte('n');
					inc(escapeCount);
				end;
				i := (i + 1) and high(transcriptBuffer);
			until i = transcriptBufindex;
		end;

		// Remove the last newline.
		if escapeCount <> 0 then dec(escapeCount);

		// Mark the box for redrawing, scroll to the bottom.
		if boxState = EBoxState.Empty then boxState := EBoxState.ShowText;
		contentWin.scrollOfs := high(longint);
	end;
end;

// ------------------------------------------------------------------

{$include box-implementation.pas}

procedure TBoxHub.TextBoxer(tickcount : dword);
// Called by main loop. Recalculates textbox parameters and makes sure all buffers are up to date, so Renderer can just
// blit the final buffer of every visible textbox.
var boxnum : dword;
	rehilite : boolean = FALSE;

	procedure _UpdateBoxPop;
	begin
		with textbox[boxnum] do begin
			if boxState = EBoxState.Vanishing then begin
				AddRefresh;
				{$ifdef sakucon}
				RefreshOverlappingBoxes;
				{$endif}
			end;

			with boxRect do with Viewportmatic.viewport[boxInViewport] do begin
				renderSizeP.w := (renderSizeP.w * dword(popRunTimeMsec) + style.popTimeMsec shr 1) div style.popTimeMsec;
				renderSizeP.h := (renderSizeP.h * dword(popRunTimeMsec) + style.popTimeMsec shr 1) div style.popTimeMsec;
				renderLocP := marginedLocP;
				dec(renderLocP.x, marginP.left + ((renderSizeP.w - marginP.left - marginP.right) * boxAnchor.x + 16384) shr 15);
				dec(renderLocP.y, marginP.top + ((renderSizeP.h - marginP.top - marginP.bottom) * boxAnchor.y + 16384) shr 15);
			end;
		end;
	end;

	procedure _UpdateBox(_box : TTextBox);
	begin
		with _box do begin
			if parametersNeedUpdate then UpdateParameters;
			if contentNeedsRender then begin
				// Figure out where implicit linebreaks go, resize the box accordingly.
				if FlowContent then
					RenderContent;
				DeriveLocP;
				// If scroll position is set to max value, reset it to the largest useful scroll value.
				// Particularly used by the dropdown console.
				with contentWin do
					if scrollOfs = high(scrollOfs) then ScrollTo(high(longint), 0);

				// If this is the choiceBox with fresh content drawn up, the highlight box needs to update immediately.
				with Choicematic do if (isActive) and (_box = choiceBox) and (boxState = EBoxState.ShowText) then
					rehilite := TRUE;
			end;
			// FlowContent may change box size, so render base after it.
			if baseNeedsRender then RenderBase;
			if finalNeedsRender then RenderFinal;
			if refreshLocP then DeriveLocP;

			// Apply autovanish to visible empty boxes.
			if (boxState = EBoxState.Empty) and (style.autovanish) then boxState := EBoxState.Vanishing;

			// Handle timing for appearing/vanishing boxes.
			case boxState of
				EBoxState.Appearing:
				begin
					inc(popRunTimeMsec, longint(tickcount));
					if (dword(popRunTimeMsec) >= style.popTimeMsec) or (style.popType = EPopType.Instant) then begin
						popRunTimeMsec := style.popTimeMsec;
						boxState := EBoxState.ShowText;
						// If the choiceBox just finished popping in, the highlight needs an update before rendering.
						with Choicematic do if (isActive) and (_box = choiceBox) then rehilite := TRUE;
					end;
					boxNeedsRedraw := TRUE;
				end;

				EBoxState.Vanishing:
				begin
					dec(popRunTimeMsec, tickcount);
					if (popRunTimeMsec <= 0) or (style.popType = EPopType.Instant) then begin
						popRunTimeMsec := 0;
						boxState := EBoxState.Null;
						// The box has vanished completely; draw over it.
						AddRefresh;
						{$ifdef sakucon}
						RefreshOverlappingBoxes;
						{$endif}
					end
					else boxNeedsRedraw := TRUE;
				end;
			end;
		end;
	end;

begin
	// Just-in-time printout of debug/transcript for dropdown console.
	with sysvar do if dropdownContentChanged then begin
		case dropdownConsoleState of
			EDDCState.Log: PrintTranscriptBuffer;
			EDDCState.Debug: PrintDebugBuffer;
		end;
		dropdownContentChanged := FALSE;
	end;

	for boxnum := 1 to high(textbox) do with textbox[boxnum] do begin
		if (boxState = EBoxState.Null) or (isHidden) or (textbox[boxnum] = Choicematic.highlightBox) then continue;
		_UpdateBox(textbox[boxnum]);
	end;

	// All boxes now have up-to-date pixel locations. Any box whose position changed still has refreshLocP set.
	// The highlight box depends on the choice box's pixel location, so best set the highlight separately here.
	with Choicematic do begin
		if rehilite then HighlightChoice(highlightIndex, EMoveType.Instant);
		if (highlightBox <> NIL) and (highlightBox.boxState <> EBoxState.Null) and (NOT highlightBox.isHidden) then
			_UpdateBox(highlightBox);
	end;

	for boxnum := 1 to high(textbox) do with textbox[boxnum] do begin
		if (boxState = EBoxState.Null) or (isHidden) then continue;

		// Boxes can finally snap to each other correctly, if either snapper or snappee had refreshLocP.
		if snapToBoxName <> '' then begin
			if snapToBox = NIL then snapToBox := BoxHub.GetBox(snapToBoxName);
			if (refreshLocP) or ((snapToBox <> NIL) and (snapToBox.refreshLocP)) then DeriveLocP;
		end;
		refreshLocP := FALSE;

		if boxNeedsRedraw then with boxRect do begin
			// If the rendering position has changed at all from the previous frame, redraw anything covered by the
			// previous.
			if (marginedLocP.x <> renderLocP.x) or (marginedLocP.y <> renderLocP.y)
			or (marginedSizeP.w <> renderSizeP.w) or (marginedSizeP.h <> renderSizeP.h)
			then begin
				//log(strcat('box clear: renderlocp %, rendersizep %', [renderLocP.ToString, renderSizeP.ToString]));
				AddRefresh;
				{$ifdef sakucon}
				RefreshOverlappingBoxes;
				{$endif}
			end;

			renderSizeP := marginedSizeP;
			renderLocP := marginedLocP;

			// Calculate pop-in/pop-out size if necessary.
			if (boxState in [EBoxState.Appearing, EBoxState.Vanishing]) and (style.popType = EPopType.GrowShrink) then
				_UpdateBoxPop();

			{log(strcat('box %: loc=% renderlocp=% sizep=% rendersizep=%',
				[boxName, boxLoc.ToString, renderLocP.ToString, marginedSizeP.ToString, renderSizeP.ToString]));}

			{$ifndef sakucon}
			// Graphical textboxes get drawn like any gob, so just add the box's position as a refresh region.
			AddRefresh;
			boxNeedsRedraw := FALSE;
			{$endif}
			// Console textboxes get special treatment. Because of the difficulty in printing textbox content in
			// console mode, boxes are always drawn fully by the console renderer.
		end;
	end;
end;

procedure TBoxHub.InheritState(const src : TBoxHub);
var _box, newbox : TTextBox;
	i : longint;
begin
	// Force new boxhub to have the same index order of boxes as source, by deleting everything from first mismatch up.
	i := 1;
	while (i < length(textbox)) and (i < length(src.textbox)) and (textbox[i].boxName = src.textbox[i].boxName) do inc(i);
	while i < length(textbox) do begin textbox[i].Destroy; inc(i); end;

	for _box in src.textbox do begin
		newbox := GetBox(_box.boxName);
		if newbox = NIL then newbox := TTextBox.Create(_box.boxName);
		newbox.CloneFrom(_box, TRUE);
		// Can't page or scroll boxes belonging to a previous level, they're frozen as they are.
		newbox.style.autoWaitkey := FALSE;
		newbox.style.freeScrollable := FALSE;
		if newbox.boxState in [EBoxState.Appearing, EBoxState.Vanishing] then newbox.boxState := EBoxState.Null;
	end;

	Assert(src.defaultTextbox <> NIL);
	defaultTextbox := GetBox(src.defaultTextbox.boxName);
	Assert(defaultTextbox <> NIL);

	if src.dialogueTitleBox <> NIL then
		dialogueTitleBox := GetBox(src.dialogueTitleBox.boxName)
	else
		dialogueTitleBox := NIL;
end;

procedure TBoxHub.ResetHub;
var i : dword;
begin
	defaultTextbox := NIL;
	dialogueTitleBox := NIL;
	allBoxesHidden := FALSE;
	delayedClear := FALSE;

	// Null box first, others use it as template.
	if length(textbox) = 0 then
		TTextBox.Create('NULL')
	else begin
		textbox[0].Reset;
		for i := high(textbox) downto 1 do begin
			case textbox[i].boxName of
				'MAINBOX', 'HIGHLIGHT', chr(1): textbox[i].Reset;
				else textbox[i].Destroy;
			end;
		end;
	end;

	with Choicematic do begin
		if isActive then Deactivate(TRUE, FALSE);
		choicePartBox := NIL;
		choiceBox := GetBox('MAINBOX');
		if choiceBox = NIL then choiceBox := TTextBox.Create('MAINBOX');
	end;
	if GetBoxIndex('HIGHLIGHT') >= dword(length(textbox)) then TTextBox.Create('HIGHLIGHT');
	if GetBoxIndex(chr(1)) >= dword(length(textbox)) then TTextBox.Create(chr(1)); // dropdown console

	activeTextInput := 0;
end;

function TBoxHub.Serialise : TStringBunch;
var box : TTextBox;
	i, j : dword;
begin
	result := NIL;
	i := 0;
	for box in textbox do
		inc(i, dword(length(box.style.decorList)) + box.content.escapeCount + box.content.choiceCount);
	setlength(result, 2 + dword(length(textbox)) * 17 + i);

	j := 0;
	for box in textbox do with box do begin
		if boxName = chr(1) then continue; // don't serialise dropdown console

		// Box params don't have helpful colons to separate keys and values so "thing -5" would assume you want to
		// subtract 5 from the string thing, which is not allowed. Wrap signed numbers in brackets to avoid.
		result[j] := strcat('tbox.setparam box:`%` viewport % fontheight % linespacing % textalign (%)',
			[boxName, boxInViewport, boxFont.origFontHeight, contentWin.linespacing, style.textAlign]);
		inc(j);
		with contentWin do begin
			result[j] := strcat('|: mincols % minrows % maxcols % maxrows % zlevel (%)',
				[fitMinCols, fitMinRows, fitMaxCols, fitMaxRows, boxZLevel]);
			inc(j);
			result[j] := strcat('|: minsizex % minsizey % maxsizex % maxsizey %',
				[paddedMinSize.w, paddedMinSize.h, paddedMaxSize.w, paddedMaxSize.h]);
			inc(j);
			result[j] := strcat('|: columns % columnpadding (%)', [columnCount, columnPadding]);
			inc(j);
		end;
		with boxRect do
			result[j] := strcat('|: padleft % padright % padtop % padbottom %',
				[padding.left, padding.right, padding.top, padding.bottom]);
		inc(j);
		result[j] := strcat('|: snaptobox `%` snaptoanchorx (%) snaptoanchory (%) exporttobox `%`',
			[snapToBoxName, snapToAnchor.x, snapToAnchor.y, content.exportToBoxName]);
		inc(j);
		with style do begin
			case baseFill of
				EFillMode.None: result[j] := '|: basefill 0';
				EFillMode.Flat: result[j] := '|: basecolor 0x' + strhex(baseColor[0].ToRGBA4);
				EFillMode.XGradient:
					result[j] := strcat('|: basecolor0 0x& basecolor1 0x& basecolor2 0x& basecolor3 0x&',
					[baseColor[0].ToRGBA4, baseColor[1].ToRGBA4, baseColor[2].ToRGBA4, baseColor[3].ToRGBA4]);
			end;
			inc(j);
			result[j] := strcat('|: textcolor 0x& language `%`',
				[textColor.ToRGBA4, languageList[boxLanguage]]);
			inc(j);
			result[j] := strcat('|: blendmode % poptype % poptime % bevel % underlineallrows %',
				[boxBlendMode, popType, popTimeMsec, doBevel, underlineAllRows]);
			inc(j);
			result[j] := strcat(
				'|: alwaysinviewport % autosizecolumns % freescrollable % autowaitkey % autovanish %',
				[alwaysInViewport, autoSizeColumns, freeScrollable, autoWaitkey, autoVanish]);
			inc(j);
			with scrollbar do result[j] := strcat('|: scrollcapcolor 0x& scrolltroughcolor 0x& scrollthumbcolor 0x&',
				[capColor.ToRGBA4, troughColor.ToRGBA4, thumbColor.ToRGBA4]);
			inc(j);
			with texture.texTileMarginsP_original do
				result[j] := strcat('|: texleftedge % texrightedge % textopedge % texbottomedge %',
					[left, right, top, bottom]);
			inc(j);
			if texture.texType <> ETextureType.None then begin
				result[j] := strcat('tbox.settexture box:`%` gob:`%` type:% style:% frame:%',
					[boxName, texture.texName, texture.texType, texture.texBlendMode, texture.texFrameIndex]);
				inc(j);
			end;
			if length(decorList) <> 0 then for i := 0 to high(decorList) do with decorList[i] do begin
				result[j] := strcat('tbox.decorate `%` gob:`%` x:% y:% sizex:% sizey:% ax:% ay:% frame:%',
					[boxName, decorName, decorLoc.x, decorLoc.y, decorSize.w, decorSize.h,
					decorAnchor.x, decorAnchor.y, decorFrameIndex]);
				inc(j);
			end;
			{$note todo: serialise box outlines}
		end;
		with content do begin
			result[j] := 'tbox.clear ' + boxName;
			inc(j);
			// Ensure text content is null-terminated.
			if txtLength >= dword(length(txtContent)) then setlength(txtContent, txtLength + 1);
			txtContent[txtLength] := 0;

			if txtLength <> 0 then begin
				// Force-convert all backticks to apostrophes to guarantee a clean print command.
				for i := txtLength - 1 downto 0 do
					if txtContent[i] = ord('`') then txtContent[i] := ord('''');
				result[j] := strcat('print box:`%` `%`', [boxName, PChar(@txtContent[0])]);
				// If user input is active in this box, cut the input part of the box context.
				if (caretPos >= 0) and (userInputLen <> 0) then begin
					setlength(result[j], result[j].Length - userInputLen);
					result[j][result[j].Length] := '`';
				end;
			end
			else result[j] := 'print box:`' + boxName + '` ``';
			inc(j);
			// Escapes are inserted directly at content offsets, rather than interspersed in text. Must be in order.
			if escapeCount <> 0 then for i := 0 to escapeCount - 1 do with escapeList[i] do begin
				if char(escapeCode) in [#0, #1, '0'] then continue;
				result[j] := '';
				if char(escapeCode) = 'c' then result[j] := strhex(RGBAquad(escapeData).ToRGBA4) + ';';
				result[j] := strcat('|: `\@%;\%%`', [escapeOfs, char(escapeCode), result[j]]);
				inc(j);
			end;
			// Choice indexes are saved to keep choiceList items associated with correct nodes.
			if choiceCount <> 0 then for i := choiceCount - 1 downto 0 do begin
				result[j] := strcat('|: `\#%=%;`', [i, choiceList[i].index]);
				inc(j);
			end;
			// If user input is active in this box, issue command to reactivate it with correct string and caret pos.
			if caretPos >= 0 then begin
				result[j] := strcat('tbox.userinputstart `%` text:`%` index:%',
					[boxName, PChar(@txtContent[txtLength - userInputLen]), caretPos]);
				inc(j);
			end;
		end;
		if (boxState in [EBoxState.Null, EBoxState.Vanishing])
		or ((boxState = EBoxState.Empty) and (style.autoVanish)) then
			result[j] := 'tbox.popout `' + boxName + '`'
		else
			result[j] := 'tbox.popin `' + boxName + '`';
		inc(j);
		if contentWin.scrollOfs <> 0 then begin
			result[j] := strcat('tbox.scroll `%` y:%', [boxName, contentWin.scrollOfs]);
			inc(j);
		end;
		with boxRect do
			result[j] := strcat('tbox.setloc `%` x:% y:% ax:% ay:%',
				[boxName, boxLoc.x, boxLoc.y, boxAnchor.x, boxAnchor.y]);
		inc(j);
	end;
	setlength(result, j);
end;

destructor TBoxHub.Destroy;
var i : dword;
begin
	if length(textbox) <> 0 then for i := high(textbox) downto 0 do textbox[i].Destroy;
	inherited;
end;

