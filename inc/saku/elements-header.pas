{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Element description data (size, location, types) is stored in TElements. Elements are abstract, and represent
// anything that has a graphics-related presence.
// Elements are drawn from 0 upwards, and are expected to have an ascending hierarchy where front elements are children
// of back elements. For example, sprites are children of a background picture, and blinking animations are children of
// the character sprites. If an element moves, its kids move along; if destroyed, its kids are too.
type TElement = class
	eleName : string31;

	// Any element can be a child of another element in the same viewport. The parent element object is kept here.
	// An element can have multiple children but only one parent. Most will be adopted by the viewport's background,
	// if any. If the parent moves or is removed or whatever, children go right along. A NIL parent means the element
	// is a background or overlay.
	eleParent : TElement;
	eleViewport : dword; // the element is positioned, clipped, and sized within this viewport

	// 32k coordinates of anchor point within the element rectangle. 0,0 aligns the element by its top left corner,
	// 32k,32k by its bottom right. A perfectly centered element of any size would have a 16k,16k anchor, at viewport
	// coordinates 16k,16k. Values outside 0..32k are allowed.
	eleAnchor : TCoord32k;
	// Element's 32k location relative to its containing viewport, and pixel location relative to the game window.
	// If the backing graphic object has a constant position offset, that's included in this pixel location.
	eleLoc : TCoords;
	eleDisplacement : TCoord32k; // temporary visual displacement, while element retains its logical position
	eleSize : TSize32k; // frame 32k size, relative to its containing viewport
	eleSizeP : TSizeP; // frame pixel size
	eleZLevel : longint; // automatic element sorting: higher Z value (more frontward) -> higher element index
	// sizep = sizep * multiplier div 32768, 32k = 100%; not fully implemented...
	eleSizeMultiplier : dword;

	eleFrameP : pointer; // NIL, or the start of a 32bpp bitmap of eleSizeP
	solidBlit : RGBAquad; // if non-zero, element is colorised with this color
	eleAlpha : byte; // 0 = transparent, 255 = fully visible (default); this is multiplied by the graphic's own alpha

	eleRenderNow : boolean; // draw/redraw at next Renderer pass, unless element is removed before that
	eleIsVisible : boolean;
	refreshLocP : boolean; // if TRUE, the element's 32k location has changed: call UpdateLocP before next render

	function GetCurrentIndex : dword;
	procedure AddRefresh; inline;
	procedure SetParent(const parentname : string31);
	procedure UpdateLocP;
	procedure UpdateSizeP;
	procedure Displace(deltax, deltay : longint);
	procedure SetLocation(x, y, anchorx, anchory : longint);
	procedure SetSolidBlit(blitcolor : dword);
	//function ToString : UTF8string; override;

	constructor Create(
		const _elename, _parent : string31;
		inviewport, locx, locy, zlevel, anchorx, anchory, sizex, sizey, index : longint);
	destructor Destroy; override;
end;

type TEleHub = class
	ele : array of TElement;
	numElements : dword;
	defaultViewport : dword; // new elements are relative to this by default

	function GetElementIndex(const name : string31) : dword;
	function GetElement(const name : string31) : TElement;
	procedure InsertInList(newelement : TElement; atindex : dword);
	procedure DeleteFromList(deleteindex : dword);
	procedure ReplaceInList(newelement : TElement; index : dword);
	procedure InheritState(const src : TEleHub);
	procedure Reset;
	procedure UpdateAll(tickcount : dword); inline;
	function Serialise : TStringBunch;
	destructor Destroy; override;
end;

