{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Game state save/load handling.
//
// The save file format (words stored x86-style LSB first):
//   signature "SSav" : DWORD = $76615353;
//   used dat count : BYTE; (front end is always implicitly included so not listed here)
//   used dat names : array of UTF-8 ministring;
//   save description : UTF-8 ministring; (empty for Autosave)
//   thumbnail bytesize : DWORD; (can be 0 for none)
//   thumbnail data : array of byte; (passed to mcgloder's generic loader so can be any format; save shrunk to 1/16th of window pixel size)
//   uncompressed savestate bytesize : DWORD; (compressed size is from next byte to EOF; if comp=uncomp, read raw)
//   compressed savestate : array of byte;
// Savestate:
//   save script bytesize : DWORD;
//   save script : array of UTF-8 char; (this is compiled and run immediately on load)
//   fiber info bytesize : DWORD;
//   fiber info : array of byte;
//   variable chunk bytesize : DWORD;
//   variable savestate : array of byte;

const savesig : dword = $76615353;

type TSaveFile = record
	usedDatList : TStringBunch;
	description : UTF8string;
	timestamp : string;
	filePath : UTF8string;
	thumbnailData : array of byte; // converted to temporary graphic on access
	srcFile : file;
	savestateOfs : dword;
end;

type TSavematic = object
	availableSaveList : array of TSaveFile;
	lastAutoSaveTime : qword;
	saveNow : boolean;
	saveNowIndex : dword;
	saveNowDesc : UTF8string;

	private
    procedure SaveState(var writer : TBufferedFileWriter; const savedesc : UTF8string);
	function GetNewSaveFile(autosave : boolean) : UTF8string;
	function GetNextAutoSaveFile : UTF8string;

	public
    function SaveStateToFile(const filename, savedesc : UTF8string) : boolean;
	procedure EnumerateSaves(onlyautosaves : boolean);
	procedure TryAutoSave;
	function LoadState(savefile : TSaveFile) : boolean;
	procedure ReadSeenGFX;
	procedure SaveGlobals;
	//procedure Destroy;
end;

var Savematic : TSavematic;

// ------------------------------------------------------------------

procedure TSavematic.EnumerateSaves(onlyautosaves : boolean);
// Populates availableSaveList with all compatible save files from known base/save and profile/save.
// If onlyautosaves is TRUE, quickly gets a minimal list of relevant autosave files.
// The files are sorted by most recent modification time, youngest first.
var filelist : TStringBunch;
	datetime : TDateTime;
	txt, txt2 : UTF8string;
	i, j, l, filecount, availablesavecount : dword;
	b : byte;

	procedure _SortList;
	var tempsave : TSaveFile;
		c : dword;
	begin
		if availablesavecount < 2 then exit;
		// Backward insertion sort.
		for c := availablesavecount - 2 downto 0 do begin
			tempsave := availableSaveList[c];
			j := c + 1;
			while (j <> availablesavecount) and (availableSaveList[j].timestamp > tempsave.timestamp) do begin
				availableSaveList[j - 1] := availableSaveList[j];
				inc(j);
			end;
			availableSaveList[j - 1] := tempsave;
		end;
	end;

begin
	setlength(availableSaveList, 0);
	txt := PathCombine([sakuparam.basePath, 'save', '*']);
	txt2 := PathCombine([GetAppConfigDir(FALSE), 'save', '*']);
	filelist := concat(FindFiles_caseless(txt), FindFiles_caseless(txt2));
	filecount := filelist.Length;
	if filecount = 0 then exit;

	j := 0; b := 0; // silence compiler
	availablesavecount := 0;
	setlength(availableSaveList, filecount);
	for i := filecount - 1 downto 0 do begin
		txt := lowercase(ExtractFileName(filelist[i]));
		// Expected filename: <projectname>.[auto]<savenumber>.sav
		if (txt.Length < 7) or (copy(txt, txt.Length - 3) <> '.sav') then continue;
		txt2 := ExtractFileExt(ExtractFileNameWithoutExt(txt));
		if txt2 = '.' then continue;

		with availableSaveList[availablesavecount] do begin
			assign(srcFile, filelist[i]);
			filemode := 0; reset(srcFile, 1); // readonly
			l := filesize(srcFile);
			if l < 26 then begin log('too small file ' + filelist[i]); close(srcFile); continue; end;

			blockread(srcFile, j, 4);
			if LEtoN(j) <> savesig then begin log('bad sig in ' + filelist[i]); close(srcFile); continue; end;

			blockread(srcFile, b, 1);
			setlength(usedDatList, 0);
			setlength(usedDatList, b);

			j := 0;
			while j < usedDatList.Length do begin
				blockread(srcFile, b, 1);
				if (b = 0) or (filepos(srcFile) + b + 4 >= l) then begin
					log('corrupted dat list in ' + filelist[i]);
					close(srcFile);
					break;
				end;
				setlength(usedDatList[j], b);
				blockread(srcFile, usedDatList[j][1], b);
				usedDatList[j] := lowercase(usedDatList[j]);
				inc(j);
			end;
			if j < usedDatList.Length then continue;

			// If currently in a game (loaded another dat beside front end), save file's used dat list must start with
			// the same dat as was loaded first (save file is for the current game).
			if length(loadedDatList) >= 2 then
				if (usedDatList.Length = 0) or (usedDatList[0] <> loadedDatList[1].projectName)
					then begin close(srcFile); continue; end;

			blockread(srcFile, b, 1);
			if ((onlyautosaves) and (b <> 0)) or (filepos(srcFile) + b + 4 >= l) then begin
				close(srcFile); continue;
			end;
			description := '';
			if b <> 0 then begin
				setlength(description, b);
				blockread(srcFile, description[1], b);
			end;

			blockread(srcFile, j, 4);
			if filepos(srcFile) + j + 4 >= l then begin close(srcFile); continue; end;
			setlength(thumbnailData, 0);
			if onlyautosaves then
				savestateOfs := filepos(srcFile) + j
			else begin
				setlength(thumbnailData, j);
				if j <> 0 then blockread(srcFile, thumbnailData[0], j);
				savestateOfs := filepos(srcFile);
			end;

			close(srcFile);
			filePath := filelist[i];
			timestamp := 'unknown';
			if FileAge(filelist[i], datetime) then timestamp := FormatDateTime('yyyy/mm/dd hh:nn:ss', datetime);
		end;
		inc(availablesavecount);
	end;
	setlength(availableSaveList, availablesavecount);

	_SortList;
end;

procedure TSavematic.SaveState(var writer : TBufferedFileWriter; const savedesc : UTF8string);
// Serialises the current game state into the given file. It is the caller's responsibility to close the writer.
// Throws exception on errors.
var i, j : dword;
	uncompressedsize : dword = 0;
	savedata : array of byte = NIL;

	procedure _BuildSaveData;
	var savescript, fiberdata : TStringBunch;
		vardata, writep : pointer;
		l, scriptbytes, vardatabytes, fiberbytes : dword;
	begin
		with HubStack do begin
			l := stackIndex;
			SelectHubLevel(0);
			savescript := NIL;
			setlength(savescript, 1);
			savescript[0] := '(mus.play ' + sysvar.currentSong + ')(stop)'; // should also seek
			// Order of serialisation is critical, viewports first, then elements, effects last.
			savescript := concat(Viewportmatic.Serialise, EleHub.Serialise, BoxHub.Serialise,
				Eventmatic.Serialise, Choicematic.Serialise, EffectHub.Serialise, savescript);
			vardatabytes := Varmon.SaveVarState(vardata);
			fiberdata := FiberHub.Serialise;
			SelectHubLevel(l);
		end;

		scriptbytes := length(savescript); // extra newline after each string
		for l := high(savescript) downto 0 do
			inc(scriptbytes, savescript[l].Length);
		fiberbytes := 0;
		if fiberdata.Length <> 0 then for l := high(fiberdata) downto 0 do
			inc(fiberbytes, fiberdata[l].Length);
		uncompressedsize := scriptbytes + vardatabytes + fiberbytes + 12;
		setlength(savedata, uncompressedsize);

		writep := @savedata[0];
		dword(writep^) := NtoLE(scriptbytes); inc(writep, 4);
		for l := 0 to high(savescript) do begin
			if savescript[l].Length <> 0 then begin
				move(savescript[l][1], writep^, savescript[l].Length);
				inc(writep, savescript[l].Length);
			end;
			byte(writep^) := $A; inc(writep); // implicit newline
		end;
		dword(writep^) := NtoLE(fiberbytes); inc(writep, 4);
		if fiberdata.Length <> 0 then for l := 0 to high(fiberdata) do begin
			move(fiberdata[l][1], writep^, fiberdata[l].Length);
			inc(writep, fiberdata[l].Length);
		end;
		dword(writep^) := NtoLE(vardatabytes); inc(writep, 4);
		move(vardata^, writep^, vardatabytes);

		// CompressSaveData;

		freemem(vardata); vardata := NIL;
	end;

begin
	writer.WriteDword(NtoLE(savesig));

	// List all loaded dats beside front end dat.
	writer.WriteByte(length(loadedDatList) - 1);
	i := 1; // front end dat is always implicitly included so start from 1
	while i < dword(length(loadedDatList)) do with loadedDatList[i] do begin
		j := projectName.Length;
		Assert((j <> 0) and (j <= 255));
		writer.WriteByte(j);
		writer.WriteBytes(@projectName[1], j);
		inc(i);
	end;

	i := savedesc.Length; if i > 255 then i := 255;
	writer.WriteByte(i);
	if i <> 0 then writer.WriteBytes(@savedesc[1], i);

	writer.WriteDword(NtoLE(dword(0))); // thumbnail

	_BuildSaveData;
	writer.WriteDword(NtoLE(uncompressedsize));
	writer.WriteBytes(@savedata[0], length(savedata));
end;

function TSavematic.SaveStateToFile(const filename, savedesc : UTF8string) : boolean;
// Generates and outputs a savestate into the given file. Tries in base/save first, then profile/save.
// If the file already exists, overwrites it.
// Returns TRUE for success and shows an OK message box. Also shows an error message box in the GUI in case of errors.
var filepath : UTF8string;
	writer : TBufferedFileWriter = NIL;
begin
	result := TRUE;
	try try
		try
			filepath := PathCombine([sakuparam.basePath, 'save', filename]);
			writer := TBufferedFileWriter.Create(filepath, NIL, 0);
			SaveState(writer, savedesc);
		except on e : EAccessDenied do begin
			log(e.Message);
			filepath := PathCombine([GetAppConfigDir(FALSE), 'save', filename]);
			writer := TBufferedFileWriter.Create(filepath, NIL, 0);
			SaveState(writer, savedesc);
		end; end;
		log('saved state: ' + filepath);
		SummonMessageBox('Game saved.');

	except on e : Exception do begin
		result := FALSE;
		LogError('Failed to save game', e, ExceptAddr);
	end; end;
	finally
		saveNow := FALSE;
		saveNowDesc := '';
		if writer <> NIL then begin writer.Destroy; writer := NIL; end;
	end;
end;

function TSavematic.GetNewSaveFile(autosave : boolean) : UTF8string;
// Returns the filename for a new normal save file. EnumerateSaves must have been called first.
var save : TSaveFile;
	base : string;
	i : dword;
begin
	base := sysvar.activeProjectName + '.';
	if autosave then base := base + 'auto';
	i := 0;
	repeat
		inc(i);
		result := base + strdec(i) + '.sav';
		for save in availableSaveList do
			if lowercase(ExtractFileName(save.filePath)) = result then continue;
	until TRUE;
end;

function TSavematic.GetNextAutoSaveFile : UTF8string;
// Returns the filename for the next new or rotating autosave file.
begin
	EnumerateSaves(TRUE);
	if (length(availableSaveList) = 0) or (length(availableSaveList) < preference.autoSaveSlots) then begin
		result := GetNewSaveFile(TRUE);
		exit;
	end;
	result := ExtractFileName(availableSaveList[high(availableSaveList)].filePath);
end;

procedure TSavematic.TryAutoSave;
// If it's been long enough since the last autosave, generates and outputs a savestate into the next appropriate
// autosave file. Resets the autosave timer. Call this periodically during gameplay.
// Success is transparent, failure shows a GUI message.
var t : qword;
begin
	if preference.autoSaveSlots <= 0 then exit;
	t := GetTickCount64;
	if t < lastAutoSaveTime + preference.autoSaveAfterMSec then exit;
	SaveStateToFile(GetNextAutoSaveFile, '');
	lastAutoSaveTime := t;
end;

function TSavematic.LoadState(savefile : TSaveFile) : boolean;
// Tries to load the given file, deserialising into current game state. Returns TRUE for success.
// Also shows an error message box in the GUI in case of errors.
var txt : UTF8string;
	i, statesize, compressedsize : dword;
	scriptsize, varchunksize, fiberinfosize : dword;
	data, p : pointer;
begin
	result := FALSE;
	data := NIL;
	// If currently in a game, assume current dat list is compatible with save's dats.
	// If in front end, load the game dats first.
	if length(loadedDatList) < 2 then begin
		for txt in savefile.usedDatList do
			if NOT LoadDatCommon(txt) then exit;
	end;

	filemode := 0; reset(savefile.srcFile, 1); // readonly
	try try
		i := filesize(savefile.srcFile);
		if savefile.savestateOfs + 4 >= i then raise Exception.Create('savestate ofs out of bounds');
		seek(savefile.srcFile, savefile.savestateOfs);

		statesize := 0;
		blockread(savefile.srcFile, statesize, 4);
		statesize := LEtoN(statesize);
		compressedsize := i - filepos(savefile.srcFile);
		getmem(data, compressedsize);
		if compressedsize <> 0 then blockread(savefile.srcFile, data^, compressedsize);
		log(strcat('size % -> %', [compressedsize, statesize]));

		//if statesize <> compressedsize then UncompressSaveData;

		if statesize < 12 then raise Exception.Create('file too tiny');
		scriptsize := LEtoN(dword(data^));
		if scriptsize + 12 > statesize then raise Exception.Create('script out of bounds');

		fiberinfosize := LEtoN(dword((data + 4 + scriptsize)^));
		i := fiberinfosize + scriptsize;
		if i + 12 > statesize then raise Exception.Create('fiberinfo out of bounds');

		varchunksize := LEtoN(dword((data + 8 + i)^));
		if i + varchunksize + 12 > statesize then raise Exception.Create('varchunk out of bounds');

		// First try to build the game state setup script.
		//txt := ''; setlength(txt, scriptsize); move((data + 4)^, txt[1], scriptsize); log(txt);
		p := CompileScript('', data + 4, data + scriptsize + 4);
		if p <> NIL then begin
			txt := 'script error: ' + string(p^);
			freemem(p); p := NIL;
			raise Exception.Create(txt);
		end;

		HubStack.Push(FALSE);
		try
			// Using a fresh hub level, restore the fibers.
			p := data + 8 + scriptsize;
			FiberHub.Deserialise(p, p + fiberinfosize);

			// Run the game state setup script.
			i := HubStack.stackIndex;
			TFiber.Create('', chr(0));
			FiberHub.exitOnAnyError := TRUE;
			FiberHub.RunFibers;
			if HubStack.stackIndex <> i then raise Exception.Create('script changed metalevel');
			if FiberHub.GetFiber('') < FiberHub.fiberCount then raise Exception.Create('script did not complete');

			// Finally import the variables. (Variables must be last since it's global state.)
			Varmon.LoadVarState(data + 12 + scriptsize + fiberinfosize, varchunksize);
			// Make sure all engine vars remain hooked up, if save file was from an older engine version.
			InitVarmon;
		except
			HubStack.Pop;
			raise;
		end;

		// The new hub level now contains the saved state. Throw away the old hubs, only keep the new one.
		HubStack.ResetToTopmost;
		sysvar.metaState := EMetaState.Normal;
		result := TRUE;

	except on e : Exception do
		LogError('Failed to load state', e, ExceptAddr);
	end;
	finally
		close(savefile.srcFile);
		if data <> NIL then begin freemem(data); data := NIL; end;
	end;
end;

// ------------------------------------------------------------------

procedure TSavematic.ReadSeenGFX;
// Initialises the seengfx list, opens the .SAV file, reads previously seen graphics into the list.
{$ifdef bonk}
var filu : file;
	i, j : dword;
	tux : string;
begin
	j := 0; seenGfxItems := 0; seenGfxSize := 4096;
	if seenGfxP <> NIL then begin freemem(seenGfxP); seenGfxP := NIL; end;
	getmem(seenGfxP, seenGfxSize);

	tux := sakuparam.basePath + sysvar.activeProjectName;
	while (tux <> '') and (tux[length(tux)] <> '.') do dec(byte(tux[0]));
	if tux = '' then halt(81);
	tux := tux + 'sav';
	assign(filu, tux);
	filemode := 0; reset(filu, 1); // read-only access
	i := IOresult;
	if i = 0 then begin
		// Read the signature.
		blockread(filu, j, 4);
		if j <> $CBCABAAB then
			LogError('Wrong sig in SAV file')
		else begin
			// Read the list size.
			blockread(filu, seenGfxItems, 2);
			i := (seenGfxItems * 16 + 1024) and $FFFFFF00;
			if i > seenGfxSize then begin
				freemem(seenGfxP); seenGfxP := NIL;
				seenGfxSize := i;
				getmem(seenGfxP, seenGfxSize);
			end;
			blockread(filu, seenGfxP^, seenGfxItems * 16);
			Log('Seen gfx in SAV: ' + strdec(seenGfxItems));
		end;
		close(filu);
	end
	else LogError('No SAV file found');

	while IOresult <> 0 do ;
end;
{$else}
begin end;
{$endif}

procedure TSavematic.SaveGlobals;
// Attempts to write the seen graphics and strings lists into a SAV file.
var filu : file;
	i, j : dword;
	tux : string;
begin
	tux := sakuparam.basePath + sysvar.activeProjectName;
	while (tux <> '') and (tux[length(tux)] <> '.') do dec(byte(tux[0]));
	if tux = '' then halt(81);
	tux := lowercase(tux) + 'sav';
	assign(filu, tux);
	filemode := 1; rewrite(filu, 1); // write-only access
	i := IOresult;
	if i = 0 then begin
		// Write the signature.
		j := $CBCABAAB;
		blockwrite(filu, j, 4);
		// Write the seen graphics list.
		// Write the seen strings lists.
		close(filu);
	end
	else LogError(errortxt(i) + ' trying to write SAV');

	while IOresult <> 0 do ;
end;

{procedure TSavematic.Destroy;
begin
end;}

