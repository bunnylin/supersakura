{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

{$include fiberhub-implementation.pas}

function TFiber.GetCurrentId : longint;
begin
	result := -1; // silence compiler warning
	for result := FiberHub.fiberCount - 1 downto 0 do if FiberHub.fiber[result] = self then exit;
	result := -1;
end;

function TFiber.ExpandRelativeLabel(const labelref : UTF8string) : UTF8string;
var i : dword;
begin
	Assert(labelref = upcase(labelref));
	result := labelref;
	if labelref = '' then exit;

	// If the given label is a single dot, it's the start of the current script.
	if labelref = '.' then begin
		i := pos('.', labelName);
		result := copy(labelName, 1, i);
		exit;
	end;

	// If the given label has a dot, it's already qualified, no need to expand.
	i := pos('.', labelref);
	if i = 0 then begin
		// The given label has no dot. If this fiber's current script has such a label, expand to that.
		i := pos('.', labelName);
		result := copy(labelName, 1, i) + labelref;
		if GetScript(result) = 0 then begin
			// No such label in this script. Try using the given label as a script's entry point.
			result := labelref + '.';
			if GetScript(result) = 0 then begin
				// No such scriptname either. Try finding this label in a nameless virtual script.
				result := '.' + labelref;
				if GetScript(result) = 0 then LogError('no such label: ' + labelref);
			end;
		end;
	end;
end;

function TFiber.ToString : UTF8string;
begin
	result := strcat('Fiber %:%  fxrefcount:%  label:%:%  state:%',
		[GetCurrentId, fiberName, fxRefCount, labelName, codeOfs, strenum(typeinfo(fiberState), @fiberState)]);
end;

procedure TFiber.ScriptGoto(const _labelname : UTF8string);
// Jumps fiber execution to the start of the given label. Label name must be uppercased before calling.
var labelex : UTF8string;
	i : dword;
begin
	Assert(_labelname = upcase(_labelname));
	if _labelname = '' then begin
		LogError(strcat('ScriptGoto fiber %:%: empty label or out of code: goto "%"',
			[fiberName, labelName, _labelname]));
		fiberState := EFiberState.Stopping;
		exit;
	end;
	labelex := ExpandRelativeLabel(_labelname);

	i := GetScript(labelex);
	if i = 0 then begin
		LogError(strcat('ScriptGoto fiber %:%: no such label: %', [fiberName, labelName, _labelname]));
		fiberState := EFiberState.Stopping;
		exit;
	end;
	//log(strcat('fiber % goto % -> %', [fiberName, labelName, scriptObjects[i].labelName]));
	labelObject := scriptObjects[i];
	labelName := labelex;
	codeOfs := 0;

	if fiberState = EFiberState.WaitChoice then Choicematic.Deactivate(TRUE, FALSE);
end;

procedure TFiber.ScriptCall(const _labelname : UTF8string);
// Pushes the current fiber execution point on a call stack, then jumps fiber execution to the start of the given
// label. The label name must be uppercased before calling.
begin
	callstack[callstackIndex].labelName := labelName;
	callstack[callstackIndex].ofs := codeOfs;
	callstackIndex := (callstackIndex + 1) and CALLSTACK_HIGH;
	// Zero out the next free slot.
	callstack[callstackIndex].labelName := '';
	ScriptGoto(_labelname);
end;

procedure TFiber.ScriptReturn(onelevel : boolean);
// Pops a fiber execution point from the call stack, if available, and continues fiber execution from there.
// If onelevel = FALSE, keeps popping until the execution point is a label in a different script.
var i : dword = 0; // silence compiler
	thisscr : UTF8string = ''; // silence compiler
	lbl : UTF8string;
begin
	if NOT onelevel then begin
		i := pos('.', labelName);
		thisscr := copy(labelName, 1, i);
	end;

	repeat
		callstackIndex := (callstackIndex - 1) and CALLSTACK_HIGH;
		if callstack[callstackIndex].labelName = '' then begin
			LogError(strcat('ScriptReturn fiber %:%: out of callstack', [fiberName, labelName]));
			fiberState := EFiberState.Stopping;
			exit;
		end;
		lbl := callstack[callstackIndex].labelName;
		callstack[callstackIndex].labelName := ''; // zero out the popped slot
	until (onelevel) or (CompStr(@lbl[1], @thisscr[2], lbl.Length, i) <> 0);

	i := GetScript(lbl);
	if i = 0 then begin
		LogError(strcat('ScriptReturn fiber %:%: no such label: %', [fiberName, labelName, lbl]));
		fiberState := EFiberState.Stopping;
		exit;
	end;

	//log(strcat('fiber % return % -> %', [fiberName, labelName, scriptObjects[i].labelName]));
	labelObject := scriptObjects[i];
	labelName := lbl;
	codeOfs := callstack[callstackIndex].ofs;
end;

procedure TFiber.ScriptCase(const _labels : TStringBunch; caseindex : dword; docall : boolean);
// Selects the label indicated by 0-based caseindex, and performs a ScriptCall or ScriptGoto depending on the docall
// parameter. _labels must be a set of valid label strings split from a colon-separated single string.
// If the index is beyond the number of available labels, does nothing.
begin
	if _labels.Length = 0 then begin
		LogError('case without labels');
		exit;
	end;
	if caseindex >= _labels.Length then begin
		LogError(strcat('case index % > max label %', [caseindex, _labels.Length - 1]));
		exit;
	end;

	if docall then
		ScriptCall(_labels[caseindex])
	else
		ScriptGoto(_labels[caseindex]);
end;

constructor TFiber.Create(_labelname, _fibername : UTF8string);
// Starts a fiber running the given label. Uses the label name as the fiber name if _fibername is empty.
// The new fiber is placed at the end of the fiber list, so if it was created from script code, the new fiber is
// guaranteed to run during the same ScriptAhead call before effects/rendering are done.
var i : dword;
begin
	_labelname := upcase(_labelname);
	// Resize the fiber list if needed.
	with FiberHub do begin
		if (fiberCount + 8 < dword(length(fiber)) shr 1) or (fiberCount >= dword(length(fiber))) then
			setlength(fiber, fiberCount + fiberCount shr 1 + 4);
		fiber[fiberCount] := self;
		inc(fiberCount);
	end;

	labelName := _labelname;
	if _fibername <> '' then
		fiberName := upcase(_fibername)
	else
		fiberName := labelName;
	fiberState := EFiberState.Normal;

	labelObject := NIL;
	i := 0;
	if _labelname <> '' then begin
		log(strcat('StartFiber: label % as #%:%', [_labelname, FiberHub.fiberCount, fiberName]));
		i := GetScript(_labelname);
		if i = 0 then begin
			LogError('TFiber.Create: label ' + _labelname + ' not found');
			fiberState := EFiberState.Stopping;
		end;
	end;
	labelObject := scriptObjects[i];

	codeOfs := 0;
	fxRefCount := 0;
	locals := NIL;
	localCount := 0;
	for i := high(callstack) downto 0 do begin
		callstack[i].labelName := '';
		callstack[i].ofs := 0;
	end;
	filldword(datastack[0], FIBER_STACK_SIZE, 0);
	dataIndex := 0;
	dataCount := 0;
	dparamcount := 0;
	callstackIndex := 0;
end;

destructor TFiber.Destroy;
var i : dword;
begin
	if fiberName <> chr(0) then
		log('Stopping fiber ' + fiberName)
	else while dataCount <> 0 do begin
		// When a debug command fiber ends, print out any basic values in the data stack.
		// This means you can just use "$a" to see the variable contents, easier than "log $a".
		case EStackToken(PopInt) of
			EStackToken.Null: log('null');
			EStackToken.Number: log(strdec(PopInt));
			EStackToken.SingleStr: log('"' + PopString + '"');
			else break;
		end;
	end;

	i := GetCurrentId;

	// Stop this fiber's effects.
	EffectHub.RemoveOwnedEffects(self);

	// Move above fibers down a notch also.
	with FiberHub do begin
		inc(i);
		while i < fiberCount do begin
			fiber[i - 1] := fiber[i];
			inc(i);
		end;
		dec(fiberCount);
	end;
	inherited;
end;

// ------------------------------------------------------------------

procedure TFiber.SetLocal(index : dword; value : longint);
// Sets fiber-local variable [index] to value. Expands the variable array as needed.
begin
	if value <> 0 then begin
		if index >= localCount then begin
			localCount := index + 1;
			if index >= dword(length(locals)) then setlength(locals, index + 256);
			Assert(locals[index] = 0); // expanded array's new slots should come inited to 0...
		end;
		locals[index] := value;
	end
	else if index < localCount then begin
		locals[index] := 0;
		if locals[localCount - 1] = 0 then begin
			dec(localCount);
			if localCount < dword(length(locals)) shr 2 then setlength(locals, length(locals) shr 1);
		end;
	end;
end;

procedure TFiber.PushInt(num : longint);
begin
	longint(dataStack[dataIndex]) := num;
	dataIndex := (dataIndex + 1) and (FIBER_STACK_SIZE - 1);
	inc(dataCount);
	if dataCount >= FIBER_STACK_SIZE then dataCount := FIBER_STACK_SIZE - 1;
end;

procedure TFiber.PushInt(token : EStackToken); inline;
begin
	PushInt(ord(token));
end;

procedure TFiber.PushBytes(srcp : pointer; numbytes : dword);
var numdwords, stackfreespace, xfersize : dword;
begin
	numdwords := numbytes shr 2;
	// Push data up to end of stack.
	stackfreespace := FIBER_STACK_SIZE - dataIndex;
	while numdwords >= stackfreespace do begin
		dec(numdwords, stackfreespace);
		xfersize := stackfreespace shl 2;
		move(srcp^, datastack[dataIndex], xfersize); inc(srcp, xfersize);
		dataIndex := 0;
		stackfreespace := FIBER_STACK_SIZE;
	end;
	// Push rest of full dwords, plus leftovers.
	xfersize := numdwords shl 2 + numbytes and 3;
	move(srcp^, datastack[dataindex], xfersize);
	inc(dataindex, numdwords);
	if numbytes and 3 <> 0 then dataindex := (dataindex + 1) and (FIBER_STACK_SIZE - 1);
	// Push the sequence's byte length.
	datastack[dataindex] := numbytes;
	dataindex := (dataindex + 1) and (FIBER_STACK_SIZE - 1);
	// Update the valid data counter.
	inc(datacount, (numbytes + 3) shr 2 + 1);
	if datacount >= FIBER_STACK_SIZE then datacount := FIBER_STACK_SIZE - 1;
end;

procedure TFiber.PushString(const s : UTF8string);
begin
	if s.Length = 0 then
		PushInt(0)
	else
		PushBytes(@s[1], s.Length);
end;

procedure TFiber.PushStringBunch(const a : TStringBunch);
var i : dword;
begin
	Assert((a.Length = 1) or (a.Length = languageList.Length));
	for i := 0 to high(a) do
		PushString(a[i]);
	if a.Length = 1 then
		PushInt(EStackToken.SingleStr)
	else
		PushInt(EStackToken.MultiStr);
end;

function TFiber.PopInt : longint;
begin
	if dataCount = 0 then begin
		LogError('TFiber.PopInt: Stack underflow');
		fiberState := EFiberState.Stopping;
		result := 0; exit;
	end;
	dec(dataCount);
	if dataIndex = 0 then
		dataIndex := FIBER_STACK_SIZE - 1
	else
		dec(dataIndex);
	result := longint(datastack[dataIndex]);
end;

function TFiber.PopString : UTF8string;
var slen, numdwords, xfersize : dword;
begin
	result := '';
	// Get the string's byte length.
	slen := PopInt;
	if slen = 0 then exit;
	numdwords := (slen + 3) shr 2;
	if numdwords > dataCount then begin
		LogError('TFiber.PopInt: Stack underflow');
		fiberState := EFiberState.Stopping;
		exit;
	end;
	dec(dataCount, numdwords);
	setlength(result, numdwords shl 2);
	if dataIndex = 0 then
		dataIndex := FIBER_STACK_SIZE
	// Get the second half of the string, if it wraps around the data stack.
	else if numdwords > dataIndex then begin
		dec(numdwords, dataIndex);
		xfersize := dataIndex shl 2;
		move(dataStack[0], result[result.Length - xfersize + 1], xfersize);
		dataIndex := FIBER_STACK_SIZE;
	end;
	// Get the first half of the string.
	move(dataStack[dataIndex - numdwords], result[1], numdwords shl 2);
	dec(dataIndex, numdwords);

	// Crop the string to its exact size.
	setlength(result, slen);
	//log(strcat('PopString[%]', [result]));
end;

procedure TFiber.Run(yieldnow : boolean);
// Executes code in the fiber until the fiber yields control, or maxFiberSteps is reached.
// If yieldnow is TRUE, only executes a single step and returns immediately.
var lastcmd : byte = CMD_NOP;

	procedure _FiberError(const msg : string; critical : boolean = FALSE);
	begin
		FiberHub.LogCallstack(fiberName);
		LogError(strcat('Fiber %:%/%: %', [labelName, codeOfs, length(labelObject.code), msg]));
		if critical then begin
			fiberState := EFiberState.WaitKey;
			yieldnow := TRUE;
		end
		else if FiberHub.exitOnAnyError then
			yieldnow := TRUE;
	end;

	// --------------------------------------------------------------

	function _PopThing(out pval : longint; out pstr : TStringBunch) : EPoppedToken;
	var tempstr : UTF8string;
		i : dword;
		typetoken : EStackToken;
	begin
		result := EPoppedToken.Null;
		pstr := NIL;
		typetoken := EStackToken(PopInt);
		case typetoken of
			EStackToken.Null: ;

			EStackToken.Number:
			begin
				pval := PopInt;
				result := EPoppedToken.Int;
			end;

			EStackToken.SingleStr:
			begin
				tempstr := PopString;
				result := EPoppedToken.Str; // should an empty string be a null token? Probably not
				setlength(pstr, languageList.Length);
				pstr.SetAll(tempstr);
			end;

			EStackToken.MultiStr:
			begin
				setlength(pstr, languageList.Length);
				for i := languageList.Length - 1 downto 0 do begin
					pstr[i] := PopString;
					if pstr[i].Length <> 0 then result := EPoppedToken.Str;
				end;
			end;

			else _FiberError(strcat('Bad stack token type: %', [typetoken]), TRUE);
		end;
		//if result = EPoppedToken.Str then log(strcat('PopString[%]', [pstr[labelObject.labelLanguage]]));
	end;

	// --------------------------------------------------------------
	function FetchParam(paramtoken : byte; out pstr : TStringBunch) : boolean;
	// Places the given parameter's value in pstr and returns TRUE. Checks named parameters first, then consumes
	// a dynamic parameter. If not found in either list or value is null, returns FALSE and pstr is empty.
	begin
		Assert(ss_paramtype[paramtoken] = ARG_STR);
		pstr := NIL;
		if namedparam.defined[paramtoken] then begin
			pstr := namedparam.pStrValue[paramtoken];
		end
		else if dparamcount <> 0 then begin
			dec(dparamcount);
			with dynamicparam[dparamcount] do case pType of
				EPoppedToken.Null: setlength(pstr, languageList.Length);
				EPoppedToken.Int: inc(dparamcount);
				EPoppedToken.Str: pstr := pStrValue;
			end;
		end;
		result := (pstr <> NIL);
	end;

	function FetchParam(paramtoken : byte; out pval : longint) : boolean;
	// Places the given parameter's value in pval and returns TRUE. Checks named parameters first, then consumes
	// a dynamic parameter. If not found in either list, returns FALSE and pval is undefined.
	begin
		Assert(ss_paramtype[paramtoken] = ARG_NUM);
		result := FALSE;
		if namedparam.defined[paramtoken] then begin
			pval := namedparam.pNumValue[paramtoken];
			result := TRUE;
		end
		else if dparamcount <> 0 then begin
			dec(dparamcount);
			with dynamicparam[dparamcount] do case pType of
				EPoppedToken.Null: ;
				EPoppedToken.Int: begin pval := pNumValue; result := TRUE; end;
				EPoppedToken.Str: inc(dparamcount);
			end;
		end;
	end;

	function FetchParamOrDefault(paramtoken : byte; default : longint) : longint;
	begin
		if NOT FetchParam(paramtoken, result) then result := default;
	end;

	function FetchParamOrDefault(paramtoken : byte; const default : UTF8string) : TStringBunch;
	begin
		if NOT FetchParam(paramtoken, result) then begin
			setlength(result, languageList.Length);
			result.SetAll(default);
		end;
	end;

	function FetchParamDynamic(out pstr : TStringbunch; out pval : longint) : EPoppedToken;
	// Places the next dynamic parameter's value in pval or pstr and returns the parameter type. If no more dynamic
	// parameters are unconsumed, returns a NULL type.
	begin
		pstr := NIL;
		result := EPoppedToken.Null;
		if dparamcount = 0 then exit;
		dec(dparamcount);
		with dynamicparam[dparamcount] do begin
			pstr := pStrValue;
			pval := pNumValue;
			result := pType;
		end;
	end;

	procedure _DerefVars(var txt : UTF8string; language : byte);
	// Expands all \$thing; instances in the given string. Cannot be nested. (If nested $thing contains ;, would fail.)
	// Acts in place, because most strings have no vars to deref, so fastest to leave source string untouched.
	var readp, endp : ^char;
		fromofs, toofs, j, l : dword;
		waitfor : char = '\';
		u : UTF8string;
	begin
		fromofs := 0; // silence compiler
		Assert(txt <> '');
		readp := @txt[1];
		endp := readp + txt.Length;
		while readp < endp do begin
			if readp^ = waitfor then begin
				if waitfor = '\' then begin
					inc(readp);
					if readp >= endp then exit; // broken escape code
					if readp^ = '$' then begin
						fromofs := readp - @txt[1];
						waitfor := ';';
					end;
				end
				else with Varmon do begin
					toofs := readp - @txt[1];
					j := fromofs + 2;
					if txt[j] in ['0'..'9'] then begin
						// Local numeric variable.
						l := dword(valx(@txt[j]));
						if l < localCount then
							u := strdec(locals[l])
						else
							u := '0';
					end
					else begin
						// Normal variable.
						u := GetStrVar(copy(txt, j, toofs - fromofs - 1), language);
					end;
					txt := copy(txt, 1, fromofs - 1) + u + copy(txt, toofs + 2);
					inc(fromofs, u.Length);
					if fromofs > txt.Length then break;
					readp := @txt[fromofs];
					endp := readp + txt.Length;
					waitfor := '\';
					continue;
				end;
			end;
			inc(readp);
		end;
	end;

	// ----------------------------------------------------------------
	{$include fibercmds.pas}

	function ConsumeParams(cmdtoken : byte) : byte;
	// When a command token is encountered, this is called to pop the command's parameters off the stack and into
	// parameter arrays. Parameter identifiers and their values are popped until EStackToken.ENDPARAMS is found.
	// Returns the CMD token that was passed in, or lastcmd if the token is CMD_AGAIN.
	var paramtoken : longint;
		poppedtype : EPoppedToken;
		poppedstr : TStringBunch;
		poppedint : longint = 0;
	begin
		if cmdtoken = CMD_AGAIN then
			cmdtoken := lastcmd
		else begin
			lastcmd := cmdtoken;
			fillbyte(namedparam.defined[0], length(namedparam.defined), 0);
		end;
		result := cmdtoken;

		dparamcount := 0;
		repeat
			// Get the next parameter identifier. Quit if it is ENDPARAMS.
			paramtoken := PopInt - ord(EStackToken.Param);
			if paramtoken < 0 then exit;
			if (paramtoken >= length(ss_paramtype))
			or (paramtoken <> CMDP_DYNAMIC) and (ss_paramtype[paramtoken] = ARG_INVALID)
			then begin
				_FiberError('Param id out of bounds', TRUE);
				exit;
			end;
			// Get the value associated with this parameter. The value may be of any type at this point, so it'll be
			// typefitted later.
			poppedtype := _PopThing(poppedint, poppedstr);

			if paramtoken = CMDP_DYNAMIC then begin
				// Add dynamic parameters in the dynamic parameter list.
				if dparamcount >= dword(length(dynamicparam)) then begin
					_FiberError('Too many dynamic params', TRUE);
					exit;
				end;
				with dynamicparam[dparamcount] do begin
					pStrValue := poppedstr;
					pNumValue := poppedint;
					pType := poppedtype;
					//if pType = EPoppedToken.Int then log('#dparam ' + strdec(poppedint)) else log('#dparam "' + poppedstr[0] + '"');
				end;
				inc(dparamcount);
			end
			else begin
				// Validate value for non-dynamic parameters.
				namedparam.defined[paramtoken] := TRUE;
				case ss_paramtype[paramtoken] of
					ARG_NUM:
					case poppedtype of
						EPoppedToken.Null: namedparam.pNumValue[paramtoken] := 0;
						EPoppedToken.Int: namedparam.pNumValue[paramtoken] := poppedint;
						EPoppedToken.Str:
						if NOT TryNumberFromString(
							poppedstr[labelObject.labelLanguage], @namedparam.pNumValue[paramtoken]) then
							case lowercase(poppedstr[labelObject.labelLanguage]) of
								'false', 'no', 'off': poppedint := 0;
								'true', 'yes', 'on': poppedint := 1;
								else begin
									_FiberError(strcat('Expected number for named param %, got string "%"',
										[paramtoken, poppedstr[labelObject.labelLanguage]]));
									exit;
								end;
							end;
					end;

					ARG_STR:
					case poppedtype of
						EPoppedToken.Null:
						begin
							setlength(namedparam.pStrValue[paramtoken], languageList.Length);
							namedparam.pStrValue[paramtoken].SetAll('');
						end;

						EPoppedToken.Int:
						begin
							setlength(namedparam.pStrValue[paramtoken], languageList.Length);
							namedparam.pStrValue[paramtoken].SetAll(strdec(poppedint));
						end;

						EPoppedToken.Str: namedparam.pStrValue[paramtoken] := poppedstr;
					end;
				end;
			end;
		until FALSE;
	end;

var i, j, stepsleft : dword;
	poppedtype1, poppedtype2 : EPoppedToken;
	poppedint1 : longint = 0;
	poppedint2 : longint = 0;
	poppedstr1 : TStringBunch = NIL;
	poppedstr2 : TStringBunch = NIL;
	workbunch : TStringBunch = NIL;
	readstr : UTF8string;
	token : EScriptToken;

	procedure _PopTwoThings; inline;
	begin
		poppedtype2 := _PopThing(poppedint2, poppedstr2);
		poppedtype1 := _PopThing(poppedint1, poppedstr1);
	end;

begin // ExecuteFiber
	try
	stepsleft := sysvar.maxFiberSteps;
	repeat
		if stepsleft = 0 then begin
			_FiberError('Exceeded max steps, fiber deadlocked?');
			exit;
		end;
		dec(stepsleft);
		// Run the next label, if reached the end of the current one. If there is no next label, terminate the fiber.
		// If the next label is empty, try the label after that.
		while codeOfs >= dword(length(labelObject.code)) do
			if labelObject.nextLabel = '' then begin
				fiberState := EFiberState.Stopping;
				break;
			end else
				ScriptGoto(labelObject.nextLabel);

		if fiberState = EFiberState.Stopping then exit;

		// Process a script token from the current execution address.
		token := EScriptToken(labelObject.code[codeOfs]);
		inc(codeOfs);

		// Direct number values. Each is converted to a longint and pushed on the fiber stack.
		if byte(token) <= 32 then begin
			PushInt(byte(token));
			PushInt(EStackToken.Number);
		end else

		case token of
			TOKEN_BYTE:
			begin
				PushInt(labelObject.code[codeOfs]);
				inc(codeOfs);
				PushInt(EStackToken.Number);
			end;

			TOKEN_WORD:
			begin
				PushInt(word((@labelObject.code[codeOfs])^));
				inc(codeOfs, 2);
				PushInt(EStackToken.Number);
			end;

			TOKEN_LONGINT:
			begin
				PushInt(longint((@labelObject.code[codeOfs])^));
				inc(codeOfs, 4);
				PushInt(EStackToken.Number);
			end;

			// String values. Each is converted to a singlestring or multistring and pushed on the fiber stack.
			// Any variable escape code \$varname; in this string is dereferenced while converting.
			TOKEN_EMPTYSTRING:
			PushInt(EStackToken.Null);

			TOKEN_CHAR:
			begin
				PushBytes(@labelObject.code[codeofs], 1);
				PushInt(EStackToken.SingleStr);
				inc(codeofs);
			end;

			TOKEN_MINILOCALSTRING, TOKEN_MINIGLOBALSTRING, TOKEN_LONGLOCALSTRING, TOKEN_LONGGLOBALSTRING:
			begin
				// These strings are directly saved in the bytecode. First read the length byte or dword.
				if token in [TOKEN_MINILOCALSTRING, TOKEN_MINIGLOBALSTRING] then begin
					i := labelObject.code[codeofs];
					inc(codeofs);
				end
				else begin
					i := dword((@labelObject.code[codeOfs])^);
					inc(codeOfs, 4);
				end;
				if i = 0 then begin // shouldn't happen, but safest to handle
					PushInt(EStackToken.Null);
					continue;
				end;

				// Grab the string content.
				readstr := '';
				setlength(readstr, i);
				move(labelObject.code[codeofs], readstr[1], i);
				inc(codeofs, i);

				// Local strings are always in the script's default language, dereference and push as a single string.
				if token in [TOKEN_MINILOCALSTRING, TOKEN_LONGLOCALSTRING] then begin
					_DerefVars(readstr, labelObject.labelLanguage);
					PushString(readstr);
					PushInt(EStackToken.SingleStr);
				end
				else begin
					// Global strings are expanded to a multi-language bunch before deref and push.
					poppedstr1 := GetStringGlobal(readstr, labelObject.labelLanguage);
					for i := high(poppedstr1) downto 0 do
						if poppedstr1[i] <> '' then _DerefVars(poppedstr1[i], i);
					PushStringBunch(poppedstr1);
				end;
			end;

			TOKEN_MINIUNIQUESTRING, TOKEN_LONGUNIQUESTRING:
			begin
				// These are byte or dword references into the unique string table.
				if token = TOKEN_MINIUNIQUESTRING then begin
					i := labelObject.code[codeofs];
					inc(codeofs);
				end
				else begin
					i := dword((@labelObject.code[codeOfs])^);
					inc(codeOfs, 4);
				end;

				// Expand to a multi-language bunch, deref, then push.
				poppedstr1 := GetStringUnique(labelObject, i);
				for i := high(poppedstr1) downto 0 do
					if poppedstr1[i] <> '' then _DerefVars(poppedstr1[i], i);
				PushStringBunch(poppedstr1);
			end;

			// Unary operations.
			TOKEN_NOT:
			begin
				case _PopThing(poppedint1, poppedstr1) of
					// Notting a null returns a "1" string.
					EPoppedToken.Null:
					begin
						PushString('1');
						PushInt(EStackToken.SingleStr);
					end;
					EPoppedToken.Int:
					begin
						if poppedint1 = 0 then
							PushInt(1)
						else
							PushInt(0);
						PushInt(EStackToken.Number);
					end;
					EPoppedToken.Str: PushInt(EStackToken.Null);
				end;
			end;

			TOKEN_NEG:
			begin
				case _PopThing(poppedint1, poppedstr1) of
					EPoppedToken.Null: PushInt(EStackToken.Null);
					EPoppedToken.Int:
					begin
						PushInt(-poppedint1);
						PushInt(EStackToken.Number);
					end;
					EPoppedToken.Str:
					if TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						PushInt(-poppedint1);
						PushInt(EStackToken.Number);
					end
					else begin
						_FiberError('Can''t negate a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
			end;

			TOKEN_VAR:
			with Varmon do begin
				// Pop the variable name.
				case _PopThing(poppedint1, poppedstr1) of
					EPoppedToken.Null:
					begin
						_FiberError('Empty variable name');
						exit;
					end;
					EPoppedToken.Int:
					begin
						// Local numeric variable.
						// Needs to output 0 even if out of range, since some cmds treat null as no arg.
						if dword(poppedint1) < localCount then
							PushInt(locals[poppedint1])
						else
							PushInt(0);
						PushInt(EStackToken.Number);
					end;
					EPoppedToken.Str:
					// Named variable...
					case GetVarType(poppedstr1[labelObject.labelLanguage]) of
						// Numeric variable. Fetch and push the value.
						VT_INT:
						begin
							PushInt(GetNumVar(poppedstr1[labelObject.labelLanguage]));
							PushInt(EStackToken.Number);
						end;
						// String variable. Fetch and push the value(s).
						VT_STR: PushStringBunch(GetStrVar(poppedstr1[labelObject.labelLanguage]));

						// Variable doesn't exist.
						else PushInt(EStackToken.Null);
					end;
				end;
			end;

			TOKEN_GETPARAM:
			begin
				i := labelObject.code[codeOfs]; inc(codeOfs);
				if i = CMDP_DYNAMIC then
					poppedtype1 := FetchParamDynamic(poppedstr1, poppedint1)
				else begin
					if (i >= length(ss_paramtype)) or (ss_paramtype[i] = ARG_INVALID) then begin
						_FiberError('getparam: bad param: ' + strdec(i));
						exit;
					end;
					poppedtype1 := EPoppedToken.Null;
					if ss_paramtype[i] = ARG_NUM then begin
						if FetchParam(i, poppedint1) then poppedtype1 := EPoppedToken.Int;
					end
					else if FetchParam(i, poppedstr1) then poppedtype1 := EPoppedToken.Str;
				end;
				case poppedtype1 of
					EPoppedToken.Null: PushInt(EStackToken.Null);
					EPoppedToken.Int:
					begin
						PushInt(poppedint1);
						PushInt(EStackToken.Number);
					end;
					EPoppedToken.Str: PushStringBunch(poppedstr1);
				end;
			end;

			TOKEN_RND:
			begin
				case _PopThing(poppedint1, poppedstr1) of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t random a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				// Getting random negative numbers is dodgy, so special handling.
				if poppedint1 >= 0 then
					PushInt(random(poppedint1))
				else
					PushInt(-random(-poppedint1));
				PushInt(EStackToken.Number);
			end;

			TOKEN_ABS:
			begin
				case _PopThing(poppedint1, poppedstr1) of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t ABS a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				PushInt(abs(poppedint1));
				PushInt(EStackToken.Number);
			end;

			TOKEN_TONUM:
			begin
				case _PopThing(poppedint1, poppedstr1) of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t convert to number: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				PushInt(poppedint1);
				PushInt(EStackToken.Number);
			end;

			TOKEN_TOSTR:
			begin
				case _PopThing(poppedint1, poppedstr1) of
					EPoppedToken.Null: PushInt(EStackToken.Null);
					EPoppedToken.Int:
					begin
						PushString(strdec(poppedint1));
						PushInt(EStackToken.SingleStr);
					end;
					EPoppedToken.Str: PushStringBunch(poppedstr1);
				end;
			end;

			TOKEN_TOHEX:
			begin
				poppedtype1 := _PopThing(poppedint1, poppedstr1);
				if poppedtype1 = EPoppedToken.Null then
					PushInt(EStackToken.Null)
				else begin
					if poppedtype1 = EPoppedToken.Str then
						if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
							_FiberError('Can''t tohex a string: ' + poppedstr1[labelObject.labelLanguage]);
							exit;
						end;
					PushString(strhex(dword(poppedint1)));
					PushInt(EStackToken.SingleStr);
				end;
			end;

			TOKEN_INT8, TOKEN_INT16:
			begin
				poppedtype1 := _PopThing(poppedint1, poppedstr1);
				if poppedtype1 = EPoppedToken.Null then
					PushInt(EStackToken.Null)
				else begin
					if poppedtype1 = EPoppedToken.Str then
						if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
							_FiberError('Can''t sign-extend a string: ' + poppedstr1[labelObject.labelLanguage]);
							exit;
						end;
					if token = TOKEN_INT8 then
						PushInt(shortint(poppedint1))
					else
						PushInt(smallint(poppedint1));
					PushInt(EStackToken.Number);
				end;
			end;

			// Binary operations. Right-hand operand is popped first.
			TOKEN_PLUS:
			begin
				_PopTwoThings;

				// Null and anything else returns that anything else.
				if poppedtype1 = EPoppedToken.Null then begin
					case poppedtype2 of
						EPoppedToken.Null: PushInt(EStackToken.Null);
						EPoppedToken.Int:
						begin
							PushInt(poppedint2);
							PushInt(EStackToken.Number);
						end;
						EPoppedToken.Str: PushStringBunch(poppedstr2);
					end;
				end
				else if poppedtype2 = EPoppedToken.Null then begin
					if poppedtype1 = EPoppedToken.Str then
						PushStringBunch(poppedstr1)
					else begin
						PushInt(poppedint1);
						PushInt(EStackToken.Number);
					end;
				end
				else begin
					if (poppedtype1 = EPoppedToken.Int) and (poppedtype2 = EPoppedToken.Int) then begin
						// Two numbers, add them.
						PushInt(longint(poppedint1 + poppedint2));
						PushInt(EStackToken.Number);
					end
					else begin
						// At least one side is a string, convert numbers to strings.
						if poppedtype1 = EPoppedToken.Int then begin
							setlength(poppedstr1, languageList.Length);
							poppedstr1.SetAll(strdec(poppedint1));
						end;
						if poppedtype2 = EPoppedToken.Int then begin
							setlength(poppedstr2, languageList.Length);
							poppedstr2.SetAll(strdec(poppedint2));
						end;

						// Concatenate and push the strings.
						for i := poppedstr1.Length - 1 downto 0 do poppedstr1[i] := poppedstr1[i] + poppedstr2[i];
						PushStringBunch(poppedstr1);
					end;
				end;
			end;

			TOKEN_MINUS:
			begin
				_PopTwoThings;
				case poppedtype1 of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t minus from a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				case poppedtype2 of
					EPoppedToken.Null: poppedint2 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
						_FiberError('Can''t minus a string: ' + poppedstr2[labelObject.labelLanguage]);
						exit;
					end;
				end;
				PushInt(longint(poppedint1 - poppedint2));
				PushInt(EStackToken.Number);
			end;

			TOKEN_MUL:
			begin
				_PopTwoThings;
				case poppedtype1 of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t multiply a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				case poppedtype2 of
					EPoppedToken.Null: poppedint2 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
						_FiberError('Can''t multiply with a string: ' + poppedstr2[labelObject.labelLanguage]);
						exit;
					end;
				end;
				PushInt(longint(poppedint1 * poppedint2));
				PushInt(EStackToken.Number);
			end;

			TOKEN_DIV:
			begin
				_PopTwoThings;
				case poppedtype1 of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t divide a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				case poppedtype2 of
					EPoppedToken.Null: poppedint2 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
						_FiberError('Can''t divide by a string: ' + poppedstr2[labelObject.labelLanguage]);
						exit;
					end;
				end;
				PushInt(longint(poppedint1 div poppedint2));
				PushInt(EStackToken.Number);
			end;

			TOKEN_MOD:
			begin
				_PopTwoThings;
				case poppedtype1 of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t modulo a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				case poppedtype2 of
					EPoppedToken.Null: poppedint2 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
						_FiberError('Can''t modulo by a string: ' + poppedstr2[labelObject.labelLanguage]);
						exit;
					end;
				end;
				PushInt(longint(poppedint1 mod poppedint2));
				PushInt(EStackToken.Number);
			end;

			TOKEN_AND:
			begin
				_PopTwoThings;
				case poppedtype1 of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t AND a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				case poppedtype2 of
					EPoppedToken.Null: poppedint2 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
						_FiberError('Can''t AND with a string: ' + poppedstr2[labelObject.labelLanguage]);
						exit;
					end;
				end;
				PushInt(poppedint1 and poppedint2);
				PushInt(EStackToken.Number);
			end;

			TOKEN_OR:
			begin
				_PopTwoThings;
				case poppedtype1 of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t OR a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				case poppedtype2 of
					EPoppedToken.Null: poppedint2 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
						_FiberError('Can''t OR with a string: ' + poppedstr2[labelObject.labelLanguage]);
						exit;
					end;
				end;
				PushInt(poppedint1 or poppedint2);
				PushInt(EStackToken.Number);
			end;

			TOKEN_XOR:
			begin
				_PopTwoThings;
				case poppedtype1 of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t XOR a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				case poppedtype2 of
					EPoppedToken.Null: poppedint2 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
						_FiberError('Can''t XOR with a string: ' + poppedstr2[labelObject.labelLanguage]);
						exit;
					end;
				end;
				PushInt(poppedint1 xor poppedint2);
				PushInt(EStackToken.Number);
			end;

			TOKEN_EQ, TOKEN_LT, TOKEN_GT, TOKEN_LE, TOKEN_GE, TOKEN_NE:
			begin
				_PopTwoThings;
				// Comparing a null with anything: treat null as the other type.
				if poppedtype1 = EPoppedToken.Null then
					if poppedtype2 = EPoppedToken.Str then begin
						poppedtype1 := EPoppedToken.Str;
						setlength(poppedstr1, languageList.Length);
						poppedstr1.SetAll('');
					end
				else begin
					poppedtype1 := EPoppedToken.Int;
					poppedint1 := 0;
				end;

				if poppedtype2 = EPoppedToken.Null then
					if poppedtype1 = EPoppedToken.Str then begin
						poppedtype2 := EPoppedToken.Str;
						setlength(poppedstr2, languageList.Length);
						poppedstr2.SetAll('');
					end
				else begin
					poppedtype2 := EPoppedToken.Int;
					poppedint2 := 0;
				end;

				i := 0;
				if poppedtype1 = EPoppedToken.Int then begin
					if poppedtype2 = EPoppedToken.Int then begin
						// Comparing two numbers.
						case token of
							TOKEN_EQ: if poppedint1 = poppedint2 then i := 1;
							TOKEN_LT: if poppedint1 < poppedint2 then i := 1;
							TOKEN_GT: if poppedint1 > poppedint2 then i := 1;
							TOKEN_LE: if poppedint1 <= poppedint2 then i := 1;
							TOKEN_GE: if poppedint1 >= poppedint2 then i := 1;
							TOKEN_NE: if poppedint1 <> poppedint2 then i := 1;
						end;
					end
					else begin
						_FiberError(strcat('Can''t compare number % and string %',
							[poppedint1, poppedstr2[labelObject.labelLanguage]]));
						exit;
					end;
				end
				else if poppedtype2 = EPoppedToken.Int then begin
					_FiberError(strcat('Can''t compare string % and number %',
						[poppedstr1[labelObject.labelLanguage], poppedint2]));
					exit;
				end
				else begin
					// Comparing two strings, case-insensitively.
					case token of
						TOKEN_EQ: if upcase(poppedstr1[labelObject.labelLanguage]) = upcase(poppedstr2[labelObject.labelLanguage]) then i := 1;
						TOKEN_LT: if upcase(poppedstr1[labelObject.labelLanguage]) < upcase(poppedstr2[labelObject.labelLanguage]) then i := 1;
						TOKEN_GT: if upcase(poppedstr1[labelObject.labelLanguage]) > upcase(poppedstr2[labelObject.labelLanguage]) then i := 1;
						TOKEN_LE: if upcase(poppedstr1[labelObject.labelLanguage]) <= upcase(poppedstr2[labelObject.labelLanguage]) then i := 1;
						TOKEN_GE: if upcase(poppedstr1[labelObject.labelLanguage]) >= upcase(poppedstr2[labelObject.labelLanguage]) then i := 1;
						TOKEN_NE: if upcase(poppedstr1[labelObject.labelLanguage]) <> upcase(poppedstr2[labelObject.labelLanguage]) then i := 1;
					end;
				end;

				PushInt(i);
				PushInt(EStackToken.Number);
			end;

			TOKEN_SET:
			with Varmon do begin
				_PopTwoThings;
				// Left side must be a variable reference.
				case poppedtype1 of
					EPoppedToken.Null:
					begin
						_FiberError('Expected variable identifier, got null');
						exit;
					end;

					EPoppedToken.Int: // writing to a local variable
					begin
						if poppedint1 > 2304000 then begin
							_FiberError('Exceeded local var limit, index ' + strdec(poppedint1));
							exit;
						end;
						case poppedtype2 of
							EPoppedToken.Null: poppedint2 := 0;
							EPoppedToken.Int: ;
							EPoppedToken.Str:
							if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
								_FiberError('Can''t set local var to a string: "' + poppedstr2[labelObject.labelLanguage] + '"');
								exit;
							end;
						end;
						SetLocal(poppedint1, poppedint2);
					end;

					EPoppedToken.Str: // writing to a named variable
					case poppedtype2 of
						EPoppedToken.Null: DeleteVar(poppedstr1[labelObject.labelLanguage]);
						EPoppedToken.Int: SetNumVar(poppedstr1[labelObject.labelLanguage], poppedint2);
						EPoppedToken.Str:
						begin
							SetStrVar(poppedstr1[labelObject.labelLanguage], poppedstr2);
							//log(strcat('% := %', [poppedstr1[labelObject.labelLanguage], poppedstr2[labelObject.labelLanguage]]));
						end;
					end;
				end;
			end;

			TOKEN_INC:
			with Varmon do begin
				_PopTwoThings;
				// Left side must be a variable reference.
				case poppedtype1 of
					EPoppedToken.Null:
					begin
						_FiberError('Expected variable identifier, got null');
						exit;
					end;

					EPoppedToken.Int: // writing to a local variable
					begin
						case poppedtype2 of
							EPoppedToken.Null: continue;
							EPoppedToken.Int: ;
							EPoppedToken.Str:
							if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
								_FiberError('Can''t increase local var by a string: "' + poppedstr2[labelObject.labelLanguage] + '"');
								exit;
							end;
						end;
						i := 0;
						if dword(poppedint1) < localCount then i := dword(locals[poppedint1]);
						SetLocal(poppedint1, longint(i) + poppedint2);
					end;

					EPoppedToken.Str: // writing to a named variable
					// Check what variable type is being increased.
					case GetVarType(poppedstr1[labelObject.labelLanguage]) of
						VT_NULL:
						begin
							// Increasing a non-existing variable, just set.
							case poppedtype2 of
								EPoppedToken.Int: SetNumVar(poppedstr1[labelObject.labelLanguage], poppedint2);
								EPoppedToken.Str: SetStrVar(poppedstr1[labelObject.labelLanguage], poppedstr2);
							end;
						end;

						VT_INT:
						case poppedtype2 of
							// Increase numeric var by number.
							EPoppedToken.Int: SetNumVar(
								poppedstr1[labelObject.labelLanguage],
								GetNumVar(poppedstr1[labelObject.labelLanguage]) + poppedint2);
							// Increase numeric var by string.
							EPoppedToken.Str:
							if TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then
								SetNumVar(
									poppedstr1[labelObject.labelLanguage],
									GetNumVar(poppedstr1[labelObject.labelLanguage]) + poppedint2)
							else begin
								_FiberError('Can''t increase by a string: ' + poppedstr2[labelObject.labelLanguage]);
								exit;
							end;
						end;

						VT_STR:
						begin
							workbunch := GetStrVar(poppedstr1[labelObject.labelLanguage]);
							// Increase a string var by a number...
							if poppedtype2 = EPoppedToken.Int then begin
								setlength(poppedstr2, languageList.Length);
								poppedstr2.SetAll(strdec(poppedint2));
							end;
							// Increase a string var by a string.
							for j := high(poppedstr2) downto 0 do workbunch[j] := workbunch[j] + poppedstr2[j];
							SetStrVar(poppedstr1[labelObject.labelLanguage], workbunch);
						end;

					end;
				end;
			end;

			TOKEN_DEC:
			with Varmon do begin
				_PopTwoThings;
				// Left side must be a variable reference.
				case poppedtype1 of
					EPoppedToken.Null:
					begin
						_FiberError('Expected variable identifier, got null');
						exit;
					end;

					EPoppedToken.Int: // writing to a local variable
					begin
						case poppedtype2 of
							EPoppedToken.Null: continue;
							EPoppedToken.Int: ;
							EPoppedToken.Str:
							if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
								_FiberError('Can''t decrease local var by a string: "' + poppedstr2[labelObject.labelLanguage] + '"');
								exit;
							end;
						end;
						i := 0;
						if dword(poppedint1) < localCount then i := locals[poppedint1];
						SetLocal(poppedint1, i - poppedint2);
					end;

					EPoppedToken.Str: // writing to a named variable
					begin
						// Check what variable type is being decreased.
						// If right side is a string, try to convert to a number.
						if (poppedtype2 = EPoppedToken.Str)
						and (NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2)) then begin
							_FiberError('Can''t decrease by a string: ' + poppedstr2[labelObject.labelLanguage]);
							exit;
						end;

						case GetVarType(poppedstr1[labelObject.labelLanguage]) of
							// Decreasing a non-existing variable, just set.
							VT_NULL: SetNumVar(poppedstr1[labelObject.labelLanguage], -poppedint2);
							// Decrease numeric var.
							VT_INT: SetNumVar(
								poppedstr1[labelObject.labelLanguage],
								GetNumVar(poppedstr1[labelObject.labelLanguage]) - poppedint2);
							// Decrease a string var.
							VT_STR:
							begin
								_FiberError('Can''t decrease a string: ' + poppedstr1[labelObject.labelLanguage]);
								exit;
							end;
						end;
					end;
				end;
			end;

			TOKEN_SHL:
			begin
				_PopTwoThings;
				case poppedtype1 of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t SHL a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				case poppedtype2 of
					EPoppedToken.Null: poppedint2 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
						_FiberError('Can''t SHL with a string: ' + poppedstr2[labelObject.labelLanguage]);
						exit;
					end;
				end;
				if poppedint2 >= 0 then
					PushInt(longint(poppedint1 shl poppedint2))
				else
					PushInt(longint(poppedint1 shr -poppedint2));
				PushInt(EStackToken.Number);
			end;

			TOKEN_SHR:
			begin
				_PopTwoThings;
				case poppedtype1 of
					EPoppedToken.Null: poppedint1 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr1[labelObject.labelLanguage], @poppedint1) then begin
						_FiberError('Can''t SHR a string: ' + poppedstr1[labelObject.labelLanguage]);
						exit;
					end;
				end;
				case poppedtype2 of
					EPoppedToken.Null: poppedint2 := 0;
					EPoppedToken.Int: ;
					EPoppedToken.Str:
					if NOT TryNumberFromString(poppedstr2[labelObject.labelLanguage], @poppedint2) then begin
						_FiberError('Can''t SHR with a string: ' + poppedstr2[labelObject.labelLanguage]);
						exit;
					end;
				end;
				if poppedint2 >= 0 then
					PushInt(poppedint1 shr poppedint2)
				else
					PushInt(poppedint1 shl -poppedint2);
				PushInt(EStackToken.Number);
			end;

			// Unconditional jump.
			TOKEN_JUMP:
			begin
				longint(i) := longint(codeOfs) + longint((@labelObject.code[codeOfs])^);
				if longint(i) < 0 then
					_FiberError('Sub-zero jump', TRUE)
				else
					if i > dword(length(labelObject.code)) then
						_FiberError('Jump out of bounds', TRUE)
					else
						codeOfs := i;
			end;

			// Conditional jump.
			TOKEN_IF:
			begin
				poppedtype1 := _PopThing(poppedint1, poppedstr1);
				if (poppedtype1 = EPoppedToken.Null)
				or (poppedtype1 = EPoppedToken.Int) and (poppedint1 = 0)
				or (poppedtype1 = EPoppedToken.Str) and (poppedstr1[labelObject.labelLanguage] = '')
				then begin
					// Condition false, get relative offset and jump past the then-segment.
					longint(i) := longint(codeOfs) + longint((@labelObject.code[codeOfs])^);
					if longint(i) < 0 then
						_FiberError('Sub-zero if-jump', TRUE)
					else
						if i > dword(length(labelObject.code)) then
							_FiberError('If-jump out of bounds', TRUE)
						else
							codeOfs := i;
				end
				else begin
					// Condition true, ignore offset, fall into then-segment.
					inc(codeOfs, 4);
				end;
			end;

			// React to the previous choice command.
			TOKEN_CHOICEREACT:
			with Choicematic do begin
				PushInt(selectedNode - 1);
				PushInt(EStackToken.Number);

				token := EScriptToken(labelObject.code[codeOfs - 2]);
				if (byte(token) <> CMD_CHOICE_GET) and (selectedNode <> -1) then begin
					if selectedNode = 0 then selectedNode := highlightIndex + 1;
					if (selectedNode < 0) or (selectedNode >= longint(choiceNodeCount)) then
						LogError('choice has no jump/call target')
					else begin
						i := 0;
						if choiceNodes[selectedNode].trackVar <> '' then
							i := Varmon.GetNumVar(choiceNodes[selectedNode].trackVar);
						ScriptCase(choiceNodes[selectedNode].jumpList, i, byte(token) = CMD_CHOICE_CALL);
					end;
				end;
			end;

			// Command parameters.
			TOKEN_CMDEND: PushInt(EStackToken.EndParams);

			TOKEN_DYNPARAM: PushInt(EStackToken.Param);

			TOKEN_PARAM:
			begin
				PushInt(ord(EStackToken.Param) + labelObject.code[codeOfs]);
				inc(codeOfs);
			end;

			// Commands.
			TOKEN_CMD:
			begin
				i := labelObject.code[codeOfs]; inc(codeOfs);
				j := ConsumeParams(i);
				InvokeCommand(j);
			end;

			// Unrecognised token.
			else begin
				_FiberError('Invalid script token: ' + strdec(byte(token)), TRUE);
				exit;
			end;
		end;
	until yieldnow or FiberHub.exitRunFibersNow;

	except
		on e : Exception do begin
			LogError('exception', e, ExceptAddr);
			_FiberError(e.Message, TRUE);
		end;
	end;
end;

