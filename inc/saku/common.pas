{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

{$include ../version.inc}
const frontEndName : string[11] = 'supersakura'; // dat loaded on startup
const copyrightYear : string[4] = '2025'; // not in version.inc because not used by sakutool

// Normal = all fibers run normally
// Single = each fiber takes a single step, then the game pauses itself
// Paused = fibers do not run
type EPauseState = (Normal, Single, Paused);

// Normal = running normal game
// Meta = running metamenu, info box, or meta dialog while the game is suspended
// Confirm quit = showing hardcoded quit box
type EMetaState = (Normal, Meta, ConfirmQuit);

type ESkipState = (No, Seen, All);

// Coscos = visually pleasing soft glide
// Halfcos = second half of coscos, starts fast, slows toward end
type EMoveType = (Instant = 0, Linear = 1, Coscos = 2, Halfcos = 3);

function EMoveTypeFromString(const instr : UTF8string) : EMoveType;
begin
	case lowercase(instr) of
		'1'..'3': result := EMoveType(ord(instr[1]) - 48);
		'linear': result := EMoveType.Linear;
		'coscos','cosine','cos': result := EMoveType.Coscos;
		'halfcos': result := EMoveType.Halfcos;
		else result := EMoveType.Instant;
	end;
end;

type ETransitionType = (Instant = 0, WipeFromLeft = 1, RaggedWipe = 2, Interlaced = 3, Crossfade = 4);

function ETransitionTypeFromString(const instr : UTF8string) : ETransitionType;
begin
	case lowercase(instr) of
		'1'..'4': result := ETransitionType(ord(instr[1]) - 48);
		'raggedwipe','ragged','r': result := ETransitionType.RaggedWipe;
		'wipe','swipe','s','w': result := ETransitionType.WipeFromLeft;
		'interlaced','i': result := ETransitionType.Interlaced;
		'crossfade','fade','dissolve','d','f': result := ETransitionType.Crossfade;
		else result := ETransitionType.Instant;
	end;
end;

type EFillMode = (None = 0, Flat = 1, XGradient = 2);

function EFillModeFromString(const instr : UTF8string) : EFillMode;
begin
	case lowercase(instr) of
		'1'..'2': result := EFillMode(ord(instr[1]) - 48);
		'flat': result := EFillMode.Flat;
		'gradient': result := EFillMode.XGradient;
		else result := EFillMode.None;
	end;
end;

type EBlendMode = (Normal = 0, Hardlight = 1, Multiply = 2, Difference = 3, Additive = 4);

function EBlendModeFromString(const instr : UTF8string) : EBlendMode;
begin
	case lowercase(instr) of
		'1'..'4': result := EBlendMode(ord(instr[1]) - 48);
		'hard','hardlight': result := EBlendMode.Hardlight;
		'mul','multiply': result := EBlendMode.Multiply;
		'diff','difference': result := EBlendMode.Difference;
		'add','additive': result := EBlendMode.Additive;
		else result := EBlendMode.Normal;
	end;
end;

type ETextureType = (None = 0, Stretched = 1, Tiled = 2);

function ETextureTypeFromString(const instr : UTF8string) : ETextureType;
begin
	case lowercase(instr) of
		'1'..'2': result := ETextureType(ord(instr[1]) - 48);
		'stretched': result := ETextureType.Stretched;
		'tiled': result := ETextureType.Tiled;
		else result := ETextureType.None;
	end;
end;

type EPopType = (Instant = 0, GrowShrink = 1, Fade = 2, Swipe = 3);

function EPopTypeFromString(const instr : UTF8string) : EPopType;
begin
	case lowercase(instr) of
		'1'..'3': result := EPopType(ord(instr[1]) - 48);
		'fade': result := EPopType.Fade;
		'swipe','wipe': result := EPopType.Swipe;
		else result := EPopType.Instant;
	end;
end;

{$ifdef sakucon}
type EPaletteMode = (LXYChroma, LXYLuma, RGB, Truecolor);
{$endif}

// Combinable values for sysvar.keysdown.
type EArrows = (Left, Right, Down, Up);

// Dropdown console states.
type EDDCState = (None, Log, Debug);

type
	// Generic 32k values, usually relative to viewport or other parent.
	TSize32k = object
		w, h : dword;
		function ToString : string;
	end;
	TCoord32k = object
		x, y : longint;
		function ToString : string;
	end;
	TEdgeSize32k = object
		left, right, top, bottom : dword;
		function ToString : string;
	end;
	TEdgeCoord32k = object
		left, right, top, bottom : longint;
		function ToString : string;
	end;
	// Generic pixel values, usually relative to viewport or other parent.
	TSizeP = object
		w, h : dword;
		procedure DeriveFrom32k(const size32k : TSize32k; const containersizep : TSizeP);
		function ToString : string;
	end;
	TCoordP = object
		x, y : longint;
		procedure DeriveFrom32k(const coord32k : TCoord32k; const containersizep : TSizeP);
		function ToString : string;
	end;
	TEdgeSizeP = TEdgeSize32k;
	TEdgeCoordP = TEdgeCoord32k;

	TSizes = object
		w, h, wp, hp : dword;
		procedure DerivePixelsFrom32k(const containersizep : TSizeP);
	end;
	TCoords = object
		x, y, xp, yp : longint;
		procedure DerivePixelsFrom32k(const containersizep : TSizeP);
	end;
	TEdgeSizes = object
		left, right, top, bottom : dword;
		leftp, rightp, topp, bottomp : dword;
		procedure DerivePixelsFrom32k(const containersizep : TSizeP);
	end;
	TEdgeCoords = object
		left, right, top, bottom : longint;
		leftp, rightp, topp, bottomp : longint;
		procedure DerivePixelsFrom32k(const containersizep : TSizeP);
	end;

	// Avoid using these where possible! Every floating point operation accumulates rounding error.
	TCoordFloat = record
		x, y : single;
	end;

	function TCoord32k.ToString : string;
	begin
		result := strdec(x) + ',' + strdec(y);
	end;
	function TSize32k.ToString : string;
	begin
		result := strdec(w) + 'x' + strdec(h);
	end;
	procedure TSizeP.DeriveFrom32k(const size32k : TSize32k; const containersizep : TSizeP);
	begin
		w := (size32k.w * containersizep.w + 16384) shr 15;
		h := (size32k.h * containersizep.h + 16384) shr 15;
	end;
	function TSizeP.ToString : string;
	begin
		result := strdec(w) + 'x' + strdec(h);
	end;
	procedure TCoordP.DeriveFrom32k(const coord32k : TCoord32k; const containersizep : TSizeP);
	begin
		if coord32k.x >= 0
			then x := (dword(coord32k.x) * containersizep.w + 16384) shr 15
			else x := -((dword(-coord32k.x) * containersizep.w + 16384) shr 15);
		if coord32k.y >= 0
			then y := (dword(coord32k.y) * containersizep.h + 16384) shr 15
			else y := -((dword(-coord32k.y) * containersizep.h + 16384) shr 15);
	end;
	function TCoordP.ToString : string;
	begin
		result := strdec(x) + ',' + strdec(y);
	end;
	procedure TSizes.DerivePixelsFrom32k(const containersizep : TSizeP);
	begin
		wp := (w * containersizep.w + 16384) shr 15;
		hp := (h * containersizep.h + 16384) shr 15;
	end;
	procedure TCoords.DerivePixelsFrom32k(const containersizep : TSizeP);
	begin
		if x >= 0
			then xp := (dword(x) * containersizep.w + 16384) shr 15
			else xp := -((dword(-x) * containersizep.w + 16384) shr 15);
		if y >= 0
			then yp := (dword(y) * containersizep.h + 16384) shr 15
			else yp := -((dword(-y) * containersizep.h + 16384) shr 15);
	end;
	procedure TEdgeSizes.DerivePixelsFrom32k(const containersizep : TSizeP);
	begin
		topp := (top * containersizep.h + 16384) shr 15;
		leftp := (left * containersizep.w + 16384) shr 15;
		rightp := (right * containersizep.w + 16384) shr 15;
		bottomp := (bottom * containersizep.h + 16384) shr 15;
	end;
	procedure TEdgeCoords.DerivePixelsFrom32k(const containersizep : TSizeP);
	begin
		if top >= 0
			then topp := (dword(top) * containersizep.h + 16384) shr 15
			else topp := -((dword(-top) * containersizep.h + 16384) shr 15);
		if left >= 0
			then leftp := (dword(left) * containersizep.w + 16384) shr 15
			else leftp := -((dword(-left) * containersizep.w + 16384) shr 15);
		if right >= 0
			then rightp := (dword(right) * containersizep.w + 16384) shr 15
			else rightp := -((dword(-right) * containersizep.w + 16384) shr 15);
		if bottom >= 0
			then bottomp := (dword(bottom) * containersizep.h + 16384) shr 15
			else bottomp := -((dword(-bottom) * containersizep.h + 16384) shr 15);
	end;
	function TEdgeSize32k.ToString : string;
	begin
		result := strcat('L% T% R% B%', [left, top, right, bottom]);
	end;
	function TEdgeCoord32k.ToString : string;
	begin
		result := strcat('L% T% R% B%', [left, top, right, bottom]);
	end;

// Runtime engine-wide variables. These are not serialised in save games.
// Except currentSong and alwaysLoopMusic. Those should be in a soundmatic instead of here...
var sysvar : record
		activeProjectName : UTF8string; // shortName of first loaded dat after front end, in lowercase
		activeBaseRes : TSizeP;
		windowTitle : UTF8string;
		windowSize : TSizeP;
		displayLanguage : UTF8string;
		currentSong : UTF8string; // base name of currently playing song, empty if none
		voidColor : RGBAquad; // rendering color for a gobless background
		{$ifdef sakucon}
		caretLocP : TCoordP; // if text input is enabled, the console port places the console caret here
		{$else}
		gamepadLeftStick, gamepadRightStick : TCoord32k;
		stickRepeatAfterMsecs : longint;
		{$endif}
		mouseLocP : TCoordP; // straight px coord within program window
		maxFiberSteps : dword; // a fiber is out of control and killed if it exceeds this many steps without yielding

		keyRepeatAfterMsecs : dword;
		keysDown : set of EArrows;
		keysPolled : boolean;

		metaState : EMetaState;
		// Main engine pause state. In SDL mode, press the pause key to pause/unpause. In console mode, it's Ctrl-P.
		// Shift-pause or Ctrl-Shift-P will execute a single step in every fiber, then pauses.
		pauseState : EPauseState;
		skipText : ESkipState;

		autoChoice : boolean; // use with skipText
		fullscreen : boolean;
		swallowRemainingUserInput : boolean;
		allowSaving : boolean;
		alwaysLoopMusic : boolean;
		{$ifndef sakucon}
		mutedAudio : boolean;
		{$endif}

		debugAllowed : byte; // if 5+, debug mode is allowed
		dropdownConsoleState : EDDCState;
		dropdownContentChanged : boolean;

		restart : boolean; // set to TRUE to reset the engine and restart the main script
		quit : boolean; // set to TRUE when quitting or restarting
	end;

{$if declared(useHeapTrace)}
// Compile with -ghl and call this anytime to test if at that moment the heap has yet been ruined.
procedure CheckHeap;
var p : pointer;
begin
	QuickTrace := FALSE;
	getmem(p, 4); freemem(p); p := NIL;
	QuickTrace := TRUE;
end;
{$endif}

var Varmon : TVarmon;
{$ifndef sakucon}
var soundserver : TBSoundServer = NIL;
{$endif}

{$include font-header.pas}
{$include elements-header.pas}
{$include fiber-header.pas}
{$include viewport-header.pas}
{$include gobs-header.pas}
{$include box-header.pas}
{$include effects-header.pas}
{$include event-header.pas}
{$include choice-header.pas}
{$include render-header.pas}

// ------------------------------------------------------------------

procedure SummonMessageBox(const txt : UTF8string); forward; // for LogError

var logbuffers : record
		transcriptBufIndex, debugBufIndex : dword;
		errorMsg : UTF8string;
		transcriptBuffer : array[0..63] of UTF8string;
		debugBuffer : array[0..63] of UTF8string;
	end;

procedure Log(const t : UTF8string);
begin
	EnterCriticalSection(LogCritter);
	writeln(LogFile, t);
	{$ifndef sakucon}
		{$ifndef WINDOWS}
			// Emphasis for error message and choice selections (anything in square brackets), using ANSI escapes.
			if (t <> '') and (t[1] = '[') then begin
				write(#27'[1m');
				writeln(t);
				write(#27'[0m');
			end else
		{$endif}
		writeln(t);
	{$endif}
	with logbuffers do begin
		debugBuffer[debugBufIndex] := t;
		debugBufIndex := (debugBufIndex + 1) and high(debugBuffer);
	end;
	LeaveCriticalSection(LogCritter);
	// If the debug log is currently visible, redraw it.
	if sysvar.dropdownConsoleState = EDDCState.Debug then sysvar.dropdownContentChanged := TRUE;
end;

procedure LogTranscript(const t : UTF8string);
begin
	with logbuffers do begin
		transcriptBuffer[transcriptBufIndex] := t;
		transcriptBufIndex := (transcriptBufIndex + 1) and high(transcriptBuffer);
	end;
	// If the transcript is currently visible, redraw it.
	if sysvar.dropdownConsoleState = EDDCState.Log then sysvar.dropdownContentChanged := TRUE;
end;

procedure LogError(const t : UTF8string);
begin
	log('[!] ' + t);
	if sysvar.quit then exit;
	if (Varmon = NIL) or (GetScript('_SAKUMETA.MSGBOX') = 0) then begin
		{$ifdef sakucon}
		writeln(t);
		{$else}
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, 'Error', @t[1], Rendermatic.SDL.mainWindowH);
		{$endif}
		exit;
	end;
	// Display an in-engine message box at the next frame change. (Metastate changes, so only done when safe.)
	if logbuffers.errorMsg = '' then
		logbuffers.errorMsg := t
	else
		logbuffers.errorMsg := logbuffers.errorMsg + '\n' + t;
end;

procedure LogError(const t : UTF8string; e : Exception; addr : pointer);
var i : dword;
	frames : ppointer;
begin
	LogError(t + ': ' + e.Message + LineEnding + BacktraceStrFunc(addr));
	if ExceptFrameCount <> 0 then begin
		frames := ExceptFrames;
		for i := 0 to ExceptFrameCount - 1 do Log(BacktraceStrFunc(frames[i]));
	end;
end;

{$ifdef sakucon}
procedure SetProgramName(newnamu : UTF8string);
begin
	sysvar.windowTitle := newnamu;
	if sysvar.pauseState = EPauseState.Paused then newnamu := newnamu + ' [paused]';
	CrtSetTitle(newnamu);
end;
{$else}
var sdl_GamepadH : PSDL_GameController;
	sdl_KeyState : pointer;

procedure SetProgramName(newnamu : UTF8string);
begin
	sysvar.windowTitle := newnamu;
	if sysvar.pauseState = EPauseState.Paused then newnamu := newnamu + ' [paused]';
	with Rendermatic.SDL do if newnamu <> '' then
		SDL_SetWindowTitle(mainWindowH, @newnamu[1])
	else
		SDL_SetWindowTitle(mainWindowH, NIL);
end;
{$endif}

// ------------------------------------------------------------------

{$ifndef sakucon}
// The dat-loader may need to resize the game window.
procedure SpawnWindow; forward;

// Timing stuff is provided by minicrt for the console port, SDL port can use these.
function GetMsecTime : ptruint; inline;
begin
	result := SDL_GetTicks;
end;

procedure Delay(msecs : dword); inline;
begin
	SDL_Delay(msecs);
end;
{$endif}
// User input key poller, invoked from sakufibercmds.
function PollKey(arrow : EArrows) : longint; forward;
// The dat-loader is used by the savestate loader.
function LoadDatCommon(loadname : UTF8string) : boolean; forward;
// Savestate loading needs to refresh engine var hooks.
procedure InitVarmon; forward;

// ------------------------------------------------------------------

{$include hubstack.pas}
{$include config.pas}
{$include font-implementation.pas}
{$include stringmatic.pas}
{$include savematic.pas}

function TryNumberFromString(const a : UTF8string; value : plongint) : boolean;
// Converts a decimal or hexadecimal at the start of a string into value^, returns TRUE if successful.
begin
	result := TRUE;
	result := (a.Length <> 0) and (a[1] in ['0'..'9','-']);
	if result then
		if (a.Length > 2) and (a[1] = '0') and (byte(a[2]) or $20 = byte('x')) then
			value^ := valhex(copy(a, 3))
		else
			value^ := valx(a);
end;

function VarmonGetProvider(const varname : string; const dest : TStringBunch) : longint;

	procedure _BoxOrNil(_box : TTextBox);
	begin
		if dest = NIL then exit;
		if _box <> NIL then
			dest.SetAll(_box.boxName)
		else
			dest.SetAll('');
	end;

begin
	result := 0;
	case lowercase(varname) of
		'_allowsaving': if sysvar.allowSaving then result := 1 else result := 0;
		'_alwaysloopmusic': if sysvar.alwaysLoopMusic then result := 1 else result := 0;
		'_autosaveminutes': result := preference.autoSaveAfterMSec div 60000;
		'_autosaveslots': result := preference.autoSaveSlots;
		'_cancancelchoice': result := Choicematic.userCanCancel;
		'_defaultviewport': result := longint(EleHub.defaultViewport);
		'_fullscreen': if sysvar.fullscreen then result := 1 else result := 0;
		'_skiptext': result := longint(sysvar.skipText);
		'_voidcolor': result := sysvar.voidColor.ToRGBA4;

		'_mainbox': if dest <> NIL then dest.SetAll(BoxHub.defaultTextbox.boxName);
		'_titlebox': _BoxOrNil(BoxHub.dialogueTitleBox);
		'_choicebox': _BoxOrNil(Choicematic.choiceBox);
		'_choicepartbox': _BoxOrNil(Choicematic.choicePartBox);
		'_highlightbox': _BoxOrNil(Choicematic.highlightBox);

		'_displaylanguage': if dest <> NIL then dest.SetAll(sysvar.displayLanguage);
		'_escint': if dest <> NIL then dest.SetAll(Eventmatic.escInterruptLabel);
		'_interrupt': if dest <> NIL then dest.SetAll(Eventmatic.interruptLabel);
		'_onhighlight': if dest <> NIL then dest.SetAll(Choicematic.onHighlight);
		'_windowtitle': if dest <> NIL then dest.SetAll(sysvar.windowTitle);
		else begin
			if dest <> NIL then dest[0] := '';
			LogError('no provider for var ' + varname);
		end;
	end;
	//if dest <> NIL then log(strcat('get $% = %|%', [varname, result, dest[0]])) else log(strcat('get $% = %|null', [varname, result]));
end;

procedure VarmonSetProvider(const varname : string; const src : TStringBunch; num : longint);
// Mcvarmon calls this when any of the designated special variables is being set. The variable defines its own type,
// and can use either the direct num parameter or src^. The caller is responsible for filling in the correct type;
// the incorrect type's value is undefined.
// The caller should ensure index 0 has a valid value, since that's probably what this will use.
var s : UTF8string;

	function _BoxOrNil : TTextBox;
	begin
		result := NIL;
		if (src <> NIL) and (src[0] <> '') then begin
			s := upcase(src[0]);
			result := BoxHub.GetBox(s);
			if result = NIL then result := TTextBox.Create(s);
		end;
	end;

begin
	//if src <> NIL then log(strcat('set $% := %|%', [varname, num, src[0]])) else log(strcat('set $% := %|null', [varname, num]));
	case lowercase(varname) of
		'_allowsaving': sysvar.allowSaving := num <> 0;
		'_alwaysloopmusic':
		begin
			sysvar.alwaysLoopMusic := num <> 0;
			{$ifndef sakucon}
			soundserver.alwaysLoop := sysvar.alwaysLoopMusic;
			{$endif}
		end;
		'_autosaveminutes': preference.autoSaveAfterMSec := num * 60000;
		'_autosaveslots': preference.autoSaveSlots := num;
		'_cancancelchoice': Choicematic.userCanCancel := num and 3;
		'_defaultviewport':
		if num > 99999 then
			LogError('bad default viewport: ' + strdec(num))
		else begin
			if num >= length(Viewportmatic.viewport) then
				Viewportmatic.SetNumViewports(num + 1);
			EleHub.defaultViewport := num;
		end;
		'_fullscreen':
		if sysvar.fullscreen <> (num <> 0) then begin
			sysvar.fullscreen := num <> 0;
			{$ifndef sakucon} SpawnWindow; {$endif}
		end;
		'_skiptext': sysvar.skipText := ESkipState(num);
		'_voidcolor':
		begin
			sysvar.voidColor.FromRGBA4(dword(num));
			Rendermatic.AddRefresh(0, 0, high(longint), high(longint), 0);
		end;

		'_mainbox':
		begin
			if src[0] = '' then
				s := 'MAINBOX'
			else
				s := upcase(src[0]);
			BoxHub.defaultTextbox := BoxHub.GetBox(s);
			if BoxHub.defaultTextbox = NIL then BoxHub.defaultTextbox := TTextBox.Create(s);
		end;
		'_titlebox': BoxHub.dialogueTitleBox := _BoxOrNil;
		'_choicebox': Choicematic.choiceBox := _BoxOrNil;
		'_choicepartbox': Choicematic.choicePartBox := _BoxOrNil;
		'_highlightbox': Choicematic.highlightBox := _BoxOrNil;

		'_displaylanguage': if src <> NIL then sysvar.displayLanguage := src[0];
		'_escint':
		with Varmon do with FiberHub do with Eventmatic do
			if (src = NIL) or (src[0] = '') then
				escInterruptLabel := ''
			else
				escInterruptLabel := fiber[executingId].ExpandRelativeLabel(upcase(src[0]));
		'_interrupt':
		with Varmon do with FiberHub do with Eventmatic do
			if (src = NIL) or (src[0] = '') then
				interruptLabel := ''
			else
				interruptLabel := fiber[executingId].ExpandRelativeLabel(upcase(src[0]));
		'_onhighlight':
		with Choicematic do with FiberHub do
			if (src = NIL) or (src[0] = '') then
				onHighlight := ''
			else
				onHighlight := fiber[executingId].ExpandRelativeLabel(upcase(src[0]));
		'_windowtitle': if src <> NIL then SetProgramName(src[0]);
	end;
end;

procedure InitVarmon;
begin
	if Varmon = NIL then Varmon := TVarmon.Create(0, 0);
	with Varmon do begin
		callbackGetter := @VarmonGetProvider;
		callbackSetter := @VarmonSetProvider;
		// Init protected variables. Accessing these from scripts gets forwarded to the above providers.
		SetCallback('_allowsaving', TRUE, VT_INT);
		SetCallback('_alwaysloopmusic', TRUE, VT_INT);
		SetCallback('_autosaveslots', TRUE, VT_INT);
		SetCallback('_autosaveminutes', TRUE, VT_INT);
		SetCallback('_cancancelchoice', TRUE, VT_INT);
		SetCallback('_defaultviewport', TRUE, VT_INT);
		SetCallback('_fullscreen', TRUE, VT_INT);
		SetCallback('_skiptext', TRUE, VT_INT);
		SetCallback('_voidcolor', TRUE, VT_INT);

		SetCallback('_mainbox', TRUE, VT_STR);
		SetCallback('_titlebox', TRUE, VT_STR);
		SetCallback('_choicebox', TRUE, VT_STR);
		SetCallback('_choicepartbox', TRUE, VT_STR);
		SetCallback('_highlightbox', TRUE, VT_STR);
		SetCallback('_displaylanguage', TRUE, VT_STR);
		SetCallback('_escint', TRUE, VT_STR);
		SetCallback('_interrupt', TRUE, VT_STR);
		SetCallback('_onhighlight', TRUE, VT_STR);
		SetCallback('_windowtitle', TRUE, VT_STR);

		// Any global preference or current session variables shouldn't be stashed in savestates.
		IgnoreVar('_autosaveslots', TRUE);
		IgnoreVar('_autosaveminutes', TRUE);
		IgnoreVar('_fullscreen', TRUE);
		IgnoreVar('_skiptext', TRUE);
		// Choice-related things get saved early so no need to duplicate in varstate.
		IgnoreVar('_choicebox', TRUE);
		IgnoreVar('_choicepartbox', TRUE);
		IgnoreVar('_highlightbox', TRUE);
		IgnoreVar('_onhighlight', TRUE);
	end;
end;

procedure ResetDefaults;
// Resets the engine state nearly completely to default values.
// Called right before the main fiber is spawned, before the start of the main loop. Also happens on game restart.
// Ensures starting is clean and restarting will not have carryover oddities.
var i : dword;
begin
	{$ifndef sakucon}
	// Stop accepting SDL text input, if appropriate.
	if SDL_IsTextInputActive = SDL_TRUE then SDL_StopTextInput;
	// Rapid fadeout and stop music.
	soundserver.FadeOut(320);
	{$endif}

	with sysvar do begin
		windowTitle := activeProjectName;
		SetProgramName(windowTitle);
		pauseState := EPauseState.Normal;
		metaState := EMetaState.Normal;
		dropdownConsoleState := EDDCState.None;
		displayLanguage := preference.displayLanguage;
		currentSong := '';
		dword(voidColor) := 0;
	end;
	Savematic.lastAutoSaveTime := GetTickCount64;

	while HubStack.Pop do; // reset

	// Init/restart the variable monster. Languagelist was set to the number of languages when the DAT was loaded.
	// We'll start with 16 variable buckets, plenty for most purposes.
	Varmon.Init(languageList.Length, 16);
	InitVarmon;

	Viewportmatic.SetNumViewports(1);

	EleHub.Reset;
	setlength(EleHub.ele, 12);

	// Clear the transcript log.
	with logbuffers do for i := high(transcriptBuffer) downto 0 do transcriptBuffer[i] := '';

	// Reset various stuff.
	{RGBtweakactive := $FF;
	for i := 255 downto 0 do begin
		RGBtweakTable[i] := i;
		RGBtweakTable[i or 256] := i;
		RGBtweakTable[i or 512] := i;
	end;}

	// Clear the screen too.
	Rendermatic.Reset;

	randomize;
end;

// ------------------------------------------------------------------

function FindDat(const loadname : UTF8string) : dword;
// Check for matching dat files in base directory, user's local profile directory, and custom dat directory.
// Dat overriding isn't supported, so they must be read in order of user preference.
begin
	result := 0;
	if preference.customDataDirectory <> '' then
		if EnumerateDats(PathCombine([preference.customDataDirectory, loadname])) then result := availableDatCount - 1;
	if EnumerateDats(PathCombine([GetAppConfigDir(FALSE), loadname])) then result := availableDatCount - 1;
	if EnumerateDats(PathCombine([sakuparam.basePath, 'data', loadname])) then result := availableDatCount - 1;
end;

procedure NiceSortDats;
// Sort dats by displayName. Shift the frontend dat to the end of the list so it can be filtered out easily.
var d : TDatFile;
	i : dword;
begin
	SortDats;
	i := GetDat(frontEndName);
	if i = 0 then raise Exception.Create(frontEndName + '.dat not found!');
	d := availableDats[i];
	while i + 1 < availableDatCount do begin
		availableDats[i] := availableDats[i + 1];
		inc(i);
	end;
	availableDats[i] := d;
end;

function LoadDatCommon(loadname : UTF8string) : boolean;
// Loads the given dat project name, which implicitly loads all the dat's parent dats too. Afterward, loads all loose
// files from the project's override directory. Calls EnumerateDats to populate availableDats[] if dat not there yet.
// Returns TRUE for success, FALSE for fail.
var datindex : dword;
	{$ifndef sakucon}
	basereschanged : boolean;
	{$endif}

	procedure _LoadOverrides(const path : UTF8string);
	var bunch : TStringBunch;
	begin
		bunch := FindFiles_caseless(path, TRUE, TRUE);
		if bunch = NIL then exit;
		log('found override path!');
		if LoadFileTree(bunch[0]) <> 0 then LogError('Failed to load ' + path + ', see log for details');
	end;

begin
	result := FALSE;
	loadname := lowercase(loadname);
	datindex := GetDat(loadname);
	if datindex = 0 then begin
		datindex := FindDat(loadname + '.dat');
		if datindex = 0 then begin
			LogError('LoadDatCommon: ' + loadname + ' not found');
			exit;
		end;
	end;

	// The console port just needs to load the dat.
	// The SDL port may also need to take into account a base resolution change.
	try
		availableDats[datindex].LoadDat;
		_LoadOverrides(PathCombine([sakuparam.basePath, 'data', availableDats[datindex].projectName, 'override']));
		_LoadOverrides(PathCombine([GetAppConfigDir(FALSE), availableDats[datindex].projectName, 'override']));

		with sysvar do if (activeProjectName = '') or (activeProjectName = frontEndName) then begin
			sysvar.activeProjectName := availableDats[datindex].projectName;
			log('Active project now: ' + sysvar.activeProjectName);
		end;

		{$ifndef sakucon}
		basereschanged :=
			(sysvar.activeBaseRes.w <> availableDats[datindex].baseResX)
			or (sysvar.activeBaseRes.h <> availableDats[datindex].baseResY);
		{$endif}
		sysvar.activeBaseRes.w := availableDats[datindex].baseResX;
		sysvar.activeBaseRes.h := availableDats[datindex].baseResY;
		{$ifndef sakucon}
		if (Rendermatic.SDL.mainWindowH <> NIL) and (NOT preference.usePrefWindowSize) and (basereschanged) then
			SpawnWindow;
		{$endif}
	except
		on e : Exception do LogError('LoadDatCommon', e, ExceptAddr);
	end;

	if languageList.Length > 1 then CountUntranslated;

	// Currently running fibers must use updated script indexes.
	FiberHub.UpdateScriptIndexes;

	Savematic.lastAutoSaveTime := GetTickCount64;
	result := TRUE;
end;

procedure SetPauseState(newstate : EPauseState);
// Call to switch cleanly between pause states.
{$ifdef sakucon}
var i : dword;
{$endif}
begin
	with sysvar do if pauseState <> newstate then begin
		if newstate = EPauseState.Paused then begin
			Rendermatic.DarkenOutput;
			{$ifndef sakucon}
			soundserver.PausePlayback(TRUE);
			{$endif}
		end
		else begin
			Rendermatic.AddRefresh(0, 0, high(longint), high(longint), 0);
			{$ifdef sakucon}
			with BoxHub do for i := high(textbox) downto 0 do with textbox[i] do
				boxNeedsRedraw := boxNeedsRedraw or (NOT isHidden);
			{$else}
			soundserver.PausePlayback(FALSE);
			{$endif}
		end;

		pauseState := newstate;
		SetProgramName(windowTitle);
	end;
end;

// ------------------------------------------------------------------

procedure SummonSaveLoad;
// If the game state allows it, shifts execution to the SAVELOAD script.
begin
end;

procedure SummonMetaMenu;
begin
	sysvar.autoChoice := FALSE;
	sakuparam.frameDelay := 0;
	if sysvar.metaState <> EMetaState.Normal then exit;
	HubStack.Push(TRUE);
	sysvar.metaState := EMetaState.Meta;
	TFiber.Create('_sakumeta.menu', 'META');
end;

procedure PrintInfoBox;
var i : dword;
begin
	with BoxHub.GetBox('METABOX') do begin
		Print('\CSuperSakura ' + GetSuperSakuraVersion);
		Print('\nCopyright 2009-' + copyrightYear + ', Kirinn Bunnylin / Mooncore');
		Print('\nhttps://supersakura.net/\nhttps://gitlab.com/bunnylin/supersakura');
		Print('\n\n\LCurrently loaded data files:');
		for i := 1 to high(availableDats) do with availableDats[i] do if loaded then
			Print(strcat('\n• % (%)', [displayName, projectName])); //, gameVersion]));
		Print(strcat('\n\nCurrent log and configuration file:\n• %\n• %',
			[sakuparam.logPath.Replace('\', '\\'), sakuparam.iniPath.Replace('\', '\\')]));
		Print('\n\nThis program is free software: you can redistribute it and/or modify it under the terms of the GNU '
			+ 'General Public License as published by the Free Software Foundation, either version 3 of the License, '
			+ 'or (at your option) any later version.');
		Print('\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without '
			+ 'even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General '
			+ 'Public License for more details.');
		Print('\n\nYou should have received a copy of the GNU General Public License along with this program.  If '
			+ 'not, see <https://www.gnu.org/licenses/>.');
		Print('\n\nSuperSakura');
		{$ifdef sakucon}
		Print('-con');
		{$endif}
		Print(' gratefully uses these components under their individual licenses:');
		{$ifndef sakucon}
		Print('\n• Simple Directmedia Library SDL2 and SDL2_ttf (ZLib license)');
		Print('\n• Community-maintained Pascal translation of SDL headers (dual ZLib/MPL)');
		Print('\n• FluidSynth (LGPL license)');
		Print('\n• BunnySynth (ZLib license)');
		{$endif}
		Print('\n• Free Pascal Compiler runtime library (modified LGPL)');
	end;
end;

procedure SummonInfoBox;
begin
	if sysvar.metaState = EMetaState.Normal then begin
		HubStack.Push(TRUE);
		sysvar.metaState := EMetaState.Meta;
	end;
	TFiber.Create('_sakumeta.info', 'META');
end;

procedure SummonSettingsDialog;
begin
end;

procedure SummonMessageBox(const txt : UTF8string);
begin
	sysvar.autoChoice := FALSE;
	if sysvar.metaState = EMetaState.ConfirmQuit then exit; // if error during quit, remain in quitting state
	sakuparam.frameDelay := 0;
	Varmon.SetStrVar('_msgboxtxt', txt);
	HubStack.Push(TRUE);
	sysvar.metaState := EMetaState.Meta;
	TFiber.Create('_sakumeta.msgbox', 'META');
end;

procedure SummonConfirmQuit;
begin
	if sysvar.metaState = EMetaState.ConfirmQuit then begin
		sysvar.quit := TRUE;
		exit;
	end;
	sysvar.autoChoice := FALSE;
	sakuparam.frameDelay := 0;
	HubStack.Push(TRUE);
	sysvar.metaState := EMetaState.ConfirmQuit;
	TFiber.Create('_sakumeta.quit', 'QUIT');
end;

