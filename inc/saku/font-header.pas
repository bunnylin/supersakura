{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

type TFont = class
	// user font height = origFontHeight * user's UI magnification (32k)
	// requested pixel height = user font height * viewport pixel height / 32k
	requestedHeightP : dword;

	// After the font is created, these are the metrics received, used for rendering. The width is an ex-width.
	// If the received font is taller than requested, the font may have hardcoded extra linespacing. This can be
	// deducted when calculating our desired pixel line height.
	fontHeightP, exWidthP, extraLinespaceP : dword;

	{$ifndef sakucon}
	// Font handle used by SDL. Nominally PTTF_Font, but the structure contents are for SDL's internal use
	// only, so we only get a pointer.
	sdlHandle : pointer;
	{$endif}
end;

type TFontmatic = object
	// When a font is requested, hits are cached here.
	font : array of record
		match, path : UTF8string;
		instances : array of TFont;
	end;

	function GetFont(namepath : UTF8string; lang : UTF8string; heightp : dword) : TFont;
	{$ifndef sakucon}
	function FindFontFile(namepath : UTF8string) : UTF8string;
	{$endif}
	procedure Reset;
end;

var Fontmatic : TFontmatic;

