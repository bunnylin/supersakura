{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// SuperSakura text box functions.

function TTextBox.GetUTF8Size(poku : pointer; slen : dword) : dword;
// Returns the width in pixels of the given UTF-8 string, by forwarding the call to SDL's TTF_SizeUTF8. Poku must point
// to a valid UTF-8 byte sequence, and slen is the byte length of the sequence. Poku^ must also be slen + 1 bytes, or
// more, to allow for a temporary terminating zero.
var i, j : dword;
begin
	// Save the original string's terminating byte, and insert a zero.
	i := byte((poku + slen)^);
	byte((poku + slen)^) := 0;
	// Get the pixel size.
	j := 1;
	if TTF_SizeUTF8(boxFont.font.sdlHandle, poku, @j, NIL) <> 0 then
		LogError('GetUTF8Size: ' + TTF_GetError);
	GetUTF8Size := j;
	// Restore the original string.
	byte((poku + slen)^) := i;
end;

// ------------------------------------------------------------------

procedure TTextBox.RenderContent;
// Draws the current box content in the content buffer. The content buffer will only contain raster text, maybe some
// emojis and a portrait image; it'll be composited over the base box image later.
var txtsurface : PSDL_Surface;
	color1, color2 : TSDL_Color;
	runcolor, textcoloramul : RGBAquad;
	txtofs, txtmark, escindex, choiceindex : dword;
	i, rowsizexp, totalsizeyp : dword;
	runalign : longint;
	destp : pointer;
	luggage : byte;
	runchoice : boolean;

	procedure _DrawCaret; //inline; // <-- compiler warning -_-
	// Draws a non-blinking text input caret at the current text position.
	var basep : pointer;
		y, caretwp, carethp, rowskipbytes : dword;
	begin
		rowskipbytes := contentWin.contentWinSizeP.w * 4;
		carethp := boxFont.font.fontHeightP;
		caretwp := carethp shr 4 + 1;
		basep := buffers.scratchP + rowsizexp * 4;
		for y := carethp - 1 downto 0 do begin
			filldword(basep^, caretwp, dword(textcoloramul));
			inc(basep, rowskipbytes);
		end;
		inc(rowsizexp, caretwp);
	end;

	procedure _StashDivider(heightp : dword);
	// Draws a thin divider line directly in contentFullP. There must be nothing else on this row.
	var padtop, padbottom, linetop, linebottom : dword;
	begin
		if heightp = 0 then exit;
		inc(totalsizeyp, heightp);
		linebottom := 0;
		linetop := heightp shr 3 + 1;
		dec(heightp, linetop);
		if heightp >= linetop then begin
			linebottom := linetop;
			dec(heightp, linetop);
		end;

		padtop := heightp shr 1;
		padbottom := heightp - padtop;

		with contentWin do begin
			if padtop <> 0 then begin
				padtop := padtop * contentWinSizeP.w;
				filldword(destp^, padtop, 0);
				inc(destp, padtop shl 2);
			end;

			linetop := linetop * contentWinSizeP.w;
			filldword(destp^, linetop, dword(textcoloramul));
			inc(destp, linetop shl 2);

			if linebottom <> 0 then begin
				linebottom := linebottom * contentWinSizeP.w;
				filldword(destp^, linebottom, (dword(textcoloramul) shr 1) and $7F7F7F7F);
				inc(destp, linebottom shl 2);
			end;

			if padbottom <> 0 then begin
				padbottom := padbottom * contentWinSizeP.w;
				filldword(destp^, padbottom, 0);
				inc(destp, padbottom shl 2);
			end;
		end;
	end;

	procedure _StashSpace(widthp : dword); //inline; // <-- compiler warning -_-
	// Adds the given width of pixels of empty space to the row buffer.
	var writep : pointer;
		skipw, y : dword;
	begin
		writep := buffers.scratchP + rowsizexp * 4;
		skipw := contentWin.contentWinSizeP.w * 4;
		for y := boxFont.font.fontHeightP - 1 downto 0 do begin
			filldword(writep^, widthp, 0);
			inc(writep, skipw);
		end;
		inc(rowsizexp, widthp);
	end;

	procedure _StashText; //inline; // <-- compiler warning -_-
	// Appends the SDL-provided text surface to the row buffer.
	var srcp, basep, writep : pointer;
		x, y, rowendskip : dword;
		a : byte;
	begin
		srcp := txtsurface^.pixels;
		basep := buffers.scratchP + rowsizexp * 4;
		rowendskip := txtsurface^.pitch - txtsurface^.w;
		for y := txtsurface^.h - 1 downto 0 do begin
			writep := basep;
			for x := txtsurface^.w - 1 downto 0 do begin
				case byte(srcp^) of
					0:
					begin
						dword(writep^) := 0;
						inc(writep, 4);
					end;

					$FF:
					begin
						dword(writep^) := dword(textcoloramul);
						inc(writep, 4);
					end;

					else begin
						a := alphamixtab[byte(srcp^), runcolor.a];
						byte(writep^) := alphamixtab[runcolor.b, a]; inc(writep);
						byte(writep^) := alphamixtab[runcolor.g, a]; inc(writep);
						byte(writep^) := alphamixtab[runcolor.r, a]; inc(writep);
						byte(writep^) := a; inc(writep);
					end;
				end;
				inc(srcp);
			end;
			inc(basep, contentWin.contentWinSizeP.w * 4);
			inc(srcp, rowendskip);
		end;
		inc(rowsizexp, dword(txtsurface^.w));
	end;

	procedure _FinaliseRow;
	var lclear, rclear, y : dword;
		readp : pointer;
	begin
		// Line padding, top.
		with contentWin do y := ((lineHeightP - boxFont.font.fontHeightP + 1) shr 1) * contentWinSizeP.w;
		filldword(destp^, y, 0);
		inc(destp, y * 4);

		// Calculate how much empty space should be cleared in the content buffer on both sides of this complete row.
		// Depends on text alignment.
		lclear := 0; rclear := 0;

		y := contentWin.contentWinSizeP.w - rowsizexp;
		case runalign of
			0: rclear := y;
			16384:
			begin
				lclear := y shr 1;
				rclear := y - lclear;
			end;
			32768: lclear := y;
			else begin
				lclear := (longint(y) * runalign + 16384) shr 15;
				rclear := y - lclear;
			end;
		end;

		readp := buffers.scratchP;
		rowsizexp := rowsizexp * 4;

		for y := boxFont.font.fontHeightP - 1 downto 0 do begin
			// Clear the left side.
			if lclear <> 0 then begin
				filldword(destp^, lclear, 0);
				inc(destp, lclear * 4);
			end;
			// Copy the row.
			move(readp^, destp^, rowsizexp);
			inc(readp, contentWin.contentWinSizeP.w * 4);
			inc(destp, rowsizexp);
			// Clear the right side.
			if rclear <> 0 then begin
				filldword(destp^, rclear, 0);
				inc(destp, rclear * 4);
			end;
		end;

		// Line padding, bottom.
		with contentWin do y := ((lineHeightP - boxFont.font.fontHeightP) shr 1) * contentWinSizeP.w;
		filldword(destp^, y, 0);
		inc(destp, y * 4);

		// Check all choices on this row, if any, adjust choice rects by lclear.
		if lclear <> 0 then begin
			y := choiceindex;
			while y <> 0 do begin
				dec(y);
				with content.choiceList[y] do
					if showLocP.top >= longint(totalsizeyp) then begin
						inc(showLocP.left, longint(lclear));
						inc(showLocP.right, longint(lclear));
					end;
			end;
		end;

		rowsizexp := 0;
		inc(totalsizeyp, contentWin.lineHeightP);
		if style.underlineAllRows then _StashDivider(boxFont.font.fontHeightP shr 4 + 1);
	end;

	procedure _EndChoiceRun;
	begin
		if (rowsizexp <> 0)
		and (choiceindex <> 0)
		and (choiceindex <= content.choiceCount) then
		with content.choiceList[choiceindex - 1] do begin
			if longint(rowsizexp) > showLocP.right then showLocP.right := rowsizexp;
			with contentWin do
				showLocP.bottom := totalsizeyp + lineHeightP - (lineHeightP - boxFont.font.fontHeightP) shr 1;
			//log(strcat('showlist % end: %', [choiceindex - 1, showLocP.ToString]));
		end;
	end;

	procedure _NewColor(colval : RGBAquad); inline;
	// Selects a new text color.
	begin
		runcolor := colval;
		textcoloramul := colval;
		textcoloramul.b := alphamixtab[textcoloramul.b, textcoloramul.a];
		textcoloramul.g := alphamixtab[textcoloramul.g, textcoloramul.a];
		textcoloramul.r := alphamixtab[textcoloramul.r, textcoloramul.a];
	end;

	procedure _Emote(index : dword); inline;
	var readp, basep, writep : pointer;
		icons : TGraphicObject;
		x, y : dword;
		a : byte;
	begin
		icons := GetGraphicObject('_EMOTES', boxFont.font.fontHeightP, boxFont.font.fontHeightP);
		readp := icons.framePtr[index mod dword(length(icons.framePtr))] + 3; // only care about alpha channel
		basep := buffers.scratchP + rowsizexp * 4;
		for y := icons.frameHeightP - 1 downto 0 do begin
			writep := basep;
			for x := icons.sizeXP - 1 downto 0 do begin
				case byte(readp^) of
					0:
					begin
						dword(writep^) := 0;
						inc(writep, 4);
					end;
					$FF:
					begin
						dword(writep^) := dword(textcoloramul);
						inc(writep, 4);
					end;
					else begin
						a := alphamixtab[byte(readp^), runcolor.a];
						byte(writep^) := alphamixtab[runcolor.b, a]; inc(writep);
						byte(writep^) := alphamixtab[runcolor.g, a]; inc(writep);
						byte(writep^) := alphamixtab[runcolor.r, a]; inc(writep);
						byte(writep^) := a; inc(writep);
					end;
				end;
				inc(readp, 4);
			end;
			inc(basep, contentWin.contentWinSizeP.w * 4);
		end;
		inc(rowsizexp, icons.sizeXP);
	end;

begin
	Assert(boxFont.font <> NIL);
	Assert(boxFont.font.sdlHandle <> NIL);
	with buffers do begin
		// Make sure the scratch + content buffer is suitably-sized.
		i := contentWin.contentWinSizeP.w * (content.fullHeightP + boxFont.font.fontHeightP) * 4;
		if (scratchP = NIL) or (i > contentAllocSize) or (i < contentAllocSize shr 3) then begin
			// One extra row of leeway.
			contentAllocSize := i + contentWin.contentWinSizeP.w * contentWin.lineHeightP * 4;
			if scratchP <> NIL then freemem(scratchP);
			getmem(scratchP, contentAllocSize); // can't use reallocmem, not guaranteed to shrink in place
		end;
		contentFullP := scratchP + contentWin.contentWinSizeP.w * boxFont.font.fontHeightP * 4;
	end;

	rowsizexp := 0; totalsizeyp := 0; choiceindex := 0;
	runalign := style.textAlign;
	destp := buffers.contentFullP;
	runchoice := FALSE;

	with content do begin
		// Make sure there's at least one byte of space past the end of the content string for a terminating null byte.
		if txtLength >= dword(length(txtContent)) then setlength(txtContent, txtLength + 8);

		escindex := 0; txtofs := 0;
		dword(color1) := $FFFFFFFF;
		dword(color2) := 0;
		_NewColor(style.textColor); // not needed, default color now first escape code; set runcolor and textcoloramul directly to color1/2
		TTF_SetFontStyle(boxFont.font.sdlHandle, 0);

		repeat
			// Check for escape codes at current txt offset.
			while (escindex < escapeCount) and (escapeList[escindex].escapeOfs = txtofs) do begin
				{with escapeList[escindex] do log(strcat('box % @%: esc %/%=% data=%',
					[boxName, txtofs, escindex, escapeCount, ByteToAscii(escapeCode), longint(escapeData)]));}
				with escapeList[escindex] do case escapeCode of
					1: _DrawCaret;
					10, byte('n'):
					begin
						if runchoice then _EndChoiceRun;
						_FinaliseRow;
					end;
					byte('B'): TTF_SetFontStyle(boxFont.font.sdlHandle, TTF_STYLE_BOLD);
					byte('b'): TTF_SetFontStyle(boxFont.font.sdlHandle, 0);
					byte('c'): _NewColor(RGBAquad(escapeData));
					byte('L'): runalign := 0;
					byte('C'): runalign := 16384;
					byte('R'): runalign := 32768;
					byte(':'): ;
					byte('&'): _Emote(escapeData);

					byte('t'): if escapeData <> 0 then _StashSpace(escapeData);
					byte('-'): _StashDivider(escapeData);

					byte('?'):
					if choiceindex < choiceCount then begin
						if runchoice then _EndChoiceRun; // no nested choices!
						// Remember the start coordinate of this choice rect.
						with choiceList[choiceindex] do begin
							showLocP.left := rowsizexp;
							showLocP.right := rowsizexp;
							showLocP.top := totalsizeyp + (contentWin.lineHeightP - boxFont.font.fontHeightP + 1) shr 1;
							showLocP.bottom := totalsizeyp;
							//log(strcat('showlist % begin: %', [choiceindex, showLocP.ToString]));
							inc(choiceindex);
							runchoice := TRUE;
						end;
					end;

					byte('.'):
					if runchoice then begin
						_EndChoiceRun;
						runchoice := FALSE;
					end;
				end;
				inc(escindex);
			end;

			// Is this the end of the text?
			if txtofs = txtLength then break;

			// Calculate the distance to the next escape, linebreak, or end of text.
			txtmark := txtLength;
			if (escindex < escapeCount) and (escapeList[escindex].escapeOfs < txtmark) then
				txtmark := escapeList[escindex].escapeOfs;

			// Render text up to the next txtmark.
			luggage := txtContent[txtmark];
			txtContent[txtmark] := 0;
			txtsurface := TTF_RenderUTF8_Shaded(boxFont.font.sdlHandle, @txtContent[txtofs], color1, color2);
			{if copy(PChar(@txtContent[txtofs]), 1, 5) <> '::box' then log(strcat('::box % render %x% pitch % [%]',
				[boxName, txtsurface^.w, txtsurface^.h, txtsurface^.pitch, copy(PChar(@txtContent[txtofs]), 1, txtmark - txtofs)]));}
			txtContent[txtmark] := luggage;
			if txtsurface = NIL then begin
				LogError('TTF_RenderUTF8 fail: ' + TTF_GetError);
				break;
			end;
			// There's at least one case where the surface is bigger than promised:
			// Majokko in Japanese, MSGothic 33px font on Windows 10; height = 34.
			// Safest to pretend the overflow doesn't exist.
			if dword(txtsurface^.h) > boxFont.font.fontHeightP then txtsurface^.h := boxFont.font.fontHeightP;
			_StashText;
			SDL_FreeSurface(txtsurface);

			txtofs := txtmark;
		until FALSE;
	end;
	_FinaliseRow;

	contentNeedsRender := FALSE;
	finalNeedsRender := TRUE;
end;

procedure TTextBox.RenderBase;
// Draws the base image for the textbox, at the box's current size.
// This includes a gradient background, a stretched texture, bevelled edges, and frame decorations.
var bufp : pointer;
	i : dword;

	procedure _ApplyTexture;
	var texobject : TGraphicObject;
		tempbmp : mcg_bitmap = NIL;
		clipsi : blitstruct;
		src_x2, src_y2, dest_x1, dest_y1, dest_x2, dest_y2 : dword;
		src_w2, src_h2, dest_w1, dest_h1, dest_w2, dest_h2, dest_w3, dest_h3, tile_w2, tile_h2 : dword;
		j : dword;

		procedure _ScaleBox(srcx1, srcy1, srcwidth, srcheight : dword; tilewidth, tileheight : dword;
			destx1, desty1, destwidth, destheight : dword);
		// Lifts the source rectangle from graphicObjects[gfxindex].bitmap^, resizes it to a new width/height, then
		// stretches or tiles it onto basebuf^ over the given destination rectangle.
		var srcp, destp : pointer;
			srcw, destw, y : dword;
		begin
			if (tilewidth = 0) or (tileheight = 0) or (destwidth = 0) or (destheight = 0) then exit;

			// Get the source rectangle into tempbmp.
			tempbmp.sizeXP := srcwidth;
			tempbmp.sizeYP := srcheight;
			srcw := texobject.sizeXP * 4;
			destw := tempbmp.sizeXP * 4;
			tempbmp.stride := destw;
			setlength(tempbmp.bitmap, destw * tempbmp.sizeYP);
			srcp := @texobject.bitmap[0] + srcy1 * srcw + srcx1 * 4;
			destp := @tempbmp.bitmap[0];
			// (ideally should resize directly from srcp^ without this intermediate tempbmp copy)
			y := tempbmp.sizeYP;
			while y <> 0 do begin
				dec(y);
				move(srcp^, destp^, destw);
				inc(srcp, srcw);
				inc(destp, destw);
			end;

			if style.texture.texType = ETextureType.Stretched then begin
				// Resize directly to target rectangle size.
				tempbmp.Resize(destwidth, destheight);
			end
			else begin
				// Resize to intermediate size, then tile the rest.
				tempbmp.Resize(tilewidth, tileheight);
				tempbmp.Tile(destwidth, destheight);
			end;

			with clipsi do begin
				srcp := @tempbmp.bitmap[0];
				destp := bufp + (desty1 * boxRect.marginedSizeP.w + destx1) * 4;
				copywidth := tempbmp.sizeXP;
				copyrows := tempbmp.sizeYP;
				srcskipwidth := 0;
				destskipwidth := (boxRect.marginedSizeP.w - copywidth) * 4;
			end;
			if (tempbmp.bitmapFormat = MCG_FORMAT_BGRX) or (style.baseFill = EFillMode.None) then
				DrawRGBX32(@clipsi)
			else
				if style.texture.texBlendMode = EBlendMode.Hardlight then
					DrawRGBA32hardlight(@clipsi)
				else
					DrawRGBA32(@clipsi);
			// Clean up.
			setlength(tempbmp.bitmap, 0);
		end;

	begin
		// Get the texture in its original resolution. This is needed to ensure the edges are pixel-perfect.
		texobject := GetGraphicObject(style.texture.texName, 0, 0);
		if texobject = NIL then begin
			LogError('BuildBoxBase: texture graphic load fail: ' + style.texture.texName);
			style.texture.texType := ETextureType.None;
			exit;
		end;
		tempbmp := mcg_bitmap.Create;
		tempbmp.bitmapFormat := texobject.memFormat;
		tempbmp.bitDepth := 32;

		with style do with texture do with boxRect do begin
			// If all texture edges are 0, the entire texture can be stretched.
			if (texTileMarginsP_ideal.left or texTileMarginsP_ideal.right
			or texTileMarginsP_ideal.top or texTileMarginsP_ideal.bottom) = 0
			then
				_ScaleBox(0, 0, texobject.sizeXP, texobject.frameHeightP,
					texSizeP.w, texSizeP.h, marginP.left, marginP.top, paddedSizeP.w, paddedSizeP.h)
			else begin
				// Source: the texture's graphic object bitmap, using texTileMarginsP_original for w1, w3, h1, h3.
				// Target: bufp^, discounting marginP edges, using texTileMarginsP_ideal for w1, w3, h1, h3.
				// If this box's padded size is too small for the ideal texture fit, zero out w2 and/or h2, and reduce
				// the other edges proportionally to fit.
				{                                 }
				{          w1       w2      w3    }
				{       0       x1      x2        }
				{     0 +-----------------------+ }
				{ h1    | x1*y1 |       |       | }
				{    y1 |-------+-------+-------| }
				{ h2    |       | w2*h2 |       | }
				{    y2 |-------+-------+-------| }
				{ h3    |       |       | w3*h3 | }
				{       +-----------------------+ }
				{                                 }

				src_x2 := texobject.sizeXP - texTileMarginsP_original.right;
				src_w2 := src_x2 - texTileMarginsP_original.left;
				src_y2 := texobject.frameHeightP - texTileMarginsP_original.bottom;
				src_h2 := src_y2 - texTileMarginsP_original.top;

				j := texTileMarginsP_ideal.left + texTileMarginsP_ideal.right;
				if j > boxRect.paddedSizeP.w then begin
					dest_w1 := texTileMarginsP_ideal.left * boxRect.paddedSizeP.w div j;
					dest_x1 := dword(marginP.left) + dest_w1;
					dest_w2 := 0; tile_w2 := 0;
					dest_x2 := dest_x1;
					dest_w3 := boxRect.paddedSizeP.w - dest_w1;
				end
				else begin
					dest_w1 := texTileMarginsP_ideal.left;
					dest_w3 := texTileMarginsP_ideal.right;
					dest_x1 := dword(marginP.left) + dest_w1;
					dest_x2 := marginedSizeP.w - marginP.right - dest_w3;
					dest_w2 := dest_x2 - dest_x1;
					tile_w2 := texSizeP.w - dest_w1 - dest_w3;
				end;

				j := texTileMarginsP_ideal.top + texTileMarginsP_ideal.bottom;
				if j > boxRect.paddedSizeP.h then begin
					dest_h1 := texTileMarginsP_ideal.top * boxRect.paddedSizeP.h div j;
					dest_y1 := dword(marginP.top) + dest_h1;
					dest_h2 := 0; tile_h2 := 0;
					dest_y2 := dest_y1;
					dest_h3 := boxRect.paddedSizeP.h - dest_h1;
				end
				else begin
					dest_h1 := texTileMarginsP_ideal.top;
					dest_h3 := texTileMarginsP_ideal.bottom;
					dest_y1 := dword(marginP.top) + dest_h1;
					dest_y2 := marginedSizeP.h - marginP.bottom - dest_h3;
					dest_h2 := dest_y2 - dest_y1;
					tile_h2 := texSizeP.h - dest_h1 - dest_h3;
				end;

				// scalebox(src loc/size, size adjusted to viewport, dest loc/size)
				// For each box area, we lift the area from the original size, resize it from its original resolution
				// to the box's viewport, and then stretch or tile the resized area to cover the target rectangle.

				// top left
				_ScaleBox(
					0, 0, texTileMarginsP_original.left, texTileMarginsP_original.top,
					dest_w1, dest_h1,
					marginP.left, marginP.top, dest_w1, dest_h1);
				// top middle
				_ScaleBox(
					texTileMarginsP_original.left, 0, src_w2, texTileMarginsP_original.top,
					tile_w2, dest_h1,
					dest_x1, marginP.top, dest_w2, dest_h1);
				// top right
				_ScaleBox(
					src_x2, 0, texTileMarginsP_original.right, texTileMarginsP_original.top,
					dest_w3, dest_h1,
					dest_x2, marginP.top, dest_w3, dest_h1);
				// left middle
				_ScaleBox(
					0, texTileMarginsP_original.top, texTileMarginsP_original.left, src_h2,
					dest_w1, tile_h2,
					marginP.left, dest_y1, dest_w1, dest_h2);
				// center
				_ScaleBox(
					texTileMarginsP_original.left, texTileMarginsP_original.top, src_w2, src_h2,
					tile_w2, tile_h2,
					dest_x1, dest_y1, dest_w2, dest_h2);
				// right middle
				_ScaleBox(
					src_x2, texTileMarginsP_original.top, texTileMarginsP_original.right, src_h2,
					dest_w3, tile_h2,
					dest_x2, dest_y1, dest_w3, dest_h2);
				// bottom left
				_ScaleBox(
					0, src_y2, texTileMarginsP_original.left, texTileMarginsP_original.bottom,
					dest_w1, dest_h3,
					marginP.left, dest_y2, dest_w1, dest_h3);
				// bottom middle
				_ScaleBox(
					texTileMarginsP_original.left, src_y2, src_w2, texTileMarginsP_original.bottom,
					tile_w2, dest_h3,
					dest_x1, dest_y2, dest_w2, dest_h3);
				// bottom right
				_ScaleBox(
					src_x2, src_y2, texTileMarginsP_original.right, texTileMarginsP_original.bottom,
					dest_w3, dest_h3,
					dest_x2, dest_y2, dest_w3, dest_h3);
			end;
		end;
		tempbmp.Destroy; tempbmp := NIL;
	end;

	procedure _ApplyBevel;
	var l, bwidth : dword;
		writep1, writep2 : ^byte;
	begin
		with boxRect do begin
			// Use a bevel size of half of the smallest padding.
			bwidth := padding.leftp;
			if padding.rightp < bwidth then bwidth := padding.rightp;
			if padding.topp < bwidth then bwidth := padding.topp;
			if padding.bottomp < bwidth then bwidth := padding.bottomp;
			// safety
			if bwidth > paddedSizeP.w then bwidth := paddedSizeP.w;
			if bwidth > paddedSizeP.h then bwidth := paddedSizeP.h;
			bwidth := bwidth shr 1;

			// Draw the bevel.
			while bwidth <> 0 do begin
				dec(bwidth);

				// Horizontal lines... writep1 right from top left, writep2 left from bottom right.
				writep1 := bufp +
					((dword(marginP.top) + bwidth) * marginedSizeP.w + dword(marginP.left) + bwidth) shl 2;
				writep2 := bufp +
					((marginedSizeP.h - marginP.bottom - bwidth) * marginedSizeP.w - bwidth - marginP.right) shl 2;
				l := paddedSizeP.w - bwidth shl 1;
				while l <> 0 do begin
					dec(l);
					// Alpha 25% closer to $FF.
					dec(writep2); writep2^ := (writep2^ * 3 + $FF) shr 2;
					// Top edge up 25%, bottom edge down 25%.
					writep1^ := (writep1^ * 3 + $FF) shr 2; inc(writep1);
					dec(writep2); dec(writep2^, writep2^ shr 2);
					writep1^ := (writep1^ * 3 + $FF) shr 2; inc(writep1);
					dec(writep2); dec(writep2^, writep2^ shr 2);
					writep1^ := (writep1^ * 3 + $FF) shr 2; inc(writep1);
					dec(writep2); dec(writep2^, writep2^ shr 2);
					// Alpha 25% closer to $FF.
					writep1^ := (writep1^ * 3 + $FF) shr 2; inc(writep1);
				end;

				// Vertical lines... writep1 down from top right, writep2 up from bottom left.
				dec(writep1, 4);
				inc(writep2, 3);
				l := paddedSizeP.h - bwidth shl 1;
				while l <> 0 do begin
					dec(l);
					// Alpha 12.5% closer to $FF.
					writep2^ := (writep2^ * 7 + $FF) shr 3;
					// Left edge up 12.5%, right edge down 12.5%.
					dec(writep1^, writep1^ shr 2); inc(writep1);
					dec(writep2); writep2^ := (writep2^ * 7 + $FF) shr 3;
					dec(writep1^, writep1^ shr 2); inc(writep1);
					dec(writep2); writep2^ := (writep2^ * 7 + $FF) shr 3;
					dec(writep1^, writep1^ shr 2); inc(writep1);
					dec(writep2); writep2^ := (writep2^ * 7 + $FF) shr 3;
					// Alpha 12.5% closer to $FF.
					writep1^ := (writep1^ * 7 + $FF) shr 3;
					inc(writep1, (marginedSizeP.w shl 2) - 3);
					dec(writep2, (marginedSizeP.w shl 2) - 3);
				end;
			end;
		end;
	end;

	procedure _ApplyDecorations;
	var j : dword;
		clipsi : blitstruct;
		decorobject : TGraphicObject;
	begin
		for j := 0 to high(style.decorList) do with style.decorList[j] do begin
			if decorName = '' then continue;
			// Get the appropriately-sized decoration.
			decorobject := GetGraphicObject(decorName, decorSize.wp, decorSize.hp);
			if decorobject = NIL then begin
				LogError('BuildBoxBase: decor graphic load fail: ' + decorName);
				decorName := '';
				break;
			end;

			// Blit the decoration.
			with clipsi do with boxRect do begin
				srcp := decorobject.framePtr[decorFrameIndex];
				destp := bufp + (decorLocP.y * longint(boxRect.marginedSizeP.w) + decorLocP.x) * 4;
				copywidth := decorSize.wp;
				copyrows := decorSize.hp;
				srcskipwidth := 0;
				destskipwidth := (boxRect.marginedSizeP.w - copywidth) * 4;
			end;
			DrawRGBA32(@clipsi);
		end;
	end;

begin
	with buffers do begin
		// Make sure the base + final buffer has enough memory reserved.
		i := boxRect.marginedSizeP.w * boxRect.marginedSizeP.h * 8;
		if (baseP = NIL) or (i > baseAllocSize) or (i < baseAllocSize shr 3) then begin
			// Adjust buffer size in 64k chunks, with 1-2 chunks for headroom.
			baseAllocSize := (i + 131072) and $FFFF0000;
			if baseP <> NIL then freemem(baseP);
			getmem(baseP, baseAllocSize); // can't use reallocmem, not guaranteed to shrink in place
		end;
		// The final buffer immediately follows the base buffer.
		finalP := baseP + baseAllocSize shr 1;
	end;

	with boxRect do begin
		// safety
		if (marginedSizeP.w or marginedSizeP.h or paddedSizeP.w or paddedSizeP.h) = 0 then exit;

		bufp := buffers.baseP;
		{log(strcat('%: marginP=% paddedsizep=% marginedsizep=% baseallocsize=% basep=&',
			[boxName, marginP.ToString, paddedSizeP.ToString, marginedSizeP.ToString, buffers.baseallocsize, buffers.baseP]));}
		if (marginedSizeP.w <> paddedSizeP.w) or (marginedSizeP.h <> paddedSizeP.h) or (style.baseFill = EFillMode.None) then
			filldword(bufp^, marginedSizeP.w * marginedSizeP.h, 0);

		case style.baseFill of
			EFillMode.None: ;

			EFillMode.Flat:
			mcg_FillFlat(style.baseColor[0],
				bufp + (dword(marginP.top) * marginedSizeP.w + dword(marginP.left)) * 4,
				paddedSizeP.w, paddedSizeP.h, (marginedSizeP.w - paddedSizeP.w));

			EFillMode.XGradient:
				mcg_FillGradient(style.baseColor[0], style.baseColor[1], style.baseColor[2], style.baseColor[3],
					bufp + (dword(marginP.top) * marginedSizeP.w + dword(marginP.left)) * 4,
					paddedSizeP.w, paddedSizeP.h, (marginedSizeP.w - paddedSizeP.w));
		end;
	end;

	if style.texture.texType <> ETextureType.None then _ApplyTexture;
	if style.doBevel <> 0 then _ApplyBevel;
	if length(style.decorList) <> 0 then _ApplyDecorations;
	baseNeedsRender := FALSE;
	finalNeedsRender := TRUE;
end;

procedure TTextBox.RenderFinal;
// Copies the base image and scrolled content window on top of each other as the final box image. Fades text at the top
// and bottom to indicate scrollability if needed.
var clipsi : blitstruct;
	writep : pointer;
	topp, bottomp, bottomclipp : longint;
	i, blitheightp, fadeheightp, maxfadeheightp : dword;
	needscrollbar : boolean;

	procedure _DrawScrollbar;
	var barwidthp, capheightp, troughheightp, thumbheightp, thumblocyp : dword;
		capcolor, troughcolor, thumbcolor : dword;
		barimg : pointer;
	begin
		if (boxRect.padding.rightp = 0) or (contentWin.contentWinSizeP.h < 4) or (dword(style.scrollbar.thumbColor) = 0)
		then exit;

		barwidthp := (boxRect.padding.rightp + 1) shr 1;
		capheightp := contentWin.contentWinSizeP.h shr 2;
		if capheightp > barwidthp then capheightp := barwidthp;
		troughheightp := contentWin.contentWinSizeP.h - (capheightp shl 1);
		thumbheightp :=
			(troughheightp * contentWin.contentWinSizeP.h + content.fullHeightP shr 1) div content.fullHeightP;
		if thumbheightp > troughheightp then exit;

		thumblocyp := 0;
		if contentWin.scrollOfsP > 0 then thumblocyp := contentWin.scrollOfsP;
		thumblocyp := (troughheightp * thumblocyp + content.fullHeightP shr 1) div content.fullHeightP;
		if thumblocyp + thumbheightp > troughheightp then thumblocyp := troughheightp - thumbheightp;

		capcolor := mcg_PremulRGBA32(dword(style.scrollbar.capColor));
		troughcolor := mcg_PremulRGBA32(dword(style.scrollbar.troughColor));
		thumbcolor := mcg_PremulRGBA32(dword(style.scrollbar.thumbColor));

		getmem(barimg, barwidthp * contentWin.contentWinSizeP.h shl 2);
		clipsi.srcp := barimg;
		clipsi.destp := writep + (contentWin.contentWinSizeP.w + barwidthp shr 1) shl 2;
		clipsi.copywidth := barwidthp;
		clipsi.copyrows := contentWin.contentWinSizeP.h;
		clipsi.srcskipwidth := 0;
		clipsi.destskipwidth := (boxRect.marginedSizeP.w - barwidthp) shl 2;

		capheightp := capheightp * barwidthp; // -> cap rect pixels
		filldword(barimg^, capheightp, capcolor);
		writep := barimg + capheightp shl 2;

		if thumblocyp <> 0 then begin
			filldword(writep^, thumblocyp * barwidthp, troughcolor);
			inc(writep, thumblocyp * barwidthp * 4);
		end;
		dec(troughheightp, thumblocyp + thumbheightp);

		thumbheightp := thumbheightp * barwidthp; // -> thumb rect pixels
		filldword(writep^, thumbheightp, thumbcolor);
		inc(writep, thumbheightp shl 2);

		if troughheightp <> 0 then begin
			troughheightp := troughheightp * barwidthp; // -> trough rect pixels
			filldword(writep^, troughheightp, troughcolor);
			inc(writep, troughheightp shl 2);
		end;

		filldword(writep^, capheightp, capcolor);

		DrawRGBA32(@clipsi);
		freemem(barimg); barimg := NIL;
	end;

begin
	finalNeedsRender := FALSE;
	boxNeedsRedraw := TRUE;

	Assert(contentWin.contentWinSizeP.w <= boxRect.marginedSizeP.w);
	move(buffers.baseP^, buffers.finalP^, buffers.baseAllocSize shr 1);
	// Check if the box is scrolled beyond the end of the buffer, or content buffer is unavailable.
	if (contentWin.scrollOfsP >= longint(content.fullHeightP))
	or (contentWin.scrollOfsP <= -contentWin.contentWinSizeP.h)
	or (buffers.contentFullP = NIL)
	then exit;

	// Set up basic blitting parameters.
	with boxRect do
		writep := buffers.finalP +
			((dword(marginP.top) + padding.topp) * marginedSizeP.w + dword(marginP.left) + padding.leftp) shl 2;
	with clipsi do begin
		srcp := buffers.contentFullP;
		destp := writep;
		copywidth := contentWin.contentWinSizeP.w;
		srcskipwidth := 0;
		destskipwidth := (boxRect.marginedSizeP.w - copywidth) * 4;
	end;

	// Calculate where the buffer would fall on the content window.
	topp := -contentWin.scrollOfsP;
	bottomp := topp + longint(content.fullHeightP);

	// Clip the buffer copy area against the window top and bottom.
	if topp < 0 then begin
		inc(clipsi.srcp, -topp * longint(contentWin.contentWinSizeP.w * 4));
		topp := 0;
	end;
	bottomclipp := 0;
	if bottomp > longint(contentWin.contentWinSizeP.h) then begin
		bottomclipp := bottomp - contentWin.contentWinSizeP.h;
		bottomp := contentWin.contentWinSizeP.h;
	end;

	// Adjust the target offset if buffer is scrolled up beyond buffer start.
	if contentWin.scrollOfsP < 0 then inc(clipsi.destp, -contentWin.scrollOfsP * longint(boxRect.marginedSizeP.w * 4));

	blitheightp := bottomp - topp;
	if blitheightp = 0 then exit;

	maxfadeheightp := blitheightp shr 1;
	i := contentWin.lineHeightP * 3 shr 1;
	if maxfadeheightp > i then maxfadeheightp := i;
	fadeheightp := 0;
	needscrollbar := FALSE;

	if (style.freescrollable) or (Choicematic.isActive) and (Choicematic.choiceBox = self) then begin
		// Add a text fade at the top, if there's more content above.
		if contentWin.scrollOfsP > 0 then begin
			needscrollbar := TRUE;
			fadeheightp := contentWin.scrollOfsP;
			if fadeheightp > maxfadeheightp then fadeheightp := maxfadeheightp;
			dec(blitheightp, fadeheightp);
			for i := 1 to fadeheightp do begin
				clipsi.copyrows := 1;
				DrawRGBA32alpha(@clipsi, i * 255 div (fadeheightp + 1));
			end;
		end;
		// Add a fade at the bottom, if there's more content below.
		fadeheightp := 0;
		if bottomclipp <> 0 then begin
			fadeheightp := bottomclipp;
			if fadeheightp > maxfadeheightp then fadeheightp := maxfadeheightp;
			dec(blitheightp, fadeheightp);
		end;
	end;

	// Normal content blit.
	clipsi.copyrows := blitheightp;
	DrawRGBA32(@clipsi);

	// Blit the bottom fade.
	if fadeheightp <> 0 then
		for i := fadeheightp downto 1 do begin
			clipsi.copyrows := 1;
			DrawRGBA32alpha(@clipsi, i * 255 div (fadeheightp + 1));
		end;
	// Also draw a basic scrollbar.
	if (needscrollbar) or (fadeheightp <> 0) then _DrawScrollbar;
end;

// ------------------------------------------------------------------

{$include boxhub-implementation.pas}

