{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

type TElementEvent = record
	eventName : UTF8string; // deprecated
	element : TElement;
	triggerLabel, mouseOnLabel, mouseOffLabel : UTF8string;
	eventState : byte; // 1 if currently overed, 0 if not, 2 while processing
	mouseOnly : boolean;
end;
type PElementEvent = ^TElementEvent;

type TTimerEvent = record
	eventName : UTF8string;
	triggerPeriod : longint; // timers trigger every x msecs, or just once if negative
	timerCounter : dword; // accumulates every frame
	triggerLabel : UTF8string; // on trigger, this is run in a new fiber
end;

type TEventMatic = class
	// Various callbacks triggered by user action or by timer.
	// Must be separated by type for most efficient iteration.
	eventArea : array of record // deprecated
		eventName : UTF8string;
		areaInViewport : dword;
		areaLoc : TEdgeCoords; // 32k relative to viewport, pixel value relative to game window
		centerPoint : TCoordP; // precalculated area center, pixel value relative to game window
		triggerLabel, mouseOnLabel, mouseOffLabel : UTF8string;
		eventState : byte; // 1 if currently overed, 0 if not
		mouseOnly : boolean;
	end;
	elementEvent : array of TElementEvent;
	numElementEvents : dword;
	timerEvent : array of TTimerEvent;
	interruptLabel : UTF8string;
	escInterruptLabel : UTF8string;
	hasTriggeredInterrupt : boolean; // TRUE if an interrupt was triggered this frame

	// On event trigger or mouseover, a new fiber is spawned at the given label. Some parameters are pushed into the
	// fiber, depending on event type:
	// - Interrupt: nothing
	// - Esc: nothing
	// - Timer: name=<eventName>
	// - Element trigger/mouseover: name=<element name>, x,y=<element 32k loc>

	public
	procedure AddAreaEvent( // deprecated
		const name : UTF8string; inviewport : dword; locx, locy, sizex, sizey : longint;
		const _triggerlabel, _mouseonlabel, _mouseofflabel : UTF8string; _mouseonly, initialstate : boolean);
	function CreateElementEvent(const _ele : TElement) : PElementEvent;
    function FindElementEvent(const _ele : TElement) : PElementEvent;
    function FindOrCreateElementEvent(const _ele : TElement) : PElementEvent;
	procedure AddTimerEvent(
		const name : UTF8string; _triggerperiod : longint; const _triggerlabel : UTF8string; starttime : dword);

	private
    procedure PushOverableParams(const ele : TElement; const fib : TFiber);

	public
	procedure CheckOverables;
	function TriggerOverables(usingmouse : boolean) : dword;
    procedure RemoveTimer(index : dword);
	procedure AdvanceTimers(tickcount : dword);
	procedure Reset;
	procedure Remove(const name : UTF8string; element : boolean = TRUE; timer : boolean = TRUE);
	function Serialise : TStringBunch;
end;

