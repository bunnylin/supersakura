{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Null = box is not shown, will be set to Appearing when text is inserted
// Empty = box is shown, will be set to Vanishing if autovanish = TRUE
// Appearing = pop-in animation, will be set to Showtext when complete
// Vanishing = pop-out animation, will be set to Null when complete
// Showtext = box+content are shown
type EBoxState = (Null = 0, Empty = 1, Appearing = 2, Vanishing = 3, ShowText = 4);

type TTextBox = class
	// Boxes are accessed by name. Special names: null, mainbox, highlight; empty name for dropdown console.
	boxName : string31;
	boxState : EBoxState;

	boxFont : record
		// Font height is set by game scripts, 32k relative to the parent viewport's height.
		origFontHeight : dword;

		font : TFont;
	end;

	contentWin : record
		// Linespacing: 32k relative to fontheightp_requested, the minimum space guaranteed between the bottom of a row
		// of text and the top of the next row. Font outlines occupy this space. If the font comes with extra
		// linespacing, that is deducted from the linespacing pixel value capped at 0.
		linespacing : dword;

		// Line height: pixel height of the active font, plus the greater of (tallest font outlines or linespacing).
		lineHeightP : dword;

		// The content window is autofitted to display this much text, using the active font. The rows are converted to
		// a pixel height using lineheightp, and cols to a pixel width using fontwidthp (ex-width), plus outline
		// margins on the left and right.
		fitMinRows, fitMaxRows : dword;
		fitMinCols, fitMaxCols : dword;

		// Box paddedSize bounds, 32k relative to parent viewport. The autofitted content window plus padding isn't
		// allowed to exceed these limits. (Ignores margins.)
		paddedMinSize, paddedMaxSize : TSize32k;

		// Actual content window pixel size bounds, derived from the above. The stricter limits apply from each;
		// in case of conflict, the padded bounds take precedence. Finally, the min sizes are forced to less than max.
		// When text content in the box is flown, soft linebreaks are added to bound the content width, and vertical
		// overflow can be displayed by scrolling the box content.
		contentMinSizeP, contentMaxSizeP : TSizeP;

		// The allowed text width is max content width less outline margins.
		flowMaxWidthP : dword;

		// Vertical offset of the content window from the top of the buffer, 32k and pixels. The window can scroll past
		// the start and end of content. Ofs 0 is start of content, each 32k is lineheightp.
		scrollOfs, scrollOfsP : longint;

		// Content window pixel size after text is flown and linewrapped, with size bounds enforced. This is not the
		// actual content, which is kept in buffers.contentFullP; rather, this is the box graphic around the text
		// itself, suitably sized that the text fits in neatly (with vertical scrolling).
		contentWinSizeP : TSizeP;

		// The content window is split into columnCount columns, with some amount of padding between them. Text output
		// ignores columns except on a \t column break, which moves output ahead to the next column's start, mostly
		// used by choicematic. A column break at the last column becomes a soft linebreak.
		//
		// Count must be 1 or more. Columns can use a fixed or automatic width, depending on autoSizeColumns. If auto,
		// content width bounds are ignored but the columns will use as little width as possible given their content.
		// If fixed, the box's maximum size bound is divided by the column count to get the column width:
		//   columnFixedWidthP = (fitMaxSizeP.w - columnPaddingP * (columnCount - 1)) / columnCount
		//
		// To look nice, some padding is allowed between columns. A positive columnPadding is 32k relative to
		// contentWinMaxSizeP.w; a negative padding is relative to the font's exWidthP.
		columnPadding : longint;
		columnCount, columnFixedWidthP, columnPaddingP : dword;
	end;

	// Position, clip, and size the box within this viewport.
	boxInViewport : dword;

	boxRect : record
		// Much like CSS, the actual textbox has padding around the content window. 32k value is relative to viewport.
		padding : TEdgeSizes;
		// Additionally, an invisible margin around the box contains box decor and shadow. Calculated automatically.
		marginP : TEdgeCoordP;

		paddedSizeP, marginedSizeP, renderSizeP : TSizeP;

		// 32k coordinates of the anchor point relative to box's parent viewport.
		boxLoc : TCoord32k;

		// 32k coordinates of an anchor point within the padded box (ignore margins). 0,0 aligns the box by its top
		// left corner, 32k,32k by its bottom right. A perfectly centered box of any size would have a 16k,16k anchor,
		// at viewport coordinates 16k,16k. Values outside 0..32k are allowed.
		boxAnchor : TCoord32k;

		// Pixel coordinates of the box's top left corner relative to the game window. This is after applying anchor,
		// box snapping, and the alwaysInViewport flag.
		paddedLocP, marginedLocP, renderLocP : TCoordP;
	end;

	// Location code flow:
	// - boxLoc and boxAnchor 32k start with defaults, then set by script in sakufibercmds tbox_setloc
	// - sakufibercmds creates a move effect, which calls SetLocation
	// - BoxHub calls SetLocation to manage the dropdown console
	// - SetLocation sets refreshLocP
	// - FlowContent sets refreshLocP after changing box size
	// - TextBoxer calls DeriveLocP if refreshLocP set, sets renderLocP each frame
	// - DeriveLocP calculates paddedLocP with anchor/snap/alwaysinviewport, marginedLocP

	// Size code flow:
	// - Various contentwin non-pixel size bounds have defaults and are set by script
	// - Bounds are also set by size effect, BoxHub to manage dropdown console
	// - SetSize calls DeriveSizeP
	// - UpdateParameters calculates font*, lineHeightP, textTileMarginsP, texSizeP, decorSizeP, outlineThicknessP;
	//   calls DeriveSizeP, sets contentNeedsRender
	// - DeriveSizeP calculates column*P, contentMixMaxSizeP
	// - FlowContent sets contentwin pixel size, calculates paddedSizep, calls DeriveMargins
	// - DeriveMargins calculates DecorLocP, marginP, marginedSizeP
	// - TextBoxer sets renderSizeP each frame

	// Scrolling: call ScrollTo, which then calculates the pixel value, sets finalNeedsRender

	// Boxes are sorted by Z-level relative to each other: lowest Z sorts as box index 0.
	boxZLevel : longint;

	// If snap-to name is not empty, this box's anchor location is snapped pixel-perfectly to the given box, at
	// snapToAnchor relative to the target box. This overrides this box's boxLoc.xy. A box can't snap to itself, and no
	// box can snap to the null box. The snap edges are specifically the padded box borders, ignoring all margins.
	snapToBoxName : string31;
	snapToBox : TTextBox;
	snapToAnchor : TCoord32k;

	// If the box state is appearing/vanishing, popruntime counts msecs toward style.poptime. When it is reached, the
	// box state changes.
	popRunTimeMsec : longint;

	buffers : record
		// Each box maintains four buffers: base graphic, full text content, scratch buffer, and the final image.
		// For efficiency, the base + final buffers are allocated in one block, likewise the scratch + content buffers.
		baseP, finalP : pointer;
		scratchP, contentFullP : pointer;
		baseAllocSize, contentAllocSize: dword;

		// Scratch buffer: for rendering individual rows. Row parts are rendered, then appended here. Once the row is
		// full, the scratch buffer is used to draw font outlines in the full content buffer, and finally to copy the
		// text itself there over the outlines.
		//   scratchBytes = contentWinSizeP.w * fontHeightP * bytes per pixel

		// Full content buffer: contains a render of all text and graphics in the box. This can be taller than the box
		// size, in which case only a scrollable part of the full content is visible.
		//   contentFullBytes = contentWinSizeP.w * content.fullHeightP * bytes per pixel
		//   contentAllocSize = scratchBytes + contentFullBytes

		// Base graphic: some kind of tiled/stretched graphic and frame decor. The content is drawn over this.
		// Final image: base image + content window. Same size as the base graphic.
		//   marginedSizeP.w * marginedSizeP.h * bytes per pixel
	end;

	{$note todo: smarter contentneedsrender} // indicate a number of rows to keep, save effort on reflow and render

	parametersNeedUpdate : boolean; // recalculates and re-renders the whole box; implies base and contentNeedsRender
	contentNeedsRender : boolean; // resize, reflow, render content; implies finalNeedsRender, maybe baseNeedsRender
	baseNeedsRender : boolean; // make a new base graphic; implies finalNeedsRender
	finalNeedsRender : boolean; // base + content -> final buffer; implies boxNeedsRedraw
	boxNeedsRedraw : boolean; // copy the final buffer to output without changes
	refreshLocP : boolean; // call DeriveLocP before doing anything with locP values
	isHidden : boolean; // the user can hide visible textboxes to see the full game window

	content : record
		// Simple UTF-8 text directly from print-commands, with escape codes separated. (Variable references get
		// expanded immediately.)
		txtContent : array of byte;
		txtLength : dword; // in bytes
		userInputLen : dword; // user input portion, bytes from end of content
		caretPos : longint; // -1 for disabled, up to userinputlen

		// List of escape codes and their offsets relative to txtContent[]. Codes:
		//   \0 = empty character
		//   \: = dialogue title terminator
		//   \n = explicit linebreak
		//   \t = column break
		//   \? = begin choice string
		//   \. = end choice string
		//   \- = horizontal divider line
		//   \B \b = enable\disable bold font
		//   \$xxx; = variable reference, not saved, immediately dereferenced
		//   \&xxx; = show emoji number xxx
		//   \cRGBA; = set primary text color temporarily to RGBA, one hex each
		//   \d = restore the default text color, immediately converted to \c with the current normal box text color
		//   \L \C \R = set text alignment temporarily to left/center/right
		//   \@xxx; = hack: place next escape code at this exact offset
		//   \#x=y; = hack: choiceList[x].index := y
		escapeList : array of record
			escapeOfs : dword;
			escapeCode : byte;
			escapeData : dword;
		end;
		escapeCount : dword;

		// Every \? escape printed in this box adds a new choice item here.
		choiceList : array of record
			index : dword; // 0 for ad-hoc; otherwise valid choicematic.choiceNodes[] index
			// Top left and bottom right (excl) pixel coords of this string, relative to the box's full content buffer.
			// These don't include any margins or padding. These are filled in by the box content renderer as it
			// encounters choice tags.
			showLocP : TEdgeCoordP;
		end;
		choiceCount : dword;

		// After the text content is broken into rows, the row count is kept here. The full content pixel height is
		// (row count * lineheightp). This gives a vertically scrollable content buffer.
		fullRowCount, fullHeightP : dword;

		// Anything printed in this box is also printed in the export target box, in that box's preferred language.
		// This allows printing game text simultaneously in Japanese in one box and English in another.
		// Empty name for disabled. Exporting can be chained. No infinite loop protection...
		exportToBoxName : string31;
		exportToBox : TTextBox;
	end;

	// Variables controlling box appearance and functionality.
	style : record
		// All string manipulation is done in all available languages at the same time. When a string is printed in
		// this box, this language index is the one displayed. Use FindLanguageIndex to match a string to index.
		boxLanguage : dword;

		// Default primary text color. Each tbox.clear reverts to this.
		textColor : RGBAquad;
		// The base image starts out filled with this color, depending on base type. The base color should always be
		// defined, as it's a fallback if something goes wrong with a textured base.
		baseColor : array[0..3] of RGBAquad;
		baseFill : EFillMode;
		boxBlendMode : EBlendMode;

		// Boxes can appear/vanish with a visual effect, applied when the final buffer is blitted into the game window.
		popType : EPopType;
		// Pop time, how long the appearing/vanishing takes.
		popTimeMsec : dword;

		texture : record
			texName : UTF8string;
			texType : ETextureType;
			texBlendMode : EBlendMode;
			// If you want, you can use a multi-frame graphic as a texture.
			texFrameIndex : dword;
			// Pixel values marking the boundaries of the texture graphic. These delineate a hash-shape, where the
			// corners get copied directly, the top and bottom are stretched horizontally, the left and right are
			// stretched vertically, and the middle is stretched both ways.
			// These are margin pixel widths, relative to the original graphic size, and are defined by script.
			texTileMarginsP_original : TEdgeSizeP;
			// Rescaled margin pixel widths, derived from the above, relative to the box's viewport. Actual may differ
			// from ideal if the box is too small to contain the correctly-sized texture; it must be squashed.
			texTileMarginsP_ideal : TEdgeSizeP;
			// The texture's rescaled pixel size, relative to the box's viewport.
			texSizeP : TSizeP;
		end;

		// 0 = no bevel, 1 = apply bevel of half of smallest edge padding
		doBevel : byte;

		// After the base and texture and bevel, decoration graphics can be pasted on the base image.
		decorList : array of record
			decorName : UTF8string;
			decorFrameIndex : dword;
			// Decoration positions are relative to the padded box. Coordinates outside the padded area are allowed.
			decorAnchor : TCoord32k;
			decorLoc : TCoord32k;
			// Pixel position relative to the margined box size.
			decorLocP : TCoordP;
			// By default, the graphic is sized from its original resolution to the box's viewport size.
			// If either decorsize is not 0, then that axis is resized to said 32k fraction of viewport size.
			decorSize : TSizes;
		end;

		// After each text row has been rendered, while the row is being copied into the full content buffer, text
		// outlines are drawn first in reverse order, before the text proper is placed in. This can be used for drop
		// shadows too.
		outline : array of record
			outlineColor : RGBAquad;
			// The thickness and offset are relative to font height.
			thickness, thicknessP : dword;
			offset : TCoords;
			// If TRUE, the outline color fades toward transparent alpha as a function of distance over thickness.
			alphafade : boolean;
		end;
		// When the outline array is modified, new outline margin pixel sizes must be calculated, which causes
		// lineheightp to adapt, and forces a re-linebreaking. These come from the furthest pixel distance any outline
		// reaches, counting its offset in that direction plus that outline's pixel thickness.
		outlineMarginP : TEdgeSizeP;

		// Freely-scrollable textboxes and choice boxes with more content than fits in the box will have a text fade
		// effect and show a scrollbar, to indicate scrollability. The scrollbar is drawn in the center of the right
		// padding area, using a width of half of the right padding. Its colors can be set here. Set all to 0 to leave
		// it invisible.
		scrollbar : record
			capColor, troughColor, thumbColor : RGBAquad;
		end;

		// Default text alignment. The alignment can be temporarily changed by escape codes during any row.
		//   0 = left, 16384 = center, 32768 = right
		textAlign : longint;

		// If the text content takes more space than the box can fit, and this is TRUE, the user can scroll the text
		// freely by pressing up/down or pgup/pgdn/home/end, or using the mouse wheel. If the box contains choices, it
		// always counts as scrollable.
		freeScrollable : boolean;
		// If a box with overflown content is not freely scrollable, and autowaitkey is TRUE, the box will page down
		// upon a keypress. When the box is displaying the bottom of the content, it no longer eats keypresses, which
		// pass to waitkey normally.
		autoWaitkey : boolean;
		// With autovanish, when a box is cleared, it goes in Vanishing state. Otherwise the empty box sticks around.
		autoVanish : boolean;
		underlineAllRows : boolean;
		// If set, the textbox is shifted and resized so its padded area is always within its viewport.
		alwaysInViewport : boolean;
		// If set, textbox columns are kept as narrow as possible, but the box's maximum width becomes unbounded.
		autoSizeColumns : boolean;
	end;

	constructor Create(const name : string31);
	destructor Destroy; override;

	procedure Reset;
	procedure AddRefresh; inline;
	procedure Clear(deleteuserinput : boolean = TRUE);
	procedure CloneFrom(srcbox : TTextBox; clonecontent : boolean);
	function ScrollTo(
		_ofs : longint; msecs : longint = -1; movetype : EMoveType = EMoveType.Halfcos; capToContent : boolean = TRUE;
		owningfiber : TFiber = NIL) : longint;
	procedure Hide(makehidden : boolean);
	procedure Snap;
	function FlowContent : boolean;
	procedure PrintFast(srcp : pointer; srclen : dword);
	procedure Print(const newtxt : UTF8string);
	procedure PrintNested(const txt : TStringBunch);
	procedure PrintNested(const txt : UTF8string);
	procedure RemoveDecoration(decornamu : UTF8string);
	procedure SetLocation(x, y, anchorx, anchory : longint);
	procedure SetSize(w, h : dword);
	function GetBoxParam(const bpname : UTF8string; out strvalue : UTF8string) : longint;
	procedure SetBoxParam(const bpname : UTF8string; const bpstr : UTF8string; bpval : longint; usedefault : boolean);
	procedure MoveCaret(numchars : dword; backward : boolean; ctrl : boolean);
	procedure StartTextInput(initialtxt : UTF8string; initialpos : longint);
	procedure StopTextInput;
	procedure InsertTextInput(const newtxt : pchar);
	procedure DeleteTextInput(numchars : dword; backspace : boolean);
	procedure DeriveLocP;
	procedure DeriveSizeP;
	procedure DeriveMargins;
	procedure UpdateParameters;
	function GetUTF8Size(poku : pointer; slen : dword) : dword; // since this depends on font size, must be in TTextBox
	function GetUTF8LongestFit(startp, endp : pointer; segmentwidthp, maxwidthp : longint) : dword;
	function ToString : UTF8string; override;
private
	procedure RenderContent;
	procedure RenderBase;
	procedure RenderFinal;
	{$ifdef sakucon}
	procedure BlitzBox;
	function RefreshOverlappingBoxes : boolean;
	{$endif}
end;

type TBoxHub = class
	var textbox : array of TTextBox;
	allBoxesHidden : boolean;
	delayedClear : boolean; // when set, the next print/transition clears the main text box and dialogue title box
	defaultTextbox : TTextBox; // print commands default to this, default "mainbox"
	dialogueTitleBox : TTextBox; // print dialogue titles in this, default "titlebox"
	activeTextInput : byte; // 0 = no; +1 for each box accepting text input
	lastGetBox : dword; // caches most recent GetBox search

	function GetBoxIndex(const name : string31) : dword;
	function GetBox(const name : string31) : TTextBox;
	procedure InsertBoxInList(const newbox : TTextBox);
	procedure RemoveBoxFromList(const removable : TTextBox);
	procedure DropdownSlideDown;
	procedure DropdownSlideUp;
	procedure HideAllBoxes(dohide : boolean);
	function CheckPageableBoxes : boolean;
	function CompleteTextInput : boolean;
	function GetBoxAt(xp, yp : longint) : dword;
	function ScrollUsingStick(stickvalue : longint; tickcount : dword) : boolean;
	procedure PrintDebugBuffer;
	procedure PrintTranscriptBuffer;
	procedure TextBoxer(tickcount : dword);
	procedure InheritState(const src : TBoxHub);
	procedure ResetHub;
	function Serialise : TStringBunch;
	destructor Destroy; override;
end;

