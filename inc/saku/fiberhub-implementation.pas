{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function GetCommandHelp(nam : UTF8string) : UTF8string;
// Returns a friendly descriptive string partially matching the given command(s). The string uses $A linebreaks.
// When called by a "help x" pseudo-command, this allows making interactive scripting more accessible.

	function _GetParamLine(cmdindex : byte) : string;
	var i, j, c : byte;
	begin
		c := ss_cmdlist[cmdindex].code;
		result := ss_cmdlist[cmdindex].id;
		for i := 0 to high(ss_cmdparams[c]) do
			if ss_cmdparams[c][i] <> ARG_INVALID then
				for j := high(ss_paramlist) downto 0 do
					if ss_paramlist[j].code = i then begin
						result := result + ' [' + ss_paramlist[j].id;
						case ss_paramtype[ss_paramlist[j].code] of
							ARG_NUM: result := result + ' #]';
							ARG_STR: result := result + ' ""]';
							else result := result + ' ??]';
						end;
					end;
	end;

var seencmdlist : array[0..255] of boolean;
	thisgroup, lastgroup : string;
	i, j, lastcode : byte;
begin
	seencmdlist[0] := FALSE; // silence the compiler
	nam := trim(nam);
	if nam = '' then begin
		fillbyte(seencmdlist[0], sizeof(seencmdlist), 0);
		result := '';
		lastgroup := '';
		for i := 1 to high(ss_cmdlist) do begin
			j := pos('.', ss_cmdlist[i].id);
			if j <> 0 then begin
				seencmdlist[ss_cmdlist[i].code] := TRUE;
				thisgroup := copy(ss_cmdlist[i].id, 1, j - 1);
				if thisgroup <> lastgroup then begin
					lastgroup := thisgroup;
					result := result + thisgroup + ', ';
				end;
			end;
		end;
		setlength(result, result.Length - 2);
		result := 'To see available commands, try help <partial command> or help <subgroup>.' +
			#$A'Available subgroups:'#$A + result +
			#$A'Ungrouped commands:'#$A;
		for i := high(ss_cmdlist) downto 0 do
			if NOT seencmdlist[ss_cmdlist[i].code] then begin
				seencmdlist[ss_cmdlist[i].code] := TRUE;
				result := result + ss_cmdlist[i].id + ', ';
			end;
		setlength(result, result.Length - 1);
		result[result.Length] := #$A;
		exit;
	end;

	result := '';
	lastcode := 0;
	nam := lowercase(nam);
	j := 0;
	for i := 1 to high(ss_cmdlist) do
		if (length(ss_cmdlist[i].id) >= length(nam)) and (nam = copy(ss_cmdlist[i].id, 1, length(nam))) then begin
			lastcode := ss_cmdlist[i].code;
			result := result + _GetParamLine(i) + #$A;
			inc(j);
		end;

	if j = 0 then
		result := 'No command matches "' + nam + '"'#$A
	else
		if j = 1 then
			for i := 1 to high(ss_cmdlist) do
				if (ss_cmdlist[i].code = lastcode) and (ss_cmdlist[i].id <> nam) then
					result := result + '(alias "' + ss_cmdlist[i].id + '")'#$A;
end;

function TFiberHub.GetFiber(const name : UTF8string) : dword;
// Returns the lowest existing fiber with a matching name, or fiberCount if no match found.
// The comparison is case-sensitive, so uppercase the name before calling.
begin
	result := 0;
	while result < fiberCount do begin
		if fiber[result].fiberName = name then exit;
		inc(result);
	end;
end;

function TFiberHub.ClearWaitKeys : boolean;
// Resumes fibers waiting for a keypress. Returns TRUE if any fiber was resumed, otherwise FALSE.
var i : dword;
begin
	result := FALSE;
	if fiberCount = 0 then exit; // no fibers exist
	for i := fiberCount - 1 downto 0 do
		if fiber[i].fiberState in [EFiberState.WaitKey, EFiberState.WaitClear] then begin
			fiber[i].fiberState := EFiberState.Normal;
			result := TRUE;
		end;
end;

procedure TFiberHub.SignalFibers(_fibername : UTF8string; const states : TFiberStates);
// Finds all fibers with the given name (case-insensitive), or all fibers if the name is empty, and puts them in
// a normal state if they were waiting for a signal. Clears every wait state except WAITCHOICE.
var i : dword;
begin
	if fiberCount = 0 then exit; // no fibers exist
	_fibername := upcase(_fibername);
	for i := fiberCount - 1 downto 0 do
		if ((states = [])
		and (fiber[i].fiberState in
			[EFiberState.WaitKey, EFiberState.WaitClear, EFiberState.WaitSignal,
			EFiberState.WaitSleep, EFiberState.WaitFx, EFiberState.WaitTyping]))
		or (fiber[i].fiberState in states) then
			if (_fibername = '') or (fiber[i].fiberName = _fibername) then
				fiber[i].fiberState := EFiberState.Normal;
end;

procedure TFiberHub.StopFibers(_fibername : UTF8string);
// Stops all fibers with the given name (case-insensitive), or all fibers if the name is empty.
var i : dword;
begin
	if fiberCount = 0 then exit; // no fibers exist
	_fibername := upcase(_fibername);
	for i := fiberCount - 1 downto 0 do
		if (_fibername = '') or (fiber[i].fiberName = _fibername) then fiber[i].fiberState := EFiberState.Stopping;
end;

procedure TFiberHub.Reset;
var i : dword;
begin
	exitOnAnyError := FALSE;
	if fiberCount = 0 then exit;
	for i := fiberCount - 1 downto 0 do fiber[i].Destroy;
	setlength(fiber, 0);
	fiberCount := 0;
end;

procedure TFiberHub.RunDebugCommand;
// Extracts the last line from the dropdown debug console, compiles and runs it in a new fiber.
var srcp, resultp : pointer;
	logline : UTF8string = ''; // assign to shut up compiler
	i : dword;
begin
	// Stop any previous debug command fiber, which could be executing a loop or sleep etc.
	StopFibers(#0);

	with BoxHub.textbox[high(BoxHub.textbox)] do with content do begin
		// Get the command line.
		srcp := @txtContent[txtLength - userInputLen];
		// Log the line and remove it from the textbox.
		setlength(logline, userInputLen + 3);
		logline[1] := '>';
		logline[2] := ' ';
		move(srcp^, logline[3], userInputLen);
		logline[userInputLen + 3] := ' ';
		log(logline);
		logline[userInputLen + 3] := #$A; // implicit newline at end

		dec(txtLength, userInputLen);
		userInputLen := 0;
		caretPos := 0;

		if (length(logline) >= 7) and (lowercase(copy(logline, 3, 4)) = 'help') then
			if (length(logline) = 7) or (logline[7] = ' ') then begin
				logline := GetCommandHelp(copy(logline, 7, length(logline)));
				repeat
					i := pos(#$A, logline);
					if i = 0 then exit;
					log(copy(logline, 1, i - 1));
					logline := copy(logline, i + 1, length(logline));
				until FALSE;
			end;

		srcp := @logline[3];
		// Compile the line.
		resultp := CompileScript('', srcp, srcp + length(logline) - 2);
		srcp := NIL;
		if (escapeCount <> 0) and (escapeList[escapeCount - 1].escapeCode = 1) then
			escapeList[escapeCount - 1].escapeOfs := txtLength;
	end;

	if resultp <> NIL then begin
		// Script compile failed. Print error messages in log.
		srcp := resultp;
		while byte(srcp^) <> 0 do begin
			log(string(srcp^));
			inc(srcp, byte(srcp^) + 1);
		end;
		freemem(resultp); resultp := NIL; srcp := NIL;
	end
	else TFiber.Create('', #0);
end;

procedure TFiberHub.RunFibers;
// Forwards all active fibers to execute code, cleans up stopped fibers.
begin
	executingId := 0;
	exitRunFibersNow := FALSE;
	while executingId < fiberCount do with fiber[executingId] do begin

		if fiberState = EFiberState.Normal then begin
			Run(sysvar.pauseState = EPauseState.Single);
			if exitRunFibersNow then exit; // metastate changed etc, these fibers no longer active
		end;

		if fiberState = EFiberState.Stopping then
			Destroy
		else
			inc(executingId);
	end;
end;

procedure TFiberHub.UpdateScriptIndexes;
// Re-gets script objects used by all active fibers. Call this if the script array changed, eg. a new script was added.
var i : dword;
begin
	if fiberCount <> 0 then for i := fiberCount - 1 downto 0 do with fiber[i] do begin
		labelObject := scriptObjects[GetScript(labelName)];
		if labelObject = scriptObjects[0] then fiberState := EFiberState.Stopping;
	end;
end;

function TFiberHub.Serialise : TStringBunch;
// Produces a series of semicolon-terminated char-value pairs describing the current fiberhub state.
var i, j, l, d : dword;
	writep : pointer;
	s : string[8];
begin
	result := NIL;
	// Backmost stack item is always empty, never saved, so that's length - 1. But there's an extra header line before
	// each callstack, restoring expected storage space at length + 0 again.
	setlength(result, fiberCount * (CALLSTACK_HIGH + 4));
	j := 0;

	if fiberCount <> 0 then for i := 0 to fiberCount - 1 do with fiber[i] do begin
		result[j] := strcat('n%;l%;o%;s%;', [fiberName, labelName, codeOfs, fiberState]);
		inc(j);

		l := callstackIndex;
		repeat
			l := (l + 1) and CALLSTACK_HIGH;
			if callstack[l].labelName <> '' then begin
				//result[j] := strcat('c%:%;', [callstack[l].ofs, callstack[l].labelName]);
				result[j] := strcat('C%:%;', [strhex(callstack[l].ofs, 1), callstack[l].labelName]);
				inc(j);
			end;
		until l = callstackIndex;

		if dataCount <> 0 then begin
			result[j] := '';
			setlength(result[j], dataCount * 9 + 1);
			writep := @result[j][1];
			d := (dataIndex + dword(length(dataStack)) - dataCount);

			for l := dataCount - 1 downto 0 do begin
				s := strhex(dataStack[d and high(dataStack)], 1);
				inc(d);
				char(writep^) := ':'; inc(writep);
				move(s[1], writep^, length(s));
				inc(writep, length(s));
			end;

			result[j][1] := 'd';
			char(writep^) := ';'; inc(writep);
			setlength(result[j], writep - @result[j][1]); // trim to exact size
			inc(j);
		end;

		if localCount <> 0 then begin
			result[j] := varEndian + strhex(localCount, 1) + ';';
			inc(j);
			result[j] := '';
			setlength(result[j], (localCount shl 2) + 1);
			move(locals[0], result[j][1], localCount shl 2);
			result[j][result[j].Length] := ';';
			inc(j);
		end;
	end;
	setlength(result, j);
end;

procedure TFiberHub.Deserialise(buf, bufend : pointer);
// Loads a saved fiberhub state from the given buffer. It is the caller's responsibility to free the buffer afterward.
// Throws exception in case of any errors.
var keyp : ^char;
	newname, newlabel : UTF8string;
	f : TFiber = NIL;
	flip : boolean;
begin
	if buf >= bufend then begin
		LogError('empty fiber state');
		exit;
	end;
	if char((bufend - 1)^) <> ';' then raise Exception.Create('fiber state lacks final semicolon');

	newname := '';
	while buf + 2 < bufend do begin
		keyp := buf;
		inc(buf, IndexChar(buf^, bufend - buf, ';'));
		byte(buf^) := 0;
		inc(buf);

		case keyp^ of
			'n': newname := PChar(keyp + 1);
			'l': begin
				newlabel := PChar(keyp + 1);
				f := TFiber.Create(newlabel, newname);
				newname := '';
			end;
			'o':
			if f = NIL then
				raise Exception.Create('ofs without label in fiber state')
			else
				f.codeOfs := valx(PChar(keyp + 1));
			's':
			if f = NIL then
				raise Exception.Create('state without label in fiber state')
			else
				f.fiberState := EFiberState(valx(PChar(keyp + 1)));

			'c','C': // c is deprecated
			if f = NIL then
				raise Exception.Create('call without label in fiber state')
			else with f do begin
				if keyp^ = 'C' then begin
					inc(keyp);
					callstack[callstackIndex].ofs := CutHex(keyp);
				end
				else begin // deprecated
					inc(keyp);
					callstack[callstackIndex].ofs := valx(PChar(keyp));
					while keyp^ <> ':' do inc(keyp);
				end;
				inc(keyp);
				callstack[callstackIndex].labelName := PChar(keyp);
				callstackIndex := (callstackIndex + 1) and CALLSTACK_HIGH;
				callstack[callstackIndex].labelName := '';
			end;

			'd':
			if f = NIL then
				raise Exception.Create('data without label in fiber state')
			else with f do begin
				while keyp^ <> #0 do begin
					inc(keyp);
					dataStack[dataIndex] := CutHex(keyp);
					dataIndex := (dataIndex + 1) and high(dataStack);
				end;
				dataCount := dataIndex;
			end;

			'v','V':
			if f = NIL then
				raise Exception.Create('localvar without label in fiber state')
			else with f do begin
				flip := varendian <> keyp^;
				inc(keyp);
				localCount := CutHex(keyp);
				locals := NIL;
				if localCount <> 0 then begin
					setlength(locals, localCount);
					if (localCount shl 2) + 1 > dword(bufend - buf) then
						raise Exception.Create('localvars out of bounds in fiber state');
					if NOT flip then begin
						move(buf^, locals[0], localCount shl 2);
						inc(buf, localCount shl 2);
					end
					else begin
						keyp := buf + localCount shl 2;
						localCount := 0;
						while buf < keyp do begin
							locals[localCount] := SwapEndian(dword(buf^));
							inc(localCount);
							inc(buf, 4);
						end;
					end;
					if char(buf^) <> ';' then raise Exception.Create('localvars didn''t end in ;');
					inc(buf);
				end;
			end;
		end;
	end;
end;

procedure TFiberHub.LogCallstack(const _fibername : UTF8string);
var i, j : dword;
begin
	if fiberCount <> 0 then for i := fiberCount - 1 downto 0 do
		if fiber[i].fiberName = _fibername then with fiber[i] do begin
			log('# ' + fiberName + ' callstack:');
			log(strcat('# %:%', [labelName, longint(codeOfs)]));
			j := callstackIndex;
			while TRUE do begin
				j := (j - 1) and CALLSTACK_HIGH;
				if callstack[j].labelName = '' then exit;
				log(strcat('# %:%', [callstack[j].labelName, longint(callstack[j].ofs)]));
			end;
		end;
end;

destructor TFiberHub.Destroy;
begin
	Reset;
	inherited;
end;

