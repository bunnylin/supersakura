{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Supersakura common rendering functions

procedure TRendermatic.Reset;
begin
	if length(refresh) < 32 then setlength(refresh, 0);
	setlength(refresh, 32); numFresh := 0;
	AddRefresh(0, 0, sysvar.windowSize.w, sysvar.windowSize.h, 0);
end;

procedure TRendermatic.ClipRGB(clipdata : pblitstruct);
// This function takes graphic dimensions and coordinates, and returns a blitstruct with offsets used by DrawRGB24,
// DrawRGBA32 and StoreRGB.
// The destination is always mv_OutputBuffy, or any other 32-bit buffer of equal size. The source bitmap must be
// a contiguous bytestream; pre-clipped source bitmaps won't work.
// Clipdata must point to a pre-filled blitstruct that ClipRGB can edit.
//
// The following parts of blitstruct must be initialised before calling:
// - srcp and destp must point to the first pixel of both bitmaps.
// - srcofs is the pixel offset to the first pixel of the source bitmap. Use to select the source frame.
// - destofsxy are pixel values relative to mv_OutputBuffy.
// - srccopywidth and srcrows must be the source bitmap's pixel size.
// - clipxyp must be the pixel boundaries to clip against.
// - clipviewport must be set.
var i : longint;
begin
	with clipdata^ do begin
		// First clip the clipping area against the program window.
		if clipx1p < 0 then clipx1p := 0;
		if clipy1p < 0 then clipy1p := 0;
		if clipx2p > longint(sysvar.windowSize.w) then clipx2p := sysvar.windowSize.w;
		if clipy2p > longint(sysvar.windowSize.h) then clipy2p := sysvar.windowSize.h;
		// And against the element's viewport.
		with Viewportmatic.viewport[clipviewport] do begin
			if clipx1p < viewportLoc.leftp then clipx1p := viewportLoc.leftp;
			if clipy1p < viewportLoc.topp then clipy1p := viewportLoc.topp;
			if clipx2p > viewportLoc.rightp then clipx2p := viewportLoc.rightp;
			if clipy2p > viewportLoc.bottomp then clipy2p := viewportLoc.bottomp;
		end;

		srcskipwidth := copywidth;
		// clip top
		i := clipy1p - destofsyp;
		if i > 0 then begin
			// i = rows to clip
			if dword(i) > copyrows then i := copyrows;
			dec(copyrows, i);
			inc(srcofs, copywidth * dword(i));
			destofsyp := clipy1p;
		end;
		// clip bottom
		i := destofsyp + longint(copyrows) - clipy2p;
		if i > 0 then begin
			// i = rows to clip
			if dword(i) > copyrows then i := copyrows;
			dec(copyrows, i);
		end;
		// clip left
		i := clipx1p - destofsxp;
		if i > 0 then begin
			// i = cols to clip
			if dword(i) > copywidth then i := copywidth;
			dec(copywidth, i);
			inc(srcofs, dword(i));
			destofsxp := clipx1p;
		end;
		// clip right
		i := destofsxp + longint(copywidth) - clipx2p;
		if i > 0 then begin
			// i = cols to clip
			if dword(i) > copywidth then i := copywidth;
			dec(copywidth, i);
		end;
		// For each row, skip all columns that are not copied. Also convert the values to bytes instead of pixels.
		// (destofsxyp, srccopywidth, and srcrows remain pixel values)
		srcofs := srcofs * 4;
		srcskipwidth := (srcskipwidth - copywidth) * 4;
		destofs := (destofsyp * longint(sysvar.windowSize.w) + destofsxp) * 4;
		destskipwidth := (sysvar.windowSize.w - copywidth) * 4;
		inc(srcp, srcofs);
		inc(destp, destofs);
	end;
end;

procedure TRendermatic.DarkenOutput;
// Change all pixels in the output buffer to be darker and less saturated.
var p, pend : ^RGBAquad;
	grey : byte;
begin
	p := outputBuffy;
	pend := p + sysvar.windowSize.w * sysvar.windowSize.h;
	while p < pend do begin
		grey := (p^.b + p^.g * 4 + p^.r * 2) shr 3;
		p^.b := (p^.b + grey) shr 1;
		p^.g := (p^.g + grey) shr 1;
		p^.r := (p^.r + grey) shr 1;
		inc(p);
	end;
	{$ifdef sakucon}
	BlitzAscii(0, 0, sysvar.windowSize.w, sysvar.windowSize.h);
	{$else}
	SDL_UpdateTexture(SDL.mainTextureH, NIL, outputBuffy, sysvar.windowSize.w * 4);
	Rendermatic.keyframe := 0;
	{$endif}
end;

{$ifdef bonk}
procedure TRendermatic.BuildRGBtweakTable(r1, g1, b1 : longint);
// Fills RGBtweaktable[] with conversion values.
// The table can be used to apply changes to a pixel's colors efficiently. First call this procedure to create the
// lookup table, where each color channel gets an individual adjustment curve from your input arguments.
// The input values may be -256..+256. At 0, the conversion curve will be a straight line. Negative numbers bend the
// curve toward zero, and at -256 it's all flat zero. Positive numbers bend the curve upwards.
//
// The curve is calculated using three points; pStart, pMid, and pEnd.
// At zero state, pStart is 0, pMid is 128 and pEnd is 255.
//
// The curve is a standard ax^2 + bx + c = 0, where
//   f(0) = pStart, f(128) = pMid and f(255) = pEnd.
// This solves to:
//   a = (   127 * pStart -   255 * pMid +   128 * pEnd) / 4145280
//   b = (-48641 * pStart + 65025 * pMid - 16384 * pEnd) / 4145280
//   c = pStart
var pStart, pMid, pEnd, i : byte;
	a, b : longint;
begin
	// Cap the values.
	if r1 < -256 then
		r1 := -256
	else
		if r1 > 256 then r1 := 256;
	if g1 < -256 then
		g1 := -256
	else
		if g1 > 256 then g1 := 256;
	if b1 < -256 then
		b1 := -256
	else
		if b1 > 256 then b1 := 256;
	// Build the RED curve.
	if r1 <= 128 then
		pStart := 0
	else
		pStart := (byte(r1 - 128) * 255 + 64) shr 7;
	pMid := (word(r1 + 256) * 255 + 256) shr 9;
	if r1 >= -128 then
		pEnd := 255
	else
		pEnd := (word(256 + r1) * 255 + 64) shr 7;
	a := word(pStart * 127) - word(pMid * 255) + word(pEnd * 128);
	b := dword(pMid * 65025) - dword(pEnd * 16384) - dword(pStart * 48641);
	for i := 255 downto 0 do RGBtweakTable[i] := (a * i + b) * i div 4145280 + pStart;
	// Build the GREEN curve.
	if g1 <= 128 then
		pStart := 0
	else
		pStart := (byte(g1 - 128) * 255 + 64) shr 7;
	pMid := (word(g1 + 256) * 255 + 256) shr 9;
	if g1 >= -128 then
		pEnd := 255
	else
		pEnd := (word(256 + g1) * 255 + 64) shr 7;
	a := word(pStart * 127) - word(pMid * 255) + word(pEnd * 128);
	b := dword(pMid * 65025) - dword(pEnd * 16384) - dword(pStart * 48641);
	for i := 255 downto 0 do RGBtweakTable[i or 256] := (a * word(i * i) + b * i) div 4145280 + pStart;
	// Build the BLUE curve.
	if b1 <= 128 then
		pStart := 0
	else
		pStart := (byte(b1 - 128) * 255 + 64) shr 7;
	pMid := (word(b1 + 256) * 255 + 256) shr 9;
	if b1 >= -128 then
		pEnd := 255
	else
		pEnd := (word(256 + b1) * 255 + 64) shr 7;
	a := word(pStart * 127) - word(pMid * 255) + word(pEnd * 128);
	b := dword(pMid * 65025) - dword(pEnd * 16384) - dword(pStart * 48641);
	for i := 255 downto 0 do RGBtweakTable[i or 512] := (a * word(i * i) + b * i) div 4145280 + pStart;
end;

procedure ApplyRGBtweak;//(clipdata : pblitstruct);
// Takes the area in rendertarget^ at the given clip destination coordinates, and runs every pixel through a gamma
// correction conversion, as precalculated by BuildRGBtweakTable into the RGBtweakTable[] array.
// Must take premul alpha into account; the RGB tweak function which currently could take any input 0..255 and return
// any output 0..255 must be scaled to return output in the range 0.. this pixel's alpha.
//var x, y : dword;
begin
	y := bdata^.sourcerows;
	while y <> 0 do begin
		dec(y);
		x := bdata^.sourcecopywidth;
		while x <> 0 do begin
			dec(x);
			byte((mv_OutputBuffy + bdata^.destofs)^) := RGBtweaktable[byte((mv_OutputBuffy + bdata^.destofs)^) + 512];
			inc(bdata^.destofs);
			byte((mv_OutputBuffy + bdata^.destofs)^) := RGBtweaktable[byte((mv_OutputBuffy + bdata^.destofs)^) + 256];
			inc(bdata^.destofs);
			byte((mv_OutputBuffy + bdata^.destofs)^) := RGBtweaktable[byte((mv_OutputBuffy + bdata^.destofs)^) + 0];
			inc(bdata^.destofs, 2);
		end;
		inc(bdata^.destofs, bdata^.destskipwidth);
	end;
end;
{$endif bonk}

procedure TRendermatic.AddRefresh(x1p, y1p, x2p, y2p, vp : longint);
// Adds the given rectangle to Refresh[], a list of areas that are going get redrawn. Crops and merges this and other
// existing refresh rects so there's no overlap.
// If vp is a valid viewport index, clips the rectangle against that viewport.
// The input values are px coordinates relative to the full window.
// X1p:Y1p is inclusive top left, X2p:Y2p is exclusive bottom right.
var i : dword;
begin
	with Viewportmatic do
		if dword(vp) < dword(length(viewport)) then with viewport[vp].viewportLoc do begin
			// Clip against viewport.
			if x1p < leftp then x1p := leftp;
			if x2p > rightp then x2p := rightp;
			if y1p < topp then y1p := topp;
			if y2p > bottomp then y2p := bottomp;
		end;

	// After clipping, any visible pixels left?
	if (x2p <= x1p) or (y2p <= y1p) then exit;

	//log(strcat('+++ AddRefresh %,% to %,%', [x1p, y1p, x2p, y2p]));

	// Look for overlaps. (remember, x1p:y1p is inclusive, x2p:y2p is exclusive)
	i := numFresh;
	while i <> 0 do begin
		dec(i);

		// no overlap?
		if (x1p >= refresh[i].right) or (y1p >= refresh[i].bottom)
		or (x2p <= refresh[i].left) or (y2p <= refresh[i].top)
		then continue;

		// For the below comparisons, A is our new area, B is the existing area.
		//
		// To minimise refresh regions, operations should be done in this order:
		//   1. Preferably drop a full area
		//   2. Clip an area
		//   3. Split an area, but only if no other choice
		//
		// Fun optimisation idea: there are 4 possible comparisons per coordinate, and 4 coordinates, for a total of
		// 4^4 combinations. Why not do the comparisons and save the boolean result into a bit array, then do a simple
		// case switch for the combinations of interest? Would still have to absolutely respect the operation order...
		//
		// A careful analysis of the if-then-else flow might allow combining more comparisons, but be super careful.
		// You have to have ">=" and "<=" on both outcomes of an if-statement, or some cases get handled incorrectly.

		// A fully inside B: drop A
		if (x1p >= refresh[i].left) and (y1p >= refresh[i].top)
		and (x2p <= refresh[i].right) and (y2p <= refresh[i].bottom)
		then exit;

		// B fully inside A: drop B
		if (x1p <= refresh[i].left) and (y1p <= refresh[i].top)
		and (x2p >= refresh[i].right) and (y2p >= refresh[i].bottom)
		then begin
			dec(numFresh);
			refresh[i] := refresh[numFresh];
			continue;
		end;

		if (x1p >= refresh[i].left) then begin
			if (y1p >= refresh[i].top) then begin
				if (x2p <= refresh[i].right) then begin
					if (y2p > refresh[i].bottom) then begin
						// A poking into B from below: clip A's top
						y1p := refresh[i].bottom; continue;
					end;
				end
				else begin
					if (y2p <= refresh[i].bottom) then begin
						// A poking into B from right: clip A's left
						x1p := refresh[i].right; continue;
					end;
				end;
			end
			else begin
				if (x2p <= refresh[i].right) and (y2p <= refresh[i].bottom) then begin
					// A poking into B from above: clip A's bottom
					y2p := refresh[i].top; continue;
				end;
			end;
		end
		else begin
			if (y1p >= refresh[i].top) and (x2p <= refresh[i].right)
			and (y2p <= refresh[i].bottom)
			then begin
				// A poking into B from left: clip A's right
				x2p := refresh[i].left; continue;
			end;
		end;

		if (x1p <= refresh[i].left) then begin
			if (y1p <= refresh[i].top) then begin
				if (x2p >= refresh[i].right) then begin
					if (y2p < refresh[i].bottom) then begin
						// B poking into A from below: clip B's top
						refresh[i].top := y2p; continue;
					end;
				end
				else begin
					if (y2p >= refresh[i].bottom) then begin
							// B poking into A from right: clip B's left
							refresh[i].left := x2p; continue;
						end;
				end;
			end
			else begin
				if (x2p >= refresh[i].right) and (y2p >= refresh[i].bottom) then begin
					// B poking into A from above: clip B's bottom
					refresh[i].bottom := y1p; continue;
				end;
			end;
		end
		else begin
			// B poking into A from left: clip B's right
			if (y1p <= refresh[i].top) and (x2p >= refresh[i].right)
			and (y2p >= refresh[i].bottom)
			then begin
				refresh[i].right := x1p; continue;
			end;
		end;

		if (x1p >= refresh[i].left) then begin
			if (y1p < refresh[i].top) then begin
				if (x2p <= refresh[i].right) then begin
					if (y2p > refresh[i].bottom) then begin
						// A crosses B vertically: split A
						AddRefresh(x1p, y1p, x2p, refresh[i].top, -1); // A's top third
						y1p := refresh[i].bottom; // A's bottom third
						continue;
					end;
				end
				else begin
					if (y2p <= refresh[i].bottom) then begin
						// A bottom left corner in B: split A
						// clip A's bottom left corner, add bottom right corner as new
						AddRefresh(refresh[i].right, refresh[i].top, x2p, y2p, -1);
						y2p := refresh[i].top; // A's top half
						continue;
					end;
				end;
			end
			else begin
				if (x2p > refresh[i].right) and (y2p > refresh[i].bottom) then begin
					// A top left corner in B: split A
					// clip A's top left corner, add top right corner as new
					AddRefresh(refresh[i].right, y1p, x2p, refresh[i].bottom, -1);
					y1p := refresh[i].bottom; // A's bottom half
					continue;
				end;
			end;
		end
		else begin
			if (y1p >= refresh[i].top) then begin
				if (x2p > refresh[i].right) then begin
					if (y2p <= refresh[i].bottom) then begin
						// A crosses B horizontally: split A
						AddRefresh(x1p, y1p, refresh[i].left, y2p, -1); // A's left third
						x1p := refresh[i].right; // A's right third
						continue;
					end;
				end
				else begin
					if (y2p > refresh[i].bottom) then begin
						// A top right corner in B: split A
						// clip A's top right corner, add top left corner as new
						AddRefresh(x1p, y1p, refresh[i].left, refresh[i].bottom, -1);
						y1p := refresh[i].bottom; // A's bottom half
						continue;
					end;
				end;
			end
			else begin
				if (x2p <= refresh[i].right) and (y2p <= refresh[i].bottom) then begin
					// A bottom right corner in B: split A
					// clip A's bottom right corner, add bottom left corner as new
					AddRefresh(x1p, refresh[i].top, refresh[i].left, y2p, -1);
					y2p := refresh[i].top; // A's top half
					continue;
				end;
			end;
		end;

	end;

	if numFresh >= dword(length(refresh)) then setlength(refresh, length(refresh) + 32);
	with refresh[numFresh] do begin
		left := x1p;
		top := y1p;
		right := x2p;
		bottom := y2p;
	end;
	inc(numFresh);

	{for i := 0 to numFresh - 1 do
		with refresh[i] do log(strcat('Refresh %: %,% to %,%', [i, left, top, right, bottom]));}
end;

{$ifdef sakucon}
procedure TRendermatic.RemoveRefresh(x1p, y1p, x2p, y2p : longint);
// The console version needs to crop out any refresh regions that would get drawn over by textboxes. The SDL version
// handles textboxes differently and has no need for this.
var i : dword;
	j : longint;

	procedure _QuickAdd(x1, y1, x2, y2 : longint);
	// Re-adding sub-parts of existing regions needs no further clipping so just append them.
	begin
		with refresh[numFresh] do begin
			left := x1; top := y1; right := x2; bottom := y2;
		end;
		inc(numFresh);
	end;

begin
	// Clip against full window.
	if x1p < 0 then x1p := 0;
	if x2p > longint(sysvar.windowSize.w) then x2p := sysvar.windowSize.w;
	if y1p < 0 then y1p := 0;
	if y2p > longint(sysvar.windowSize.h) then y2p := sysvar.windowSize.h;
	// After clipping, any visible pixels left?
	if (x2p <= x1p) or (y2p <= y1p) then exit;

	//log(strcat('+++ DelRefresh %,% to %,%', [x1p, y1p, x2p, y2p]));

	// Remove this region from existing refresh regions.
	i := numFresh;
	while i <> 0 do begin
		dec(i);
		// no overlap?
		if (x1p >= refresh[i].right) or (y1p >= refresh[i].bottom)
		or (x2p <= refresh[i].left) or (y2p <= refresh[i].top)
		then continue;

		// For the below comparisons, A is our exclusion, B is the existing area.

		// A fully inside B: split B into four
		if (x1p > refresh[i].left) and (y1p > refresh[i].top)
		and (x2p < refresh[i].right) and (y2p < refresh[i].bottom)
		then begin
			j := refresh[i].bottom;
			refresh[i].bottom := y1p; // reduce B into top part
			_QuickAdd(refresh[i].left, y2p, refresh[i].right, j); // new bottom
			_QuickAdd(refresh[i].left, refresh[i].top, x1p, y2p); // new left
			_QuickAdd(x2p, refresh[i].top, refresh[i].right, y2p); // new right
			continue;
		end;

		// B fully inside A: drop B
		if (x1p <= refresh[i].left) and (y1p <= refresh[i].top)
		and (x2p >= refresh[i].right) and (y2p >= refresh[i].bottom)
		then begin
			dec(numFresh);
			refresh[i] := refresh[numFresh];
			continue;
		end;

		if (x1p > refresh[i].left) then begin
			if (y1p > refresh[i].top) then begin
				if (x2p < refresh[i].right) then begin
					if (y2p >= refresh[i].bottom) then begin
						// A poking into B from below: split B
						j := refresh[i].bottom;
						refresh[i].bottom := y1p; // reduce B into top part
						_QuickAdd(refresh[i].left, y1p, x1p, j); // new left
						_QuickAdd(x2p, y1p, refresh[i].right, j); // new right
					end;
				end

				else begin // x2p >= refresh[i].right
					if (y2p < refresh[i].bottom) then begin
						// A poking into B from right: split B
						j := refresh[i].bottom;
						refresh[i].bottom := y1p; // reduce B into top part
						_QuickAdd(refresh[i].left, y1p, x1p, y2p); // new left
						_QuickAdd(refresh[i].left, y2p, refresh[i].right, j); // new bottom
					end

					else begin // y2p >= refresh[i].bottom
						// A top left corner in B: split B
						j := refresh[i].bottom;
						refresh[i].bottom := y1p; // reduce B to top
						_QuickAdd(refresh[i].left, y1p, x1p, j); // new left
					end;
				end;
			end

			else begin
				if (x2p < refresh[i].right) then begin
					if (y2p < refresh[i].bottom) then begin
						// A poking into B from above: split B
						j := refresh[i].top;
						refresh[i].top := y2p; // reduce B into bottom part
						_QuickAdd(refresh[i].left, j, x1p, y2p); // new left
						_QuickAdd(x2p, j, refresh[i].right, y2p); // new right
					end

					else begin // y2p >= refresh[i].bottom
						// A crosses B vertically: split B
						j := refresh[i].right;
						refresh[i].right := x1p; // reduce B to left
						_QuickAdd(x2p, refresh[i].top, j, refresh[i].bottom); // new right
					end;
				end

				else begin // x2p >= refresh[i].right
					if (y2p < refresh[i].bottom) then begin
						// A bottom left corner in B: split B
						j := refresh[i].top;
						refresh[i].top := y2p; // reduce B to bottom
						_QuickAdd(refresh[i].left, j, x1p, y2p); // new left
					end

					else begin // y2p >= refresh[i].bottom
						// B poking into A from left: clip B's right
						refresh[i].right := x1p;
					end;
				end;
			end;
		end

		else begin // x1p <= refresh[i].left
			if (y1p > refresh[i].top) then begin
				if (x2p < refresh[i].right) then begin
					if (y2p < refresh[i].bottom) then begin
						// A poking into B from left: split B
						j := refresh[i].bottom;
						refresh[i].bottom := y1p; // reduce B into top part
						_QuickAdd(x2p, y1p, refresh[i].right, y2p); // new right
						_QuickAdd(refresh[i].left, y2p, refresh[i].right, j); // new bottom
					end

					else begin // y2p >= refresh[i].bottom
						// A top right corner in B: split B
						j := refresh[i].bottom;
						refresh[i].bottom := y1p; // reduce B to top
						_QuickAdd(x2p, y1p, refresh[i].right, j); // new right
					end;
				end

				else begin // x2p >= refresh[i].right
					if (y2p < refresh[i].bottom) then begin
						// A crosses B horizontally: split B
						j := refresh[i].bottom;
						refresh[i].bottom := y1p; // reduce B to top
						_QuickAdd(refresh[i].left, y2p, refresh[i].right, j); // new bottom
					end

					else begin // y2p >= refresh[i].bottom
						// B poking into A from above: clip B's bottom
						refresh[i].bottom := y1p;
					end;
				end;
			end

			else begin // y1p <= refresh[i].top
				if (x2p < refresh[i].right) then begin
					if (y2p < refresh[i].bottom) then begin
						// A bottom right corner in B: split B
						j := refresh[i].top;
						refresh[i].top := y2p; // reduce B to bottom
						_QuickAdd(x2p, j, refresh[i].right, y2p); // new right
					end

					else begin // y2p >= refresh[i].bottom
						// B poking into A from right: clip B's left
						refresh[i].left := x2p;
					end;
				end

				else begin // x2p >= refresh[i].right
					if (y2p < refresh[i].bottom) then begin
						// B poking into A from below: clip B's top
						refresh[i].top := y2p;
					end;
				end;
			end;
		end;

	end;

	{if numFresh <> 0 then for i := 0 to numFresh - 1 do
		with refresh[i] do log(strcat('Refresh %: %,% to %,%', [i, left, top, right, bottom]));}
end;
{$endif sakucon}

procedure TRendermatic.RenderTransition(const effect : TEffectTransition);
// Mixes an old stashed view with the currently active one. Effect must be a valid transition effect, which is used to
// track the transition's progress. When starting a transition, StashRender should be called to retain a view to
// transition from.
var srcp, destp, datap : pointer;
	i, j, k, l : dword;
	targetx, targety, targetsizex, targetsizey : word;
	x, y, tox : dword;
	rowstartskipbytes, rowendskipbytes : dword;
begin
	if effect.ageLimit = 0 then exit;

	with Viewportmatic.viewport[effect.fxViewport] do begin
		targetx := viewportLoc.leftp;
		targety := viewportLoc.topp;
		targetsizex := viewportSizeP.w;
		targetsizey := viewportSizeP.h;
	end;
	rowendskipbytes := (sysvar.windowSize.w - targetsizex) shl 2;

	// Set up stream pointers.
	i := (targety * sysvar.windowSize.w + targetx) shl 2;
	srcp := effect.data + i;
	destp := outputBuffy + i;

	case effect.transitionStyle of

		ETransitionType.Instant: ;

		ETransitionType.WipeFromLeft:
		begin
			// Calculate soft edge width.
			tox := targetsizex shr 4 + 1;
			// Calculate completion amount: runs from 0 to targetsizex + edge size.
			// This is the leading edge of the soft edge.
			i := dword(high(coscos)) * effect.age div effect.ageLimit;
			j := (coscos[i] * (targetsizex + tox)) shr 16;
			// Get the width of the completed area behind the soft edge and skip it.
			if j > tox then begin
				rowstartskipbytes := (j - tox) shl 2;
				inc(srcp, rowstartskipbytes);
				inc(destp, rowstartskipbytes);
				inc(rowendskipbytes, rowstartskipbytes);
			end;
			// Get the width of the pending area ahead of the soft edge.
			k := 0;
			if j < targetsizex then begin
				k := (targetsizex - j) shl 2;
				inc(rowendskipbytes, k);
			end;
			// Clip the soft edge.
			if j < tox then
				tox := j
			else
				if j > targetsizex then dec(tox, j - targetsizex);

			for y := targetsizey - 1 downto 0 do begin
				{$push}{$R-}
				// Do the soft edge.
				x := tox; l := 0;
				while x <> 0 do begin
					dec(x); inc(l);
					byte(destp^) := (byte(destp^) * x + byte(srcp^) * l) div tox;
					inc(srcp); inc(destp);
					byte(destp^) := (byte(destp^) * x + byte(srcp^) * l) div tox;
					inc(srcp); inc(destp);
					byte(destp^) := (byte(destp^) * x + byte(srcp^) * l) div tox;
					inc(srcp, 2); inc(destp, 2);
				end;
				// Fill the pending area ahead of the soft edge.
				if k <> 0 then move(srcp^, destp^, k);
				// Skip to next row.
				inc(srcp, rowendskipbytes);
				inc(destp, rowendskipbytes);
				{$pop}
			end;
		end;

		ETransitionType.RaggedWipe:
		begin
			// Calculate completion amount: runs from 0 to width (preset to targetsizex + maxedgesize).
			// This is the leading edge of the soft edge.
			i := dword(high(coscos)) * effect.age div effect.ageLimit;
			j := (coscos[i] * effect.width) shr 16;
			// Get the width of the pending area ahead of the soft edge.
			k := 0;
			if j < targetsizex then begin
				k := (targetsizex - j) shl 2;
				inc(rowendskipbytes, k);
			end;

			datap := effect.data + (sysvar.windowSize.w * sysvar.windowSize.h) shl 2;
			for y := targetsizey - 1 downto 0 do begin
				// Get the soft edge width for this row.
				tox := word(datap^); inc(datap, 2);
				// Get the width of the completed area behind the soft edge, skip it.
				if j > tox then begin
					rowstartskipbytes := (j - tox);
					if rowstartskipbytes > targetsizex then rowstartskipbytes := targetsizex;
					rowstartskipbytes := rowstartskipbytes shl 2;
					inc(srcp, rowstartskipbytes);
					inc(destp, rowstartskipbytes);
				end;
				// Clip the soft edge.
				if j < tox then
					tox := j
				else
					if j > targetsizex then
						if j >= tox + targetsizex then
							tox := 0
						else
							dec(tox, j - targetsizex);
				{$push}{$R-}
				// Do the soft edge.
				x := tox; l := 0;
				while x <> 0 do begin
					dec(x); inc(l);
					byte(destp^) := (byte(destp^) * x + byte(srcp^) * l) div tox;
					inc(srcp); inc(destp);
					byte(destp^) := (byte(destp^) * x + byte(srcp^) * l) div tox;
					inc(srcp); inc(destp);
					byte(destp^) := (byte(destp^) * x + byte(srcp^) * l) div tox;
					inc(srcp, 2); inc(destp, 2);
				end;
				// Fill the pending area ahead of the soft edge.
				if k <> 0 then move(srcp^, destp^, k);
				// Skip to next row.
				inc(srcp, rowendskipbytes);
				inc(destp, rowendskipbytes);
				{$pop}
			end;
		end;

		ETransitionType.Interlaced:
		begin
			// Calculate completion amount: runs from 0 to targetsizey - 1.
			i := dword(high(coscos)) * effect.age div effect.ageLimit;
			j := (coscos[i] * targetsizey) shr 16;
			k := targetsizey - 1 - j; // inverse
			l := targetsizex shl 2;
			rowendskipbytes := sysvar.windowSize.w shl 2;

			for y := targetsizey - 1 downto 0 do begin
				{$push}{$R-}
				if (y and 1 = 0) and (y > j)
				or (y and 1 <> 0) and (y < k)
				then begin
					move(srcp^, destp^, l);
				end;
				inc(srcp, rowendskipbytes);
				inc(destp, rowendskipbytes);
				{$pop}
			end;
		end;

		ETransitionType.Crossfade:
		begin
			k := effect.age * 255 div effect.ageLimit; // time elapsed, 255 = 100%
			j := 255 - k; // time left

			for y := targetsizey - 1 downto 0 do begin
				for x := targetsizex - 1 downto 0 do begin
					{$push}{$R-}
					byte(destp^) := alphamixtab[k, byte(destp^)] + alphamixtab[j, byte(srcp^)];
					inc(srcp); inc(destp);
					byte(destp^) := alphamixtab[k, byte(destp^)] + alphamixtab[j, byte(srcp^)];
					inc(srcp); inc(destp);
					byte(destp^) := alphamixtab[k, byte(destp^)] + alphamixtab[j, byte(srcp^)];
					inc(srcp, 2); inc(destp, 2);
					{$pop}
				end;
				inc(srcp, rowendskipbytes);
				inc(destp, rowendskipbytes);
			end;
		end;

		else begin
			LogError('RenderTransition: bad transition style: ' + strdec(longint(effect.transitionStyle)));
			effect.Destroy;
		end;
	end;

	srcp := NIL; destp := NIL;
end;

procedure TRendermatic.RenderRect(const refrect : TEdgeCoordP; const destbuf : pointer; noboxes : boolean);
// Completely redraws everything in the given rectangle. The destination buffer must be outputBuffy or another buffer
// of the same exact size. Optionally skips drawing textboxes.
var i, j, k, l : dword;
	clipsi : blitstruct;
	poku : pointer;
begin
	// Find the lowest alpha-less element that covers the entire refresh area.
	i := high(dword);
	with EleHub do if numElements <> 0 then begin
		for j := numElements - 1 downto 0 do with ele[j] do
			if (eleIsVisible or eleRenderNow) and (eleAlpha = $FF) then begin
				if ele[j] is TGob then with ele[j] as TGob do
				if ((runtimeDynamic) and (solidBlit.a = $FF))
				or ((cachedObject <> NIL)
				and (cachedObject.memFormat in [MCG_FORMAT_BGR, MCG_FORMAT_BGRX, MCG_FORMAT_INDEXED])
				and ((dword(solidBlit) = 0) or (solidBlit.a = $FF)))
				then
					if (eleLoc.xp <= refrect.left)
					and (eleLoc.yp <= refrect.top)
					and (eleLoc.xp + longint(eleSizeP.w) >= refrect.right)
					and (eleLoc.yp + longint(eleSizeP.h) >= refrect.bottom)
					then begin
						i := j;
						break;
					end;
			end;
	end;

	// If no alpha-less element covers the entire area, gotta draw all elements that touch the area at all, and paint
	// the background void to avoid potential ghost images.
	if i = high(dword) then begin
		i := (refrect.top * longint(sysvar.windowSize.w) + refrect.left) shl 2;
		poku := destbuf + i;
		j := refrect.bottom - refrect.top; // rows
		k := refrect.right - refrect.left; // cols
		l := sysvar.windowSize.w * 4;
		while j <> 0 do begin
			dec(j);
			filldword(poku^, k, dword(sysvar.voidColor));
			inc(poku, l);
		end;
		i := 0;
	end;

	// Draw that lowest alpha-less element and everything over it.
	with EleHub do while i < numElements do if ele[i] is TGob then with ele[i] as TGob do begin
		if (eleIsVisible or eleRenderNow) then begin

			Assert((runtimeDynamic) or (cachedObject <> NIL));
			with clipsi do begin
				srcp := eleFrameP;
				destp := destbuf;
				srcofs := 0;
				destofsxp := eleLoc.xp;
				destofsyp := eleLoc.yp;

				// Select the right frame to draw by adjusting the image source offset.
				copywidth := eleSizeP.w;
				copyrows := eleSizeP.h;

				// Clip against the refresh rectangle.
				clipx1p := refrect.left;
				clipy1p := refrect.top;
				clipx2p := refrect.right;
				clipy2p := refrect.bottom;
				// Also clip against the containing viewport.
				clipviewport := eleViewport;
			end;

			// Calculate the how much of the element should be drawn after clipping.
			ClipRGB(@clipsi);

			if (clipsi.copywidth <> 0) and (clipsi.copyrows <> 0) then begin
				// At last, draw the graphic.
				{with clipsi do log(strcat('[draw %:% (src %) locp=%,% sizep=%x% a=% frame=% p=&]',
					[i, eleName, gfxListName, destofsxp, destofsyp, copywidth, copyrows, eleAlpha, gobFrame, eleFrameP]));}

				if dword(solidBlit) <> 0 then begin
					j := dword(solidBlit);
					if eleAlpha <> $FF then RGBAquad(j).a := RGBAquad(j).a * eleAlpha div $FF;
				end;

				if runtimeDynamic then begin
					// If solidBlit = 0 then do nothing - a virtual element.
					if dword(solidBlit) <> 0 then DrawFlat(@clipsi, RGBAquad(j));
				end

				else if dword(solidBlit) <> 0 then begin
					DrawSolid(@clipsi, RGBAquad(j));
				end
				else if eleAlpha <> $FF then
					DrawRGBA32alpha(@clipsi, eleAlpha)
				else if cachedObject.memFormat = MCG_FORMAT_BGRX then
					DrawRGBX32(@clipsi)
				else
					DrawRGBA32(@clipsi);
			end;
		end;

		inc(i);
	end;

	// After all graphics have been drawn in a refresh rectangle, apply the gamma correction effect over that rectangle,
	// if necessary.
	{$ifdef bonk}
	if RGBtweakactive <> $FF then with clipsi do begin
		{$note this RGBtweak doesn't handle viewports correctly}
		destp := destbuf;
		destofsx := refrect.left;
		destofsy := refrect.top;
		sourcecopywidth := refrect.right - refrect.left;
		sourcerows := refrect.bottom - refrect.top;
		sourceofs := 0;
		cliplocxp := viewport[1].viewportLoc.leftp; clipsizexp := viewport[1].viewportsizexp;
		cliplocyp := viewport[1].viewportLoc.topp; clipsizeyp := viewport[1].viewportsizeyp;
		ClipRGB(@clipsi);
		ApplyRGBtweak(@clipsi);
	end;
	{$endif}

	// Draw ongoing transitions now.
	// (With textboxes merged into elements, transition should be drawn at a specific Z-level. Things that shouldn't be
	// included in the effect, like boxes, should be given higher Z-levels.)
	with EffectHub do
		if transitionsActive <> 0 then
			for i := fxCount - 1 downto 0 do
				if fx[i] is TEffectTransition then RenderTransition(fx[i] as TEffectTransition);

	{$ifndef sakucon}
	// Draw textboxes, maybe. If taking a screenshot for a transition etc, don't want textboxes.
	if NOT noboxes then begin
		for i := 1 to high(BoxHub.textbox) do with BoxHub.textbox[i] do begin
			if (boxState <> EBoxState.Null) and (NOT isHidden) then begin
				with clipsi do begin
					Assert(buffers.finalP <> NIL, 'finalP NULL for ' + boxName);
					srcp := buffers.finalP;
					destp := destbuf;
					destofsxp := boxRect.renderLocP.x;
					destofsyp := boxRect.renderLocP.y;
					copywidth := boxRect.renderSizeP.w;
					copyrows := boxRect.renderSizeP.h;
					srcofs := 0;

					// Clip against the refresh rectangle.
					clipx1p := refrect.left;
					clipy1p := refrect.top;
					clipx2p := refrect.right;
					clipy2p := refrect.bottom;
					// Also clip against the containing viewport.
					clipviewport := boxInViewport;
				end;

				// Calculate the how much of the element should be drawn after clipping.
				ClipRGB(@clipsi);

				if (clipsi.copywidth <> 0) and (clipsi.copyrows <> 0) then begin

					//with clipsi do log(strcat('[box %: locp=%,% sizep=%x%]', [boxName, destofsxp, destofsyp, copywidth, copyrows]));

					// Draw the box.
					if boxState in [EBoxState.Appearing, EBoxState.Vanishing] then begin
						case style.popType of
							EPopType.Fade:
								DrawRGBA32alpha(@clipsi, dword(popRunTimeMsec) * 255 div style.popTimeMsec);
							EPopType.Swipe:
								DrawRGBA32wipe(@clipsi, (dword(popRunTimeMsec) shl 15) div style.popTimeMsec,
								boxState = EBoxState.Appearing);
							else DrawRGBA32(@clipsi);
						end;
					end
					else
						case style.boxBlendMode of
							EBlendMode.Hardlight: DrawRGBA32Hardlight(@clipsi);
							EBlendMode.Multiply: DrawRGBA32Multiply(@clipsi);
							EBlendMode.Difference: DrawRGBA32Difference(@clipsi);
							EBlendMode.Additive: DrawRGBA32Additive(@clipsi);
							else DrawRGBA32(@clipsi);
						end;
				end;

			end;
		end;
	end;
	{$endif}
end;

function TRendermatic.StashRender(viewnum{, cutoffz} : dword; out destbuf : pointer) : dword;
// Copies the viewport's area from outputBuffy^ into destbuf^. The caller is responsible for freeing destbuf.
// Any elements whose z is above cutoffz are removed from destbuf^.
// Returns the byte size of the stashed area.
// The resulting image can be used as a snapshot of an earlier game state, for example as the departing transition
// image, or as a save game thumbnail.
var backupfresh : array of TEdgeCoordP;
	i, backupnumfresh : dword;
begin
	// Initial bulk copy.
	result := (sysvar.windowSize.w * sysvar.windowSize.h) shl 2;
	getmem(destbuf, result + sysvar.windowSize.h shl 1); // plus space for ragged list
	move(outputBuffy^, destbuf^, result);
	// Back up the current refresh regions.
	backupfresh := refresh;
	backupnumfresh := numfresh;
	refresh := NIL;
	numfresh := 0;
	// Build a new refresh list from currently shown textboxes.
	for i := high(BoxHub.textbox) downto 1 do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Null) and (NOT isHidden) then AddRefresh;
	// Draw over all the textboxes.
	while numfresh <> 0 do begin
		dec(numfresh);
		RenderRect(refresh[numfresh], destbuf, TRUE);
	end;
	// Restore the refresh regions.
	refresh := backupfresh;
	numfresh := backupnumfresh;
	backupfresh := NIL;
end;

procedure TRendermatic.Destroy;
begin
	if outputBuffy <> NIL then begin freemem(outputBuffy); outputBuffy := NIL; end;
end;

