{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

procedure TChoicematic.Init;
// Resets everything to defaults. Can't be a constructor because restarting a game needs to call this.
// Only called by hubstack...
begin
	choiceBox := NIL;
	choicePartBox := NIL;
	highlightBox := NIL;
	setlength(choiceNodes, 0);
	setlength(choiceNodes, 8);
	fillbyte(choiceNodes[0], sizeof(TChoiceNode), 0); // virtual root node
	choiceNodeCount := 1;
	selectedNode := -1;
	activeLanguage := 0;
	userCanCancel := 0;
	isActive := FALSE;
	//highlightIndex := -1;
	setlength(mostRecentChoice, 4);
	ResetChoices;
end;

procedure TChoicematic.ResetChoices;
// Removes all added choices, ready for a new set of choices.
begin
	if isActive then Deactivate(TRUE, FALSE);
	Assert(choiceNodeCount <> 0);

	RemoveChoice(''); // clears all choices properly
	if length(choiceNodes) > 20 then // unusually large choice list, reduce size
		setlength(choiceNodes, length(choiceNodes) - 4);

	fillbyte(choiceNodes[0], sizeof(TChoiceNode), 0); // zero out virtual root node
	highlightIndex := -1;
	onHighlight := '';
	if mostRecentChoice.Length > 4 then setlength(mostRecentChoice, 4);
	mostRecentChoice[0] := '';
end;

procedure TChoicematic.Deactivate(cancelled, clearboxes : boolean);
// If the choicematic is active, disables it, clears choice-related boxes, and resumes all fibers that were waiting
// for a choice. If the choice was cancelled, sets the finalised choice to empty.
var l : dword;
	i : byte;
begin
	if NOT isActive then exit;
	isActive := FALSE;

	if cancelled then begin
	// Remember which choice was last highlighted, so can default highlight to that if coming back here.
		if choiceBox <> NIL then with choiceBox.content do begin
			Assert(highlightIndex < longint(choiceCount));
			Assert(choiceList[highlightIndex].index < choiceNodeCount);

			l := choiceList[highlightIndex].index;
			i := choiceNodes[selectedNode].level;
			if dword(i + 1) >= mostRecentChoice.Length then
				setlength(mostRecentChoice, mostRecentChoice.Length shl 1);
			if mostRecentChoice[i] <> choiceNodes[l].choiceTxt[activeLanguage] then begin
				mostRecentChoice[i] := choiceNodes[l].choiceTxt[activeLanguage];
				mostRecentChoice[i + 1] := '';
			end;
		end;

		selectedNode := -1;
		highlightIndex := -1;
	end;

	if clearboxes then begin
		if choiceBox <> NIL then choiceBox.Clear;
		if choicePartBox <> NIL then choicePartBox.Clear;
		if highlightBox <> NIL then highlightBox.Clear;
	end;

	FiberHub.SignalFibers('', [EFiberState.WaitChoice]);
end;

function TChoicematic.EqualsChoice(const txt : TStringBunch; lvl : byte; index : longint) : boolean;
// Returns TRUE if the given choice node index and its parents match the choice strings in txt.
// The string comparison is case-insensitive.
// Use lvl 1 to compare the full txt; use lvl 2 to compare one less from end (parent).
var i : dword;
begin
	result := FALSE;
	Assert((lvl >= 1) and (lvl <= txt.Length));
	Assert(index < longint(choiceNodeCount));
	i := txt.Length - lvl;
	while TRUE do begin
		if upcase(txt[i]) <> upcase(choiceNodes[index].choiceTxt[activeLanguage]) then exit;
		if choiceNodes[index].parent = 0 then if i = 0 then break else exit;
		if i = 0 then exit;
		dec(i);
		index := choiceNodes[index].parent;
	end;
	result := TRUE;
end;

function TChoicematic.FindChoice(const txt : TStringBunch; lvl : byte = 1) : dword;
// Returns the choiceNodes[] index of the given ["verb","noun"] bunch, in Choicematic's active language; returns 0
// if not found. The node can be locked or unlocked. The choice string comparison is case-insensitive.
// Use lvl 1 to find the full node; use lvl 2 to find its parent instead.
begin
	Assert((lvl >= 1) and (lvl <= txt.Length));
	// This scans from the last choice backward, comparing from the last given part to the first.
	// So LOOK:DOG:RUDELY will find a "RUDELY" choice node, then verifies its parent is "DOG", and again, "LOOK".
	result := choiceNodeCount;
	while result <> 0 do begin
		dec(result);
		if (choiceNodes[result].choiceTxt <> NIL) and (EqualsChoice(txt, lvl, result)) then exit;
	end;
end;

function TChoicematic.GetFullChoiceTxt(nodeindex : dword) : UTF8string;
// Returns the full colon-separated choice text of this node and its parents.
begin
	Assert((nodeindex < choiceNodeCount) and (nodeindex <> 0));
	Assert(choiceNodes[nodeindex].choiceTxt <> NIL);
	result := choiceNodes[nodeindex].choiceTxt[activeLanguage];
	while TRUE do begin
		nodeindex := choiceNodes[nodeindex].parent;
		if nodeindex = 0 then exit;
		result := choiceNodes[nodeindex].choiceTxt[activeLanguage] + ':' + result;
	end;
end;

procedure TChoicematic.HighlightChoice(showindex : dword; _style : EMoveType = EMoveType.Halfcos);
// Sets the higlight box over the specified choice index. The box slides over with the given style.
// Scrolls the choiceBox if the highlighted choice is currently out of view.
// If the onHighlight callback is defined, spawns a new fiber to run it.
var lightlocp : TEdgeCoordP;
	{$ifndef sakucon} lightloc : TEdgeCoord32k; {$endif}
	i, newscrollofsp : longint;
begin
	Assert(isActive);
	highlightIndex := showindex;
	if choiceBox <> NIL then with choiceBox do begin
		Assert(showindex < content.choiceCount, strcat('hilite %/%', [showindex, content.choiceCount]));
		// The coords are pixel values relative to the box's content, without any margins or padding.
		lightlocp := content.choiceList[showindex].showLocP;

		// If the choice being highlighted is (almost) outside the scroll window, scroll choicebox to make it visible.
		newscrollofsp := contentWin.scrollOfsP;

		i := lightlocp.bottom + longint(contentWin.lineHeightP - contentWin.contentWinSizeP.h);
		if i > contentWin.scrollOfsP then newscrollofsp := i;

		i := lightlocp.top - contentWin.lineHeightP;
		if i < contentWin.scrollOfsP then newscrollofsp := i;

		i := ScrollTo((newscrollofsp shl 15 + longint(contentWin.lineHeightP shr 1)) div longint(contentWin.lineHeightP));
		// Recalculate capped scroll pixel offset.
		newscrollofsp := (i * longint(contentWin.lineHeightP) + 16384) shr 15;

		// Deduct choicebox's final scrolling offset.
		dec(lightlocp.top, newscrollofsp);
		dec(lightlocp.bottom, newscrollofsp);

		// Add choicebox's margins and pixel coordinates relative to the full game window.
		// This gives us the exact unpadded choice pixel coords relative to the full game window.
		with boxRect do begin
			i := paddedLocP.x + longint(padding.leftp);
			inc(lightlocp.left, i);
			inc(lightlocp.right, i);
			i := paddedLocP.y + longint(padding.topp);
			inc(lightlocp.top, i);
			inc(lightlocp.bottom, i);
		end;

		{$ifdef sakucon}
		choiceBox.boxNeedsRedraw := TRUE;
		{$else}

		if highlightBox <> NIL then begin
			with highlightBox do with boxRect do begin
				// Add the highlight box's own padding.
				if parametersNeedUpdate then UpdateParameters;
				dec(lightlocp.left, padding.leftp);
				inc(lightlocp.right, longint(padding.rightp));
				dec(lightlocp.top, padding.topp);
				inc(lightlocp.bottom, longint(padding.bottomp));
				// Convert the exact full-window pixel location to 32k coords relative to the highlight box's viewport.
				with Viewportmatic.viewport[boxInViewport] do begin
					lightloc.left := lightlocp.left * 32768 div longint(viewportSizeP.w);
					lightloc.right := lightlocp.right * 32768 div longint(viewportSizeP.w);
					lightloc.top := lightlocp.top * 32768 div longint(viewportSizeP.h);
					lightloc.bottom := lightlocp.bottom * 32768 div longint(viewportSizeP.h);
				end;
				// Make sure the highlight box pops in.
				if boxState in [EBoxState.Null, EBoxState.Vanishing] then _style := EMoveType.Instant;
				boxState := EBoxState.Appearing;
			end;

			i := 160; // msec
			if _style = EMoveType.Instant then i := 0;
			with lightloc do begin
				TEffectBoxMove.Create(NIL, i, highlightBox, left, top, 0, 0, _style);
				TEffectBoxSize.Create(NIL, i, highlightBox, right - left, bottom - top, _style);
			end;
		end;
		{$endif}
	end;

	// Trigger the on-highlight callback if defined.
	if onHighlight <> '' then TFiber.Create(onHighlight, '');
end;

procedure TChoicematic.MoveHighlightUp(steps : dword = 1);
var i, closestindex, closestdist : dword;
begin
	Assert(isActive);
	if choiceBox = NIL then begin
		if highlightIndex < longint(steps) then i := 0 else i := highlightIndex - steps;
		HighlightChoice(i);
		exit;
	end;

	if choiceBox.content.choiceCount = 0 then exit;
	with choiceBox.content do while steps <> 0 do begin
		if highlightIndex = 0 then break;
		closestindex := high(dword); closestdist := high(dword);
		for i := highlightIndex - 1 downto 0 do begin
			// Is this choice above the highlighted choice?
			if (choiceList[i].showLocP.bottom <= choiceList[highlightIndex].showLocP.top)
			// Does this choice overlap the highlighted choice's X coords?
			and (choiceList[i].showLocP.left < choiceList[highlightIndex].showLocP.right)
			and (choiceList[i].showLocP.right > choiceList[highlightIndex].showLocP.left)
			// Is the choice closer than previous closest?
			and (dword(choiceList[highlightIndex].showLocP.top - choiceList[i].showLocP.top) < closestdist)
			then begin
				closestindex := i;
				closestdist := choiceList[highlightIndex].showLocP.top - choiceList[i].showLocP.top;
			end;
		end;
		if closestindex >= choiceCount then break;
		highlightIndex := closestindex;
		dec(steps);
	end;
	HighlightChoice(highlightIndex);
end;

procedure TChoicematic.MoveHighlightDown(steps : dword = 1);
var i, closestindex, closestdist : dword;
begin
	Assert(isActive);
	if choiceBox = NIL then begin
		HighlightChoice(highlightIndex + longint(steps));
		exit;
	end;

	if choiceBox.content.choiceCount = 0 then exit;
	with choiceBox.content do while steps <> 0 do begin
		if highlightIndex + 1 >= longint(choiceCount) then break;
		closestindex := high(dword); closestdist := high(dword);
		for i := highlightIndex + 1 to choiceCount - 1 do begin
			// Is this choice below the highlighted choice?
			if (choiceList[i].showLocP.top >= choiceList[highlightIndex].showLocP.bottom)
			// Does this choice overlap the highlighted choice's X coords?
			and (choiceList[i].showLocP.left < choiceList[highlightIndex].showLocP.right)
			and (choiceList[i].showLocP.right > choiceList[highlightIndex].showLocP.left)
			// Is the choice closer than previous closest?
			and (dword(choiceList[i].showLocP.top - choiceList[highlightIndex].showLocP.top) < closestdist)
			then begin
				closestindex := i;
				closestdist := choiceList[i].showLocP.top - choiceList[highlightIndex].showLocP.top;
			end;
		end;
		if closestindex >= choiceCount then break;
		highlightIndex := closestindex;
		dec(steps);
	end;
	HighlightChoice(highlightIndex);
end;

procedure TChoicematic.MoveHighlightLeft;
var i, closestindex, closestdist : dword;
begin
	Assert(isActive);
	if choiceBox = NIL then begin
		if highlightIndex <> 0 then HighlightChoice(highlightIndex - 1);
		exit;
	end;

	with choiceBox.content do begin
		if (highlightIndex = 0) or (choiceCount = 0) then exit;
		closestindex := high(dword); closestdist := high(dword);
		for i := choiceCount - 1 downto 0 do begin
			if i = dword(highlightIndex) then continue;
			// Is this choice to the left of the highlighted choice?
			if (choiceList[i].showLocP.right <= choiceList[highlightIndex].showLocP.left)
			// Does this choice overlap the highlighted choice's Y coords?
			and (choiceList[i].showLocP.top < choiceList[highlightIndex].showLocP.bottom)
			and (choiceList[i].showLocP.bottom > choiceList[highlightIndex].showLocP.top)
			// Is the choice closer than previous closest?
			and (dword(choiceList[highlightIndex].showLocP.left - choiceList[i].showLocP.left) < closestdist)
			then begin
				closestindex := i;
				closestdist := choiceList[highlightIndex].showLocP.left - choiceList[i].showLocP.left;
			end;
		end;
		if closestindex < choiceCount then HighlightChoice(closestindex);
	end;
end;

procedure TChoicematic.MoveHighlightRight;
var i, closestindex, closestdist : dword;
begin
	Assert(isActive);
	if choiceBox = NIL then begin
		HighlightChoice(highlightIndex + 1);
		exit;
	end;

	with choiceBox.content do begin
		if (choiceCount = 0) or (highlightIndex + 1 = longint(choiceCount)) then exit;
		closestindex := high(dword); closestdist := high(dword);
		for i := choiceCount - 1 downto 0 do begin
			if i = dword(highlightIndex) then continue;
			// Is this choice to the right of the highlighted choice?
			if (choiceList[i].showLocP.left >= choiceList[highlightIndex].showLocP.right)
			// Does this choice overlap the highlighted choice's Y coords?
			and (choiceList[i].showLocP.top < choiceList[highlightIndex].showLocP.bottom)
			and (choiceList[i].showLocP.bottom > choiceList[highlightIndex].showLocP.top)
			// Is the choice closer than previous closest?
			and (dword(choiceList[i].showLocP.left - choiceList[highlightIndex].showLocP.left) < closestdist)
			then begin
				closestindex := i;
				closestdist := choiceList[i].showLocP.left - choiceList[highlightIndex].showLocP.left;
			end;
		end;
		if closestindex < choiceCount then HighlightChoice(closestindex);
	end;
end;

procedure TChoicematic.PrintCurrentChoices;
// Prints currently available nodes in the choiceBox. Also prints the parent choice in choicePartBox, if appropriate.
// Sets the initial highlight to the user's most recent choice at this level, if possible.
var autolite : UTF8string;
	i : dword;
	tab : boolean = FALSE;
begin
	highlightIndex := 0;
	// Print parent choices.
	if (selectedNode > 0) and (choicePartBox <> NIL) then with choicePartBox do begin
		PrintNested('\B');
		PrintNested(GetFullChoiceTxt(selectedNode));
		PrintNested('\b\n');
	end;

	Assert(selectedNode < longint(choiceNodeCount));
	Assert(choiceNodes[selectedNode].level < mostRecentChoice.Length);
	autolite := mostRecentChoice[choiceNodes[selectedNode].level];

	// Print the currently available nodes in the choiceBox.
	if choiceBox <> NIL then with choiceBox do
		for i := selectedNode + 1 to choiceNodeCount - 1 do with choiceNodes[i] do
			// Only show choices that are defined, not locked, and have the current node as parent.
			if (choiceTxt <> NIL) and (NOT isLocked) and (parent = dword(selectedNode)) then begin
				if tab then PrintNested('\t');
				tab := TRUE;
				PrintNested('\?');
				PrintNested(choiceTxt);
				PrintNested('\.');
				// Each \? adds an item to the box's displayed choices list. Associate the newest with a choice node.
				with content do choiceList[choiceCount - 1].index := i;
				// Each node has a default most-recently-selected child. Set the initial highlight to this.
				if choiceTxt[activeLanguage] = autolite then highlightIndex := content.choiceCount - 1;
			end;
end;

procedure TChoicematic.SelectChoice(showindex : dword);
// Selects the indicated choice. If further choice levels exist beyond this, prints any sub-choices on the new level.
// Otherwise saves the choice and deactivates choicematic, resuming game flow.
var txt : UTF8string;
	l : dword;
	i : byte;
begin
	Assert(isActive);
	highlightIndex := showindex;

	if choiceBox = NIL then begin
		selectedNode := 0;
		txt := '[choice ' + strdec(showindex) + ']';
		log(txt);
		if sysvar.metaState = EMetaState.Normal then LogTranscript(txt);
		Deactivate(FALSE, clearOnDeactivate);
		exit;
	end;

	with choiceBox.content do begin
		Assert(showindex < choiceCount, strcat('showindex % >= ccount %', [showindex, choiceCount]));
		Assert(choiceList[showindex].index < choiceNodeCount);

		l := choiceList[showindex].index;

		// Remember the selected choice text for later automatic re-highlighting, unless this it's an ad-hoc choice.
		if l <> 0 then begin
			i := choiceNodes[selectedNode].level;
			if dword(i + 1) >= mostRecentChoice.Length then
				setlength(mostRecentChoice, mostRecentChoice.Length shl 1);
			if mostRecentChoice[i] <> choiceNodes[l].choiceTxt[activeLanguage] then begin
				mostRecentChoice[i] := choiceNodes[l].choiceTxt[activeLanguage];
				mostRecentChoice[i + 1] := '';
			end;
		end;

		selectedNode := l;

		if (selectedNode = 0) or (choiceNodes[selectedNode].children = 0) then begin
			// This choice node is ad-hoc or has no children! Finalise selection and deactivate choicematic.
			// Ad-hoc choice strings are not conveniently available, not especially stored outside box content.
			if selectedNode = 0 then
				txt := '[choice ' + strdec(showindex) + ']'
			else begin
				txt := '[' + GetFullChoiceTxt(selectedNode) + ']';
			end;
			log(txt);
			if sysvar.metaState = EMetaState.Normal then LogTranscript(txt);

			Deactivate(FALSE, clearOnDeactivate);
			exit;
		end;

		// This node has children! Print new set of choices.
		if choicePartBox <> NIL then choicePartBox.Clear;
		if highlightBox <> NIL then highlightBox.Clear;
		choiceBox.Clear;
		PrintCurrentChoices;
		if choiceCount = 0 then begin
			LogError('no available subchoices');
			RevertChoice;
			highlightIndex := -1;
		end;
	end;
end;

function TChoicematic.RevertChoice : boolean;
// Backs toward the top choice level, if possible. Returns FALSE if already on top level.
var l : dword;
	i : byte;
begin
	Assert(isActive);
	Assert(dword(selectedNode) < choiceNodeCount);
	result := FALSE;
	if selectedNode = 0 then exit;

	// Remember which choice cancelled from so can default highlight to that if coming back here.
	if choiceBox <> NIL then with choiceBox.content do begin
		Assert(highlightIndex < longint(choiceCount));
		Assert(choiceList[highlightIndex].index < choiceNodeCount);
		i := choiceNodes[selectedNode].level;
		if dword(i + 1) >= mostRecentChoice.Length then
			setlength(mostRecentChoice, mostRecentChoice.Length shl 1);
		l := choiceList[highlightIndex].index;
		if mostRecentChoice[i] <> choiceNodes[l].choiceTxt[activeLanguage] then begin
			mostRecentChoice[i] := choiceNodes[l].choiceTxt[activeLanguage];
			mostRecentChoice[i + 1] := '';
		end;
	end;

	// Back up and print choices.
	Assert(longint(choiceNodes[selectedNode].parent) < selectedNode);
	selectedNode := choiceNodes[selectedNode].parent;
	if choiceBox <> NIL then choiceBox.Clear;
	if choicePartBox <> NIL then choicePartBox.Clear;
	PrintCurrentChoices;
	if (choiceBox <> NIL) and (choiceBox.content.choiceCount = 0) then begin
		LogError('RevertChoice: no available choices');
		Deactivate(TRUE, FALSE);
	end;
	result := TRUE;
end;

procedure TChoicematic.Activate(
	const fiber : TFiber; const _startparent : UTF8string; clearboxes, printchoices, suspendfiber : boolean);
// Prints out choices in a textbox and sets the triggering fiber to rest.
// By default choicematic starts at the root choice level, but can optionally start under an existing choice parent,
// which must be given as a colon-separated string.
// Printchoices can be disabled in the special case where the textbox has already had choices and potentially other
// text printed in it by the game script.
begin
	clearOnDeactivate := clearboxes;
	if clearboxes and printchoices then begin
		if choiceBox <> NIL then choiceBox.Clear;
		if choicePartBox <> NIL then choicePartBox.Clear;
	end;

	selectedNode := 0;
	if _startparent <> '' then begin
		selectedNode := FindChoice(_startparent.Split(':', FALSE));
		if selectedNode = 0 then begin
			LogError('bad choice startparent ' + _startparent);
			log(StateToString);
			selectedNode := 0;
		end;
	end;
	if printchoices then PrintCurrentChoices;

	if (choiceBox <> NIL) and (choiceBox.content.choiceCount = 0) then begin
		log(StateToString);
		LogError('choicematic activate: no choices available');
		exit;
	end;

	isActive := TRUE;
	if (fiber <> NIL) and (suspendfiber) then fiber.fiberState := EFiberState.WaitChoice;
	if highlightIndex < 0 then highlightIndex := 0;
	if (choiceBox <> NIL) and (highlightBox <> NIL) and (NOT choiceBox.contentNeedsRender)
	and (highlightIndex < longint(choiceBox.content.choiceCount)) then
		HighlightChoice(highlightindex, EMoveType.Instant);

	{$ifdef sakucon}
	// The SDL version builds the choice text before displaying it, and so knows each choice's coordinates and can call
	// HighlightChoice whenever. (It is called automatically immediately after box content has been rendered.)
	// However, the console version only prints the text on demand during rendering, and so won't know choice
	// coordinates until after the first render, and can't call HighlightChoice before that.
	// So, while on SDL the on-highlight callback is triggered for the first time in response to an implicit initial
	// HighlightChoice, the console version needs to trigger this explicitly here the first time.
	if onHighlight <> '' then TFiber.Create(onHighlight, '');
	{$endif}
end;

function TChoicematic.AddChoice(
	const txt : TStringBunch; index : longint; const _jumplist, _trackvar : UTF8string; locked : boolean;
	languageindex : byte) : dword;
// Adds the given choice to the current choices list, at the given index, overwriting any existing choice there.
// The txt string array has a string for each language. Each string has colon-separated choice parts.
// If index is negative, ensure parent exists, then use the first free slot or overwrite if choice already exists.
// If index is > 0, parent is created if doesn't exist yet but must be in a lower-numbered index! The given index then
// must be free or this exact choice string.
// Index 0 is root and can't be overwritten.
// If overwriting a choice, existing children relationships are kept, and jumplist and trackvar are kept if new values
// are empty.
// Jumplist must be a colon-separated list of script labels, or empty. Jumplist must be uppercased before calling.
// Trackvar, if not empty, names the script variable whose value indicates which jumplist label to jump to.
// Returns the used index.
var txttable : array of TStringBunch = NIL;
	i : longint;

	procedure _InitNodes(upto : dword);
	// Make room for more choice nodes. Generate empty indexes if new one is beyond list.
	begin
		if upto >= dword(length(choiceNodes)) then setlength(choiceNodes, upto + upto shr 1 + 8);
		while choiceNodeCount <= upto do with choiceNodes[choiceNodeCount] do begin
			choiceTxt := NIL; // fillbyte would not release the memory so clear these individually
			jumpList := NIL;
			trackvar := '';
			fillbyte(choiceNodes[choiceNodeCount], sizeof(TChoiceNode), 0);
			inc(choiceNodeCount);
		end;
	end;

	function _GetFreeIndex : dword;
	begin
		result := 1;
		while (result < choiceNodeCount) and (choiceNodes[result].choiceTxt <> NIL) do
			inc(result);
		if result >= dword(length(choiceNodes)) then
			_InitNodes(result)
		else with choiceNodes[result] do begin
			choiceTxt := NIL; // fillbyte would not release the memory so clear these individually
			jumpList := NIL;
			trackvar := '';
			fillbyte(choiceNodes[result], sizeof(TChoiceNode), 0);
			inc(choiceNodeCount);
		end;
	end;

	procedure _SetChoiceTxt(node : dword; lvl : byte);
	var l : dword;
	begin
		with choiceNodes[node] do begin
			Assert(choiceTxt = NIL, strcat('cnode % already "%"', [node, choiceTxt[0]]));
			setlength(choiceTxt, length(txttable));
			// Txttable currently: [ ["a","b","c"], ["ay","be","see"] ]
			// Extract into choices: ["a","ay"], ["b","be"], ["c","see"]
			for l := high(txttable) downto 0 do
				choiceTxt[l] := txttable[l][txttable[l].Length - lvl];
		end;
	end;

	function _EnsureParent(lvl : byte) : dword;
	var l : dword;
	begin
		result := FindChoice(txttable[activeLanguage], lvl);
		if result <> 0 then exit;

		// This choice level doesn't exist, add it. Parent must exist first!
		l := 0;
		if lvl < txttable[activeLanguage].Length then l := _EnsureParent(lvl + 1);

		result := _GetFreeIndex;
		if l > 0 then inc(choiceNodes[l].children);
		choiceNodes[result].parent := l;
		choiceNodes[result].level := choiceNodes[l].level + 1;
		_SetChoiceTxt(result, lvl);
	end;

begin
	Assert(index <> 0);
	Assert(txt <> NIL);
	Assert(_jumplist = upcase(_jumplist));
	Assert(languageindex < languageList.Length);
	activeLanguage := languageindex;

	// Txt has an "a:b:c" parts string for each language: ["a:b:c","ay:be:see"]
	// This needs to be split: [ ["a","b","c"], ["ay","be","see"] ]
	setlength(txttable, txt.Length);
	for i := high(txt) downto 0 do
		txttable[i] := txt[i].Split(':', FALSE);

	// Ensure parent exists.
	i := 0;
	if txttable[activeLanguage].Length > 1 then i := _EnsureParent(2);

	// If negative index, re-use the index of this exact choice if it already exists, else use a new free index.
	if index < 0 then index := FindChoice(txttable[activeLanguage], 1);
	if index = 0 then index := _GetFreeIndex;
	// If specified index directly, set up space for it.
	if dword(index) >= choiceNodeCount then _InitNodes(index);
	result := index;

	if i >= index then begin
		LogError(strcat('Can''t add [%] at %, parent must exist below', [txt[activeLanguage], index]));
		Log(StateToString);
		exit;
	end;

	// Node being written to must be free or already this exact choice string.
	if (choiceNodes[index].choiceTxt <> NIL) and (NOT EqualsChoice(txttable[activeLanguage], 1, index)) then begin
		LogError(strcat('Can''t add [%] at %, must be empty or identical', [txt[activeLanguage], index]));
		Log(StateToString);
		exit;
	end;

	// Save the choice details. Keep existing child count, jumplist, trackvar.
	with choiceNodes[index] do begin
		if choiceTxt = NIL then begin
			_SetChoiceTxt(index, 1);
			parent := i;
			inc(choiceNodes[i].children);
			level := choiceNodes[i].level + 1;
		end;
		if _jumplist <> '' then jumpList := _jumplist.Split(':', FALSE);
		if _trackvar <> '' then trackVar := _trackvar;
		isLocked := locked;
	end;

	// If choicematic is already active, refresh presented choices.
	if (isActive) and (choiceBox <> NIL) then begin // todo: only if script wants choices printed!
		i := choiceBox.contentWin.scrollOfs;
		choiceBox.Clear;
		PrintCurrentChoices;
		choiceBox.ScrollTo(i, 0, EMoveType.Instant);
		// Will re-highlight automatically.
	end;
end;

procedure TChoicematic.RemoveChoice(index : dword);
// Removes the given choice from the currently available choices list, and all its children. If this is the last child
// of its parent and the parent doesn't have a jumplist or trackvar, the parent is also removed.
// Choice indexes are not shifted as a result, deleted indexes are just marked as unused.
// If choicematic is active, this calls immediately de-activates it.
var i : dword;
begin
	Assert(index < choiceNodeCount);
	Assert(choiceNodes[index].choiceTxt <> NIL, 'RemoveChoice empty txt in ' + strdec(index));
	if isActive then Deactivate(TRUE, FALSE);

	with choiceNodes[index] do begin
		choiceTxt := NIL;
		jumpList := NIL;
		trackVar := '';

		if children <> 0 then
			for i := choiceNodeCount - 1 downto index + 1 do
				if (choiceNodes[i].choiceTxt <> NIL) and (choiceNodes[i].parent = index) then
					RemoveChoice(i);

		Assert(choiceNodes[parent].children > 0);
		dec(choiceNodes[parent].children);
		if (parent <> 0)
		and (choiceNodes[parent].children = 0)
		and (choiceNodes[parent].jumpList = NIL)
		and (choiceNodes[parent].trackVar = '')
		then RemoveChoice(parent);
	end;

	// Crop out all emptied choices from the end of the choice list.
	while (choiceNodeCount > 1) and (choiceNodes[choiceNodeCount - 1].choiceTxt = NIL) do
		dec(choiceNodeCount);
end;

procedure TChoicematic.RemoveChoice(const removetxt : UTF8string);
// Removes the matching choice from the currently available choices list, and all its children.
// Only compares against the active language, case-insensitively. If the input string is empty, clears out the entire
// choice list. If choicematic is active, this calls immediately de-activates it.
// If no existing choice matches, nothing is removed.
var i : longint;
begin
	if isActive then Deactivate(TRUE, FALSE);
	if removetxt = '' then begin
		// Have to clear all choicetxts, or set choice by a high index may re-enable uncleared choices.
		while choiceNodeCount > 1 do begin
			dec(choiceNodeCount);
			choiceNodes[choiceNodeCount].choiceTxt := NIL;
		end;
	end
	else begin
		i := FindChoice(removetxt.Split(':', FALSE));
		if i <> 0 then RemoveChoice(i);
	end;
end;

procedure TChoicematic.LockChoice(const txt : UTF8string; avail : boolean);
// Enables or disables the given choice in choiceNodes. Only compares against the active language, case-insensitively.
// Locking a parent normally means there's no way to select its children; children have their own lock values also.
// An empty input string enables or disables all defined choices. If no matching choice found, does nothing.
// Enables the choice if avail is TRUE, else disables it.
var i : longint;
begin
	if choiceNodeCount < 2 then exit; // nothing to do!
	if isActive then Deactivate(TRUE, FALSE);
	avail := NOT avail;

	if txt = '' then begin
		// Toggle all choices.
		for i := choiceNodeCount - 1 downto 1 do choiceNodes[i].isLocked := avail;
		exit;
	end;

	// Toggle matching choice.
	i := FindChoice(txt.Split(':', FALSE));
	if i <> 0 then choiceNodes[i].isLocked := avail;
end;

function TChoicematic.StateToString : UTF8string;
var i : dword;
begin
	result := strcat('% nodes, active %', [choiceNodeCount, isActive]);
	for i := 0 to choiceNodeCount - 1 do with choiceNodes[i] do
		if choiceTxt <> NIL then
			result := result + strcat('\n%: % (lock=%, "$%" = %)',
				[i, choiceTxt[activeLanguage], isLocked, trackVar, Varmon.GetNumVar(trackVar)]);
end;

function TChoicematic.Serialise : TStringBunch;
var opt : string;
	i, j : dword;
begin
	result := NIL;
	if isActive then while RevertChoice do ; // easiest to save if forced to top level choices
	setlength(result, 5 + choiceNodeCount);
	j := 0;
	// Need to define these early or choice.set won't know what to work with.
	if choiceBox <> NIL then begin
		result[0] := '$_choicebox := `' + choiceBox.boxName + '`';
		inc(j);
	end;
	if choicePartBox <> NIL then begin
		result[j] := '$_choicepartbox := `' + choicePartBox.boxName + '`';
		inc(j);
	end;
	if highlightBox <> NIL then begin
		result[j] := '$_highlightbox := `' + highlightBox.boxName + '`';
		inc(j);
	end;
	if onHighlight <> '' then begin
		result[j] := '$_onhighlight := `' + onHighlight + '`';
		inc(j);
	end;

	if choiceNodeCount > 1 then for i := 1 to choiceNodeCount - 1 do
		with choiceNodes[i] do if choiceTxt <> NIL then begin
			opt := '';
			if jumpList <> NIL then opt := ' label:`' + jumpList.Replace(':','\:').Join(':') + '`';
			if trackVar <> '' then opt := opt + ' var:' + trackVar;
			if isLocked then opt := opt + ' value:1';
			result[j] := strcat('choice.set % `%`%', [i, GetFullChoiceTxt(i), opt]);
			inc(j);
		end;

	if isActive then begin
		result[j] := strcat('choice.get noclear:% noprint:1 nowait:1', [NOT clearOnDeactivate]);
		inc(j);
	end;
	setlength(result, j);
end;

