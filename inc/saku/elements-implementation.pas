{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function TElement.GetCurrentIndex : dword;
// Returns the EleHub.ele[] index that this one currently occupies, or maxuint if not listed.
begin
	result := EleHub.numElements;
	while result <> 0 do begin
		dec(result);
		if EleHub.ele[result] = self then exit;
	end;
	result := high(dword);
end;

procedure TElement.AddRefresh; inline;
begin
	Rendermatic.AddRefresh(
		eleLoc.xp, eleLoc.yp, eleLoc.xp + longint(eleSizeP.w), eleLoc.yp + longint(eleSizeP.h), eleViewport);
end;

procedure TElement.SetParent(const parentname : string31);
// Sets this element's parent to the highest element with the given name, which is in the same viewport. If no such
// parent exists, logs an error and leaves the parent unchanged.
// Parentname must be uppercased before calling.
// Pass an empty parentname to orphan the element.
var i : dword;
begin
	Assert(parentname = upcase(parentname));
	eleParent := NIL;
	if parentname = '' then exit;

	if eleName = parentname then begin
		LogError('element can''t adopt self');
		exit;
	end;

	with EleHub do begin
		for i := numElements - 1 downto 0 do
			if (ele[i].eleName = parentname) and (ele[i].eleViewport = eleViewport) then begin
				eleParent := ele[i];
				exit;
			end;
	end;
	LogError('SetParent: no suitable element "' + parentname + '" to adopt ' + eleName);
end;

procedure TElement.UpdateLocP;
// Recalculates the pixel location of the element based on its 32k coordinates. Refreshes the screen appropriately.
// Doesn't update children. Call this while moving an element. However, if a viewport's size changes, call UpdateSizeP
// instead, since the meta offset depends on the size, and must be added to the final pixel offset.
var x, y : longint;
begin
	if eleIsVisible then AddRefresh;

	with Viewportmatic.viewport[eleViewport] do begin
		x := 0; y := 0;

		// Apply the graphic's constant offset and size multiplier to it, if any.
		if (self is TGob) and ((self as TGob).cachedObject <> NIL) then
			with (self as TGob).cachedObject do begin
				x := origOfsX;
				y := origOfsY;
				if eleSizeMultiplier <> 32768 then begin
					if x >= 0
						then x := (dword(x) * eleSizeMultiplier + 16384) shr 15
						else x := -((dword(-x) * eleSizeMultiplier + 16384) shr 15);
					if y >= 0
						then y := (dword(y) * eleSizeMultiplier + 16384) shr 15
						else y := -((dword(-y) * eleSizeMultiplier + 16384) shr 15);
				end;
			end;

		// Add the script-defined 32k offset and temporary displacement.
		inc(x, eleLoc.x + eleDisplacement.x);
		inc(y, eleLoc.y + eleDisplacement.y);

		// Convert 32k to pixel values.
		if x >= 0
			then eleLoc.xp := (dword(x) * viewportSizeP.w + 16384) shr 15
			else eleLoc.xp := -((dword(-x) * viewportSizeP.w + 16384) shr 15);
		if y >= 0
			then eleLoc.yp := (dword(y) * viewportSizeP.h + 16384) shr 15
			else eleLoc.yp := -((dword(-y) * viewportSizeP.h + 16384) shr 15);

		// Element pixel location is relative to game window, not to parent viewport.
		inc(eleLoc.xp, viewportLoc.leftp);
		inc(eleLoc.yp, viewportLoc.topp);
	end;

	// Apply anchoring displacement. This must be in pixels to align to things pixel-perfectly.
	if (eleAnchor.x or eleAnchor.y) <> 0 then begin
		if eleAnchor.x >= 0 then
			dec(eleLoc.xp, (longint(eleSizeP.w) * eleAnchor.x + 16384) shr 15)
		else
			inc(eleLoc.xp, (longint(eleSizeP.w) * -eleAnchor.x + 16384) shr 15);
		if eleAnchor.y >= 0 then
			dec(eleLoc.yp, (longint(eleSizeP.h) * eleAnchor.y + 16384) shr 15)
		else
			inc(eleLoc.yp, (longint(eleSizeP.h) * -eleAnchor.y + 16384) shr 15);
	end;

	if eleIsVisible then AddRefresh;
	refreshLocP := FALSE;
end;

procedure TElement.UpdateSizeP;
// Recalculates the pixel size of the element based on its graphic's original resolution versus its parent viewport,
// and the element's multiplication factor. Doesn't update children.
// Call this if viewports are changing. This also adds a screen refresh region.
begin
	if eleIsVisible then AddRefresh;

	// Calculate the new pixel size.
	with Viewportmatic.viewport[eleViewport] do begin
		eleSizeP.w := (eleSize.w * viewportSizeP.w + 16384) shr 15;
		eleSizeP.h := (eleSize.h * viewportSizeP.h + 16384) shr 15;
	end;
	// Apply the size multiplier. (32k = 100%)
	if eleSizeMultiplier <> 32768 then begin
		eleSizeP.w := (eleSizeP.w * eleSizeMultiplier + 16384) shr 15;
		eleSizeP.h := (eleSizeP.h * eleSizeMultiplier + 16384) shr 15;
	end;

	if self is TGob then with self as TGob do
		if NOT runtimeDynamic then EnsureCached;

	// Update the location too, it may depend on the new size.
	refreshLocP := TRUE;
end;

procedure TElement.Displace(deltax, deltay : longint);
// Adjusts an element's temporary visual displacement. Applies to children too recursively.
// The values are 32k relative to the element's viewport.
var i : dword;
begin
	inc(eleDisplacement.x, deltax);
	inc(eleDisplacement.y, deltay);
	refreshLocP := TRUE;

	// Move all children by the same amount.
	with EleHub do
		for i := numElements - 1 downto 0 do
			if ele[i].eleParent = self then ele[i].Displace(deltax, deltay);
end;

procedure TElement.SetLocation(x, y, anchorx, anchory : longint);
// Call this to set an existing element's 32k screen position. This also makes sure the element's children are moved
// along. The position may be outside the containing viewport. Refreshes the screen appropriately.
// Caution: if you repeatedly change a parent's anchor, its children's positions suffer accumulating rounding error.
var dx, dy : longint;

	procedure _SetKid(const kid : TElement);
	var j : dword;
	begin
		with kid do begin
			inc(eleLoc.x, dx);
			inc(eleLoc.y, dy);
			refreshLocP := TRUE;
		end;
		with EleHub do
			for j := numElements - 1 downto 0 do
				if ele[j].eleParent = kid then _SetKid(ele[j]);
	end;

var i : dword;
	dax, day : longint;
begin
	dx := x - eleLoc.x;
	dy := y - eleLoc.y;
	eleLoc.x := x;
	eleLoc.y := y;

	dax := anchorx - eleAnchor.x;
	day := anchory - eleAnchor.y;
	if (dax or day) <> 0 then begin
		// Have to apply position delta caused by new anchor position to kids.
		// But kids have their own anchors which do not move, only their positions move.
		if dax >= 0 then
			dec(dx, longint((eleSize.w * dword(dax) + 16384) shr 15))
		else
			inc(dx, longint((eleSize.w * dword(-dax) + 16384) shr 15));
		if day >= 0 then
			dec(dy, longint((eleSize.h * dword(day) + 16384) shr 15))
		else
			inc(dy, longint((eleSize.h * dword(-day) + 16384) shr 15));
	end;
	eleAnchor.x := anchorx;
	eleAnchor.y := anchory;

	if (dx or dy) = 0 then exit;

	refreshLocP := TRUE;

	with EleHub do
		for i := numElements - 1 downto 0 do
			if ele[i].eleParent = self then _SetKid(ele[i]);
end;

procedure TElement.SetSolidBlit(blitcolor : dword);
var i : dword;
begin
	dword(solidBlit) := blitcolor;
	if eleIsVisible then eleRenderNow := TRUE;
	// Also do the kids.
	with EleHub do
		for i := numElements - 1 downto 0 do
			if ele[i].eleParent = self then ele[i].SetSolidBlit(blitcolor);
end;

constructor TElement.Create(const _eleName, _parent : string31;
	inviewport, locx, locy, zlevel, anchorx, anchory, sizex, sizey, index : longint);
// Creates an element, set to be drawn immediately on the next Renderer pass.
// eleName is used to refer to the element in script code.
// Initial 32k x and y coordinates must be set, relative to the containing viewport. A z-level is used to place this
// element relative to other elements in this viewport.
var i : longint;
begin
	Assert(_eleName = upcase(_eleName));
	Assert(_parent = upcase(_parent));
	if inviewport < 0 then inviewport := EleHub.defaultViewport;
	if inviewport >= length(Viewportmatic.viewport) then begin
		LogError(strcat('TElement.Create: bad viewport % for %', [inviewport, _eleName]));
		inviewport := 0;
	end;
	eleViewport := inviewport;

	eleName := _eleName;
	eleParent := NIL;
	eleAnchor.x := anchorx;
	eleAnchor.y := anchory;
	eleLoc.x := locx;
	eleLoc.y := locy;
	eleZLevel := zlevel;
	eleSize.w := sizex;
	eleSize.h := sizey;
	eleSizeMultiplier := 32768;
	eleAlpha := $FF;
	eleRenderNow := TRUE;

	Assert(eleDisplacement.x or eleDisplacement.y = 0);
	Assert(dword(solidBlit) = 0);
	Assert(eleIsVisible = FALSE);
	Assert(refreshLocP = FALSE);

	with EleHub do begin
		// If an element already exists by this same name, the old one is deleted.
		if numElements <> 0 then
			for i := numElements - 1 downto 0 do if (i <> index) and (ele[i].eleName = eleName) then begin
				log(strcat('del existing %:%', [i, eleName]));
				ele[i].Destroy;
				break;
			end;

		// If a suitable index wasn't specified, place the element automatically at the top of its z-level.
		if dword(index) >= numElements then begin
			index := numElements;
			while index <> 0 do begin
				dec(index);
				if ele[index].eleZLevel <= zlevel then begin
					inc(index);
					break;
				end;
			end;
		end;
		// Otherwise an index was specified. Either way, shove elements up one slot and insert the new one.
		InsertInList(self, index);
	end;

	if _parent <> '' then SetParent(_parent);
end;

destructor TElement.Destroy;
// Immediately removes an element and its children.
var i, thisindex : dword;
begin
	if eleIsVisible then AddRefresh;

	with EleHub do if numElements <> 0 then
		for i := numElements - 1 downto 0 do
			if ele[i].eleParent = self then ele[i].Destroy;

	with EffectHub do if fxCount <> 0 then
		for i := fxCount - 1 downto 0 do
			if fx[i].fxElement = self then fx[i].fxElement := NIL;

	Eventmatic.Remove(eleName, TRUE, FALSE);

	thisindex := GetCurrentIndex;
	if thisindex < EleHub.numElements then EleHub.DeleteFromList(thisindex);
	inherited;
end;

// ------------------------------------------------------------------

function TEleHub.GetElementIndex(const name : string31) : dword;
// Returns the index of the lowest existing element with a matching name, or numElements if no match found.
// Names should normally be unique. The comparison is case-sensitive, so uppercase the name before calling.
begin
	result := 0;
	while result < numElements do begin
		if ele[result].eleName = name then exit;
		inc(result);
	end;
end;

function TEleHub.GetElement(const name : string31) : TElement;
// Returns the lowest existing element with a matching name, or NIL if no match found.
// Name must be uppercased before calling.
var i : dword;
begin
	i := 0;
	while i < numElements do begin
		if ele[i].eleName = name then begin result := ele[i]; exit; end;
		inc(i);
	end;
	result := NIL;
end;

procedure TEleHub.InsertInList(newelement : TElement; atindex : dword);
// Places the given element in the element list at the given index, adjusts other elements and references to them
// accordingly.
var i : dword;
begin
	Assert(atindex <= numElements);
	if numElements >= dword(length(ele)) then setlength(ele, length(ele) + length(ele) shr 1 + 12);
	if numElements <> 0 then begin
		for i := numElements - 1 downto atindex do
			ele[i + 1] := ele[i];
	end;

	ele[atindex] := newelement;
	inc(numElements);
end;

procedure TEleHub.DeleteFromList(deleteindex : dword);
// Removes the element at the given index from the element list, adjusts other elements and references to them
// accordingly. Does NOT destroy the removed element object. This should only be called from the TElement destructor!
var i : dword;
begin
	Assert(numElements <> 0);
	Assert(deleteindex < numElements);
	dec(numElements);

	// References to this element elsewhere have already been deleted in the destructor. Now just need to ensure that
	// all element references indexes above this are shifted down by one.
	if deleteindex <> numElements then begin
		for i := deleteindex + 1 to numElements do
			ele[i - 1] := ele[i];
	end;
	ele[numElements] := NIL;

	if numElements + 48 < dword(length(ele)) then setlength(ele, length(ele) - 32);
end;

procedure TEleHub.ReplaceInList(newelement : TElement; index : dword);
// Destroys the element at the given index, places newelement in its place.
var e : TElement;
begin
	Assert(numElements <> 0);
	Assert(index < numElements);
	e := ele[index];
	ele[index] := newelement;
	e.Destroy;
end;

procedure TEleHub.InheritState(const src : TEleHub);
var i : dword;
begin
	setlength(ele, src.numElements + 10);
	numElements := src.numElements;
	defaultViewport := src.defaultViewport;
	if numElements = 0 then exit;
	for i := numElements - 1 downto 0 do
		if src.ele[i] is TGob then
			ele[i] := TGob.Clone(src.ele[i] as TGob)
		else
			log('[!] bad clone type: ' + src.ele[i].eleName);
	// Element references must also shift to current hub.
	for i := numElements - 1 downto 0 do
		if ele[i].eleParent <> NIL then ele[i].SetParent(ele[i].eleParent.eleName);
end;

procedure TEleHub.Reset;
begin
	while numElements <> 0 do ele[numElements - 1].Destroy;
	setlength(ele, 0);
	defaultViewport := 0;
end;

procedure TEleHub.UpdateAll(tickcount : dword); inline;
var i, j, k, l : dword;
	roweventcount, coleventcount : dword;
begin
	Assert(numElements <> 0);
	for i := numElements - 1 downto 0 do begin
		with ele[i] do begin
			if refreshLocP then UpdateLocP;
			if eleRenderNow then begin
				AddRefresh;
				eleRenderNow := FALSE;
				eleIsVisible := TRUE;
			end;
		end;
		if ele[i] is TGob then with ele[i] as TGob do begin
			// Update animation timers, set element to redraw if timer elapsed.
			if (eleIsVisible)
			and (animTimer <> $FFFFFFFF) // element must not be frozen
			then begin

				if (NOT runtimeDynamic) and (cachedObject = NIL) then
					cachedObject := GetGraphicObject(gfxListName, eleSizeP.w, eleSizeP.h);
				if cachedObject.sourceFile = NIL then continue; // loadtime dynamic without backing file can't have anim
				Assert(runtimeDynamic or (length(cachedObject.sourceFile.sequence) <> 0), 'null seq for ' + eleName);

				j := tickcount; roweventcount := 64;
				while animTimer <= j do begin
					dec(j, animTimer);
					animSeqP := (animSeqP + 1) mod dword(length(cachedObject.sourceFile.sequence));
					repeat
						dec(roweventcount);
						if roweventcount = 0 then begin
							LogError('Infinite loop in anim seq? ' + eleName);
							animTimer := $FFFFFFFF;
							break;
						end;
						k := cachedObject.sourceFile.sequence[animSeqP];
						// Get the new frame number.
						l := (k shr 16) and $1FFF;
						case (k shr 16) and $6000 of
							// $2000: if l <= high(gvar) then gobFrame := word(gvar[l]);
							$4000: gobFrame := random(l);
							else gobFrame := l;
						end;
						// Process the command.
						if k and $80000000 <> 0 then begin
							// jump command
							coleventcount := 1;
							animSeqP := gobFrame mod dword(length(cachedObject.sourceFile.sequence));
						end
						else begin
							// Show frame and delay.
							coleventcount := 0;
							gobFrame := gobFrame mod dword(length(cachedObject.framePtr));
							AddRefresh;

							l := k and $FFFF; k := k and $3FFF;
							case l of
								$8000..$BFFF: animTimer := random(k);
								// $C000..$FFFE: if k <= high(gvar) then animTimer := gvar[k];
								$FFFF: animTimer := $FFFFFFFF; // stop here
								else animTimer := k;
							end;
						end;
					until coleventcount = 0;
					RefreshFrameP;
				end;
				if animTimer <> $FFFFFFFF then dec(animTimer, j);
			end;
		end;
	end;
end;

function TEleHub.Serialise : TStringBunch;
var parent : string31;
	i, j : dword;
begin
	result := NIL;
	setlength(result, numElements * 3);
	j := 0;
	if numElements <> 0 then for i := 0 to numElements - 1 do if ele[i] is TGob then with ele[i] as TGob do begin
		Assert((GobType >= low(EGobType)) and (GobType <= high(EGobType)));
		parent := '';
		if eleParent <> NIL then parent := eleParent.eleName;
		result[j] := strcat(
			'show `%` % name:`%` parent:`%` x:% y:% viewport:% frame:% z:% ax:% ay:% w:% h:% alpha:%',
			[gfxListName, strenum(typeinfo(GobType), @GobType), eleName, parent, eleLoc.x, eleLoc.y, eleViewport,
			gobFrame, eleZLevel, eleAnchor.x, eleAnchor.y, eleSize.w, eleSize.h, eleAlpha]);
		inc(j);
		result[j] := strcat('gfx.setsequence `%` %', [eleName, animSeqP]); // needs animTimer too...
		inc(j);
		if dword(solidBlit) <> 0 then begin
			result[j] := strcat('gfx.setsolidblit `%` 0x&', [eleName, solidBlit.ToRGBA4]);
			inc(j);
		end;
	end;
	setlength(result, j);
end;

destructor TEleHub.Destroy;
begin
	Reset;
	inherited;
end;

