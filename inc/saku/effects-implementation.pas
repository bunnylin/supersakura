{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

procedure TEffectHub.Reset;
var i : longint;
begin
	if fxCount <> 0 then begin
		for i := fxCount - 1 downto 0 do fx[i].Destroy;
		setlength(fx, 0);
	end;
	setlength(fx, 16);
	fxCount := 0;
	transitionsActive := 0;
end;

procedure TEffectHub.Update(tickcount : dword);
// Advances the timer on all effects, destroys expired effects. If multiple effects expire in the same time span, their
// expiry order is undefined. (Would have to collect all expired and sort them before destroying.)
var i : dword;
begin
	if fxCount = 0 then exit;
	for i := fxCount - 1 downto 0 do with fx[i] do begin
		inc(age, tickcount);
		if (not infinite) and (age >= ageLimit) then
			Destroy
		else
			Update;
	end;
end;

procedure TEffectHub.RemoveOwnedEffects(const ownerfiber : TFiber);
// Detaches or destroys all effects owned by the given fiber, shifting remaining effects accordingly.
var i : dword;
begin
	Assert(fxCount < dword(length(fx)), strcat('[!] fxcount % > fx-len %', [fxCount, length(fx)]));
	if fxCount = 0 then exit;
	for i := fxCount - 1 downto 0 do with fx[i] do begin
		if fxFiber = ownerfiber then begin
			if destroyOnFiberLoss then
				Destroy
			else
				fxFiber := NIL;
		end;
	end;
end;

procedure TEffectHub.RemoveOwnedEffects(const ownerbox : TTextBox);
// Detaches or destroys all effects attached to the given box, shifting remaining effects accordingly.
var i : dword;
begin
	Assert(fxCount < dword(length(fx)), strcat('[!] fxcount % > fx-len %', [fxCount, length(fx)]));
	if fxCount = 0 then exit;
	for i := fxCount - 1 downto 0 do
		with fx[i] do if fxBox = ownerbox then Destroy;
end;

procedure TEffectHub.RemoveTimed;
// Destroys all timed effects immediately.
var i : dword;
begin
	Assert(fxCount < dword(length(fx)), strcat('[!] fxcount % > fx-len %', [fxCount, length(fx)]));
	if fxCount = 0 then exit;
	for i := fxCount - 1 downto 0 do
		with fx[i] do if NOT infinite then Destroy;
end;

function TEffectHub.Serialise : TStringBunch;
var i : dword;
begin
	result := NIL;
	setlength(result, fxCount);
	if fxCount <> 0 then for i := 0 to fxCount - 1 do result[i] := fx[i].Serialise;
end;

destructor TEffectHub.Destroy;
var i : dword;
begin
	if fxCount <> 0 then
		for i := fxCount - 1 downto 0 do if fx[i] <> NIL then fx[i].Destroy;
	inherited;
end;

// ------------------------------------------------------------------

constructor TEffect.Create(const owningfiber : TFiber; msecs : longint);
begin
	//log(strcat('Creating % as fx[%] (ownerfiber %)', [self.ToString, EffectHub.fxCount, owningfiber <> NIL]));
	with EffectHub do begin
		// Grow the FX array if out of space.
		if fxCount >= dword(length(fx)) then setlength(fx, length(fx) shl 1);
		// Insert the new effect.
		fxIndex := fxCount;
		fx[fxCount] := self;
		inc(fxCount);
	end;
	if owningfiber <> NIL then inc(owningfiber.fxRefCount);
	fxFiber := owningfiber;
	fxBox := NIL;
	fxElement := NIL;
	fxViewport := high(dword);
	age := 0;
	ageLimit := dword(msecs);
	infinite := msecs < 0;
	destroyOnFiberLoss := FALSE;
end;

function TEffect.Serialise : UTF8string;
begin
	result := '';
end;

procedure TEffect.Update;
begin
end;

destructor TEffect.Destroy;
begin
	//log(strcat('Destroying fx[%] % (ownerfiber %)', [fxIndex, self.ToString, fxFiber <> NIL]));
	if fxFiber <> NIL then
		with fxFiber do begin
			dec(fxRefCount);
			if (fxRefCount = 0) and (fiberState = EFiberState.WaitFx) then
				fiberState := EFiberState.Normal;
		end;

	with EffectHub do begin
		dec(fxCount);
		// Grab the largest effect index and put it here in place of the destroyed one, to avoid empty array slots.
		// This does shuffle ongoing effects, but that hasn't been a problem so far.
		if (fxIndex < fxCount) and (fxIndex < dword(length(fx))) and (fxCount < dword(length(fx))) then begin
			fx[fxIndex] := fx[fxCount];
			fx[fxIndex].fxIndex := fxIndex;
		end;
		// Shrink the FX array if there's a lot of free slots and the array's not yet too small.
		if (fxCount shl 1 + 8 < dword(length(fx))) and (length(fx) > 16) then
			setlength(fx, length(fx) shr 1);
	end;
	inherited;
end;

function TEffect.GetProgress(style : EMoveType) : single;
begin
	case style of
		EMoveType.Instant: result := 1.0;
		EMoveType.Linear: result := age / ageLimit;
		EMoveType.Coscos: result := coscos[round(dword(high(coscos)) * age / ageLimit)] / 65535;
		EMoveType.Halfcos: result := (32768 - coscos[round((high(coscos) shr 1) * (ageLimit - age) / ageLimit)]) / 32768;
		else begin
			LogError('bad style: ' + strdec(longint(style)));
			result := 0;
		end;
	end;
end;

// ------------------------------------------------------------------

constructor TEffectBoxMove.Create(
	const owningfiber : TFiber; msecs : longint;
	_box : TTextBox; locx, locy, anchorx, anchory : longint; style : EMoveType);
// Moves a box from its current 32k position to a new 32k position, over a period of msecs. The anchor position can
// also be shifted.
var i : dword;
begin
	inherited Create(owningfiber, msecs);
	Assert(_box <> NIL);

	// Set up the effect.
	fxBox := _box;
	fromLoc.x := _box.boxRect.boxLoc.x;
	fromLoc.y := _box.boxRect.boxLoc.y;
	toLoc.x := locx;
	toLoc.y := locy;
	fromAnchor.x := _box.boxRect.boxAnchor.x;
	fromAnchor.y := _box.boxRect.boxAnchor.y;
	toAnchor.x := anchorx;
	toAnchor.y := anchory;
	moveStyle := style;
	if ageLimit = 0 then moveStyle := EMoveType.Instant;

	// If a move effect on this box is already live, replace it.
	with EffectHub do begin
		if fxCount >= 2 then
			for i := fxCount - 2 downto 0 do
				if (fx[i] is TEffectBoxMove) and (fx[i].fxBox = _box) then begin fx[i].Destroy; break; end;
	end;

	// For instant effects, call update immediately.
	if moveStyle = EMoveType.Instant then Update;
end;

function TEffectBoxMove.Serialise : UTF8string;
begin
	result := strcat('tbox.move `%` x:% y:% ax:% ay:% time:% style:%',
		[fxBox.boxName, toLoc.x, toLoc.y, toAnchor.x, toAnchor.y, ageLimit - age, moveStyle]);
end;

procedure TEffectBoxMove.Update;
var progress, progress_inverse : single;
begin
	progress := GetProgress(moveStyle);
	progress_inverse := 1.0 - progress;
	fxBox.SetLocation(
		round(toLoc.x * progress + fromLoc.x * progress_inverse),
		round(toLoc.y * progress + fromLoc.y * progress_inverse),
		round(toAnchor.x * progress + fromAnchor.x * progress_inverse),
		round(toAnchor.y * progress + fromAnchor.y * progress_inverse));
end;

destructor TEffectBoxMove.Destroy;
begin
	if fxBox <> NIL then fxBox.SetLocation(toLoc.x, toLoc.y, toAnchor.x, toAnchor.y);
	inherited;
end;

// ------------------------------------------------------------------

constructor TEffectBoxScroll.Create(
	const owningfiber : TFiber; msecs : longint; _box : TTextBox; _toofs : longint; style : EMoveType);
// Scrolls a box from its current 32k ofs to a new ofs, over a period of msecs.
begin
	inherited Create(owningfiber, msecs);
	Assert(_box <> NIL);

	// Set up the effect.
	fxBox := _box;
	fromOfs := _box.contentWin.scrollOfs;
	toOfs := _toofs;
	scrollStyle := style;
	if ageLimit = 0 then scrollStyle := EMoveType.Instant;

	// Caller must destroy any live scroll effect on this box before calling.

	// For instant effects, call update immediately.
	if scrollStyle = EMoveType.Instant then Update;
end;

function TEffectBoxScroll.Serialise : UTF8string;
begin
	result := strcat('tbox.scroll `%` y:% time:% style:% force:1',
		[fxBox.boxName, toOfs, ageLimit - age, scrollStyle]);
end;

procedure TEffectBoxScroll.Update;
var progress : single;
begin
	with fxBox do begin
		progress := GetProgress(scrollStyle);
		contentWin.scrollOfs := round(toOfs * progress + fromOfs * (1.0 - progress));
		contentWin.scrollOfsP := round(contentWin.scrollOfs * longint(contentWin.lineHeightP) / 32768);
		finalNeedsRender := TRUE;
	end;
end;

destructor TEffectBoxScroll.Destroy;
begin
	if fxBox <> NIL then with fxBox do begin
		contentWin.scrollOfs := toOfs;
		contentWin.scrollOfsP := round(contentWin.scrollOfs * longint(contentWin.lineHeightP) / 32768);
		finalNeedsRender := TRUE;
	end;
	inherited;
end;

// ------------------------------------------------------------------

constructor TEffectBoxSize.Create(
	const owningfiber : TFiber; msecs : longint; _box : TTextBox; sizex, sizey : dword; style : EMoveType);
// Resized a box from its current 32k size to a new 32k size, over a period of msecs.
// Set sizex or sizey to -1 to not resize that dimension.
var i : dword;
begin
	inherited Create(owningfiber, msecs);
	Assert(_box <> NIL);

	// Set up the effect.
	fxBox := _box;
	with _box do with boxRect do with Viewportmatic.viewport[_box.boxInViewport] do begin
		if parametersNeedUpdate then UpdateParameters; // update and reflow to get paddedSizeP if sizes just changed
		if contentNeedsRender then FlowContent;
		fromSize.w := (paddedSizeP.w shl 15 + (viewportSizeP.w shr 1)) div viewportSizeP.w;
		fromSize.h := (paddedSizeP.h shl 15 + (viewportSizeP.h shr 1)) div viewportSizeP.h;
	end;
	toSize.w := sizex;
	toSize.h := sizey;
	sizeStyle := style;
	if ageLimit = 0 then sizeStyle := EMoveType.Instant;

	// If a size effect on this box is already live, replace it.
	with EffectHub do begin
		if fxCount >= 2 then
			for i := fxCount - 2 downto 0 do
				if (fx[i] is TEffectBoxSize) and (fx[i].fxBox = _box) then begin fx[i].Destroy; break; end;
	end;

	// For instant effects, call update immediately.
	if sizeStyle = EMoveType.Instant then Update;
end;

function TEffectBoxSize.Serialise : UTF8string;
begin
	result := strcat('tbox.setsize `%` w:% h:% time:% style:%',
		[fxBox.boxName, toSize.w, toSize.h, ageLimit - age, sizeStyle]);
end;

procedure TEffectBoxSize.Update;
var progress, progress_inverse : single;
	newsize : TSize32k;
begin
	progress := GetProgress(sizeStyle);
	progress_inverse := 1.0 - progress;
	newsize.w := high(dword);
	newsize.h := high(dword);
	if toSize.w <> high(dword) then newsize.w := round(toSize.w * progress + fromSize.w * progress_inverse);
	if toSize.h <> high(dword) then newsize.h := round(toSize.h * progress + fromSize.h * progress_inverse);
	fxBox.SetSize(newsize.w, newsize.h);
end;

destructor TEffectBoxSize.Destroy;
begin
	if fxBox <> NIL then fxBox.SetSize(toSize.w, toSize.h);
	inherited;
end;

// ------------------------------------------------------------------

constructor TEffectAlphaSlide.Create(const owningfiber : TFiber; msecs : longint; _ele : TElement; _toalpha : byte);
// Timed effect that slides the given element's alpha from its current value to a new value over a duration of msecs.
// 255 = fully opaque, 0 = fully transparent.
// NOTE: if you alpha slide a element that has a child element, trouble ensues. Even if the child is alphaslid the same
// amount simultaneously, the type of alpha blending the old renderer uses makes the child stand out sharply in any
// areas where it overlaps the parent. So before sliding an element's alpha, hide its kids. To fix this, the alpha'ed
// elements plus kids would have to be composited in a separate buffer that later gets blitted to output. Or implement
// the new renderer and its pixel shader approach.
var i : dword;
begin
	inherited Create(owningfiber, msecs);
	Assert(_ele <> NIL);

	// Set up the effect.
	fxElement := _ele;
	fromAlpha := _ele.eleAlpha;
	toAlpha := _toalpha;

	// If a slide effect on this box is already live, replace it.
	with EffectHub do begin
		if fxCount >= 2 then
			for i := fxCount - 2 downto 0 do
				if (fx[i] is TEffectAlphaSlide) and (fxElement = _ele) then begin fx[i].Destroy; break; end;
	end;

	// For instant effects, call update immediately.
	if ageLimit = 0 then begin ageLimit := 1; age := 1; Update; end;
end;

function TEffectAlphaSlide.Serialise : UTF8string;
begin
	result := strcat('gfx.setalpha `%` alpha:% time:%',
		[fxElement.eleName, toAlpha, ageLimit - age]);
end;

procedure TEffectAlphaSlide.Update;
var progress : single;
begin
	if fxElement = NIL then begin Destroy; exit; end;
	with fxElement do begin
		progress := age / ageLimit;
		eleAlpha := round(toAlpha * progress + fromAlpha * (1.0 - progress));
		if eleIsVisible then AddRefresh;
	end;
end;

destructor TEffectAlphaSlide.Destroy;
begin
	if fxElement <> NIL then with fxElement do begin
		eleAlpha := toAlpha;
		if eleIsVisible then AddRefresh;
	end;
	inherited;
end;

// ------------------------------------------------------------------

constructor TEffectBash.Create(
	const owningfiber : TFiber; msecs : longint; _ele : TElement; direction32k, freq32k, amp32k : longint);
// This causes the target element and its children to start oscillating.
// Typically used to replicate the harsh full-screen shake old games enjoyed using, or cause sprites or textboxes to
// shudder, or even simulate a slow-shifting earthquake or other major disturbance.
// This directly temporarily modifies the screen coordinates of an element, which is probably not great...
// Requirements:
// - Must apply to any element, and recursively all its kids
// - Must not affect element's logical coordinates, only a visual displacement
// - Must handle more than one simultaneous bash on the same element
// - Must be performant
// - Should not fail if element has a move effect as well as bash ongoing
//
// Direction: an angle expressed in 32k, where 0 is up, +8k is right, +/- 16k is down, and so on; wraps around.
// Frequency: 16k = 0.5 Hz, 32k = 1 Hz, 64k = 2Hz
// Amplitude: 32k = +/- 100% of viewport size, 64k = +/- 200% of vp size
// Msecs: amplitude shrinks toward 0 over this duration. If msecs <= 0, amplitude remains constant and effect lasts
//   until element is removed.
//
// Multiple bash effects can apply simultaneously to the same element, although you can't consistently achieve
// a perfectly circular motion since this doesn't support offsetting the frequency progression.
//
// An earlier version of this tracked the bash through msec-precise force and inertia calculations, as if the screen
// was attached to the window's center with a rubber band, but that's only useful if there's a constant disrupting
// force acting on the object (such as another object physically smacking on it and sticking together for a bit). Since
// a bash only gave the screen a single directed impulse, it looked identical to a simple oscillator. Using coscos
// looks the same but runs faster.
var max_amp : single;
	i : dword;
begin
	inherited Create(owningfiber, msecs);
	Assert(_ele <> NIL);
	if (freq32k = 0) or (amp32k = 0) then begin
		LogError(strcat('bad bash param: %, %, %', [direction32k, freq32k, amp32k]));
		exit;
	end;
	// Sanity checks.
	direction32k := abs(direction32k mod 32768); // result: angle 0..32767
	if freq32k < 0 then begin
		direction32k := direction32k xor $4000;
		freq32k := abs(freq32k);
	end;
	if amp32k < 0 then begin
		direction32k := direction32k xor $4000;
		amp32k := abs(amp32k);
	end;
	if amp32k > 65535 then amp32k := 65535;

	// Set up the effect.
	fxElement := _ele;
	displacement.x := 0;
	displacement.y := 0;
	rotationsPerMsec := freq32k / 32768000;
	max_amp := amp32k / 32768;

	// Convert the direction to a unit vector.
	// Y component:
	if direction32k >= 16384 then
		i := direction32k - 16384 // down-left-up (16k:+max to 32k:-max) arc
	else
		i := 16384 - direction32k; // up-right-down (0:-max to 16k:+max) arc
	i := (i * dword(high(coscos)) + 8192) shr 14; // scale to 0..high(coscos)
	directionUnitVector.y := (coscos[i] - 32768) / 32768;

	// X component (90 degrees offset):
	direction32k := (direction32k + 8192) and $7FFF;
	if direction32k >= 16384 then
		i := direction32k - 16384 // right-down-left (16k:+max to 32k:-max) arc
	else
		i := 16384 - direction32k; // left-up-right (0:-max to 16k:+max) arc
	i := (i * dword(high(coscos)) + 8192) shr 14; // scale to 0..high(coscos)
	directionUnitVector.x := (coscos[i] - 32768) / 32768;

	directionUnitVector.x := directionUnitVector.x * max_amp;
	directionUnitVector.y := directionUnitVector.y * max_amp;

	//log(strcat('Bash direction=% freq=% amp=% dura=% -> % rotations/msec, % max amp: vector (%, %)',
	//	[direction32k, freq32k, amp32k, msecs, rotationsPerMsec, max_amp, directionUnitVector.x, directionUnitVector.y]));
end;

function TEffectBash.Serialise : UTF8string;
begin
	result := ''; // kind of a problem, current bash displacement isn't preservable...
end;

procedure TEffectBash.Update;
var i : longint;
	fullcoslength : dword;
begin
	if fxElement = NIL then begin Destroy; exit; end;
	// Cancel the previous displacement.
	fxElement.Displace(-displacement.x, -displacement.y);

	// Scale to double the coscos table's length.
	fullcoslength := high(coscos) shl 1;
	// Calculate the current phase, using effect age as the parameter of a cosine^2 curve.
	// The fraction of current rotation is the current phase; shift by a quarter to start upward from middle.
	// Decaying bashes should also increase their oscillation frequency toward the end of the effect, it looks better.
	if NOT infinite then
		i := trunc(frac(age * (rotationsPerMsec + rotationsPerMsec * age / ageLimit) + 0.25) * fullcoslength)
	else
		i := trunc(frac(age * rotationsPerMsec + 0.25) * fullcoslength);

	// Get the coscos value for this phase.
	if i >= length(coscos) then i := fullcoslength - i;

	// Scale the coscos value to -32768..32767.
	i := coscos[i] - 32768;

	// Maximum amplitude decays over time (unless infinite). The decay is nonlinear, so the bash ends less abruptly.
	if NOT infinite then
		i := (i * (ageLimit - age) div ageLimit) * (ageLimit - age) div ageLimit; // (avoid range overflow)

	// Calculate new displacement.
	displacement.x := round(directionUnitVector.x * i);
	displacement.y := round(directionUnitVector.y * i);

	with fxElement do begin
		// If bash values are extreme, range check errors are possible. Cap extreme location values to avoid.
		if eleDisplacement.x + displacement.x > 65535 then
			displacement.x := 65535 - eleDisplacement.x
		else
			if eleDisplacement.x + displacement.x < -65535 then displacement.x := -eleDisplacement.x - 65535;
		if eleDisplacement.y + displacement.y > 65535 then
			displacement.y := 65535 - eleDisplacement.y
		else
			if eleDisplacement.y + displacement.y < -65535 then displacement.y := -eleDisplacement.y - 65535;

		// Place shaken element at its new displaced location.
		Displace(displacement.x, displacement.y);
	end;

	//log(strcat('bash displacement (%, %) at age %/%, amp %', [displacement.x, displacement.y, age, ageLimit, i]));
end;

destructor TEffectBash.Destroy;
begin
	// Cancel the latest applied displacement.
	if fxElement <> NIL then fxElement.Displace(-displacement.x, -displacement.y);
	inherited;
end;

// ------------------------------------------------------------------

constructor TEffectFlash.Create(
	const owningfiber : TFiber; msecs, _count : longint;
	_viewport, _color : dword; _style : EMoveType; z : longint);
begin
	if msecs <= 0 then begin
		msecs := 256; _count := 0;
	end;
	inherited Create(owningfiber, msecs);

	Assert(_viewport < dword(length(Viewportmatic.viewport)));
	fxViewport := _viewport;
	count := dword(_count);
	color := _color;
	fromAlpha := (color and $F) * $11;
	flashtime := ageLimit; // each individual flash takes this long
	infinite := _count <= 0;
	if (ageLimit or count) > $FFFF then
		ageLimit := high(ageLimit)
	else
		ageLimit := ageLimit * count;
	style := _style;

	fxElement := TGob.Create(
		'|' + strhex(color), '_FLASH' + strdec(_viewport) + '_' + strhex(_color) + '_' + strdec(msecs), '',
		EGobType.Overlay, _viewport, 0, 0, z, 0, 0, 32768, 32768);
end;

function TEffectFlash.Serialise : UTF8string;
begin
	result := strcat('(gfx.remove `%`) gfx.flash count:% time:% color:0x& viewport:% style:% z:%',
		[fxElement.eleName, count, flashtime, color, fxViewport, style, fxElement.eleZLevel]);
end;

procedure TEffectFlash.Update;
var progress : single;
begin
	if fxElement = NIL then begin Destroy; exit; end;
	while age >= flashtime do begin
		if NOT infinite then begin
			dec(count);
			if count = 0 then begin Destroy; exit; end;
			//dec(ageLimit, flashtime);
		end;
		dec(age, flashtime);
	end;

	if style <> EMoveType.Instant then begin
		ageLimit := flashtime;
		progress := 1.0 - GetProgress(style);
		if (flashtime or count) > $FFFF then // avoid overflow if duration or count very large
			ageLimit := high(ageLimit)
		else
			ageLimit := flashtime * count;
	end
	else if age + age < flashtime then // instant is a 50-50 on-off pulse wave
		progress := 1.0
	else
		progress := 0;

	fxElement.solidBlit.a := round(fromAlpha * progress);
	fxElement.AddRefresh;
end;

destructor TEffectFlash.Destroy;
begin
	if fxElement <> NIL then begin fxElement.Destroy; fxElement := NIL; end;
	inherited;
end;

// ------------------------------------------------------------------

constructor TEffectMove.Create(
	const owningfiber : TFiber; msecs : longint;
	_ele : TElement; locx, locy, anchorx, anchory : longint; style : EMoveType);
var i : dword;
begin
	inherited Create(owningfiber, msecs);
	Assert(_ele <> NIL);

	// Set up the effect.
	fxElement := _ele;
	fromLoc.x := _ele.eleLoc.x;
	fromLoc.y := _ele.eleLoc.y;
	toLoc.x := locx;
	toLoc.y := locy;
	fromAnchor := _ele.eleAnchor;
	toAnchor.x := anchorx;
	toAnchor.y := anchory;
	moveStyle := style;
	if ageLimit = 0 then moveStyle := EMoveType.Instant;

	// If a move effect on this box is already live, replace it.
	with EffectHub do begin
		if fxCount >= 2 then
			for i := fxCount - 2 downto 0 do
				if (fx[i] is TEffectMove) and (fx[i].fxElement = _ele) then begin fx[i].Destroy; break; end;
	end;

	// For instant effects, call update immediately.
	if moveStyle = EMoveType.Instant then Update;
end;

constructor TEffectMove.Create(
	const owningfiber : TFiber; msecs : longint; _ele : TElement; locx, locy : longint; style : EMoveType);
begin
	Assert(_ele <> NIL);
	Create(owningfiber, msecs, _ele, locx, locy, _ele.eleAnchor.x, _ele.eleAnchor.y, style);
end;

function TEffectMove.Serialise : UTF8string;
begin
	result := strcat('gfx.move `%` x:% y:% ax:% ay:% time:% style:%',
		[fxElement.eleName, toLoc.x, toLoc.y, toAnchor.x, toAnchor.y, ageLimit - age, moveStyle]);
end;

procedure TEffectMove.Update;
var progress, progress_inverse : single;
	x, y, ax, ay : longint;
begin
	if fxElement = NIL then begin Destroy; exit; end;
	with fxElement do begin
		progress := GetProgress(moveStyle);
		progress_inverse := 1.0 - progress;
		x := round(toLoc.x * progress + fromLoc.x * progress_inverse);
		y := round(toLoc.y * progress + fromLoc.y * progress_inverse);
		ax := round(toAnchor.x * progress + fromAnchor.x * progress_inverse);
		ay := round(toAnchor.y * progress + fromAnchor.y * progress_inverse);

		SetLocation(x, y, ax, ay);
	end;
end;

destructor TEffectMove.Destroy;
begin
	if fxElement <> NIL then
		fxElement.SetLocation(toLoc.x, toLoc.y, toAnchor.x, toAnchor.y);
	inherited;
end;

// ------------------------------------------------------------------

constructor TEffectSleep.Create(const owningfiber : TFiber; msecs : longint);
begin
	inherited Create(owningfiber, msecs);
	if owningfiber = NIL then begin
		LogError('sleep without fiber');
		ageLimit := 0;
		exit;
	end;
	owningfiber.fiberState := EFiberState.WaitSleep;
	destroyOnFiberLoss := TRUE;
end;

function TEffectSleep.Serialise : UTF8string;
begin
	result := strcat('sleep `%` time:%', [fxFiber.fiberName, ageLimit - age]);
end;

destructor TEffectSleep.Destroy;
begin
	if (fxFiber <> NIL) and (fxFiber.fiberState = EFiberState.WaitSleep) then
		fxFiber.fiberState := EFiberState.Normal;
	inherited;
end;

// ------------------------------------------------------------------

constructor TEffectTransition.Create(
	const owningfiber : TFiber; msecs : longint; _viewport : dword; style : ETransitionType);
var i, j, y : dword;
	r : byte;
begin
	inherited Create(owningfiber, msecs);
	inc(EffectHub.transitionsActive);
	if _viewport >= dword(length(Viewportmatic.viewport)) then begin
		LogError('bad viewport: ' + strdec(_viewport));
		ageLimit := 0;
		exit;
	end;
	// Hack: loading an asset takes time. If we start counting the transition age before the transitioning-in assets
	// are loaded, part of the transition is noticeably skipped as the first asset draw is delayed by potentially
	// several hundred msec.
	// The asset load from disk and resize are done as soon as gfx.show is called, which likely happens immediately
	// after this transition create call. Therefore the current frame's duration will be longer than expected.
	// Transition update is called during the current frame, but it will see the normal short tickcount from the
	// previous frame. However, the next frame start reveals a long tickcount, so the *second* effect update will thus
	// experience a sudden jump in age.
	// To avoid this, mark the transition as an infinite effect here to keep it going no matter its age, and use
	// startDelay to count updates. On the second update, clear infinite and start counting the actual effect age.
	infinite := TRUE;
	startDelay := 2;

	// Set up the effect.
	fxViewport := _viewport;
	transitionStyle := style;

	y := Rendermatic.StashRender(_viewport, data);

	if style = ETransitionType.Instant then
		ageLimit := 0
	else if style = ETransitionType.RaggedWipe then with Viewportmatic.viewport[_viewport] do begin
		// Calculate the maximum soft edge width.
		i := viewportSizeP.w shr 2 + 1;
		// Stash the transition width, with soft edge allowance.
		width := viewportSizeP.w + i;

		// Build a raggedness list. This assigns a different soft edge width to each row.
		// Find the largest integer square root of the maximum soft edge width.
		j := 1;
		while j * j <= i do inc(j);
		dec(j);
		// Build the list. This is stored at the end of the effect's image stash buffer.
		i := y;
		for y := viewportSizeP.h - 1 downto 0 do begin
			r := byte(random(j) + 1);
			word((data + i)^) := r * r;
			inc(i, 2);
		end;
	end;

	// Tell the graphics cacher that previously visible graphics are probably not needed anymore and may be freed if
	// cache space is low. Then, re-getting all current elements ensures those will remain in memory.
	ReleaseGfx;
	with EleHub do if numElements <> 0 then
		for i := numElements - 1 downto 0 do
			if ele[i] is TGob then with ele[i] as TGob do
				if NOT runtimeDynamic then
					EnsureCached;
end;

function TEffectTransition.Serialise : UTF8string;
begin
	result := ''; // can't carry over stashed viewport bitmap, so transitions finish immediately
end;

procedure TEffectTransition.Update;
begin
	// Because many transition effects are drawn differently based on time and location, the transition render cannot
	// be easily split into separate refresh rectangles. Due to the way refresh areas are merged and split, the only
	// way to be sure the transition effect is atomic is to refresh the whole viewport every frame.
	Viewportmatic.viewport[fxViewport].AddRefresh;
	// Hack: Only start counting effect age from the second effect update onward.
	if infinite then begin
		dec(startDelay);
		age := 0;
		if startDelay = 0 then begin
			infinite := FALSE;
			age := preference.restTime; // pretend the second frame's tickcount was the perfect desired length
			if age > ageLimit then Destroy;
		end;
	end;
end;

destructor TEffectTransition.Destroy;
begin
	if data <> NIL then begin freemem(data); data := NIL; end;
	dec(EffectHub.transitionsActive);
	with Viewportmatic do if fxViewport < dword(length(viewport)) then viewport[fxViewport].AddRefresh;
	inherited;
end;

