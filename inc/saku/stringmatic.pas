{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

var totalUntranslated : dword;

procedure TranslateExternal;
begin
end;

function AutoTranslate(srcstring : UTF8string; srclanguage : byte) : UTF8string;
begin
	result := srcstring + 'nyan' + strdec(srclanguage); // tbd
end;

function GetStringGlobal(const s : UTF8string; _labellanguage : byte) : TStringBunch;
// Finds the given string in the given language in the global string table, returns the whole bunch duly translated.
var i, j : dword;
begin
	Assert(_labellanguage < languageList.Length);
	Assert(length(scriptObjects) <> 0);
	result := NIL;
	setlength(result, languageList.Length);
	Assert(result[0] = '');

	if (s <> '') and (globalStringCount <> 0) then with scriptObjects[0] do begin
		i := FindGlobalString(s);
		if i < globalStringCount then with stringTable[i] do begin
			// Found it!
			Assert((txt.Length <> 0) and (txt.Length <= languageList.Length + 1));
			Assert(txt[0] = s);
			// If canonical language is not 0 (English) and first content string is empty, generate a translation!
			if (preference.autotrans.internalEnabled or preference.autotrans.externalEnabled)
			and (_labellanguage <> 0) and (txt[1] = '') then
				txt[1] := AutoTranslate(s, _labellanguage);
			// Copy the strings to a new bunch without the ID column.
			for j := 1 to high(txt) do
				result[j - 1] := txt[j];
		end;
	end;

	// Ensure the canonical language fills in for any remaining empty index.
	for i := high(result) downto 0 do if result[i] = '' then result[i] := s;
end;

function GetStringUnique(const srclabel : TScriptObject; id : dword) : TStringBunch;
// Finds the given canonical string by id in the given script's string table, returns the whole bunch duly translated.
// If the requested string is out of bounds, returns a set of empty strings.
var i : dword;
begin
	with srclabel do if id < dword(length(stringTable)) then with stringTable[id] do begin
		Assert(txt.Length <> 0);
		// If canonical language is not 0 (English) and first content string is empty, generate a translation!
		if (preference.autotrans.internalEnabled or preference.autotrans.externalEnabled)
		and (labelLanguage <> 0) and (txt[0] = '') then
			txt[0] := AutoTranslate(txt[labelLanguage], labelLanguage);
		// Ensure the canonical language temporarily fills in any remaining empty index.
		result := copy(txt);
		for i := high(result) downto 0 do if result[i] = '' then result[i] := result[labelLanguage];
		exit;
	end;
	Log('[!] unique string not found: ' + srclabel.labelName + '.' + strdec(id));
	result := NIL;
	setlength(result, languageList.Length);
	Assert(result[0] = '');
end;

procedure CountUntranslated;
// Fills in the totalUntranslated value by counting every string where language 0 is empty.
// It would be faster to do this directly during import, same as totalStringCount, but the logic gets a bit messy
// particularly with unique strings. An extra iteration after DAT load here keeps things maintainable.
var i, j : dword;
begin
	totalUntranslated := 0;
	if length(scriptObjects) < 2 then exit;
	for i := high(scriptObjects) downto 1 do with scriptObjects[i] do
		if length(stringTable) <> 0 then
			for j := high(stringTable) downto 0 do with StringTable[j] do
				if (txt.Length <> 0) and (txt[0] = '') then inc(totalUntranslated);
	Log(strcat('untranslated strings %/%', [totalUntranslated, totalStringCount]));
end;

