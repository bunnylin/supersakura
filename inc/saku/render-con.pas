{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// SuperSakura console-specific rendering functions.

type LXYtriplet = packed record
	luma : byte; x, y : longint;
end;

function DiffRGBX(c1, c2 : RGBAquad) : dword;
// Returns the squared difference between two srgb32 RGBX colors, weighed in favor of green.
var b, g, r : byte;
begin
	b := abs(c1.b - c2.b);
	g := abs(c1.g - c2.g);
	r := abs(c1.r - c2.r);
	result := b * b * 2 + g * g * 4 + r * r * 3;
end;

function DiffLXYluma(c1, c2 : LXYtriplet) : dword;
// Returns the squared difference between two LXY colors, weighed in favor of luma.
var l, x, y : dword;
begin
	l := abs(c1.luma - c2.luma);
	x := abs(c1.x - c2.x);
	y := abs(c1.y - c2.y);
	result := l * l * 3 + (x * x + y * y) * 2;
end;

function DiffLXYchroma(c1, c2 : LXYtriplet) : dword;
// Returns the squared difference between two LXY colors, weighed in favor of chroma.
var l, x, y : dword;
begin
	l := abs(c1.luma - c2.luma);
	x := abs(c1.x - c2.x);
	y := abs(c1.y - c2.y);
	result := l * l * 2 + (x * x + y * y) * 3;
end;

procedure TRendermatic.InitPalette;
// Creates a lookup palette for converting 24-bit RGB colors to 16-color ASCII approximations.
// Since consoles' 16-color palettes are so granular, there's no point doing a lookup table for all 16 million colors in the
// 24-bit color space. Instead, we'll cut the bitdepth in half and restrict the input to 4 bits per channel, so the lookup
// table only needs 16*16*16 = 4096 entries.

	function _RGBtoLXY(b, g, r : byte) : LXYtriplet;
	// This converts an sRGB value to a kind of YCH space, great for perceptual comparisons. The first component is L', but the
	// C and H are further transformed to x and y coordinates on a hue/saturation hexagon.
	// On a hexagon with red on the right, green top left, and blue lower left:
	// red   =  1.0, 0.0	=  255, 0
	// green = -0.5, 0.866  = -128, 221
	// blue  = -0.5, -0.866 = -128, -221
	var multiplier, inverse : single;
		c : byte;
		{$push}{$scopedEnums off}
		msc : (red, green, blue);
		{$pop}
		mscval, lscval : byte;
	begin
	// This converts sRGB to BT.709 L'. Both in and out are in range 0..255.
	result.luma := (6966 * r + 23436 * g + 2366 * b + 16383) shr 15;

	// Greyscale?
	if (r = g) and (r = b) then begin
		result.x := 0; result.y := 0;
		exit;
	end;

	// Get chroma/saturation, where in-range 0..255, out-range 1..255.
	msc := red; mscval := r;
	if g > r then begin
		msc := green; mscval := g;
	end;
	if b > mscval then begin
		msc := blue; mscval := b;
	end;
	lscval := r;
	if g < r then lscval := g;
	if b < lscval then lscval := b;
	c := mscval - lscval;

	// Translate hue into x and y coordinates, range -255..255.
	case msc of
		red:
		if g > b then begin
			// red-yellow
			multiplier := (g - b) / c;
			inverse := 1 - multiplier;
			result.x := round(255 * inverse + 128 * multiplier);
			result.y := round(0 * inverse + 221 * multiplier);
		end
		else begin
			// red-magenta
			multiplier := (b - g) / c;
			inverse := 1 - multiplier;
			result.x := round(255 * inverse + 128 * multiplier);
			result.y := round(0 * inverse + -221 * multiplier);
		end;

		green:
		if r > b then begin
			// green-yellow
			multiplier := (r - b) / c;
			inverse := 1 - multiplier;
			result.x := round(-128 * inverse + 128 * multiplier);
			result.y := 221;
		end
		else begin
			// green-cyan
			multiplier := (b - r) / c;
			inverse := 1 - multiplier;
			result.x := round(-128 * inverse + -255 * multiplier);
			result.y := round(221 * inverse + 0 * multiplier);
		end;

		blue:
		if g > r then begin
			// blue-cyan
			multiplier := (g - r) / c;
			inverse := 1 - multiplier;
			result.x := round(-128 * inverse + -255 * multiplier);
			result.y := round(-221 * inverse + 0 * multiplier);
		end
		else begin
			// blue-magenta
			multiplier := (r - g) / c;
			inverse := 1 - multiplier;
			result.x := round(-128 * inverse + 128 * multiplier);
			result.y := -221;
		end;
	end;

	// Apply the saturation as a multiplier toward 0,0.
	with result do begin
		if x >= 0 then
			x := (x * c + 128) div 255
		else
			x := (x * c - 128) div 255;
		if y >= 0 then
			y := (y * c + 128) div 255
		else
			y := (y * c - 128) div 255;
	end;
end;

// Reference font brightness (set pixels per glyph), normalised, sorted: see doc/asciiluma.ods
// Gradient that looks fine on any font:
// ....::::((((cccctttt2222pppp8888@@@@
// ....::::((((cccctttt2222pppp8888@@@@
// ....::::((((cccctttt2222pppp8888@@@@
// ....::::((((cccctttt2222pppp8888@@@@
const maxcover = 0.8; // even the @ doesn't actually give 100% pixel coverage
var asciigradient : array[0..8] of record
	ch : char;
	cover : single;
end = (
(ch: '.'; cover: 0.1138 * maxcover),
(ch: ':'; cover: 0.2276 * maxcover),
(ch: '('; cover: 0.3616 * maxcover),
(ch: 'c'; cover: 0.4516 * maxcover),
(ch: 't'; cover: 0.5202 * maxcover),
(ch: '2'; cover: 0.6270 * maxcover),
(ch: 'p'; cover: 0.7430 * maxcover),
(ch: '8'; cover: 0.8215 * maxcover),
(ch: '@'; cover: 1.0000 * maxcover));

const numcombos = length(asciigradient) * 16;

	procedure _MakeRGBPalette;
	var RGBwritep, linearwritep, readp1, readp2 : pointer;
		lut_palcharRGB : array[0..(numcombos * 3)] of byte; // high end can't use -1 due to optimisation
		lut_palcharlinear : array[0..(numcombos * 3) - 1] of word;
		lut_ditheredRGB : array[0..(numcombos * numcombos * 3)] of byte;
		mycol : RGBAquad;
		score, bestscore : dword;
		i, j, nearest : dword;
		l : word;
		ch, rr, gg, bb : byte;
	begin
		// Build the sRGB and linear RGB lookup tables for each console palette index + character.
		RGBwritep := @lut_palcharRGB[0];
		linearwritep := @lut_palcharlinear[0];
		for ch := 0 to high(asciigradient) do for i := 0 to 15 do begin
			l := round(lut_SRGBtoLinear[crtpalette[i].b] * asciigradient[ch].cover);
			word(linearwritep^) := l; inc(linearwritep, 2);
			byte(RGBwritep^) := lut_LineartoSRGB[l]; inc(RGBwritep);

			l := round(lut_SRGBtoLinear[crtpalette[i].g] * asciigradient[ch].cover);
			word(linearwritep^) := l; inc(linearwritep, 2);
			byte(RGBwritep^) := lut_LineartoSRGB[l]; inc(RGBwritep);

			l := round(lut_SRGBtoLinear[crtpalette[i].r] * asciigradient[ch].cover);
			word(linearwritep^) := l; inc(linearwritep, 2);
			byte(RGBwritep^) := lut_LineartoSRGB[l]; inc(RGBwritep);
		end;

		// Build the lookup table of all 50-50 dithered combinations of the above.
		RGBwritep := @lut_ditheredRGB[0];
		readp1 := @lut_palcharlinear[0];
		for i := numcombos - 1 downto 0 do begin

			readp2 := @lut_palcharlinear[0];
			for j := numcombos - 1 downto 0 do begin
				byte(RGBwritep^) := lut_LineartoSRGB[(word(readp1^) + word(readp2^)) shr 1];
				inc(readp2, 2); inc(RGBwritep);
				byte(RGBwritep^) := lut_LineartoSRGB[(word((readp1 + 2)^) + word(readp2^)) shr 1];
				inc(readp2, 2); inc(RGBwritep);
				byte(RGBwritep^) := lut_LineartoSRGB[(word((readp1 + 4)^) + word(readp2^)) shr 1];
				inc(readp2, 2); inc(RGBwritep);
			end;

			inc(readp1, 6);
		end;

		// Fill in the 4-bit sRGB to ASCII conversion lookup table.
		nearest := 0;
		for rr := 0 to 15 do for gg := 0 to 15 do for bb := 0 to 15 do begin
			// Convert the 4-bit sRGB lookup index to 8-bit sRGB.
			mycol.b := bb * 17; // * 255/15
			mycol.g := gg * 17;
			mycol.r := rr * 17;

			// Find the nearest flat palette index to this color.
			bestscore := $FFFFFFFF;
			for i := 15 downto 0 do begin
				j := crtpalette[i].b + crtpalette[i].g shl 8 + crtpalette[i].r shl 16;
				score := DiffRGBX(mycol, RGBAquad(j));
				if score < bestscore then begin
					nearest := i;
					bestscore := score;
				end;
			end;
			palette[rr][gg][bb].flatcolor := nearest;

			// Find the nearest palette index + character to this color.
			readp1 := @lut_palcharRGB[0];
			bestscore := $FFFFFFFF;
			for i := numcombos - 1 downto 0 do begin
				score := DiffRGBX(mycol, RGBAquad(readp1^));
				if score < bestscore then begin
					nearest := i;
					bestscore := score;
				end;
				inc(readp1, 3);
			end;
			j := numcombos - 1 - nearest;
			with palette[rr][gg][bb] do begin
				color1 := j and $F;
				char1 := asciigradient[j shr 4].ch;
			end;

			// Check which 50-50 dither mix (including with itself) results in the best match.
			readp1 := @lut_ditheredRGB[0] + j * numcombos * 3;
			for i := numcombos - 1 downto 0 do begin
				score := DiffRGBX(mycol, RGBAquad(readp1^));
				if score <= bestscore then begin
					nearest := i;
					bestscore := score;
				end;
				inc(readp1, 3);
			end;
			j := numcombos - 1 - nearest;
			with palette[rr][gg][bb] do begin
				// Keep pairs in consistent order to avoid dither banding.
				if (color2 < color1) or ((color2 = color1) and (char2 < char1)) then begin
					color2 := color1; char2 := char1;
					color1 := j and $F;
					char1 := asciigradient[j shr 4].ch;
				end
				else begin
					color2 := j and $F;
					char2 := asciigradient[j shr 4].ch;
				end;
			end;
		end;
	end;

	procedure _MakeLXYPalette;
	var LXYwritep, linearwritep, readp1, readp2 : pointer;
		lut_palcharLXY : array[0..(numcombos * 9)] of byte; // high end can't use -1 due to optimisation
		lut_palcharlinear : array[0..(numcombos * 3) - 1] of word;
		lut_ditheredLXY : array[0..(numcombos * numcombos * 9)] of byte;
		_Differ : Function(c1, c2 : LXYtriplet) : dword;
		mycol : LXYtriplet;
		score, bestscore : dword;
		i, j, nearest : dword;
		l : word;
		ch, rr, gg, bb : byte;
	begin
		if sakuparam.palettemode = EPaletteMode.LXYChroma then
			_Differ := @DiffLXYchroma
		else
			_Differ := @DiffLXYluma;

		// Build the linear RGB and LXY lookup tables for each console palette index + character.
		LXYwritep := @lut_palcharLXY[0];
		linearwritep := @lut_palcharlinear[0];
		for ch := 0 to high(asciigradient) do for i := 0 to 15 do begin
			l := round(lut_SRGBtoLinear[crtpalette[i].b] * asciigradient[ch].cover);
			word(linearwritep^) := l; inc(linearwritep, 2);
			bb := lut_LineartoSRGB[l];

			l := round(lut_SRGBtoLinear[crtpalette[i].g] * asciigradient[ch].cover);
			word(linearwritep^) := l; inc(linearwritep, 2);
			gg := lut_LineartoSRGB[l];

			l := round(lut_SRGBtoLinear[crtpalette[i].r] * asciigradient[ch].cover);
			word(linearwritep^) := l; inc(linearwritep, 2);
			rr := lut_LineartoSRGB[l];

			LXYtriplet(LXYwritep^) := _RGBtoLXY(bb, gg, rr);
			inc(LXYwritep, sizeof(LXYtriplet));
		end;

		// Build the lookup table of all 50-50 dithered combinations of the above.
		LXYwritep := @lut_ditheredLXY[0];
		readp1 := @lut_palcharlinear[0];
		for i := numcombos - 1 downto 0 do begin

			readp2 := @lut_palcharlinear[0];
			for j := numcombos - 1 downto 0 do begin
				bb := lut_LineartoSRGB[(word(readp1^) + word(readp2^)) shr 1]; inc(readp2, 2);
				gg := lut_LineartoSRGB[(word((readp1 + 2)^) + word(readp2^)) shr 1]; inc(readp2, 2);
				rr := lut_LineartoSRGB[(word((readp1 + 4)^) + word(readp2^)) shr 1]; inc(readp2, 2);

				LXYtriplet(LXYwritep^) := _RGBtoLXY(bb, gg, rr);
				inc(LXYwritep, sizeof(LXYtriplet));
			end;

			inc(readp1, 6);
		end;

		// Fill in the 4-bit sRGB to ASCII conversion lookup table.
		nearest := 0;
		for rr := 0 to 15 do for gg := 0 to 15 do for bb := 0 to 15 do begin
			// Convert the 4-bit sRGB lookup index to 8-bit sRGB, then to LXY.
			mycol := _RGBtoLXY(bb * 17, gg * 17, rr * 17);

			// Find the nearest flat palette index to this color.
			bestscore := $FFFFFFFF;
			for i := 15 downto 0 do begin
				score := _Differ(mycol, _RGBtoLXY(crtpalette[i].b, crtpalette[i].g, crtpalette[i].r));
				if score < bestscore then begin
					nearest := i;
					bestscore := score;
				end;
			end;
			palette[rr][gg][bb].flatcolor := nearest;

			// Find the nearest palette index + character to this color.
			readp1 := @lut_palcharLXY[0];
			bestscore := $FFFFFFFF;
			for i := numcombos - 1 downto 0 do begin
				score := _Differ(mycol, LXYtriplet(readp1^));
				if score < bestscore then begin
					nearest := i;
					bestscore := score;
				end;
				inc(readp1, sizeof(LXYtriplet));
			end;
			j := numcombos - 1 - nearest;
			with palette[rr][gg][bb] do begin
				color1 := j and $F;
				char1 := asciigradient[j shr 4].ch;
			end;

			// Check which 50-50 dither mix (including with itself) results in the best match.
			readp1 := @lut_ditheredLXY[0] + j * numcombos * sizeof(LXYtriplet);
			for i := numcombos - 1 downto 0 do begin
				score := _Differ(mycol, LXYtriplet(readp1^));
				if score <= bestscore then begin
					nearest := i;
					bestscore := score;
				end;
				inc(readp1, sizeof(LXYtriplet));
			end;
			j := numcombos - 1 - nearest;
			with palette[rr][gg][bb] do begin
				// Keep pairs in consistent order to avoid dither banding.
				if (color2 < color1) or ((color2 = color1) and (char2 < char1)) then begin
					color2 := color1; char2 := char1;
					color1 := j and $F;
					char1 := asciigradient[j shr 4].ch;
				end
				else begin
					color2 := j and $F;
					char2 := asciigradient[j shr 4].ch;
				end;
			end;
		end;
	end;

var p : byte;
begin
	log(strcat('Palette mode: %', [sakuparam.palettemode]));
	if sakuparam.palettemode = EPaletteMode.Truecolor then exit;

	// Acquire the exact RGB palette used by our console, if possible. Assume default palette for non-responding terminals.
	GetConsolePalette;
	for p := 0 to 15 do
		log(strcat('%: &&&', [p, crtpalette[p].r, crtpalette[p].g, crtpalette[p].b]));

	if sakuparam.palettemode = EPaletteMode.RGB then
		_MakeRGBPalette
	else
		_MakeLXYPalette;
end;

// The Ascii Blitzer needs to run fast, so it has platform-specific code. Bear in mind SuperSakura does frequent full-screen
// transitions. On Windows only the WriteConsoleOutput command is fast enough; trying to write things one character at a time
// literally takes a second to do the full console. Whereas *nix terminals by design only accept input one character at a time,
// but at least are optimised to do it fast. (Some terminal emulators are vastly slower than others, however.)
// So, the WinBlitz builds a char/color buffer in memory and blits it in one go, while the NixBlitz builds a string of
// characters and color escape codes and prints that one line at a time.
{$ifdef WINDOWS}
var asciiBuf : array of dword;
{$endif}
procedure TRendermatic.BlitzAscii(x1, y1, x2, y2 : longint);
var srcp, writep, endp : pointer;
	thispal : PPaletteEntry;
	outbuf : string;
	workbuf : string[20];
	x, y : longint;
	b, g, r : byte;
	thiscolor : byte = 0; // silence compiler
	thischar : char = ' ';

	procedure _TrueBlitz; inline;
	begin
		endp := @outbuf[230];
		y := y1;
		while y < y2 do begin
			GotoXY(x1, y);
			srcp := outputBuffy + (y * longint(sysvar.windowSize.w) + x1) shl 2;
			writep := @outbuf[1];

			x := x1;
			while x < x2 do begin
				b := byte(srcp^); inc(srcp);
				g := byte(srcp^); inc(srcp);
				r := byte(srcp^); inc(srcp, 2);
				workbuf := chr(27) + '[48;2;' + strdec(r) + ';' + strdec(g) + ';' + strdec(b) + 'm ';
				move(workbuf[1], writep^, length(workbuf));
				inc(writep, length(workbuf));

				if writep > endp then begin
					byte(outbuf[0]) := writep - @outbuf[1];
					write(outbuf);
					writep := @outbuf[1];
				end;
				inc(x);
			end;

			byte(outbuf[0]) := writep - @outbuf[1];
			write(outbuf);
			inc(y);
		end;
	end;

	procedure _GetPalette;
	begin
		b := byte(srcp^); inc(srcp);
		g := byte(srcp^); inc(srcp);
		r := byte(srcp^); inc(srcp, 2);
		if (b or g or r) = 0 then begin
			thiscolor := 0;
			thischar := ' ';
			exit;
		end;

		thispal := @palette[r shr 4][g shr 4][b shr 4];
		if (x xor y) and 1 = 0 then begin
			thiscolor := thispal^.color1;
			thischar := thispal^.char1;
		end
		else begin
			thiscolor := thispal^.color2;
			thischar := thispal^.char2;
		end;
	end;

{$ifdef WINDOWS}
var sx, sy, cellcount : dword;
begin
	if (x1 >= x2) or (y1 >= y2) then exit; // safety
	if sakuparam.palettemode = EPaletteMode.Truecolor then begin _TrueBlitz; exit; end;

	sx := x2 - x1; sy := y2 - y1;
	cellcount := sx * sy;
	if cellcount >= dword(length(asciiBuf)) then begin
		setlength(asciiBuf, 0);
		setlength(asciiBuf, cellcount);
	end;
	writep := @asciiBuf[0];
	for y := y1 to y2 - 1 do begin
		srcp := outputBuffy + (y * longint(sysvar.windowSize.w) + x1) shl 2;
		for x := x1 to x2 - 1 do begin
			_GetPalette;
			dword(writep^) := byte(thischar) + thiscolor shl 16;
			inc(writep, 4);
		end;
	end;
	CrtWriteConOut(@asciiBuf[0], sx, sy, x1, y1, x2, y2);
end;
{$else}
var lastcolor : byte;
begin
	if (x1 >= x2) or (y1 >= y2) then exit; // safety

	if sakuparam.palettemode = EPaletteMode.Truecolor then begin _TrueBlitz; exit; end;

	lastcolor := $FF;
	SetColor(0);
	endp := @outbuf[240];
	for y := y1 to y2 - 1 do begin
		GotoXY(x1, y);
		srcp := outputBuffy + (y * longint(sysvar.windowSize.w) + x1) shl 2;
		writep := @outbuf[1];

		for x := x1 to x2 - 1 do begin
			_GetPalette;
			if lastcolor <> thiscolor then begin
				lastcolor := thiscolor;
				byte(writep^) := 27; inc(writep);
				byte(writep^) := ord('['); inc(writep);
				byte(writep^) := ord(termtextcolor[thiscolor][1]); inc(writep);
				byte(writep^) := ord(termtextcolor[thiscolor][2]); inc(writep);
				byte(writep^) := ord('m'); inc(writep);
			end;
			char(writep^) := thischar; inc(writep);

			if writep > endp then begin
				byte(outbuf[0]) := writep - @outbuf[1];
				write(outbuf);
				writep := @outbuf[1];
			end;
		end;

		byte(outbuf[0]) := writep - @outbuf[1];
		write(outbuf);
	end;
end;
{$endif}

// ------------------------------------------------------------------

{$include render-implementation.pas}

procedure TRendermatic.RenderEverything;
// Renders anything that has changed (dirty rectangles) into outputbuffy^, and blitzes it to the console output.
var i : dword;
	refreshed : boolean;
begin
	if sysvar.pauseState = EPauseState.Paused then exit;
	// Make sure all boxes overlapped by other boxes (themselves maybe also overlapped) are refreshed.
	repeat
		refreshed := FALSE;
		for i := 0 to high(BoxHub.textbox) do with BoxHub.textbox[i] do
			if (boxState <> EBoxState.Null) and (NOT isHidden) and (boxNeedsRedraw) then
				refreshed := refreshed or RefreshOverlappingBoxes;
	until refreshed = FALSE;

	// Exclude all visible textboxes from the normal graphic refresh rects. Fully redraw all boxes needing it.
	for i := 0 to high(BoxHub.textbox) do with BoxHub.textbox[i] do
		if (boxState <> EBoxState.Null) and (NOT isHidden) then with boxRect do begin
			RemoveRefresh(renderLocP.x, renderLocP.y,
				renderLocP.x + longint(renderSizeP.w), renderLocP.y + longint(renderSizeP.h));
			if boxNeedsRedraw then begin
				BlitzBox;
				boxNeedsRedraw := FALSE;
			end;
		end;

	// Draw all graphics in refresh rects not excluded by textboxes.
	while numfresh <> 0 do begin
		dec(numfresh);
		// First draw into a full RGBX output buffer. (But at a very low resolution.)
		RenderRect(refresh[numfresh], outputBuffy, TRUE);
		// Then convert to ASCII and write to the console.
		with refresh[numfresh] do BlitzAscii(left, top, right, bottom);
	end;

	GotoXY(sysvar.caretLocP.x, sysvar.caretLocP.y);
	SetColor(7);
end;
