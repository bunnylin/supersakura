{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

procedure SpawnWindow;
// Call this to adjust the game to whatever the console's current size is. (Doesn't actually spawn a new console.)
var i, j : dword;
begin
	if preference.usePrefWindowSize then
		sysvar.windowSize := preference.prefWindowSize
	else begin
		GetConsoleSize(i, j);
		if i <> 0 then sysvar.windowSize.w := i;
		if j <> 0 then sysvar.windowSize.h := j;
	end;
	if sakuparam.overrideWindowSize.w <> 0 then sysvar.windowSize.w := sakuparam.overrideWindowSize.w;
	if sakuparam.overrideWindowSize.h <> 0 then sysvar.windowSize.h := sakuparam.overrideWindowSize.h;
	log('Game window size: ' + sysvar.windowSize.toString);
	UpdateCoscosTable(sysvar.windowSize.w + sysvar.windowSize.h);

	// Forget any ongoing transition. These rely on a stashed copy of the screen being transitioned away from, and
	// a new screen size means the old copy would be mis-sized.
	with EffectHub do
		if transitionsActive <> 0 then
			for i := fxCount - 1 downto 0 do
				if fx[i] is TEffectTransition then fx[i].Destroy;

	with sysvar do begin
		// Set up buffers for output.
		i := windowSize.w * windowSize.h * 4;
		with Rendermatic do begin
			if outputBuffy <> NIL then begin freemem(outputBuffy); outputBuffy := NIL; end;
			getmem(outputBuffy, i);
		end;

		// Console graphics need less caching memory than SDL.
		asman_gfxMemLimit := 65536 + 8 * (windowSize.w * windowSize.h + activeBaseRes.w * activeBaseRes.h);
	end;

	// The viewports may need adjusting. Updating viewport 0 causes it to import the new window pixel size, which
	// cascades down to child viewports. All content in the viewports also gets marked for refreshing.
	// This must be done for all metastates in the hub stack.
	for i := 0 to HubStack.stackIndex do begin
		HubStack.SelectHubLevel(i);
		if length(Viewportmatic.viewport) <> 0 then Viewportmatic.viewport[0].Update;
	end;
end;

procedure HandleConEvent(com : string31);
var i : dword;
begin
	if com = '' then exit;

	// A few keyboard commands must be handled early regardless of gamemode...
	if com[1] = #0 then case com of
		// Ctrl-P
		#0#4#16: UserInput_CtrlP;

		// Ctrl-Alt-P or Ctrl-Shift-P
		#0#2#16, #0#5#16: SetPauseState(EPauseState.Single);

		// Ctrl-R
		#0#4#18: SpawnWindow;

		// Ctrl-Q
		#0#4#17:
		begin
			sysvar.autoChoice := FALSE;
			sysvar.skipText := ESkipState.No;
			if sysvar.pauseState <> EPauseState.Normal then SetPauseState(EPauseState.Normal);
			SummonConfirmQuit;
			exit;
		end;

		// Ctrl-W
		#0#4#23:
		begin
			i := ord(sakuparam.palettemode) + 1;
			sakuparam.palettemode := EPaletteMode(i mod byte(ord(high(EPaletteMode)) + 1));
			Rendermatic.InitPalette;
			Rendermatic.Reset;
			with BoxHub do for i := high(textbox) downto 0 do with textbox[i] do
				boxNeedsRedraw := boxNeedsRedraw or (NOT isHidden);
		end;
	end;

	// If we're paused, anything else must be ignored.
	if sysvar.pauseState = EPauseState.Paused then exit;

	case com[1] of
		#0:
		case com of
			#0#4#2: UserInput_CtrlB;
			#0#4#4: UserInput_CtrlD;
			#0#4#6: UserInput_CtrlF;
			#0#4#9: UserInput_CtrlI; // may not work due to silly terminal, try Ctrl-V instead...
			#0#4#12: UserInput_CtrlL;
			#0#4#13, #0#2#13: UserInput_CtrlM; // Ctrl-M may not work due to silly terminal... also accept Ctrl-Alt-M
			#0#4#19: UserInput_CtrlS;
			#0#4#20: UserInput_CtrlT;
			#0#4#22: UserInput_CtrlV;
			#0#4#$EE#$90#$A3: UserInput_End(TRUE);
			#0#4#$EE#$90#$A4: UserInput_Home(TRUE);
			#0#4#$EE#$90#$A5: UserInput_Left(TRUE);
			#0#4#$EE#$90#$A6: UserInput_Up(TRUE);
			#0#4#$EE#$90#$A7: UserInput_Right(TRUE);
			#0#4#$EE#$90#$A8: UserInput_Down(TRUE);
			#0#1#$EE#$91#$B9: UserInput_Menu; // Shift-F10

			// Debug mode activation.
			#0#4#24: with sysvar do if debugAllowed = 0 then inc(debugAllowed);
			#0#4#25: with sysvar do if debugAllowed in [1,4] then inc(debugAllowed);
			#0#4#26: with sysvar do if debugAllowed in [2,3] then inc(debugAllowed);
		end;

		#$EE:
		case com of
			#$EE#$90#$A1: UserInput_PageUp;
			#$EE#$90#$A2: UserInput_PageDown;
			#$EE#$90#$A3: UserInput_End(FALSE);
			#$EE#$90#$A4: UserInput_Home(FALSE);
			#$EE#$90#$A5: UserInput_Left(FALSE);
			#$EE#$90#$A6: UserInput_Up(FALSE);
			#$EE#$90#$A7: UserInput_Right(FALSE);
			#$EE#$90#$A8: UserInput_Down(FALSE);
			#$EE#$90#$AE: UserInput_Delete;
			#$EE#$90#$AF: UserInput_Menu;
		end;

		#$8: UserInput_Backspace;
		#$D: UserInput_Enter;
		#$1B: UserInput_Esc;
		else begin
			com[length(com) + 1] := #0;
			UserInput_TextInput(pchar(@com[1]));
		end;
	end;
end;

{$include base-all.pas}
