{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

type TViewport = class
	viewportIndex : dword;
	// Graphics are rendered into logical viewports, to make it easier to have separate interface elements of possibly
	// varying pixel ratios. Viewports are defined in relation to parent viewports.
	// Viewport 0 is special, hardcoded to be equal to the full output window.
	// Due to the cascading nature of size changes, always call Update after changing anything in a viewport.
	viewportParent : dword;
	// If ratio is non-zero, you can force a viewport into a specific aspect ratio. It gets letterboxed within its
	// parent viewport. If ratio is zero, the full parent window is used, but its aspect ratio could be anything.
	// Generally you want to force an aspect ratio for game graphics, but use the full window for textboxes.
	viewportRatio : TSizeP;
	// Viewport location is defined by its inclusive top left and exclusive bottom right corner, relative to the
	// viewport's parent letterboxed to this port's ratio. Useful for having the game view inside a fixed viewframe.
	viewportLoc : TEdgeCoords;
	// A shortcut for viewport pixel size, derives from definitive location coordinates.
	viewportSizeP : TSizeP;

	function GetBackgroundIndex : dword;
	procedure Update;
	procedure AddRefresh; inline;
	procedure Reset;
	function ToString : UTF8string; override;
	constructor Create(viewnum : dword; const clonefrom : TViewport);
end;

type TViewportmatic = class
	viewport : array of TViewport;

	procedure SetNumViewports(newcount : dword);
	procedure InheritState(const src : TViewportmatic);
	procedure Reset;
	function Serialise : TStringBunch;
	destructor Destroy; override;
end;

