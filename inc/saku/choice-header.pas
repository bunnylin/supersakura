{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Choicematic controls player choices in textboxes.
// Tracking an array of children would be very costly when building a large flat list, so track parent only.
type TChoiceNode = record
	choiceTxt : TStringBunch; // this node's display text, without parent strings; if NIL, choice is empty
	parent : dword; // node index; 0 is virtual root node; parent must be < child node
	children : dword; // total children for choiceNodes[index]; if 0, selection will be final
	jumpList : TStringBunch; // list of labels; use jumpList[$trackVar] upon choice.goto or choice.call
	trackVar : UTF8string; // name of script variable or empty for jumpList[$trackVar]
	isLocked : boolean; // while TRUE, node will not be printed or selectable
	level : byte; // 0 = root, 1 = verb (1st order), 2 = noun (2nd order)...
end;

// Choices are specified in two different ways - choice.set commands to create a node tree under Choicematic's control,
// or directly printing "\?choice\." strings in boxes. Only one box at a time can be designated as the Choice box, but
// nothing prevents scripts from printing ad-hoc choices in any box (indeed, nested multi-language printing causes even
// Choicematic to print the same choices in multiple boxes).
//
// The number of printed choices needs to be tracked per-box, including their display rects. Each box has a choice
// strings array, count zeroed on box clear. The Print function reacts to any \? by adding an item to that array, and
// zeroes the related node index and display rect. If the Print call came from Choicematic, the caller can immediately
// override the most recent item's choice index with a valid choice node. Therefore any ad-hoc choice print always has
// a 0 index. When the box is rendered, the display rect values are filled in.
//
// Choice.get returns the selected node index - 1. So -1 for ad-hoc.

type TChoicematic = class
	choiceBox, choicePartBox, highlightBox : TTextBox;

	// If not empty, each time the user changes the highlight, this label gets spawned in a new fiber.
	onHighlight : UTF8string;

	// This stores the most recent choice in the form ["Press","Red button"].
	mostRecentChoice : TStringBunch;

	// Choicelist comes straight from sakurascript's choice.set command.
	choiceNodes : array of TChoiceNode;
	choiceNodeCount : dword; // maybe valid node count in choiceNodes, incl. empty NIL choiceTxt nodes, incl. root
	// Node most recently selected by the user. While Choicematic is active, this is the node whose children are
	// printed as options. When a choice without children is selected, that final node index remains in this, and the
	// choice-triggering fiber can use this to decide where to jump/call. If the selected choice is ad-hoc, the value
	// is 0; if choicematic was cancelled, the value is -1.
	selectedNode : longint;

	// The choiceBox.choiceList[] index that's currently or most recently highlighted, -1 for cancelled.
	// This value is returned by choice.gethighlight.
	highlightIndex : longint;
	// The most recent added choice sets this to its label's canonical language index. For any interaction with added
	// choices, choice strings are identified in the canonical language.
	activeLanguage : byte;
	userCanCancel : byte; // 0: no, 1: cancellable at base level, 2: cancellable at sublevel, 3: cancellable at any

	isActive, clearOnDeactivate : boolean;

	procedure Init; // can't be a constructor
	procedure ResetChoices;
	procedure Deactivate(cancelled, clearboxes : boolean);
	function EqualsChoice(const txt : TStringBunch; lvl : byte; index : longint) : boolean;
	function FindChoice(const txt : TStringBunch; lvl : byte = 1) : dword;
	function GetFullChoiceTxt(nodeindex : dword) : UTF8string;
	procedure HighlightChoice(showindex : dword; _style : EMoveType = EMoveType.Halfcos);
	procedure MoveHighlightUp(steps : dword = 1);
	procedure MoveHighlightDown(steps : dword = 1);
	procedure MoveHighlightLeft;
	procedure MoveHighlightRight;
	procedure PrintCurrentChoices;
	procedure SelectChoice(showindex : dword);
	function RevertChoice : boolean;
	procedure Activate(
		const fiber : TFiber; const _startparent : UTF8string; clearboxes, printchoices, suspendfiber : boolean);
	function AddChoice(
		const txt : TStringBunch; index : longint; const _jumplist, _trackvar : UTF8string; locked : boolean;
		languageindex : byte) : dword;
	procedure RemoveChoice(index : dword);
	procedure RemoveChoice(const removetxt : UTF8string);
	procedure LockChoice(const txt : UTF8string; avail : boolean);
	function StateToString : UTF8string;
	function Serialise : TStringBunch;
end;

