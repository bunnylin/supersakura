{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// SuperSakura SDL-specific rendering functions.

procedure TRendermatic.CreateRendererAndTexture;
// Prepares SDL components for rendering duties, throws an exception on errors.
var rendinfo : TSDL_RendererInfo;
	i, j : dword;
	txformat : dword;
	txaccess, txwidth, txheight : longint;
begin
	keyframe := preference.keyFrameSpan;
	with SDL do begin
		// Release the old-sized texture and renderer, if any.
		if mainTextureH <> NIL then begin
			SDL_DestroyTexture(mainTextureH); mainTextureH := NIL;
		end;
		if rendererH <> NIL then begin
			SDL_DestroyRenderer(rendererH); rendererH := NIL;
		end;

		// Create the renderer!
		i := 0;
		if preference.useVSync then i := SDL_RENDERER_PRESENTVSYNC;
		try
			rendererH := SDL_CreateRenderer(mainWindowH, -1, i);
		except
			on e : Exception do log(e.Message);
		end;

		if rendererH = NIL then begin
			log('Failed to create SDL renderer: ' + SDL_GetError);
			log('Trying software fallback...');
			rendererH := SDL_CreateRenderer(mainWindowH, -1, SDL_RENDERER_SOFTWARE);

			if rendererH = NIL then raise Exception.Create('Failed to create SDL renderer: ' + SDL_GetError);
		end;

		SDL_GetRendererOutputSize(rendererH, @i, @j);
		log(strcat('New renderer output size: %x%', [i, j]));

		// Clear the window a few times (double/triple buffering).
		for i := 2 downto 0 do begin
			SDL_SetRenderDrawColor(rendererH, 0, 0, 0, 255);
			SDL_RenderClear(rendererH);
			SDL_RenderPresent(rendererH);
		end;

		i := SDL_GetRendererInfo(rendererH, @rendinfo);
		if i <> 0 then
			LogError('Error fetching renderer info: ' + SDL_GetError)
		else begin
			log('Using renderer: ' + rendinfo.name);
			log('Desired texture size: ' + sysvar.windowSize.ToString);
			log(strcat('Max texture size (effectively, largest possible window): %x%',
				[rendinfo.max_texture_width, rendinfo.max_texture_height]));
			if rendinfo.flags and SDL_RENDERER_SOFTWARE <> 0 then
				log('We''re a software renderer')
			else
				log('We''re a hardware renderer');
			if rendinfo.flags and SDL_RENDERER_ACCELERATED <> 0 then
				log('We''re accelerated')
			else
				log('We''re not accelerated');
			if rendinfo.flags and SDL_RENDERER_PRESENTVSYNC <> 0 then
				log('We''re vsynched')
			else
				log('We''re not vsynched');
		end;

		// Create the texture that is directly used as output.
		mainTextureH := SDL_CreateTexture(
			rendererH, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, sysvar.windowSize.w, sysvar.windowSize.h);
		if mainTextureH = NIL then raise Exception.Create('Failed to create SDL maintex: ' + SDL_GetError);

		i := SDL_QueryTexture(mainTextureH, @txformat, @txaccess, @txwidth, @txheight);
		if i <> 0 then
			LogError('Error fetching texture info: ' + SDL_GetError)
		else begin
			log(strcat('Using texture %x%, format $&', [txwidth, txheight, txformat]));
			case txaccess of
				SDL_TEXTUREACCESS_STATIC: log('Static texture access, not lockable');
				SDL_TEXTUREACCESS_STREAMING: log('Streaming texture access, lockable');
				SDL_TEXTUREACCESS_TARGET: log('Texture is a render target');
				else log('Unknown texture access');
			end;
		end;

		// Set up buffers for output.
		i := sysvar.windowSize.w * sysvar.windowSize.h * 4;
		if outputBuffy <> NIL then begin freemem(outputBuffy); outputBuffy := NIL; end;
		getmem(outputBuffy, i);

		// Make sure we start with a clean, black window.
		filldword(outputBuffy^, sysvar.windowSize.w * sysvar.windowSize.h, 0);
		SDL_UpdateTexture(mainTextureH, NIL, outputBuffy, sysvar.windowSize.w * 4);
	end;
end;

{$include render-implementation.pas}

procedure TRendermatic.RenderEverything;
// Renders anything that has changed (dirty rectangles) into outputbuffy^, sends it to SDL for displaying to the user.
// https://wiki.libsdl.org/MigrationGuide#If_your_game_just_wants_to_get_fully-rendered_frames_to_the_screen
var rekt : TSDL_Rect;
	windowstride : dword;
begin
	if sysvar.pauseState <> EPauseState.Paused then begin
		// If absolutely nothing has changed in the display buffer (numfresh = 0), we can keep displaying the last
		// frame and just do nothing. This reduces GPU usage by like 80%, considering visual novels have mostly static
		// graphics. However, if the window buffer has been invalidated by the user moving the window outside the
		// desktop or moving another window over our game window, garbage is left behind until a new frame is pushed.
		// To cover this, we can force a buffer push even if unchanged, once every 8 frames or so.
		if (numfresh = 0) and (keyframe <> 0) then begin dec(keyframe); exit; end;
		keyframe := preference.keyFrameSpan;

		windowstride := sysvar.windowSize.w * 4;
		while numfresh <> 0 do begin
			dec(numfresh);

			RenderRect(refresh[numfresh], outputBuffy, FALSE);

			with refresh[numfresh] do begin
				rekt.x := left;
				rekt.y := top;
				rekt.w := right - left;
				rekt.h := bottom - top;

				SDL_UpdateTexture(SDL.mainTextureH, @rekt,
					outputBuffy + top * longint(windowstride) + left * 4,
					windowstride);
			end;
		end;
	end;

	// Push the texture into the renderer. Apparently we can't rely on the previous frame still being there, so gotta
	// do fullscreen updates.
	SDL_RenderCopy(SDL.rendererH, SDL.mainTextureH, NIL, NIL);

	// Ask the renderer to display the new frame (or add it in the display queue, if double/triple buffering).
	// If the renderer was created with vsync enabled, this call may block until next vsync.
	SDL_RenderPresent(Rendermatic.SDL.rendererH);
end;

