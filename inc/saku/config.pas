{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Commandline parameters.
var sakuparam : record
	appName, basePath, logPath, iniPath : UTF8string;
	cmdlineDats : TStringBunch;
	overrideWindowSize : TSizeP;
	frameDelay : dword; // 0: normal frame limiter; else run at max cpu, pretend each frame takes frameDelay msec
	{$ifdef sakucon}
	palettemode : EPaletteMode;
	{$endif}
end;

// User preferences, from a config file, some modifiable in-game using a settings dialog.
type TPrefs = object
	restTime : dword; // rest between frames to reach a minimum frame duration, milliseconds
	usePrefWindowSize : boolean; // if false, game window size is set automatically based on game data
	prefWindowSize : TSizeP;
	customDataDirectory : UTF8string; // additional location for game dats
	displayLanguage : UTF8string; // newly-started games display text in this language if available
	uiMagnification : dword; // text size adjustment, 32k = 100%
	ctrlDeadZone : longint; // minimal recognised value for gamepad sticks
	autoSaveSlots, autoSaveAfterMSec : longint;
	targetFps, keyFrameSpan : byte;
	useVSync : boolean;

	// This keeps the user's font preference. Actual found font files/metrics are in fontmatic.
	fonts : array of record
		// "English" or "Japanese", should have only one active font per language, must be lowercase.
		fontLang : UTF8string;
		fontMatch : UTF8string; // full or partial font file path with wildcards
	end;

	autotrans : record
		externalCmd : UTF8string; // this is invoked to translate
		rateLimitBytes, requestDelayMsec : longint;
		internalEnabled, internalAuto, externalEnabled, transliterations : boolean;
	end;

	{$ifndef sakucon}
	audio : record
		driver : TBSoundServer.ETargetSink;
		driverName : UTF8string;
		soundFontPath : UTF8string;
	end;
	{$endif}

	procedure Reset;
	function GetFontPref(language : UTF8string) : dword;
	procedure ReadConfig(const inifile : UTF8string);
	procedure WriteConfig;
end;

var preference : TPrefs;

// ------------------------------------------------------------------

procedure TPrefs.Reset;
begin
	{$ifndef sakucon}
	targetFps := 30; // low to save energy
	prefWindowSize.w := 640; prefWindowSize.h := 480;
	setlength(fonts, 0);

	with audio do begin
		driver := TS_BUNNY;
		soundFontPath := '';
	end;
	{$else}
	targetFps := 20; // consoles don't need too many FPS
	{$endif}
	keyFrameSpan := 10;
	restTime := 1000 div targetFps;

	customDataDirectory := '';
	displayLanguage := '';
	usePrefWindowSize := FALSE;
	uiMagnification := 32768;
	ctrlDeadZone := 6400; // meaningless on consoles
	autoSaveSlots := 2;
	autoSaveAfterMSec := 2 * 60000;
	useVSync := TRUE; // meaningless on consoles
	with autotrans do begin
		externalCmd := '';
		rateLimitBytes := 4000;
		requestDelayMsec := 1500;
		internalEnabled := FALSE;
		internalAuto := TRUE;
		externalEnabled := FALSE;
		transliterations := TRUE;
	end;
end;

function TPrefs.GetFontPref(language : UTF8string) : dword;
// Returns the index of the user's preferred name/path for the given language's font, if specified, or a value out of
// range otherwise.
begin
	result := 0;
	language := lowercase(language);
	while result < dword(length(fonts)) do begin
		if language = fonts[result].fontLang then exit;
		inc(result);
	end;
end;

procedure TPrefs.ReadConfig(const inifile : UTF8string);
var f : TIniFileLoader;
	i, j, l : dword;
	key, value : UTF8string;
begin
	log('Reading config from ' + inifile);
	f := TIniFileLoader.Open(inifile);

	try
		while TRUE do begin
			key := f.ReadKeyValue(value);
			case key of
				'': break;

				{$ifndef sakucon}
				'font':
				begin
					i := pos(' ', value);
					if i <= 0 then begin
						LogError('expected "font <language> <match>", got font ' + value);
						continue;
					end;
					setlength(fonts, length(fonts) + 1);
					with fonts[high(fonts)] do begin
						fontLang := copy(value, 1, i - 1);
						fontMatch := copy(value, i + 1);
						// Remove double-quotes, if they are present for some reason.
						if (fontMatch.Length > 1 ) and (fontMatch[1] = '"') and (fontMatch[fontMatch.Length] = '"') then
							fontMatch := copy(fontMatch, 2, fontMatch.Length - 2);
					end;
				end;

				'audio':
				begin
					// Allow selecting fluidsynth backend driver with "fluid/pipewire" etc.
					i := pos('/', value);
					if i > 0 then begin
						audio.driverName := lowercase(copy(value, i + 1));
						setlength(value, i - 1);
					end;
					case lowercase(value) of
						'': audio.driver := {$ifdef WINDOWS}TS_WINDOWS{$else}TS_FLUID{$endif}; // pending bunny
						'auto','bunny': audio.driver := TS_BUNNY;
						'none': audio.driver := TS_NONE;
						{$ifdef WINDOWS}
						'windows': audio.driver := TS_WINDOWS;
						{$endif}
						'fluid','fluidsynth': audio.driver := TS_FLUID;
						else begin
							LogError('Unknown audio driver "' + value + '", using default.');
							audio.driver := {$ifdef WINDOWS}TS_WINDOWS{$else}TS_FLUID{$endif}; // pending bunny
						end;
					end;
				end;

				'soundfont':
				audio.soundFontPath := value;
				{$endif}

				'datadir':
				begin
					customDataDirectory := value;
					// Remove surrounding quotes if present.
					if (customDataDirectory.Length >= 3)
					and (customDataDirectory[1] = '"') and (customDataDirectory[customDataDirectory.Length] = '"')
					then customDataDirectory := copy(customDataDirectory, 2, customDataDirectory.Length - 2);
				end;

				'autosaveslots':
				autoSaveSlots := EnsureRange(valx(value), 0, 999);

				'autosaveminutes':
				autoSaveAfterMSec := EnsureRange(valx(value), 0, 99999) * 60000;

				'translatorint':
				if value = 'auto' then begin
					autotrans.internalAuto := TRUE;
					autotrans.internalEnabled := autotrans.externalEnabled;
				end
				else begin
					autotrans.internalAuto := FALSE;
					autotrans.internalEnabled :=
						(value = 'on') or (value = '1') or (value = 'true') or (value = 'enabled') or (value = 'yes');
				end;
				'translatorext':
				begin
					autotrans.externalEnabled := (value = 'auto') or
						(value = 'on') or (value = '1') or (value = 'true') or (value = 'enabled') or (value = 'yes');
					if autotrans.internalAuto then autotrans.internalEnabled := autotrans.externalEnabled;
				end;
				'transliterations':
				autotrans.transliterations := (value = 'auto') or
					(value = 'on') or (value = '1') or (value = 'true') or (value = 'enabled') or (value = 'yes');
				'translatorcmd':
				autotrans.externalCmd := value;
				'ratelimit':
				autotrans.rateLimitBytes := valx(value);
				'requestdelay':
				autotrans.requestDelayMsec := valx(value);

				'uimag':
				begin
					uiMagnification := abs(valx(value));
					if uiMagnification = 0 then
						uiMagnification := 32768
					else
						uiMagnification := EnsureRange(uiMagnification, 2048, 262144);
				end;

				'displaylanguage':
				displayLanguage := value;

				'fps':
				begin
					i := dword(abs(valx(value)));
					if (i > 0) and (i < 255) then begin
						targetFps := i;
						restTime := 1000 div targetFps;
					end;
				end;

				'kfs':
				keyFrameSpan := byte(abs(valx(value)));

				'vsync':
				begin
					value := lowercase(value);
					useVSync :=
						(value = 'on') or (value = '1') or (value = 'true')
						or (value = 'enabled') or (value = 'yes') or (value = 'auto');
				end;

				'analogdz':
				begin
					ctrlDeadZone := abs(valx(value));
					if (ctrlDeadZone = 0) or (ctrlDeadZone > 32000) then ctrlDeadZone := 6400;
				end;

				'winsize':
				begin
					l := 1;
					i := abs(CutNumberFromString(value, l));
					j := abs(CutNumberFromString(value, l));
					usePrefWindowSize := (i <> 0) and (j <> 0);
					if usePrefWindowSize then begin
						prefWindowSize.w := i;
						prefWindowSize.h := j;
					end;
				end;
			end;
		end;
		sakuparam.iniPath := inifile;
	finally
		f.Destroy;
	end;
end;

procedure TPrefs.WriteConfig;
var inifile : UTF8string;
	f : TBufferedFileWriter;
	{$ifndef sakucon} i : dword; {$endif}
begin
	try
		inifile := PathCombine([sakuparam.basePath, sakuparam.appName + '.ini']);
		f := TBufferedFileWriter.Create(inifile, NIL, 262144);
	except
		on EAccessDenied do begin
			// Fall back to profile location if workdir not allowed.
			inifile := PathCombine([GetAppConfigDir(FALSE), sakuparam.appName + '.ini']);
			try
				f := TBufferedFileWriter.Create(inifile, NIL, 262144);
			except on e : Exception do begin
				LogError('Failed to write ' + inifile, e, ExceptAddr);
				exit;
			end; end;
		end;
		on e : Exception do begin
			LogError('Failed to write ' + inifile, e, ExceptAddr);
			exit;
		end;
	end;

	log('Writing config to ' + inifile);
	try
		f.WriteLine('# SuperSakura configuration');
		f.WriteString('# You can edit this directly if SuperSakura is not running.' + LineEnding + LineEnding);

		// Limit to 78 chars per row, Laz cursor range 22-100.
		f.WriteLine('# Game window size in windowed mode. Set to "auto" to use the game''s default');
		f.WriteLine('# resolution, scaled as large as comfortably fits on your screen.');
		f.WriteLine('# Override the default by giving a pixel size, for example: WinSize 512x384');
		f.WriteString('winsize ');
		if usePrefWindowSize then
			f.WriteLine(prefWindowSize.ToString)
		else
			f.WriteLine('auto');

		{$ifndef sakucon}
		f.WriteLine('');
		f.WriteLine('# The available audio systems are: None, Bunny, Fluid, Windows.');
		f.WriteLine('# BunnySynth is a built-in softsynth, best for FM soundtracks, good for MIDI.');
		f.WriteLine('# You can use FluidSynth if you have its runtime library. A soundfont must be');
		f.WriteLine('# specified if using Fluid. FluidSynth''s backend driver can also be selected');
		f.WriteLine('# with Fluid/<driver>, for example Fluid/sdl2.');
		f.WriteLine('# On Windows, the default GS wavetable synth is available.');
		f.WriteString('audio ');
		case audio.driver of
			TS_FLUID:
			begin
				f.WriteString('fluid');
				if audio.driverName <> '' then f.WriteString('/' + audio.driverName);
				f.WriteLine('');
			end;
			TS_NONE: f.WriteLine('none');
			{$ifdef WINDOWS}
			TS_WINDOWS: f.WriteLine('windows');
			{$endif}
			else f.WriteLine('bunny');
		end;

		f.WriteLine('');
		f.WriteLine('# Soundfont file to use with FluidSynth. Leave unspecified to autodetect, if');
		f.WriteLine('# you have a soundfont installed in a standard default path.');
		if audio.soundFontPath = '' then
			f.WriteLine('#soundfont path/file.sf2')
		else
			f.WriteLine('soundfont ' + audio.soundFontPath);
		{$endif}

		f.WriteLine('');
		f.WriteLine('# Game data directory. Leave commented out to use primarily "data" under the');
		f.WriteLine('# engine''s base directory, or secondarily in your local profile.');
		f.WriteLine('# Uncomment to use a custom dat-file storage path.');
		if customDataDirectory = '' then
			f.WriteLine('#datadir ~/mystuff/datfiles')
		else
			f.WriteLine('datadir ' + customDataDirectory);

		f.WriteLine('');
		f.WriteLine('# Set the number of allowed autosave files in rotation, and the minimum');
		f.WriteLine('# minutes to wait between autosaves.');
		f.WriteLine('autosaveslots ' + strdec(autoSaveSlots));
		f.WriteLine('autosaveminutes ' + strdec(autoSaveAfterMSec div 60000));

		f.WriteLine('');
		f.WriteLine('# The internal autotranslator can translate simple words and punctuation to');
		f.WriteLine('# English. Set to on, off, or auto (enabled if external is also enabled).');
		f.WriteLine('# It can also optionally transliterate common expressions and honorifics.');
		f.WriteLine('# The external translator runs a configurable shell command, invoking any');
		f.WriteLine('# external commandline translation tool (it runs "<cmd> <string>"). The tool');
		f.WriteLine('# must return only the translated string in stdout and exit code 0. If cmd is');
		f.WriteLine('# not specified here, it will try to use the Translate-Shell utility if');
		f.WriteLine('# available. To avoid exceeding web translation usage limits, a rate limiter');
		f.WriteLine('# applies on number of bytes per 5 seconds, and a msec delay between requests.');
		f.WriteString('translatorint ');
		if autotrans.internalAuto then
			f.WriteLine('auto')
		else if autotrans.internalEnabled then
			f.WriteLine('on')
		else
			f.WriteLine('off');
		f.WriteString('transliterations ');
		if autotrans.transliterations then
			f.WriteLine('on')
		else
			f.WriteLine('off');
		f.WriteString('translatorext ');
		if autotrans.externalEnabled then
			f.WriteLine('on')
		else
			f.WriteLine('off');
		if autotrans.externalCmd = '' then f.WriteString('#');
		f.WriteLine('translatorcmd ' + autotrans.externalCmd);
		f.WriteLine('ratelimit ' + strdec(autotrans.rateLimitBytes));
		f.WriteLine('requestdelay ' + strdec(autotrans.requestDelayMsec));

		f.WriteLine('');
		f.WriteLine('# Default display language when starting a new game. Defaults to empty, which');
		f.WriteLine('# uses the game''s original language.');
		f.WriteLine('displaylanguage ' + displayLanguage);

		{$ifndef sakucon}
		f.WriteLine('');
		f.WriteLine('# Interface size multiplier. If you want the in-game font to be slightly');
		f.WriteLine('# smaller or larger, change this. The multiplier is this value divided by');
		f.WriteLine('# 32768. For example, 32768 = normal, 16384 = half size, 65536 = double size.');
		f.WriteLine('uimag ' + strdec(uiMagnification));

		f.WriteLine('');
		f.WriteLine('# Preferred font for each language. Specify a language, followed by');
		f.WriteLine('# a font face or file name, with optional * and ? wildcards.');
		f.WriteLine('# Leave unspecified to use default fonts. Examples:');
		f.WriteLine('# font English Times New*');
		f.WriteLine('# font Japanese ~/myfonts/kittens-serif-bold.ttf');
		if length(fonts) <> 0 then
			for i := 0 to high(fonts) do with fonts[i] do
				f.WriteLine('font ' + fontLang + ' ' + fontMatch);
		{$endif}

		f.WriteLine('');
		f.WriteLine('# Target frame rate. Higher FPS makes animations smoother but uses more power.');
		f.WriteLine('fps ' + strdec(targetFps));

		{$ifndef sakucon}
		f.WriteLine('');
		f.WriteLine('# Key frame span. Higher KFS saves power but shows more garbage when moving');
		f.WriteLine('# desktop windows around.');
		f.WriteLine('kfs ' + strdec(keyFrameSpan));

		f.WriteLine('');
		f.WriteLine('# Vertical sync. Enabling this may reduce the frame rate, but looks nicer.');
		if useVSync then
			f.WriteLine('vsync on')
		else
			f.WriteLine('vsync off');

		f.WriteLine('');
		f.WriteLine('# Gamepad analog stick deadzone. Sticks may often get stuck at a slightly');
		f.WriteLine('# off-center position, where 0 is center and 32768 is as far as it goes.');
		f.WriteLine('# Any value smaller than the deadzone is treated as a 0. Default: 6400.');
		f.WriteLine('analogdz ' + strdec(ctrlDeadZone));
		{$endif}
	finally
		f.Destroy;
	end;
end;

function HandleCommandlineParams : boolean;
// Processes the ssakura commandline. Returns FALSE in case of errors etc.
var txt : UTF8string;
begin
	result := TRUE;
	with sakuparam do begin
		appName := '';
		basePath := '';
		logPath := '';
		setlength(cmdlineDats, 0);
		overrideWindowSize.w := 0; overrideWindowSize.h := 0;
		frameDelay := 0;
		{$ifdef sakucon}
		palettemode := EPaletteMode.LXYChroma;
		{$endif}
	end;

	repeat
		txt := GetNextParam;
		case txt of
			chr(0): break;
			'', '-': ;
			'-autotest': begin sysvar.autoChoice := TRUE; sysvar.skipText := ESkipState.All; end;
			'-framedelay': sakuparam.frameDelay := valx(GetNextParam);
			'-x': sakuparam.overrideWindowSize.w := valx(GetNextParam);
			'-y': sakuparam.overrideWindowSize.h := valx(GetNextParam);
			'-v', '-version':
			begin
				writeln(GetSuperSakuraVersion);
				result := FALSE;
			end;

			else begin
				if txt[1] = '-' then begin
					writeln('Unrecognised option: ' + txt);
					result := FALSE;
					exit;
				end
				else begin
					setlength(sakuparam.cmdlineDats, sakuparam.cmdlineDats.Length + 1);
					sakuparam.cmdlineDats[high(sakuparam.cmdlineDats)] := txt;
				end;
			end;
		end;
	until FALSE;

	if (NOT print_commandline_help) or (NOT result) then exit;

	result := FALSE;

	{$ifdef sakucon}
	writeln('SuperSakura-con  ' + GetSuperSakuraVersion);
	{$else}
	writeln('SuperSakura  ' + GetSuperSakuraVersion);
	{$endif}
	writeln;
	txt := ExtractFileNameWithoutExt(paramstr(0));
	writeln('Usage: ' + txt + ' [game shortname] [-options]');
	{$ifdef sakucon}
	if txt = 'supersakura-con' then txt := 'supersakura';
	{$endif}

	writeln('If game name is not specified, ' + txt + '.dat is loaded by default.');
	writeln;
	writeln('Options:');
	writeln('-x=n             Override window pixel width');
	writeln('-y=n             Override window pixel height');
	writeln('-autotest        Generate rapid random inputs');
	writeln('-framedelay=n    Disable the frame limiter; max cpu, n msec every frame');
end;

