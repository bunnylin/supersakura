{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Namespace object for the Rendermatic. It builds output frames from graphic assets and framebuffer effects.
type TRendermatic = object
	{$ifdef sakucon}
	// Lookup table for converting 24-bit colors into colorful ASCII approximations. CC1 is the single nearest, CC2 is
	// either the same or something that gives an even better result at a 50-50 dither.
	type TPaletteEntry = packed record
		flatcolor : byte;
		color1, color2 : byte;
		char1, char2 : char;
	end;
	type PPaletteEntry = ^TPaletteEntry;
	var palette : array[0..$F] of array[0..$F] of array[0..$F] of TPaletteEntry;
	{$else}
	SDL : record
		mainWindowH : PSDL_Window;
		rendererH : PSDL_Renderer;
		mainTextureH : PSDL_Texture;
	end;
	{$endif}
	// BGRA buffer for the full game window: windowSize.w * windowSize.h * 4
	outputBuffy : pointer;

	refresh : array of TEdgeCoordP;
	numFresh : dword;
	//RGBtweaktable : array[0..767] of byte; // fullscreen 3-chn adjustment
	//RGBtweakactive : byte;
	{$ifndef sakucon}
	keyframe : byte;
	{$endif}

	procedure Reset;
	procedure ClipRGB(clipdata : pblitstruct);
	//procedure BuildRGBtweakTable(r1, g1, b1 : longint);
	procedure DarkenOutput;
	procedure RenderTransition(const effect : TEffectTransition);
	procedure RenderRect(const refrect : TEdgeCoordP; const destbuf : pointer; noboxes : boolean);
	function StashRender(viewnum : dword; out destbuf : pointer) : dword;
	procedure RenderEverything;
	procedure AddRefresh(x1p, y1p, x2p, y2p, vp : longint);
	{$ifdef sakucon}
	procedure InitPalette;
	procedure RemoveRefresh(x1p, y1p, x2p, y2p : longint);
	procedure BlitzAscii(x1, y1, x2, y2 : longint);
	{$else}
	procedure CreateRendererAndTexture;
	{$endif}
	procedure Destroy;
end;

var Rendermatic : TRendermatic;

