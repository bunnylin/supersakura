{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

procedure InitGamepad;
var i : dword;
begin
	// Release the previous gamepad, if any.
	if sdl_GamepadH <> NIL then begin
		SDL_GameControllerClose(sdl_GamepadH);
		sdl_GamepadH := NIL;
	end;

	i := SDL_NumJoysticks;
	while i <> 0 do begin
		dec(i);
		if SDL_IsGameController(i) = SDL_TRUE then begin
			sdl_GamepadH := SDL_GameControllerOpen(i);
			if sdl_GamepadH <> NIL then break;
			LogError('Failed to open gamepad: ' + SDL_GetError);
		end;
	end;

	if sdl_GamepadH <> NIL then log('Opened gamepad: ' + SDL_GameControllerName(sdl_GamepadH));
	with sysvar do begin
		gamepadLeftStick.x := 0; // sticks should be in -32768..32767 by SDK docs, triggers in 0..32767.
		gamepadLeftStick.y := 0;
		gamepadRightStick.x := 0;
		gamepadRightStick.y := 0;
	end;
end;

procedure AutosizeGameWindow;
// This calculates the nicest window size that fits on the user's desktop.
// The main game data file should be loaded before this, as it gives us the native resolution of the game being loaded.
// We can use that size to figure out a good multiplier that doesn't use weird fractions.
var mymode : TSDL_DisplayMode;
	myrekt : TSDL_Rect;
	i : dword;
begin
	if SDL_GetDesktopDisplayMode(0, @mymode) <> 0 then begin
		LogError(SDL_GetError); exit;
	end;
	if SDL_GetDisplayUsableBounds(0, @myrekt) <> 0 then begin
		LogError(SDL_GetError); exit;
	end;
	log(strcat('SDL reports desktop size: %x% (usable area: %x%)', [mymode.w, mymode.h, myrekt.w, myrekt.h]));

	// Shrink the usable desktop area slightly to avoid auto-sized windows being virtually fullscreen. It looks nicer
	// if there's at least a little guaranteed space around a non-fullscreen window.
	dec(myrekt.w, myrekt.w shr 6);
	dec(myrekt.h, myrekt.h shr 6);

	with sysvar do begin
		if (activeBaseRes.w = 0) or (activeBaseRes.h = 0) then begin
			LogError('bad baseres');
			exit;
		end;

		// For graphics caching, eight times the fullscreen plus base res sizes should be plenty.
		asman_gfxMemLimit := (dword(mymode.w * mymode.h) + activeBaseRes.w * activeBaseRes.h) * 4 * 8;

		// Start from 0.5x, add 0.5 more until a good size is reached.
		i := 1;
		while
			((activeBaseRes.w * i) shr 1 <= dword(myrekt.w)) and
			((activeBaseRes.h * i) shr 1 <= dword(myrekt.h))
			do inc(i);
		dec(i);
		windowSize.w := (activeBaseRes.w * i) shr 1;
		windowSize.h := (activeBaseRes.h * i) shr 1;
		log(strcat('Game base res: %; Multiplier: %/2; Target res: %',
			[activeBaseRes.ToString, i, windowSize.ToString]));
	end;
end;

procedure SpawnWindow;
// Removes the existing game window, and creates a new one.
// SDL2 has a fullscreen toggle, but it seems bugged under some conditions, so manually recreating the whole display
// stack seems to be the best way.
var dispmode : TSDL_DisplayMode;
	i, j : dword;
begin
	log(':: Spawning game window');
	// Forget any ongoing transition. These rely on a stashed copy of the screen being transitioned away from, and
	// a new screen size means the old copy would be mis-sized.
	with EffectHub do
		if transitionsActive <> 0 then
			for i := fxCount - 1 downto 0 do
				if fx[i] is TEffectTransition then fx[i].Destroy;

	// Close and release the existing game window, if any.
	if Rendermatic.SDL.mainWindowH <> NIL then begin
		SDL_DestroyWindow(Rendermatic.SDL.mainWindowH); Rendermatic.SDL.mainWindowH := NIL;
	end;

	// Any further keydown events instantly after this are likely junk repeats.
	sysvar.swallowRemainingUserInput := TRUE;

	// The previously loaded graphics can be cut loose.
	ReleaseGFX;

	{$ifdef bonk}
	// Resize the window. You'd think this would just work, but returning from fullscreen may cause the renderer to
	// adopt a wrong size??
	i := 0;
	if usefull then i := SDL_WINDOW_FULLSCREEN_DESKTOP;
	if SDL_SetWindowFullscreen(Rendermatic.SDL.mainWindowH, i) <> 0 then begin
		LogError('SetWindowFullscreen: ' + SDL_GetError);
		exit;
	end;
	{$endif}

	// Figure out what size the game window needs to be.
	sysvar.windowSize := preference.prefWindowSize;
	if NOT preference.usePrefWindowSize then AutosizeGameWindow;
	if sakuparam.overrideWindowSize.w <> 0 then sysvar.windowSize.w := sakuparam.overrideWindowSize.w;
	if sakuparam.overrideWindowSize.h <> 0 then sysvar.windowSize.h := sakuparam.overrideWindowSize.h;

	// Arbitrary minimum bounds.
	if sysvar.windowSize.w < 16 then sysvar.windowSize.w := 16;
	if sysvar.windowSize.h < 16 then sysvar.windowSize.h := 16;

	i := SDL_WINDOW_SHOWN;
	if sysvar.fullscreen then i := i or SDL_WINDOW_FULLSCREEN_DESKTOP;

	// (If requesting fullscreen, the window pixel sizes are ignored.)
	Rendermatic.SDL.mainWindowH := SDL_CreateWindow(
		NIL, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, sysvar.windowSize.w, sysvar.windowSize.h, i);
	if Rendermatic.SDL.mainWindowH = NIL then
		raise Exception.Create('Failed to create SDL window: ' + SDL_GetError);

	// Rename the window!
	SetProgramName(sysvar.windowTitle);

	// Confirm what we got. When switching away from fullscreen, the display mode may show an incorrect size, so the
	// new window size must be taken from GetWindowSize.
	SDL_GetWindowDisplayMode(Rendermatic.SDL.mainWindowH, @dispmode);
	SDL_GL_GetDrawableSize(Rendermatic.SDL.mainWindowH, @i, @j);
	SDL_GetWindowSize(Rendermatic.SDL.mainWindowH, @sysvar.windowSize.w, @sysvar.windowSize.h);
	log(strcat('GetWinDisplayMode: %x%', [dispmode.w, dispmode.h]));
	log(strcat('GL_GetDrawable: %x%', [i, j]));
	log('GetWindowSize: ' + sysvar.windowSize.ToString);

	// Create the renderer and texture. All output goes first into our own outputbuffy, then that gets pushed into the
	// texture, and the renderer is called to copy the full texture to the game window every frame.
	Rendermatic.CreateRendererAndTexture;

	// The viewports may need adjusting. Updating viewport 0 causes it to import the new window pixel size, which
	// cascades down to child viewports. All content in the viewports also gets marked for refreshing.
	// This must be done for all metastates in the hub stack.
	for i := 0 to HubStack.stackIndex do begin
		HubStack.SelectHubLevel(i);
		if length(Viewportmatic.viewport) <> 0 then Viewportmatic.viewport[0].Update;
	end;
end;

procedure HandleSDLevent(evd : PSDL_event);
// Call this with each new SDL event that comes in.

	procedure _HandleKeyPress(sym : longint; modifier : word);
	// The symbol is an SDL virtual keycode, and the modifier is an SDL_Keymod. Use this for any direct keyboard input,
	// but NOT for typing in any strings. Typed strings require bonus localisation handling.
	var s : string[2];
	begin
		// === Keyboard shortcuts ===
		if (modifier and KMOD_CTRL <> 0) and (sym >= SDLK_A) and (sym <= SDLK_Z) then begin
			case sym of
				SDLK_B: UserInput_CtrlB;
				SDLK_D: UserInput_CtrlD;
				SDLK_F: UserInput_CtrlF;
				SDLK_I: UserInput_CtrlI;
				SDLK_L: Userinput_CtrlL;
				SDLK_M: UserInput_CtrlM;
				SDLK_S: UserInput_CtrlS;
				SDLK_T: UserInput_CtrlT;
				SDLK_V: UserInput_CtrlV;

				// Debug mode activation.
				SDLK_X: if sysvar.debugAllowed = 0 then inc(sysvar.debugAllowed);
				SDLK_Y: if sysvar.debugAllowed in [1,4] then inc(sysvar.debugAllowed);
				SDLK_Z: if sysvar.debugAllowed in [2,3] then inc(sysvar.debugAllowed);
			end;
		end

		// === Cursor keys, Enter, ESC, etc ===
		else case sym of
			SDLK_RETURN, SDLK_RETURN2, SDLK_KP_ENTER: UserInput_Enter;
			SDLK_ESCAPE: UserInput_Esc;
			SDLK_APPLICATION: UserInput_Menu;
			SDLK_F2, SDLK_MUTE, SDLK_AUDIOMUTE: // are those two mutes really the same?
			with sysvar do begin
				mutedAudio := NOT mutedAudio;
				soundserver.PausePlayback(mutedAudio);
			end;
			SDLK_F10: if modifier and KMOD_SHIFT <> 0 then UserInput_Menu;
			SDLK_RIGHT, SDLK_KP_6: UserInput_Right(modifier and KMOD_CTRL <> 0);
			SDLK_LEFT, SDLK_KP_4: UserInput_Left(modifier and KMOD_CTRL <> 0);
			SDLK_DOWN, SDLK_KP_2: UserInput_Down(modifier and KMOD_CTRL <> 0);
			SDLK_UP, SDLK_KP_8: UserInput_Up(modifier and KMOD_CTRL <> 0);
			SDLK_BACKSPACE: UserInput_Backspace;
			SDLK_HOME, SDLK_KP_7: UserInput_Home(modifier and KMOD_CTRL <> 0);
			SDLK_END, SDLK_KP_1: UserInput_End(modifier and KMOD_CTRL <> 0);
			SDLK_PAGEUP, SDLK_KP_9: UserInput_PageUp;
			SDLK_PAGEDOWN, SDLK_KP_3: UserInput_PageDown;
			SDLK_DELETE: UserInput_Delete;
			SDLK_A..SDLK_Z, SDLK_0..SDLK_9:
			if BoxHub.activeTextInput = 0 then begin
				s := char(sym) + #0;
				UserInput_TextInput(@s[1]);
			end;

			//else log(strcat('Unused keydown: % ($&) mod $&', [sym, sym, modifier]));
		end;
	end;

begin
	// A few keyboard commands must be handled early regardless of gamemode, and various system events must be handled
	// no matter what.
	case evd^.type_ of
		//SDL_KEYUP:
		//with evd^.key.keysym do
		//log('>> keyup ' + strdec(sym) + ' mod $' + strhex(_mod));

		SDL_KEYDOWN:
		with evd^.key do begin
			// A fullscreen switch is disruptive; any keydown events appearing instantly after it are likely junk.
			if sysvar.swallowremaininguserinput then exit;

			// Try to swallow auto-repeated modified keypresses. Otherwise holding down any ctrl/alt + key combo gets
			// normal keyboard repeat, which is basically never the expected behavior.
			if keysym._mod <> 0 then
				if (_repeat <> 0)
					then exit;

			// Modifier keys by themselves, can't use "in" because values exceed 255.
			//   [SDLK_LALT, SDLK_LCTRL, SDLK_LSHIFT, SDLK_LGUI,
			//   SDLK_RALT, SDLK_RCTRL, SDLK_RSHIFT, SDLK_RGUI]
			// Assuming the values remain constant, this comparison covers them all.
			if (keysym.sym >= SDLK_LCTRL) and (keysym.sym <= SDLK_RGUI) then exit;

			// The pause button.
			if (keysym.sym = SDLK_PAUSE)
			or (keysym._mod and KMOD_CTRL <> 0)
			and (keysym.sym = SDLK_P)
			then begin
				sakuparam.framedelay := 0;
				// Shift-pause (modifiers $1 and $2) goes to single-step mode, otherwise normal pause function.
				if keysym._mod and KMOD_SHIFT <> 0 then
					SetPauseState(EPauseState.Single)
				else
					UserInput_CtrlP;
				exit;
			end;

			// Alt-Enter = 13 / $100 or $240
			if (keysym.sym = SDLK_RETURN)
			or (keysym.sym = SDLK_RETURN2)
			or (keysym.sym = SDLK_KP_ENTER) then
				if (keysym._mod = KMOD_LALT)
				or (keysym._mod = KMOD_RALT)
				or (keysym._mod = $240) // alt-gr
				then begin
					sysvar.fullscreen := NOT sysvar.fullscreen;
					SpawnWindow;
					exit;
				end;

			// ctrl-q = 113 / $40 and 113 / $80
			if (keysym.sym = SDLK_Q) and (keysym._mod and $C0 <> 0) then
				evd^.type_ := SDL_QUITEV;
		end;

		// Gamepad pause command.
		SDL_CONTROLLERBUTTONDOWN:
		if evd^.cbutton.button = SDL_CONTROLLER_BUTTON_START then UserInput_CtrlP;

		SDL_CONTROLLERDEVICEADDED:
		begin
			log('Controller added');
			InitGamepad;
		end;
		SDL_CONTROLLERDEVICEREMOVED:
		begin
			log('Controller removed');
			InitGamepad;
		end;

		// If this happens, the renderer has been wrecked? Can happen when the user task switches while in fullscreen.
		// Redrawing the whole screen should set things right.
		SDL_RENDER_TARGETS_RESET:
		begin
			log('RENDER TARGETS RESET');
			Rendermatic.Reset;
		end;

		SDL_RENDER_DEVICE_RESET: log('RENDER DEVICE RESET - DO SOMETHING!?');
	end;

	if evd^.type_ = SDL_QUITEV then begin
		sysvar.autoChoice := FALSE;
		sysvar.skipText := ESkipState.No;
		if sysvar.pauseState <> EPauseState.Normal then SetPauseState(EPauseState.Normal);
		SummonConfirmQuit;
		exit;
	end;

	// If we're paused, anything else must be ignored.
	if sysvar.pauseState = EPauseState.Paused then exit;

	case evd^.type_ of
		SDL_KEYDOWN: _HandleKeyPress(evd^.key.keysym.sym, evd^.key.keysym._mod);
		SDL_TEXTINPUT: UserInput_TextInput(pchar(evd^.text.text));
		SDL_MOUSEMOTION: UserInput_Mouse(evd^.motion.x, evd^.motion.y, 0);
		SDL_MOUSEBUTTONDOWN: UserInput_Mouse(evd^.button.x, evd^.button.y, evd^.button.button);
		//SDL_MOUSEBUTTONUP: writeln('Musbutt up: ',evd^.button.button);
		SDL_MOUSEWHEEL: UserInput_Wheel(evd^.wheel.y);
		SDL_CONTROLLERBUTTONDOWN: UserInput_GamepadButtonDown(evd^.cbutton.button);
		SDL_CONTROLLERBUTTONUP: UserInput_GamepadButtonUp(evd^.cbutton.button);
		SDL_CONTROLLERAXISMOTION: UserInput_GamepadAxis(evd^.caxis.axis, evd^.caxis.value);
	end;
end;

procedure HandleKeyRepeat(tickcount : dword); inline;
// This handles gamepad button repeat.
begin
	if tickcount < sysvar.keyRepeatAfterMsecs then
		dec(sysvar.keyRepeatAfterMsecs, tickcount)
	else begin
		// Is any button still held down?
		if sysvar.keysDown = [] then begin
			// No, stop repeating.
			sysvar.keyRepeatAfterMsecs := 0;
		end
		else begin
			// Yes, retrigger all held down buttons and reset delay.
			if EArrows.Up in sysvar.keysDown then
				UserInput_GamepadButtonDown(SDL_CONTROLLER_BUTTON_DPAD_UP);
			if EArrows.Down in sysvar.keysDown then
				UserInput_GamepadButtonDown(SDL_CONTROLLER_BUTTON_DPAD_DOWN);
			if EArrows.Left in sysvar.keysDown then
				UserInput_GamepadButtonDown(SDL_CONTROLLER_BUTTON_DPAD_LEFT);
			if EArrows.Right in sysvar.keysDown then
				UserInput_GamepadButtonDown(SDL_CONTROLLER_BUTTON_DPAD_RIGHT);
			sysvar.keyRepeatAfterMsecs := 160;
		end;
	end;
end;

// ------------------------------------------------------------------

procedure InitSDL;
var i : longint;

	procedure _DoInit(initsys : dword; const sysnamu : string);
	begin
		i := SDL_InitSubSystem(initsys);
		if i <> 0 then raise Exception.Create(strcat('Failed to init %: % %', [sysnamu, i, SDL_GetError]));
		Log(sysnamu + ' inited');
	end;
begin
	fillbyte(Rendermatic.SDL, sizeof(Rendermatic.SDL), 0);
	sdl_GamepadH := NIL;

	Log(strcat('SDL headers: %.%.%', [SDL_MAJOR_VERSION, SDL_MINOR_VERSION, SDL_PATCHLEVEL]));

	// Evidently this is needed to avoid an exception if GDB is being used...
	//SDL_SetHint(SDL_HINT_WINDOWS_DISABLE_THREAD_NAMING, '1');

	_DoInit(SDL_INIT_EVENTS, 'SDL_events');
	_DoInit(SDL_INIT_TIMER, 'SDL_timer');

	try
		_DoInit(SDL_INIT_VIDEO, 'SDL_video');
	except on e : Exception do begin
		Log(e.Message);
		Log('Video init failed, try to force software rendering...');
		SDL_SetHintWithPriority(SDL_HINT_RENDER_DRIVER, 'software', SDL_HINT_OVERRIDE);
		_DoInit(SDL_INIT_VIDEO, 'SDL_video');
	end; end;
	_DoInit(SDL_INIT_AUDIO, 'SDL_audio');
	_DoInit(SDL_INIT_GAMECONTROLLER, 'SDL_gamecontroller');
	if TTF_Init <> 0 then raise Exception.Create('TTF_Init: ' + TTF_GetError);

	i := SDL_GetNumVideoDrivers;
	Log(strdec(i) + ' video drivers');
	while i > 0 do begin
		dec(i);
		Log(strdec(i) + ': ' + SDL_GetVideoDriver(i));
	end;
	Log('Using driver: ' + SDL_GetCurrentVideoDriver);

	sdl_KeyState := SDL_GetKeyboardState(NIL);
	if sdl_KeyState = NIL then raise Exception.Create('SDL_GetKbState: ' + SDL_GetError);
end;

procedure InitAudio;
var font : PChar;
	i : dword;
const defaults : array of PChar = (
	'/usr/share/sounds/sf2/default-GM.sf2',
	'/usr/share/sounds/sf3/default-GM.sf3',
	'/usr/share/soundfonts/freepats-general-midi.sf2',
	'/usr/share/sounds/sf2/FluidR3_GM.sf2',
	'/usr/share/soundfonts/FluidR3_GM.sf2'
	);
begin
	with preference.audio do begin
		Log(strcat('audio = %', [strenum(typeinfo(driver), @driver)]));

		soundserver := TBSoundServer.Create;
		try
			soundserver.SetSink(driver);
		except on e : Exception do begin
			LogError('Failed to set audio sink', e, ExceptAddr);
			soundserver.SetSink(TS_NONE);
			exit;
		end; end;

		if driver = TS_FLUID then begin
			soundserver.fluidDriverName := PChar(preference.audio.driverName);
			// Load the soundfont.
			try
				if soundFontPath <> '' then
					font := @soundFontPath[1]
				else begin
					{$ifndef UNIX}LogError('[!] no soundfont specified'); exit;{$endif}
					font := '';
					for i := high(defaults) downto 0 do
						if FileExists(defaults[i]) then begin
							font := defaults[i];
							break;
						end;
					if font = '' then begin
						LogError('[!] no soundfont specified, no default ones found');
						exit;
					end;
				end;
				soundserver.AddFluidSoundFont(font);
				Log('using soundfont ' + font);
			except on e : Exception do begin
				LogError('Failed to set soundfont "' + font + '"', e, ExceptAddr);
			end; end;
		end;
	end;
end;

{$include base-all.pas}
