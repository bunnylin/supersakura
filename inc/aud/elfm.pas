{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// uses midi_instrument_matcher;

function Decomp_ElfM(const loader: TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Reads the indicated .M Elf music file, and saves it in outputfile as a normal midi file.
// Throws DecompException for bad failures, returns TRUE if successful.
var ssgdatastart, trackdatastart : dword;
begin
	result := FALSE;
	if loader.buffySize < 40 then raise DecompException.Create('file too tiny');
	// Offsets for three data blocks.
	if LEtoN(loader.ReadWord) <> 6 then raise DecompException.Create('bad first word');
	ssgdatastart := LEtoN(loader.ReadWord);
	trackdatastart := LEtoN(loader.ReadWord);
	if (ssgdatastart < 6) or (ssgdatastart > trackdatastart) then raise DecompException.Create('bad ssg data ofs');
	if trackdatastart >= loader.fullFileSize then raise DecompException.Create('bad track data ofs');

	raise SkipException.Create('elf M not supported yet');
	if outdir = basename then write(':)'); // silence compiler, we'll use them eventually
	result := TRUE;
end;

