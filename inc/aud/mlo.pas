{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// uses midi_instrument_matcher;

function Decomp_MLO(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Reads the indicated MLO music file, and saves it in outdir/basename as a normal midi file.
// Throws DecompException for bad failures, returns TRUE if successful.
var i, numtracks : dword;
	instrument_ofs : word;
	trackofs : array[0..15] of word;
begin
	result := FALSE;
	if loader.buffySize < 50 then raise DecompException.Create('file too tiny');

	if LEtoN(loader.ReadWord) <> 1 then raise DecompException.Create('sus 1st word');
	instrument_ofs := LEtoN(loader.ReadWord);
	if (instrument_ofs < 10) or (dword(instrument_ofs + 8) >= loader.fullFileSize) then
		raise DecompException.Create('bad instrument section');
	if LEtoN(loader.ReadWord) <> instrument_ofs - 4 then raise DecompException.Create('sus 3rd word');

	numtracks := 0;
	repeat
		if numtracks > 8 then raise DecompException.Create('too many tracks');
		i := LEtoN(loader.ReadWord);
		if i = 0 then break;
		if i = 1 then raise DecompException.Create('track len 1');
		if loader.ofs + i > instrument_ofs then raise DecompException.Create('track len oob');
		trackofs[numtracks] := loader.ofs;
		inc(numtracks);
		inc(loader.readp, i - 2);
	until FALSE;

	raise SkipException.Create('MLO music not supported yet');
	if outdir = basename then write(':)'); // silence compiler, we'll use them eventually
	result := TRUE;
end;

