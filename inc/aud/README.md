### Audio converters

For audio format documentation, see [/doc/mus](../../doc/mus).

- elfm: Elf's `M` FM music
- midi: Standard `MID`, RIFF `MDS`, Recomposer `RCP`, `SC5`, `MMD`
- mlo: `MLO` FM music
- muap98: Packen Software's `O` FM music
- pmd: Masahiro Kajihara's Professional Music Driver `M`, `M2` FM music
- wav: `WAV`, `MP3, `OGG``
