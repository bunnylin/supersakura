{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// uses midi_instrument_matcher;

function Decomp_PMD(const loader: TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Reads the indicated .M PMD music file, and saves it in outdir/basename as a normal midi file.
// Throws DecompException for bad failures, returns TRUE if successful.
const MODE_OPN = 0;
	MODE_OPM = 1;
	MODE_OPL = 2;
// Mapping of OPN percussion instruments to MIDI rhythm channel notes.
const percussionmap : array[0..10] of byte = (36, 38, 41, 45, 48, 37, 40, 42, 46, 49, 53); // 39,42,46,49,51
const pcm_rhythm_map : array[0..5] of byte = (36, 40, 59, 42, 47, 37);

type TTrackType = (FM_TRACK, SSG_TRACK, PCM_TRACK, RHYTHM_TRACK, RHYTHM_PATTERN);

var midiobject : TMidiWriter;
	outputbuffer : pointer;
	meta_track, instrument_track : dword;
	main_loop_begin_tick, main_loop_end_tick : dword;
	processing_limit_ticks, processing_granularity : dword;
	songmode, baseofs : byte;
	track, tracks_ended : byte;
	i : dword;

	pmdtrack : array[0..23] of record
		ptr, readptr, rhythm_return_ptr : dword;
		F6_loop_ofs : dword;
		staccato : record
			constant, relative, random, minimum_note_length : byte;
		end;
		track_type : TTrackType;
		midichannel : byte;
		inited, completed : boolean;

		track_volume : byte;
		track_transpose_semitones, track_detune_semitones : longint;
		current_MIDI_detune, current_PMD_detune : longint;
		SSG_pitch_interval_correction : boolean;
		SSG_env_depends_on_tempo : boolean; {$note todo: handle ssg envelope when tempo-independent}

		instrument : mim_TInstrument;
		instrument_changed : boolean;

		current_note : byte;
		note_is_on, rhythm_redirect : boolean;
		next_note_accent : longint;
		tie_to_next_note : boolean;
		current_MIDI_bend : longint; // starts at step $2000, range 0..3FFF
		target_MIDI_bend : longint; // +/- delta from current bend
		MIDI_bend_range_cents : dword; // MIDI pitch bend range is 0..16383 steps, by default maps to 4 semitones = 400 cents
		current_note_age, pitch_slide_duration : dword;
		ssg_envelope_level, ssg_phase : byte;
		ssg_phase_age : dword;

		LFO : array[1..2] of record
			depth : record
				current, evolve_rate_ticks, ticks_until_next : longint;
				evolved_counter, evolved_counter_max : byte;
				evolve_by : shortint;
			end;
			delay_after_noteon, step_rate_ticks, width_multiplier : byte; // LFO setup $F2 command: xx, yy, zz, aa
			waveform : byte;
			step_height : shortint;
			vibrato, tremolo, retrigger_on_note, is_active : boolean;
			rate_ignores_tempo : boolean; {$note todo: handle lfo rate when tempo-independent}
		end;

		repetition : record
			id : array[0..31] of word;
			counter : array[0..31] of byte;
			active_loops : byte;
		end;
	end;
	numtracks : byte;

	tempo : record
		PMD_tempo, PMD_timerB, MIDI_tempo : longint;
		ticks_per_whole_note : byte;
	end;
	globalvolume : record
		fm_attenuation, ssg_attenuation, pcm_attenuation, rhythm_attenuation : longint;
		master_pcm_rhythm_volume : longint;
		pcm_rhythm_volume : array[0..5] of longint;
	end;
	global_transpose_semitones : longint;
	pcm_rhythm_active : array[0..5] of boolean;

	procedure MatchInstrument;
	var l : longint;
		j : byte;
	begin
		with pmdtrack[track] do begin
			l := instrument.mimmatch;
			mim_MatchInstrument(instrument);
			instrument_changed := FALSE;

			// Some LFO configurations can be used for a sharp pitch dip, for a makeshift bass drum effect.
			if instrument.midimatch < 112 then
				for j := 1 to 2 do with LFO[j] do
					if (vibrato)
					and (delay_after_noteon = 0) and (step_rate_ticks = 1) and (step_height < -10) and (width_multiplier > 50)
					then instrument.midimatch := $F4;

			if (decomp_param.verbose) and (l <> instrument.mimmatch) then decomp_logger(
				strcat('[%] Track % instrument = midi %/%, mim %, bv %',
				[basename, track, instrument.midimatch, instrument.midimatch and $7F, instrument.mimmatch, instrument.base_velocity]));
		end;
	end;

	procedure SelectInstrument(index : dword);
	const opn_ops : array[0..3] of byte = (1, 3, 2, 4); // wonky storage order in file
	var j, oldreadofs : dword;
		definitionblocksize : byte;
	begin
		with pmdtrack[track] do begin
			if NOT (track_type in [FM_TRACK, PCM_TRACK]) then
				raise DecompException.Create('Can''t select instrument on this track');

			if track_type = FM_TRACK then begin
				oldreadofs := loader.ofs;
				loader.ofs := instrument_track;
				if songmode = MODE_OPL then definitionblocksize := 10 else definitionblocksize := 26;
				while (loader.ofs + definitionblocksize <= loader.buffySize) and (word(loader.readp^) <> LEtoN(word($FF00)))
				and (byte(loader.readp^) <> index) do
					inc(loader.readp, definitionblocksize);
				if (loader.ReadByte <> index) then raise DecompException.Create(
					'Instrument $' + strhex(index) + ' @ $' + strhex(oldreadofs - 1) + ' not found');

				if songmode = MODE_OPL then begin
					instrument.feedback := (byte((loader.readp + 8)^) shr 1) and 7;
					instrument.algorithm := byte((loader.readp + 8)^) and 1;
					for j := 1 to 2 do with instrument.op[j] do begin
						tremolo := byte(loader.readp^) and $80 <> 0;
						vibrato := byte(loader.readp^) and $40 <> 0;
						egt := byte(loader.readp^) and $20 <> 0;
						envscale := byte(loader.readp^) and $10 <> 0;
						freqmulfactor := byte(loader.readp^) and $F;
						keyscalelevel := byte((loader.readp + 2)^) shr 6;
						attenuation := byte((loader.readp + 2)^) and $3F;
						attackrate := byte((loader.readp + 4)^) shr 4;
						decayrate := byte((loader.readp + 4)^) and $F;
						sustainlevel := byte((loader.readp + 6)^) shr 4;
						releaserate := byte((loader.readp + 6)^) and $F;
						inc(loader.readp);
					end;
				end
				else begin
					instrument.feedback := (byte((loader.readp + 24)^) shr 3) and 7;
					instrument.algorithm := byte((loader.readp + 24)^) and 7;
					for j := 0 to 3 do with instrument.op[opn_ops[j]] do begin
						detune := (byte(loader.readp^) shr 4) and 7;
						freqmulfactor := byte(loader.readp^) and $F;
						attenuation := byte((loader.readp + 4)^) and $7F;
						keyscalelevel := byte((loader.readp + 8)^) shr 6;
						attackrate := byte((loader.readp + 8)^) and $1F;
						AMSmask := byte((loader.readp + 12)^) and $80 <> 0;
						decayrate := byte((loader.readp + 12)^) and $1F;
						sustainrate := byte((loader.readp + 16)^) and $1F;
						sustainlevel := byte((loader.readp + 20)^) shr 4;
						releaserate := byte((loader.readp + 20)^) and $F;
						inc(loader.readp);
					end;
				end;

				instrument_changed := TRUE;
				loader.ofs := oldreadofs;
			end;

			{if track_type = PCM_TRACK then begin
				raise DecompException.Create('Can''t select instrument on PCM track');
			end;}
		end;
	end;

	procedure SetTrackVolume(newvol : longint);
	begin
		with pmdtrack[track] do begin
			if newvol < 0 then newvol := 0;
			case track_type of
				FM_TRACK: if newvol > 127 then newvol := 127;
				SSG_TRACK, RHYTHM_TRACK, RHYTHM_PATTERN: if newvol > 15 then newvol := 15;
				PCM_TRACK: if newvol > 255 then newvol := 255;
			end;
			track_volume := newvol;
			if (rhythm_redirect) or (track_type in [RHYTHM_TRACK, RHYTHM_PATTERN]) then exit;

			// Scale to 0..127 MIDI volume range and apply master volume tweaks.
			// Attenuation defaults to 0, no attenuation.
			case track_type of
				FM_TRACK:
				begin
					newvol := round(newvol * (globalvolume.fm_attenuation xor $FF) / 255);
					newvol := round(sqrt(16129 / power(10, ((127 - newvol) * 0.75 * 0.05))));
				end;

				SSG_TRACK:
				begin
					newvol := round(newvol * (globalvolume.ssg_attenuation xor $FF) / 255);
					newvol := round(sqrt(16129 / power(10, ((15 - newvol) * 3 * 0.05))));
				end;

				PCM_TRACK:
				newvol := round(newvol * (globalvolume.pcm_attenuation xor $FF) / 510);

				RHYTHM_TRACK, RHYTHM_PATTERN:
				with globalvolume do
					newvol := round(newvol {* master_pcm_rhythm_volume / 63} * (rhythm_attenuation xor $FF) / 255 * 127 / 15);
			end;

			midiobject.track[midichannel].SetVolume(newvol);
		end;
	end;

	procedure SetSSGExpression(newlevel : longint);
	begin
		with pmdtrack[track] do begin
			if newlevel > 15 then newlevel := 15 else if newlevel < 0 then newlevel := 0;
			ssg_envelope_level := newlevel;
			midiobject.track[midichannel].SetExpression(mim_SSGVolumeToExpression[newlevel]);
		end;
	end;

	procedure TempoFromTimerB;
	begin
		tempo.PMD_tempo := round(4296 / (256 - tempo.PMD_timerB));
		tempo.MIDI_tempo := round(60000000 / 4296 * (256 - tempo.PMD_timerB));
		midiobject.SetTempo(midiobject.track[pmdtrack[track].midichannel].current_tick, tempo.MIDI_tempo);
	end;

	procedure TimerBFromTempo;
	begin
		tempo.PMD_timerB := 256 - round(4296 / tempo.PMD_tempo);
		tempo.MIDI_tempo := round(60000000 / tempo.PMD_tempo);
		midiobject.SetTempo(midiobject.track[pmdtrack[track].midichannel].current_tick, tempo.MIDI_tempo);
	end;

	procedure SetBend(midibend : longint = $2000);
	begin
		with pmdtrack[track] do begin
			if midibend < 0 then midibend := 0 else if midibend > $3FFF then midibend := $3FFF;
			midiobject.track[midichannel].SetPitchBend(midibend);
			current_MIDI_bend := midibend;
		end;
	end;

	procedure SetBendRange(semitones : byte = 4);
	var current_bend_cents : longint;
	begin
		with pmdtrack[track] do begin
			if MIDI_bend_range_cents = dword(semitones * 100) then exit;
			current_bend_cents := round((current_MIDI_bend - $2000) * longint(MIDI_bend_range_cents) / $4000);

			MIDI_bend_range_cents := semitones * 100;
			midiobject.track[midichannel].SetBendRange(MIDI_bend_range_cents);

			if current_bend_cents <> 0 then
				SetBend($2000 + round(current_bend_cents * $4000 / MIDI_bend_range_cents));
		end;
	end;

	procedure SetDetune(steps : longint = 0);
	var cents, j : longint;
	begin
		with pmdtrack[track] do begin
			current_PMD_detune := steps;
			// Try to calculate well-tempered MIDI cents from wonky PMD steps.
			if track_type = FM_TRACK then begin
				// This is roughly accurate up to a distance of 1 semitone, but beyond that it's not correct. See doc/pmd.txt.
				{l := current_note;
				// If detuning down, the correct span is the one below the current note, otherwise the one above.
				if steps < 0 then inc(l, $B);
				l := l mod $C; // ignore the octave
				j := ((6 * l + 74) * l + 27) div 55 + 36;}
				// Over longer distances the constant value 50 appears to work best. (25 for OPL.)
				j := 50;
				if songmode = MODE_OPL then j := j shr 1;
				cents := steps * 100 div j;
			end
			else begin
				if SSG_pitch_interval_correction then
					j := 256
				else begin
					// With correction disabled, the detune amount depends on which note is playing.
					// Let's just calibrate this against the most recent note, it's close enough.
					j := round(512 * power(0.5, current_note / 12));
					if j = 0 then j := 1;
				end;
				cents := steps * 100 div j;
			end;

			track_detune_semitones := round(cents / 100);
			current_MIDI_detune := cents - track_detune_semitones * 100;
			{if rhythm_redirect then
				midiobject.track[10].SetDetune(current_MIDI_detune, midiobject.track[midichannel].current_tick)
			else}
			if (NOT rhythm_redirect) and (NOT (track_type in [RHYTHM_TRACK, RHYTHM_PATTERN])) then
				midiobject.track[midichannel].SetDetune(current_MIDI_detune);
		end;
	end;

	procedure EnableLFO(lfonum : byte);
	var j : longint;
	begin
		with pmdtrack[track] do with LFO[lfonum] do begin
			if NOT is_active then begin
				depth.ticks_until_next := depth.evolve_rate_ticks;
				depth.evolved_counter := 0;
				depth.current := step_height;
				is_active := TRUE;
			end;

			j := depth.current * width_multiplier * 127;
			if track_type = FM_TRACK then begin
				// OPL is twice as sensitive, so should use 25, but 20 sounds better...
				if songmode = MODE_OPL then j := j div 20
				else j := j div 50;
			end
			else begin
				if SSG_pitch_interval_correction then
					j := j div 256
				else begin
					// With correction disabled, the vibrato depth depends on which note is playing.
					// Let's just calibrate this against the most recent note, it's close enough.
					j := round(j / (192 * power(0.5, current_note / 12)));
				end;
			end;
			if j < 0 then j := 0 else if j > 127 then j := 127;
			{$note todo: emulate advanced LFO types in TimeAhead}
			if vibrato then midiobject.track[pmdtrack[track].midichannel].SetVibrato(j);
			if tremolo then midiobject.track[pmdtrack[track].midichannel].SetTremolo(j); // <-- not correct
		end;
	end;

	procedure DisableLFO(lfonum : byte);
	begin
		with pmdtrack[track].LFO[lfonum] do if is_active then begin
			if vibrato then midiobject.track[pmdtrack[track].midichannel].SetVibrato(0);
			if tremolo then midiobject.track[pmdtrack[track].midichannel].SetTremolo(0);
			is_active := FALSE;
		end;
	end;

	function GetCurrentBentNote : longint;
	begin
		with pmdtrack[track] do
			GetCurrentBentNote := current_note + round((current_MIDI_bend - $2000) * longint(MIDI_bend_range_cents) / ($4000 * 100));
	end;

	procedure StartPitchSlide(tonote : longint; duration : dword);
	var distance_cents : longint;
		doubledist : dword;
	begin
		with pmdtrack[track] do begin
			if tonote < 0 then tonote := 0 else if tonote > 127 then tonote := 127;

			distance_cents := (tonote - current_note) * 100;

			// Adjust MIDI bend range if necessary... hopefully the playback device supports this.
			doubledist := abs(distance_cents) shl 1;
			if doubledist > MIDI_bend_range_cents then SetBendRange(doubledist div 100)
			else if (doubledist < MIDI_bend_range_cents) then
				if doubledist > 400 then SetBendRange(doubledist div 100) else SetBendRange(4);

			target_MIDI_bend := distance_cents * $4000 div longint(MIDI_bend_range_cents) - current_MIDI_bend + $2000;
			pitch_slide_duration := duration;
		end;
	end;

	procedure NoteOff(velo : dword = $20);
		function slowestrelease : byte;
		var i : byte;
		begin
			slowestrelease := $FF;
			case pmdtrack[track].instrument.instrument_type of
				MIM_TIT_OPL: i := 2;
				MIM_TIT_OPN: i := 4;
				else i := 1;
			end;
			while i <> 0 do with pmdtrack[track].instrument.op[i] do begin
				if (NOT muted) and (mim_IsCarrier(pmdtrack[track].instrument, i)) and (releaserate < slowestrelease) then
					slowestrelease := releaserate;
				dec(i);
			end;
		end;
	begin
		with pmdtrack[track] do begin
			// Consider volume envelope release rate.
			inc(velo, slowestrelease shl 3);
			if velo > 127 then velo := 127;

			if rhythm_redirect then
				midiobject.track[10].NoteOff(current_note, velo, midiobject.track[midichannel].current_tick)
			else
				midiobject.track[midichannel].NoteOff(current_note, velo);
			note_is_on := FALSE;
		end;
	end;

	function GetNoteOnVelocity : longint;
	var target_amp : single;
	begin
		with pmdtrack[track] do begin
			// Output amplitude as fraction = (MIDI velocity / 127)^2
			target_amp := instrument.base_velocity / 127;
			target_amp := target_amp * target_amp;

			// Rhythm_redirect means any note on this track is actually played on the midi rhythm channel. Since
			// multiple tracks can have rhythm at the same time, their track volume changes can't just be combined
			// on the rhythm channel, since those would override each other. Instead, the source track's volume
			// must be applied against each rhythm note's velocity.
			if rhythm_redirect then begin
				if track_type = FM_TRACK then
					dec(next_note_accent, 127 - track_volume)
				else
					dec(next_note_accent, 15 - track_volume);
			end;

			if next_note_accent <> 0 then begin
				// A PMD note accent is a direct FM/SSG volume add/subtract.
				// FM/SSG volumes have a linear relationship with amplitude measured in dB.
				// So the accent can be converted to an output amplitude fraction. The existing base velocity
				// in target_amp is already an amplitude fraction. The two are combined by multiplying.
				if track_type = FM_TRACK then
					target_amp := target_amp * power(10, next_note_accent * 0.0375)
				else
					target_amp := target_amp * power(10, next_note_accent * 0.15);

				next_note_accent := 0;
			end;

			// Convert back to a velocity.
			result := round(sqrt(target_amp) * 127);
			if result <= 0 then result := 1 else
			if result > $7F then result := $7F;
		end;
	end;

	procedure NoteOn(new_note : longint; duration : longint = -1);
	begin
		with pmdtrack[track] do begin
			if new_note < 0 then new_note := 0 else if new_note > 127 then new_note := 127;

			// Notes are played either by a direct note command or a start pitch slide command.
			// (Ongoing pitch slides trigger notes separately if they need to.)
			// In each case, either a note is already playing or isn't.
			// If a note is playing but tie is not active, the note needs to be automatically offed.
			// If a note is playing and tie is active, the note must be instantly set to the new pitch.

			if note_is_on then begin
				midiobject.track[midichannel].SetPortamento(tie_to_next_note, 0);
				if NOT tie_to_next_note then
					NoteOff
				else
					StartPitchSlide(new_note, 1);
			end;

			if NOT note_is_on then begin
				with LFO[1] do if (is_active) and (retrigger_on_note) and (delay_after_noteon <> 0) then DisableLFO(1);
				with LFO[2] do if (is_active) and (retrigger_on_note) and (delay_after_noteon <> 0) then DisableLFO(2);

				if (instrument.midimatch < $80) then begin
					if rhythm_redirect then begin
						rhythm_redirect := FALSE;
						// Volume changes during rhythm_redirect aren't output on the midi track, so if the same track
						// intersperses rhythm and melody, re-set the track volume on switch to melodic.
						SetTrackVolume(track_volume);
					end;
					midiobject.track[midichannel].SelectInstrument(instrument.midimatch);
					midiobject.track[midichannel].NoteOn(new_note, GetNoteOnVelocity);
					if instrument.instrument_type = MIM_TIT_SSG then begin
						// Translate non-noise SSG volume envelope to MIDI expression commands.
						ssg_phase := 0;
						SetSSGExpression(instrument.op[1].startlevel);
					end;
				end
				else begin
					// Percussion instrument, always goes on channel 10.
					rhythm_redirect := TRUE;
					new_note := mim_CheckDynamicPercussion(instrument, new_note, duration, tempo.PMD_tempo * 48);
					midiobject.track[10].NoteOn(new_note, GetNoteOnVelocity, midiobject.track[midichannel].current_tick);
				end;
				SetBend;
				current_note := new_note;
				current_note_age := 0;
				note_is_on := TRUE;
			end;
			tie_to_next_note := FALSE;
		end;
	end;

	procedure TimeAhead(ticks : dword);
	var new_note, l : longint;
		j : byte;
	begin
		with pmdtrack[track] do while ticks <> 0 do begin
			inc(midiobject.track[midichannel].current_tick);
			if note_is_on then begin
				inc(current_note_age);

				if NOT rhythm_redirect then begin
					if instrument.instrument_type = MIM_TIT_SSG then begin
						// Translate non-noise SSG volume envelope to MIDI expression commands.
						{$note todo: some FM instruments need active enveloping too, select which env phases to apply per template}
						case ssg_phase of
							0:
							begin
								case instrument.op[1].attackrate of
									0..16:
									if current_note_age mod byte(18 - instrument.op[1].attackrate) = 0 then
										SetSSGExpression(ssg_envelope_level + 1);
									else SetSSGExpression(ssg_envelope_level + instrument.op[1].attackrate - 16);
								end;
								if ssg_envelope_level = 15 then begin
									ssg_phase := 1;
									ssg_phase_age := 0;
									if instrument.op[1].decayrate = 0 then ssg_phase := 2;
								end;
							end;

							1:
							begin
								inc(ssg_phase_age);
								l := ssg_envelope_level;
								case instrument.op[1].decayrate of
									1..16: if ssg_phase_age mod byte(34 - instrument.op[1].decayrate * 2) = 0 then dec(l);
									else inc(l, 16 - instrument.op[1].decayrate);
								end;
								if l <= 15 - instrument.op[1].sustainlevel then begin
									l := 15 - instrument.op[1].sustainlevel;
									ssg_phase := 2;
									ssg_phase_age := 0;
								end;
								SetSSGExpression(l);
							end;

							2:
							if instrument.op[1].sustainrate <> 0 then begin
								inc(ssg_phase_age);
								case instrument.op[1].sustainrate of
									1..16: if ssg_phase_age mod byte(34 - instrument.op[1].sustainrate * 2) = 0 then
										SetSSGExpression(ssg_envelope_level - 1);
									else SetSSGExpression(ssg_envelope_level + 16 - instrument.op[1].sustainrate);
								end;
								if ssg_envelope_level = 0 then NoteOff(127);
							end;

							3:
							if instrument.op[1].releaserate <> 0 then begin
								inc(ssg_phase_age);
								case instrument.op[1].releaserate of
									1..8: if ssg_phase_age mod byte(18 - instrument.op[1].releaserate * 2) = 0 then
										SetSSGExpression(ssg_envelope_level - 1);
									else SetSSGExpression(ssg_envelope_level + 16 - instrument.op[1].releaserate * 2);
								end;
								if ssg_envelope_level = 0 then NoteOff(127);
							end;
						end;
					end;

					for j := 1 to 2 do with LFO[j] do begin
						if (is_active) and (depth.evolve_by <> 0) and (depth.evolve_rate_ticks <> 0)
						and ((depth.evolved_counter_max = 0) or (depth.evolved_counter < depth.evolved_counter_max)) then
						with depth do begin
							dec(ticks_until_next);
							if ticks_until_next <= 0 then begin
								inc(current, evolve_by);
								if current < 0 then current := 0 else if current > 127 then current := 127;
								inc(evolved_counter);
								EnableLFO(j);
								ticks_until_next := evolve_rate_ticks;
							end;
						end;
						if (NOT is_active) and (vibrato or tremolo) and (current_note_age >= delay_after_noteon) then
							EnableLFO(j);
					end;
				end;
			end;

			if target_MIDI_bend <> 0 then begin
				inc(current_MIDI_bend, target_MIDI_bend div longint(pitch_slide_duration));
				dec(target_MIDI_bend, target_MIDI_bend div longint(pitch_slide_duration));
				dec(pitch_slide_duration);

				new_note := current_note;
				if current_MIDI_bend < 0 then
					repeat
						dec(new_note, longint(MIDI_bend_range_cents div 100));
						inc(current_MIDI_bend, $4000);
					until current_MIDI_bend >= 0;
				if current_MIDI_bend > $4000 then
					repeat
						inc(new_note, longint(MIDI_bend_range_cents div 100));
						dec(current_MIDI_bend, $4000);
					until current_MIDI_bend <= $4000;
				if new_note <> current_note then begin
					midiobject.track[midichannel].SetPortamento(TRUE, 0);
					midiobject.track[midichannel].NoteOn(new_note, GetNoteOnVelocity);
					NoteOff($7F);
					current_note := new_note;
					note_is_on := TRUE;
				end;

				SetBend(current_MIDI_bend);
			end;

			dec(ticks);
		end;
	end;

	procedure ProcessTrackData;
	var j, l, endofs : dword;
		command : byte;
	begin
		with pmdtrack[track] do begin
			//decomp_logger(strcat('## reading track % from $&, curtick %, limit %', [track, loader.ofs, midiobject.track[midichannel].current_tick, processing_limit_ticks]));
			endofs := meta_track;
			if track_type = RHYTHM_PATTERN then
				endofs := instrument_track
			else if (track + 1 < numtracks) and (loader.ofs < pmdtrack[track + 1].ptr) then
				endofs := pmdtrack[track + 1].ptr;

			while midiobject.track[midichannel].current_tick <= processing_limit_ticks do begin

				if loader.ofs >= endofs then
					raise DecompException.Create(strcat('Track % overrun @ $&', [track, loader.ofs]));
				command := loader.ReadByte;
				//decomp_logger(strcat('track %: @&=$& (tracks ended %/%, this=%)', [track, loader.ofs, command, tracks_ended, numtracks, completed]));

				case command of
					$00..$7F:
					begin
						// Rhythm pattern call.
						if track_type = RHYTHM_TRACK then begin
							i := meta_track + command + command;
							if i + 1 >= loader.fullFileSize then
								raise DecompException.Create(strcat('Rhythm pattern % oob', [command]));
							i := LEtoN(word(loader.PtrAt(i)^)) + baseofs;
							if (i <= meta_track) or (i >= loader.buffySize) then
								raise DecompException.Create(strcat('Rhythm pattern % address $& oob', [command, i]));
							track_type := RHYTHM_PATTERN;
							endofs := instrument_track;
							rhythm_return_ptr := loader.ofs;
							loader.ofs := i;
							continue;
						end;

						// Rest.
						if command = $0F then begin
							if (note_is_on) and (NOT tie_to_next_note) then NoteOff;
							TimeAhead(loader.ReadByte);
							continue;
						end;

						// Notes.
						// Top nibble is octave, and lower nibble is the note. Convert it to a midi note, 12..107.
						if byte(command and $F) >= $C then
							raise DecompException.Create(strcat('bad note $& @ $&', [command, loader.ofs - 1]));
						i := byte(command and $F) + byte(command shr 4) * $C + 12;

						// j = total duration
						j := loader.ReadByte;
						// l = staccato portion of duration
						l := staccato.constant + j * staccato.relative div 255;
						if staccato.random <> 0 then
							inc(l, (loader.ofs * (loader.ofs shr 1)) mod staccato.random);
						if j - l < staccato.minimum_note_length then begin
							longint(l) := j - staccato.minimum_note_length;
							if longint(l) < 0 then l := 0;
						end;

						if instrument_changed then MatchInstrument;

						{if instrument.midimatch >= $80 then
							decomp_logger(strcat('Perc hit! % at note %, duration %',
							[instrument.midimatch and $7F, longint(i) + global_transpose_semitones + track_transpose_semitones + track_detune_semitones + instrument.transpose, j]
							));}
						NoteOn(longint(i) + global_transpose_semitones + track_transpose_semitones + track_detune_semitones + instrument.transpose, j - l);

						// If followed by a pitch slide from the same note, with 0 staccato, treat as implicit tie ahead.
						if (byte(loader.readp^) = $DA) and (byte((loader.readp + 1)^) = command) and (l = 0) then begin
							TimeAhead(j);
							tie_to_next_note := TRUE;
						end
						// If followed by tie/slur, ignore staccato, skip NoteOff.
						else if byte(loader.readp^) in [$C1, $FB] then
							TimeAhead(j)
						else begin
							// Otherwise send a NoteOff after the full duration minus staccato.
							TimeAhead(j - l);
							if (instrument.instrument_type = MIM_TIT_SSG) and (NOT rhythm_redirect) then begin
								ssg_phase := 3; ssg_phase_age := 0;
							end else
								NoteOff;
							TimeAhead(l);
						end;
					end;

					// End of track, or rhythm hit.
					$80..$87:
					if track_type <> RHYTHM_PATTERN then begin
						if command = $80 then begin
							//decomp_logger(strcat('track %, end $&, curtick %, F6-loopofs %', [track, loader.ofs, midiobject.track[midichannel].current_tick, longint(F6_loop_ofs)]));
							dec(loader.readp);
							if note_is_on then NoteOff;
							if F6_loop_ofs <> $FFFFFFFF then begin
								loader.ofs := F6_loop_ofs;
								processing_granularity := 1;
							end;

							if NOT completed then begin
								completed := TRUE;
								inc(tracks_ended);
								with midiobject.track[midichannel] do
									if current_tick > main_loop_end_tick then main_loop_end_tick := current_tick;
							end;
							break;
						end;
						raise DecompException.Create(strcat('Unknown command $& @ $&', [command, loader.ofs - 1]));
					end
					else begin
						i := ((command and $07) shl 8) + loader.ReadByte;
						//decomp_logger(strcat('& Rhythm', [i]));
						for j := 0 to 10 do
							if i and (1 shl j) <> 0 then
								midiobject.track[10].NoteOn(percussionmap[j], $44, midiobject.track[midichannel].current_tick);
						inc(midiobject.track[midichannel].current_tick, loader.ReadByte);
						for j := 0 to 10 do
							if i and (1 shl j) <> 0 then
								midiobject.track[10].NoteOff(percussionmap[j], $50, midiobject.track[midichannel].current_tick);
					end;

					$B1: staccato.random := loader.ReadByte;

					$B2: global_transpose_semitones := shortint(loader.ReadByte);

					$B3:
					begin
						staccato.minimum_note_length := loader.ReadByte;
						if staccato.minimum_note_length = 0 then staccato.minimum_note_length := 1;
					end;

					$B4:
					if (track_type = PCM_TRACK) and (loader.ofs = ptr + 1) then begin
						// Add up to 8 extra PPZ tracks.
						for j := 1 to 8 do begin
							i := LEtoN(loader.ReadWord);
							if (i <> 0) and (i < loader.buffySize) then begin
								pmdtrack[numtracks].ptr := i + baseofs;
								pmdtrack[numtracks].readptr := pmdtrack[numtracks].ptr;
								pmdtrack[numtracks].track_type := PCM_TRACK;
								inc(numtracks);
							end;
						end;
						midiobject.SetNumberOfTracks(numtracks + 1);
					end
					else raise DecompException.Create('B4 not up front on PCM track @ $' + strhex(loader.ofs - 1));

					// Set FM operator keyon delay directly.
					$B5: if track_type = FM_TRACK then inc(loader.readp, 2)
					else raise DecompException.Create('B5 outside FM track @ $' + strhex(loader.ofs - 1));

					// Set LFO depth evolution limit counter.
					$B7: begin
						i := loader.ReadByte;
						LFO[(i shr 7) + 1].depth.evolved_counter_max := i and $7F;
					end;

					// Set FM operator attenuation directly.
					$B8:
					if track_type = FM_TRACK then with instrument do begin
						i := loader.ReadByte;
						if i and $80 = 0 then begin // absolute
							if songmode = MODE_OPL then begin
								if i and 1 <> 0 then op[1].attenuation := byte(loader.readp^) shr 1;
								if i and 2 <> 0 then op[2].attenuation := byte(loader.readp^) shr 1;
							end
							else for l := 1 to 4 do begin
								if i and 1 <> 0 then op[l].attenuation := byte(loader.readp^);
								i := i shr 1;
							end;
						end
						else begin // relative
							if songmode = MODE_OPL then begin
								for l := 0 to 1 do if i and (1 shl l) <> 0 then begin
									longint(j) := op[l + 1].attenuation + shortint(loader.readp^) div 2;
									if longint(j) < 0 then j := 0 else if j > 63 then j := 63;
									op[l + 1].attenuation := j;
								end;
							end
							else begin
								for l := 0 to 3 do if i and (1 shl l) <> 0 then begin
									longint(j) := op[l + 1].attenuation + shortint(loader.readp^);
									if longint(j) < 0 then j := 0 else if j > 127 then j := 127;
									op[l + 1].attenuation := j;
								end;
							end;
						end;
						inc(loader.readp);
						instrument_changed := TRUE;
					end
					else raise DecompException.Create('B8 outside FM track @ $' + strhex(loader.ofs - 1));

					$B9: LFO[2].delay_after_noteon := loader.ReadByte;

					// Apply LFO2 directly to FM operators.
					$BA: if track_type = FM_TRACK then inc(loader.readp)
					else raise DecompException.Create('BA outside FM track @ $' + strhex(loader.ofs - 1));

					$BB: LFO[2].rate_ignores_tempo := (loader.ReadByte <> 0);

					$BC: begin
						i := loader.ReadByte;
						if i > 6 then
							raise DecompException.Create(strcat('LFO2 invalid waveform % @ $&', [i, loader.ofs - 2]));
						if (i >= 3) and (decomp_param.verbose) then decomp_logger('# LFO2 special waveform ' + strdec(i));
						LFO[2].waveform := i;
					end;

					// LFO2 depth evolution.
					$BD: with LFO[2] do begin
						depth.evolve_rate_ticks := loader.ReadByte * step_rate_ticks * width_multiplier * 4;
						depth.evolve_by := shortint(loader.ReadByte);
					end;

					$BE:
					with LFO[2] do begin
						i := loader.ReadByte;
						if track_type in [RHYTHM_TRACK, RHYTHM_PATTERN] then
							raise DecompException.Create('LFO2 on rhythm track @ $' + strhex(loader.ofs - 2));
						vibrato := (i and 1 <> 0);
						tremolo := (i and 2 <> 0);
						retrigger_on_note := (i and 4 = 0);
						if (vibrato) and (track_type = PCM_TRACK) then
							raise DecompException.Create('Vibrato on PCM track @ $' + strhex(loader.ofs - 2));
						if is_active then if (i and 3 <> 0) then EnableLFO(2) else DisableLFO(2);
						instrument_changed := TRUE; // catch makeshift bass drums
					end;

					$BF:
					with LFO[2] do begin
						delay_after_noteon := loader.ReadByte;
						step_rate_ticks := loader.ReadByte;
						step_height := shortint(loader.ReadByte);
						width_multiplier := loader.ReadByte;
						if is_active then EnableLFO(2);
						instrument_changed := TRUE; // catch makeshift bass drums
					end;

					$C0:
					with globalvolume do begin
						i := loader.ReadByte;
						case i of
							0, 1: decomp_logger('C0 track mute @ $' + strhex(loader.ofs - 2));
							$F7: inc(loader.readp); // set PCM channel mode to PMD86-PCM/PMDB2-ADPCM

							$F8: inc(rhythm_attenuation, shortint(loader.ReadByte));
							$F9: rhythm_attenuation := loader.ReadByte;
							$FA: inc(pcm_attenuation, shortint(loader.ReadByte));
							$FB: pcm_attenuation := loader.ReadByte;
							$FC: inc(ssg_attenuation, shortint(loader.ReadByte));
							$FD: ssg_attenuation := loader.ReadByte;
							$FE: inc(fm_attenuation, shortint(loader.ReadByte));
							$FF: fm_attenuation := loader.ReadByte;
							else raise DecompException.Create(strcat('Unknown command C0 & @ $&', [i, loader.ofs - 2]));
						end;

						if track <> 6 then decomp_logger('C0 outside track 6 @ $' + strhex(loader.ofs - 2));
						if midiobject.track[pmdtrack[track].midichannel].current_tick <> 0 then
							decomp_logger('C0 not up front @ $' + strhex(loader.ofs - 2));

						case i of
							$F8, $F9:
							begin
								if rhythm_attenuation < 0 then rhythm_attenuation := 0
								else if rhythm_attenuation > 255 then rhythm_attenuation := 255;
							end;
							$FA, $FB:
							begin
								if pcm_attenuation < 0 then pcm_attenuation := 0
								else if pcm_attenuation > 255 then pcm_attenuation := 255;
							end;
							$FC, $FD:
							begin
								if ssg_attenuation < 0 then ssg_attenuation := 0
								else if ssg_attenuation > 255 then ssg_attenuation := 255;
							end;
							$FE, $FF:
							begin
								if fm_attenuation < 0 then fm_attenuation := 0
								else if fm_attenuation > 255 then fm_attenuation := 255;
								SetTrackVolume(pmdtrack[track].track_volume);
							end;
						end;
					end;

					// Slur, handled in conjunction with NoteOn and PitchSlide.
					$C1: ;

					$C2: LFO[1].delay_after_noteon := loader.ReadByte;

					$C4: staccato.relative := loader.ReadByte;

					// Apply LFO1 directly to FM operators.
					$C5: if track_type = FM_TRACK then inc(loader.readp)
					else raise DecompException.Create('C5 outside FM track @ $' + strhex(loader.ofs - 1));

					$C6: begin
						if (track <> 0) then raise DecompException.Create('C6 not up front @ $' + strhex(loader.ofs - 1));
						// Add up to 3 extra FM tracks.
						for j := 1 to 3 do begin
							i := LEtoN(loader.ReadWord);
							if (i <> 0) and (i < loader.buffySize) then begin
								pmdtrack[numtracks].ptr := i + baseofs;
								pmdtrack[numtracks].readptr := pmdtrack[numtracks].ptr;
								pmdtrack[numtracks].track_type := FM_TRACK;
								inc(numtracks);
							end;
						end;
						midiobject.SetNumberOfTracks(numtracks + 1);
					end;

					// Adjust FM op detune directly.
					$C7:
					if (track_type = FM_TRACK) and (songmode <> MODE_OPL) then with instrument do begin
						i := loader.ReadByte;
						if i and 1 <> 0 then op[1].detune := byte(op[1].detune + smallint(loader.readp^));
						if i and 2 <> 0 then op[2].detune := byte(op[2].detune + smallint(loader.readp^));
						if i and 4 <> 0 then op[3].detune := byte(op[3].detune + smallint(loader.readp^));
						if i and 8 <> 0 then op[4].detune := byte(op[4].detune + smallint(loader.readp^));
						inc(loader.readp, 2);
						instrument_changed := TRUE;
					end
					else raise DecompException.Create('C7 outside OPN FM track @ $' + strhex(loader.ofs - 1));

					// Set FM op detune directly.
					$C8:
					if (track_type = FM_TRACK) and (songmode <> MODE_OPL) then with instrument do begin
						i := loader.ReadByte;
						if i and 1 <> 0 then op[1].detune := byte(loader.readp^);
						if i and 2 <> 0 then op[2].detune := byte(loader.readp^);
						if i and 4 <> 0 then op[3].detune := byte(loader.readp^);
						if i and 8 <> 0 then op[4].detune := byte(loader.readp^);
						inc(loader.readp, 2);
						instrument_changed := TRUE;
					end
					else raise DecompException.Create('C8 outside OPN FM track @ $' + strhex(loader.ofs - 1));

					// Set volume envelope adjustment by tempo.
					$C9: if track_type = SSG_TRACK then
						SSG_env_depends_on_tempo := (loader.ReadByte <> 0)
					else if track_type in [PCM_TRACK, RHYTHM_TRACK] then
						inc(loader.readp)
					else raise DecompException.Create(strcat('C9 on non-SSG/PCM track % @ $&', [track, loader.ofs - 1]));

					$CA: LFO[1].rate_ignores_tempo := (loader.ReadByte <> 0);

					$CB: begin
						i := loader.ReadByte;
						if i > 6 then
							raise DecompException.Create(strcat('LFO1 invalid waveform % @ $&', [i, loader.ofs - 2]));
						if (i >= 3) and (decomp_param.verbose) then decomp_logger('# LFO1 special waveform ' + strdec(i));
						LFO[1].waveform := i;
					end;

					// Set SSG pitch interval correction.
					$CC: if track_type = SSG_TRACK then
						SSG_pitch_interval_correction := (loader.ReadByte <> 0)
					else raise DecompException.Create('CC outside SSG track @ $' + strhex(loader.ofs - 1));

					// Set volume envelope parameters, extended.
					$CD:
					case track_type of
						SSG_TRACK:
						with instrument.op[1] do begin
							attackrate := loader.ReadByte;
							decayrate := loader.ReadByte;
							sustainrate := loader.ReadByte;
							i := loader.ReadByte;
							sustainlevel := i shr 4;
							releaserate := i and $F;
							startlevel := loader.ReadByte;
							instrument_changed := TRUE;
						end;

						PCM_TRACK: inc(loader.readp, 5);

						else begin
							inc(loader.readp, 5);
							decomp_logger(strcat('CD on non-SSG/PCM track % @ $&', [track, loader.ofs - 1]));
						end;
					end;

					// Set FM operators to use.
					$CF:
					begin
						if track_type <> FM_TRACK then
							raise DecompException.Create('CF outside FM track @ $' + strhex(loader.ofs - 1));
						i := loader.ReadByte;
						if songmode = MODE_OPL then begin
							instrument.op[1].muted := (i and $10 = 0);
							instrument.op[2].muted := (i and $20 = 0);
						end
						else begin
							instrument.op[1].muted := (i and $10 = 0);
							instrument.op[2].muted := (i and $20 = 0);
							instrument.op[3].muted := (i and $40 = 0);
							instrument.op[4].muted := (i and $80 = 0);
						end;
						instrument_changed := TRUE;
					end;

					// Adjust noise generation frequency.
					$D0:
					begin
						if track_type <> SSG_TRACK then
							raise DecompException.Create('D0 outside SSG track @ $' + strhex(loader.ofs - 1));
						longint(i) := instrument.op[1].noisefreq + shortint(loader.ReadByte);
						if longint(i) < 0 then longint(i) := 0 else if i > 255 then i := 255;
						instrument.op[1].noisefreq := i;
						instrument_changed := TRUE;
					end;

					$D2:
					begin
						decomp_logger(strcat('D2-& global fadeout @ $&', [loader.ReadByte, loader.ofs - 1]));
					end;

					// Play FM sound effect.
					$D3: inc(loader.readp);

					// Play SSG sound effect.
					$D4: inc(loader.readp);

					$D5: SetDetune(current_PMD_detune + smallint(LEtoN(loader.ReadWord)));

					// LFO1 depth evolution.
					$D6: with LFO[1] do begin
						depth.evolve_rate_ticks := loader.ReadByte * step_rate_ticks * width_multiplier * 4;
						depth.evolve_by := shortint(loader.ReadByte);
					end;

					$DA:
					begin
						if track_type in [RHYTHM_TRACK, RHYTHM_PATTERN] then
							raise DecompException.Create('Pitch slide on rhythm track @ $' + strhex(loader.ofs - 1));

						// Top nibble is octave, and lower nibble is the note. Convert it to a midi note, 12..107.
						i := loader.ReadByte; // from note
						inc(loader.readp); // skip the to-note byte for a moment...
						if byte(i and $F) >= $C then
							raise DecompException.Create('bad note $' + strhex(i) + ' @ $' + strhex(loader.ofs - 1));
						i := byte(i and $F) + byte(i shr 4) * $C + 12;

						// j = total duration
						j := loader.ReadByte;
						// l = staccato portion of duration
						l := staccato.constant + j * staccato.relative div 255;
						if staccato.random <> 0 then
							inc(l, (loader.ofs * (loader.ofs shr 1)) mod staccato.random);
						if j - l < staccato.minimum_note_length then begin
							longint(l) := j - staccato.minimum_note_length;
							if longint(l) < 0 then l := 0;
						end;

						if instrument_changed then MatchInstrument;

						NoteOn(
							longint(i) + global_transpose_semitones + track_transpose_semitones + track_detune_semitones + instrument.transpose, j - l);

						i := byte(loader.PtrAt(loader.ofs - 2)^); // to note
						if byte(i and $F) >= $C then
							raise DecompException.Create('bad note $' + strhex(i) + ' @ $' + strhex(loader.ofs - 1));
						i := byte(i and $F) + byte(i shr 4) * $C + 12;

						StartPitchSlide(
							longint(i) + global_transpose_semitones + track_transpose_semitones + track_detune_semitones + instrument.transpose, j);

						// If followed by tie/slur, ignore staccato, skip NoteOff.
						if byte(loader.readp^) in [$C1, $FB] then
							TimeAhead(j)
						else begin
							// Otherwise send a NoteOff after the full duration minus staccato.
							TimeAhead(j - l);
							if (instrument.instrument_type = MIM_TIT_SSG) and (NOT rhythm_redirect) then begin
								ssg_phase := 3; ssg_phase_age := 0;
							end else
								NoteOff;
							TimeAhead(l);
						end;
					end;

					// Adjust the "Status1" byte, save as a meta cue string.
					$DB:
					begin
						i := loader.ReadByte;
						if shortint(i) < 0 then
							midiobject.WriteMetaString(midiobject.track[midichannel].current_tick, 7, strdec(shortint(i)))
						else
							midiobject.WriteMetaString(midiobject.track[midichannel].current_tick, 7, '+' + strdec(i));
					end;

					// Set the "Status1" byte, save as a meta cue string.
					$DC: midiobject.WriteMetaString(midiobject.track[midichannel].current_tick, 7, strdec(loader.ReadByte));

					$DD, $DE:
					begin
						next_note_accent := loader.ReadByte;
						case track_type of
							FM_TRACK: if next_note_accent > 127 then next_note_accent := 127;
							SSG_TRACK: if next_note_accent > 15 then next_note_accent := 15;
						end;
						if command = $DD then next_note_accent := -next_note_accent;
					end;

					$DF: tempo.ticks_per_whole_note := loader.ReadByte;

					$E2: SetTrackVolume(track_volume - loader.ReadByte);

					$E3: SetTrackVolume(track_volume + loader.ReadByte);

					$E5:
					begin
						j := loader.ReadByte;
						if NOT (j in [1..6]) then
							raise DecompException.Create('E5 bad rhythm index @ $' + strhex(loader.ofs - 2));
						longint(i) := globalvolume.pcm_rhythm_volume[j - 1] + shortint(loader.ReadByte);
						if longint(i) < 0 then i := 0 else if i > 31 then i := 31;
						globalvolume.pcm_rhythm_volume[j - 1] := i;
					end;

					$E7:
					begin
						inc(track_transpose_semitones, shortint(loader.ReadByte));
						if track_transpose_semitones < -128 then track_transpose_semitones := -128
						else if track_transpose_semitones > 127 then track_transpose_semitones := 127;
					end;

					$E8:
					with globalvolume do begin
						master_pcm_rhythm_volume := loader.ReadByte;
						if master_pcm_rhythm_volume > 63 then master_pcm_rhythm_volume := 63;
					end;

					// Set rhythm instrument panning on this track.
					$E9: inc(loader.readp);

					$EA:
					begin
						i := loader.ReadByte;
						j := i shr 5;
						if j in [0,7] then raise DecompException.Create('EA bad rhythm index @ $' + strhex(loader.ofs - 2));
						globalvolume.pcm_rhythm_volume[j - 1] := i and $1F;
					end;

					$EB:
					begin
						i := loader.ReadByte;
						for j := 0 to 5 do
							if i and (1 shl j) <> 0 then begin
								//decomp_logger(strcat('EB &, % active %', [i, j, pcm_rhythm_active[j]]));
								if pcm_rhythm_active[j] then
									midiobject.track[10].NoteOff(pcm_rhythm_map[j], $7F, midiobject.track[midichannel].current_tick);
								if i and $80 = 0 then begin
									l := $7F * globalvolume.master_pcm_rhythm_volume * globalvolume.pcm_rhythm_volume[j] div (63 * 31);
									midiobject.track[10].NoteOn(pcm_rhythm_map[j], l, midiobject.track[midichannel].current_tick);
									pcm_rhythm_active[j] := TRUE;
								end;
							end;
					end;

					// Set track panning to xx.
					$EC: inc(loader.readp);

					// Select tone/noise generation.
					$ED:
					begin
						if track_type <> SSG_TRACK then
							raise DecompException.Create('ED outside SSG track @ $' + strhex(loader.ofs - 1));
						i := loader.ReadByte;
						instrument.enable_tone := (i and 7 <> 0);
						instrument.enable_noise := (i and $38 <> 0);
						instrument_changed := TRUE;
					end;

					// Set noise generation frequency to xx.
					$EE:
					begin
						instrument.op[1].noisefreq := loader.ReadByte;
						instrument_changed := TRUE;
					end;

					// Direct FM chip comms.
					$EF: inc(loader.readp, 2);

					// Set volume envelope parameters.
					$F0:
					case track_type of
						SSG_TRACK:
						with instrument.op[1] do begin
							i := loader.ReadByte; // attack length
							j := loader.ReadByte; // decay depth
							if shortint(j) > 0 then begin
								startlevel := 0;
								if j < 15 then startlevel := 15 - j;
								sustainlevel := 0;
							end
							else begin
								startlevel := 15;
								sustainlevel := -shortint(j);
							end;
							case i of
								1..15: attackrate := (startlevel + i shr 1) div i + 16;
								16..31: attackrate := (startlevel + i shr 1) div i;
								else attackrate := 31;
							end;
							decayrate := 31;
							i := loader.ReadByte; // sustain rate
							case i of
								0: sustainrate := 0;
								1..31: sustainrate := 17 - i shr 1;
								else sustainrate := 1;
							end;
							i := loader.ReadByte; // release rate
							case i of
								0: releaserate := 15;
								1..15: releaserate := 9 - i shr 1;
								else releaserate := 1;
							end;
							instrument_changed := TRUE;
						end;

						PCM_TRACK: inc(loader.readp, 4);
						else raise DecompException.Create('F0 on FM/Rhythm track @ $' + strhex(loader.ofs - 2))
					end;

					$F1:
					with LFO[1] do begin
						i := loader.ReadByte;
						if track_type in [RHYTHM_TRACK, RHYTHM_PATTERN] then
							raise DecompException.Create('LFO1 on rhythm track @ $' + strhex(loader.ofs - 2));
						vibrato := (i and 1 <> 0);
						tremolo := (i and 2 <> 0);
						retrigger_on_note := (i and 4 = 0);
						if (vibrato) and (track_type = PCM_TRACK) then
							raise DecompException.Create('Vibrato on PCM track @ $' + strhex(loader.ofs - 2));
						if is_active then if (i and 3 <> 0) then EnableLFO(1) else DisableLFO(1);
						instrument_changed := TRUE; // catch makeshift bass drums
					end;

					$F2:
					with LFO[1] do begin
						delay_after_noteon := loader.ReadByte;
						step_rate_ticks := loader.ReadByte;
						step_height := shortint(loader.ReadByte);
						width_multiplier := loader.ReadByte;
						if is_active then EnableLFO(1);
						instrument_changed := TRUE; // catch makeshift bass drums
					end;

					$F3:
					begin
						case track_type of
							FM_TRACK: SetTrackVolume(track_volume - 4);
							PCM_TRACK: SetTrackVolume(track_volume - 16);
							SSG_TRACK, RHYTHM_TRACK, RHYTHM_PATTERN: SetTrackVolume(track_volume - 1);
						end;
					end;

					$F4:
					begin
						case track_type of
							FM_TRACK: SetTrackVolume(track_volume + 4);
							PCM_TRACK: SetTrackVolume(track_volume + 16);
							SSG_TRACK, RHYTHM_TRACK, RHYTHM_PATTERN: SetTrackVolume(track_volume + 1);
						end;
					end;

					$F5: track_transpose_semitones := shortint(loader.ReadByte);

					$F6:
					begin
						if repetition.active_loops <> 0 then
							raise DecompException.Create('F6 loop inside F9 loop @ $' + strhex(loader.ofs - 1));
						F6_loop_ofs := loader.ofs;
						if (main_loop_begin_tick = high(main_loop_begin_tick))
						or (midiobject.track[midichannel].current_tick > main_loop_begin_tick) then
							main_loop_begin_tick := midiobject.track[midichannel].current_tick;
						// Wipe bend range at main loop start, so next use of it is fully output.
						current_MIDI_bend := $2000;
						MIDI_bend_range_cents := 1;
					end;

					$F7:
					with repetition do begin
						i := LEtoN(loader.ReadWord) + baseofs;
						if i >= loader.fullFileSize then
							raise DecompException.Create('F7 oob @ $' + strhex(loader.ofs - 1));
						j := byte(loader.PtrAt(i)^);
						if j <> 0 then begin
							if active_loops = 0 then
								raise DecompException.Create('F7 without F9 or in infinite loop @ $' + strhex(loader.ofs - 1));
							if (dword(counter[active_loops - 1] + 1) >= j) then begin
								// Exit the loop early.
								loader.ofs := i + 4;
								dec(active_loops);
							end;
						end;
					end;

					$F8:
					with repetition do begin
						if active_loops = 0 then
							raise DecompException.Create('F8 without F9 @ $' + strhex(loader.ofs - 1));
						i := loader.ReadByte;
						if i = 0 then raise DecompException.Create('F8 loop count 0 @ $' + strhex(loader.ofs - 1));
						//decomp_logger(strcat('F8 at $&, rep[%].counter=%, limit=%', [loader.ofs, active_loops - 1, counter[active_loops - 1], i]));
						inc(counter[active_loops - 1]);
						if counter[active_loops - 1] < i then begin
							// Loop back.
							inc(loader.readp);
							loader.ofs := LEtoN(loader.ReadWord) + baseofs + 2;
						end
						else begin
							// Exit the loop.
							inc(loader.readp, 3);
							dec(active_loops);
						end;
					end;

					$F9:
					with repetition do begin
						if active_loops >= length(counter) then
							raise DecompException.Create('F9 nesting overflow @ $' + strhex(loader.ofs - 1));

						id[active_loops] := LEtoN(loader.ReadWord);
						if id[active_loops] + baseofs > loader.fullFileSize then
							raise DecompException.Create('F9 loop oob @ $' + strhex(loader.ofs - 3));
						if byte(loader.PtrAt(id[active_loops] + baseofs)^) = 0 then begin
							// Infinite repeat, treat as the track's main loop.
							F6_loop_ofs := loader.ofs;
							if (main_loop_begin_tick = high(main_loop_begin_tick))
							or (midiobject.track[midichannel].current_tick > main_loop_begin_tick) then
								main_loop_begin_tick := midiobject.track[midichannel].current_tick;
							byte(loader.PtrAt(id[active_loops] + baseofs - 1)^) := $80;
							// Wipe bend range at main loop start, so next use of it is fully output.
							current_MIDI_bend := $2000;
							MIDI_bend_range_cents := 1;
						end else begin
							counter[active_loops] := 0;
							inc(active_loops);
						end;
					end;

					$FA: SetDetune(smallint(LEtoN(loader.ReadWord))); // can rarely appear on PCM/rhythm tracks

					$FB: pmdtrack[track].tie_to_next_note := TRUE;

					$FC:
					begin
						i := loader.ReadByte;
						case i of
							$FF: begin
								tempo.PMD_tempo := loader.ReadByte;
								if tempo.PMD_tempo < 18 then
									raise DecompException.Create('Bad tempo: ' + strdec(tempo.PMD_tempo));
								TimerBFromTempo;
							end;
							$FE: begin
								inc(tempo.PMD_timerB, shortint(loader.ReadByte));
								if tempo.PMD_timerB < 0 then
									tempo.PMD_timerB := 0
								else
									if tempo.PMD_timerB > 250 then tempo.PMD_timerB := 250;
								TempoFromTimerB;
							end;
							$FD: begin
								inc(tempo.PMD_tempo, shortint(loader.ReadByte));
								if tempo.PMD_tempo < 18 then
									tempo.PMD_tempo := 18
								else
									if tempo.PMD_tempo > 255 then tempo.PMD_tempo := 255;
								TimerBFromTempo;
							end;
							else begin
								tempo.PMD_timerB := i;
								TempoFromTimerB;
							end;
						end;
					end;

					$FD:
					begin
						i := loader.ReadByte;
						if (track_type = FM_TRACK) and (i > 127) then
							raise DecompException.Create('FM volume ' + strdec(i) + ' > 127 @ $' + strhex(loader.ofs - 2));
						SetTrackVolume(i);
					end;

					$FE: staccato.constant := loader.ReadByte;

					$FF:
					case track_type of
						SSG_TRACK: raise DecompException.Create('FF on SSG track @ $' + strhex(loader.ofs - 1));
						RHYTHM_TRACK: raise DecompException.Create('FF on rhythm track @ $' + strhex(loader.ofs - 1));
						RHYTHM_PATTERN: begin
							track_type := RHYTHM_TRACK;
							loader.ofs := rhythm_return_ptr;
							endofs := meta_track;
						end;
						else SelectInstrument(loader.ReadByte);
					end;

					else raise DecompException.Create(strcat('Unknown command $& @ $&', [command, loader.ofs - 1]));
				end;

			end;
		end;
	end;

begin
	result := FALSE;
	if loader.buffySize < 40 then raise DecompException.Create('file too tiny');

	songmode := MODE_OPN;
	baseofs := 1;
	Assert(loader.ofs = 0);
	if (byte(loader.readp^) <= 2) and (word((loader.readp + 1)^) <= LEtoN(word($1A)))
	and (LEtoN(word((loader.readp + 3)^)) < loader.buffySize) then
		songmode := loader.ReadByte
	else if (word(loader.readp^) <= LEtoN(word($1A))) and (LEtoN(word((loader.readp + 2)^)) < loader.buffySize) then
		baseofs := 0
	else
		raise DecompException.Create('Bad header');

	{$ifdef enable_decomp_hacks}
	// Hack: Cut ridiculous loop count to a sensible number.
	if (game = gid.Sakura) and (basename = 'sk_09') then byte(loader.PtrAt($691)^) := $F;
	{$endif enable_decomp_hacks}

	// Read the track pointers, an array of word addresses, which runs up to the beginning of the first address.
	fillbyte(pmdtrack, sizeof(pmdtrack[0]) * length(pmdtrack), 0);
	numtracks := 0;
	while (numtracks < dword(length(pmdtrack))) and (loader.ofs + 1 < loader.buffySize) do begin
		with pmdtrack[numtracks] do begin
			ptr := LEtoN(loader.ReadWord) + baseofs;
			readptr := ptr;
			track_type := FM_TRACK;
			if (songmode = MODE_OPN) and (numtracks in [6..8]) then track_type := SSG_TRACK;
			if ((songmode = MODE_OPN) and (numtracks = 9)) or ((songmode = MODE_OPM) and (numtracks = 8)) then
				track_type := PCM_TRACK;
			if (songmode = MODE_OPN) and (numtracks = 10) then track_type := RHYTHM_TRACK;
		end;
		inc(numtracks);
		if loader.ofs >= pmdtrack[0].ptr then break;
	end;

	if numtracks = 0 then raise DecompException.Create('No tracks identified!');
	if numtracks <> 13 then raise DecompException.Create('Suspicious track count: ' + strdec(numtracks));
	dec(numtracks);
	instrument_track := pmdtrack[numtracks].ptr;
	dec(numtracks);
	meta_track := pmdtrack[numtracks].ptr;

	midiobject := TMidiWriter.Create;

	try
		midiobject.SetNumberOfTracks(numtracks + 1); // control track is generated as an extra track
		for i := 1 to numtracks do dec(midiobject.track[i].midichan);
		midiobject.track[10].SetVolume(127);
		track := 0;
		global_transpose_semitones := 0;
		fillbyte(globalvolume, sizeof(globalvolume), 0);
		with globalvolume do begin
			master_pcm_rhythm_volume := 48;
			for i := high(pcm_rhythm_volume) downto 0 do pcm_rhythm_volume[i] := 15;
		end;
		for i := high(pcm_rhythm_active) downto 0 do pcm_rhythm_active[i] := FALSE;
		tempo.ticks_per_whole_note := 96;
		tempo.PMD_timerB := 200;
		TempoFromTimerB;
		main_loop_begin_tick := high(main_loop_begin_tick);
		main_loop_end_tick := 0;
		processing_limit_ticks := 8;
		processing_granularity := 32;
		track := 6; // hack for picking up $C0 on G track before starting any other track
		tracks_ended := 0;

		repeat
			inc(processing_limit_ticks, processing_granularity);

			while track < numtracks do begin
				with pmdtrack[track] do begin
					// Init running track data when entering the track for the first time.
					if NOT inited then begin
						instrument.instrument_type := MIM_TIT_SSG;
						if track_type = FM_TRACK then begin
							instrument.instrument_type := MIM_TIT_OPN;
							if songmode = MODE_OPL then instrument.instrument_type := MIM_TIT_OPL;
						end;
						instrument.midimatch := 80; // default to square wave (synth lead 1)
						instrument.enable_tone := TRUE;
						instrument.mimmatch := -1;

						// Figure out which MIDI channel to use. Must skip the percussion channel.
						midichannel := track + 1;
						if midichannel >= 10 then inc(midichannel);

						F6_loop_ofs := $FFFFFFFF;
						staccato.minimum_note_length := 1;

						case track_type of
							FM_TRACK: SetTrackVolume(108);
							SSG_TRACK: SetTrackVolume(8);
							PCM_TRACK: SetTrackVolume(128);
							RHYTHM_TRACK: SetTrackVolume(15);
						end;

						SetBendRange;
						SetBend;
						inited := TRUE;
					end;

					loader.ofs := readptr;
					ProcessTrackData;
					readptr := loader.ofs;
				end;

				inc(track);
			end;
			track := 0;
		until tracks_ended >= numtracks;

		// Get the exact tick count of the longest written track.
		processing_limit_ticks := 0;
		for i := numtracks - 1 downto 0 do with midiobject.track[pmdtrack[i].midichannel] do begin
			if current_tick > processing_limit_ticks then processing_limit_ticks := current_tick;
		end;

		// Any $EB rhythm notes still going at this point must be offed before LoopEnd.
		for i := 0 to 5 do if pcm_rhythm_active[i] then
			midiobject.track[10].NoteOff(pcm_rhythm_map[i], $7F, processing_limit_ticks);

		// Save a metadata loop marker if any track included an F6 loop.
		if main_loop_begin_tick < high(main_loop_begin_tick) then begin
			// Any track that didn't go as far as the other tracks but has an infinite loop needs to have that loop
			// unrolled to extend the track to full length.

			// Roll out 1 extra tick, since the loop start may be in the middle of a tick in a PMD file, but with MIDI
			// we're restricted to 1-tick granularity. So must make sure all tracks have rolled to a tick boundary.
			inc(main_loop_begin_tick);
			inc(main_loop_end_tick);

			for track := 0 to numtracks - 1 do with pmdtrack[track] do with midiobject.track[midichannel] do
				if current_tick < main_loop_end_tick then begin
					processing_limit_ticks := main_loop_end_tick;
					//writeln(strcat('track %: unroll ticks %', [track, main_loop_end_tick, processing_limit_ticks]));
					loader.ofs := readptr;
					ProcessTrackData;
				end;

			getmem(outputbuffer, 16);

			midiobject.WriteMetaString(main_loop_begin_tick, 6, 'LoopStart');
			midiobject.WriteMetaString(main_loop_end_tick, 6, 'LoopEnd');

			freemem(outputbuffer);
		end;

		// Asynchronously looped tracks may still have notes playing after LoopEnd, clean them up.
		for track := numtracks - 1 downto 0 do with pmdtrack[track] do
			if note_is_on then begin TimeAhead(1); NoteOff; end;

		outputbuffer := NIL;
		midiobject.Finalise(outputbuffer, i);
		SaveFile(PathCombine([outdir, basename], 'm.mid'), outputbuffer, i);
		freemem(outputbuffer); outputbuffer := NIL;
		result := TRUE;

	finally
		midiobject.Destroy; midiobject := NIL;
	end;
end;

function Decomp_LeafFM(const loader: TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
var buffy : pointer = NIL;
	bufsize : dword;
	subloader : TFileLoader = NIL;
begin
	result := FALSE;
	bufsize := LEtoN(loader.ReadDword);
	// The first dword should be the file size after uncompressing. Expect a reasonable size...
	if (bufsize <= loader.fullFileSize) or (bufsize > loader.fullFileSize * 10) then
		raise DecompException.Create('sus 1st dword');
	// The second dword is the start of the PMD header, xorred. The first 4 bytes are always the same, check for that.
	if dword(loader.readp^) <> BEtoN(dword($00FFE5FF)) then raise DecompException.Create('sus 2nd dword');

	XorBuffer(loader.readp, loader.endp, $FF);
	getmem(buffy, bufsize);
	Decompress_LZSS(loader.readp, loader.endp, buffy, bufsize, 18, TRUE, FALSE);
	try
		subloader := TFileLoader.FromMemory(buffy, bufsize, FALSE);
		result := Decomp_PMD(subloader, outdir, basename, game);
	finally
		if subloader <> NIL then subloader.Destroy;
		freemem(buffy); buffy := NIL;
	end;
end;

