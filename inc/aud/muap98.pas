{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// uses midi_instrument_matcher;

function Decomp_MUAP98(const loader: TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Reads the indicated .O MUAP98 music file, and saves it in outdir/basename as a normal midi file.
// Throws DecompException for bad failures, returns TRUE if successful.
var i, numtracks : dword;
	trackofs : array[0..16] of word;
begin
	result := FALSE;
	if loader.buffySize < 80 then raise DecompException.Create('file too tiny');
	// Expect an array of 17 word addresses in ascending order.
	i := $25;
	for numtracks := 0 to 16 do begin
		trackofs[numtracks] := LEtoN(loader.ReadWord);
		if trackofs[numtracks] <= i then raise DecompException.Create('track ofs not ascending');
		if trackofs[numtracks] > loader.fullFileSize then raise DecompException.Create('track ofs oob');
		i := trackofs[numtracks];
	end;

	if (trackofs[0] and 1 <> 0) or (trackofs[0] > $2A) then raise DecompException.Create('sus track 0 ofs');

	raise SkipException.Create('MUAP98 music not supported yet');
	if outdir = basename then write(':)'); // silence compiler, we'll use them eventually
	result := TRUE;
end;

