{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Decomp --- Sound conversion code

function Decomp_WAV(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Reads the indicated WAV file, compresses and saves it in outdir/basename.
// Throws DecompException for bad failures, returns TRUE if successful.
var i : dword;
begin
	result := FALSE;
	if loader.ReadDword <> BEtoN(dword($52494646)) then raise DecompException.Create('bad riff sig');
	i := LEtoN(loader.ReadDword);
	if (i > loader.fullFileSize - 8) or (i + $800 < loader.fullFileSize) then // files may have extra padding at end
		raise DecompException.Create('sus chunksize');
	if loader.ReadDword <> BEtoN(dword($57415645)) then raise DecompException.Create('bad wave sig');
	if loader.ReadDword <> BEtoN(dword($666D7420)) then raise DecompException.Create('bad fmt sig');
	raise SkipException.Create('WAV compression not supported yet');
	if outdir = basename then write(':)'); // silence compiler, we'll use them eventually
	result := TRUE;
end;

function Decomp_MP3(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Reads the indicated MP3 file, saves it unchanged in outdir/basename.
// Throws DecompException for bad failures, returns TRUE if successful.

	function _ReadHeader : boolean;
	var fromofs, nextofs : dword;
		tries, ver, layer, bitrateindex, freqindex : byte;
	const br : array of word = (0, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320);
	const fq : array of word = (44100, 48000, 32000);
	begin
		fromofs := loader.ofs;
		for tries := 7 downto 0 do begin // frame starts may be imprecise
			loader.bitSelect := $80;
			if loader.ReadBits(11) = $7FF then begin // frame synch
				ver := loader.ReadBits(2);
				layer := loader.ReadBits(2);
				if (ver <> 1) and (layer <> 0) then begin
					loader.ReadBit;
					bitrateindex := loader.ReadBits(4);
					freqindex := loader.ReadBits(2);
					if (bitrateindex in [1..$E]) and (freqindex <> 3) then begin
						nextofs := 144000 * br[bitrateindex] div fq[freqindex] - 1;
						if nextofs * 2 + fromofs <= loader.fullFileSize then begin
							loader.ofs := fromofs + nextofs; // seek to predicted start of next frame
							exit(TRUE);
						end;
					end;
				end;
			end;

			inc(fromofs);
			loader.ofs := fromofs;
		end;
		result := FALSE;
	end;

var i : dword;
begin
	result := FALSE;
	if loader.fullFileSize < $1000 then raise DecompException.Create('file too tiny');
	// Very rarely an MP3 begins with a bunch of 00's, like there was an ID3 that was wiped out. Unlikely with VNs.
	//while (loader.readp + $1000 < loader.endp) and (dword(loader.readp^) = 0) do inc(loader.readp, 4);

	// Recognise and skip an ID3 header.
	if LEtoN(dword(loader.readp^)) and $FFFFFF = $334449 then begin
		loader.ofs := 6;
		i := loader.ReadByte shl 21;
		i := i or (loader.ReadByte shl 14);
		i := i or (loader.ReadByte shl 7);
		i := (i or loader.ReadByte) + 10;
		if i + $800 > loader.fullFileSize then raise DecompException.Create('id3 size oob');
		loader.ofs := i;
	end;

	// Easy to get false positives, so try to match the first four frame headers to prove it's an MP3.
	for i := 0 to 3 do
		if NOT _ReadHeader then raise DecompException.Create('bad framehdr ' + strdec(i));

	SaveFile(
		PathCombine([outdir, basename], 'mp3'),
		loader.PtrAt(0), loader.fullFileSize);

	result := TRUE;
end;

function Decomp_OGG(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Reads the indicated OGG file, saves it unchanged in outdir/basename.
// Throws DecompException for bad failures, returns TRUE if successful.
begin
	result := FALSE;
	if (LEtoN(dword(loader.readp^)) <> BEtoN(dword($4F676753))) then // OggS
		raise DecompException.Create('no ogg sig');

	SaveFile(
		PathCombine([outdir, basename], 'ogg'),
		loader.PtrAt(0), loader.fullFileSize);

	result := TRUE;
end;

