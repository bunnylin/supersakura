{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// Decomp --- Music and sound conversion code

type TRCPTrack = record
	// Working variables for each track.
	startofs : dword; // beginning offset of the track's header
	measurefrom : dword; // used by FC and FD same measure commands
	cur_ticks : dword;
	livenotes : array of record
		note, duration : byte;
	end;
	live_note_count : dword;

	repenestlevel{, repenestbackup} : byte;
	repetition : array[0..15] of record
		fromlofs, fromtick : dword;
		loopcounter : byte;
		_ticks, _data1, _data2 : byte; // backup event values, must be restored on loop back
		inmeasure : dword;
	end;

	channel : byte; // midi channel assigned to this track
	transpose : shortint;
end;

type TRCPinfo = record
	filetype : (RCP_MMD, RCP_1, RCP_2, RCP_3);
	midiobject : TMidiWriter;
	timesignature : word;
	tempo : dword;
	numtracks : byte;
	event, event_rest_ticks, data1, data2 : byte;
	global_transpose : shortint;
	main_loop_begin_tick, main_loop_end_tick : longint;
	tracklist : array of TRCPTrack;
end;

function ConvertRCPTracks(const loader : TFileLoader; RCPinfo : TRCPinfo) : boolean;

	procedure _SortLiveNotes(track, index : byte);
	// Sorts the given live note by remaining duration, ascending order, shortest at index 0.
	// Assumes every note except the specified is already correctly sorted.
	var swapnote, swapdura : byte;
	begin
		with RCPinfo do with tracklist[track] do begin
			if (index <> 0) and (livenotes[index].duration < livenotes[index - 1].duration) then begin
				// Shift down!
				repeat
					swapnote := livenotes[index].note;
					swapdura := livenotes[index].duration;
					livenotes[index] := livenotes[index - 1];
					dec(index);
					livenotes[index].note := swapnote;
					livenotes[index].duration := swapdura;
				until (index = 0) or (livenotes[index].duration >= livenotes[index - 1].duration);
				exit;
			end;

			// Shift up!
			while (dword(index + 1) < live_note_count) and (livenotes[index].duration > livenotes[index + 1].duration) do begin
				swapnote := livenotes[index].note;
				swapdura := livenotes[index].duration;
				livenotes[index] := livenotes[index + 1];
				inc(index);
				livenotes[index].note := swapnote;
				livenotes[index].duration := swapdura;
			end;
		end;
	end;

	procedure _AdvanceTrack(delay_ticks, track : byte);
	var m, offcount : dword;
	begin
		with RCPinfo do with tracklist[track] do begin
			if live_note_count <> 0 then begin
				offcount := 0;
				for m := 0 to live_note_count - 1 do with livenotes[m] do begin
					if delay_ticks >= duration then begin
						midiobject.track[channel].NoteOn(note, 0, cur_ticks + duration);
						inc(offcount);
					end
					else
						dec(duration, delay_ticks);
				end;

				dec(live_note_count, offcount);
				if live_note_count <> 0 then
					for m := 0 to live_note_count - 1 do livenotes[m] := livenotes[m + offcount];
			end;
			inc(cur_ticks, delay_ticks);
		end;
	end;

	procedure _LoopBack(track : byte);
	begin
		with RCPinfo do with tracklist[track] do with repetition[repenestlevel - 1] do begin
			inc(loopcounter);
			loader.ofs := fromlofs;
			// Restore event data from start of loop, so an immediate abbreviation still works.
			event := $F9;
			event_rest_ticks := _ticks;
			data1 := _data1;
			data2 := _data2;
			measurefrom := inmeasure;
		end;
	end;

	procedure _ProcessTrackEvents(track : byte; unrolling_main : boolean);

		procedure _AddComment;
		var comment : UTF8string = '';
			writeofs : dword = 1;
		begin
			setlength(comment, 128);
			with RCPinfo do with tracklist[track] do case filetype of
				RCP_2:
				begin
					dec(loader.readp, 4);
					repeat
						// Concatenate the last two bytes of all subsequent F7 event quads.
						if writeofs >= comment.Length then setlength(comment, comment.Length * 2);
						word((@(comment[writeofs]))^) := word((loader.readp + 2)^);
						inc(writeofs, 2);
						inc(loader.readp, 4);
					until (loader.readp + 4 >= loader.endp) or (byte(loader.readp^) <> $F7);
					// Trim whitespace from end, output string.
					repeat dec(writeofs); until (writeofs = 0) or (NOT (comment[writeofs] in [#0, ' ']));
					setlength(comment, writeofs);
					midiobject.track[channel].AddMetaString(1, comment, cur_ticks);
				end;

				else decomp_logger('[!] comment on non-rcp v2 @ $' + strhex(loader.ofs));
			end;
		end;

		procedure _MeasureEnd;
		begin
			with RCPinfo do with tracklist[track] do begin
				loader.ofs := measurefrom;
				measurefrom := 0;
			end;
		end;

		procedure _SameMeasure;
		var newaddr : dword;
		begin
			with RCPinfo do with tracklist[track] do begin
				if measurefrom <> 0 then
					_MeasureEnd
				else begin
					measurefrom := loader.ofs;
					repeat
						// Jump to the new address, remember the old one.
						newaddr := (data2 shl 8) or data1;
						if newaddr >= loader.buffySize then
							LogError('FC jump out of bounds @ ' + strhex(loader.ofs - 4))
						else begin
							if filetype = RCP_2 then dec(newaddr, 44); // jump is relative to track header start
							loader.ofs := startofs + newaddr;
							if byte(loader.readp^) <> $FC then
								break
							else
								decomp_logger('[!] double FC @ $' + strhex(loader.ofs)); // re-jump immediately?
							data1 := byte((loader.readp + 2)^);
							data2 := byte((loader.readp + 3)^);
						end;
					until FALSE;
				end;
			end;
		end;

	var m : longint;
		n : dword;
	begin
		with RCPinfo do with tracklist[track] do while loader.readp < loader.endp do begin
			// If the previous command was not $F0..FE, add rest_ticks to track's current ticks.
			if event < $F0 then _AdvanceTrack(event_rest_ticks, track);

			m := loader.ReadByte;
			if (filetype = RCP_MMD) and (m and $F0 = $80) then begin
				// MMD event $8x: Only has the difference from the previous event bytes.
				if m and 8 <> 0 then event := loader.ReadByte;
				Assert(event <> $FE);
				if m and 4 <> 0 then event_rest_ticks := loader.ReadByte;
				if m and 2 <> 0 then data1 := loader.ReadByte;
				if m and 1 <> 0 then data2 := loader.ReadByte;
			end
			else begin
				if m = $FE then exit; // end of track!
				event := m;
				event_rest_ticks := loader.ReadByte;
				data1 := loader.ReadByte;
				data2 := loader.ReadByte;
			end;
			//decomp_logger(strcat('&: track%: event $&, ticks %, data $& $&', [loader.ofs, track, event, event_rest_ticks, data1, data2]));

			case event of
				// Note on.
				$00..$7F:
				if (data1 <> 0) and (data2 <> 0) then begin
					if data2 > 127 then raise DecompException.Create('note velo > 127');
					// Duration and velocity data must be non-zero. Otherwise this is an empty note, or rest.
					// i = note, data1 = duration, data2 = velocity
					m := event + global_transpose + transpose;
					if m < 0 then
						m := 0
					else
						if m > $7F then m := $7F;

					// If this note isn't playing yet, add as a new note. Else bump the existing one's duration.
					n := live_note_count;
					while n <> 0 do begin
						dec(n);
						if livenotes[n].note = m then break;
					end;

					if (live_note_count = 0) or (livenotes[n].note <> m) then begin
						n := live_note_count;
						if live_note_count >= dword(length(livenotes)) then
							setlength(livenotes, live_note_count + 8);
						inc(live_note_count);
						midiobject.track[channel].NoteOn(m, data2, cur_ticks);
						livenotes[n].note := m;
					end;

					livenotes[n].duration := data1;
					_SortLiveNotes(track, n);
				end;

				// User exclusive message, ignore...
				$90..$97:
				if decomp_param.verbose then decomp_logger('[!] ignore user sysex @ $' + strhex(loader.ofs));

				// System exclusive message, maybe can ignore...
				$98:
				begin
					if decomp_param.verbose then decomp_logger('[!] ignore track sysex @ $' + strhex(loader.ofs));
					case filetype of
						// String of bytes terminating with F7, read and discard.
						RCP_MMD:
						while (loader.readp < loader.endp) and (loader.ReadByte <> $F7) do ;

						// Series of F7 event quads, read and discard.
						RCP_2:
						while (loader.readp + 4 < loader.endp) and (byte(loader.readp^) = $F7) do inc(loader.readp, 4);

						else decomp_logger('[!] track sysex @ $' + strhex(loader.ofs));
					end;
				end;

				// Manufacturer-specific commands. Probably stuff like reverb and chorus, shouldn't ignore these...
				$C0..$DF:
				if decomp_param.verbose then
					decomp_logger(strcat('[!] ignore manufacturer-specific $& & & &', [event, event_rest_ticks, data1, data2]));

				// Instrument and bank change.
				$E2:
				midiobject.track[channel].SelectInstrument(data1, data2, cur_ticks);

				// Channel change.
				$E6:
				if data1 = 0 then // if 0, disable the channel
					break
				else
					channel := (data1 - 1) and $F;

				// Tempo change.
				$E7:
				begin
					m := 60000000 div (data1 * tempo div 64);
					midiobject.SetTempo(cur_ticks, m);
				end;

				// Channel aftertouch.
				$EA:
				midiobject.track[channel].SetAftertouch(data1, -1, cur_ticks);

				// Control change.
				$EB:
				// Controllers 98/99 are synth-specific and can have unpleasant consequences on midi devices
				// other than the original... Look up XG NRPN assignments for likely effects.
				midiobject.track[channel].AddController(data1, data2, cur_ticks);

				// Instrument change (ignore for percussion channel 10).
				$EC:
				if channel <> 9 then
					midiobject.track[channel].SelectInstrument(data1, -1, cur_ticks)
				else if decomp_param.verbose then
					decomp_logger('[!] prog change on rhythm chn');

				// Note aftertouch (polyphonic).
				$ED:
				midiobject.track[channel].SetAftertouch(data2, data1, cur_ticks);

				// Pitch bend.
				$EE:
				midiobject.track[channel].SetPitchBend((data2 shl 7) or data1, cur_ticks);

				// Comment.
				$F6: _AddComment;

				// Loop end.
				$F8:
				// Is there a loop start somewhere waiting for us?
				if repenestlevel = 0 then begin
					if decomp_param.verbose then decomp_logger('[!] F8 without F9');
				end
				else with repetition[repenestlevel - 1] do begin
					// Infinite loop? This ends normal mmdtrack processing, but if this track is shorter than
					// the longest track, may need to return here to unwind the infinite loop until length met.
					if event_rest_ticks in [0, $FE, $FF] then begin
						if unrolling_main then
							_LoopBack(track)
						else begin
							if longint(fromtick) > main_loop_begin_tick then main_loop_begin_tick := fromtick;
							//decomp_logger(strcat('track % infinite loop [%..%]', [track, fromtick, cur_ticks]));
							exit;
						end;
					end
					// Finite loop; keep going back until repetition count is met.
					else if loopcounter < event_rest_ticks then
						_LoopBack(track)
					else
						dec(repenestlevel);
				end;

				// Loop start.
				$F9:
				if repenestlevel > high(repetition) then
					raise DecompException.Create('F9 loop nested too much @ $' + strhex(loader.ofs - 4))
				else begin
					// Push current state onto repetition stack.
					with repetition[repenestlevel] do begin
						fromlofs := loader.ofs;
						fromtick := cur_ticks;
						loopcounter := 1;
						_ticks := event_rest_ticks;
						_data1 := data1;
						_data2 := data2;
						inmeasure := measurefrom;
					end;
					inc(repenestlevel);
				end;

				// "Same Measure" goto command.
				$FC: _SameMeasure;

				// "Measure End" return command.
				$FD: if measurefrom <> 0 then _MeasureEnd;

				else decomp_logger(
					strcat('[!] unknown event $& & & & @ $&', [event, event_rest_ticks, data1, data2, loader.ofs]));
			end;

			if (unrolling_main) and (longint(cur_ticks) >= main_loop_end_tick) then exit;
		end;
	end;

	procedure _FinishLiveNotes(track : byte);
	// At the end of all tracks, any notes still on must be offed!
	var m, n : dword;
	begin
		with RCPinfo do with tracklist[track] do begin
			for m := 0 to live_note_count - 1 do begin
				n := cur_ticks;
				inc(n, livenotes[m].duration);
				midiobject.track[channel].NoteOn(livenotes[m].note, 0, n);
			end;
		end;
	end;

var trackindex : byte;
begin
	with RCPinfo do begin
		for trackindex := 0 to numtracks - 1 do with tracklist[trackindex] do
			if channel <> $FF then begin
				loader.ofs := startofs;
				event := 0; event_rest_ticks := 0; data1 := 0; data2 := 0;
				_ProcessTrackEvents(trackindex, FALSE);

				if longint(cur_ticks) > main_loop_end_tick then main_loop_end_tick := cur_ticks;
			end;

		if main_loop_begin_tick >= 0 then begin
			// Any track that didn't go as far as the other tracks but has an infinite loop needs to have that loop
			// unrolled to extend the track to full length.
			for trackindex := 0 to numtracks - 1 do with tracklist[trackindex] do
				if (longint(cur_ticks) < main_loop_end_tick) and (repenestlevel <> 0) then begin
					_LoopBack(trackindex);
					_ProcessTrackEvents(trackindex, TRUE);
				end;

			midiobject.track[0].AddMetaString(6, 'LoopStart', main_loop_begin_tick);
			midiobject.track[0].AddMetaString(6, 'LoopEnd', main_loop_end_tick);
		end;
		for trackindex := numtracks - 1 downto 0 do
			if tracklist[trackindex].live_note_count <> 0 then _FinishLiveNotes(trackindex);
	end;
	result := TRUE;
end;

function Decomp_MMD(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Reads the indicated .MMD midi file, and saves it in outdir/basename as a normal midi file.
// Throws DecompException for bad failures, returns TRUE if successful.
var RCPinfo : TRCPinfo;
	outputbuffer : pointer;
	i : longint;
	l : dword;
	txt : string;
	utxt : UTF8string;
	trackindex : byte;
	isfuga : boolean;
begin
	result := FALSE;

	if loader.fullFileSize < 120 then raise DecompException.Create('file too tiny');

	// Fuga MMD's are XOR'ed by $A5 and have a different header...
	i := 0;
	loader.ofs := 3;
	for l := 2 downto 0 do if loader.ReadByte = $A5 then inc(i);
	isfuga := (i >= 2);

	with RCPinfo do begin
		if isfuga then begin
			// Fuga MMD. Decrypt and get track offsets.
			XorBuffer(loader.PtrAt(0), loader.endp, $A5);

			timesignature := BEtoN(loader.ReadWordFrom(1));
			global_transpose := shortint(loader.ReadByteFrom(4));

			loader.ofs := 6;
			numtracks := 0;
			setlength(tracklist, 18);
			for trackindex := 17 downto 0 do begin
				i := LEtoN(word(loader.readp^));
				if (i < $30) or (loader.ofs + word(i) > loader.fullFileSize) then
					raise DecompException.Create(strcat('track % bad size: $&', [numtracks, i]));
				tracklist[numtracks].startofs := loader.ofs + $2C;
				tracklist[numtracks].channel := byte((loader.readp + 4)^);
				tracklist[numtracks].transpose := shortint((loader.readp + 5)^);
				inc(numtracks);
				inc(loader.readp, i);
			end;

			filetype := RCP_2;
		end
		else begin
			// Non-Fuga MMD. Validate track offsets. Expect at least 15 valid ones.
			loader.ofs := 2; l := $49;
			numtracks := 0;
			for trackindex := 17 downto 0 do begin
				i := LEtoN(loader.ReadWord);
				if dword(i) <= l then begin
					if numtracks >= 15 then
						break
					else
						raise DecompException.Create('track ofs below previous');
				end;
				if dword(i) >= loader.buffySize then begin
					if numtracks >= 15 then
						break
					else
						raise DecompException.Create('track oob');
				end;
				inc(numtracks);
				l := i;
				inc(loader.readp);
				// Fourth byte is midi port (high nibble) and midi channel (low nibble). Port is normally 0, rarely 1.
				i := loader.ReadByte;
				if NOT (i in [0..$1F, $80, $FF]) then raise DecompException.Create('track has bad channel $' + strhex(i));
			end;
			timesignature := $0404;
			global_transpose := 0;

			filetype := RCP_MMD;
		end;

		setlength(tracklist, numtracks);
		midiobject := TMidiWriter.Create;

		try
			midiobject.SetNumberOfTracks(numtracks);
			main_loop_begin_tick := -1;
			main_loop_end_tick := -1;

			// MMD header:
			// byte: tempo
			// byte: ?
			// array[0..17] of record:
			//   word: track offset
			//   byte: key
			//   byte: channel
			// byte: ?
			// byte: ?
			// null-terminated Shift-JIS string at $50: song title
			// (If first track begins within [$4A..$50] then there's no title.)
			// (If there's space after the song title before the first track, another string may be present.)
			// All tracks should follow immediately in order.
			if NOT isfuga then begin
				i := LEtoN(loader.ReadWordFrom(2));
				if i > $50 then begin
					utxt := '';
					setlength(utxt, i - $50);
					move(loader.PtrAt($50)^, utxt[1], utxt.Length);
					utxt := TrimRight(utxt.Replace(#0, #$A));
					midiobject.WriteMetaString(0, 3, GetUTF8(utxt));
				end;
			end;

			// Initial tempo.
			midiobject.ticks_per_quarter_note := $30;
			tempo := loader.ReadByteFrom(0);
			if tempo = 0 then raise DecompException.Create('null tempo');
			midiobject.SetTempo(0, 60000000 div tempo);
			// Time signature (normally 4/4).
			l := 1;
			while dword(1 shl l) < (timesignature shr 8) do inc(l); // denominator
			txt := #$FF#$58#4 + char(timesignature and $FF) + char(l) + #$18#8;
			midiobject.track[0].AddCommand(0, txt.Length, @txt[1]);

			// Initialise MMD tracks.
			for trackindex := 0 to numtracks - 1 do with tracklist[trackindex] do begin
				if NOT isfuga then begin
					l := trackindex * 4 + 2;
					startofs := LEtoN(loader.ReadWordFrom(l));
					transpose := shortint(loader.ReadByteFrom(l + 2));
					channel := loader.ReadByteFrom(l + 3) and $F;
				end;
				if transpose and $80 <> 0 then transpose := 0;
				cur_ticks := 0;
				repenestlevel := 0;
				measurefrom := 0;
				livenotes := NIL;
				live_note_count := 0;

				// If the track is disabled, screw it.
				if channel in [$80, $FF] then continue;

				// Make sure the track starts with a neutral state, all controllers off, pitch bend centered, etc.
				midiobject.track[channel].Reset;
			end;

			ConvertRCPTracks(loader, RCPinfo);

			outputbuffer := NIL;
			midiobject.Finalise(outputbuffer, dword(i));
			SaveFile(PathCombine([outdir, basename], 'mid'), outputbuffer, dword(i));
			freemem(outputbuffer); outputbuffer := NIL;
			result := TRUE;

		finally
			midiobject.Destroy; midiobject := NIL;
		end;
	end;
end;

function Decomp_RCP(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Reads the indicated .RCP or .SC5 Recomposer v2 midi file, and saves it in outdir/basename as a normal midi file.
// Throws DecompException for bad failures, returns TRUE if successful.
var RCPinfo : TRCPinfo;
	outputbuffer : pointer = NIL;
	memo : array[0..11] of UTF8string;
	txt : string = '';
	i : dword;
	l : longint;
	j, trackindex : byte;
begin
	result := FALSE;

	Assert(loader.ofs = 0);
	if loader.buffySize < $590 then raise DecompException.Create('file too tiny');
	if (char(loader.readp^) = 'M') and (char((loader.readp + 1)^) in ['1','C']) and (loader.ReadWordFrom(2) = 0)
	then raise DecompException.Create('RCP v1 file? not implemented');

	move(loader.readp^, txt[1], 31);
	byte(txt[0]) := 31;
	if txt = 'COME ON MUSIC RECOMPOSER RCP3.0' then raise DecompException.Create('RCP v3, not implemented');
	byte(txt[0]) := 28;
	if txt <> 'RCM-PC98V2.0(C)COME ON MUSIC' then raise DecompException.Create('Unknown file signature');

	with RCPinfo do begin
		midiobject := TMidiWriter.Create;
		filetype := RCP_2;

		// For some notes on how to understand the Recomposer midi formats, see doc/midi.md.
		try
			// Read the header.
			with midiobject do begin
				setlength(sequence_name, 64);
				move(loader.PtrAt(32)^, sequence_name[1], 64);
				i := 64;
				while (i <> 0) and (sequence_name[i] in [#0, ' ']) do dec(i);
				setlength(sequence_name, i);
				sequence_name := GetUTF8(sequence_name);
			end;

			for l := 0 to 11 do begin
				setlength(memo[l], 28);
				move(loader.PtrAt(96 + l * 28)^, memo[l][1], 28);
				i := 28;
				while (i <> 0) and (memo[l][i] in [#0, ' ']) do dec(i);
				setlength(memo[l], i);
				memo[l] := GetUTF8(memo[l]);
			end;

			loader.ofs := $1C0;
			midiobject.ticks_per_quarter_note := byte(loader.readp^) + byte((loader.readp + 27)^) shl 8;
			tempo := byte((loader.readp + 1)^);
			timesignature := word((loader.readp + 2)^);
			//keysignature := byte((loader.readp + 4)^);
			global_transpose := byte((loader.readp + 5)^);
			numtracks := byte((loader.readp + 26)^);
			if NOT (numtracks in [1..18]) then numtracks := 18; // could be 9 in RCP v1, 36+ in v3
			midiobject.SetNumberOfTracks(numtracks);
			setlength(tracklist, numtracks);
			main_loop_begin_tick := -1;
			main_loop_end_tick := -1;

			for l := 0 to 11 do if memo[l] <> '' then
				midiobject.track[0].AddMetaString(1, memo[l], 0);

			// Initial tempo.
			l := 60000000 div tempo;
			midiobject.SetTempo(0, l);
			// Time signature (often 4/4).
			l := 1;
			while dword(1 shl l) < (timesignature shr 8) do inc(l); // denominator
			txt := #$FF#$58#4 + char(timesignature and $FF) + char(l) + #$18#8;
			midiobject.track[0].AddCommand(0, txt.Length, @txt[1]);

			loader.ofs := $586; // jump to start of first track
			i := 0;

			// Initialise RCP tracks.
			for trackindex := 0 to numtracks - 1 do tracklist[trackindex].cur_ticks := 0;

			for trackindex := 0 to numtracks - 1 do with tracklist[trackindex] do begin
				inc(loader.readp, i); // move to next track
				if loader.readp + $30 > loader.endp then raise DecompException.Create('Unexpected end of file!');
				startofs := loader.ofs + 44;
				i := LEtoN(word(loader.readp^)); // track size

				if (decomp_param.verbose) and (byte((loader.readp + 3)^) <> 0) then
					decomp_logger('[!] ignore rhythm flag on track ' + strdec(trackindex));
				channel := byte((loader.readp + 4)^);
				transpose := shortint((loader.readp + 5)^);
				if transpose and $80 <> 0 then transpose := -global_transpose;
				// Adjust own track's starting tick relative to all others. (Negative on this adjusts others forward.)
				l := shortint((loader.readp + 6)^);
				if l > 0 then
					inc(cur_ticks, dword(l))
				else if l < 0 then
					for j := numtracks - 1 downto 0 do
						if j <> trackindex then
							inc(tracklist[j].cur_ticks, dword(-l));

				repenestlevel := 0;
				measurefrom := 0;
				livenotes := NIL;
				live_note_count := 0;

				// Get the track's name, add it to midi track data.
				byte(txt[0]) := 36;
				move((loader.readp + 8)^, txt[1], 36);
				while (byte(txt[0]) <> 0) and (txt[byte(txt[0])] in [#0, ' ']) do dec(byte(txt[0]));
				midiobject.track[channel and $F].track_name := GetUTF8(txt);

				// If the track is disabled, or only contains the header, screw it.
				if i <= $30 then channel := $FF;
				if channel = $FF then continue;
				channel := channel and $F;
				event := $FF; event_rest_ticks := 0;

				// Make sure the track starts with a neutral state, all controllers off, pitch bend centered, etc.
				midiobject.track[channel].Reset;
			end;

			ConvertRCPTracks(loader, RCPinfo);

			midiobject.Finalise(outputbuffer, i);
			SaveFile(PathCombine([outdir, basename], 'mid'), outputbuffer, i);
			freemem(outputbuffer); outputbuffer := NIL;
			result := TRUE;

		finally
			midiobject.Destroy; midiobject := NIL;
		end;
	end;
end;

function Decomp_MID(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Validates the indicated standard midi file, and saves a copy in outdir/basename.
// Throws DecompException for bad failures, returns TRUE if successful.
var size : ptruint;
	outpath : UTF8string;
begin
	result := FALSE;
	if loader.fullFileSize < 22 then raise DecompException.Create('file too tiny');
	if dword(loader.readp^) <> BEtoN(dword($4D546864)) then raise DecompException.Create('missing MThd sig');
	if loader.ReadDwordFrom(4) <> BEtoN(dword(6)) then raise DecompException.Create('bad header size');
	if BEtoN(loader.ReadWordFrom(8)) > 2 then raise DecompException.Create('bad format word');

	size := loader.fullFileSize;
	{$ifdef enable_decomp_hacks}
	// Hack: If it's type 0 (single-track), check that the track's length looks feasible. Sometimes the given length is
	// wrong, or there's extra garbage past track end. Midi players can usually deal with this, but may as well fix.
	// Koukan Nikki 2 - m02.mid - bad midi file, no music
	// MaDoll - mad01.mid - needs longer track length
	if (loader.ReadWordFrom(8) = 0)
	and (loader.ReadDwordFrom(14) = BEtoN(dword($4D54726B))) // MTrk
	then begin
		size := BEtoN(loader.ReadDwordFrom(18));
		if size + 22 > loader.fullFileSize then begin
			decomp_logger('[!] size past eof');
			size := loader.fullFileSize - 22;
		end;
		// Track must end with FF 2F 00.
		loader.ofs := size + 19;
		if (byte(loader.readp^) <> $FF) or (byte((loader.readp + 1)^) <> $2F)
		or (byte((loader.readp + 2)^) <> 0) then begin
			decomp_logger('[!] misplaced track end');
			loader.ofs := loader.fullFileSize - 3;
			while (loader.ofs > 22)
			and ((byte(loader.readp^) <> $FF) or (byte((loader.readp + 1)^) <> $2F)
			or (byte((loader.readp + 2)^) <> 0))
			do dec(loader.readp);
			if loader.ofs = 22 then raise DecompException.Create('missing track end');
			size := loader.ofs - 22 + 3;
		end;

		dword(loader.PtrAt(18)^) := NtoBE(dword(size));
		inc(size, 22);
	end;

	// Hack: Rename subtype to default.
	outpath := basename;
	case game of
		gid.ZestFan_w: if basename.EndsWith('_p') then setlength(outpath, outpath.Length - 2);
	end;
	{$endif} {$note todo: madoll: convert first ch16 noteon to loopStart}

	SaveFile(PathCombine([outdir, outpath], 'mid'), loader.PtrAt(0), size);
	result := TRUE;
end;

function Decomp_MDS(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Converts a RIFF MDS to a standard midi file, and saves into outdir/basename.
// Throws DecompException for bad failures, returns TRUE if successful.
var midiobject : TMidiWriter = NIL;
	outputbuffer : pointer = NIL;
	i, tick, blocksize : dword;
	e : array[0..2] of byte;
begin
	result := FALSE;
	if loader.fullFileSize < 50 then raise DecompException.Create('file too tiny');
	if BEtoN(loader.ReadDword) <> $52494646 then raise DecompException.Create('no riff sig');
	if LEtoN(loader.ReadDword) <> loader.fullFileSize - 8 then raise DecompException.Create('bad riff size');
	if BEtoN(loader.ReadDword) <> $4D494453 then raise DecompException.Create('no riff mids sig');
	if BEtoN(loader.ReadDword) <> $666D7420 then raise DecompException.Create('no riff mids fmt sig');
	if LEtoN(loader.ReadDword) <> $C then raise DecompException.Create('bad fmt size');

	midiobject := TMidiWriter.Create;

	try
		// Read the mds header.
		midiobject.ticks_per_quarter_note := word(LEtoN(loader.ReadDword));
		midiobject.SetNumberOfTracks(16);
		// Ignore max buffer size value, and format flags dword.
		inc(loader.readp, 8);

		// The data section should begin here with "data" sig and run until end of file.
		if BEtoN(loader.ReadDword) <> $64617461 then raise DecompException.Create('missing data sig');
		i := LEtoN(loader.ReadDword);
		if i + loader.ofs <> loader.fullFileSize then
			raise DecompException.Create('bad data section len: $' + strhex(i));
		// Ignore block count.
		inc(loader.readp, 4);
		tick := 0;
		repeat
			// Begin new block.
			i := LEtoN(loader.ReadDword); // absolute tick at block start
			// The data is divided into blocks, each recording the starting tick count at the start of block. This
			// makes seeking through the song easier. But the given tick count is sometimes slightly wrong (eg. Amy98).
			// This is not a problem for seeking, but in normal playback causes a noticeable hiccup.
			// For a straight midi conversion ignore block start tick values and just keep a running tick counter.
			//if i <> tick then decomp_logger('[!] block start tick off by ' + strdec(i - tick));
			blocksize := LEtoN(loader.ReadDword); // block bytesize
			if blocksize mod 12 <> 0 then
				raise DecompException.Create(strcat('non-aligned blocksize $& @ $&', [blocksize, loader.ofs]));
			blocksize := blocksize div 12;
			while (blocksize <> 0) and (loader.readp + 4 <= loader.endp) do begin
				i := LEtoN(loader.ReadDword);
				if (tick or i) >= $80000000 then
					raise DecompException.Create(strcat('delta time way too big: $& + $& @ $&', [tick, i, loader.ofs]));
				inc(tick, i);
				inc(loader.readp, 4); // ignore stream ID

				// Read and interpret midi event.
				e[0] := byte((loader.readp + 3)^);
				if e[0] >= 2 then raise DecompException.Create('bad event type: $' + strhex(e[0]));
				if e[0] <> 0 then
					midiobject.SetTempo(tick, LEtoN(loader.ReadDword) and $FFFFFF)
				else begin
					e[0] := loader.ReadByte;
					e[1] := loader.ReadByte;
					e[2] := loader.ReadByte;
					inc(loader.readp);
					i := 3; // most midi messages are 3 bytes, C0 instrument change and D0 aftertouch are 2 bytes.
					if e[0] and $F0 in [$C0, $D0] then i := 2;
					midiobject.track[e[0] and $F].AddCommand(tick, i, @e[0]);
				end;

				dec(blocksize);
			end;
		until loader.readp >= loader.endp;

		midiobject.Finalise(outputbuffer, i);
		SaveFile(PathCombine([outdir, basename], 'mid'), outputbuffer, i);
		freemem(outputbuffer); outputbuffer := NIL;
		result := TRUE;

	finally
		midiobject.Destroy; midiobject := NIL;
	end;
end;

