{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function ReadFileName(const loader : TFileLoader; len : byte) : string31;
// Read a fixed-length string from loader's current read position, advance the read pointer. Trim nulls and spaces.
var i : byte;
begin
	byte(result[0]) := 0;
	move(loader.readp^, result[1], len);
	inc(loader.readp, len);
	i := 1;
	while (i <= len) and NOT (result[i] in [#0,' ']) do inc(i);
	byte(result[0]) := i - 1;
end;

procedure DispatchAndKeep(const outputdir, name : UTF8string; buf : pointer; size : dword; game : PGameRecord);
var subloader : TFileLoader;
	path : UTF8string;
	save : boolean;
begin
	with decomp_param do if (fileFilter <> '') and (NOT WildcardMatch(fileFilter, name)) then exit;
	try
		path := PathCombine([outputdir, 'temp', name]);
		save := decomp_param.keepTemp;
		// May need to combine 2+ images during conversion, easiest using original files. Always save such graphics.
		if NOT save then case lowercase(ExtractFileExt(name)) of
			'.as2', '.da1', '.ew', '.g', '.gdt', '.mrs', '.vrs', '.xmg': save := TRUE;
		end;
		if save then SaveFile(path, buf, size);

		subloader := TFileLoader.FromMemory(buf, size, FALSE);
		subloader.fileName := path;
		DispatchFile(path, game, subloader); // dispatch frees subloader
	except
		on e : DecompException do LogError(e.Message);
	end;
end;

function Decomp_Split(
	const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord; size : dword = 0) : boolean;
// Unpacks and re-dispaches files that were simply appended together. Offsets and sizes must be in gameConst.hardBytes,
// or parameter size must be non-zero for equal-sized pieces.
// Throws DecompException for bad failures, returns TRUE if successful.
var i : dword;
	basename, ext : UTF8string;
	readp : ^word;
begin
	result := FALSE;
	basename := lowercase(ExtractFileNameWithoutExt(loader.fileName));
	ext := lowercase(ExtractFileExt(loader.fileName));

	if size <> 0 then begin
		if size shl 1 > loader.fullFileSize then
			raise DecompException.Create(strcat('File size % < piece size %', [loader.fullFileSize, size]));
		i := 1;
		while loader.RemainingBytes >= longint(size) do begin
			DispatchAndKeep(outputdir, basename + strdec(i) + ext, loader.readp, size, game);
			inc(loader.readp, size);
			inc(i);
		end;
	end

	else with gameConst do begin
		if (length(hardBytes) = 0) or (length(hardBytes[0]) = 0) then
			raise Exception.Create('Can''t split file, hard offsets/sizes not available');
		readp := @hardBytes[0][0];

		for i := 1 to length(hardBytes[0]) shr 2 do begin
			DispatchAndKeep(outputdir, basename + strdec(i) + ext,
				loader.PtrAt(LEtoN(readp^)), LEtoN((readp + 1)^), game);
			inc(readp, 2);
		end;
	end;
	result := TRUE;
end;

function Decomp_JastPCK(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks files from a Jast Memorial Collection pack and saves them in outputdir/temp/.
// The files ought to be further split between the three contained games, and forwarded to conversion functions...
// Throws DecompException for bad failures, returns TRUE if successful.
var workbuffy : array of byte = NIL;
	filename : UTF8string;
	i, j, fileofs, compressedsize, uncompressedsize, indexofs : dword;

	procedure _Decompress;
	var z : tzstream;
		l : longint;
	begin
		z.next_in := NIL;
		l := inflateInit(z);
		if l <> z_OK then raise Exception.Create('inflateInit: ' + zError(l));

		setlength(workbuffy, uncompressedsize);

		z.next_in := loader.PtrAt(fileofs);
		z.avail_in := compressedsize;
		z.total_in := 0;
		z.next_out := @workbuffy[0];
		z.avail_out := uncompressedsize;
		z.total_out := 0;
		l := inflate(z, Z_FINISH);
		inflateEnd(z);

		if NOT (l in [Z_OK, Z_STREAM_END]) then
			raise DecompException.Create('zInflate: ' + zError(l));
	end;

begin
	result := FALSE;
	if loader.fullFileSize < $200 then raise DecompException.Create('file too tiny');

	i := BEtoN(loader.ReadDword) shr 8; // "VFT"
	dec(loader.readp);
	j := (BEtoN(loader.ReadDword) shr 8) or $202020; // "ech" or "ECH", force into lowercase
	if (i <> $564654) or (j <> $656368) then raise DecompException.Create('no vftech sig');
	dec(loader.readp);

	indexofs := LEtoN(loader.ReadDword);
	if (indexofs < $8000) or (indexofs + $80 > loader.fullFileSize) then
		raise DecompException.Create('bad list index ofs $' + strhex(indexofs));

	loader.ofs := indexofs - 1;
	while loader.readp < loader.endp do begin
		fileofs := LEtoN(loader.ReadDword);
		if (fileofs < 10) or (fileofs >= indexofs) then
			raise DecompException.Create(strcat('bad file ofs $& @ $&', [fileofs, loader.ofs - 4]));
		dec(fileofs); // given value is off by one

		compressedsize := LEtoN(loader.ReadDword);
		if (compressedsize < 4) or (fileofs + compressedsize > indexofs) then
			raise DecompException.Create(strcat('bad file size $& @ $&', [compressedsize, loader.ofs - 4]));

		filename := loader.ReadString;
		// The file name is followed by three useless metadata strings.
		for i := 2 downto 0 do
			while (loader.readp < loader.endp) and (loader.ReadByte <> 0) do ;

		// The first dword of file data is the uncompressed size; the compressed datastream follows.
		uncompressedsize := LEtoN(loader.ReadDwordFrom(fileofs));
		inc(fileofs, 4);
		dec(compressedsize, 4);

		try
			_Decompress;
			// None of these are usefully convertable right now so just dump them.
			SaveFile(PathCombine([outputdir, 'temp', filename]), @workbuffy[0], uncompressedsize);
			//DispatchAndKeep(outputdir, filename, @workbuffy[0], uncompressedsize, game);
		except
			on e : DecompException do LogError(e.Message);
		end;

		setlength(workbuffy, 0);
	end;

	result := TRUE;
end;

function Decomp_TiareDAT(
	const loader : TFileLoader; const hedfilename, outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks files from a Jast/Tiare .DAT file, using the accompanying .HED file, and saves them in outputdir/temp/.
// The unpacked files are then further forwarded to conversion functions.
// Throws DecompException for bad failures, returns TRUE if successful.
var i : dword;
	hedfile : TFileLoader = NIL;
	filename : string[12];
begin
	result := FALSE;
	if loader.fullFileSize < $10000 then raise DecompException.Create('file too tiny');

	// Load the header file.
	try
		hedfile := TFileLoader.Open(PathCombine([ExtractFileDir(loader.filename), hedfilename]));
	except on E : Exception do
		raise DecompException.Create(E.Message);
	end;

	try
		if hedfile.fullFileSize < $1000 then raise DecompException.Create('hed too tiny');
		if (hedfile.fullFileSize - 17) mod 20 <> 0 then raise DecompException.Create('uneven hed size');
		// The hed file should start with a reference to its associated dat.
		filename := hedfile.ReadString;
		if upcase(filename) <> upcase(ExtractFileName(loader.fileName)) then
			raise DecompException.Create('hed does not start with dat file name');

		// Skip rest of list header.
		hedfile.ofs := 17;

		// Start extracting resources from the dat.
		while (hedfile.readp + 20 <= hedfile.endp) do begin

			filename := ReadFileName(hedfile, 16);
			if filename = 'end' then break;

			loader.ofs := LEtoN(hedfile.ReadDword);

			// Read the next data offset to use as this one's end offset.
			if hedfile.readp + 20 <= hedfile.endp then
				i := LEtoN(dword((hedfile.readp + 16)^))
			else
				i := loader.buffySize;

			// Validate the data offsets.
			if i > loader.buffySize then begin
				LogError(strcat('% end $& past dat end $&', [filename, i, loader.buffySize]));
				i := loader.buffySize;
			end;
			if i < loader.ofs then begin
				LogError(strcat('% end $& before start $&', [filename, i, loader.ofs]));
				i := loader.buffySize;
			end;

			// Get the data byte size.
			dec(i, loader.ofs);

			DispatchAndKeep(outputdir, filename, loader.readp, i, game);
		end;

	finally
		if hedfile <> NIL then begin hedfile.free; hedfile := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_ExcellentDAT(
	const loader : TFileLoader; const listfilename, outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks files from an Excellents .DAT file, using the accompanying .LST file, and saves them in outputdir/temp/.
// The unpacked files are then further forwarded to conversion functions.
// Throws DecompException for bad failures, returns TRUE if successful.
var i : dword;
	listfile : TFileLoader = NIL;
	filename : string[12];
begin
	result := FALSE;
	// Is it a useless MEMORY.DAT or SAVEx.DAT?
	filename := lowercase(ExtractFileName(loader.fileName));
	if (filename = 'memory.dat') or (filename = 'save0.dat') or (filename = 'save1.dat') or (filename = 'save2.dat')
	then raise SkipException.Create('this has no resources');

	// Load the list file.
	try
		listfile := TFileLoader.Open(PathCombine([ExtractFileDir(loader.filename), listfilename]));
	except on E : Exception do
		raise DecompException.Create(E.Message);
	end;

	try
		// Check list file signature.
		setlength(filename, 11);
		move(listfile.readp^, filename[1], 11);

		if filename <> 'D_Lib -02- ' then raise DecompException.Create('no .lst signature');

		// Skip rest of list header.
		listfile.ofs := 16;

		// Start extracting resources from the dat.
		while (listfile.readp + 16 <= listfile.endp) do begin

			// Read the resource filename.
			byte(filename[0]) := 12;
			move(listfile.readp^, filename[1], 12);
			while (byte(filename[0]) <> 0) and (filename[byte(filename[0])] in [#0,#32]) do dec(byte(filename[0]));
			i := pos('\', filename); // replace backslashes with underscore
			if i <> 0 then filename[i] := '_';
			if filename = '[[End]]' then break;

			// Read the data offset.
			inc(listfile.readp, 12);
			loader.ofs := LEtoN(listfile.ReadDword);

			// Read the next data offset to use as this one's end offset, unless next is [[End]] or EOF.
			if (listfile.readp + 16 <= listfile.endp) and (dword(listfile.readp^) <> BEtoN(dword($5B5B456E))) then
				i := LEtoN(dword((listfile.readp + 12)^))
			else
				i := loader.buffySize;

			// Validate the data offsets.
			if i > loader.buffySize then begin
				LogError(strcat('end ofs $& beyond .dat end $&', [i, loader.buffySize]));
				i := loader.buffySize;
			end;
			if i < loader.ofs then begin
				LogError(strcat('end ofs $& before start offset $&', [i, loader.ofs]));
				i := loader.buffySize;
			end;

			// Get the data byte size.
			dec(i, loader.ofs);

			// Suffixless files are probably graphics, so give them a .G extension.
			if (pos('.', filename) = 0) and (i > 8) then begin
				// Check that the file starts with a graphic signature or correct-looking image size.
				if (BEtoN(word(loader.readp^)) = $5069) // Pi sig
				or (word(loader.readp^) <> 0)
				and (word((loader.readp + 2)^) <> 0)
				and (byte(loader.readp^) in [0..3]) // valid image widths: 001..3FF
				and (byte((loader.readp + 2)^) in [0..2]) // valid heights: 001..2FF
				then filename := filename + '.g';
			end;

			DispatchAndKeep(outputdir, filename, loader.readp, i, game);
		end;

	finally
		if listfile <> NIL then begin listfile.free; listfile := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_ExcellentLib(
	const loader : TFileLoader; const catfilename, outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks files from an Excellents .LIB file, using the accompanying .CAT file, and saves them in outputdir/temp/.
// Catfilename should not have a path; the same path is used that loader has.
// The unpacked files are then further forwarded to conversion functions.
// Throws DecompException for bad failures, returns TRUE if successful.
var catp, resp : pointer;
	catfile : TFileLoader;
	catofs, catpsize : dword;
	comptype, filesize, startofs : dword;
	filename : string[12];
begin
	result := FALSE;
	// Check lib file signature.
	if loader.ReadDword <> BEtoN(dword($4C696230)) then raise DecompException.Create('no .lib signature');

	resp := NIL; catp := NIL;
	try
		// Load the catalog.
		catfile := TFileLoader.Open(PathCombine([ExtractFileDir(loader.filename), catfilename]));

		try
			// Check cat file signature.
			if catfile.ReadDword <> BEtoN(dword($43617431)) then
				raise DecompException.Create('no .cat signature');

			if loader.ReadWord <> catfile.ReadWord then
				raise DecompException.Create('.cat and .lib have mismatching postsig word');

			// Uncompress the Softdisk-style LZ77 stream.
			Decompress_LZ77(catfile.readp, catfile.buffySize - 6, catp, catpsize);

		finally
			catfile.Destroy; catfile := NIL;
		end;

		// Start extracting resources from the lib.
		catofs := 0;

		while catofs + 22 <= catpsize do begin
			// Read the resource filename.
			byte(filename[0]) := 12;
			move((catp + catofs)^, filename[1], 12);
			inc(catofs, 12);

			// Remove trailing spaces.
			filename := trim(filename);
			// Suffixless files are probably graphics, so give them a .G extension.
			// (Mayclub98 has Z65 in DISK_A which is a graphic, but is LZ77-compressed in addition to Pi, probably
			// programmer error. Still works though.)
			if (pos('.', filename) = 0) then filename := filename + '.g';

			with decomp_param do if (fileFilter <> '') and (NOT WildcardMatch(fileFilter, filename)) then begin
				inc(catofs, 10);
				continue;
			end;

			// Read the compression type.
			comptype := word((catp + catofs)^);
			inc(catofs, 2);
			if comptype > 1 then raise DecompException.Create(strcat('bad compression % for %', [comptype, filename]));

			// Read the resource size and location.
			filesize := dword((catp + catofs)^);
			inc(catofs, 4);
			startofs := dword((catp + catofs)^) + 6;
			inc(catofs, 4);

			// Validate the data offsets.
			if startofs > loader.buffySize then begin
				LogError(filename + ' start ofs beyond .lib end!');
				startofs := loader.buffySize;
			end;
			if startofs + filesize > loader.buffySize then begin
				LogError(filename + ' file end beyond .lib end!');
				filesize := loader.buffySize - startofs;
			end;

			if filesize = 0 then
				decomp_logger('[!] filesize 0!')
			else begin
				if comptype = 0 then
					DispatchAndKeep(outputdir, filename, loader.PtrAt(startofs), filesize, game)
				else begin
					// LZSS-compressed files...
					inc(startofs, 4); dec(filesize, 4); // skip unpacked size dword
					Decompress_LZ77(loader.PtrAt(startofs), filesize, resp, filesize);
					DispatchAndKeep(outputdir, filename, resp, filesize, game);
					freemem(resp); resp := NIL;
				end;
			end;
		end;

	finally
		if catp <> NIL then begin freemem(catp); catp := NIL; end;
		if resp <> NIL then begin freemem(resp); resp := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_AILArc(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from AIL's archives. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
var i, count, readofs, filesize, compression, outsize : dword;
	basename, ext : UTF8string;
	p : pointer = NIL;
	suffix : string[3];
begin
	result := FALSE;
	count := LEtoN(loader.ReadWord);
	if (count = 0) or (count > 200) or (count * 8 > loader.fullFileSize) then
		raise DecompException.Create('sus filecount');

	basename := lowercase(ExtractFileNameWithoutExt(loader.fileName));
	ext := lowercase(ExtractFileExt(loader.fileName));
	readofs := (count + 1) shl 1;
	for i := 0 to count - 1 do begin
		filesize := LEtoN(loader.ReadWord);
		//decomp_logger(strcat('readofs $&/$&, size $&', [readofs, loader.fullFileSize, filesize]));
		if readofs + filesize > loader.fullFileSize then begin
			if i + 1 = count then break; // the last file often has an invalid size, ignore it
			raise DecompException.Create('file oob @ $' + strhex(loader.ofs - 2));
		end;
		if filesize = 0 then continue;
		if filesize < 4 then raise DecompException.Create('sus filesize @ $' + strhex(loader.ofs - 2));

		compression := LEtoN(loader.ReadWordFrom(readofs));
		inc(readofs, 2);
		dec(filesize, 2);
		// Pre-reserve memory, some of these files expand to as much as 2.5 megs!? Such a file has multiple 640x400
		// size indicators in close proximity, multiple frames?..
		outsize := 2000000 + filesize shl 4;
		getmem(p, outsize);

		try
			case compression of
				0: Decompress_LZSS3(
					loader.PtrAt(readofs), loader.PtrAt(readofs) + filesize, p, outsize, 258, FALSE, TRUE, $FF);
				1: Decompress_LZSS(
					loader.PtrAt(readofs), loader.PtrAt(readofs) + filesize, p, outsize, 18, FALSE, TRUE, $FF);
				else begin
					if decomp_param.verbose then decomp_logger('unk compression $' + strhex(compression));
					dec(readofs, 2);
					inc(filesize, 2);
					DispatchAndKeep(outputdir, basename + '.' + strdec(i) + '.bin', loader.PtrAt(readofs), filesize, game);
					inc(readofs, filesize);
					continue;
				end;
			end;
			inc(readofs, filesize);

			if (dword(p^) = 0) and (basename[1] = 's') then
				suffix := 'snl'
			else if BEtoN(word(p^)) = $001A then
				suffix := 'm'
			else if ext = '.mmd' then
				suffix := 'mmd'
			else
				suffix := 'ach';

			DispatchAndKeep(outputdir, basename + '_' + strdec(i) + '.' + suffix, p, outsize, game);
		finally
			freemem(p); p := NIL;
		end;
	end;
	result := TRUE;
end;

function Decomp_AtlusBin(const loader : TFileLoader; const outputdir, basename : UTF8string; game : PGameRecord) : boolean;
// Re-dispatches files from Atlus Giten Megaten bin files. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
var size, i : dword;
begin
	result := FALSE;
	if loader.fullFileSize < 8 then raise DecompException.Create('file too tiny');
	i := 0;
	// The first two filename characters identify what each bin file contains.
	case copy(basename, 1, 2) of
		'fc': // graphics
		repeat
			size := LEtoN(loader.ReadDword);
			if size < 32 then raise DecompException.Create('sus chunk size');
			if ptrint(size) > loader.RemainingBytes then raise DecompException.Create('chunk oob');
			DispatchAndKeep(outputdir, basename + '_' + strdec(i) + '.fc', loader.readp, size, game);
			inc(i);
			inc(loader.readp, size);
		until loader.readp >= loader.endp;
		else raise SkipException.Create('unsupported subtype');
	end;
	result := TRUE;
end;

function Decomp_MegatechVOL(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from Megatech VOL archives. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
var index, ofs, nextofs, i : dword;
	basename : UTF8string;
	suffix : string[3];
begin
	result := FALSE;
	ofs := LEtoN(loader.ReadDword);
	if (ofs < $20) or (ofs >= loader.fullFileSize) or (ofs and $F <> 0) then
		raise DecompException.Create('sus 1st ofs');

	index := 0;
	basename := ExtractFileNameWithoutExt(loader.fileName);
	while TRUE do begin
		nextofs := LEtoN(loader.ReadDword);
		if (nextofs < ofs) or (nextofs > loader.fullFileSize) then
			raise DecompException.Create('sus ofs $' + strhex(nextofs));

		if ofs <> nextofs then begin
			i := LEtoN(loader.ReadDwordFrom(ofs));
			case i and $FFFF of
				$4D45: suffix := 'emi';
				$4347, $444D, $4349, $5843: // GC, MD, IC, CX
				begin
					byte(suffix[0]) := 2;
					byte(suffix[1]) := (i and $FF) or $20; // lowercase
					byte(suffix[2]) := ((i shr 8) and $FF) or $20;
				end;
				else begin
					if i = $61657243 then // Crea
						suffix := 'voc'
					else if i and $FFFFFF = $485047 then
						suffix := 'gph'
					else
						suffix := 'bin';
				end;
			end;
			DispatchAndKeep(
				outputdir, basename + '.' + strdec(index) + '.' + suffix, loader.PtrAt(ofs), nextofs - ofs, game);
		end;

		if nextofs = loader.fullFileSize then break;
		ofs := nextofs;
		inc(index);
	end;

	result := TRUE;
end;

function Decomp_DesArc(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// DAC and DLB were used in Excellents/Desire games.
var filename : string[12];
	i, j, size, readofs, numfiles : dword;
begin
	result := FALSE;
	i := loader.fullFileSize shr 2;
	if i < $400 then raise DecompException.Create('file too tiny');

	// DLB needs a total xor ff, otherwise the same as DAC.
	if dword(loader.readp^) = BEtoN(dword($444C421A)) then begin
		inc(loader.readp, 12);
		XorBuffer(loader.readp, loader.endp, $FF);
		j := LEtoN(loader.ReadDword);
		if j > i then raise DecompException.Create('sus 1st file ofs');
	end;

	readofs := loader.ofs;
	numfiles := 0;
	size := 0;
	while (dword(loader.readp^) <> 0) and (loader.ofs < i) do begin
		if NOT (byte(loader.readp^) in [$21..$7A]) then
			raise DecompException.Create('sus char in filename @ $' + strhex(loader.ofs));
		inc(loader.readp, 12);
		j := LEtoN(loader.ReadDword);
		if (j > $40000) or (loader.ofs + size + j > loader.fullFileSize) then
			raise DecompException.Create('File oob @ $' + strhex(loader.ofs));
		inc(size, j);
		inc(numfiles);
	end;
	if numfiles < 10 then raise DecompException.Create('sus index');
	inc(loader.readp, 16);
	i := loader.ofs;
	loader.ofs := readofs;
	readofs := i;

	for i := numfiles - 1 downto 0 do begin
		filename := ReadFileName(loader, 12);
		size := LEtoN(loader.ReadDword);
		DispatchAndKeep(outputdir, filename, loader.PtrAt(readofs), size, game);
		inc(readofs, size);
	end;

	result := TRUE;
end;

function Decomp_FA1(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// This format was used in some Himeya and Forest/Foster games.
var i, readofs, archivesize, inputsize, uncompressedsize : dword;
	filecount : word;
	archiveflag, fileattribute : byte;
	filename : string[12];

	procedure _DecompressAndDispatch;
	var outp : pointer = NIL;
	begin
		try try
			Decompress_CsLZ16(loader.PtrAt(readofs), loader.PtrAt(readofs + inputsize), outp, uncompressedsize);
			DispatchAndKeep(outputdir, filename, outp, uncompressedsize, game);
		except
			on e : DecompException do LogError(e.Message);
		end;
		finally
			if outp <> NIL then begin freemem(outp); outp := NIL; end;
		end;
	end;

begin
	result := FALSE;
	if loader.ReadDword <> BEtoN(dword($46413100)) then // "FA1 "
		raise DecompException.Create('no fa1 sig');

	archivesize := LEtoN(loader.ReadDword);
	if (archivesize + 20 > loader.fullFileSize) or (archivesize < loader.fullFileSize shr 1) then
		raise DecompException.Create('bad arc size: ' + strdec(archivesize));

	filecount := LEtoN(loader.ReadWord);
	if (filecount = 0) or (filecount > 999) then raise DecompException.Create('sus filecount: ' + strdec(filecount));

	i := loader.ReadByte;
	if i > 5 then raise DecompException.Create('bad disk id: $' + strhex(i));
	archiveflag := loader.ReadByte;
	if NOT (archiveflag in [0,$80]) then raise DecompException.Create('bad arc flag: $' + strhex(archiveflag));

	// Read the file list. Starts from first $400 boundary after end of archive data.
	loader.ofs := (archivesize + $3FF) and $FFFFFC00;

	// If flag is set, the whole section is encrypted with XOR $FF.
	if archiveflag and $80 <> 0 then XorBuffer(loader.readp, loader.endp, $FF);
	readofs := $C;

	while loader.readp + 20 <= loader.endp do begin
		// Each entry is 8+3 filename, then attribute flag and two size dwords.
		i := 0;
		while i < 8 do begin
			if byte((loader.readp + i)^) in [0, $20] then break;
			byte(filename[i + 1]) := byte((loader.readp + i)^);
			inc(i);
		end;
		inc(i);
		dword((@filename[i])^) := dword((loader.readp + 7)^);
		filename[i] := '.';
		inc(i, 3);
		while (i <> 0) and (filename[i] in [#0, #32, '.']) do dec(i);
		byte(filename[0]) := i;
		inc(loader.readp, 11);

		fileattribute := loader.ReadByte;
		if fileattribute > 1 then decomp_logger(strcat('[!] attr $& in %', [fileattribute, filename]));
		inputsize := LEtoN(loader.ReadDword);
		uncompressedsize := LEtoN(loader.ReadDword);
		if readofs + inputsize > archivesize then raise DecompException.Create(filename + ' out of bounds');

		try
			if fileattribute and 1 <> 0 then
				_DecompressAndDispatch
			else
				DispatchAndKeep(outputdir, filename, loader.PtrAt(readofs), inputsize, game);

		except
			on e : DecompException do LogError(e.Message);
		end;

		// The next file is aligned at a word boundary.
		readofs := (readofs + inputsize + 1) and $FFFFFFFE;
	end;

	result := TRUE;
end;

function Decomp_FA2(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// This format was used in some Forest/Foster Windows games.
var readofs, flags, filecount : dword;

	procedure _ReadAll;
	var filelist : pointer = NIL;
		subloader : TFileLoader = NIL;
		indexsize : dword = 0;
		inputsize, uncompressedsize : dword;
		filename : string[12];
		fileattribute : byte;

		procedure _DecompressAndDispatch;
		var outp : pointer = NIL;
		begin
			try try
				Decompress_CsLZ16(
					loader.PtrAt(readofs), loader.PtrAt(readofs + inputsize), outp, uncompressedsize, TRUE);
				DispatchAndKeep(outputdir, filename, outp, uncompressedsize, game);
			except
				on e : DecompException do LogError(e.Message);
			end;
			finally
				if outp <> NIL then begin freemem(outp); outp := NIL; end;
			end;
		end;

	begin
		try
			// Uncompressed file index?
			if flags and 1 = 0 then
				subloader := TFileLoader.FromMemory(loader.PtrAt(readofs), loader.fullFileSize - readofs, FALSE)
			else begin
				// Compressed index, unpack to filelist^, place into subloader for easy access.
				Decompress_CsLZ16(loader.PtrAt(readofs), loader.endp, filelist, indexsize, TRUE);
				subloader := TFileLoader.FromMemory(filelist, indexsize, TRUE);
			end;
			//Savefile('out.bin', subloader.readp, subloader.fullFileSize); exit; // to dump the index

			// Read all files in the index.
			readofs := $10;
			while subloader.readp < subloader.endp do begin
				filename := ReadFileName(subloader, 15);
				fileattribute := subloader.ReadByte;
				if fileattribute > 2 then
					raise DecompException.Create(filename + ' bad attrib $' + strhex(fileattribute));
				inc(subloader.readp, 8); // unknown dwords

				uncompressedsize := LEtoN(subloader.ReadDword);
				inputsize := LEtoN(subloader.ReadDword);
				if readofs + inputsize > loader.fullFileSize then
					raise DecompException.Create(filename + ' out of bounds');

				try
					if fileattribute and 2 <> 0 then
						_DecompressAndDispatch
					else
						DispatchAndKeep(outputdir, filename, loader.PtrAt(readofs), inputsize, game);

				except
					on e : DecompException do LogError(e.Message);
				end;

				readofs := (readofs + inputsize + $F) and $FFFFFFF0; // 16-byte alignment
			end;

		finally
			if subloader <> NIL then subloader.Destroy;
		end;
	end;

begin
	result := FALSE;
	if loader.ReadDword <> BEtoN(dword($46413200)) then // "FA2 "
		raise DecompException.Create('no fa2 sig');

	flags := LEtoN(loader.ReadDword);
	if flags > 1 then raise DecompException.Create('bad flags: $' + strhex(flags));

	readofs := LEtoN(loader.ReadDword);
	if (readofs < 1000) or (readofs + $20 > loader.fullFileSize) then
		raise DecompException.Create('bad list ofs: $' + strhex(readofs));

	filecount := LEtoN(loader.ReadDword);
	if (filecount = 0) or (filecount > 4999) then raise DecompException.Create('sus filecount: ' + strdec(filecount));

	_ReadAll;

	result := TRUE;
end;

function Decomp_OtakuArc(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// This format was used for Windows ports by Otaku Publishing, as well as a few ZyX games ported by G-Collections.
var i, fileofs, inputsize : dword;
	indexsize : word;
	filename : string[12];
begin
	result := FALSE;

	if loader.fullFileSize < $200 then raise DecompException.Create('file too tiny');
	indexsize := LEtoN(loader.ReadWord);
	if (indexsize = 0) or (indexsize >= loader.fullFileSize) then
		raise DecompException.Create('bad index size: ' + strdec(indexsize));

	while loader.ofs + 16 <= indexsize do begin
		// Read next file name. Each entry is a normal "xxx.yyy" string, up to 12 chars, padded with zeroes.
		filename := ReadFileName(loader, 12);
		if filename = '' then break; // empty filename indicates end of index

		// Read this file's offset, and next file's offset to calculate size.
		fileofs := LEtoN(loader.ReadDword);
		if (fileofs < indexsize) or (fileofs >= loader.fullFileSize) then
			raise DecompException.Create('bad file ofs: ' + filename + ' @ $' + strhex(fileofs));
		i := LEtoN(loader.ReadDwordFrom(loader.ofs + 12));
		if (i < indexsize) or (i > loader.fullFileSize) or (i < fileofs) then
			raise DecompException.Create('bad file end ofs: ' + filename + ' @ $' + strhex(i));
		inputsize := i - fileofs;

		DispatchAndKeep(outputdir, filename, loader.PtrAt(fileofs), inputsize, game);
	end;

	result := TRUE;
end;

function Decomp_DL1(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// DL1 archives were used by C's/Himeya for Nanaei 2, YES-HG, and Divi-Dead.
// Aaru also used this format as "FL1" for their games.
var i, fileofs, inputsize : dword;
	filecount : word;
	filename : string[12];
begin
	result := FALSE;

	if loader.fullFileSize < $77000 then raise DecompException.Create('file too tiny');
	i := loader.ReadDword;
	if ((i <> BEtoN(dword($444C312E))) and (i <> BEtoN(dword($464C312E)))) or (loader.ReadDword <> BEtoN(dword($301A0000))) then
		raise DecompException.Create('no dl1.0 sig');

	filecount := LEtoN(loader.ReadWord);
	if (filecount = 0) or (filecount > $8000) then
		raise DecompException.Create('bad filecount: ' + strdec(filecount));
	fileofs := LEtoN(loader.ReadDword);
	if (fileofs < $10) or (fileofs + dword(filecount * 16) > loader.fullFileSize) then
		raise DecompException.Create('file list oob: ' + strhex(fileofs));

	// Files are kept sequentially from $10 onward, the index only gives file sizes.
	loader.ofs := fileofs;
	fileofs := $10;

	while filecount <> 0 do begin
		// Read next file name. Each entry is a normal "xxx.yyy" string, up to 12 chars, padded with zeroes.
		filename := ReadFileName(loader, 12);
		Assert(filename <> '');

		inputsize := LEtoN(loader.ReadDword);
		if (inputsize = 0) or (fileofs + inputsize >= loader.fullFileSize) then
			raise DecompException.Create(strcat('file oob: % @ $&:$&', [filename, fileofs, inputsize]));

		DispatchAndKeep(outputdir, filename, loader.PtrAt(fileofs), inputsize, game);

		inc(fileofs, inputsize);
		dec(filecount);
	end;

	result := TRUE;
end;

function Decomp_FL2(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// FL2 archives were used by Aaru.
var i, fileofs, inputsize, filecount : dword;
	filename : string[12];
begin
	result := FALSE;

	if loader.fullFileSize < $77000 then raise DecompException.Create('file too tiny');
	i := loader.ReadDword;
	if (i <> BEtoN(dword($464C322E))) or (loader.ReadDword <> BEtoN(dword($30002000))) then
		raise DecompException.Create('no fl2.0 sig');

	filecount := LEtoN(loader.ReadDword);
	if (filecount = 0) or (filecount > $8000) then
		raise DecompException.Create('bad filecount: ' + strdec(filecount));
	inc(loader.readp, 4); // unknown dword
	fileofs := LEtoN(loader.ReadDword);
	if (fileofs < $14) or (fileofs + dword(filecount * 8) > loader.fullFileSize) then
		raise DecompException.Create('file list oob: ' + strhex(fileofs));

	// Files are kept sequentially from $14 onward, the index only gives file sizes.
	loader.ofs := fileofs;
	fileofs := $20;

	while filecount <> 0 do begin
		inputsize := LEtoN(loader.ReadDword);
		if inputsize = $FFFFFFFF then break;

		// Read next file name. Each entry is a Pascal string, up to 12 characters, no padding.
		filename := string(loader.readp^);
		inc(loader.readp, length(filename) + 1);
		if filename = '' then
			decomp_logger('[!] empty filename')
		else begin
			if (inputsize = 0) or (fileofs + inputsize >= loader.fullFileSize) then
				raise DecompException.Create(strcat('file oob: % @ $&:$&', [filename, fileofs, inputsize]));

			DispatchAndKeep(outputdir, filename, loader.PtrAt(fileofs), inputsize, game);
		end;

		inc(fileofs, inputsize);
		dec(filecount);
	end;

	result := TRUE;
end;

function Decomp_MAXPCK(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks files from a Panda House MAX PCK archive and saves them in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
var filename : UTF8string;
	i, j, fileofs, compressedsize : dword;
begin
	result := FALSE;
	if loader.fullFileSize < $4200 then raise DecompException.Create('file too tiny');
	i := loader.ReadDword;
	j := loader.ReadDword;
	if (i <> BEtoN(dword($A8212590))) or (j <> BEtoN(dword($AB32B939))) then raise DecompException.Create('no pck sig');

	// Decrypt, just rotating the header by one bit.
	loader.ofs := 0;
	for i := $1FFF downto 0 do begin
		word(loader.readp^) := NtoLE(RolWord(LEtoN(word(loader.readp^))));
		inc(loader.readp, 2);
	end;

	loader.ofs := 32;
	while loader.ofs < $4000 do begin
		filename := ReadFileName(loader, 16);
		if filename = '' then break;

		fileofs := LEtoN(loader.ReadDword);
		compressedsize := LEtoN(loader.ReadDword);
		if (fileofs < $4000) or (fileofs + compressedsize > loader.fullFileSize) then
			raise DecompException.Create(strcat('bad ofs/size $&/& @ $&', [fileofs, compressedsize, loader.ofs - 8]));

		inc(loader.readp, 8); // skip timestamp info

		try
			DispatchAndKeep(outputdir, filename, loader.PtrAt(fileofs), compressedsize, game);
		except
			on e : DecompException do LogError(e.Message);
		end;
	end;

	result := TRUE;
end;

function Decomp_MIO(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// MIO archives were used by C's/Himeya for Nanaei 1 and Ash.
var i, baseofs, fileofs, inputsize : dword;
	filecount : word;
	filename : string[12];
begin
	result := FALSE;

	if loader.fullFileSize < $1000 then raise DecompException.Create('file too tiny');
	i := loader.ReadDword;
	if (i <> BEtoN(dword($4D494F44))) or (loader.ReadDword <> BEtoN(dword($41544130))) then
		raise DecompException.Create('no miodata sig');
	inc(loader.readp, 6);

	filecount := LEtoN(loader.ReadWord);
	if (filecount = 0) or (filecount > 1000) then
		raise DecompException.Create('bad filecount: ' + strdec(filecount));
	baseofs := ((filecount + 1) * 16 + $3FF) and $FFFFFC00; // files start from the next $400 boundary after file list

	while filecount <> 0 do begin
		// Read next file name. Each entry is a normal "xxx.yyy" string, up to 12 chars, padded with zeroes.
		filename := ReadFileName(loader, 12);
		Assert(filename <> '');

		inputsize := LEtoN(loader.ReadWord);
		fileofs := dword(LEtoN(loader.ReadWord) * $400) + baseofs;
		if (inputsize = 0) or (fileofs + inputsize > loader.fullFileSize) then
			raise DecompException.Create(strcat('file oob: % @ $&:$&', [filename, fileofs, inputsize]));

		DispatchAndKeep(outputdir, filename, loader.PtrAt(fileofs), inputsize, game);

		dec(filecount);
	end;

	result := TRUE;
end;

function Decomp_CsArc(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// This format was used for Windows ports by Himeya. There's an AR and ARC2 variant.
var filename : string[15];
	i, listofs, listcount, key1, key2 : dword;

	procedure _Decrypt32(p : pdword; endp : pointer);
	var key3 : dword;
	begin
		while p < endp do begin
			key3 := dword(key1 + key2);
			p^ := dword(p^ - key3);
			inc(p); // inc by dword
			key1 := key2;
			key2 := key3;
		end;
	end;

	procedure _Decrypt8(p : pbyte; endp : pointer);
	var key3 : byte;
	begin
		while p < endp do begin
			key3 := byte(byte(key1) + byte(key2));
			p^ := byte(p^ - key3);
			inc(p);
			key1 := key2;
			key2 := key3;
		end;
	end;

	procedure _UnpackARC2;
	var fileofs, inputsize, numwritten : dword;
		filetype : char;

		procedure _NewFile;
		var j : dword;
			p : pointer;
		begin
			j := numwritten;
			inc(numwritten);
			while j <> 0 do begin
				dec(j);
				p := loader.PtrAt(listofs + j shl 4);
				if CompareByte(p^, loader.readp^, 16) <= 0 then begin inc(j); break; end;
				move(p^, (p + 16)^, 16);
			end;
			move(loader.readp^, loader.PtrAt(listofs + j shl 4)^, 16);
		end;

		function _IsDupe : boolean;
		var comp, min, mid, max : longint;
		begin
			result := FALSE;
			if numwritten = 0 then exit;
			// binary search
			min := 0; max := numwritten - 1;
			repeat
				mid := (min + max) shr 1;
				comp := CompareByte(loader.PtrAt(listofs + dword(mid shl 4))^, loader.readp^, 16);
				if comp = 0 then begin result := TRUE; exit; end;
				if comp < 0 then
					min := mid + 1
				else
					max := mid - 1;
			until min > max;
		end;

	begin
		listcount := LEtoN(loader.ReadDword);
		if (listcount = 0) or (listcount > $FFFF) then
			raise DecompException.Create('bad filecount: ' + strdec(listcount));
		listofs := LEtoN(loader.ReadDword);
		if (listofs <> $10000) or (listofs > loader.fullFileSize shr 1) then
			raise DecompException.Create('bad filelist ofs: $' + strhex(listofs));
		loader.ofs := listofs;
		dec(listofs, 16); // hack for _IsDupe:
		// array of char[16], sorted on new insert; each new type+filename goes here for easy binary search for duplicates.
		// Must start building it at 16 bytes before actual list to avoid stomping the first list item.
		numwritten := 0;

		while listcount <> 0 do begin
			dec(listcount);
			if NOT (char(loader.readp^) in ['w','l']) then begin
				// There are many duplicate files. Any filename+type already seen can be skipped.
				// But don't de-duplicate voice waves, there are too many thousands of them, takes too long.
				if _IsDupe then begin
					inc(loader.readp, 32);
					continue;
				end;
				_NewFile;
			end;

			filetype := char(loader.ReadByte);
			// Read next file name. Each entry is a normal "xxx.yyy" string, up to 15 chars, padded with nulls.
			filename := ReadFileName(loader, 15);
			if filename = '' then break;

			case filetype of
				'a': filename := filename + '.anm';
				'b': filename := filename + '.bmp';
				'l': filename := filename + '.ldp';
				'm': filename := filename + '.mds';
				's': filename := filename + '.sco';
				'w': filename := filename + '.wav';
				'x': filename := filename + '.syx';
				#1..#4: filename := filename + '.' + chr(byte(filetype) + $30);
				else raise DecompException.Create('unknown filetype: ' + filetype + ' @ $' + strhex(loader.ofs));
			end;

			// Read file's start offset and size.
			fileofs := LEtoN(loader.ReadDword);
			if (fileofs < loader.ofs) or (fileofs >= loader.fullFileSize) then
				raise DecompException.Create('bad file ofs: ' + filename + ' @ $' + strhex(fileofs));
			inputsize := LEtoN(loader.ReadDword);
			if fileofs + inputsize > loader.fullFileSize then
				raise DecompException.Create(strcat('bad file size: % @ $&: $&', [filename, fileofs, inputsize]));
			key1 := LEtoN(loader.ReadDword);
			key2 := LEtoN(loader.ReadDword);

			if (key1 or key2) <> 0 then _Decrypt32(loader.PtrAt(fileofs), loader.PtrAt(fileofs) + inputsize);

			DispatchAndKeep(outputdir, filename, loader.PtrAt(fileofs), inputsize, game);
		end;
	end;

	procedure _UnpackAR;
	var ext : UTF8string;
		fileofs, inputsize : dword;
		k1, k2 : byte;
	begin
		ext := lowercase(ExtractFileExt(loader.fileName));
		case ext of
			'.wva': ext := '.wav';
			'.sca': ext := '.scr';
			'.ewa': ext := '.ew';
			else raise DecompException.Create('unknown filetype: ' + ext);
		end;
		k1 := (i shr 8) and $FF; // keys are the 3rd and 4th byte of the file, right after 2-byte sig
		k2 := i and $FF;

		listofs := LEtoN(loader.ReadDword);
		if (listofs <> $C) or (listofs > loader.fullFileSize shr 1) then
			raise DecompException.Create('bad filelist ofs: $' + strhex(listofs));
		listcount := LEtoN(loader.ReadDword);
		if (listcount = 0) or (listcount > $FFFF) then
			raise DecompException.Create('bad filecount: ' + strdec(listcount));
		loader.ofs := listofs;

		while listcount <> 0 do begin
			dec(listcount);

			// Read next file name. Each entry is up to 8 chars, padded with nulls.
			filename := ReadFileName(loader, 8);
			Assert(filename <> '');
			filename := filename + ext;

			// Read file's start offset and size.
			fileofs := LEtoN(loader.ReadDword);
			if (fileofs < loader.ofs) or (fileofs >= loader.fullFileSize) then
				raise DecompException.Create('bad file ofs: ' + filename + ' @ $' + strhex(fileofs));
			inputsize := LEtoN(loader.ReadDword);
			if fileofs + inputsize > loader.fullFileSize then
				raise DecompException.Create(strcat('bad file size: % @ $&: $&', [filename, fileofs, inputsize]));

			if (k1 or k2) <> 0 then begin
				key1 := k1; key2 := k2;
				_Decrypt8(loader.PtrAt(fileofs), loader.PtrAt(fileofs) + inputsize);
			end;

			DispatchAndKeep(outputdir, filename, loader.PtrAt(fileofs), inputsize, game);
		end;
	end;

begin
	result := FALSE;

	if loader.fullFileSize < $20000 then raise DecompException.Create('file too tiny');

	i := BEtoN(loader.ReadDword);
	if i = $61726332 then
		_UnpackARC2
	else if i shr 16 = $4152 then
		_UnpackAR
	else
		raise DecompException.Create('no csarc sig');

	result := TRUE;
end;

function Decomp_CsArc3(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// This format was used for Windows ports by Himeya. This is more complex than the AR/ARC2 formats.
var listendp, buffy : pointer;
	filename, filename2 : string31;
	i, flags, sectorsize, database, listbase, fileofs, srcsize : dword;
	b, nameofs : byte;
begin
	result := FALSE;
	if loader.fullFileSize < $10000 then raise DecompException.Create('file too tiny');
	if (loader.ReadDword <> BEtoN(dword($61726333))) then // arc3
		raise DecompException.Create('no arc3 sig');

	i := BEtoN(loader.ReadDword);
	if i <> 1 then raise DecompException.Create('sus ver ' + strdec(i));

	sectorsize := BEtoN(loader.ReadDword);
	if sectorsize <> $800 then begin
		decomp_logger('[!] sus sector size $' + strhex(sectorsize));
		sectorsize := $800;
	end;

	database := BetoN(loader.ReadDword);
	if (database < 2) or (database * sectorsize >= loader.fullFileSize) then
		raise DecompException.Create('bad data base');
	database := database * sectorsize;

	i := BEtoN(loader.ReadDword);
	if i * sectorsize > loader.fullFileSize then decomp_logger('[!] bad total sectors $' + strhex(i));

	i := BEtoN(loader.ReadDword);
	if i <> 0 then raise DecompException.Create('sus 6th dword');

	listbase := BEtoN(loader.ReadDword);
	if listbase <> 1 then raise DecompException.Create('sus list base');
	listbase := listbase * sectorsize;

	i := BEtoN(loader.ReadDword);
	if (i < $100) or (i > $80000) then raise DecompException.Create('sus list size');
	listendp := loader.PtrAt(listbase) + i;

	// Read the file list.
	filename := '';
	loader.ofs := listbase;
	if NOT byte(loader.readp^) in [$F1..$FE] then raise DecompException.Create('sus 1st file in list');
	while loader.readp < listendp do begin
		b := loader.ReadByte;
		if b = 0 then continue;

		if b = $FF then begin
			inc(byte(filename[length(filename)]));
			i := BEtoN(loader.ReadDword) shr 8;
			dec(loader.readp);
			//decomp_logger('[' + filename + ']'#9 + strhex((i and $7FFFFF) * sectorsize));
		end
		else if b > $F0 then begin
			b := b and $F;
			move(loader.readp^, filename[1], b);
			inc(loader.readp, b);
			byte(filename[0]) := b;
			dec(loader.readp);
			i := BEtoN(loader.ReadDword) and $FFFFFF;
			if i and $800000 = 0 then inc(loader.readp, 3);
			//dec(loader.readp); decomp_logger('[' + filename + ']'#9 + strhex(i * sectorsize) + #9 + strhex((BEtoN(loader.ReadDword) and $FFFFFF) * sectorsize));
		end
		else if b < $F0 then begin
			nameofs := b shr 4;
			b := b and $F;
			if nameofs + b > $F then raise DecompException.Create('[!] too long name @ $' + strhex(loader.ofs - 1));
			move(loader.readp^, filename[nameofs + 1], b);
			inc(loader.readp, b);
			byte(filename[0]) := nameofs + b;

			i := BEtoN(loader.ReadDword) shr 8;
			dec(loader.readp);
			//decomp_logger('[' + filename + ']'#9 + strhex((i and $7FFFFF) * sectorsize));
		end
		else begin // b = $F0, duplicate file
			//decomp_logger('[' + filename + ']+'#9 + strhex((BEtoN(dword(loader.readp^)) shr 8) * sectorsize));
			i := BEtoN(loader.ReadDword) shr 8;
			dec(loader.readp);
		end;

		if game^.g in [gid.AdamDF, gid.AdamDF_e] then
			while i and $800000 = 0 do begin
				i := BEtoN(loader.ReadDword) shr 8;
				dec(loader.readp);
			end;
		if b = $F0 then continue;
		i := i and $7FFFFF;

		// Unshuffle the filename. Check if user is interested in this file.
		if length(filename) > 3 then
			filename2 := copy(filename, 4) + '.' + copy(filename, 1, 3)
		else
			filename2 := filename;
		while (filename2 <> '') and (filename2[length(filename2)] = ' ') do dec(byte(filename2[0])); // trim end
		with decomp_param do if (fileFilter <> '') and (NOT WildcardMatch(fileFilter, filename2)) then continue;

		fileofs := i * sectorsize + database;
		if fileofs + 32 >= loader.fullFileSize then raise DecompException.Create('file start oob: ' + filename2);
		listbase := loader.ofs;
		loader.ofs := fileofs;

		srcsize := BEtoN(dword((loader.readp + 8)^));
		//decomp_logger(strcat('fileofs $&, srcsize $&, loader full size $&', [fileofs, srcsize, loader.fullFileSize]));
		if fileofs + srcsize  + 32 > loader.fullFileSize then raise DecompException.Create('file end oob: ' + filename2);
		flags := BEtoN(dword((loader.readp + 20)^));
		inc(loader.readp, 32);
		decomp_logger(strcat('[%]'#9'&'#9'&', [filename2, fileofs, srcsize]));
		// Decrypt file data if needed.
		if flags and 2 <> 0 then XorBuffer(loader.readp, loader.readp + srcsize, $FF);

		if word(loader.readp^) <> BEtoN(word($6C7A)) then
			DispatchAndKeep(outputdir, filename2, loader.readp, srcsize, game)
		else begin
			// Starts with "lz" and uncompressed size dword, need to decompress.
			inc(loader.readp, 2);
			i := BEtoN(loader.ReadDword);
			if i <> 0 then begin
				getmem(buffy, i);
				try
					Decompress_LZZE(loader, srcsize - 6, buffy, i);
					if i <> 0 then DispatchAndKeep(outputdir, filename2, buffy, i, game);
				except
					on e : Exception do LogError(e.Message);
				end;
				freemem(buffy);
			end;
		end;

		loader.ofs := listbase;
	end;

	result := TRUE;
end;

function Decomp_SM2MPX10(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// This Ikura GDL archive was used by some DO and ZyX games ported by G-Collections.
var i, fileofs, inputsize, numfiles : dword;
	filename : string[12];
begin
	result := FALSE;

	if loader.fullFileSize < $200 then raise DecompException.Create('file too tiny');
	if (loader.ReadDword <> BEtoN(dword($534D324D))) // SM2M
	or (loader.ReadDword <> BEtoN(dword($50583130))) // PX10
	then raise DecompException.Create('no sm2mpx10 sig');

	numfiles := LEtoN(loader.ReadDword);
	if (numfiles = 0) or (numfiles > 9999) then
		raise DecompException.Create('bad filecount: $' + strhex(numfiles));

	i := LEtoN(loader.ReadDword);
	if (i < $20) or (i > loader.fullFileSize shr 1) then
		raise DecompException.Create('bad filelist end ofs: $' + strhex(i));
	inc(loader.readp, 12);
	i := LEtoN(loader.ReadDword);
	if i <> $20 then raise DecompException.Create('expected filelist start ofs $20, got $' + strhex(i));

	while numfiles <> 0 do begin
		if loader.ofs + 20 >= loader.fullFileSize then
			raise DecompException.Create('unexpected eof @ $' + strhex(loader.ofs));

		// Read next file name. Each entry is a normal "xxx.yyy" string, up to 12 chars, padded with nulls.
		filename := ReadFileName(loader, 12);
		Assert(filename <> '');

		fileofs := LEtoN(loader.ReadDword);
		inputsize := LEtoN(loader.ReadDword);
		if (fileofs < loader.ofs) or (fileofs >= loader.fullFileSize) then
			raise DecompException.Create('bad file ofs: ' + filename + ' @ $' + strhex(fileofs));
		if fileofs + inputsize > loader.fullFileSize then
			raise DecompException.Create('bad file size: ' + filename + ' @ $' + strhex(inputsize));

		DispatchAndKeep(outputdir, filename, loader.PtrAt(fileofs), inputsize, game);

		dec(numfiles);
	end;

	result := TRUE;
end;

function Decomp_ScooPX(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// ScooP games used this simple archive format for all except the last few of their games.
var i, filecount : dword;
	ofslist : array of dword = NIL;
	basename, ext : UTF8string;
begin
	result := FALSE;

	if loader.fullFileSize < 350 then raise DecompException.Create('file too tiny');
	// Starts with either a byte or dword filecount.
	filecount := LEtoN(loader.ReadDword);
	if (filecount = 0) or (filecount > 999) or (filecount shl 3 > loader.fullFileSize)
	or (LEtoN(loader.ReadDwordFrom((filecount + 1) shl 2)) <> loader.fullFileSize) then begin
		loader.ofs := 0;
		filecount := loader.ReadByte;
	end;
	setlength(ofslist, filecount + 1);
	ofslist[0] := LEtoN(loader.ReadDword);
	for i := 1 to filecount do begin
		ofslist[i] := LEtoN(loader.ReadDword);
		if ofslist[i] > loader.fullFileSize then raise DecompException.Create('file startofs oob');
		if ofslist[i] <= ofslist[i - 1] + 4 then raise DecompException.Create('startofs not strictly ascending');
	end;
	if ofslist[filecount] <> loader.fullFileSize then raise DecompException.Create('last startofs != eof');
	if ofslist[0] <> loader.ofs then raise DecompException.Create('first startofs not right after header');

	basename := lowercase(ExtractFileNameWithoutExt(loader.fileName)) + '.';
	ext := lowercase(ExtractFileExt(loader.fileName));
	case ext of
		'.apg', '.agx': ext := '.apx';
		'.mcg': ext := '.mc';
		'.bix': ext := '.bin';
		'.wax','.se': ext := '.wav';
		'.six','.bgm': ext := '.mid';
		'.bmx': ext := '.bmp';
	end;

	for i := 0 to filecount - 1 do
		DispatchAndKeep(outputdir, basename + strdec(i) + ext, loader.PtrAt(ofslist[i]), ofslist[i + 1] - ofslist[i], game);

	result := TRUE;
end;

function Decomp_SZH(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches this generically-compressed file. The file is optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// SZH was used by Silence/Sogna, mostly containing their graphic files.
var buffy : pointer = NIL;
	i, bufsize : dword;
	suffix : string[3];
begin
	result := FALSE;

	if loader.fullFileSize < 8 then raise DecompException.Create('file too tiny');
	byte(suffix[0]) := 3;
	for i := 1 to 3 do begin
		if NOT (byte(loader.readp^) in [ord('A')..ord('Z')]) then
			raise DecompException.Create('sus suffix at file start');
		byte(suffix[i]) := loader.ReadByte;
	end;
	suffix := lowercase(suffix);

	Decompress_LZSSbitstream(loader, buffy, bufsize);

	try
		DispatchAndKeep(outputdir, ExtractFileNameWithoutExt(loader.fileName) + '.' + suffix, buffy, bufsize, game);
	finally
		if buffy <> NIL then freemem(buffy);
	end;

	result := TRUE;
end;

function Decomp_SGSDat(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// Sgs.dat was used in Sogna's Windows games, with some contained files further LZ-compressed.
var buffy : pointer = NIL;
	basename : UTF8string;
	bufsize, filecount, compressedsize, uncompressedsize, startofs : dword;
	compressed : boolean;
begin
	result := FALSE;

	if loader.fullFileSize < 8000000 then raise DecompException.Create('file too tiny');
	if (loader.ReadDword <> BEtoN(dword($5347532E))) // SGS.
	or (loader.ReadDword <> BEtoN(dword($44415420))) // DAT
	or (loader.ReadDword <> BEtoN(dword($312E3030))) // 1.00
	then raise DecompException.Create('no sgs sig');

	filecount := LEtoN(loader.ReadDword);
	if (filecount = 0) or (filecount > $2000) then raise DecompException.Create('sus filecount');

	while filecount <> 0 do begin
		dec(filecount);

		basename := ExtractFileName(ReadFileName(loader, 19));
		with decomp_param do if (fileFilter <> '') and (NOT WildcardMatch(fileFilter, basename)) then begin
			// If file is filtered out, don't decompress, skip to next file.
			inc(loader.readp, 13);
			continue;
		end;

		compressed := loader.ReadByte <> 0;
		compressedsize := LEtoN(loader.ReadDword);
		uncompressedsize := LEtoN(loader.ReadDword);
		startofs := LEtoN(loader.ReadDword);
		if startofs + compressedsize > loader.fullFileSize then raise DecompException.Create('file oob');

		if compressed then begin
			getmem(buffy, uncompressedsize shl 2);
			try
				Decompress_LZSS4(
					loader.PtrAt(startofs), loader.PtrAt(startofs) + compressedsize, buffy, bufsize,
					TRUE, // flag bits read top down
					$FF); // flag meaning flipped
				DispatchAndKeep(outputdir, basename, buffy, bufsize, game);
			finally
				if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
			end;
		end
		else DispatchAndKeep(outputdir, basename, loader.PtrAt(startofs), uncompressedsize, game);
	end;

	result := TRUE;
end;

function Decomp_WakuDat(const loader : TFileLoader; const outputdir : UTF8string; game : PGameRecord) : boolean;
// Unpacks and re-dispatches files from this archive. The files are optionally saved in outputdir/temp/.
// Throws DecompException for bad failures, returns TRUE if successful.
// DAT archives used in Waku Waku Mahjong Panic 2 with a XOR-encrypted index.
var filename : string[12];
	i, j, readofs, listofs : dword;
begin
	result := FALSE;
	// Wakuwaku2 has some other dat files that are not archives.
	if loader.fullFileSize <= $4000 then raise DecompException.Create('file too tiny');

	i := LEtoN(loader.ReadWordFrom(loader.fullFileSize - 2));
	if (i + $100 > loader.fullFileSize) or (i and $F <> 0) then raise DecompException.Create('sus listsize');
	listofs := loader.fullFileSize - i;
	loader.ofs := listofs;

	// Decrypt index.
	XorBuffer(loader.readp, loader.endp, $99);

	if dword(loader.readp^) <> BEtoN(dword($445F4C69)) then raise DecompException.Create('no dlib sig');
	inc(loader.readp, 16);

	for j := (loader.RemainingBytes shr 4) - 2 downto 0 do begin
		filename := ReadFileName(loader, 12);
		readofs := LEtoN(loader.ReadDword);
		if j <> 0 then i := LEtoN(dword((loader.readp + 12)^)) else i := listofs;
		if (i > readofs) and (i <= loader.fullFileSize) then
			DispatchAndKeep(outputdir, filename, loader.PtrAt(readofs), i - readofs, game)
		else
			decomp_logger('[!] file end oob');
	end;

	result := TRUE;
end;

