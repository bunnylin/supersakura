unit midi_instrument_matcher;

{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

{$mode objfpc}
{$codepage UTF8}
{$unitpath ../lib/moonlibs}
{$librarypath ../lib/moonlibs}

interface

{$ifdef separate_definition}
type TInstrument_FMOPL = record
	op : array[1..2] of record
		freqmulfactor, keyscalelevel, attenuation : byte;
		attackrate, decayrate, sustainlevel, releaserate : byte;
		muted, tremolo, vibrato, egt, envscale : boolean;
	end;
	feedback, algorithm : byte;
end;

type TInstrument_FMOPN = record
	op : array[1..4] of record
		detune, freqmulfactor, attenuation, keyscalelevel : byte;
		attackrate, decayrate, sustainrate, sustainlevel, releaserate : byte;
		muted, AMSmask : boolean;
	end;
	feedback, algorithm : byte;
end;

type TInstrument_SSG = record
	attackrate, decayrate, sustainrate, sustainlevel, releaserate, startlevel : byte;
	noisefreq : byte;
	enable_tone, enable_noise : boolean;
end;
{$endif}

type mim_TInstrumentType = (MIM_TIT_OPN, MIM_TIT_OPL, MIM_TIT_SSG);

// This contains all three types of instrument definition crammed together. Looks messier, but takes less code to use.
type mim_TOperator = record
	// OPN only
	detune : byte;
	AMSmask : boolean;
	// OPN + OPL
	freqmulfactor, attenuation, keyscalelevel : byte;
	muted : boolean;
	// OPN + SSG
	sustainrate : byte;
	// OPN + OPL + SSG
	attackrate, decayrate, sustainlevel, releaserate : byte;
	// OPL only
	tremolo, vibrato, egt, envscale : boolean;
	// SSG only
	startlevel, noisefreq : byte;
end;

type mim_TInstrument = record
	op : array[1..4] of mim_TOperator;
	enable_tone, enable_noise : boolean; // primarily SSG, but theoretically available for FM too
	feedback, algorithm : byte;
	transpose, base_velocity : shortint;
	max_sustain_duration_csec : byte;

	instrument_type : mim_TInstrumentType;
	// This is where the matched instrument number is saved after calling mim_MatchInstrument.
	midimatch, mimmatch : longint;
end;

function mim_IsCarrier(const instrument : mim_TInstrument; opnum : byte) : boolean;
procedure mim_MatchInstrument(var data : mim_TInstrument);
function mim_CheckDynamicPercussion(
	var instrument : mim_TInstrument; notenumber : byte; duration : longint; ticks_per_minute : dword) : byte;

var mim_logger : procedure (const logtext : UTF8string);

// Expression lookup table:
// sqrt(16129 / (10 ^ ((15 - SSG volume) * 3 * 0.05)))
const mim_SSGVolumeToExpression : array[0..15] of byte =
	(0, 11, 13, 16, 19, 23, 27, 32, 38, 45, 54, 64, 76, 90, 107, 127);

// ------------------------------------------------------------------

implementation

uses sysutils, math, mccommon;

const DEFAULT_VELOCITY = $64;

var mimtable : array of mim_TInstrument;
	tableindex : record
		oplfirst, opllast : dword;
		opn4first, opn4last, opn3first, opn3last, opn2first, opn2last, opn1first, opn1last : dword;
	end;
{$include mimtable.inc}

var recent_bad_mims : array[0..3] of longint = (-1, -1, -1, -1);
var recent_bad_mim_score : array[0..3] of longint;

function mim_IsCarrier(const instrument : mim_TInstrument; opnum : byte) : boolean;
begin
	mim_IsCarrier := TRUE;
	with instrument do case instrument_type of
		MIM_TIT_SSG: ;
		MIM_TIT_OPL: mim_IsCarrier := (algorithm <> 0) or (opnum = 2);
		MIM_TIT_OPN:
		case opnum of
			1: mim_IsCarrier := (algorithm in [7]);
			2: mim_IsCarrier := (algorithm in [4..7]);
			3: mim_IsCarrier := (algorithm in [5..7]);
			4: ;
			else raise Exception.Create('bad opnum');
		end;
	end;
end;

procedure AutoTranspose(var instrument : mim_TInstrument; ops : byte);
// Frequency multiplication factor on modulators affects timbre, but on carriers it simply transposes the instrument's
// pitch. To standardise instrument translation, it's good to reduce all freqmulfactors toward 1, saving the lowest
// factor as the instrument's official transposing amount.
// Freqmulfactor 0 is special in that it halves the frequency, rather than multiplies by 0.
var i, lowestfmf : byte;
begin
	Assert(ops in [1..4]);
	lowestfmf := 99;
	instrument.transpose := 0;
	for i := 1 to ops do with instrument.op[i] do
		if (not muted) and (freqmulfactor in [0, 1, 2, 4, 8]) then
			if freqmulfactor < lowestfmf then lowestfmf := freqmulfactor;

	if lowestfmf in [2, 4, 8] then begin
		for i := 1 to ops do with instrument.op[i] do
			if freqmulfactor mod lowestfmf <> 0 then exit; // if transpose would result in fractions, give up
		for i := 1 to ops do with instrument.op[i] do
			freqmulfactor := freqmulfactor div lowestfmf;
	end
	else if lowestfmf = 0 then
		for i := 1 to ops do with instrument.op[i] do
			if freqmulfactor = 0 then freqmulfactor := 1 else freqmulfactor := freqmulfactor shl 1;

	with instrument do case lowestfmf of
		8: transpose := 36; // +3 octaves
		4: transpose := 24; // +2 octaves
		2: transpose := 12; // +1 octave
		0: transpose := -12; // -1 octave
	end;
end;

procedure AutoVelocity(var instrument : mim_TInstrument; ops : byte);
// Carrier attenuation can be applied as a MIDI velocity tweak. Although most instruments have 0 attenuation in at
// least one carrier, some are deliberately softened by attenuating all carriers. Such attenuation can be reduced
// toward 0 here to improve instrument standardisation.
var i, lowestattenuation : byte;
	attlist : array[1..4] of boolean;
begin
	Assert(ops in [1..4]);
	attlist[1] := FALSE; // silence a compiler warning
	lowestattenuation := 99;
	for i := 1 to ops do with instrument.op[i] do begin
		attlist[i] := FALSE;
		if (not muted) and (mim_IsCarrier(instrument, i)) then begin
			if attenuation < lowestattenuation then lowestattenuation := attenuation;
			attlist[i] := TRUE;
		end;
	end;

	// Each step of FM attenuation is -0.75 dB. (Each 6.0206 dB halves amplitude.)
	// MIDI volume = sqrt(16129 / (10 ^ (FM attenuation * 0.75 * 0.05)))
	if (lowestattenuation > 0) and (lowestattenuation < 99) then begin
		instrument.base_velocity := round(
			sqrt(16129 / power(10, (lowestattenuation * 0.75 * 0.05)))
			* instrument.base_velocity / 127
			);
		for i := 1 to ops do
			if attlist[i] then
				with instrument.op[i] do dec(attenuation, lowestattenuation);
	end;
end;

procedure Dump(const data : mim_TInstrument);
var i : byte;
	c : char;
begin
	case data.instrument_type of
		MIM_TIT_OPL:
		begin
			for i := 1 to 2 do with data.op[i] do begin
				if muted then
					mim_logger(strcat('op %: ---muted---', [i]))
				else begin
					c := 'M'; if mim_IsCarrier(data, i) then c := 'C';
					mim_logger(strcat('op % %:', [i, c]));
					mim_logger(strcat(#9'muted %, tremolo %, vibrato %, egt %, envscale %',
						[muted, tremolo, vibrato, egt, envscale]));
					mim_logger(strcat(#9'freqmulfactor %, keyscalelevel %, attenuation %',
						[freqmulfactor, keyscalelevel, attenuation]));
					mim_logger(strcat(#9'AR %, DR %, SL %, RR %',
						[attackrate, decayrate, sustainlevel, releaserate]));
				end;
			end;
			mim_logger(strcat('feedback %, algorithm % :: transpose % :: basevelo %',
				[data.feedback, data.algorithm, data.transpose, data.base_velocity]));
		end;

		MIM_TIT_OPN:
		begin
			for i := 1 to 4 do with data.op[i] do begin
				if muted then
					mim_logger(strcat('op %: ---muted---', [i]))
				else begin
					c := 'M'; if mim_IsCarrier(data, i) then c := 'C';
					mim_logger(strcat('op % %:', [i, c]));
					mim_logger(strcat(#9'detune %, freqmulfactor %, attenuation %, keyscalelevel %',
						[detune, freqmulfactor, attenuation, keyscalelevel]));
					mim_logger(strcat(#9'AR %, DR %, SR %, SL %, RR % :: AMSmask %',
						[attackrate, decayrate, sustainrate, sustainlevel, releaserate, AMSmask]));
				end;
			end;
			mim_logger(strcat('feedback %, algorithm % :: transpose % :: basevelo %',
				[data.feedback, data.algorithm, data.transpose, data.base_velocity]));
		end;

		MIM_TIT_SSG:
		with data.op[1] do begin
			mim_logger(strcat('enable_tone %, enable_noise %, noisefreq % :: transpose % :: basevelo %',
				[data.enable_tone, data.enable_noise, noisefreq, data.transpose, data.base_velocity]));
			mim_logger(strcat(#9'AR %, DR %, SR %, SL %, RR %, AL %',
				[attackrate, decayrate, sustainrate, sustainlevel, releaserate, startlevel]));
		end;

		else raise Exception.Create('Bad instrument type');
	end;
end;

function Diff(const data1, data2 : mim_TInstrument) : longint;
var i, j : byte;
begin
	if data1.instrument_type <> data2.instrument_type then Diff := high(Diff) else
	case data1.instrument_type of
		MIM_TIT_OPL:
		begin
			if data1.algorithm <> data2.algorithm then begin
				Diff := high(Diff) - 1;
				exit;
			end;
			Diff := abs(data1.feedback - data2.feedback) shl 3;
			for i := 1 to 2 do begin
				if data1.op[i].muted <> data2.op[i].muted then
					inc(Diff, 256)
				else if NOT data1.op[i].muted then begin
					if data1.op[i].tremolo <> data2.op[i].tremolo then inc(Diff, 32);
					if data1.op[i].vibrato <> data2.op[i].vibrato then inc(Diff, 32);
					if data1.op[i].egt <> data2.op[i].egt then inc(Diff, 8);
					if data1.op[i].envscale <> data2.op[i].envscale then inc(Diff, 8);
					inc(Diff, abs(data1.op[i].freqmulfactor - data2.op[i].freqmulfactor) shl 2);
					inc(Diff, abs(data1.op[i].keyscalelevel - data2.op[i].keyscalelevel) shl 3);
					inc(Diff, abs(data1.op[i].attenuation - data2.op[i].attenuation) shl 1);
					inc(Diff, abs(data1.op[i].attackrate - data2.op[i].attackrate) shl 4);
					inc(Diff, abs(data1.op[i].decayrate - data2.op[i].decayrate) shl 3);
					inc(Diff, abs(data1.op[i].sustainlevel - data2.op[i].sustainlevel) shl 2);
					inc(Diff, abs(data1.op[i].releaserate - data2.op[i].releaserate) shl 2);
				end;
			end;
		end;

		MIM_TIT_OPN:
		begin
			if data1.algorithm <> data2.algorithm then begin
				Diff := high(Diff) - 1;
				exit;
			end;
			j := abs(data1.feedback - data2.feedback) shl 3;
			Diff := j * j;
			for i := 1 to 4 do begin
				if data1.op[i].muted then break;
				if data1.op[i].AMSmask <> data2.op[i].AMSmask then inc(Diff, 256);
				if mim_IsCarrier(data1, i) then
					inc(Diff, abs(data1.op[i].detune - data2.op[i].detune))
				else
					inc(Diff, abs(data1.op[i].detune - data2.op[i].detune) shl 3);
				if data1.op[i].freqmulfactor <> data2.op[i].freqmulfactor then begin
					inc(Diff, 256);
					if (data1.op[i].freqmulfactor shr 1 <> data2.op[i].freqmulfactor)
					and (data1.op[i].freqmulfactor <> data2.op[i].freqmulfactor shr 1)
					then inc(Diff, 768);
				end;
				j := abs(data1.op[i].keyscalelevel - data2.op[i].keyscalelevel) shl 2;
				inc(Diff, longint(j * j));
				j := abs(data1.op[i].attenuation - data2.op[i].attenuation);
				inc(Diff, longint(j * j));
				j := abs(data1.op[i].attackrate - data2.op[i].attackrate) shl 1;
				inc(Diff, longint(j * j));
				j := abs(data1.op[i].decayrate - data2.op[i].decayrate) shl 1;
				inc(Diff, longint(j * j));
				j := abs(data1.op[i].sustainrate - data2.op[i].sustainrate) shl 1;
				inc(Diff, longint(j * j));
				j := abs(data1.op[i].sustainlevel - data2.op[i].sustainlevel) shl 1;
				inc(Diff, longint(j * j));
				j := abs(data1.op[i].releaserate - data2.op[i].releaserate) shl 1;
				inc(Diff, longint(j * j));
			end;
		end;

		else raise Exception.Create('Bad instrument type');
	end;
end;

function TrySortCarriers(var data : mim_TInstrument; op1, op2 : byte) : boolean;
// Compares the given operator numbers using a somewhat consistent comparison metric, and swaps the operators to be in
// line with the sort. This should only be used on equivalent operators that can be safely swapped without altering the
// sound texture. This is used to keep similar operators always on the same operator number, allowing more standardised
// comparisons between instruments.
// Returns true if swapped the operators, false if not.
var swapper : mim_TOperator;
	sortscore : longint;
begin
	result := FALSE;
	Assert(op1 in [1..4]);
	Assert(op2 in [1..4]);
	Assert(data.op[op1].muted = FALSE);
	Assert(data.op[op2].muted = FALSE);
	Assert(op1 <> op2);

	sortscore := data.op[op1].attenuation - data.op[op2].attenuation;
	inc(sortscore, (data.op[op1].freqmulfactor - data.op[op2].freqmulfactor) * 4);
	inc(sortscore, data.op[op1].attackrate - data.op[op2].attackrate);
	inc(sortscore, data.op[op1].decayrate - data.op[op2].decayrate);
	inc(sortscore, data.op[op1].sustainlevel - data.op[op2].sustainlevel);

	if sortscore > 0 then begin
		swapper := data.op[op2];
		data.op[op2] := data.op[op1];
		data.op[op1] := swapper;
		result := TRUE;
	end;
end;

procedure mim_MatchInstrument(var data : mim_TInstrument);
// Finds a reasonable MIDI instrument match for the given FM/SSG instrument. Saves the result in the instrument's
// midimatch field, and the match's closest lookup table index in its mimmatch field.
var tempdata : mim_TInstrument;
	swapper : mim_TOperator;
	i, j, startindex, lastindex : dword;
	l, diffscore, bestscore : longint;

	procedure LogPoorMatch;
	var m : byte;
	begin
		mim_logger(strcat('## Poor FM instrument match, score %, tentative midi %/mim % [%..%]',
			[bestscore, data.midimatch, data.mimmatch, startindex, lastindex - 1]));
		for m := high(recent_bad_mims) downto 0 do
			if (recent_bad_mims[m] = data.mimmatch) and (recent_bad_mim_score[m] = bestscore) then exit;
		for m := high(recent_bad_mims) downto 1 do begin
			recent_bad_mims[m] := recent_bad_mims[m - 1];
			recent_bad_mim_score[m] := recent_bad_mim_score[m - 1];
		end;
		recent_bad_mims[0] := data.mimmatch;
		recent_bad_mim_score[0] := bestscore;
		Dump(data);
		Dump(tempdata);
	end;

begin
	data.midimatch := 0;
	data.mimmatch := -1;
	data.transpose := 0;
	data.base_velocity := DEFAULT_VELOCITY;
	bestscore := high(bestscore);

	if length(mimtable) = 0 then SetupInstruments;

	tempdata := data;
	case data.instrument_type of
		MIM_TIT_SSG:
		with data do begin
			if NOT enable_noise then begin
				// All melodic SSG are mapped to this. Sounds fine as long as the volume envelope is handled.
				dec(base_velocity, 13);
				transpose := 12;
				midimatch := 71; // Clarinet, supposedly the most square-wavey instrument
			end
			else if (NOT enable_tone) and ((op[1].sustainrate = 0) or (op[1].sustainlevel = 0)) then begin
				// Sustained flat noise.
				dec(base_velocity, 22);
				transpose := 30 - op[1].noisefreq;
				midimatch := 126; // Applause, the closest thing General MIDI has...
				mimmatch := -2;
			end
			else begin
				// All other noisy SSG use a dynamic percussion mapper.
				midimatch := $FF;
				mimmatch := -3;
			end;
			exit;
		end;

		MIM_TIT_OPL: begin
			if (data.algorithm = 1) and (data.feedback = 0) then TrySortCarriers(tempdata, 1, 2);
			AutoVelocity(tempdata, 2);
			startindex := tableindex.oplfirst; lastindex := tableindex.opllast;
		end;

		MIM_TIT_OPN:
		begin
			j := 0;
			for i := 3 downto 0 do begin
				if data.op[i + 1].attenuation >= 64 then tempdata.op[i + 1].muted := TRUE;
				if tempdata.op[i + 1].muted then j := j or dword(1 shl i);
			end;

			startindex := 0; lastindex := 0;
			if j = 0 then begin
				// Nothing's muted, do full 4-op matching.
				startindex := tableindex.opn4first; lastindex := tableindex.opn4last;
				// But try to standardise operator order.
				case data.algorithm of
					7: begin
						TrySortCarriers(tempdata, 3, 4);
						if TrySortCarriers(tempdata, 2, 3) then TrySortCarriers(tempdata, 3, 4);
						if data.feedback = 0 then
							if TrySortCarriers(tempdata, 1, 2) then
								if TrySortCarriers(tempdata, 2, 3) then TrySortCarriers(tempdata, 3, 4);
					end;
					6: TrySortCarriers(tempdata, 3, 4);
					5: begin
						TrySortCarriers(tempdata, 3, 4);
						if TrySortCarriers(tempdata, 2, 3) then TrySortCarriers(tempdata, 3, 4);
					end;
					4: if data.feedback = 0 then if TrySortCarriers(tempdata, 2, 4) then begin
						swapper := tempdata.op[1];
						tempdata.op[1] := tempdata.op[3];
						tempdata.op[3] := swapper;
					end;
					1: if data.feedback = 0 then TrySortCarriers(tempdata, 1, 2);
				end;
			end
			else begin
				// At least one operator is muted. Rearrange the operators to a standard layout for easier matching.
				// If possible, sort all unmuted to 1 and upward, muted at 4 and downward; use highest equivalent
				// algorithm number; remember feedback only applies to operator 1.

				case j of
					// ops 2+3+4 unmuted
					1: if data.algorithm in [5..7] then begin // 2+3+4 -> 2+3+1
						tempdata.op[1] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 7;
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
						TrySortCarriers(tempdata, 2, 3);
						if TrySortCarriers(tempdata, 1, 2) then TrySortCarriers(tempdata, 2, 3);
					end
					else if data.algorithm = 4 then begin // (3*4)+2 -> (1*2)+3
						tempdata.op[1] := data.op[3]; tempdata.feedback := 0; tempdata.algorithm := 6;
						tempdata.op[2] := data.op[4]; tempdata.op[3] := data.op[2]; tempdata.op[4].muted := TRUE;
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
					end
					else if data.algorithm = 3 then begin // (2+3)*4 -> (1+3)*4
						tempdata.op[1] := data.op[2]; tempdata.op[2].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 2;
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
						TrySortCarriers(tempdata, 1, 3);
					end
					else begin // 2*3*4 -> 1*2*4
						tempdata.op[1] := data.op[2]; tempdata.feedback := 0; tempdata.algorithm := 3;
						tempdata.op[2] := data.op[3]; tempdata.op[3].muted := TRUE;
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
					end;
					// ops 1+3+4 unmuted
					2: if data.algorithm = 7 then begin // 1+3+4 -> 1+3+2
						tempdata.op[2] := data.op[4]; tempdata.op[4].muted := TRUE;
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
						TrySortCarriers(tempdata, 2, 3);
						if tempdata.feedback = 0 then
							if TrySortCarriers(tempdata, 1, 2) then TrySortCarriers(tempdata, 2, 3);
					end
					else if data.algorithm = 6 then begin // 3+4, op 1 ignored -> 1+2
						tempdata.op[2] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.algorithm := 7;
						tempdata.op[1] := data.op[3]; tempdata.op[3].muted := TRUE; tempdata.feedback := 0;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
						TrySortCarriers(tempdata, 1, 2);
					end
					else if data.algorithm = 5 then begin // (1*3)+(1*4) -> (1*3)+(1*2)
						tempdata.op[2] := data.op[4]; tempdata.op[4].muted := TRUE;
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
						TrySortCarriers(tempdata, 2, 3);
					end
					else if data.algorithm in [0,3,4] then begin // (3*4), op 1 ignored -> (1*2)
						tempdata.op[2] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.algorithm := 6;
						tempdata.op[1] := data.op[3]; tempdata.op[3].muted := TRUE; tempdata.feedback := 0;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
					end
					else if data.algorithm = 2 then begin // (1+3)*4
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
						if tempdata.feedback = 0 then TrySortCarriers(tempdata, 1, 3);
					end
					else begin // (1*3*4) -> (1*2*4)
						tempdata.op[2] := data.op[3]; tempdata.op[3].muted := TRUE; tempdata.algorithm := 3;
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
					end;
					// ops 3+4 unmuted
					3: if data.algorithm in [5..7] then begin // 3+4 -> 1+2
						tempdata.op[2] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.algorithm := 7;
						tempdata.op[1] := data.op[3]; tempdata.op[3].muted := TRUE; tempdata.feedback := 0;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
						TrySortCarriers(tempdata, 1, 2);
					end
					else begin // 3*4 -> 1*2
						tempdata.op[2] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.algorithm := 6;
						tempdata.op[1] := data.op[3]; tempdata.op[3].muted := TRUE; tempdata.feedback := 0;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
					end;
					// ops 1+2+4 unmuted
					4: if data.algorithm in [5..7] then begin // 1+2+4, (1*2)+4, (1*2)+(1*4) -> 1+2+3, (1*2)+3, (1*2)+(1*3)
						tempdata.op[3] := data.op[4]; tempdata.op[4].muted := TRUE;
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
						if data.algorithm <> 6 then TrySortCarriers(tempdata, 2, 3);
						if (data.algorithm = 7) and (tempdata.feedback = 0) then
							if TrySortCarriers(tempdata, 1, 2) then TrySortCarriers(tempdata, 2, 3);
					end
					else if data.algorithm = 4 then begin // (1*2)+4 -> (1*2)+3
						tempdata.op[3] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.algorithm := 6;
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
					end
					else if data.algorithm = 3 then begin // (1*2*4)
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
					end
					else if data.algorithm = 2 then begin // (1*4), op 2 ignored -> (1*2)
						tempdata.op[2] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.algorithm := 6;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
					end
					else begin // op 4 alone, ops 1 and 2 ignored
						tempdata.op[1] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 7;
						startindex := tableindex.opn1first; lastindex := tableindex.opn1last;
					end;
					// ops 2+4 unmuted
					5: if data.algorithm in [4..7] then begin // 2+4 -> 2+1
						tempdata.op[1] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 7;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
					end
					else begin // op 4 alone, op 2 ignored
						tempdata.op[1] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 7;
						startindex := tableindex.opn1first; lastindex := tableindex.opn1last;
					end;
					// ops 1+4 unmuted
					6: if data.algorithm = 7 then begin // 1+4 -> 1+2
						tempdata.op[2] := data.op[4]; tempdata.op[4].muted := TRUE;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
						if tempdata.feedback = 0 then TrySortCarriers(tempdata, 1, 2);
					end
					else if data.algorithm in [2,5] then begin // (1*4) -> (1*2)
						tempdata.op[2] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.algorithm := 6;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
					end
					else begin // op 4 alone, op 1 ignored
						tempdata.op[1] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 7;
						startindex := tableindex.opn1first; lastindex := tableindex.opn1last;
					end;
					// op 4 unmuted
					7: begin
						tempdata.op[1] := data.op[4]; tempdata.op[4].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 7;
						startindex := tableindex.opn1first; lastindex := tableindex.opn1last;
					end;
					// ops 1+2+3 unmuted
					8: if data.algorithm in [5..7] then begin // 1+2+3, (1*2)+3, (1*2)+(1*3)
						startindex := tableindex.opn3first; lastindex := tableindex.opn3last;
						if data.algorithm <> 6 then TrySortCarriers(tempdata, 2, 3);
						if (data.algorithm = 7) and (tempdata.feedback = 0) then
							if TrySortCarriers(tempdata, 1, 2) then TrySortCarriers(tempdata, 2, 3);
					end
					else if data.algorithm = 4 then begin // (1*2), op 3 ignored
						tempdata.op[3].muted := TRUE; tempdata.algorithm := 6;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
					end;
					// ops 2+3 unmuted
					9: if data.algorithm in [5..7] then begin // 2+3 -> 2+1
						tempdata.op[1] := data.op[3]; tempdata.op[3].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 7;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
						if tempdata.feedback = 0 then TrySortCarriers(tempdata, 1, 2);
					end;
					// ops 1+3 unmuted
					10: if data.algorithm = 7 then begin // 1+3 -> 1+2
						tempdata.op[2] := data.op[3]; tempdata.op[3].muted := TRUE;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
						if tempdata.feedback = 0 then TrySortCarriers(tempdata, 1, 2);
					end
					else if data.algorithm = 5 then begin // (1*3) -> (1*2)
						tempdata.op[2] := data.op[3]; tempdata.op[3].muted := TRUE; tempdata.algorithm := 6;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
					end
					else if data.algorithm = 6 then begin // op 3 alone, op 1 ignored
						tempdata.op[1] := data.op[3]; tempdata.op[3].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 7;
						startindex := tableindex.opn1first; lastindex := tableindex.opn1last;
					end;
					// op 3 unmuted
					11: if data.algorithm in [5..7] then begin
						tempdata.op[1] := data.op[3]; tempdata.op[3].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 7;
						startindex := tableindex.opn1first; lastindex := tableindex.opn1last;
					end;
					// ops 1+2 unmuted
					12: if data.algorithm = 7 then begin // 1+2
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
						if tempdata.feedback = 0 then TrySortCarriers(tempdata, 1, 2);
					end
					else if data.algorithm in [4..6] then begin // 1*2
						tempdata.algorithm := 6;
						startindex := tableindex.opn2first; lastindex := tableindex.opn2last;
					end;
					// op 2 unmuted
					13: if data.algorithm in [4..7] then begin
						tempdata.op[1] := data.op[2]; tempdata.op[2].muted := TRUE; tempdata.feedback := 0;
						tempdata.algorithm := 7;
						startindex := tableindex.opn1first; lastindex := tableindex.opn1last;
					end;
					// op 1 unmuted
					14: if data.algorithm = 7 then begin
						startindex := tableindex.opn1first; lastindex := tableindex.opn1last;
					end;
					// all ops muted! Unmask op 1 but fix velocity at 0.
					15: begin
						tempdata.op[1].muted := FALSE; tempdata.base_velocity := -128;
						startindex := tableindex.opn1first; lastindex := tableindex.opn1last;
					end;
				end;

				if lastindex = 0 then with tempdata do begin
					// All operators feeding to output are muted, even though some modulators may not be!
					// Treat as a single-op but with velocity a t0.
					op[1].muted := FALSE; op[2].muted := TRUE; op[3].muted := TRUE; op[4].muted := TRUE;
					base_velocity := -128;
					startindex := tableindex.opn1first; lastindex := tableindex.opn1last;
				end;
			end;

			case j of
				0: begin AutoTranspose(tempdata, 4); AutoVelocity(tempdata, 4); end;
				1, 2, 4, 8: begin AutoTranspose(tempdata, 4); AutoVelocity(tempdata, 4); end;
				3, 5, 6, 9, 10, 12: begin AutoTranspose(tempdata, 2); AutoVelocity(tempdata, 2); end;
				7, 11, 13, 14: begin AutoTranspose(tempdata, 1); AutoVelocity(tempdata, 1); end;
			end;
		end;

		else raise Exception.Create('Bad instrument type');
	end;

	for i := startindex to lastindex - 1 do begin
		diffscore := Diff(tempdata, mimtable[i]);
		if diffscore < bestscore then begin
			data.mimmatch := i;
			bestscore := diffscore;
			if diffscore = 0 then break;
		end;
	end;
	data.midimatch := mimtable[data.mimmatch].midimatch;
	inc(tempdata.transpose, mimtable[data.mimmatch].transpose); // already auto-transposed, apply possible manual tweak
	l := tempdata.base_velocity + mimtable[data.mimmatch].base_velocity;
	if l < 0 then l := 0 else if l > 127 then l := 127;
	tempdata.base_velocity := l;

	if ((data.instrument_type = MIM_TIT_OPL) and (bestscore > 32))
	or ((data.instrument_type = MIM_TIT_OPN) and (
		((bestscore > 15) and (tempdata.op[2].muted)) or
		((bestscore > 99) and (tempdata.op[3].muted)) or
		(bestscore > 149))
	)
	or (data.midimatch = $F2)
	then LogPoorMatch;

	data.transpose := tempdata.transpose;
	data.base_velocity := tempdata.base_velocity;
end;

function mim_CheckDynamicPercussion(
	var instrument : mim_TInstrument; notenumber : byte; duration : longint; ticks_per_minute : dword) : byte;
var i, diffscore, bestscore : dword;
	j : longint;
begin
	// Convert OPN note duration from ticks to centiseconds; leave OPL duration unconverted but cap at 96 ticks.
	if instrument.instrument_type <> MIM_TIT_OPL then
		duration := dword(duration) * 6000 div ticks_per_minute
	else
		if duration > 96 then duration := 96;

	while TRUE do with instrument do begin
		result := midimatch and $7F;
		case midimatch of
			$80:
			case duration of
				16..19:
				begin
					result := 44;
					if NOT (notenumber in [91..94]) then mim_logger('# magic cymbal (ph) weird note ' + strdec(notenumber));
				end;
				20..48:
				begin
					result := 46;
					if NOT (notenumber in [91..94]) then mim_logger('# magic cymbal (oh) weird note ' + strdec(notenumber));
				end;
				49..72:
				begin
					result := 55;
					if NOT (notenumber in [90..94]) then mim_logger('# magic cymbal (sc) weird note ' + strdec(notenumber));
				end;
				73..255:
				begin
					result := 49;
					if NOT (notenumber in [84..95]) then mim_logger('# magic cymbal (cc) weird note ' + strdec(notenumber));
				end;
				else mim_logger('# magic cymbal weird duration ' + strdec(duration));
			end;

			$81:
			case duration of
				8..16:
				begin
					result := 69;
					if NOT (notenumber in [95]) then mim_logger('# magic cymbal 2 (m) weird note ' + strdec(notenumber));
				end;
				24..36:
				begin
					result := 46;
					if NOT (notenumber in [94]) then mim_logger('# magic cymbal 2 (oh) weird note ' + strdec(notenumber));
				end;
				48:
				begin
					result := 55;
					if NOT (notenumber in [101]) then mim_logger('# magic cymbal 2 (sc) weird note ' + strdec(notenumber));
				end;
				49..255:
				begin
					result := 57;
					if NOT (notenumber in [95]) then mim_logger('# magic cymbal 2 (cc) weird note ' + strdec(notenumber));
				end;
				else mim_logger('# magic cymbal 2 weird duration ' + strdec(duration));
			end;

			// Low conga/high open conga.
			$82: if notenumber >= 55 then result := 63 else result := 64;

			$F2:
			begin
				// Although hi-hats particularly may have a long nominal duration, their volume envelope may cut the
				// note short. This is important when evaluating whether a hat is open or closed.
				// The decay rate applies until the 4-bit sustain level is reached (where 0 is max volume, 15 silence,
				// -3dB per step). The sustain rate applies from there. A weighed average gives a combined decay rate.
				//
				// We'll consider a note's useful duration over when it has fallen below -18 dB (12.5% amplitude,
				// sustain level 6). The decay rate is -3dB in:
				//   562.5 centiseconds * 0.5 ^ (rate / 2)

				max_sustain_duration_csec := 0;
				for i := 1 to 4 do begin
					if (NOT instrument.op[i].muted) and (mim_IsCarrier(instrument, i)) then begin
						// Sufficiently soft operators may as well be ignored.
						if op[i].attenuation >= 63 then continue;
						// Special cases where the envelope is stuck at full, infinite duration.
						if (op[i].sustainrate = 0) or ((op[i].decayrate = 0) and (op[i].sustainlevel <> 0)) then begin
							max_sustain_duration_csec := 255;
							break;
						end;

						// Calculate how long the envelope takes until -18 dB, in centiseconds.
						// If sustain level is beyond -18 dB, the decay phase covers the entire useful duration.
						if op[i].sustainlevel >= 6 then
							j := round(6 * 562.5 * power(0.5, op[i].decayrate / 2))
						else begin
							// Useful duration is the entire decay phase plus some part of the sustain phase.
							j := 0;
							if op[i].sustainlevel <> 0 then
								j := round(op[i].sustainlevel * 562.5 * power(0.5, op[i].decayrate / 2));
							j := j + round((6 - op[i].sustainlevel) * 562.5 * power(0.5, op[i].sustainrate / 2));
						end;

						if j > max_sustain_duration_csec then begin
							if j >= 255 then begin
								max_sustain_duration_csec := 255;
								break;
							end;
							max_sustain_duration_csec := j;
						end;
					end;
				end;
				midimatch := $F3;
				continue; // while true loop, re-enters
			end;

			$F3:
			begin
				mim_logger(strcat('# magic fm hat: duration %, note %, maxdura % csec',
					[duration, notenumber, max_sustain_duration_csec]));
				if duration > max_sustain_duration_csec then duration := max_sustain_duration_csec;

				case duration of
					0..8: begin
						result := 42; // closed hi-hat
						base_velocity := DEFAULT_VELOCITY - 40;
					end;
					else begin
						result := 46; // open hi-hat
						base_velocity := DEFAULT_VELOCITY - 40;
					end;
				end;
			end;

			$F4:
			begin
				mim_logger(strcat('# magic fm tom: duration %, note %',
					[duration, notenumber]));
				//if duration > max_sustain_duration_csec then duration := max_sustain_duration_csec;

				case notenumber of
					0..59: begin
						result := 36; // bass drum 1
						base_velocity := DEFAULT_VELOCITY + 1;
					end;
					else begin
						result := 50; // high tom
						base_velocity := DEFAULT_VELOCITY;
					end;
				end;
			end;

			// Magic SSG percussion.
			$FF:
			begin
				bestscore := high(bestscore);
				for i := high(ssgmimtable) downto 0 do begin
					if ssgmimtable[i].enable_tone <> enable_tone then continue;
					j := abs(ssgmimtable[i].duration - duration) shr 1;
					diffscore := j * j;
					j := abs(ssgmimtable[i].notenumber - notenumber) shr 2;
					inc(diffscore, dword(j * j));
					j := ssgmimtable[i].noisefreq - op[1].noisefreq;
					inc(diffscore, dword(j * j));
					if diffscore >= bestscore then continue; // obviously no good, move on
					j := ssgmimtable[i].startlevel - op[1].startlevel;
					inc(diffscore, dword(j * j));
					j := ssgmimtable[i].attackrate - op[1].attackrate;
					inc(diffscore, dword(j * j));
					j := ssgmimtable[i].decayrate - op[1].decayrate;
					inc(diffscore, dword(j * j));
					j := ssgmimtable[i].sustainlevel - op[1].sustainlevel;
					inc(diffscore, dword(j * j));
					j := ssgmimtable[i].sustainrate - op[1].sustainrate;
					inc(diffscore, dword(j * j));
					j := ssgmimtable[i].releaserate - op[1].releaserate;
					inc(diffscore, dword(j * j));
					if diffscore < bestscore then begin
						bestscore := diffscore;
						result := ssgmimtable[i].midimatch;
						base_velocity := DEFAULT_VELOCITY + ssgmimtable[i].base_velocity;
					end;
				end;
				if bestscore > 1 then with op[1] do
					mim_logger(strcat('## Poor SSG hit: duration %, note %, tonal %, freq %; AL %, AR %, DR %, SL %, SR %, RR % :: midi % -> score &',
					[duration, notenumber, enable_tone, noisefreq, startlevel, attackrate, decayrate, sustainlevel, sustainrate, releaserate, result, longint(bestscore)]));
			end;
		end;

		break;
	end; // while true
end;

initialization
finalization
end.
