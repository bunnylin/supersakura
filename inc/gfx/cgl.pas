{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_CGL(const src : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a Fournine CGL graphic. Adds its metadata to graphicFiles[] and saves the bitmap
// in outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	buffy : pointer = NIL;

	procedure _Load;
	const constlit : array[0..15] of word = (1, 2, $40, $80, 0,0,0,0, 0,0,0,0, $FFFE, $FFFD, $FFBF, $FF7F);
	var writep, endp : pointer;
		rowp : ^byte;
		buf : array of byte = NIL;
		p1, p2, p3, d : dword;
		x, y, w, reps : word;
		cmd, plane : byte;
		endearly : boolean = FALSE;
	begin
		with tempimage do begin
			setlength(buf, length(bitmap) + 16); // leeway for rogue repeats
			endp := @buf[0] + length(bitmap);
			p1 := length(bitmap) shr 2;
			p2 := p1 + p1;
			p3 := p1 + p2;

			try
				for plane := 0 to 3 do begin
					writep := @buf[plane * p1];
					while (writep < endp) and (src.readp < src.endp) do begin
						cmd := src.ReadByte;
						case cmd of
							0: break; // end of plane
							$20:
							begin
								byte(writep^) := 0; inc(writep);
							end;
							$21..$3F: // literal run
							begin
								reps := cmd and $1F;
								move(src.readp^, writep^, reps);
								inc(src.readp, reps);
								inc(writep, reps);
							end;
							$40..$4F: // single byte repeat
							begin
								reps := cmd and $F;
								if reps = 0 then begin
									reps := src.ReadByte;
									if reps = 0 then reps := LEtoN(src.ReadWord);
								end;
								if writep + reps > endp then reps := endp - writep; // safety
								FillByte(writep^, reps, src.ReadByte);
								inc(writep, reps);
							end;
							$50..$5F: // output run of half-literals, constant 2nd nibble
							begin
								reps := cmd and 3;
								if reps = 0 then reps := src.ReadByte;
								d := reps shr 1;
								rowp := @w;
								if reps > 1 then for d := (reps shr 1) - 1 downto 0 do begin
									rowp^ := byte(src.readp^) and $F0;
									(rowp + 1)^ := (byte(src.readp^) and $0F) shl 4;
									inc(src.readp);
									if cmd and 4 <> 0 then w := w or $0F0F;
									if cmd and 8 <> 0 then w := RorWord(w, 4);
									word(writep^) := w;
									inc(writep, 2);
								end;
								if reps and 1 <> 0 then begin
									byte(writep^) := src.ReadByte and $F0;
									if cmd and 4 <> 0 then byte(writep^) := byte(writep^) or $0F;
									if cmd and 8 <> 0 then byte(writep^) := RorByte(byte(writep^), 4);
									inc(writep);
								end;
							end;
							$60: // copy 1 byte from previous column
							begin
								byte(writep^) := byte((writep - sizeYP)^);
								inc(writep);
							end;
							$64: // copy 1 byte from 2 rows ago
							begin
								byte(writep^) := byte((writep - 2)^);
								inc(writep);
							end;
							$68: // copy 1 byte from previous column, 1 row ahead
							begin
								byte(writep^) := byte((writep - sizeYP + 1)^);
								inc(writep);
							end;
							$6C: // copy 1 byte from previous column, 1 row ago
							begin
								byte(writep^) := byte((writep - sizeYP - 1)^);
								inc(writep);
							end;
							$70..$73: // copy from previous column
							begin
								reps := cmd and $F;
								if reps = 0 then reps := src.ReadByte;
								if writep + reps > endp then reps := endp - writep; // safety
								MemCopy(writep - sizeYP, writep, reps);
								inc(writep, reps);
							end;
							$80: // repeat byte pair
							begin
								w := src.ReadWord;
								reps := src.ReadByte;
								FillWord(writep^, reps, w);
								inc(writep, reps shl 1);
							end;
							$90..$9F: // repeat bit pattern
							begin
								reps := src.ReadByte;
								rowp := @w;
								rowp^ := (cmd and $0C) shr 2;
								(rowp + 1)^ := cmd and $03;
								w := w * $55;
								FillWord(writep^, reps, w);
								inc(writep, reps shl 1);
							end;
							$A0..$BF: // repeat rotated byte pair
							begin
								reps := cmd and 7;
								if reps = 0 then reps := src.ReadByte;
								rowp := @w;
								rowp^ := src.ReadByte;
								d := 1;
								if cmd and 8 <> 0 then d := 2;
								if cmd and $F0 = $B0 then
									(rowp + 1)^ := RolByte(rowp^, d)
								else
									(rowp + 1)^ := RorByte(rowp^, d);
								FillWord(writep^, reps, w);
								inc(writep, reps shl 1);
							end;
							$C0..$C3: // repeat XOR'ed byte pair
							begin
								rowp := @w;
								rowp^ := src.ReadByte;
								(rowp + 1)^ := byte(w xor constlit[cmd and $F]);
								reps := src.ReadByte;
								FillWord(writep^, reps, w);
								inc(writep, reps shl 1);
							end;
							$D0..$D3, $DC..$DF: // repeat const byte pair
							begin
								reps := src.ReadByte;
								FillWord(writep^, reps, BEtoN(constlit[cmd and $F]));
								inc(writep, reps shl 1);
							end;
							else begin
								decomp_logger('[!] unk cmd ' + strhex(cmd) + ' @ $'+strhex(src.ofs-1));
								inc(decomp_stats.numErrors);
								exit;
							end;
						end;
					end;

					// Sometimes a plane ends one byte too early...
					if writep < @buf[(plane + 1) * p1] then endearly := TRUE;
				end;

			finally
				// Expand decompressed bitplanes into output buffer.
				rowp := @buf[0];
				for x := 0 to (sizeXP shr 3) - 1 do begin
					writep := @bitmap[x shl 2];
					for y := 0 to sizeYP - 1 do begin
						dword(writep^) := ExplodeBits(rowp^)
							or (ExplodeBits((rowp + p1)^) shl 1)
							or (ExplodeBits((rowp + p2)^) shl 2)
							or (ExplodeBits((rowp + p3)^) shl 3);
						inc(writep, stride);
						inc(rowp);
					end;
				end;

				{$ifdef enable_decomp_hacks}
				if decomp_param.doHacks then begin
					// Hack: If any plane ended one byte early, repeat previous row shifted right by one pixel.
					if endearly then begin
						writep := @bitmap[0] + (sizeYP * stride) - 4;
						if dword((writep - stride)^) <> $FFFFFFFF then
							dword(writep^) := NtoBE(
								dword(BEtoN(dword((writep - stride)^)) shr 4) or
								dword((BEtoN(dword((writep - stride - 4)^)) and $F) shl 28)
								);
					end;
				end;
				{$endif}
			end;
		end;
	end;

	procedure _ReadHeader;
	var w, h, i : dword;
		pal : TSRGBPalette = NIL;
	begin
		if src.ReadWord <> BEtoN(word($0F04)) then raise DecompException.Create('bad sig');

		src.bitSelect := $80;
		setlength(pal, 16);
		for i := 0 to 15 do with pal[i] do begin
			r := src.ReadBits(4);
			g := src.ReadBits(4);
			b := src.ReadBits(4);
			a := $F;
			dword(pal[i]) := dword(pal[i]) or (dword(pal[i]) shl 4);
		end;

		w := src.ReadByte shl 3;
		h := LEtoN(src.ReadWord);
		if (w > 640) or (h > 400) or (w = 0) or (h = 0) then
			raise DecompException.Create(strcat('sus image size %x%', [w, h]));

		tempimage := mcg_bitmap.Init(w, h, MCG_FORMAT_INDEXED, 4);
		tempimage.palette := pal;
		pal := NIL;
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			srcFilePath := PathCombine([outdir, basename], 'png');
			origSizeXP := w;
			origSizeYP := h;
			origOfsXP := 0;
			origOfsYP := 0;
		end;
	end;

var bytesize : dword;
begin
	result := FALSE;
	if src.fullFileSize < 37 then raise DecompException.Create('file too tiny');

	try
		_ReadHeader;
		_Load;

		PostProcessGraphic(tempimage, metadata, game, high(dword));

		tempimage.MakePNG(buffy, bytesize);
		SaveFile(metadata.srcFilePath, buffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
	end;
	result := TRUE;
end;

