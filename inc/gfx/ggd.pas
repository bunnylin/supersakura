{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_GGD(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a GGD graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	buffy : pointer = NIL;
	sig, bytesize, transparentindex : dword;

	procedure _LoadBits24(const src : TFileLoader);
	var writep, imgendp : pointer;
		i, dist : dword;
		bpp, cmd : byte;
	begin
		with metadata do begin
			origSizeXP := LEtoN(src.ReadWord);
			origSizeYP := LEtoN(src.ReadWord);
			// Bitmap width must be divisible by 2, add an extra pixel to width if necessary, crop it after decompress.
			// (Maybe it should even round to next divisible by 4?)
			if origSizeXP and 1 <> 0 then inc(origSizeXP);
			// HACK: fix height for CSMT EV09.
			if origSizeYP = 481 then origSizeYP := 480;

			if (origSizeXP > 4480) or (origSizeYP > 1200) or (origSizeXP = 0) or (origSizeYP = 0) then
				raise DecompException.Create(strcat('sus image24 size %x%', [origSizeXP, origSizeYP]));

			// Some RGB images have a single specific color used as transparent. In that case, unpack directly to RGBA.
			// If the image starts with a long run of max green, that's obviously the transparent color. Otherwise call
			// into _postproc to guess the possible transparency by image name.
			if (dword(src.readp^) = BEtoN(dword($0500FF00))) and (word((src.readp + 4)^) = BEtoN(word($00FF))) then
				transparentindex := BEtoN(dword($00FF00FF))
			else
				transparentindex := SelectTransparentIndex(CN_GGD, metadata, tempimage, game);

			if transparentindex = high(dword) then
				tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_BGR, 24)
			else
				tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_BGRA, 32);

			with tempimage do begin
				bpp := bitDepth shr 3;
				writep := @bitmap[0];
				imgendp := writep + length(bitmap);
			end;
		end;

		// The compressed image stream follows immediately.
		while (src.readp < src.endp) and (writep < imgendp) do begin
			cmd := src.ReadByte;
			case cmd of
				0: // repeat last pixel
				begin
					i := src.ReadByte;
					dist := 1;
				end;

				1: // copy from near
				begin
					i := src.ReadByte;
					dist := src.ReadByte;
				end;

				2: // copy from far
				begin
					i := src.ReadByte;
					dist := LEtoN(src.ReadWord);
				end;

				3: // copy single pixel from near
				begin
					i := 1;
					dist := src.ReadByte;
				end;

				4: // copy single pixel from far
				begin
					i := 1;
					dist := LEtoN(src.ReadWord);
				end;

				else begin // literals
					i := cmd - 4;
					if bpp = 3 then begin
						i := i * 3;
						move(src.readp^, writep^, i);
						inc(src.readp, i);
						inc(writep, i);
					end
					else while i <> 0 do begin
						dword(writep^) := dword(src.readp^) or BEtoN(dword($FF));
						if dword(writep^) = transparentindex then byte((writep + 3)^) := 0;
						inc(src.readp, 3);
						inc(writep, 4);
						dec(i);
					end;
					continue;
				end;
			end;

			// Complete the pixel copy.
			i := i * bpp;
			dist := dist * bpp;
			if writep + i > imgendp then begin
				i := imgendp - writep;
				if decomp_param.verbose then decomp_logger('[!] copy past end');
			end;
			MemCopy(writep - dist, writep, i);
			inc(writep, i);
		end;
	end;

	procedure _LoadBits32(const src : TFileLoader);
	var writep, imgendp : pointer;
		i, dist : dword;
		cmd, alphamask : byte;
	begin
		with metadata do begin
			if dword(src.readp^) = $30303030 then inc(src.readp, 4); // extra-long signature

			origSizeXP := LEtoN(src.ReadWord);
			origSizeYP := LEtoN(src.ReadWord);

			if (origSizeXP > 800) or (origSizeYP > 1024) or (origSizeXP = 0) or (origSizeYP = 0) then
				raise DecompException.Create(strcat('sus image32 size %x%', [origSizeXP, origSizeYP]));

			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_BGRA, 32);
		end;

		i := LEtoN(src.ReadDwordFrom(16));
		if (i < $30) or (i + LEtoN(src.ReadDwordFrom(20)) > src.fullFileSize) then
			raise DecompException.Create('bad header size: $' + strhex(i));
		src.ofs := i;

		// The compressed image stream follows the header.
		writep := @tempimage.bitmap[0];
		imgendp := writep + length(tempimage.bitmap);
		alphamask := 0;

		while (src.readp < src.endp) and (writep < imgendp) do begin
			cmd := src.ReadByte;
			case cmd of
				0: // short repeat last pixel
				begin
					dist := 1;
					i := src.ReadByte;
				end;

				1: // long repeat last pixel
				begin
					dist := 1;
					i := LEtoN(src.ReadWord);
				end;

				2: // copy single pixel from near
				begin
					dist := src.ReadByte;
					i := 1;
				end;

				3: // copy single pixel from far
				begin
					dist := LEtoN(src.ReadWord);
					i := 1;
				end;

				4: // short copy from near
				begin
					dist := src.ReadByte;
					i := src.ReadByte;
				end;

				5: // long copy from near
				begin
					dist := src.ReadByte;
					i := LEtoN(src.ReadWord);
				end;

				6: // short copy from far
				begin
					dist := LEtoN(src.ReadWord);
					i := src.ReadByte;
				end;

				7: // long copy from far
				begin
					dist := LEtoN(src.ReadWord);
					i := LEtoN(src.ReadWord);
				end;

				8: // copy previous pixel once
				begin
					dist := 1;
					i := 1;
				end;

				9: // copy pixel from above
				begin
					dist := metadata.origSizeXP;
					i := 1;
				end;

				10: // copy pixel from above and behind
				begin
					dist := metadata.origSizeXP + 1;
					i := 1;
				end;

				11: // copy pixel from above and ahead
				begin
					dist := metadata.origSizeXP - 1;
					i := 1;
				end;

				else begin // literals
					i := (cmd - 11) * 4;
					alphamask := alphamask or byte((src.readp + 3)^);
					move(src.readp^, writep^, i);
					inc(src.readp, i);
					inc(writep, i);
					continue;
				end;
			end;

			// Complete the pixel copy.
			i := i * 4;
			dist := dist * 4;
			if writep + i > imgendp then begin
				i := imgendp - writep;
				if decomp_param.verbose then decomp_logger('[!] copy past end');
			end;
			alphamask := alphamask or byte((writep - dist + 3)^);
			if i = 4 then
				dword(writep^) := dword((writep - dist)^)
			else if dist = 4 then
				filldword(writep^, i shr 2, dword((writep - dist)^))
			else
				MemCopy(writep - dist, writep, i);
			inc(writep, i);
		end;

		// A fully transparent alpha layer can be scrapped.
		if alphamask = 0 then tempimage.bitmapFormat := MCG_FORMAT_BGRX;
	end;

	procedure _LoadBits8(const src : TFileLoader; multi : boolean);
	var writep : pointer;
		i : dword;
	begin
		if LEtoN(src.ReadDword) <> $28 then raise DecompException.Create('expected $28 as 2nd dword');

		with metadata do begin
			origSizeXP := LEtoN(src.ReadDword);
			i := LEtoN(src.ReadDword);
			if longint(i) < 0 then i := -longint(i); // height can be signed, use abs value anyway
			origSizeYP := i;

			// HACK: fix width for two Angel Crisis images.
			if game = gid.AngelCrisis then
				if (graphicName = 'ENWIN2') or (graphicName = 'H_ICO') then inc(origSizeXP, 2);

			if (origSizeXP = 0) or (origSizeYP = 0)
			or ((NOT multi) and ((origSizeXP > 1280) or (origSizeYP > 2048)))
			or ((multi) and ((origSizeXP > 2560) or (origSizeYP > 2400)))
			then raise DecompException.Create(strcat('sus image8 size %x%', [origSizeXP, origSizeYP]));

			// Allow some leeway in case of mis-sized images.
			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP + 2, MCG_FORMAT_INDEXED, 8);
			dec(tempimage.sizeYP, 2);
		end;

		// Read the palette.
		setlength(tempimage.palette, 256);
		inc(src.readp, $1C);
		for i := 0 to 255 do dword(tempimage.palette[i]) := LEtoN(src.ReadDword);
		inc(src.readp, 4); // unknown dword

		transparentindex := SelectTransparentIndex(CN_GGD, metadata, tempimage, game);

		// The compressed image stream follows immediately.
		writep := @tempimage.bitmap[0];
		Decompress_LZSS(src.readp, src.endp, writep, i, 18);
	end;

	procedure _LoadBitsMulti(const src : TFileLoader);
	var hdrsize : dword;
	begin
		hdrsize := LEtoN(src.ReadDword);
		if (hdrsize = 0) or (hdrsize > src.fullFileSize shr 1) then
			raise DecompException.Create('bad hdrsize $' + strhex(hdrsize));
		src.ofs := hdrsize;
		sig := BEtoN(src.ReadDword);
		if sig = $B9AAB3B3 then
			_LoadBits24(loader)
		else if sig = $47474130 then
			_LoadBits32(loader)
		else if sig = $CDCAC9B8 then
			_LoadBits8(loader, TRUE)
		else
			raise DecompException.Create('unexpected gad inner sig $' + strhex(sig));
	end;

	procedure _LoadBitsGan(const src : TFileLoader);
	var i, baseindex, startofs, imgbytes, spanlen : dword;
		spanp, imgendp, writep, writeendp, basep : pointer;
		runlen, runtarget, prev_lit : dword;

		function _ReadVarLen(var fromp : pointer) : dword;
		var b : byte;
		begin
			result := byte(fromp^); inc(fromp);
			if result >= $80 then begin
				b := byte(fromp^); inc(fromp);
				if (result and b) = $FF then begin
					result := dword(fromp^); inc(fromp, 4);
				end
				else result := ((result and $7F) shl 8) or b;
			end;
		end;

		procedure _OutputRLE;
		var lit : dword;
		begin
			repeat
				if runlen < runtarget then begin
					inc(runlen);
					dword(writep^) := prev_lit; inc(writep, 3);
				end
				else begin
					lit := NtoLE(LEtoN(dword(src.readp^)) and $FFFFFF); inc(src.readp, 3);
					dword(writep^) := lit; inc(writep, 3);
					inc(runlen);
					if lit = prev_lit then begin
						runlen := 2;
						runtarget := _ReadVarLen(src.readp);
					end;
					prev_lit := lit;
				end;
				dec(spanlen);
			until spanlen = 0;
		end;

	begin
		if src.fullFileSize < $3000 then raise DecompException.Create('file too tiny');
		with metadata do begin
			frameCount := LEtoN(src.ReadDwordFrom(12));
			if (frameCount = 0) or (frameCount > 30) then
				raise DecompException.Create('sus numframes ' + strdec(frameCount));

			origSizeXP := 640;
			origFrameHeightP := 480;
			origSizeYP := origFrameHeightP * frameCount;
			// Allow some leeway for dword writes.
			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP + 1, MCG_FORMAT_BGR, 24);
			dec(tempimage.sizeYP, 1);
			writep := @tempimage.bitmap[0];
		end;

		// Read frame header, unpack each.
		for i := 1 to metadata.frameCount do begin
			src.ofs := $2000 + i * 16;
			if (LEtoN(src.ReadDword) <> i) and (decomp_param.verbose) then decomp_logger('repeated frame ' + strdec(i));
			baseindex := LEtoN(src.ReadDword);
			if baseindex > i then raise DecompException.Create('bad baseindex $' + strhex(baseindex));
			startofs := LEtoN(src.ReadDword);
			imgbytes := LEtoN(src.ReadDword);
			if (startofs < $2810) or (startofs + imgbytes > src.fullFileSize) then
				raise DecompException.Create(strcat('frame % oob $&, $&', [i, startofs, imgbytes]));
			src.ofs := startofs;
			imgendp := src.readp + imgbytes;
			writeendp := writep + 640 * 480 * 3;
			prev_lit := $1000000;
			runlen := 0; runtarget := 0;

			if baseindex <> 0 then begin
				// This frame has a base image. There's a span lengths section before the pixels section.
				basep := @tempimage.bitmap[(baseindex - 1) * 640 * 480 * 3];
				startofs := LEtoN(src.ReadDword);
				spanp := src.readp;
				inc(src.readp, startofs - 4);

				while (src.readp < imgendp) and (writep < writeendp) do begin
					// Output literals.
					spanlen := _ReadVarLen(spanp);
					if spanlen <> 0 then begin
						inc(basep, spanlen * 3);
						if writep + spanlen * 3 > writeendp then begin
							LogError('lits oob');
							break;
						end;
						_OutputRLE;
					end;
					if writep >= writeendp then break;
					// Copy from base image.
					spanlen := _ReadVarLen(spanp);
					if spanlen <> 0 then begin
						spanlen := spanlen * 3;
						if writep + spanlen > writeendp then begin
							LogError('copy oob');
							break;
						end;
						move(basep^, writep^, spanlen);
						inc(basep, spanlen);
						inc(writep, spanlen);
					end;
				end;
			end
			else begin
				// No base image, plain RLE-compressed bitmap.
				spanlen := 640 * 480;
				_OutputRLE;
			end;
		end;
	end;

	procedure _LoadBitsP(const src : TFileLoader);
	var q : qword;
		startofs, i : dword;
	begin
		if BEtoN(src.ReadDword) <> $41494B45 then raise DecompException.Create('expected AIKE as 2nd dword');
		startofs := LEtoN(src.ReadDwordFrom(20));
		if startofs + LEtoN(src.ReadDwordFrom(24)) <> src.fullFileSize then raise DecompException.Create('sus sizes');
		src.ofs := startofs;

		// Build encryption key: sig xor seed.
		q := qword(src.PtrAt(0)^) xor qword(src.PtrAt(12)^);

		// Decrypt the data. (Efficiently: one qword at a time, then leftovers one byte at a time.)
		i := src.RemainingBytes shr 3;
		while i <> 0 do begin
			qword(src.readp^) := qword(src.readp^) xor q;
			inc(src.readp, 8);
			dec(i);
		end;
		q := LEtoN(q);
		while src.readp < src.endp do begin
			byte(src.readp^) := byte(src.readp^) xor byte(q);
			inc(src.readp);
			q := q shr 8;
		end;

		tempimage := mcg_bitmap.FromPNG(src.PtrAt(startofs), src.fullFileSize - startofs, []);
		with metadata do with tempimage do begin
			origSizeXP := sizeXP;
			origSizeYP := sizeYP;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 28 then raise DecompException.Create('file too tiny');

	sig := BEtoN(loader.ReadDword);
	if sig = $28000000 then begin // plain 8-bit GGD without sig, Angel Crisis or Crazy Knuckle 2
		loader.ofs := 0;
		sig := $CDCAC9B8;
	end;

	metadata := CopyOrCreateGraphicFile(upcase(basename));
	metadata.srcFilePath := PathCombine([outdir, basename], 'png');
	transparentindex := high(dword);

	try
		case sig of
			$CDCAC9B8: _LoadBits8(loader, FALSE);
			$B9AAB3B3: _LoadBits24(loader);
			$47474130: _LoadBits32(loader); // "GGA0"
			$47475046: _LoadBitsP(loader); // "GGPF"
			$47414420: _LoadBitsMulti(loader); // "GAD "
			$47414E4D: _LoadBitsGan(loader); // "GANM"
			else raise DecompException.Create('missing GGD sig');
		end;

		freemem(buffy); buffy := NIL;

		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(buffy, bytesize);
		SaveFile(metadata.srcFilePath, buffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
	end;
	result := TRUE;
end;

