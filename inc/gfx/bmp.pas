{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_BMP(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
var src : TFileLoader;
	tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	i, bytesize, transparentindex : dword;
begin
	result := FALSE;
	src := loader;
	if loader.fullFileSize < $100 then raise DecompException.Create('file too tiny');

	try
		// C's Ware's BMP starts with LZ and is LZSS-compressed.
		// Aaru's BMP starts with PD but is otherwise the same.
		i := BEtoN(word(src.readp^));
		if (i = $4C5A) or (i = $5044) then begin
			bytesize := LEtoN(src.ReadDwordFrom(6));
			src := TFileLoader.FromMemory(NIL, bytesize, TRUE);
			Decompress_LZSS(loader.PtrAt($A), loader.endp, src.readp, bytesize, 18);
		end

		// Crowd's ZBM is a BMP with the first 100 bytes xor'd by $FF.
		else if BEtoN(word(src.readp^)) = $BDB2 then begin
			for bytesize := 24 downto 0 do begin
				dword(src.readp^) := dword(src.readp^) xor $FFFFFFFF;
				inc(src.readp, 4);
			end;
			loader.ofs := 0;
		end;

		// Normally sig is "BM", but some Guilty games use "MB".
		if (word(src.readp^) <> $424D) and (word(src.readp^) <> $4D42) then
			raise DecompException.Create('no bmp signature');

		try
			tempimage := mcg_bitmap.FromBMP(src.readp, src.fullFileSize, []);
		except on e : Exception do raise DecompException.Create(e.Message); end;
		if tempimage.bitDepth = 16 then tempimage.Expand16bpp; // BGR16 to BGR

		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			origOfsXP := 0;
			origOfsYP := 0;
			origSizeXP := tempimage.sizeXP;
			origSizeYP := tempimage.sizeYP;
			origFrameHeightP := origSizeYP;
			srcFilePath := PathCombine([outdir, basename], 'png');
			frameCount := 0;
			if upcase(copy(loader.fileName, loader.fileName.Length - 2)) = 'MON' then
				srcFilePath := srcFilePath.Replace('.png','-mon.png');
		end;

		transparentindex := SelectTransparentIndex(CN_BMP, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if src <> loader then begin src.Destroy; src := NIL; end;
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_CWL(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Used by Crowd games.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	bytesize : dword;
	key : longint;
begin
	result := FALSE;
	if loader.fullFileSize < $1000 then raise DecompException.Create('file too tiny');

	if BEtoN(loader.ReadDword) <> $63776420 then raise DecompException.Create('no cwl sig');

	try
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			srcFilePath := PathCombine([outdir, basename], 'png');
			key := loader.ReadByteFrom(52) + $259A;
			origSizeXP := longint(LEtoN(loader.ReadDwordFrom(44))) + key;
			origSizeYP := longint(LEtoN(loader.ReadDwordFrom(48))) + key;
			if (origSizeXP < 2) or (origSizeXP > 1800) or (origSizeYP < 2) or (origSizeYP > 2000) then
				raise DecompException.Create(strcat('sus image size %x%', [origSizeXP, origSizeYP]));

			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_BGR16, 16);
			with tempimage do begin
				move(loader.PtrAt($38)^, bitmap[0], loader.fullFileSize - $38);
				Expand16bpp; // BGR16 to BGR
			end;
		end;

		PostProcessGraphic(tempimage, metadata, game, high(dword));

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_EWBMP(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Used only in the Windows port of Fatal Relations, an encrypted, non-compressed bitmap.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	i, j, bytesize : dword;
	key1, key2, key3 : byte;
begin
	result := FALSE;
	if loader.fullFileSize < $180 then raise DecompException.Create('file too tiny');

	if BEtoN(loader.ReadWord) <> $4557 then raise DecompException.Create('no ew signature');
	if LEtoN(loader.ReadWord) <> 1 then raise DecompException.Create('expected 1 as second word');

	try
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			srcFilePath := PathCombine([outdir, basename], 'png');
			origSizeXP := LEtoN(loader.ReadWord);
			origSizeYP := LEtoN(loader.ReadWord);
			if (origSizeXP < 2) or (origSizeXP > 640) or (origSizeYP < 2) or (origSizeYP > 480) then
				raise DecompException.Create(strcat('sus image size %x%', [origSizeXP, origSizeYP]));
			i := LEtoN(loader.ReadWord);
			if NOT (i in [1,4,8]) then raise DecompException.Create('bad bitdepth: ' + strdec(i));

			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_INDEXED, i);

			with tempimage do begin
				i := LEtoN(loader.ReadWord);
				if (i < 2) or (i > 256) then raise DecompException.Create('bad palsize: ' + strdec(i));
				setlength(palette, i);
				key1 := loader.ReadByte;
				key2 := loader.ReadByte;

				loader.ofs := $18;
				for i := 0 to high(palette) do begin
					dword(palette[i]) := $FF000000 or (BEtoN(loader.ReadDword) shr 8);
					if length(palette) <= 16 then begin
						j := dword(palette[i]) and $F0F0F0F0;
						dword(palette[i]) := j or (j shr 4);
					end;
				end;
				i := loader.ofs;

				// Decrypt.
				while loader.readp < loader.endp do begin
					key3 := byte(key1 + key2);
					key1 := key2; key2 := key3;
					byte(loader.readp^) := byte(byte(loader.readp^) - key3);
					inc(loader.readp);
				end;

				// Copy bits to our image buffer. Must flip image vertically.
				loader.ofs := i;
				i := stride * sizeYP;
				for j := sizeYP - 1 downto 0 do begin
					dec(i, stride);
					move(loader.readp^, bitmap[i], stride);
					inc(loader.readp, stride);
				end;
			end;
		end;

		PostProcessGraphic(tempimage, metadata, game, high(dword));

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_ACH(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Converts AIL's ACH images.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	writep : pointer;
	i, x, y, bytesize, transparentindex : dword;
begin
	result := FALSE;
	if loader.fullFileSize < 80 then raise DecompException.Create('file too tiny');
	// Too much risk of false positives, only try this on AIL's games.
	case game of
		gid.MahaBarata, gid.Skirmish, gid.DualSoul98, gid.InmaSeiGa98, gid.MajoGari98, gid.Kyouhaku98,
		gid.RuriiroNoYuki: ;
		else raise DecompException.Create('wrong game');
	end;

	try
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			origSizeXP := LEtoN(loader.ReadWord);
			origSizeYP := LEtoN(loader.ReadWord);
			{$ifdef enable_decomp_hacks}
			// Hack: Add sizes to a graphic that doesn't have them.
			if (game = gid.Kyouhaku98) and (origSizeXP = $FFFF) and (loader.fullFileSize = $780) then begin
				loader.ofs := 0;
				origSizeXP := 16;
				origSizeYP := 240;
			end;
			{$endif}
			if (origSizeXP < 2) or (origSizeXP > 640) or (origSizeXP and 7 <> 0)
			or (origSizeYP < 2) or (origSizeYP > 400) then
				raise DecompException.Create(strcat('sus image size %x%', [origSizeXP, origSizeYP]));

			origFrameHeightP := origSizeYP;
			origOfsXP := 0;
			origOfsYP := 0;
			srcFilePath := PathCombine([outdir, basename], 'png');
			frameCount := 0;

			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_INDEXED, 4);
			with tempimage do begin
				setlength(palette, 16);
				if loader.fullFileSize >= dword(length(bitmap) + 4 + 48) then begin
					// Palette is present.
					i := 0;
					while i < 16 do begin
						palette[i].r := byte(loader.readp^) and $F;
						palette[i].g := byte(loader.readp^) shr 4;
						inc(loader.readp);
						palette[i].b := byte(loader.readp^) and $F;
						palette[i].a := $F;
						dword(palette[i]) := dword(palette[i]) * $11;
						inc(i);
						palette[i].r := byte(loader.readp^) shr 4;
						inc(loader.readp);
						palette[i].g := byte(loader.readp^) and $F;
						palette[i].b := byte(loader.readp^) shr 4;
						inc(loader.readp);
						palette[i].a := $F;
						dword(palette[i]) := dword(palette[i]) * $11;
						inc(i);
					end;
					inc(loader.readp, 24);
				end
				else if lowercase(ExtractFileNameWithoutExt(loader.fileName)) = 'logo' then begin
					dword(palette[0]) := $FFFFFFFF;
					dword(palette[7]) := 0;
					palette[7].a := $FF;
				end;

				if (loader.RemainingBytes < length(bitmap)) or (loader.RemainingBytes > length(bitmap) + 1) then
					raise DecompException.Create(
						strcat('file size % not expected %', [loader.RemainingBytes, length(bitmap)]));

				// The image data is split into bitplanes but not otherwise compressed.
				writep := @tempimage.bitmap[0];
				i := origSizeXP shr 3;
				for y := origSizeYP - 1 downto 0 do begin
					for x := i - 1 downto 0 do begin
						dword(writep^) := ExplodeBits(byte(loader.readp^))
							+ (ExplodeBits(byte((loader.readp + i)^)) shl 1)
							+ (ExplodeBits(byte((loader.readp + i + i)^)) shl 2)
							+ (ExplodeBits(byte((loader.readp + i * 3)^)) shl 3);
						inc(writep, 4);
						inc(loader.readp, 1);
					end;
					inc(loader.readp, i * 3);
				end;
			end;
		end;

		transparentindex := SelectTransparentIndex(CN_ACH, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_C24(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	writep, rowarray : pointer;
	i, j, w, h, ofsx, ofsy, bytesize, numframes, frameindex : dword;
	runcode, bpp : byte;
	singlesize : boolean = TRUE;
	c25 : boolean;

	procedure _Unpack24;
	var x, y : dword;
	begin
		for y := metadata.origFrameHeightP - 1 downto 0 do begin
			loader.ofs := LEtoN(dword(rowarray^)); inc(rowarray, 4);
			runcode := $FF;
			x := tempimage.sizeXP;
			repeat
				i := loader.ReadByte;
				if i = runcode then i := LEtoN(loader.ReadWord);
				if i > x then i := x;
				dec(x, i);

				if runcode = 0 then begin
					// Opaque span.
					if bpp = 24 then begin
						i := i * 3;
						move(loader.readp^, writep^, i);
						inc(loader.readp, i);
						inc(writep, i);
					end
					else while i <> 0 do begin
						word(writep^) := word(loader.readp^);
						inc(loader.readp, 2);
						inc(writep, 2);
						byte(writep^) := byte(loader.readp^);
						inc(loader.readp);
						inc(writep);
						byte(writep^) := $FF;
						inc(writep);
						dec(i);
					end;
				end
				else if i <> 0 then begin
					// Transparent run.
					if bpp = 24 then raise DecompException.Create('alpha run in single-frame image');
					tempimage.bitmapFormat := MCG_FORMAT_BGRA;
					filldword(writep^, i, 0);
					inc(writep, i shl 2);
				end;

				runcode := runcode xor $FF;
			until x = 0;
		end;
	end;

	procedure _Unpack25;
	var x, y : dword;
	begin
		for y := metadata.origFrameHeightP - 1 downto 0 do begin
			loader.ofs := LEtoN(dword(rowarray^)); inc(rowarray, 4);
			x := tempimage.sizeXP;
			repeat
				i := loader.ReadByte;
				if i >= $F0 then begin // full alpha span
					j := 2;
					i := i and $F;
				end
				else if i >= $80 then begin // opaque span
					j := 1;
					i := i and $7F;
				end
				else j := 0; // transparent run

				if i = 0 then i := LEtoN(loader.ReadWord);
				if i > x then i := x;
				dec(x, i);

				case j of
					0: // transparent run
					begin
						tempimage.bitmapFormat := MCG_FORMAT_BGRA;
						filldword(writep^, i, 0);
						inc(writep, i shl 2);
					end;

					1: // opaque span
					while i <> 0 do begin
						word(writep^) := word(loader.readp^);
						inc(loader.readp, 2);
						inc(writep, 2);
						byte(writep^) := byte(loader.readp^);
						inc(loader.readp);
						inc(writep);
						byte(writep^) := $FF;
						inc(writep);
						dec(i);
					end;

					2: // full alpha span
					begin
						i := i shl 2;
						move(loader.readp^, writep^, i);
						inc(loader.readp, i);
						inc(writep, i);
					end;
				end;

			until x = 0;
		end;
	end;

	procedure _WrapUp;
	begin
		PostProcessGraphic(tempimage, metadata, game, high(dword));

		tempimage.MakePNG(pngbuffy, bytesize);
		tempimage.Destroy; tempimage := NIL;
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);
		freemem(pngbuffy); pngbuffy := NIL;

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < $200 then raise DecompException.Create('file too tiny');
	i := loader.ReadDword;
	c25 := (i = BEtoN(dword($43323500)));
	if (NOT c25) and (i <> BEtoN(dword($43323400))) then raise DecompException.Create('no c24/25 signature');

	numframes := LEtoN(loader.ReadDword);
	// Normally gallery thumbnail frames might go to 400+, but then Akogare has grphmenu.c25 with 5162 frames!?
	if (numframes = 0) or (numframes > 5162) or (numframes shl 3 > loader.fullFileSize) then
		raise DecompException.Create('sus numframes ' + strdec(numframes));

	// Validate frame info.
	ofsx := 0; ofsy := 0; w := 0; h := 0;
	for frameindex := numframes - 1 downto 0 do begin
		i := LEtoN(loader.ReadDword);
		if i + 40 > loader.fullFileSize then raise DecompException.Create('sus startofs $' + strhex(i));
		if i = 0 then continue; // empty frame

		j := w;
		w := LEtoN(loader.ReadDwordFrom(i));
		singlesize := singlesize and ((j = 0) or (j = w));
		j := h;
		h := LEtoN(loader.ReadDwordFrom(i + 4));
		singlesize := singlesize and ((j = 0) or (j = h));
		if (w < 2) or (w > 1280) or (h < 2) or (h > 960) then
			raise DecompException.Create(strcat('sus frame size %x%', [w, h]));

		j := ofsx;
		ofsx := LEtoN(loader.ReadDwordFrom(i + 8));
		singlesize := singlesize and ((j = 0) or (j = ofsx));
		j := ofsy;
		ofsy := LEtoN(loader.ReadDwordFrom(i + 12));
		singlesize := singlesize and ((j = 0) or (j = ofsy));
		if (longint(ofsx) < -99) or (longint(ofsx) > 620) or (longint(ofsy) < -99) or (longint(ofsy) > 460) then
			raise DecompException.Create(strcat('sus frame ofs %x%', [longint(ofsx), longint(ofsy)]));
	end;
	bpp := 24;
	if (c25) or (numframes <> 1) then bpp := 32;
	singlesize := singlesize and (h * numframes < 8000); // if single image would be huge, split

	try
		if singlesize then begin
			// If all frames are the same size, save them in a single output file.
			metadata := CopyOrCreateGraphicFile(upcase(basename));
			with metadata do begin
				srcFilePath := PathCombine([outdir, basename], 'png');
				frameCount := numframes;
				origSizeXP := w;
				origFrameHeightP := h;
				origSizeYP := h * numframes;
				origOfsXP := longint(ofsx);
				origOfsYP := longint(ofsy);
				tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_BGR, bpp);
				if bpp = 32 then tempimage.bitmapFormat := MCG_FORMAT_BGRX;
				writep := @tempimage.bitmap[0];
			end;
		end;

		for frameindex := 0 to numframes - 1 do begin
			i := LEtoN(loader.ReadDwordFrom(8 + frameindex shl 2));
			if i = 0 then begin // empty frame
				if singlesize then begin
					i := w * h;
					filldword(writep^, i, 0);
					inc(writep, i shl 2);
				end;
				continue;
			end;

			loader.ofs := i;
			rowarray := loader.readp + 16;

			if NOT singlesize then begin
				// If frames are not all the same size, save each frame in a separate output file.
				metadata := CopyOrCreateGraphicFile(upcase(basename) + '.' + strdec(frameindex));
				with metadata do begin
					srcFilePath := PathCombine([outdir, basename + '.png']);
					origSizeXP := LEtoN(loader.ReadDword);
					origSizeYP := LEtoN(loader.ReadDword);
					origFrameHeightP := origSizeYP;
					origOfsXP := longint(LEtoN(loader.ReadDword));
					origOfsYP := longint(LEtoN(loader.ReadDword));
					tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_BGR, bpp);
					if bpp = 32 then tempimage.bitmapFormat := MCG_FORMAT_BGRX;
					writep := @tempimage.bitmap[0];
				end;
			end;

			if c25 then _Unpack25 else _Unpack24;

			// If frames don't fit together, save each frame in separate files.
			if NOT singlesize then _WrapUp;
		end;

		// Save all frames in one file.
		if singlesize then _WrapUp;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_TIFF(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	writep, endp : ^byte;
	i, bytesize : dword;
begin
	result := FALSE;
	if loader.fullFileSize < $200 then raise DecompException.Create('file too tiny');

	if dword(loader.readp^) = BEtoN(dword($4D4D002A)) then raise SkipException.Create('unsupported tiff byteorder');
	if dword(loader.readp^) <> BEtoN(dword($49492A00)) then raise DecompException.Create('no tiff signature');

	try
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			srcFilePath := PathCombine([outdir, basename], 'png');
			origSizeXP := LEtoN(loader.ReadDwordFrom(30));
			origSizeYP := LEtoN(loader.ReadDwordFrom(42));
			if (origSizeXP < 2) or (origSizeXP > 1280) or (origSizeYP < 2) or (origSizeYP > 800) then
				raise DecompException.Create(strcat('sus image size %x%', [origSizeXP, origSizeYP]));
			if origSizeXP and 1 <> 0 then raise SkipException.Create('odd width');

			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_INDEXED, 4);
			with tempimage do begin
				setlength(tempimage.palette, 16);
				loader.ofs := $100;
				for i := 0 to 15 do
					with palette[i] do begin
						r := byte(loader.readp^);
						g := byte((loader.readp + 32)^);
						b := byte((loader.readp + 64)^);
						inc(loader.readp, 2);
					end;

				// Copy the uncompressed bitmap data, flipping the nibbles.
				i := sizeXP * sizeYP shr 1;
				loader.ofs := $200;
				if loader.readp + i > loader.endp then
					raise SkipException.Create('not enough input stream for bitmap');
				writep := @bitmap[0];
				endp := writep + i;
				while writep < endp do begin
					writep^ := RorByte(byte(loader.readp^), 4);
					inc(loader.readp);
					inc(writep);
				end;
			end;
		end;

		PostProcessGraphic(tempimage, metadata, game, high(dword));

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

