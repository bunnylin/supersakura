{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_VDF(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a VDF (or LDF etc) image. Adds its metadata to graphicFiles[] and saves the
// bitmap in outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	mask : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	stream : TFileLoader = NIL;
	format : (vdf, ldf, mdf);

	procedure _LoadADF(const src : TFileLoader);
	var temploader : TFileLoader = NIL;
		name : UTF8string;
		i, count, size, readofs : dword;
	begin
		if loader.fullFileSize < $1200 then raise DecompException.Create('file too tiny');
		count := LEtoN(src.ReadWord);
		if (count = 0) or (count > 24) then raise DecompException.Create('sus framecount ' + strdec(count));
		src.ofs := $800;
		readofs := $1000;
		try
			for i := 0 to count - 1 do begin
				size := LEtoN(src.ReadWordFrom(6 + i * 2));
				name := lowercase(ExtractFileNameWithoutExt(src.ReadString));
				if decomp_param.verbose then decomp_logger(name);
				inc(src.readp);
				if (size = 0) or (readofs + size > src.fullFileSize) then
					raise DecompException.Create('sus frame size $' + strhex(size));

				temploader := TFileLoader.FromMemory(src.PtrAt(readofs), size, FALSE);
				inc(readofs, size);
				Decomp_VDF(temploader, outdir, name, game);
				temploader.Destroy; temploader := NIL;
			end;
		finally
			if temploader <> NIL then temploader.Destroy;
		end;
	end;

	procedure _LoadBits(const src : TFileLoader);
	var temppal : TSRGBPalette = NIL;
		planes : byte;

		function _Decompress : TFileLoader;
		var lzbytes, outbytes : dword;
			p : pointer;
		begin
			lzbytes := LEtoN(dword(src.ReadDword));
			outbytes := LEtoN(dword(src.ReadDword)) + $1000; // extra for undertable
			if longint(lzbytes) > src.RemainingBytes then
				raise DecompException.Create(strcat('sus compressed size $& @ $&', [lzbytes, src.ofs - 8]));
			getmem(p, outbytes);
			Decompress_LZSS(src.readp, src.readp + lzbytes, p, outbytes, 18, FALSE, TRUE, 0, 1); // use undertable 1
			result := TFileLoader.FromMemory(p, outbytes, TRUE);
			//SaveFile(PathCombine([outdir, basename + strhex(lzbytes)], 'bin'), p, outbytes);
			inc(src.readp, lzbytes);
		end;

		procedure _Unpack;
		var colstartp, writep : pointer;
			cols, rows, rowcount, repeats, d, d2 : dword;
			b, plane : byte;
		begin
			cols := tempimage.sizeXP shr 3;
			rows := tempimage.sizeYP;
			writep := @tempimage.bitmap[0];
			filldword(writep^, length(tempimage.bitmap) shr 2, 0);

			colstartp := writep;
			rowcount := rows;
			plane := 0;

			with tempimage do while stream.readp < stream.endp do begin
				b := stream.ReadByte;
				if b = 5 then begin
					// Output pair repeat.
					repeats := stream.ReadByte;
					if repeats = 0 then repeats := 256;
					if repeats * 2 > rowcount then begin
						decomp_logger('[!] pair rep col overflow');
						break;
					end;
					dec(rowcount, repeats * 2);
					//decomp_logger(strcat('rep % x & &', [repeats, byte(stream.readp^), byte((stream.readp + 1)^)]));

					d := ExplodeBits(stream.ReadByte) shl plane;
					d2 := ExplodeBits(stream.ReadByte) shl plane;
					while repeats <> 0 do begin
						dword(writep^) := dword(writep^) or d;
						inc(writep, tempimage.stride);
						dword(writep^) := dword(writep^) or d2;
						inc(writep, tempimage.stride);
						dec(repeats);
					end;
				end
				else if b <> 3 then begin
					// Output literal.
					if b = 4 then b := stream.ReadByte;
					//decomp_logger('lit ' + strhex(b) + ' @ $' + strhex(stream.ofs));
					dword(writep^) := dword(writep^) or (ExplodeBits(b) shl plane);
					inc(writep, tempimage.stride);
					dec(rowcount);
				end
				else begin
					// Output repeat.
					if stream.readp + 2 > stream.endp then begin
						decomp_logger('[!] out of inputs during repeat');
						break;
					end;
					repeats := stream.ReadByte;
					if repeats = 0 then repeats := 256;
					if repeats > rowcount then begin
						decomp_logger(strcat('[!] col overflow, reps % at col-%, row-%', [repeats, cols, rowcount]));
						break;
					end;
					dec(rowcount, repeats);
					//decomp_logger('rep ' + strdec(repeats) + ' x ' + strhex(byte(stream.readp^)));
					d := ExplodeBits(stream.ReadByte) shl plane;
					while repeats <> 0 do begin
						dword(writep^) := dword(writep^) or d;
						inc(writep, tempimage.stride);
						dec(repeats);
					end;
				end;

				if rowcount = 0 then begin
					// All rows done for the current bitplane in this column, on to next bitplane...
					inc(plane);
					if (plane = 4) or ((planes shr plane) and 1 = 0) then begin
						dec(cols);
						if cols = 0 then break; // all done!
						// Next column...
						inc(colstartp, 4);
						plane := 0;
					end;
					rowcount := rows;
					writep := colstartp;
					//decomp_logger('end col, plane=' + strdec(plane) + ', cols remaining ' + strdec(cols));
				end;
			end;
		end;

		function _DecryptPal(c1, c2, c3 : word) : RGBAquad;
		var scheme, charm, strange : word;
		begin
			if c2 = 0 then begin
				c3 := (c3 * 3 + 10) div 20;
				if c3 > $F then c3 := $F;
				result.FromRGBA((c3 * dword($11111100)) or $FF);
				exit;
			end;
			scheme := 0; // silence compiler
			DivMod(c1, 60, scheme, c1); // scheme := c1 div 60; c1 := c1 mod 60
			c1 := c1 * 5 div 3;
			c2 := (c2 * c3) div 100;
			charm := c3 - c2;
			strange := c1 * c2 div 100;

			case scheme of
				0: begin
					c1 := c3;
					c2 := charm + strange;
					c3 := charm;
				end;
				1: begin
					c1 := c3 - strange;
					c2 := c3;
					c3 := charm;
				end;
				2: begin
					c1 := charm;
					c2 := c3;
					c3 := charm + strange;
				end;
				3: begin
					c1 := charm;
					c2 := c3 - strange;
					//c3 := c3;
				end;
				4: begin
					c1 := charm + strange;
					c2 := charm;
					//c3 := c3;
				end;
				5: begin
					c1 := c3;
					c2 := charm;
					c3 := c3 - strange;
				end;
			end;

			result.b := byte((c3 * 3 + 10) div 20);
			result.g := byte((c2 * 3 + 10) div 20);
			result.r := byte((c1 * 3 + 10) div 20);
			result.a := $FF;
			if result.b > $F then result.b := $F;
			if result.g > $F then result.g := $F;
			if result.r > $F then result.r := $F;
			dword(result) := dword(result) or (dword(result) shl 4);
		end;

		procedure _ReadPal;
		var i, j : dword;
		begin
			for i := 47 downto 0 do begin
				j := LEtoN(src.ReadWord);
				if j > $180 then raise DecompException.Create('sus big value in pal: $' + strhex(j));
			end;
			// The palette must be painstakingly unshuffled and decrypted.
			setlength(temppal, 16);
			j := 4;
			for i := 0 to 7 do begin
				temppal[i + 8] := _DecryptPal(
					LEtoN(src.ReadWordFrom(j)),
					LEtoN(src.ReadWordFrom(j + 16)),
					LEtoN(src.ReadWordFrom(j + 32)));
				temppal[i] := _DecryptPal(
					LEtoN(src.ReadWordFrom(j + 48)),
					LEtoN(src.ReadWordFrom(j + 64)),
					LEtoN(src.ReadWordFrom(j + 80)));
				inc(j, 2);
			end;

			i := dword(temppal[0]);
			dword(temppal[0]) := dword(temppal[8]);
			dword(temppal[8]) := i;
		end;

		procedure _ApplyMask;
		var maskp, srcp, destp : pointer;
			i, m : dword;
			newbmp : array of byte = NIL;
			j : byte;
		begin
			Assert((mask <> NIL) and (tempimage <> NIL));
			if (mask.sizeXP <> tempimage.sizeXP) or (mask.sizeYP <> tempimage.sizeYP) then begin
				if decomp_param.verbose then decomp_logger('[!] mask and image not same size');
				exit;
			end;
			Assert(length(mask.bitmap) = length(tempimage.bitmap));
			setlength(tempimage.palette, 17);
			dword(tempimage.palette[16]) := 0; // we'll use this for transparency
			tempimage.bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
			i := tempimage.sizeXP * tempimage.sizeYP;
			setlength(newbmp, i);

			maskp := @mask.bitmap[0];
			srcp := @tempimage.bitmap[0];
			destp := @newbmp[0];
			i := i shr 3;

			// Apply the 4bpp mask on 4bpp tempimage, save result in 8bpp newbmp.
			while i <> 0 do begin
				// For efficiency, process the mask in dwords (8 pixels). Each $F in the mask is a transparent pixel.
				m := LEtoN(dword(maskp^)); inc(maskp, 4);
				qword(destp^) := $1010101010101010;
				if m = high(dword) then begin
					inc(destp, 8);
					inc(srcp, 4);
				end
				else for j := 3 downto 0 do begin
					if m and $F0 <> $F0 then byte(destp^) := (byte(srcp^) shr 4) and $F;
					inc(destp);
					if m and $0F <> $0F then byte(destp^) := byte(srcp^) and $F;
					inc(destp); inc(srcp);
					m := m shr 8;
				end;
				dec(i);
			end;

			setlength(tempimage.bitmap, 0);
			tempimage.bitmap := newbmp;
			newbmp := NIL;
			tempimage.bitDepth := 8;
			tempimage.stride := tempimage.stride shl 1;
		end;

		procedure _LoadSection(out ofsxp, ofsyp : longint);
		var i, sizexp, sizeyp : dword;
		begin
			ofsxp := stream.ReadByte shl 3;
			ofsyp := LEtoN(stream.ReadWord);
			sizexp := stream.ReadByte shl 3;
			sizeyp := LEtoN(stream.ReadWord);
			if (ofsxp > 639) or (ofsyp > 399) then
				raise DecompException.Create(strcat('sus image ofs %,%', [ofsxp, ofsyp]));
			if (ofsxp + longint(sizexp) > 640) or (ofsyp + longint(sizeyp) > 400) or (sizexp = 0) or (sizeyp = 0) then
				raise DecompException.Create(strcat('sus image size %x%', [sizexp, sizeyp]));

			i := stream.ReadByte;
			planes := i and $F;
			if (i shr 4 <> 8) or (planes = 0) then raise DecompException.Create('sus bitflag $' + strhex(i));

			tempimage := mcg_bitmap.Init(sizexp, sizeyp, MCG_FORMAT_INDEXED, 4);
			tempimage.palette := temppal;
			_Unpack;
		end;

		procedure _Save(ofsxp, ofsyp : longint);
		var buffy : pointer = NIL;
			bytesize : dword;
		begin
			try
				metadata := CopyOrCreateGraphicFile(upcase(basename));
				with metadata do begin
					srcFilePath := PathCombine([outdir, basename], 'png');
					origOfsXP := ofsxp;
					origOfsYP := ofsyp;
					origSizeXP := tempimage.sizeXP;
					origSizeYP := tempimage.sizeYP;
				end;

				PostProcessGraphic(tempimage, metadata, game, 16);

				tempimage.MakePNG(buffy, bytesize);
				tempimage.Destroy; tempimage := NIL;

				SaveFile(metadata.srcFilePath, buffy, bytesize);

				AddOrReplaceGraphicFile(metadata);
				metadata := NIL;
			finally
				if buffy <> NIL then freemem(buffy);
			end;
		end;

	var ofsxp, ofsyp : longint;
	begin
		_ReadPal;

		case format of
			vdf: // single uncompressed RLE-encoded image
			begin
				stream := src;
				_LoadSection(ofsxp, ofsyp);
				_Save(ofsxp, ofsyp);
			end;

			ldf: // single compressed image
			begin
				stream := _Decompress;
				_LoadSection(ofsxp, ofsyp);
				stream.Destroy; stream := NIL;
				_Save(ofsxp, ofsyp);
			end;

			mdf: // single compressed image with alpha mask
			begin
				stream := _Decompress;
				_LoadSection(ofsxp, ofsyp);
				stream.Destroy; stream := NIL;
				mask := tempimage; tempimage := NIL;

				stream := _Decompress;
				_LoadSection(ofsxp, ofsyp);
				stream.Destroy; stream := NIL;
				_ApplyMask;
				_Save(ofsxp, ofsyp);
			end;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 118 then raise DecompException.Create('file too tiny');

	case BEtoN(loader.ReadDword) of
		$56444600, $47444600: format := vdf; // or gdf
		$4C444600: format := ldf;
		$4D444600: format := mdf;
		$41444600:
		begin // multiple uncompressed VDF images
			_LoadADF(loader);
			exit(TRUE);
		end;
		else raise DecompException.Create('no vdf sig');
	end;

	try
		_LoadBits(loader);
	finally
		if (format in [ldf, mdf]) and (stream <> NIL) then stream.Destroy;
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if mask <> NIL then begin mask.Destroy; mask := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
	end;

	result := TRUE;
end;

function Decomp_HDF(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a HDF image. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	buffy : pointer = NIL;
	bytesize : dword;

	procedure _LoadBits(const src : TFileLoader);
	var framep, writep, maskp : pointer;
		plane : array[0..3] of pointer;
		q : qword;
		ofs, min_ofs, i, size : dword;
		x, y, w, h, maxwidth, maxheight : word;
		alpha, b : byte;
	begin
		// Validate frame headers and find out largest frame size.
		i := BEtoN(dword(src.readp^));
		if (i = 0) or (i > src.fullFileSize shr 2) or (i and 3 <> 0) then raise DecompException.Create('sus 1st ofs');
		min_ofs := 0;
		maxwidth := 0; maxheight := 0;
		while src.ofs < i do begin
			// Assume minimum frame byte size of two 6-byte headers + five single-byte planes = 17 bytes.
			ofs := BEtoN(src.ReadDword);
			if ofs = src.fullFileSize then begin dec(src.readp, 4); break; end;
			if (ofs < min_ofs) or (ofs + 7 > src.fullFileSize) then
				raise DecompException.Create('sus ofs @ $' + strhex(src.ofs));
			x := LEtoN(src.ReadWordFrom(ofs));
			if NOT x in [1,$F] then raise DecompException.Create('sus frame 1st word @ $' + strhex(ofs));

			w := LEtoN(src.ReadWordFrom(ofs + 2));
			h := LEtoN(src.ReadWordFrom(ofs + 4));
			if (w > 400) or (h > 200) then raise DecompException.Create('sus frame size @ $' + strhex(ofs));
			if w > maxwidth then maxwidth := w;
			if h > maxheight then maxheight := h;

			size := w * h;
			min_ofs := ofs;
			if x = 1 then inc(min_ofs, size + 6); // alpha mask present
			b := loader.ReadByteFrom(min_ofs);
			if b = $F then inc(min_ofs, (size shl 2) + 6) // 4bpp color bitmap present
			else inc(min_ofs, size + 6); // 1bpp color bitmap
			if min_ofs > src.fullFileSize then raise DecompException.Create('frame oob @ $' + strhex(ofs));
		end;

		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			srcFilePath := PathCombine([outdir, basename], 'png');
			origOfsXP := 0;
			origOfsYP := 0;
			origSizeXP := maxwidth shl 3;
			origFrameHeightP := maxheight;
			frameCount := src.ofs shr 2;
			origSizeYP := maxheight * frameCount;

			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_INDEXEDALPHA, 8);
			with tempimage do begin
				setlength(palette, 17);
				palette[0].FromRGBA($FF);
				palette[1].FromRGBA($FF);
				palette[2].FromRGBA4($600F);
				palette[3].FromRGBA4($E00F);
				palette[4].FromRGBA4($42AF);
				palette[5].FromRGBA4($EA2F);
				palette[6].FromRGBA4($001F);
				palette[7].FromRGBA4($545F);
				palette[8].FromRGBA4($CBCF);
				palette[9].FromRGBA4($212F);
				palette[10].FromRGBA4($557F);
				palette[11].FromRGBA4($CCDF);
				palette[12].FromRGBA4($121F);
				palette[13].FromRGBA4($565F);
				palette[14].FromRGBA4($CDCF);
				dword(palette[15]) := $FFFFFFFF;
				palette[16].FromRGBA($FF00FF00); // transparent pink

				framep := @bitmap[0];
				filldword(framep^, sizeXP * sizeYP shr 2, $10101010);

				for i := 0 to frameCount - 1 do begin
					ofs := BEtoN(src.ReadDwordFrom(i shl 2));
					// Read the alpha mask header, if present.
					maskp := NIL;
					b := src.ReadByteFrom(ofs);
					w := LEtoN(src.ReadWordFrom(ofs + 2));
					h := LEtoN(src.ReadWordFrom(ofs + 4));
					size := w * h;
					if b = 1 then begin
						maskp := src.PtrAt(ofs + 6);
						// Read the color bitmap header.
						inc(ofs, 6 + size);
						if (w <> LEtoN(src.ReadWordFrom(ofs + 2)))
						or (h <> LEtoN(src.ReadWordFrom(ofs + 4)))
						then raise DecompException.Create('mask and bmp not same size, frame ' + strdec(i));
						b := src.ReadByteFrom(ofs);
					end;
					plane[0] := src.PtrAt(ofs + 6);
					plane[1] := plane[0];
					if b and 2 <> 0 then inc(plane[1], size);
					plane[2] := plane[1];
					if b and 4 <> 0 then inc(plane[2], size);
					plane[3] := plane[2];
					if b and 8 <> 0 then inc(plane[3], size);

					if size <> 0 then for y := h - 1 downto 0 do begin
						// Merge the color planes.
						writep := framep;
						for x := w - 1 downto 0 do begin
							if maskp = NIL then alpha := $FF else begin
								alpha := byte(maskp^); inc(maskp);
							end;
							if alpha = 0 then
								inc(writep, 8)
							else begin
								q := ExplodeBitsQ(byte(plane[0]^)) + ExplodeBitsQ(byte(plane[1]^)) shl 1
									+ ExplodeBitsQ(byte(plane[2]^)) shl 2 + ExplodeBitsQ(byte(plane[3]^)) shl 3;
								for b := 7 downto 0 do begin
									// Alpha mask: top bit is leftmost pixel.
									// Color bits: top nibble of first byte is leftmost pixel.
									alpha := RolByte(alpha, 1);
									if alpha and 1 <> 0 then byte(writep^) := q and $F;
									q := RorQWord(q, 8);
									inc(writep);
								end;
							end;
							inc(plane[0]); inc(plane[1]); inc(plane[2]); inc(plane[3]);
						end;
						inc(framep, stride);
					end;
					inc(framep, longint(stride) * (maxheight - h));
				end;
			end;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 82 then raise DecompException.Create('file too tiny');

	{$ifdef enable_decomp_hacks}
	// Hack: Skip last 4 frames that seem broken.
	case basename of
		'missile1': if loader.ReadWordFrom($42) = NtoBE(word($608)) then
			word(loader.PtrAt($42)^) := NtoBE(word(loader.fullFileSize));
		'mis2': if loader.ReadWordFrom($32) = NtoBE(word($FD0)) then
			word(loader.PtrAt($32)^) := NtoBE(word(loader.fullFileSize));
	end;
	{$endif}

	try
		_LoadBits(loader);

		PostProcessGraphic(tempimage, metadata, game, 16);

		tempimage.MakePNG(buffy, bytesize);
		tempimage.Destroy; tempimage := NIL;

		SaveFile(metadata.srcFilePath, buffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;
	finally
		if buffy <> NIL then freemem(buffy);
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
	end;

	result := TRUE;
end;

