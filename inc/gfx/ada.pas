{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

type TMultiFrame = record
		maxbounds : record
			left, right, top, bottom : dword;
		end;
		frame : array of record
			startofs : dword;
			width, height, ofsx, ofsy : word;
		end;
		numframes : byte;
	end;

procedure LoadAniWrapper(const src : TFileLoader; out frames : TMultiFrame; imgtype : byte);
var i, j, lx, sx : dword;
begin
	with frames do begin
		numframes := src.ReadByte;
		if (numframes < 2) or (dword(numframes * 3) >= src.fullFileSize) or (numframes > 50) then
			raise DecompException.Create('sus numframes ' + strdec(numframes));
		j := numframes * 2 + 3;
		i := LEtoN(src.ReadWord); // array terminator should point to end of last frame data

		// First frame must start right after end of array.
		if LEtoN(word(src.readp^)) <> j + 2 then raise DecompException.Create('sus 1st frame ofs');

		// Hack: fix bad terminator
		if (i = $0EC7) and (lowercase(ExtractFileNameWithoutExt(src.fileName)) = 'm702') and (numframes = 7) then begin
			dec(numframes); dec(j, 2);
		end;

		if (i > src.fullFileSize) or (i < src.fullFileSize shr 1) or (i <> LEtoN(src.ReadWordFrom(j))) then
			raise DecompException.Create('bad first word');

		with maxbounds do begin
			left := high(dword); right := 0;
			top := high(dword); bottom := 0;
		end;
		frame := NIL;
		setlength(frame, numframes + 1);
		for i := 0 to numframes do with frame[i] do begin
			startofs := LEtoN(src.ReadWord);
			if (startofs >= src.fullFileSize) or (startofs < j) then raise DecompException.Create('frame ofs oob');
			if i = numframes then break;

			j := startofs;
			if imgtype = 6 then begin
				lx := src.ReadByteFrom(startofs) shl 3; inc(startofs);
				ofsy := src.ReadByteFrom(startofs); inc(startofs);
				sx := src.ReadByteFrom(startofs) shl 3; inc(startofs);
				height := src.ReadByteFrom(startofs); inc(startofs);
			end
			else begin
				lx := src.ReadWordFrom(startofs) shl 3; inc(startofs, 2);
				ofsy := src.ReadWordFrom(startofs); inc(startofs, 2);
				sx := src.ReadWordFrom(startofs) shl 3; inc(startofs, 2);
				height := src.ReadWordFrom(startofs); inc(startofs, 2);
			end;

			if (sx > 640) or (height > 200) or (sx = 0) or (height = 0) then
				raise DecompException.Create(strcat('sus image size %x%', [sx, height]));
			if (lx > 639) or (ofsy > 199) then
				raise DecompException.Create(strcat('sus image ofs &:%,%', [j, lx, ofsy]));
			ofsx := lx; width := sx;
			if ofsx < maxbounds.left then maxbounds.left := ofsx;
			if ofsy < maxbounds.top then maxbounds.top := ofsy;
			if ofsx + width > maxbounds.right then maxbounds.right := ofsx + width;
			if ofsy + height > maxbounds.bottom then maxbounds.bottom := ofsy + height;
		end;
	end;
end;

function _GetLeftovers(const src : TFileLoader) : dword;
// Returns number of bytes from read pointer to eof, discounting any number of nulls at eof.
// This can be used to determine if a lot of data is left after decompression was thought finished, implying an error.
var p : ^byte;
begin
	if src.RemainingBytes < 0 then raise DecompException.Create('read past eof');
	result := src.RemainingBytes;
	p := src.endp - 1;
	while (result <> 0) and (p^ = 0) do begin dec(p); dec(result); end;
end;

function Decomp_PR6(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a PR6 image. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	imagebytesize : dword;

	procedure _Unpack1(const src : TFileLoader; writep : pointer; cols, rows : word);
	// This is PR6 v1, used in Lipstick Adv 1.
	var colstartp, copyp : pointer;
		d : dword;
		y, copiableincol, rowcount : word;
		a, b, c, copylen : byte;
	begin
		Assert((cols <> 0) and (rows <> 0));
		rowcount := rows;
		colstartp := writep;

		with tempimage do while src.readp < src.endp do begin
			a := src.ReadByte;
			b := src.ReadByte;
			c := src.ReadByte;
			if (a = $93) and (b <> $93) then begin
				// Copy from before. bb = src col, cc = src row, dd = count; must take image's offset in account.
				copylen := src.ReadByte;
				y := sizeYP - rowcount;
				d := (colstartp - @bitmap[0]) shr 2;
				if (b < metadata.origOfsXP shr 3) or (c < metadata.origOfsYP) then
					raise DecompException.Create(strcat('copy from -ofs @ %,%', [d shl 1, y]));
				dec(b, metadata.origOfsXP shr 3);
				dec(c, metadata.origOfsYP);
				if (b >= sizeXP shr 3) or (c >= sizeYP) then
					raise DecompException.Create(strcat('copy oob @ %,%', [d shl 1, y]));
				if (b > d) or ((b = d) and (c >= y)) then raise DecompException.Create(
					strcat('copy from ahead @ %,%', [d shl 1, y]));

				copyp := @bitmap[b shl 2 + c * stride];;
				copiableincol := rows - c;

				while copylen <> 0 do begin
					dec(copylen);
					dword(writep^) := dword(copyp^);
					inc(writep, stride);
					inc(copyp, stride);
					// Track and column-wrap copy source pointer.
					dec(copiableincol);
					if copiableincol = 0 then begin
						copiableincol := rows;
						inc(b);
						copyp := @bitmap[b shl 2];
					end;
					// Track and column-wrap write pointer.
					dec(rowcount);
					if rowcount = 0 then begin
						dec(cols);
						if (cols = 0) and (copylen <> 0) then raise DecompException.Create('rep oob');
						inc(colstartp, 4);
						writep := colstartp;
						rowcount := rows;
					end;
				end;
			end
			else begin
				// "93 93 cc" copy is actually literal "93 cc dd".
				if a = $93 then begin
					b := c;
					c := src.ReadByte;
				end;
				// Output literal byte triplet.
				d := ExplodeBits(a);
				d := d or (ExplodeBits(b) shl 1);
				d := d or (ExplodeBits(c) shl 2);
				dword(writep^) := d;

				dec(rowcount);
				if rowcount <> 0 then
					inc(writep, stride)
				else begin
					dec(cols);
					inc(colstartp, 4);
					writep := colstartp;
					rowcount := rows;
				end;
			end;
			if cols = 0 then break;
		end;
		if cols <> 0 then raise DecompException.Create('out of inputs');
		d := _GetLeftovers(src);
		if d > 4 then raise DecompException.Create('leftover input ' + strdec(d));
	end;

	procedure _Unpack2(const src : TFileLoader; writep : pointer; cols, rows : word);
	// This is PR6 v2, used in Dragon Knight 1 and Dragoon Armor and many others.
	var basep, colstartp, copyp : pointer;
		d : dword;
		y, rowcount : word;
		a, b, c : byte;
	begin
		Assert((cols <> 0) and (rows <> 0));
		rowcount := rows;
		basep := writep;
		colstartp := writep;

		with tempimage do while src.readp < src.endp do begin
			a := src.ReadByte;
			b := src.ReadByte;
			c := src.ReadByte;
			if (a = $E9) and (b <> $E9) then begin
				// Copy from before. bb = src row, cc and $3F + 1 = count
				y := sizeYP - rowcount;
				if c >= $80 then raise DecompException.Create(
					strcat('rep $& $& @ %,%', [b, c, (colstartp - basep) shl 1, y]));

				if game = gid.AngelHearts then begin // in this game only, rows are relative to full screen height
					if b >= metadata.origOfsYP then
						dec(b, metadata.origOfsYP)
					else
						raise DecompException.Create('copy neg');
				end;

				if (c < $40) and (b >= y) then raise DecompException.Create(
					strcat('copy from ahead @ %,%', [(colstartp - basep) shl 1, y]));

				copyp := colstartp + b * stride;
				if c >= $40 then begin
					if cols = sizeXP then raise DecompException.Create('copy oob');
					dec(copyp, 4);
					c := c and $3F;
				end;
				inc(c);

				if c > rowcount then raise DecompException.Create('copy past col end');
				dec(rowcount, c);
				while c <> 0 do begin
					dword(writep^) := dword(copyp^);
					inc(writep, stride);
					inc(copyp, stride);
					dec(c);
				end;
				if rowcount = 0 then begin
					dec(cols);
					if cols = 0 then break;
					inc(colstartp, 4);
					writep := colstartp;
					rowcount := rows;
				end;
			end
			else begin
				// "E9 E9 cc" copy is actually literal "E9 cc dd".
				if a = $E9 then begin
					b := c;
					c := src.ReadByte;
				end;
				// Output literal byte triplet.
				d := ExplodeBits(a);
				d := d or (ExplodeBits(b) shl 1);
				d := d or (ExplodeBits(c) shl 2);
				dword(writep^) := d;

				dec(rowcount);
				if rowcount <> 0 then
					inc(writep, stride)
				else begin
					dec(cols);
					if cols = 0 then break;
					inc(colstartp, 4);
					writep := colstartp;
					rowcount := rows;
				end;
			end;
		end;
		if cols <> 0 then raise DecompException.Create('out of inputs');
	end;

	procedure _LoadBits(const src : TFileLoader);
	var err : string;
		i : dword;
	begin
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do with tempimage do begin
			origOfsXP := src.ReadByte shl 3;
			origOfsYP := src.ReadByte;
			i := src.ReadByte shl 3;
			origSizeYP := src.ReadByte;
			if (i > 640) or (origSizeYP > 200) or (i = 0) or (origSizeYP = 0) then
				raise DecompException.Create(strcat('sus image size %x%', [i, origSizeYP]));
			if (origOfsXP > 639) or (origOfsYP > 199) then
				raise DecompException.Create(strcat('sus image ofs %,%', [origOfsXP, origOfsYP]));
			origSizeXP := i;

			sizeXP := origSizeXP;
			sizeYP := origSizeYP;
			stride := sizeXP shr 1;
			i := stride * metadata.origSizeYP;
			setlength(bitmap, i);

			SetupPalette(tempimage, 8);

			try
				_Unpack2(src, @bitmap[0], sizeXP shr 3, sizeYP);
			except on e2 : DecompException do begin
				err := 'pr6v2: ' + e2.Message;
				try
					src.ofs := 4;
					_Unpack1(src, @bitmap[0], sizeXP shr 3, sizeYP);
				except on e1 : DecompException do begin
					err := err + '; pr6v1: ' + e1.Message;
					raise DecompException.Create(err);
				end end;
			end; end;
		end;
		// Hack: Some Foxy 1 graphics have extra garbage bytes, ignore.
		if game = gid.Foxy1 then begin
			err := ExtractFileNameWithoutExt(src.fileName);
			if (err = '15-2') or (err = '1-3') then exit;
		end;
		// Because it's hard to conclusively identify an .ada file as PR6 format, fail if there's notable leftover
		// data; likely a different format in that case. But if the file suffix is not ada, then tolerate more.
		i := _GetLeftovers(src);
		if (i > 1300) or ((i > 4) and (lowercase(ExtractFileExt(src.fileName)) = '.ada')) then
			raise DecompException.Create('leftover input ' + strdec(i));
	end;

	function _LoadAni(const src : TFileLoader) : boolean;
	var frames : TMultiFrame;
		framebytesize, i, j : dword;
	begin
		result := FALSE;
		LoadAniWrapper(src, frames, 6);

		metadata := CopyOrCreateGraphicFile(upcase(basename) + '.A');
		with metadata do with tempimage do with frames.maxbounds do begin
			frameCount := frames.numframes;
			origOfsXP := left;
			origOfsYP := top;
			origSizeXP := right - left;
			j := bottom - top;
			origSizeYP := j * frameCount;

			SetupPalette(tempimage, 9);
			sizeXP := origSizeXP;
			sizeYP := origSizeYP;
			stride := sizeXP shr 1;
			framebytesize := stride * j;
			i := framebytesize * frames.numframes;
			setlength(bitmap, i);
			filldword(bitmap[0], i shr 2, $88888888); // all transparent

			src.ofs := 0;
			with frames do for i := 0 to numframes - 1 do with frame[i] do begin
				src.ofs := startofs;
				// Calculate starting write offset from frame origin, plus frame's offset against max bounds.
				j := longint(i * framebytesize) + (ofsy - origOfsYP) * longint(stride) + (ofsx - origOfsXP) shr 1;
				_Unpack2(src, @bitmap[0] + j, width shr 3, height);
				if src.ofs > frame[i + 1].startofs then raise DecompException.Create('frame unpack went oob');
			end;
		end;

		result := TRUE;
	end;

var buffy : pointer = NIL;
	isani : boolean;
begin
	result := FALSE;
	if loader.fullFileSize < 8 then raise DecompException.Create('file too tiny');
	case upcase(ExtractFileExt(loader.fileName)) of
		'.ADA','.ANI','.PR6': ;
		else raise DecompException.Create('sus ext'); // too many false positives, restrict to known suffixes
	end;

	tempimage := mcg_bitmap.Create();
	with tempimage do begin
		bitDepth := 4;
		bitmapFormat := MCG_FORMAT_INDEXED;
		bitmap := NIL;
		palette := NIL;
	end;

	try
		// First see if it's multiple PR6's in an ANI wrapper, then try for just a single PR6.
		isani := TRUE;
		try
			_LoadAni(loader);
		except on e : DecompException do begin
			isani := FALSE;
			if decomp_param.verbose then decomp_logger('PR6 ANI: ' + e.Message);
			if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
			loader.ofs := 0;
			_LoadBits(loader);
		end; end;

		SelectImageResolution(metadata, game);
		{$ifdef enable_decomp_hacks}
		if isani then
			ApplyGraphicsHacks(metadata, tempimage, game, 8)
		else
			ApplyGraphicsHacks(metadata, tempimage, game, high(dword));
		{$endif}

		tempimage.MakePNG(buffy, imagebytesize);

		with metadata do begin
			if isani then
				srcFilePath := PathCombine([outdir, basename], 'a.png')
			else
				srcFilePath := PathCombine([outdir, basename], 'png');
			SaveFile(srcFilePath, buffy, imagebytesize);
		end;

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_PR7(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a PR7/PD7/PD8 image. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	imagebytesize : dword;
	special : array[0..23] of byte;
	highspecial : byte; // 15 for pd7v1, 23 for pd7v2/pd8
	isani, ispr6v3, ispd7 : boolean;

	procedure _Unpack(const src : TFileLoader; writep : pointer; cols, rows : word);
	var basep, colstartp, copyp : pointer;
		d : dword;
		rowcount : word;
		a, b, c, cmd, reps, mask : byte;

		procedure _Literal(b, r, g, i : byte);
		var d : dword;
		begin
			d := ExplodeBits(b);
			d := d or (ExplodeBits(r) shl 1);
			d := d or (ExplodeBits(g) shl 2);
			d := d or (ExplodeBits(i) shl 3);
			dword(writep^) := d;
			inc(writep, tempimage.stride);
			dec(rowcount);
		end;

		procedure _OutputMasked;
		var plane : array[0..3] of byte;
			m, x, bits : byte;
		begin
			dword((@plane[0])^) := 0;
			x := src.ReadByte;
			m := mask;
			for bits := 0 to 3 do begin
				if m and 1 = 0 then plane[bits] := x;
				if m and $10 = 0 then plane[bits] := plane[bits] or (x xor $FF);
				m := m shr 1;
			end;
			_Literal(plane[0], plane[1], plane[2], plane[3]);
		end;

		function _EndCol : boolean;
		begin
			dec(cols);
			if cols = 0 then exit(TRUE);
			inc(colstartp, 4);
			writep := colstartp;
			rowcount := rows;
			result := FALSE;
		end;

	begin
		Assert((cols <> 0) and (rows <> 0));
		rowcount := rows;
		basep := writep;
		colstartp := writep;
		copyp := NIL;
		mask := 0;

		with tempimage do while (cols <> 0) and (src.readp < src.endp) do begin
			b := src.ReadByte;
			cmd := $FF;
			for a := highspecial downto 0 do if b = special[a] then begin cmd := a; break; end;

			// Special code byte appears doubled? It's a surprise literal! Not a special code after all.
			if (cmd <> $FF) and (byte(src.readp^) = special[a]) then begin cmd := $FF; inc(src.readp); end;

			if cmd <> $FF then begin
				// Special command.
				if cmd in [0..8] then begin
					// Copy something from earlier.
					a := src.ReadByte;
					case cmd of
						1..4: // Copy 1..4 from this column.
						begin
							if a >= rows - rowcount then
								raise DecompException.Create('copy from future at $' + strhex(src.ofs - 1));
							copyp := colstartp + a * stride;
							reps := cmd;
						end;

						5..8: // Copy 1..4 from previous column.
						begin
							if colstartp = basep then raise DecompException.Create(strhex(cmd) + ' in 1st col');
							copyp := colstartp + a * stride - 4;
							reps := cmd - 4;
						end;

						else begin // Copy n from this or previous column.
							reps := src.ReadByte;
							copyp := colstartp + a * stride;
							if reps and $40 <> 0 then
								raise DecompException.Create(strcat('unexpected reps $& @ $&', [reps, src.ofs - 1]));
							if reps and $80 <> 0 then begin
								reps := reps and $3F;
								dec(copyp, 4);
							end;
							inc(reps);
						end;
					end;
					if a + reps > rows then
						raise DecompException.Create('src col overflow @ $' + strhex(src.ofs - 1));
					if (rowcount < reps) and (cols = 1) then
						raise DecompException.Create('dest col overflow @ $' + strhex(src.ofs - 1));
					while reps <> 0 do begin
						dword(writep^) := dword(copyp^);
						inc(copyp, stride);
						inc(writep, stride);
						dec(rowcount);
						if rowcount = 0 then if _EndCol then break;
						dec(reps);
					end;
				end
				else begin
					if highspecial > 16 then begin
						// PD7v2 has a different set of commands 9..23.
						case cmd of
							9..11: // Output one or two masked bytes.
							begin
								if cmd = 9 then mask := src.ReadByte;
								_OutputMasked;
								if rowcount = 0 then _EndCol;
								if cmd = 11 then begin
									_OutputMasked;
									if rowcount = 0 then _EndCol;
								end;
							end;

							22, 23: // Copy previous row, shifted right or left by xx nibbles.
							begin
								if rowcount = rows then
									raise DecompException.Create('copy prev at top row @ $' + strhex(src.ofs - 1));
								d := NtoBE(dword((writep - stride)^));
								for a := src.ReadByte - 1 downto 0 do
									if cmd = 22 then
										d := (d and $F0000000) or (d shr 4)
									else
										d := (d and $F) or ((d and $FFFFFFF) shl 4);
								dword(writep^) := BEtoN(d);
								inc(writep, stride);
								dec(rowcount);
								if rowcount = 0 then _EndCol;
							end;

							else for reps := 1 downto 0 do begin
								// Output two literal byte BRGI quartets.
								a := src.ReadByte; b := src.ReadByte; c := src.ReadByte;
								case cmd of
									12: _Literal(a, a, b, c); // x, g, i
									13: _Literal(a, b, a, c); // x, r, i
									14: _Literal(a, b, c, a); // x, r, g
									15: _Literal(a, b, b, c); // b, x, i
									16: _Literal(a, b, c, b); // b, x, g
									17: _Literal(a, b, c, c); // b, r, x
									18: _Literal(0, a, b, c); // literal without blue
									19: _Literal(a, 0, b, c); // literal without red
									20: _Literal(a, b, 0, c); // literal without green
									21: _Literal(a, b, c, 0); // literal without intensity
								end;
								if rowcount = 0 then _EndCol;
							end;
						end;
					end
					else if (ispr6v3) and (cmd = 9) then begin
						// Output single row with all bitplanes equal.
						a := src.ReadByte;
						_Literal(a, a, a, 0);
						if rowcount = 0 then if _EndCol then break;
					end
					else begin
						// Output two literal byte quartets.
						for reps := 1 downto 0 do begin
							a := src.ReadByte; b := src.ReadByte;
							case cmd of
								9: _Literal(a, b, src.ReadByte, 0);
								10: _Literal(a, a, b, 0);
								11: _Literal(b, a, a, 0);
								12: _Literal(a, b, a, 0);
								13: _Literal(0, a, b, 0);
								14: _Literal(a, 0, b, 0);
								15: _Literal(a, b, 0, 0);
							end;
							if rowcount = 0 then if _EndCol then break;
						end;
					end;
				end;
			end
			else begin
				// Output literal byte triplet (pr6v3) or quartet (pr7, pd7).
				a := src.ReadByte;
				c := src.ReadByte;
				if ispr6v3 then
					_Literal(b, a, c, 0)
				else begin
					_Literal(a, c, src.ReadByte, b);
				end;
				if rowcount = 0 then _EndCol;
			end;
		end;
		if cols <> 0 then raise DecompException.Create('out of inputs');
	end;

	procedure _LoadBits(const src : TFileLoader);

		procedure _ReadSpecials;
		var l, n : byte;
		begin
			highspecial := 15;
			for l := 0 to 23 do begin
				special[l] := src.ReadByte;
				// Special codes must be unique.
				if l <> 0 then for n := l - 1 downto 0 do if special[n] = special[l] then begin
					if l < 16 then raise DecompException.Create('non-unique special $' + strhex(special[l]));
					dec(src.readp, l - 15); // anything read after the 16th code is really image data, go back there
					exit;
				end;
				// Special codes are never < $C8.
				if NOT (special[l] in [$C8..$FF]) then begin
					if l < 16 then raise DecompException.Create('sus special $' + strhex(special[l]));
					dec(src.readp, l - 15);
					exit;
				end;
			end;
			highspecial := 23;
		end;

	var i, j, secondimageofs, secondheight : dword;
	begin
		secondimageofs := 0;
		secondheight := 0;
		if (NOT isani) and (metadata = NIL) then begin
			metadata := CopyOrCreateGraphicFile(upcase(basename));
			with metadata do with tempimage do begin
				if ispd7 then begin
					secondimageofs := LEtoN(src.ReadWord);
					if secondimageofs + 56 >= src.fullFileSize then raise DecompException.Create('1st word oob');
				end;
				origOfsXP := LEtoN(src.ReadWord) shl 3;
				origOfsYP := LEtoN(src.ReadWord);
				sizeXP := LEtoN(src.ReadWord) shl 3;
				sizeYP := LEtoN(src.ReadWord);
				if (ispd7) and (sizeYP > 200) and (sizeYP <= 400) then sizeYP := 200; // clamp to max 200

				if (sizeXP > 640) or (sizeYP > 200) or (sizeXP = 0) or (sizeYP = 0) then
					raise DecompException.Create(strcat('sus img size %x%', [sizeXP, sizeYP]));
				if (origOfsXP > 639) or (origOfsYP > 399) or ((NOT ispd7) and (origOfsYP > 199)) then
					raise DecompException.Create(strcat('sus img ofs %,%', [origOfsXP, origOfsYP]));

				if secondimageofs <> 0 then begin
					// The first image is always 200 px high, whatever its header says, if there's a second to append.
					sizeYP := 200;
					i := secondimageofs;
					if (LEtoN(src.ReadWordFrom(i)) shl 3 <> dword(origOfsXP))
					or (LEtoN(src.ReadWordFrom(i + 2)) <> dword(origOfsYP) + 200)
					or (LEtoN(src.ReadWordFrom(i + 4)) shl 3 <> sizeXP)
					then raise DecompException.Create('sus 2nd img pos');
					inc(i, 6);
					secondheight := LEtoN(src.ReadWordFrom(i));
					if (secondheight = 0) or (secondheight > 200) then
						raise DecompException.Create('sus 2nd img height: ' + strdec(secondheight));
					secondimageofs := i + 2 + 32; // to start of special codes
				end;

				origSizeXP := sizeXP;
				origSizeYP := sizeYP;
				stride := sizeXP shr 1;
				i := stride * (origSizeYP + secondheight);
				setlength(bitmap, i);
			end;
		end;

		with tempimage do begin
			if palette <> NIL then
				inc(src.readp, 32)
			else begin
				setlength(palette, 16);
				for i := 0 to 15 do with palette[i] do begin
					j := src.ReadByte;
					b := (j and $F) * 17;
					r := (j shr 4) * 17;
					g := (src.ReadByte and $F) * 17;
					a := $FF;
				end;
			end;

			_ReadSpecials;

			if NOT isani then begin
				_Unpack(src, @bitmap[0], sizeXP shr 3, sizeYP);
				if ispd7 then begin
					if secondimageofs <> 0 then begin
						Assert(secondheight <> 0);
						src.ofs := secondimageofs;
						_ReadSpecials;
						i := stride * sizeYP;
						inc(sizeYP, secondheight);
						metadata.origSizeYP := sizeYP;
						_Unpack(src, @bitmap[i], sizeXP shr 3, secondheight);
					end;
					exit;
				end;

				// Fail if there's notable leftover data; likely a different format in that case.
				i := _GetLeftovers(src);
				if i > 4 then raise DecompException.Create('leftover input ' + strdec(i));
			end;
		end;
	end;

	function _LoadAni(const src : TFileLoader) : boolean;
	var frames : TMultiFrame;
		framebytesize, i, j : dword;
	begin
		result := FALSE;
		highspecial := 15;
		LoadAniWrapper(src, frames, 7);

		metadata := CopyOrCreateGraphicFile(upcase(basename) + '.A');
		with metadata do with tempimage do with frames.maxbounds do begin
			frameCount := frames.numframes;
			origOfsXP := left;
			origOfsYP := top;
			origSizeXP := right - left;
			j := bottom - top;
			origSizeYP := j * frameCount;

			SetupPalette(tempimage, 9);
			sizeXP := origSizeXP;
			sizeYP := origSizeYP;
			stride := sizeXP shr 1;
			framebytesize := stride * j;
			i := framebytesize * frames.numframes;
			setlength(bitmap, i);
			filldword(bitmap[0], i shr 2, $88888888); // all transparent

			with frames do for i := 0 to numframes - 1 do with frame[i] do begin
				src.ofs := startofs;
				_LoadBits(src);
				// Calculate starting write offset from frame origin, plus frame's offset against max bounds.
				j := longint(i * framebytesize) + (ofsy - origOfsYP) * longint(stride) + (ofsx - origOfsXP) shr 1;
				_Unpack(src, @bitmap[0] + j, width shr 3, height);
				if src.ofs > frame[i + 1].startofs then raise DecompException.Create('frame unpack went oob');
			end;
		end;

		result := TRUE;
	end;

var buffy : pointer = NIL;
	ext : string[4];
begin
	result := FALSE;
	if loader.fullFileSize < 60 then raise DecompException.Create('file too tiny');

	tempimage := mcg_bitmap.Create();
	with tempimage do begin
		bitDepth := 4;
		bitmapFormat := MCG_FORMAT_INDEXED;
		bitmap := NIL;
		palette := NIL;
	end;

	try
		ext := lowercase(ExtractFileExt(loader.fileName));
		ispr6v3 := (ext = '.pr6') or (game = gid.DeJa1);
		ispd7 := (ext = '.pd7') or (ext = '.pd8');
		// First see if it's multiple PR7's in an ANI wrapper, then try for just a single PR7.
		isani := TRUE;
		try
			_LoadAni(loader);
		except on e : DecompException do begin
			isani := FALSE;
			if decomp_param.verbose then decomp_logger('PR7 ANI: ' + e.Message);
			if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
			loader.ofs := 0;
			_LoadBits(loader);
		end; end;

		SelectImageResolution(metadata, game);
		{$ifdef enable_decomp_hacks}
		if isani then
			ApplyGraphicsHacks(metadata, tempimage, game, 8)
		else
			ApplyGraphicsHacks(metadata, tempimage, game, high(dword));
		{$endif}

		tempimage.MakePNG(buffy, imagebytesize);
		with metadata do begin
			if isani then
				srcFilePath := PathCombine([outdir, basename], 'a.png')
			else
				srcFilePath := PathCombine([outdir, basename], 'png');
			SaveFile(srcFilePath, buffy, imagebytesize);
		end;

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_ADA(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an ADA image. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	bytesize : dword;

	procedure _Unpack(const src : TFileLoader; writep : pointer; cols, rows : word; oddness : boolean);
	var nextrow : longint;
		rowcount, reps : word;
		w, w2 : word;
		pattern : byte;

		procedure _NextRow;
		begin
			dec(rowcount);
			if rowcount <> 0 then begin
				inc(writep, nextrow);
				oddness := NOT oddness;
			end
			else begin
				rowcount := rows;
				dec(cols);
				inc(writep, 2);
				nextrow := -nextrow; // going down/up in alternate columns
			end;
		end;

		const lut_pat1 : array of word = (0, $0010, $0101, $1101, $1111);
		const lut_pat2 : array of word = (0, $1000, $1010, $0111, $1111);

		procedure _GetPattern; inline;
		var b : byte;
		begin
			b := pattern mod 5; // green pattern
			w := lut_pat1[b];
			w2 := lut_pat2[b];
			pattern := pattern div 5;
			b := pattern mod 5; // red pattern
			w := w or (lut_pat1[b] shl 1);
			w2 := w2 or (lut_pat2[b] shl 1);
			b := pattern div 5; // blue pattern
			w := w or (lut_pat1[b] shl 2);
			w2 := w2 or (lut_pat2[b] shl 2);
		end;

	begin
		nextrow := tempimage.stride;
		Assert((cols <> 0) and (rows <> 0));
		rowcount := rows;

		while src.readp < src.endp do begin
			if src.ReadBit then begin
				// Output literal byte triplet.
				w := ExplodeBitsW(src.ReadBits(4));
				w := w or (ExplodeBitsW(src.ReadBits(4)) shl 1);
				w := w or (ExplodeBitsW(src.ReadBits(4)) shl 2);
				word(writep^) := w;
				_NextRow;
				if cols = 0 then exit;
			end
			else begin
				// Repeat a dithering pattern.
				pattern := src.ReadBits(7);
				if pattern >= 125 then raise DecompException.Create('bad pattern @ $' + strhex(src.ofs - 1));
				reps := src.ReadBits(6);
				if reps > word(rows - rowcount) + cols * rows then
					raise DecompException.Create('rep oob @ $' + strhex(src.ofs - 1));
				_GetPattern;
				while reps <> 0 do begin
					if oddness then word(writep^) := w else word(writep^) := w2;
					_NextRow;
					if cols = 0 then exit;
					dec(reps);
				end;
			end;
		end;
	end;

	procedure _LoadBits(const src : TFileLoader);
	// This is the ADA variant in Koroshi no Dress 2.
	var x, y, w, h, i : dword;
	begin
		i := LEtoN(src.ReadWord);
		if i < $C000 then raise DecompException.Create('sus ofs $' + strhex(i));
		dec(i, $C000);

		y := i div 80;
		x := (i mod 80) shl 3;
		w := src.ReadByte shl 2;
		h := src.ReadByte;
		if (w > 640) or (h > 200) or (w = 0) or (h = 0) then
			raise DecompException.Create(strcat('sus image size %x%', [w, h]));
		if (x > 639) or (y > 199) then
			raise DecompException.Create(strcat('sus image ofs &:%,%', [i, x, y]));

		tempimage := mcg_bitmap.Init(w, h, MCG_FORMAT_INDEXED, 4);
		metadata := CopyOrCreateGraphicFile(upcase(basename));

		with metadata do with tempimage do begin
			srcFilePath := PathCombine([outdir, basename], 'png');
			origOfsXP := x;
			origOfsYP := y;
			origSizeXP := sizeXP;
			origSizeYP := sizeYP;
			i := stride * origSizeYP;
			if dword(length(bitmap)) < i then setlength(bitmap, 0);
			setlength(bitmap, i);

			_Unpack(src, @bitmap[0], sizeXP shr 2, sizeYP, origOfsYP and 1 <> 0);

			// Hack: some files have garbage at the end, ignore.
			case game of
				gid.KoroshinoDress1: if graphicName = 'BIKE' then exit;
			end;
		end;
		// Fail if there's notable leftover data; likely a different format in that case.
		i := _GetLeftovers(src);
		if i > 4 then raise DecompException.Create('leftover input ' + strdec(i));
	end;

var buffy : pointer = NIL;
begin
	result := FALSE;
	if loader.fullFileSize < 8 then raise DecompException.Create('file too tiny');

	// Words Worth has some GP4-ADA files, handled by the GP4 loader.
	bytesize := LEtoN(loader.ReadWordFrom(0));
	if (bytesize > loader.fullFileSize shr 1) and (bytesize + 100 < loader.fullFileSize) then begin
		bytesize := LEtoN(loader.ReadWordFrom(2));
		if (bytesize = loader.fullFileSize - 1) and (loader.ReadByteFrom(bytesize) = $1A) then
			exit(Decomp_GP4(loader, outdir, basename, game));
	end;

	try
		_LoadBits(loader);

		SelectImageResolution(metadata, game);
		{$ifdef enable_decomp_hacks}
		ApplyGraphicsHacks(metadata, tempimage, game, high(dword));
		{$endif}

		tempimage.MakePNG(buffy, bytesize);
		SaveFile(metadata.srcFilePath, buffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_MDA(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an MDA animation. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	imagebytesize : dword;

	procedure _Unpack(const src : TFileLoader; writep : pointer; cols, rows : word; oddness : boolean);
	var d, d2 : dword;
		nextrow : longint;
		rowcount, reps : word;
		cmd, cmdi, pattern : byte;

		procedure _NextRow;
		begin
			dec(rowcount);
			if rowcount <> 0 then begin
				inc(writep, nextrow);
				oddness := NOT oddness;
			end
			else begin
				rowcount := rows;
				dec(cols);
				inc(writep, 4);
				nextrow := -nextrow; // going down/up in alternate columns
			end;
		end;

		const lut_pat1 : array of dword = (0, $00100010, $01010101, $11011101, $11111111);
		const lut_pat2 : array of dword = (0, $10001000, $10101010, $01110111, $11111111);

		procedure _GetPattern; inline;
		var b : byte;
		begin
			b := pattern mod 5; // green pattern
			d := lut_pat1[b] shl 2;
			d2 := lut_pat2[b] shl 2;
			pattern := pattern div 5;
			b := pattern mod 5; // red pattern
			d := d or (lut_pat1[b] shl 1);
			d2 := d2 or (lut_pat2[b] shl 1);
			b := pattern div 5; // blue pattern
			d := d or lut_pat1[b];
			d2 := d2 or lut_pat2[b];
		end;

	begin
		nextrow := tempimage.stride;
		Assert((cols <> 0) and (rows <> 0));
		rowcount := rows;
		cmdi := 1;

		while src.readp < src.endp do begin
			dec(cmdi);
			if cmdi = 0 then begin
				cmd := src.ReadByte;
				cmdi := 8;
			end;

			if cmd and $80 <> 0 then begin
				// Output literal byte triplet.
				d := ExplodeBits(src.ReadByte);
				d := d or (ExplodeBits(src.ReadByte) shl 1);
				d := d or (ExplodeBits(src.ReadByte) shl 2);
				dword(writep^) := d;
				_NextRow;
				if cols = 0 then exit;
			end
			else begin
				// Repeat a dithering pattern.
				pattern := src.ReadByte;
				if pattern >= 125 then raise DecompException.Create('bad pattern @ $' + strhex(src.ofs - 1));
				reps := src.ReadByte;
				if reps > word(rows - rowcount) + cols * rows then
					raise DecompException.Create('rep oob @ $' + strhex(src.ofs - 1));
				_GetPattern;
				while reps <> 0 do begin
					if oddness then dword(writep^) := d else dword(writep^) := d2;
					_NextRow;
					if cols = 0 then exit;
					dec(reps);
				end;
			end;

			cmd := byte(cmd shl 1);
		end;
	end;

	procedure _LoadBits(const src : TFileLoader);
	var maxbounds : record
			left, right, top, bottom : dword;
		end;
		frameinfo : array of record
			startofs : dword;
			width, height, ofsx, ofsy : word;
		end;
		framebytesize, i, j, x : dword;
		numframes : byte;
	begin
		numframes := src.ReadByte; // last word pointer is not a frame
		if (numframes < 2) or (numframes > 52) then
			raise DecompException.Create('sus framecount ' + strdec(numframes));
		if src.fullFileSize < dword(numframes * 6 + 1) then raise DecompException.Create('too tiny');

		with maxbounds do begin
			left := high(dword); right := 0;
			top := high(dword); bottom := 0;
		end;
		frameinfo := NIL;
		setlength(frameinfo, numframes);
		for i := 0 to numframes - 1 do begin
			// First frame header appeared while still reading frame addresses; stop reading and carry on.
			if (i <> 0) and (src.ofs = frameinfo[0].startofs - 4) then begin
				setlength(frameinfo, i);
				break;
			end;

			x := LEtoN(src.ReadWord);
			if x > $F000 then begin
				if (x shr 8 <> $F0) or (x and $FF <> src.ofs) then
					raise DecompException.Create('bad terminator $' + strhex(x));
				// Last item in array may be an F0zz terminator, in which case drop it and carry on.
				if i = byte(numframes - 1) then begin
					setlength(frameinfo, i);
					break;
				end
			end;
			if x + 6 > src.fullFileSize then raise DecompException.Create('frame oob $' + strhex(x + 6));

			with frameinfo[i] do begin
				j := LEtoN(src.ReadWordFrom(x));
				if j < $C000 then raise DecompException.Create('sus ofs $' + strhex(j));
				dec(j, $C000);
				ofsy := j div 80;
				ofsx := (j mod 80) shl 3;
				width := src.ReadByteFrom(x + 2) shl 3;
				height := src.ReadByteFrom(x + 3);
				if (width > 640) or (height > 200) or (width = 0) or (height = 0) then
					raise DecompException.Create(strcat('sus image size %x%', [width, height]));
				if (ofsx > 639) or (ofsy > 199) then
					raise DecompException.Create(strcat('sus image ofs &:%,%', [j, ofsx, ofsy]));
				if ofsx < maxbounds.left then maxbounds.left := ofsx;
				if ofsy < maxbounds.top then maxbounds.top := ofsy;
				if ofsx + width > maxbounds.right then maxbounds.right := ofsx + width;
				if ofsy + height > maxbounds.bottom then maxbounds.bottom := ofsy + height;
				startofs := x + 4;
			end;
		end;

		metadata := CopyOrCreateGraphicFile(upcase(basename) + '.A');
		metadata.srcFilePath := PathCombine([outdir, basename], 'a.png');
		with maxbounds do
			tempimage := mcg_bitmap.Init(right - left, (bottom - top) * numframes, MCG_FORMAT_INDEXEDALPHA, 4);

		with metadata do with tempimage do begin

			frameCount := length(frameinfo);
			with maxbounds do begin
				origOfsXP := left;
				origOfsYP := top;
				origSizeXP := sizeXP;
				origFrameHeightP := bottom - top;
				origSizeYP := sizeYP;
			end;
			framebytesize := stride * origFrameHeightP;

			SetupPalette(tempimage, 9);
			i := framebytesize * frameCount;
			setlength(bitmap, i);
			filldword(bitmap[0], i shr 2, $88888888); // all transparent

			for i := 0 to frameCount - 1 do with frameinfo[i] do begin
				src.ofs := startofs;
				// Calculate starting write offset from frame's offset against max bounds, plus target frame origin.
				j := longint(i * framebytesize) + (ofsy - origOfsYP) * longint(stride) + (ofsx - origOfsXP) shr 1;
				_Unpack(src, @bitmap[0] + j, width shr 3, height, ofsy and 1 <> 0);
			end;

			// Hack: some files have garbage at the end, ignore.
			case game of
				gid.CanCanB2:
				if copy(graphicName, 1, 2) = 'K1' then exit;
				gid.DragoonArmor:
				if (graphicName = 'HIME_A.A') or (graphicName = 'KURO_A.A') or (graphicName = 'KK_A.A') then exit;
				gid.Illumina:
				if copy(graphicName, 1, 2) = 'C0' then exit;
				gid.UrotsukiDouji:
				if graphicName = '19C.A' then exit;
				gid.Xna:
				if (graphicName = 'X109A.A') or (graphicName = 'X109B.A') or (graphicName = 'X110.A') then exit;
			end;

			// Fail if there's notable leftover data; likely a different format in that case.
			i := _GetLeftovers(src);
			if (i > 900) or ((frameCount < 6) and (i > 256)) then
				raise DecompException.Create('leftover input ' + strdec(i));
		end;
	end;

var buffy : pointer = NIL;
begin
	result := FALSE;
	if loader.fullFileSize < 40 then raise DecompException.Create('file too tiny');

	try
		_LoadBits(loader);

		PostProcessGraphic(tempimage, metadata, game, 8);

		tempimage.MakePNG(buffy, imagebytesize);
		SaveFile(metadata.srcFilePath, buffy, imagebytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
	end;
	result := TRUE;
end;

