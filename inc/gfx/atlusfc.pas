{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_AtlusFC(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an Atlus FC graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	numframes, numtiles : word;
	hasalpha : boolean = FALSE;
	tiles : array of byte = NIL;
	palettes : array of TSRGBPalette = NIL;
	frameofs : array[0..39] of word;

	procedure _Unpack(const src : TFileLoader);
	var tilep, writep : pointer;
		name : string[15];
		i, j : dword;
		x, y, cols, rows : word;
		b : byte;
		step : shortint = 8;
		hflip, vflip : boolean;
	begin
		for i := 0 to numframes - 1 do begin
			src.ofs := frameofs[i];
			name := upcase(basename);
			if (numframes > 1) or (length(palettes) > 1) then name := name + '.';
			if length(palettes) > 1 then name := name + 'A';
			if numframes > 1 then name := name + strdec(i);
			metadata := CopyOrCreateGraphicFile(name);
			with metadata do begin
				decomp_logger(graphicName);
				frameCount := 1;
				srcFilePath := PathCombine([outdir, lowercase(name) + '.png']);
				origOfsXP := smallint(LEtoN(src.ReadWord)) * 8;
				origOfsYP := smallint(LEtoN(src.ReadWord)) * 8;
				cols := LEtoN(src.ReadWord);
				rows := LEtoN(src.ReadWord);
				origSizeXP := cols shl 3;
				origSizeYP := rows shl 3;
				origFrameHeightP := origSizeYP;
				if (origSizeXP = 0) or (origSizeYP = 0) or (origSizeXP > 640) or (origSizeYP > 400) then
					raise DecompException.Create(strcat('sus size %x%', [origSizeXP, origSizeYP]));
				tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_INDEXED, 8);
				tempimage.palette := palettes[0];
				if hasalpha then tempimage.bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
			end;
			// Blit all tiles onto bitmap.
			with tempimage do for x := 0 to cols - 1 do begin
				writep := @bitmap[x * 8];
				for y := rows - 1 downto 0 do begin
					j := LEtoN(word(src.ReadWord));
					// Special bitflags $1000 to $8000.
					if j and $1000 <> 0 then begin // fill with index 4?? no, sometimes correct, usually not
						for b := 7 downto 0 do begin
							qword(writep^) := $0404040404040404;
							inc(writep, stride);
						end;
						continue;
					end;
					hflip := (j and $8000) <> 0;
					vflip := (j and $4000) <> 0;
					j := j and $3FFF;
					if j >= numtiles then begin
						decomp_logger(strcat('[!] tile oob $& @ %,%', [j, x, y]));
						j := (j and $FFF) mod numtiles;
					end;
					// Copy the tile row by row.
					tilep := @tiles[0] + (j shl 6);
					step := 8;
					if vflip then begin
						inc(tilep, 56);
						step := -8;
					end;
					for b := 7 downto 0 do begin
						if hflip then
							qword(writep^) := SwapEndian(qword(tilep^))
						else
							qword(writep^) := qword(tilep^);
						inc(tilep, step);
						inc(writep, stride);
					end;
				end;
			end;

			PostProcessGraphic(tempimage, metadata, game, $10);
			tempimage.MakePNG(pngbuffy, j);
			SaveFile(metadata.srcFilePath, pngbuffy, j);
			freemem(pngbuffy); pngbuffy := NIL;
			AddOrReplaceGraphicFile(metadata);
			metadata := NIL;

			// If there are multiple palette variants, save each separately.
			if length(palettes) > 1 then for b := 1 to high(palettes) do begin
				metadata := CopyOrCreateGraphicFile(name); // get a new value copy of the same metadata, and edit it
				if numframes = 1 then
					inc(byte(name[length(name)]))
				else
					inc(byte(name[length(name) - 1]));
				decomp_logger(name);
				metadata.graphicName := name;
				metadata.srcFilePath := PathCombine([outdir, lowercase(name) + '.png']);
				tempimage.palette := palettes[b];
				tempimage.MakePNG(pngbuffy, j);
				SaveFile(metadata.srcFilePath, pngbuffy, j);
				freemem(pngbuffy); pngbuffy := NIL;
				AddOrReplaceGraphicFile(metadata);
				metadata := NIL;
			end;
			tempimage.Destroy; tempimage := NIL;

		end;
	end;

	procedure _ReadSheet(const src : TFileLoader);
	var writep : pointer;
		temptile : array[0..31] of byte;
		mask : array[0..3] of byte;
		i, sheetlen : dword;
		count, palmask : word;
		plane, mode, import : byte;
		notimport : boolean;
	begin
		temptile[0] := 0; // silence compiler
		i := LEtoN(src.ReadWord);
		count := LEtoN(src.ReadWord);
		if count = 0 then raise DecompException.Create('sus palcount');
		if i <> dword(count * 32 + 4) then raise DecompException.Create('sus sheet hdr size');
		palmask := LEtoN(src.ReadWord);
		setlength(palettes, count);
		for plane := 0 to count - 1 do begin
			setlength(palettes[plane], 16);
			for i := 0 to 15 do with palettes[plane][i] do begin
				a := $FF;
				if (palmask shl i) and $8000 = 0 then begin
					b := $FF; g := 0; r := $FF; // unused palette entry, make it magenta
					inc(src.readp, 2);
					continue;
				end;
				b := (byte(src.readp^) and $F) * $11;
				r := (src.ReadByte shr 4) * $11;
				if byte(src.readp^) and $F0 <> 0 then raise DecompException.Create('bad pal');
				g := src.ReadByte * $11;
			end;
		end;
		sheetlen := LEtoN(src.ReadDword);
		if src.ofs + sheetlen > src.fullFileSize then raise DecompException.Create('sheet oob');
		inc(src.readp, 2); // mystery word, usually 0
		numtiles := LEtoN(src.ReadWord);
		if numtiles = 0 then raise DecompException.Create('no tiles');
		inc(src.readp, 15); // mystery array, usually all 0
		setlength(tiles, (numtiles + 1) * 64); // 8*8 pixels, 4bpp but extra bit for alpha so 8bpp
		writep := @tiles[0];
		// Tile 0 is always fully transparent.
		filldword(writep^, 16, $10101010);
		inc(writep, 64);

		if numtiles > 1 then for count := numtiles - 2 downto 0 do begin
			if src.RemainingBytes < 9 then begin
				if decomp_param.verbose then decomp_logger('[!] tile data oob, remaining after this: ' + strdec(count));
				break;
			end;
			// Each tile starts with a single compression mode byte. Top nibble is the bitplane mask:
			// 8x: bitplane 1 specified
			// 4x: bitplane 2 specified
			// 2x: bitplane 4 specified
			// 1x: bitplane 8 specified
			mode := src.ReadByte;
			if mode and $F >= $E then raise DecompException.Create('mode E/F!');
			if mode and $F in [6..$B] then import := src.ReadByte;

			// Initial fill for unspecified bitplanes. Fill with FF if bit $01 of mode is set, otherwise with 0.
			for plane := 0 to 3 do
				if (mode shl plane) and $80 = 0 then begin
					if mode and 1 = 0 then i := 0 else i := $FFFFFFFF;
					dword((@temptile[plane * 8])^) := i;
					dword((@temptile[plane * 8 + 4])^) := i;
				end;

			for plane := 0 to 3 do begin
				notimport := TRUE;
				// Import. Plane import source is 2-bit value, all 4 planes packed thus: 11224488
				if mode and $F in [6..$B] then begin
					i := (import shr ((3 - plane) shl 1)) and 3;
					if i <> 3 then begin
						if i <> plane then qword((@temptile[plane * 8])^) := qword((@temptile[i * 8])^);
						notimport := FALSE;
					end;
				end;

				if (mode shl plane) and $80 = 0 then continue; // empty plane, no overrides after import

				// Masked literals.
				mask[plane] := $FF;
				if NOT (mode and $F in [0, 1, 6, 7]) then mask[plane] := src.ReadByte;
				for i := 0 to 7 do begin
					if (mask[plane] shl i) and $80 <> 0 then
						temptile[plane shl 3 + i] := src.ReadByte
					else if notimport then begin
						if mode and $F in [2, 5, 8, $B, $C] then
							temptile[plane shl 3 + i] := 0
						else
							temptile[plane shl 3 + i] := $FF;
					end;
				end;
			end;
			if src.RemainingBytes < 8 then begin
				if decomp_param.verbose then decomp_logger('[!] tile data oob, remaining after this: ' + strdec(count));
				break;
			end;

			// If needed, apply cascading on all masked bytes on all planes.
			if mode and $F in [$C, $D] then
				for plane := 0 to 3 do
					for i := 1 to 7 do
						if (mask[plane] shl i) and $80 = 0 then
							temptile[plane shl 3 + i] := temptile[plane shl 3 + i - 1];

			// Copy it to tiles[] array, expanding bits to proper 8bpp. Apply alpha mask on each row while at it.
			for i := 0 to 7 do begin
				qword(writep^) := ExplodeBitsQ(temptile[i])
					or (ExplodeBitsQ(temptile[i + 8]) shl 1)
					or (ExplodeBitsQ(temptile[i + 16]) shl 2)
					or (ExplodeBitsQ(temptile[i + 24]) shl 3);
				mode := src.ReadByte;
				if mode <> $FF then begin
					hasalpha := TRUE;
					for import := 0 to 7 do
						if (mode shl import) and $80 = 0 then
							byte((writep + import)^) := $10;
				end;
				inc(writep, 8);
			end;
		end;

		if hasalpha then for i := 0 to high(palettes) do begin
			setlength(palettes[i], 17);
			dword(palettes[i][16]) := 0;
		end;
	end;

	procedure _ReadHeader(const src : TFileLoader);
	var i, j, sheetstart : dword;
	begin
		sheetstart := dword(LEtoN(src.ReadDword) + 2);
		if (sheetstart < $12) or (sheetstart + $20 >= src.fullFileSize) then
			raise DecompException.Create('sus sheet ofs');
		numframes := LEtoN(src.ReadWord);
		if numframes = 0 then raise SkipException.Create('empty graphic');
		if numframes > 255 then raise DecompException.Create('sus numframes');
		j := numframes;
		numframes := 0;
		while j <> 0 do begin
			dec(j);
			i := LEtoN(src.ReadWord) + 2;
			if i + 10 > src.fullFileSize then raise DecompException.Create('sus frameofs');
			frameofs[numframes] := i;
			if numframes <> 0 then begin
				i := dword(IndexWord(frameofs[0], numframes, frameofs[numframes]));
				if i < numframes then continue; // skip duplicates
			end;
			inc(numframes);
		end;
		src.ofs := sheetstart;
		_ReadSheet(loader);
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 32 then raise DecompException.Create('file too tiny');

	try
		_ReadHeader(loader);
		_Unpack(loader);
	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

