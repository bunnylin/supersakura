{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_AIC(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an AIC graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	btree : TBinTree = NIL;

	procedure _Delace(const img : mcg_bitmap);
	// Shuffle the pixel bits to correct order: 76543210 -> 1357 0246
	// Images should always have a multiple of 8 width, so can use a dword to shuffle 8 pixels at a time.
	var p, pend : pointer;
		d : dword;
	begin
		p := @img.bitmap[0];
		pend := p + length(img.bitmap);
		while p < pend do begin
			d := dword(p^);
			dword(p^) := (d and $24242424)
				or ((d and $90909090) shr 3)
				or ((d and $09090909) shl 3)
				or ((d and $40404040) shr 6)
				or ((d and $02020202) shl 6);
			inc(p, 4);
		end;
	end;

	procedure _Unpack(const src : TFileLoader; const img : mcg_bitmap);
	var writep, endp, copyp : pointer;
		d : dword;
	const copydistlut : array[0..14] of record x : shortint; y : byte; end = (
		// positive = ago, negative = ahead
		(x:1; y:0), (x:2; y:0), (x:4; y:0), (x:8; y:0),
		(x:-1; y:1), (x:0; y:1), (x:1; y:1), (x:4; y:1),
		(x:-1; y:2), (x:0; y:2), (x:1; y:2),
		(x:0; y:4), (x:2; y:4),
		(x:0; y:8), (x:4; y:8));
	begin
		src.ofs := $2A4;
		src.bitSelect := 1;
		// The image will be directly decompressed into a 4bpp buffer.
		// Set up an access pointer and some important position markers.
		writep := @img.bitmap[0];
		endp := writep + length(img.bitmap);

		while (src.readp <= src.endp) and (writep < endp) do begin
			d := ReadBinaryCode(src, btree, @src.ReadBit2); // in unit mcbtree
			if d <= 255 then begin
				// Literal.
				byte(writep^) := byte(d);
				inc(writep);
			end
			else begin
				// Copy from before.
				if (d < $102) or (d > $11E) or (d and 1 <> 0) then
					raise DecompException.Create('bad copy cmd $' + strhex(d));
				d := (d - $102) shr 1;
				copyp := writep - copydistlut[d].y * img.stride - copydistlut[d].x;
				if copyp < @img.bitmap[0] then raise DecompException.Create('copy oob');
				byte(writep^) := byte(copyp^);
				inc(writep);
			end;
		end;

		if (writep < endp) and (src.readp >= src.endp) then LogError(
			'image incomplete, input ran out, output still needs ' + strdec(endp - writep) + ' bytes');
	end;

	procedure _BuildTree(const src : TFileLoader);
	var p2 : ^word;
		topnode, thisnode : pointer;
		stacklevel : byte = 0;
		nodestack : array[0..15] of pointer;
	begin
		p2 := src.PtrAt($84);
		src.ofs := $40;
		src.bitSelect := 1;
		setlength(btree, 1024);
		filldword(btree[0], 512, 0);
		thisnode := @btree[0];
		topnode := thisnode;
		nodestack[0] := thisnode;

		// Each node in btree consists of two words (4 bytes). Possible states:
		// Left = 0: node is a leaf with value Right, or has no defined branches
		// Left != 0 node has a branch to Left, and...
		//   Right = 0: branch to Right is not defined yet
		//   Right != 0: node has a branch to Right

		while (src.ofs < $80) and (p2 < src.endp) do begin
			// Whether it's a branch or leaf, a new node will be needed.
			inc(topnode, 4);

			if src.ReadBit2 then begin
				// New branch.
				if word(thisnode^) = 0 then begin
					// Branch left; keep current node in stack, must come back later to add a right branch or leaf.
					word(thisnode^) := topnode - thisnode;
					inc(stacklevel);
				end
				else begin
					// Branch right; overwrite current node in stack, no need to ever come to this node again.
					word((thisnode + 2)^) := topnode - thisnode;
				end;
				thisnode := topnode;
				nodestack[stacklevel] := thisnode;
			end
			else begin
				// New leaf. Saved in new top node - value on right, left remains 0 to indicate a leaf.
				word((topnode + 2)^) := LEtoN(p2^); inc(p2);

				if word(thisnode^) = 0 then begin
					// Leaf on left; still need a leaf or branch on this node's right.
					word(thisnode^) := topnode - thisnode;
				end
				else begin
					// Leaf on right, this node is complete, return to last incomplete parent node if any.
					word((thisnode + 2)^) := topnode - thisnode;
					if stacklevel = 0 then exit; // no more incomplete parent nodes - tree complete!
					dec(stacklevel);
					thisnode := nodestack[stacklevel];
				end;
			end;
		end;
	end;

	procedure _ReadHeader(const src : TFileLoader);
	var i, j, x, y : dword;
	begin
		if src.ReadWord <> LEtoN(word($0040)) then raise DecompException.Create('sus header size');
		x := LEtoN(src.ReadWord);
		y := LEtoN(src.ReadWord);
		i := LEtoN(src.ReadWord);
		j := LEtoN(src.ReadWord);
		if (i > 640) or (j > 400) or (i < 8) or (j < 8) or ((i or j) and 7 <> 0) then
			raise DecompException.Create(strcat('sus image size %x%', [i, j]));
		if (x + i > 640) or (y + j > 400) then
			raise DecompException.Create(strcat('sus image ofs %x%', [x, y]));

		if (dword(src.readp^) <> BEtoN(dword($41494331))) // "AIC110"
		or (word((src.readp + 4)^) <> BEtoN(word($3130)))
		then raise DecompException.Create('sus sig');
		inc(src.readp, 6);

		tempimage := mcg_bitmap.Init(i, j, MCG_FORMAT_INDEXED, 4);

		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			frameCount := 0; // pass through MarkAnims in post
			srcFilePath := PathCombine([outdir, basename], 'png');
			origSizeXP := i;
			origFrameHeightP := j;
			origSizeYP := j;
			origOfsXP := x;
			origOfsYP := y;
		end;

		with tempimage do begin
			setlength(palette, 16);

			for i := 0 to high(palette) do with palette[i] do begin
				r := src.ReadByte * $11;
				g := src.ReadByte * $11;
				b := src.ReadByte * $11;
				a := $FF;
			end;
		end;
	end;

var bytesize, transparentindex : dword;
begin
	result := FALSE;
	if loader.fullFileSize < 720 then raise DecompException.Create('file too tiny');

	try
		_ReadHeader(loader);
		_BuildTree(loader);
		_Unpack(loader, tempimage);
		setlength(btree, 0);
		_Delace(tempimage);

		transparentindex := SelectTransparentIndex(CN_AIC, metadata, tempimage, game);

		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

