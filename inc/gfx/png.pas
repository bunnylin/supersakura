{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_CWP(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Converts a Crowd PNG into a standard PNG.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	bytesize : dword;
begin
	result := FALSE;
	if loader.fullFileSize < $100 then raise DecompException.Create('file too tiny');

	if BEtoN(loader.ReadDword) <> $43574450 then raise DecompException.Create('no cwdp sig');

	try
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		metadata.srcFilePath := PathCombine([outdir, basename], 'png');

		getmem(pngbuffy, loader.fullFileSize + 20);
		dword(pngbuffy^) := NtoBE(dword($D)); // IHDR length
		dword((pngbuffy + 4)^) := NtoBE(dword($49484452)); // IHDR sig
		// IHDR content can be copied straight from the CWP. Also has IDAT length as the last dword.
		move(loader.PtrAt(4)^, (pngbuffy + 8)^, 21);
		dword((pngbuffy + 29)^) := NtoBE(dword($49444154)); // IDAT sig
		// IDAT content is the bulk of the CWP.
		bytesize := loader.fullFileSize - $19 - 1;
		move(loader.PtrAt($19)^, (pngbuffy + 33)^, bytesize);
		dword((pngbuffy + bytesize + 37)^) := 0; // IEND length
		dword((pngbuffy + bytesize + 41)^) := NtoBE(dword($49454E44)); // IEND sig

		// It can now be loaded as a standard PNG. Except red and blue channels are the wrong way around, so flip them.
		tempimage := mcg_bitmap.FromPNG(pngbuffy, loader.fullFileSize + 20, [MCG_FLAG_RGBFLIP]);
		freemem(pngbuffy); pngbuffy := NIL;

		with metadata do begin
			origSizeXP := tempimage.sizeXP;
			origSizeYP := tempimage.sizeYP;
		end;

		// The alpha channel is bogus, so scrap it.
		if tempimage.bitmapFormat = MCG_FORMAT_BGRA then tempimage.bitmapFormat := MCG_FORMAT_BGRX;

		PostProcessGraphic(tempimage, metadata, game, high(dword));

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

