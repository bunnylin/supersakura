{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_GP4(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an Elf GP4 graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	isada : boolean = FALSE;

	procedure _Unpack(const src : TFileLoader);
	var destp, endp, colp, colendp : pointer;
		dtable : array[0..17 * 16 - 1] of byte;
		lastcolor : byte;

		function _GetDistCode : longint;
		begin
			if src.ReadBit then begin
				if src.ReadBit then begin
					// 11...: Copy from far away, 2+xxx... columns ago, 8-yyyy rows ago
					result := 0;
					while src.ReadBit do inc(result);
					result := (8 - src.ReadBits(4)) * tempimage.stride + 8 + (result shl 2);
				end
				else begin
					// 10xxx: Copy within current column
					result := src.ReadBits(3);
					if result = 0 then
						result := tempimage.stride * 16 // 16 rows ago
					else if result = 1 then
						result := tempimage.stride * 8
					else
						result := byte(8 - result) * tempimage.stride; // 1..6 rows ago
				end;
			end
			else begin
				// 0xxxx: Copy from previous column, (8 - xxxx) rows ago
				result := (8 - src.ReadBits(4)) * tempimage.stride + 4;
			end;
		end;

		function _GetLenCode : dword;
		begin
			if src.ReadBit then begin // 1...
				if src.ReadBit then begin // 11...
					if src.ReadBit then begin // 111...
						result := src.ReadBits(6) + 16;
						if result = 79 then inc(result, src.ReadBits(10));
					end
					else begin // 110xxx
						result := 8 + src.ReadBits(3);
					end;
				end
				else begin // 10xx
					result := 4 + src.ReadBits(2);
				end;
			end
			else begin // 0x
				if src.ReadBit then result := 3 else result := 2;
			end;
		end;

		procedure _DeltaCode;
		// Reads a delta code from the input bit stream, adjusts the delta table, and outputs a single byte.
		var tablep, p : ^byte;
		begin
			tablep := @dtable[lastcolor shl 4];
			p := tablep;
			lastcolor := tablep^;
			// A delta code is just the number of 1-bits in a row until a 0-bit, eg. 111110 = 5.
			// Move ahead 1 cell on this row for each 1-bit. When we see a 0-bit, the new color byte is in that cell.
			// Swap each cell into the frontmost cell while going; the new color byte remains up front when we're done.
			while src.ReadBit do begin
				inc(p);
				lastcolor := p^;
				p^ := tablep^;
				tablep^ := lastcolor;
			end;

			// Output the color byte, and remember it for the next delta code.
			byte(destp^) := lastcolor;
			inc(destp);
		end;

	var x, y, i : dword;
		dist : longint;
	begin
		// The image will be directly decompressed into an 8bpp buffer, even if the palette is only 16 colors. The
		// algorithm doesn't care about the actual palette values or presence/absence of alpha; we're only dealing with
		// an input bit stream and an output byte stream.

		// Set up the color delta table.
		destp := @dtable[0];
		i := 0;
		for y := 16 downto 0 do begin // 16 colors + extra row
			for x := 15 downto 0 do begin
				byte(destp^) := i and $F;
				inc(destp);
				inc(i);
			end;
			inc(i);
		end;

		colp := @tempimage.bitmap[0];
		endp := colp + length(tempimage.bitmap);
		colendp := endp;
		destp := colp;
		src.bitSelect := 0;

		while colendp < endp + tempimage.stride do begin
			lastcolor := $10; // first literal in each column uses the extra delta table row

			while destp < colendp do begin
				if src.ReadBit then begin
					// Copy from earlier in the image.
					dist := _GetDistCode;
					if destp - dist < @tempimage.bitmap[0] then raise DecompException.Create('copy from oob');
					y := _GetLenCode;
					if destp + y * tempimage.stride > colendp then raise DecompException.Create('copy oob');
					for i := y - 1 downto 0 do begin
						dword(destp^) := dword((destp - dist)^);
						inc(destp, tempimage.stride);
					end;
				end
				else begin
					// Output four delta code literals.
					for i := 3 downto 0 do _DeltaCode;
					destp := destp + tempimage.stride - 4;
				end;
			end;

			inc(colp, 4);
			inc(colendp, 4);
			destp := colp;

			if src.readp > src.endp then raise DecompException.Create('image incomplete, input ran out');
		end;
		if src.readp + 8 < src.endp then raise DecompException.Create(
			'image complete, but input still has ' + strdec(src.RemainingBytes) + ' bytes');
	end;

	procedure _ReadAnim(const src : TFileLoader);
	var seqs, b : byte;
		w, w2 : word;
		firstseq : word;
	begin
		seqs := src.ReadByte;
		if (seqs = 0) or (seqs > $F) then raise DecompException.Create('sus seq count');
		firstseq := LEtoN(src.ReadWord);
		w := firstseq;
		if seqs <> 1 then for b := 0 to seqs - 2 do begin
			w2 := LEtoN(src.ReadWord);
			if w2 <= w then raise DecompException.Create('seq not ascending');
			w := w2;
		end;
		// Read frame data...
		// Read sequence data...
	end;

	procedure _ReadHeader(const src : TFileLoader);
	var x, y, i, j : dword;
		name : UTF8string;
	begin
		x := BEtoN(src.ReadWord); // ofs xy
		y := BEtoN(src.ReadWord);
		i := BEtoN(src.ReadWord) + 1; // size xy - 1
		j := BEtoN(src.ReadWord) + 1;
		if (x >= 640) or (y >= 400) or (x and 3 <> 0) then
			raise DecompException.Create(strcat('sus image ofs %,%', [x, y]));
		if i and 3 <> 0 then i := i and $FFFFFFC; // ignore partial column; Shangrlia2 has two images of 201p width
		if (i = 0) or (j = 0) or (i > 640) or (j > 400) then
			raise DecompException.Create(strcat('sus image size %x%', [i, j]));

		tempimage := mcg_bitmap.Init(i, j, MCG_FORMAT_INDEXED, 8);
		with tempimage do begin
			// Read the palette.
			setlength(palette, 16);
			for i := 0 to 15 do with palette[i] do begin
				j := BEtoN(src.ReadWord);
				if (i <> 8) and (j and 1 = 0) then
					raise DecompException.Create('last pal bit not set for ' + strdec(i));
				g := (j shr 12) * $11;
				r := ((j shr 7) and $F) * $11;
				b := ((j shr 2) and $F) * $11;
				a := $FF;
			end;
			if isada then begin
				palette[8].a := 0;
				bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
			end;
		end;

		name := basename;
		if isada then name := name + '_a';
		metadata := CopyOrCreateGraphicFile(upcase(name));
		with metadata do begin
			srcFilePath := PathCombine([outdir, name], 'png');
			origSizeXP := tempimage.sizeXP;
			origSizeYP := tempimage.sizeYP;
			origOfsXP := x;
			origOfsYP := y;
		end;
	end;

var pngbuffy : pointer = NIL;
	animofs, bytesize, transparentindex : dword;
begin
	result := FALSE;
	if loader.fullFileSize < 108 then raise DecompException.Create('file too tiny');

	try
		// Check for ADA header. If present, the GP4 is inside, plus there's some extra anim data.
		animofs := LEtoN(loader.ReadWordFrom(0));
		if (animofs > loader.fullFileSize shr 2) and (animofs + 100 < loader.fullFileSize) then begin
			bytesize := LEtoN(loader.ReadWordFrom(2));
			if (bytesize = loader.fullFileSize - 1) and (loader.ReadByteFrom(bytesize) = $1A) then begin
				loader.ofs := animofs;
				_ReadAnim(loader);
				loader.ofs := 4; // GP4 data directly follows the anim header
				loader.Crop(0, animofs);
				loader.buffySize := loader.fullFileSize;
				isada := TRUE;
				transparentindex := 8;
			end;
		end;

		_ReadHeader(loader);
		_Unpack(loader);

		if NOT isada then transparentindex := SelectTransparentIndex(CN_GP4, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

