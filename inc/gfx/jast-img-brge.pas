{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Expand_JASTRLE(inp, endp : pointer; out size : dword) : pointer;
// Takes the bytes from inp^ to endp^ and uncompresses them into a new buffer. Returns the buffer and its uncompressed
// length in the size parameter.
// The RLE algorithm is: Treat all bytes as literals unless there are two of the same in a row, in which case read an
// extra byte and output that many bytes of the repeated literal. If repeat count = 0, output 256 copies.
var writep : ^byte;
	rep : dword;
begin
	getmem(result, 65536);
	writep := result;
	while inp < endp do begin
		if writep - result >= 65536 - 256 then begin freemem(result); raise DecompException.Create('rle oom'); end;

		if (endp - inp >= 3) and (byte(inp^) = byte((inp + 1)^)) then begin // repeat
			rep := byte((inp + 2)^);
			if rep = 0 then rep := 256;
			FillByte(writep^, rep, byte(inp^));
			inc(inp, 3);
			inc(writep, rep);
		end
		else begin // literal
			writep^ := byte(inp^);
			inc(writep);
			inc(inp);
		end;
	end;
	size := writep - result;
end;

function Decomp_BRGE(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer (B) and accompanying files (R,G,E) as a joint BRGE image. Adds its metadata to
// graphicFiles[] and saves the bitmap in outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	bf, rf, gf, ef : TFileLoader;
	bytesize : dword;
	iscompressed : boolean;

	procedure _LoadBits;
	// Merges the bitplanes from bf, rf, gf, and ef.
	var writep, endp : pointer;
		i : dword;
		s : string[7];
	begin
		// Interestingly, the files don't have a header, so we only know the total number of pixels in the final
		// image, but not its resolution. However, the game only uses a few different resolutions so we can guess
		// the correct image dimensions from its pixel count.
		bytesize := bf.fullFileSize;
		if rf.fullFileSize < bytesize then bytesize := rf.fullFileSize;
		if gf.fullFileSize < bytesize then bytesize := gf.fullFileSize;
		if ef.fullFileSize < bytesize then bytesize := ef.fullFileSize;
		if (bf.fullFileSize or rf.fullFileSize or gf.fullFileSize or ef.fullFileSize) - bytesize > 522 then
			raise Exception.Create('bitplanes not equal size'); // or at least mostly equal, may have a garbage tail
		bytesize := bytesize shl 2;

		tempimage := mcg_bitmap.Create();
		with tempimage do begin
			bitDepth := 4;
			bitmapFormat := MCG_FORMAT_INDEXED;
			bitmap := NIL;
			palette := NIL;
			setlength(palette, 16);
			palette[0].FromRGBA4($000F);
			palette[15].FromRGBA4($FFFF);
			case game of
				gid.AngelsEve3H: begin
					palette[1].FromRGBA4($05FF);
					palette[2].FromRGBA4($F03F);
					palette[3].FromRGBA4($A4FF);
					palette[4].FromRGBA4($6FBF);
					palette[5].FromRGBA4($5EFF);
					palette[6].FromRGBA4($FF5F);
					palette[7].FromRGBA4($F80F);
					palette[8].FromRGBA4($777F);
					palette[9].FromRGBA4($00AF);
					palette[10].FromRGBA4($930F);
					palette[11].FromRGBA4($F6DF);
					palette[12].FromRGBA4($0B5F);
					palette[13].FromRGBA4($FA9F);
					palette[14].FromRGBA4($FCBF);
				end;
				gid.AngelsEve5: begin
					palette[1].FromRGBA4($009F);
					palette[2].FromRGBA4($F00F);
					palette[3].FromRGBA4($C4FF);
					palette[4].FromRGBA4($2F2F);
					palette[5].FromRGBA4($0FFF);
					palette[6].FromRGBA4($FF0F);
					palette[7].FromRGBA4($4CDF);
					palette[8].FromRGBA4($777F);
					palette[9].FromRGBA4($00DF);
					palette[10].FromRGBA4($A00F);
					palette[11].FromRGBA4($F0EF);
					palette[12].FromRGBA4($171F);
					palette[13].FromRGBA4($FA9F);
					palette[14].FromRGBA4($FCBF);
				end;
				gid.AngelsSpecial2: begin
					palette[1].FromRGBA4($06EF);
					palette[2].FromRGBA4($A8FF);
					palette[3].FromRGBA4($0A9F);
					palette[4].FromRGBA4($788F);
					palette[5].FromRGBA4($0FFF);
					palette[6].FromRGBA4($FAFF);
					palette[7].FromRGBA4($FF4F);
					palette[8].FromRGBA4($E05F);
					palette[9].FromRGBA4($A50F);
					palette[10].FromRGBA4($F98F);
					palette[11].FromRGBA4($FBAF);
					palette[12].FromRGBA4($FDCF);
					palette[13].FromRGBA4($5D8F);
					palette[14].FromRGBA4($50AF);
				end;
			end;

			case bytesize of
				//3840: begin sizeXP := 240; sizeYP := 32; end; // commented out anim sizes, import those from bin file
				//5184: begin sizeXP := 144; sizeYP := 72; end;
				//6048: begin sizeXP := 216; sizeYP := 56; end;
				//6720: begin sizeXP := 240; sizeYP := 56; end;
				//6912: begin sizeXP := 288; sizeYP := 48; end;
				//7680: begin sizeXP := 192; sizeYP := 80; end;
				//8448: begin sizeXP := 192; sizeYP := 88; end;
				//8640: begin sizeXP := 240; sizeYP := 72; end;
				9632: begin sizeXP := 344; sizeYP := 56; end;
				11776: begin sizeXP := 368; sizeYP := 64; end;
				12288: begin sizeXP := 128; sizeYP := 192; end;
				13056: begin sizeXP := 408; sizeYP := 64; end;
				13824: begin sizeXP := 384; sizeYP := 72; end; // 3H a01
				//14112: begin sizeXP := 504; sizeYP := 56; end;
				15840: begin sizeXP := 360; sizeYP := 88; end; // special2 a04
				15872: begin sizeXP := 128; sizeYP := 248; end;
				16640: begin sizeXP := 520; sizeYP := 64; end;
				16896: begin sizeXP := 528; sizeYP := 64; end; // 3H a10
				17600: begin sizeXP := 400; sizeYP := 88; end;
				//18720: begin sizeXP := 312; sizeYP := 120; end;
				29568: begin sizeXP := 336; sizeYP := 176; end; // special2 a05
				29952: begin sizeXP := 288; sizeYP := 208; end; // special2 a01
				32640: begin sizeXP := 480; sizeYP := 136; end;
				34656: begin sizeXP := 456; sizeYP := 152; end;
				44352: begin sizeXP := 504; sizeYP := 176; end; // special2 a03
				46080: begin sizeXP := 360; sizeYP := 256; end; // 3H a07
				61248: begin sizeXP := 480; sizeYP := 168; end; // 3H a04
				67200: begin sizeXP := 480; sizeYP := 280; end;
				78144: begin sizeXP := 528; sizeYP := 296; end; // special2 a02
				128000: begin sizeXP := 640; sizeYP := 400; end;
				else begin
					// This could be an animation, try to find it in the game's scene bin file, already in memory.
					sizeXP := 0;
					s := basename;
					with gameConst do if (length(hardBytes) >= 2) and (length(hardBytes[0]) > 255) then begin
						for i := length(hardBytes[0]) - 8 downto 0 do
							if CompareByte(hardBytes[0][i], s[1], 3) = 0 then begin
								sizeXP := hardBytes[0][i + 3] shl 3;
								sizeYP := hardBytes[0][i + 4] shl 3;
								if (decomp_param.verbose) and (sizeXP * sizeYP <> bytesize shl 1) then
									decomp_logger('[!] bytesize mismatch');
								break;
							end;
					end;

					if sizeXP = 0 then raise DecompException.Create('sus bytesize ' + strdec(bytesize));
				end;
			end;
			stride := sizeXP shr 1;

			setlength(bitmap, bytesize);

			writep := @bitmap[0];
			endp := writep + bytesize;
			while writep < endp do begin
				dword(writep^) := ExplodeBits(bf.ReadByte)
					or (ExplodeBits(rf.ReadByte) shl 1)
					or (ExplodeBits(gf.ReadByte) shl 2)
					or (ExplodeBits(ef.ReadByte) shl 3);
				inc(writep, 4);
			end;

			metadata := CopyOrCreateGraphicFile(upcase(basename));
			with metadata do begin
				origSizeXP := sizeXP;
				origSizeYP := sizeYP;
				origFrameHeightP := sizeYP;
				frameCount := 0;
				srcFilePath := PathCombine([outdir, basename], 'png');
			end;
		end;
	end;

	procedure _LoadAllFiles;
	var i : dword;
		newpath : UTF8string;
		found : TStringBunch;
	begin
		// These images are split into four files, one for each bitplane. The blue file is currently in loader, need
		// to load the rest. If any file is missing, fail out.
		bf := loader; rf := NIL; gf := NIL; ef := NIL;
		// First figure out if these are compressed (.bc) or uncompressed (.b) files. All four should be the same.
		i := loader.fileName.Length;
		iscompressed := byte(loader.fileName[i]) or $40 = ord('c');
		if iscompressed then dec(i);

		newpath := loader.fileName;
		newpath[i] := 'r';
		found := FindFiles_caseless(newpath, FALSE, TRUE);
		if found.Length = 0 then raise DecompException(newpath + ' not found');
		rf := TFileLoader.Open(found[0]);

		newpath[i] := 'g';
		found := FindFiles_caseless(newpath, FALSE, TRUE);
		if found.Length = 0 then raise DecompException(newpath + ' not found');
		gf := TFileLoader.Open(found[0]);

		newpath[i] := 'e';
		found := FindFiles_caseless(newpath, FALSE, TRUE);
		if found.Length = 0 then raise DecompException(newpath + ' not found');
		ef := TFileLoader.Open(found[0]);
	end;

var buffy : pointer = NIL;
begin
	result := FALSE;
	if loader.fullFileSize < 500 then raise DecompException.Create('file too tiny');

	_LoadAllFiles;

	try
		if iscompressed then begin
			buffy := Expand_JASTRLE(bf.readp, bf.endp, bytesize);
			bf := TFileLoader.FromMemory(buffy, bytesize, TRUE); buffy := NIL;

			buffy := Expand_JASTRLE(rf.readp, rf.endp, bytesize);
			rf.Destroy; rf := NIL;
			rf := TFileLoader.FromMemory(buffy, bytesize, TRUE); buffy := NIL;

			buffy := Expand_JASTRLE(gf.readp, gf.endp, bytesize);
			gf.Destroy; gf := NIL;
			gf := TFileLoader.FromMemory(buffy, bytesize, TRUE); buffy := NIL;

			buffy := Expand_JASTRLE(ef.readp, ef.endp, bytesize);
			ef.Destroy; ef := NIL;
			ef := TFileLoader.FromMemory(buffy, bytesize, TRUE); buffy := NIL;
		end;

		{$ifdef enable_decomp_hacks}
		// Hack: Crop out garbage tails.
		if game = gid.AngelsEve3H then case basename of
			's18','s53','s56': begin
				if bf.fullFileSize > 16800 then bf.Crop(0, 16800);
				if rf.fullFileSize > 16800 then rf.Crop(0, 16800);
				if gf.fullFileSize > 16800 then gf.Crop(0, 16800);
				if ef.fullFileSize > 16800 then ef.Crop(0, 16800);
			end;
		end;
		{$endif}

		//decomp_logger(strcat('b %, r %, g %, e %', [bf.fullFileSize, rf.fullFileSize, gf.fullFileSize, ef.fullFileSize]));

		_LoadBits;

		SelectImageResolution(metadata, game);
		{$ifdef enable_decomp_hacks}
		ApplyGraphicsHacks(metadata, tempimage, game, high(dword));
		{$endif}

		tempimage.MakePNG(buffy, bytesize);
		SaveFile(metadata.srcFilePath, buffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if rf <> NIL then begin rf.Destroy; rf := NIL; end;
		if gf <> NIL then begin gf.Destroy; gf := NIL; end;
		if ef <> NIL then begin ef.Destroy; ef := NIL; end;
		if (bf <> NIL) and (bf <> loader) then begin bf.Destroy; bf := NIL; end;
	end;
	result := TRUE;
end;

