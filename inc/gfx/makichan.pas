{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_Makichan(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a Maki-chan or MAG graphic. Adds its metadata to graphicFiles[] and saves the
// bitmap in outdir/basename as a normal PNG.
// Throws an exception in case of errors.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	i, bytesize : dword;
	transparentindex : dword = high(dword);

	procedure _UnpackMaki(const loader : TFileLoader; subtype : byte);
	// Attempts to decompress a Maki v1 image from loader, and puts the result in tempimage and metadata.
	// Caller is responsible for freeing them. Subtype must be 1 for MAKI01A, or 2 for MAKI01B.
	// Throws an exception in case of errors, which may still return something in the result variables.
	var flagAmask : array[0..31999] of byte;
		outp, colorp : pointer;
		ofsa, ofsb, extflag : dword;
		x, y : dword;
	begin
		inc(loader.readp, 4); // computer model, skip
		inc(loader.readp, 20); // user name etc, skip

		//flagbsize := BEtoN(loader.ReadWord);
		//pxasize := BEtoN(loader.ReadWord);
		//pxbsize := BEtoN(loader.ReadWord);
		inc(loader.readp, 6);
		extflag := BEtoN(loader.ReadWord);
		if extflag <> 0 then raise DecompException.Create('extflag <> 0');

		with metadata do begin
			origOfsXP := BEtoN(loader.ReadWord);
			origOfsYP := BEtoN(loader.ReadWord);
			origSizeXP := BEtoN(loader.ReadWord);
			origSizeYP := BEtoN(loader.ReadWord);

			if origOfsXP <> 0 then raise DecompException.Create('ofsx <> 0');
			if origOfsYP <> 0 then raise DecompException.Create('ofsy <> 0');
			if origSizeXP <> 640 then raise DecompException.Create('sizex <> 640');
			if origSizeYP <> 400 then raise DecompException.Create('sizey <> 400');

			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_INDEXED, 4);

			// Read GRB palette.
			with tempimage do begin
				setlength(palette, 16);
				for x := 0 to 15 do with palette[x] do begin
					g := loader.ReadByte and $F0;
					r := loader.ReadByte and $F0;
					b := loader.ReadByte and $F0;
					a := $FF;
					// Only the top nibble is significant; the bottom nibble is 0 if the top is 0, else it is $F.
					if g <> 0 then g := g or $F;
					if r <> 0 then r := r or $F;
					if b <> 0 then b := b or $F;
				end;
			end;
		end;

		// First construct the flag A alpha mask (320x400).
		// We'll use the low nibbles of each byte to put 4 bits in each byte.
		// Each flag A bit sets 4x4 mask bits.
		ofsa := 0;
		ofsb := loader.ofs + 1000;
		loader.bitSelect := 0;

		for y := 99 downto 0 do begin
			for x := 79 downto 0 do begin
				if loader.ReadBit then begin
					// Flag A is true, set 4x4 block using the next flag B word.
					flagAmask[ofsa + 000] := loader.ReadByteFrom(ofsb) shr 4;
					flagAmask[ofsa + 080] := loader.ReadByteFrom(ofsb) and $F;
					inc(ofsb);
					flagAmask[ofsa + 160] := loader.ReadByteFrom(ofsb) shr 4;
					flagAmask[ofsa + 240] := loader.ReadByteFrom(ofsb) and $F;
					inc(ofsb);
				end
				else begin
					// Flag A is false, clear 4x4 block.
					flagAmask[ofsa + 000] := 0;
					flagAmask[ofsa + 080] := 0;
					flagAmask[ofsa + 160] := 0;
					flagAmask[ofsa + 240] := 0;
				end;
				inc(ofsa);
			end;
			// Jump to the next 4x4 row's start.
			inc(ofsa, 240);
		end;

		{$ifdef bonk}
		// If you want to render the alpha mask, use this...
		with tempimage do begin
			sizeXP := sizeXP shr 1;
			bitDepth := 1;
			for x := 0 to 15999 do
				bitmap[x] := (flagAmask[x * 2] shl 4) or flagAmask[x * 2 + 1];
			palette[0].r := 0; palette[0].g := 0; palette[0].b := 0;
			palette[1].r := $DD; palette[1].g := $44; palette[1].b := $66;
		end;
		metadata.origSizeXP := tempimage.sizeXP;
		// Expand 1bpp indexed --> 8bpp indexed.
		tempimage.ExpandBitdepth;
		exit;
		{$endif}

		// Fill in the pixel colors...
		colorp := loader.PtrAt(ofsb);
		outp := @tempimage.bitmap[0];

		for ofsa := 0 to 31999 do begin

			// For each bit in the flag A buffer...
			// if the bit is 0, output a 0 byte;
			// if the bit is 1, output a byte from the pixel color stream.
			if flagAmask[ofsa] and 8 = 0 then
				byte(outp^) := 0
			else begin
				byte(outp^) := byte(colorp^); inc(colorp);
			end;
			inc(outp);

			if flagAmask[ofsa] and 4 = 0 then
				byte(outp^) := 0
			else begin
				byte(outp^) := byte(colorp^); inc(colorp);
			end;
			inc(outp);

			if flagAmask[ofsa] and 2 = 0 then
				byte(outp^) := 0
			else begin
				byte(outp^) := byte(colorp^); inc(colorp);
			end;
			inc(outp);

			if flagAmask[ofsa] and 1 = 0 then
				byte(outp^) := 0
			else begin
				byte(outp^) := byte(colorp^); inc(colorp);
			end;
			inc(outp);

		end;

		// Apply the vertical XOR filter.
		// (each row is 640 4-bit pixels, so 320 bytes)
		// MAKI01A xors from two rows above;
		// MAKI01B xors from four rows above.
		if subtype = 2 then
			ofsa := 320 * 4
		else
			ofsa := 320 * 2;
		outp := @tempimage.bitmap[ofsa];

		for ofsb := (128000 - ofsa) div 4 - 1 downto 0 do begin
			dword(outp^) := dword(outp^) xor dword((outp - ofsa)^);
			inc(outp, 4);
		end;
	end;

	procedure _UnpackMAG(const loader : TFileLoader);
	// Attempts to decompress a MAG v2 image from loader, and puts the result in tempimage and metadata.
	// Caller is responsible for freeing them.
	// Also works on MAX images, which are MSX-flavored MAG v2, and Fairy Dust's SGV.
	// Throws an exception in case of errors, which may still return something in the result variables.
	var actionbuffy : array of byte = NIL;
		header : record
			modelString : dword;
			modelCode, modelFlags, screenMode : byte;
			left, top, right, bottom : dword;
			flagAofs, flagBofs, colorOfs : dword;
		end;
		paddedleft, paddedright : dword;
		cropleft, cropright : dword;
		bytewidth : dword;
		isSGV : boolean = FALSE;

	const
		delx : array[0..15] of byte = (0,1,2,4,0,1,0,1,2,0,1,2,0,1,2,0);
		dely : array[0..15] of byte = (0,0,0,0,1,1,2,2,2,4,4,4,8,8,8,16);

		procedure _DoDecompress;
		// The straightforward main MAG v2 decompressing loop.
		var outp, endp : pointer;
			actionindex, flagbindex, colorindex : dword;

			procedure _GrabWord(action : byte);
			// Decompression helper, saves a word of output.
			var w : word;
			begin
				if action = 0 then begin
					// Fetch a new word from the color index stream.
					if colorindex + 1 >= loader.buffySize then begin
						LogError(strcat('color array read oob: outofs=%/% colorindex=%',
							[outp - @tempimage.bitmap[0], bytewidth * tempimage.sizeYP, colorindex]));
						w := 0;
					end
					else begin
						w := LEtoN(loader.ReadWordFrom(colorindex));
						inc(colorindex, 2);
					end;
				end
				else begin
					// Copy a previously output word.
					w := word((outp - (dely[action] * bytewidth) - delx[action] * 2)^);
				end;
				// Output the word.
				word(outp^) := w;
				inc(outp, 2);
			end;

		begin
			loader.ofs := header.flagAofs;
			loader.bitSelect := 0;
			actionindex := 0;
			flagbindex := header.flagBofs;
			colorindex := header.colorOfs;
			outp := @tempimage.bitmap[0];
			endp := outp + bytewidth * tempimage.sizeYP - 1;

			{$ifdef enable_decomp_hacks}
			// Hack: Last row is corrupted, stop early. This is fixed in postproc.
			if (game = gid.Ukiuki) and (metadata.graphicName = 'EVW_94') then begin
				dword((endp - 3)^) := 0;
				dec(endp, bytewidth);
			end;
			{$endif}

			// Decompress until the output buffer is full...
			repeat

				// Read the next flag A bit.
				if loader.ofs >= header.flagBofs then
					raise DecompException.Create('flag A read oob');

				if loader.ReadBit then begin
					// If the flag A bit is set, read the next flag B byte, and xor the current action byte with it.
					if flagbindex >= header.colorOfs then
						raise DecompException.Create('flag B read oob');

					actionbuffy[actionindex] := actionbuffy[actionindex] xor loader.ReadByteFrom(flagbindex);
					inc(flagbindex);
				end;

				// Act on the top action nibble.
				_GrabWord(actionbuffy[actionindex] shr 4);
				if outp >= endp then break;

				// Act on the bottom action nibble.
				_GrabWord(actionbuffy[actionindex] and $F);
				if outp >= endp then break;

				// Advance the action buffer pointer, looping around the action buffer.
				actionindex := (actionindex + 1) mod dword(length(actionbuffy));

			until FALSE;
		end;

		procedure _ConvertYJK(hasrgb : boolean);
		// Converts tempimage^ from YJK-encoding to 24-bit RGB.
		var workimage : array of byte = NIL;
			readp, writep : pointer;
			loopx, loopy, readval : dword;
			Y, J, K, outval : longint;
			rowJ : array of longint = NIL;
			rowK : array of longint = NIL;
		begin
			with tempimage do begin
				setlength(workimage, sizeXP * sizeYP * 3);

				readp := @bitmap[0];
				writep := @workimage[0];
				setlength(rowJ, bytewidth);
				setlength(rowK, bytewidth);

				// For all rows in the image...
				for loopy := sizeYP - 1 downto 0 do begin

					// Extract the J and K values for this row.
					loopx := bytewidth;
					while loopx <> 0 do begin
						dec(loopx, 4);
						readval := dword(readp^); inc(readp, 4);
						// Grab J from low 3 bits of bytes 2 and 3.
						J := ((readval shr 16) and 7) or (((readval shr 24) and 7) shl 3);
						// Grab K from low 3 bits of bytes 0 and 1.
						K := (readval and 7) or (((readval shr 8) and 7) shl 3);

						// Scale from unsigned 6-bit to unsigned 9-bit.
						J := (J * 511 + 31) div 63;
						K := (K * 511 + 31) div 63;
						// These are supposed to be signed 9-bit, so apply the sign.
						dec(J, (J and $100) * 2);
						dec(K, (K and $100) * 2);

						// Save J and K.
						filldword(rowJ[loopx], 4, dword(J));
						filldword(rowK[loopx], 4, dword(K));
					end;
					dec(readp, bytewidth);

					// Smooth the J and K rows using linear interpolation.
					// This is optional, and reduces blocky color fringing noticeably. However, it can also produce new
					// artifacts over saturated gradients. To avoid that, each pixel would need to retain its luma
					// calculated with non-smoothed J and K, while only importing chroma and saturation from the smooth
					// values...This is less necessary when indexed-color pixels are included.
					if NOT hasrgb then begin
						loopx := 2;
						while loopx + 4 < bytewidth do begin
							J := rowJ[loopx];
							Y := rowJ[loopx + 3];
							rowJ[loopx + 0] := (J * 7 + Y    ) div 8;
							rowJ[loopx + 1] := (J * 5 + Y * 3) div 8;
							rowJ[loopx + 2] := (J * 3 + Y * 5) div 8;
							rowJ[loopx + 3] := (J     + Y * 7) div 8;
							K := rowK[loopx];
							Y := rowK[loopx + 3];
							rowK[loopx + 0] := (K * 7 + Y    ) div 8;
							rowK[loopx + 1] := (K * 5 + Y * 3) div 8;
							rowK[loopx + 2] := (K * 3 + Y * 5) div 8;
							rowK[loopx + 3] := (K     + Y * 7) div 8;
							inc(loopx, 4);
						end;
					end;

					// Extract and apply the Y values for this row.
					for loopx := bytewidth - 1 downto 0 do begin
						// Grab Y for this pixel, a 5-bit value.
						Y := byte(readp^) shr 3;
						inc(readp);

						if (hasrgb) and (Y and 1 <> 0) then begin
							// Straight RGB!
							Y := Y shr 1;
							byte(writep^) := palette[Y].b; inc(writep);
							byte(writep^) := palette[Y].g; inc(writep);
							byte(writep^) := palette[Y].r; inc(writep);
						end
						else begin
							// Scale Y from unsigned 5-bit to unsigned 8-bit.
							Y := (Y * 255 + 15) div 31;
							J := rowJ[loopx];
							K := rowK[loopx];
							// BLUE = (5/4 * Y) - (2/4 * J) - (1/4 * K)
							outval := (5 * Y - 2 * J - K) div 4;
							if outval < 0 then
								outval := 0
							else
								if outval > 255 then outval := 255;
							byte(writep^) := outval;
							inc(writep);
							// GREEN = Y + K
							outval := Y + K;
							if outval < 0 then
								outval := 0
							else
								if outval > 255 then outval := 255;
							byte(writep^) := outval;
							inc(writep);
							// RED = Y + J
							outval := Y + J;
							if outval < 0 then
								outval := 0
							else
								if outval > 255 then outval := 255;
							byte(writep^) := outval;
							inc(writep);
						end;
					end;
				end;

				setlength(bitmap, 0); bitmap := workimage; workimage := NIL;

				// It's no longer an indexed-color image, release the palette.
				setlength(palette, 0);
				bitmapFormat := MCG_FORMAT_BGR;
				bitDepth := 24;

				// If the image used to be 4bpp, the conversion halved its pixel width.
				if (header.screenMode and $80 = 0) then
					sizeXP := sizeXP shr 1;
				stride := sizeXP * 3;
			end;
		end;

		procedure _ReadSGVPal;
		// For SGV images, the palette is kept in a separate pal.dat file.
		var basepath, palpath : UTF8string;
			f : TFileLoader;
			sb : TStringBunch;
			l, palofs : dword;
		begin
			palofs := loader.ReadByte * 48;
			basepath := ExtractFileDir(loader.fileName);
			palpath := PathCombine([basepath, 'pal.dat']);
			sb := FindFiles_caseless(palpath, FALSE, TRUE);
			if sb = NIL then begin
				palpath := PathCombine([ExtractFileDir(basepath), 'pal.dat']);
				sb := FindFiles_caseless(palpath, FALSE, TRUE);
			end;
			if sb = NIL then begin
				decomp_logger('[!] pal.dat not found');
				SetupPalette(tempimage, 16); // use a fake palette
				exit;
			end;
			f := TFileLoader.Open(sb[0]);
			try
				if palofs + 48 > f.fullFileSize then raise DecompException.Create('pal ofs oob: ' + strdec(palofs));
				f.ofs := palofs;
				setlength(tempimage.palette, 16);
				for l := 0 to 15 do with tempimage.palette[l] do begin
					g := (f.ReadByte shr 4) * 17;
					r := (f.ReadByte shr 4) * 17;
					b := (f.ReadByte shr 4) * 17;
					a := $FF;
				end;
				with tempimage do if transparentindex < dword(length(palette)) then begin
					palette[transparentindex].a := 0;
					bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
				end;
			finally
				f.Destroy;
			end;
		end;

		procedure _ReadPalette;
		// Reads and converts the palette at the end of the header.
		var palindex : dword;
			palettedepth, palettemask : byte;
		begin
			with tempimage do begin
				// Let's read from current position up to the start of the flag A stream.
				setlength(palette, (header.flagAofs - loader.ofs) div 3);
				if length(palette) > 256 then
					raise DecompException.Create('too long palette: ' + strdec(length(palette)));

				// Note the number of significant bits in each palette byte, depending on the computer model this image
				// was saved on. The default for PC98 and X1 is 4 bits per channel.
				palettedepth := 4;
				case header.modelCode of
					// MSXes can display thousands of colors with a special encoding, but their palette is only 9-bit.
					$03: palettedepth := 3;
					// X68000 has 16-bit graphics, but for purposes of palettisation, ignore the single intensity bit.
					$68: palettedepth := 5;
					// The Macintosh II has a full 24-bit palette.
					$99: palettedepth := 8;
				end;

				// Exception: an MSX using the Deca Loader can fake more bits.
				if (header.modelCode = $03) and (LEtoN(loader.ReadDwordFrom($20)) = $61636544) then palettedepth := 8;

				// 256-color images will generally use the full 8 bits, except for MSXes and late PC88's, whose total
				// palette is still limited.
				if (length(palette) = 256) and (NOT (header.modelCode in [$03,$88])) then palettedepth := 8;

				palettemask := (1 shl palettedepth - 1) shl (8 - palettedepth);

				if length(palette) <> 0 then begin
					for palindex := 0 to length(palette) - 1 do
						with palette[palindex] do begin
							// Read the palette bytes and mirror the significant bits.
							g := loader.ReadByte and palettemask;
							r := loader.ReadByte and palettemask;
							b := loader.ReadByte and palettemask;
							a := $FF;
							inc(g, g shr palettedepth + g shr (palettedepth + palettedepth));
							inc(r, r shr palettedepth + r shr (palettedepth + palettedepth));
							inc(b, b shr palettedepth + b shr (palettedepth + palettedepth));
						end;
				end;
			end;
		end;

	begin
		header.modelString := 0; // silence compiler
		fillbyte(header, sizeof(header), 0);
		loader.ofs := 0;
		// Normally these files have the "MAKI02  " sig, followed by model code and comments section.
		if BEtoN(dword(loader.readp^)) = $4D414B49 then begin
			// Many files start with a 4-letter computer model string, let's save it.
			// If the actual model code is left empty, this may be used instead.
			loader.ofs := 8;
			header.modelString := LEtoN(dword(loader.readp^));

			// Username, comments, etc, $1A, skip.
			repeat
				if (loader.readp + 32 >= loader.endp) or (loader.ofs > $200) then begin
					loader.ofs := 0;
					raise DecompException.Create('comment block $1A not found');
				end;
			until loader.ReadByte = $1A;

			// The header starts after the first 0 after the 1A.
			repeat
				if (loader.readp + 32 >= loader.endp) or (loader.ofs > $200) then begin
					loader.ofs := 0;
					raise DecompException.Create('header start 0 not found');
				end;
			until loader.ReadByte = 0;

			// Remember the beginning of the header for use below.
			i := loader.ofs - 1;
		end

		// Fairy Dust's SGV files start with "SGV".
		else if BEtoN(dword(loader.readp^)) shr 8 = $534756 then begin
			inc(loader.readp, 3);
			// SGVP has transparency, SGVF and SGVW don't.
			i := loader.ReadByte;
			if (i = ord('P')) or (metadata.graphicName = 'EV_12B') then transparentindex := 2;
			isSGV := TRUE;
			i := 0;
		end

		// The whole sig, model code, and comments may be omitted. The header must still start with 0.
		else if loader.ReadByte <> 0 then raise DecompException.Create('no sig, and no 0 at start');

		// Read the header.
		with header do begin
			if NOT isSGV then begin
				modelCode := loader.ReadByte;
				modelFlags := loader.ReadByte;
				screenMode := loader.ReadByte;

				if screenMode and 120 <> 0 then
					raise DecompException.Create('bad screen mode $' + strhex(header.screenMode));

				// If the model code is empty, but the model string looks familiar, use it.
				if modelCode = 0 then case modelString of
					$4B383658: modelCode := $68; // "X68K"
					$20454758: modelCode := $68; // "XGE ", maybe also a 68k?
					$54535058: modelCode := $68; // "XPST"
					$41563838: modelCode := $88; // "88VA"
					$32464954: modelCode := $99; // "TIF2", probably related to Macs?
					$46464954: modelCode := $99; // "TIFF", probably related to Macs?
					$20504D42: modelCode := $99; // "BMP ", probably related to Macs?
					$4E574F54: ; // "TOWN", FM-Towns
				end;
			end;

			// Read the header further: edge coordinates.
			inc(left, LEtoN(loader.ReadWord));
			inc(top, LEtoN(loader.ReadWord));
			inc(right, LEtoN(loader.ReadWord));
			inc(bottom, LEtoN(loader.ReadWord));

			if (left > right) or (top > bottom) then
				raise DecompException.Create('bad edge coordinates');

			if (right > 1600) or (bottom > 1600) then
				raise DecompException.Create(strcat('sus image size %,%..%,%', [left, top, right, bottom]));

			// Read the header further: section offsets.
			{$push}{$R-} // in case of bogus maxuint values, let it fail further on...
			flagAofs := i + LEtoN(loader.ReadDword);
			flagBofs := i + LEtoN(loader.ReadDword);
			inc(loader.readp, 4); // skip flag B stream size, unnecessary
			colorOfs := i + LEtoN(loader.ReadDword);
			{$pop}
			inc(loader.readp, 4); // skip color index stream size, unnecessary

			if (flagAofs >= flagBofs) or (flagBofs >= colorOfs) or (colorOfs >= loader.buffySize) then
				raise DecompException.Create(strcat('bad section offset, A=$&, B=$&, C=$&, filesize=%',
					[longint(flagAofs), longint(flagBofs), longint(colorOfs), loader.buffySize]));
		end;

		tempimage := mcg_bitmap.Create;
		with tempimage do with metadata do begin
			bitmap := NIL;
			palette := NIL;
			bitmapFormat := MCG_FORMAT_INDEXED;

			// Read GRB palette, usually 16, sometimes up to 256 entries.
			if NOT isSGV then
				_ReadPalette
			else
				_ReadSGVPal;

			if (header.screenMode and $80 = 0) then
				bitDepth := 4
			else
				bitDepth := 8;

			// Calculate pixels per byte. (at 8bpp = 1 pixel; at 4bpp = 2 pixels)
			i := 8 div bitDepth;

			// Calculate the byte location of the left and right edges.
			paddedleft := header.left div i;
			paddedright := header.right div i;

			// Pad the edges to a multiple of 4 bytes.
			// And convert the right edge to exclusive instead of inclusive.
			paddedleft := paddedleft and $FFFC;
			paddedright := (paddedright + 1 + 3) and $FFFC;
			bytewidth := paddedright - paddedleft;

			// Prepare tempimage. The decompressed padded output goes in tempimage.bitmap[].
			sizeXP := bytewidth * i;
			sizeYP := header.bottom - header.top + 1;
			stride := (sizeXP * bitDepth) shr 3;

			// Calculate how many pixels of padding are being added. This will be used later to delete the padding.
			cropleft := header.left - paddedleft * i;
			cropright := sizeXP - (header.right - header.left + 1) - cropleft;

			// Set up actionbuffy for one row of flag B bytes.
			setlength(actionbuffy, bytewidth div 4);
			fillbyte(actionbuffy[0], length(actionbuffy), 0);

			// Unpack the image into the output buffy.
			setlength(bitmap, bytewidth * sizeYP);
			_DoDecompress;

			// If this is an MSX2+ screen, we may need to decode the YJK colors.
			// This sets tempimage to be 24-bit RGB, no longer indexed.
			if (header.modelCode = 3) and (header.modelFlags in [$24, $34, $44]) then
				_ConvertYJK(header.modelFlags <> $44);

			// If still using a 4bpp indexed palette, expand to 8bpp.
			if bitDepth < 8 then Force8bpp;

			origOfsXP := header.left;
			origOfsYP := header.top;

			// Remove the left/right padding, if any.
			tempimage.Crop(cropleft, cropright, 0, 0);

			origSizeXP := sizeXP;
			origSizeYP := sizeYP;
			origFrameHeightP := sizeYP;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < $40 then raise DecompException.Create('file too tiny');

	metadata := CopyOrCreateGraphicFile(upcase(basename));
	metadata.srcFilePath := PathCombine([outdir, basename], 'png');

	try
		// If the file starts with a "MAKI" signature, it can be sent to the right decompressor immediately.
		i := BEtoN(loader.ReadDword);
		if i = $4D414B49 then begin
			case LEtoN(loader.ReadDword) of
				$20413130: _UnpackMaki(loader, 1); // 01A
				$20423130: _UnpackMaki(loader, 2); // 01B
				$20203230: _UnpackMAG(loader); // 02
				else raise DecompException.Create('unknown MAKI subtype $' + strhex(i));
			end;
		end
		// Without the signature, it could still be valid with a reduced header. Try MAG...
		else _UnpackMAG(loader);

		//transparentindex := SelectTransparentIndex(CN_PI, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

