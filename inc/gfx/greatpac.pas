{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_GreatPAC(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a Great PAC graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	buffy : pointer = NIL;
	bytesize, transparentindex : dword;

	procedure _UnpackColumns(const src : TFileLoader);
	var writep, endp : ^byte;
		cmd, reps, plane : byte;
	begin
		writep := buffy;
		endp := buffy + (metadata.origSizeXP shr 1) * metadata.origSizeYP;
		while (src.readp < src.endp) and (writep < endp) do begin
			cmd := src.ReadByte;
			reps := cmd and $1F;
			case (cmd and $E0) of
				$00: // literals
				begin
					move(src.readp^, writep^, reps);
					inc(src.readp, reps);
					inc(writep, reps);
				end;

				$20: // repeat byte
				begin
					fillbyte(writep^, reps, src.ReadByte);
					inc(writep, reps);
				end;

				$40: // repeat word
				begin
					fillword(writep^, reps, src.ReadWord);
					inc(writep, reps * 2);
				end;

				$60: // repeat dword
				begin
					filldword(writep^, reps, src.ReadDword);
					inc(writep, reps * 4);
				end;

				$80: // copy from first plane
				begin
					plane := ((writep - buffy) div metadata.origSizeYP) and 3;
					move((writep - metadata.origSizeYP * plane)^, writep^, reps);
					inc(writep, reps);
				end;

				$A0: // copy from second plane
				begin
					plane := ((writep - buffy) div metadata.origSizeYP - 1) and 3;
					move((writep - metadata.origSizeYP * plane)^, writep^, reps);
					inc(writep, reps);
				end;

				$C0: // repeat 00
				begin
					fillbyte(writep^, reps, 0);
					inc(writep, reps);
				end;

				$E0: // repeat FF
				begin
					fillbyte(writep^, reps, $FF);
					inc(writep, reps);
				end;
			end;
		end;
	end;

	procedure _FinaliseImage;
	var readp : ^byte;
		writep : pointer;
		numsrcbytes, x, y : dword;
	begin
		with tempimage do begin
			numsrcbytes := (stride * sizeYP) shr 2;
			writep := @bitmap[0];
			readp := buffy;

			x := 0; y := sizeYP;
			while numsrcbytes <> 0 do begin
				dec(numsrcbytes);
				dword(writep^) := ExplodeBits(readp^) + ExplodeBits((readp + sizeYP)^) shl 1
					+ ExplodeBits((readp + sizeYP * 2)^) shl 2 + ExplodeBits((readp + sizeYP * 3)^) shl 3;
				inc(readp);

				dec(y);
				if y <> 0 then
					inc(writep, sizeXP shr 1)
				else begin
					y := sizeYP;
					inc(x, 4);
					writep := @bitmap[0] + x; // not @bitmap[x], goes out of range if image height 1
					inc(readp, sizeYP * 3);
				end;
			end;
		end;
	end;

	procedure _LoadBitsFrom(const src : TFileLoader);
	var w, h, i, j : dword;
		pal : TSRGBPalette = NIL;
	begin
		if word(src.readp^) <> 0 then raise DecompException.Create('expected initial 0000');

		setlength(pal, 16);
		for i := 0 to 15 do begin
			j := src.ReadByte shl 16;
			inc(j, src.ReadByte shl 8);
			inc(j, src.ReadByte);
			if j and $F0F0F0 <> 0 then
				raise DecompException.Create('high nibble set in palette: $' + strhex(j));
			dword(pal[i]) := j + j shl 4 + $FF000000;
		end;
		i := src.ReadByte;
		if NOT (i in [0,6]) then raise DecompException.Create('expected 00 after pal, got $' + strhex(i));
		i := src.ReadByte;
		if i <> 0 then raise DecompException.Create('expected 00 after pal+1, got $' + strhex(i));

		inc(src.readp);
		w := src.ReadByte * 8;
		h := BEtoN(src.ReadWord);
		if (w > 640) or (h > 400) or (w = 0) or (h = 0) then
			raise DecompException.Create(strcat('sus image size %x%', [w, h]));

		tempimage := mcg_bitmap.Init(w, h, MCG_FORMAT_INDEXED, 4);
		tempimage.palette := pal;
		pal := NIL;
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			srcFilePath := PathCombine([outdir, basename], 'png');
			origSizeXP := w;
			origSizeYP := h;
			origOfsXP := 0;
			origOfsYP := 0;
			getmem(buffy, (origSizeXP shr 1) * origSizeYP);
		end;

		// The compressed image is saved as 8-bit columns. Every group of 4 columns is merged into an 8px 4bpp column.
		_UnpackColumns(src);
		_FinaliseImage;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 60 then raise DecompException.Create('file too tiny');

	try
		_LoadBitsFrom(loader);
		freemem(buffy); buffy := NIL;

		transparentindex := SelectTransparentIndex(CN_GREATPAC, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(buffy, bytesize);
		SaveFile(metadata.srcFilePath, buffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
	end;
	result := TRUE;
end;

