{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_OZM(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an OZM graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	plane : array[0..3] of pointer;
	buffy : pointer = NIL;
	transparentindex, bytesize : dword;

	procedure _ReadColors(readp : pointer; count : byte);
	var c : byte;
	begin
		setlength(tempimage.palette, count);
		with tempimage do for c := 0 to count - 1 do with palette[c] do begin
			r := byte(readp^); inc(readp);
			g := byte(readp^); inc(readp);
			b := byte(readp^); inc(readp);
			a := $FF;
			// The palette only has low nibbles, so extend to full byte.
			dword((@palette[c])^) := dword((@palette[c])^) or (dword((@palette[c])^) shl 4);
		end;
	end;

	procedure _GetPalette;
	// The palette is stored in an RGB file, sometimes with the same name as the graphic, often not.
	var rgbfile : TFileLoader = NIL;
		filename : UTF8string = '';
		fnbunch : TStringBunch;
	begin
		with metadata do begin
			if copy(graphicName, 1, 4) = 'ROGO' then
				filename := 'rogo'
			else
				filename := graphicName;
			filename := PathCombine([ExtractFilePath(loader.fileName), filename + '.rgb']);
			fnbunch := FindFiles_caseless(filename, FALSE, TRUE);
			if fnbunch = NIL then begin
				// Default fallback.
				case game of
					gid.DennouGarou: filename := 'tittle1.rgb';
					gid.Lakers1: filename := 'b-rgb.rgb';
				end;
				filename := PathCombine([ExtractFilePath(loader.fileName), filename]);
				fnbunch := FindFiles_caseless(filename, FALSE, TRUE);
				if fnbunch = NIL then begin
					LogError('no pal for ' + graphicName);
					exit;
				end;
			end;
			filename := fnbunch[0]
		end;
		rgbfile := TFileLoader.Open(filename);
		try
			_ReadColors(rgbfile.readp, 16);
		finally
			rgbfile.Destroy;
		end;
	end;

	procedure _UnpackColumns(const src : TFileLoader);
	var writep, endp : pointer;
		reps, w : word;
		cmd, b : byte;
	begin
		writep := buffy;
		endp := buffy + (metadata.origSizeXP shr 1) * metadata.origSizeYP;
		while (src.readp < src.endp) and (writep < endp) do begin
			cmd := src.ReadByte;
			if cmd <> 0 then begin
				// Literal.
				byte(writep^) := cmd; inc(writep);
			end
			else begin
				// Special command.
				cmd := src.ReadByte;
				case cmd of
					1: begin // output one 0 byte
						byte(writep^) := 0; inc(writep);
					end;
					2: begin // output two 0 bytes
						word(writep^) := 0; inc(writep, 2);
					end;
					3: begin // output three 0 bytes
						word(writep^) := 0; inc(writep, 2);
						byte(writep^) := 0; inc(writep);
					end;
					4: begin // output four 0 bytes
						dword(writep^) := 0; inc(writep, 4);
					end;
					5: begin // output five 0 bytes
						dword(writep^) := 0; inc(writep, 4);
						byte(writep^) := 0; inc(writep);
					end;
					6: begin // output b for xxxx bytes
						b := src.ReadByte;
						reps := LEtoN(src.ReadWord);
						fillbyte(writep^, reps, b);
						inc(writep, reps);
					end;
					7: begin // output ww for xxxx bytes
						w := src.ReadWord;
						reps := LEtoN(src.ReadWord);
						if reps > 1 then begin
							fillword(writep^, reps shr 1, w);
							inc(writep, reps and $FFFE);
						end;
						if reps and 1 <> 0 then begin
							byte(writep^) := byte(LEtoN(w));
							inc(writep);
						end;
					end;
					else begin
						LogError(strcat('UNK CODE $00-& srcofs $&', [cmd, src.ofs - 1]));
						break;
					end;
				end;
			end;
		end;
	end;

	procedure _FinaliseImage;
	var writep : pointer;
		numsrcbytes, x, y : dword;
	begin
		with tempimage do begin
			numsrcbytes := (stride * sizeYP) shr 2;
			writep := @bitmap[0];

			x := 0; y := sizeYP;
			while numsrcbytes <> 0 do begin
				dec(numsrcbytes);
				dword(writep^) := ExplodeBits(byte(plane[0]^)) + ExplodeBits(byte(plane[1]^)) shl 1
					+ ExplodeBits(byte(plane[2]^)) shl 2 + ExplodeBits(byte(plane[3]^)) shl 3;
				inc(plane[0]); inc(plane[1]); inc(plane[2]); inc(plane[3]);

				dec(y);
				if y <> 0 then
					inc(writep, stride)
				else begin
					y := sizeYP;
					inc(x, 4);
					writep := @bitmap[0] + x; // not @bitmap[x], goes out of range if image height 1
				end;
			end;
		end;
	end;

	procedure _LoadBitsFrom(const src : TFileLoader);
	type THeader = packed record
		ver : word;
		width, height : word;
		startofs : word;
		flag : byte;
		pal : array[0..47] of byte;
		endmark : byte;
	end;
	var header : ^THeader;
		i, j : dword;
	begin
		header := src.readp;
		i := BEtoN(header^.ver);
		if (i <> $200) and (i <> $300) and (i <> $301) then raise DecompException.Create('bad version $' + strhex(i));

		metadata := CopyOrCreateGraphicFile(upcase(basename));
		metadata.srcFilePath := PathCombine([outdir, basename], 'png');

		with metadata do begin
			origSizeXP := LEtoN(header^.width);
			origSizeYP := LEtoN(header^.height);

			if (origSizeXP > 80) or (origSizeYP > 400) or (origSizeXP = 0) or (origSizeYP = 0) then
				raise DecompException.Create(strcat('sus image size %x%', [origSizeXP, origSizeYP]));
			origSizeXP := origSizeXP * 8;

			with header^ do begin
				if (ver and $F = 3) and (endmark <> $F) then
					raise DecompException.Create('bad endmark $' + strhex(endmark));

				startofs := LEtoN(startofs);
				if (startofs < $39) or (startofs > $3A) then
					raise DecompException.Create('bad startofs $' + strhex(startofs));
				src.ofs := startofs;

				if flag > 1 then raise DecompException.Create('bad flag $' + strhex(flag));

				tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_INDEXED, 4);

				if flag = 0 then
					_GetPalette
				else
					_ReadColors(@pal[0], 16);
			end;

			i := (origSizeXP shr 1) * origSizeYP;
			getmem(buffy, i);
			fillbyte(buffy^, i, 0);
			i := i shr 2;
			plane[0] := buffy;
			for j := 1 to 3 do plane[j] := plane[j - 1] + i;
		end;

		// The compressed image is saved one bitplane at a time, each bitplane as 8-bit columns.
		_UnpackColumns(src);
		// Merge the bitplanes into a 4bpp image.
		_FinaliseImage;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 500 then raise DecompException.Create('file too tiny');

	try
		_LoadBitsFrom(loader);
		freemem(buffy); buffy := NIL;

		transparentindex := SelectTransparentIndex(CN_OZM, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(buffy, bytesize);
		SaveFile(metadata.srcFilePath, buffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_OLH(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an OLH graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	plane : array[0..3] of pointer;
	buffy : pointer = NIL;
	metaname : UTF8string;
	frameindex, planesize : dword;

	procedure _UnpackColumns(const src : TFileLoader);
	var writep, endp : pointer;
		reps, w : word;
		cmd, b : byte;
	type EPlaneAct = (PA_OR, PA_AND, PA_COPY, PA_NEG);

		procedure _FromPlane(n : byte; action : EPlaneAct);
		var dist : dword;
			currentplane : byte;
		begin
			currentplane := dword(writep - buffy) div planesize;
			if currentplane <= n then raise DecompException.Create(strcat('bad src plane % at $&', [n, src.ofs - 1]));
			dist := byte(currentplane - n) * planesize;
			case action of
				PA_OR: while reps <> 0 do begin
					byte(writep^) := byte(writep^) or byte((writep - dist)^);
					inc(writep);
					dec(reps);
				end;
				PA_AND: while reps <> 0 do begin
					byte(writep^) := byte(writep^) and byte((writep - dist)^);
					inc(writep);
					dec(reps);
				end;
				PA_COPY: MemCopy(writep - dist, writep, reps);
				PA_NEG: while reps <> 0 do begin
					byte(writep^) := byte((writep - dist)^) xor $FF;
					inc(writep);
					dec(reps);
				end;
			end;
		end;

	begin
		writep := buffy;
		endp := buffy + tempimage.stride * tempimage.sizeYP;
		while (src.readp < src.endp) and (writep < endp) do begin
			cmd := src.ReadByte;
			b := cmd and $F0;
			if b in [$00, $10, $F0] then begin // output as literal
				byte(writep^) := cmd; inc(writep);
				continue;
			end;

			// Get the number of bytes being output from Fx, F0 xx, F0 0x xx, or F0 00 xx xx.
			reps := cmd and $F;
			if reps = 0 then begin
				reps := src.ReadByte;
				if reps < $10 then begin
					if reps = 0 then
						reps := BEtoN(src.ReadWord)
					else
						reps := reps shl 8 + src.ReadByte;
				end;
			end;

			case b of
				// Output literals * reps.
				$20:
				begin
					move(src.readp^, writep^, reps);
					inc(src.readp, reps);
				end;

				// Output b * reps.
				$30: fillbyte(writep^, reps, src.ReadByte);

				// Output aabb for reps bytes.
				$40: begin
					b := src.ReadByte;
					w := word(b and $F + word(b and $F0 shl 4)) * 17; // ab -> aabb
					w := BEtoN(w);
					b := reps and 1;
					if reps > 1 then begin
						fillword(writep^, reps shr 1, w);
						inc(writep, reps - b);
					end;
					if b <> 0 then byte(writep^) := w and $FF;
					reps := b;
				end;

				// Output 00 * reps.
				$50: fillbyte(writep^, reps, 0);

				// Output FF * reps.
				$60: fillbyte(writep^, reps, $FF);

				// Copy from 2 rows ago.
				$70: MemCopy(writep - 2, writep, reps);

				// Copy from 4 rows ago.
				$80: MemCopy(writep - 4, writep, reps);

				// Copy from previous column.
				$90: MemCopy(writep - tempimage.sizeYP, writep, reps);

				// Copy from plane 0/1, maybe inverted.
				$A0, $C0: _FromPlane((b and $40) shr 6, PA_COPY);
				$B0, $D0: _FromPlane((b and $40) shr 6, PA_NEG);

				// Copy reps bytes from special location.
				$E0: begin
					b := src.ReadByte;
					case b of
						// Copy from plane 2, maybe inverted.
						0: _FromPlane(2, PA_COPY);
						1: _FromPlane(2, PA_NEG);

						// Bitwise operation from previous planes.
						// 2 = (0 | 1); 3 = (0 & 1); 4 = (1 | 2); 5 = (1 & 2); 6 = (0 | 2); 7 = (0 & 2)
						2..7: begin
							if b in [4,5] then _FromPlane(1, PA_COPY) else _FromPlane(0, PA_COPY);
							if b < 4 then _FromPlane(1, EPlaneAct(b and 1)) else _FromPlane(2, EPlaneAct(b and 1));
						end;

						// Copy from two columns ago.
						8: MemCopy(writep - tempimage.sizeYP * 2, writep, reps);

						// Copy from 16 rows ago.
						9: MemCopy(writep - 16, writep, reps);

						else begin
							LogError(strcat('UNK CODE $Ex-& srcofs $&', [b, src.ofs - 1]));
							break;
						end;
					end;
				end;

				// Nothing unknown left!
				//else decomp_logger(strcat('UNK CODE $& srcofs $&', [cmd, src.ofs - 1]));
			end;
			inc(writep, reps);
		end;
	end;

	procedure _FinaliseImage;
	var writep : pointer;
		numsrcbytes, x, y : dword;
	begin
		with tempimage do begin
			numsrcbytes := stride * sizeYP;
			setlength(bitmap, numsrcbytes);
			numsrcbytes := numsrcbytes shr 2;
			writep := @bitmap[0];

			x := 0; y := sizeYP;
			while numsrcbytes <> 0 do begin
				dec(numsrcbytes);
				dword(writep^) := ExplodeBits(byte(plane[0]^)) + ExplodeBits(byte(plane[1]^)) shl 1
					+ ExplodeBits(byte(plane[2]^)) shl 2 + ExplodeBits(byte(plane[3]^)) shl 3;
				inc(plane[0]); inc(plane[1]); inc(plane[2]); inc(plane[3]);

				dec(y);
				if y <> 0 then
					inc(writep, stride)
				else begin
					y := sizeYP;
					inc(x, 4);
					writep := @bitmap[0] + x; // not @bitmap[x], goes out of range if image height 1
				end;
			end;
		end;
	end;

	function _LoadBitsFrom(const src : TFileLoader) : boolean;
	var i, j : dword;
		flag : byte;
	begin
		result := FALSE;
		if (src.ofs <> 0) and (decomp_param.verbose) then decomp_logger('frame start @ $' + strhex(src.ofs));
		i := BEtoN(src.ReadDword);
		if i shr 8 <> $4F4C48 then begin
			if src.ofs = 4 then raise DecompException.Create('bad olh sig');
			exit; // single-frame OLH but with some padding, nothing more to do
		end;
		result := TRUE;
		dec(src.readp);
		i := src.ofs + 32;
		repeat
			if src.ofs > i then raise DecompException.Create('missing 1a at start');
		until src.ReadByte = $1A;
		if src.ReadByte <> 0 then raise DecompException.Create('expected 1a 00 at start');

		flag := src.ReadByte;
		if flag and 3 <> 3 then raise DecompException.Create('unexpected flag $' + strhex(flag));

		with tempimage do begin
			sizeXP:= 640;
			sizeYP := 400;
			if flag and 4 <> 0 then begin
				sizeXP := src.ReadByte * 8;
				sizeYP := LEtoN(src.ReadWord);
			end;

			if (sizeXP > 640) or (sizeYP > 400) or (sizeXP = 0) or (sizeYP = 0) then
				raise DecompException.Create(strcat('Suspicious image size %x%', [sizeXP, sizeYP]));
			stride := sizeXP shr 1;

			// Read the palette if present. 0GRB order, one nibble per component.
			if flag and 8 <> 0 then for i := 0 to 15 do with palette[i] do begin
				j := src.ReadByte;
				b := (j and $F) * 17;
				r := (j shr 4) * 17;
				g := (src.ReadByte and $F) * 17;
				a := $FF;
			end;

			i := stride * sizeYP;
			fillbyte(buffy^, i, 0);
			planesize := i shr 2;
			plane[0] := buffy;
			for j := 1 to 3 do plane[j] := plane[j - 1] + planesize;
		end;

		// The compressed image is saved one bitplane at a time, each bitplane as 8-bit columns.
		_UnpackColumns(src);
		// Merge the bitplanes into a 4bpp image.
		_FinaliseImage;
	end;

var indexstring, trueoutputname : UTF8string;
	p : pointer = NIL;
	paltemp : TSRGBPalette = NIL;
	transparentindex, bytesize : dword;
begin
	result := FALSE;
	if loader.fullFileSize < 22 then raise DecompException.Create('file too tiny');

	metaname := upcase(basename);
	trueoutputname := PathCombine([outdir, basename]);
	indexstring := '';
	frameindex := 0;
	getmem(buffy, 640 * 400 shr 1);
	setlength(paltemp, 16);

	try
		while loader.ofs < loader.fullFileSize do begin
			tempimage := mcg_bitmap.Create();
			with tempimage do begin
				bitDepth := 4;
				bitmapFormat := MCG_FORMAT_INDEXED;
				bitmap := NIL;
				palette := paltemp; // carry palette between frames
			end;

			if NOT _LoadBitsFrom(loader) then break;

			// Add .number to multi-frame rips. Most OLHs just have one frame though.
			if (frameindex <> 0) or (loader.ofs < loader.fullFileSize) then indexstring := '.' + strdec(frameindex);
			metadata := CopyOrCreateGraphicFile(metaname + indexstring);
			metadata.origSizeXP := tempimage.sizeXP;
			metadata.origSizeYP := tempimage.sizeYP;
			metadata.srcFilePath := trueoutputname + indexstring + '.png';

			transparentindex := SelectTransparentIndex(CN_OLH, metadata, tempimage, game);
			PostProcessGraphic(tempimage, metadata, game, transparentindex);

			paltemp := tempimage.palette;

			tempimage.MakePNG(p, bytesize);
			tempimage.Destroy; tempimage := NIL;
			SaveFile(metadata.srcFilePath, p, bytesize);
			freemem(p); p := NIL;

			AddOrReplaceGraphicFile(metadata);
			metadata := NIL;
			inc(frameindex);
		end;
		if frameindex > 1 then decomp_logger('extracted ' + strdec(frameindex) + ' frames');

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if p <> NIL then begin freemem(p); p := NIL; end;
	end;
	result := TRUE;
end;

