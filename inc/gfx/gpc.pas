{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_GPC(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a GPC or GPA graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	buffy : pointer = NIL;
	bufendp : pointer;
	maxbounds : record
		left, right, top, bottom : dword;
	end;
	frameinfo : array of record
		startofs, interlace_v : dword;
		width, height, ofsx, ofsy : word;
		padright : byte;
	end;
	pal : TSRGBPalette = NIL;
	framebytesize, rowbytewidth : dword;
	isGPA : boolean;

	procedure _ReadHeader(const src : TFileLoader; paletteonly : boolean); forward;

	procedure _ImportPalette(const src : TFileLoader);

		function _TryLocate(const basename : string31) : UTF8string;
		// Often a file.gpa will have a file.gpc in a ../gpc/ directory with the valid palette.
		// But the directory name is inconsistent between games, so have to wildcard-search.
		var sb : TStringBunch;
			basepath : UTF8string;
			l : byte;
		begin
			sb := FindFiles_caseless(basename, FALSE, TRUE);
			if sb <> NIL then exit(sb[0]);
			basepath := ExtractFilePath(ExtractFileDir(src.fileName));
			sb := FindFiles_caseless(basepath + '*g*/' + basename, FALSE, TRUE);
			if sb <> NIL then exit(sb[0]);
			sb := FindFiles_caseless(basepath + 'pic*/' + basename, FALSE, TRUE);
			if sb <> NIL then exit(sb[0]);

			l := length(basename);
			if (l > 5) and (basename[l - 4] in ['A'..'Z']) and (basename[l - 5] in ['0'..'9']) then begin
				sb := FindFiles_caseless(
					basepath + '*g*/' + copy(basename, 1, l - 5) + copy(basename, l - 3), FALSE, TRUE);
				if sb <> NIL then exit(sb[0]);
			end;
			result := '';
		end;

	var f : TFileLoader = NIL;
		name : UTF8string;
		i : dword;
	begin
		name := '';
		with metadata do case game of
			gid.Arabesque: name := 'als_o04l.gpc';
			gid.CanCanB3:
			case graphicName of
				'K9B.A': name := 'k9s.gpc';
				'N29A4.A': name := 'n29a3.gpc';
				else name := 'k10u.gpc';
			end;
			gid.CanCanB4:
			case graphicName of
				'S05OP.A': name := 's05.gpc';
				'A19AZ.A': name := 'a19a.gpc';
				else name := 'b14.gpc';
			end;
			gid.CanCanB5:
			case graphicName of
				'C12.A': name := 'c12a1.gpc';
				'H18.A': name := 'h18b.gpc';
				'H32.A': name := 'h32l.gpc';
				'K19.A': name := 'k19a.gpc';
				else name := lowercase(copy(graphicName, 1, 3)) + '.gpc';
			end;
			gid.CanCanB5half:
			if graphicName = 'AN13A.A' then
				name := 'cg13a.gpc'
			else
				name := 'cg' + graphicName[3] + graphicName[4] + '.gpc';
			gid.DeadBrain1:
			case graphicName of
				'BL077.A': name := 'bl077b.gpc';
				'BL085.A': name := 'bl085a.gpc';
				'BLED02.A': name := 'bled02b.gpc';
				'BL4749.A': name := 'bl047.gpc';
				'BLOP15.A': name := 'blopz.gpc';
			end;
			gid.DeadBrain2:
			case graphicName of
				'DB163C.A': name := 'db162.gpc';
				'DB196RB.A',
				'DB196RBB.A': name := 'db195r.gpc';
				'DB200.A': name := 'db200a.gpc';
				'DB206.A': name := 'db206_0.gpc';
			end;
			gid.DengekiNurse1:
			begin
				name := copy(graphicName, 1, length(graphicName) - 2) + '.gpc';
				name[4] := '_'; // animations have hyphens, base graphics have underscores
			end;
			gid.Dracula:
			case graphicName of
				'DR4.A': name := 'r4.gpc';
				'MA12.A': name := 'm12.gpc';
				else name := 'd0' + graphicName[3] + graphicName[4] + '.gpc';
			end;
			gid.GlassnoUnmei: name := copy(graphicName, 1, 5) + '*_a.gpc';
			gid.GokuMandala: name := 'gm006c.gpc';
			gid.HimiHana: name := copy(graphicName, 1, 3) + '.gpc';
			gid.KimiDake: name := '#' + copy(graphicName, 2, 3) + '.gpc';
			gid.KoroshinoDress3:
			if graphicName = 'G116.A' then
				name := 'g116a.gpc'
			else
				name := copy(graphicName, 1, 4) + '.gpc';
			gid.KounaiShasei3:
			case graphicName of
				'Y3015.A': name := 'y3015t.gpc';
				'Y3020.A': name := 'y3020t.gpc';
				'Y3090.A': name := 'y3091.gpc';
				'Y3092.A': name := 'y3093.gpc';
			end;
			gid.KuruKaji:
			case graphicName of
				'K065A1.A': name := 'k065.gpc';
				'K079.A': name := 'k079a.gpc';
			end;
			gid.LamMal:
			if (copy(graphicName, 1, 3) = 'NO-') and (copy(graphicName, length(graphicName) - 1) = '.A') then
				name := 'no-1.gpc'
			else case graphicName of
				'3007.A': name := 'l-3007.gpc';
				'BOUZU-1.A','BOUZU-2.A','IN.A','MR-X.A','OYAJI.A': name := 'don9.gpc';
				'L-1062.A': name := 'l-1062b.gpc';
				'L-5019.A': name := 'l-5019a.gpc';
			end;
			gid.Mai:
			case graphicName of
				'147UWAME.A': name := 'ed147.gpc';
				'GO1.A': name := 'gameover.gpc';
				'MA042.A': name := 'ma042a.gpc';
				//'MA055.A': name := 'mb055.gpc'; // incorrect, what should it import?
				'MB055.A': name := 'mb055b.gpc';
				'OP008K.A','OP008.A','OP002A.A': name := 'op008ka.gpc';
			end;
			gid.MarinePhilt:
			case graphicName of
				'MP060.A': name := 'mp058.gpc';
				'MP072.A','MP072M.A': name := 'mp071.gpc';
				'MP073A.A': name := 'mp073''.gpc';
				'MP074''.A': name := 'mp_flame.gpc';
				'MP103.A': name := 'mp102.gpc';
				'MP127ALI.A': name := 'mp127.gpc';
				'MP202.A': name := 'mp202j.gpc';
				'MP215.A': name := 'mp215a.gpc';
			end;
			gid.Miho:
			if graphicName = 'A-23-1.A' then
				name := 'a-23.gpc'
			else
				name := 'a-' + graphicName[3] + graphicName[4] + '*.gpc';
			gid.Miki: name := copy(graphicName, 1, 7) + '.gpc';
			gid.Naomi: name := 'n_011b.gpc';
			gid.PiaCarrot:
			case graphicName of
				'MUSIC.A',
				'MUSICTBL.A': name := 'music_bg.gpc';
			end;
			gid.Saori: if length(graphicName) > 5 then name := 's' + graphicName[4] + graphicName[5] + '.gpc';
			gid.ShinjukuMono: name := 'g.gpc';
			gid.Tesera:
			if graphicName = 'T-041AB.A' then
				name := 't-041a.gpc'
			else
				name := copy(graphicName, 1, 5) + '.gpc';
			gid.ToumeiNingen: name := 'pal.gpc';
			gid.XIX: if length(graphicName) > 3 then name := 'ruins' + graphicName[2] + graphicName[3] + '.gpc';
			gid.YokohamaElegy: if graphicName = 'RB_41P6' then name := 'rb_41p4.gpc';
		end;
		if name <> '' then name := _TryLocate(name);

		if (name = '') and (isGPA) then with metadata do begin
			i := length(graphicName);
			// Try the easy default of XYZ.A importing its palette from XYZ.GPC.
			name := _TryLocate(copy(graphicName, 1, i - 2) + '.gpc');
			// Also in case of something like 123X.A try 123.GPC.
			if name = '' then
				if (i > 3) and (graphicName[i - 3] in ['0'..'9']) and (graphicName[i - 2] in ['A'..'Z']) then
					name := _TryLocate(copy(graphicName, 1, i - 3) + '.gpc');
		end;

		if name <> '' then begin
			try
				f := TFileLoader.Open(name);
				_ReadHeader(f, TRUE);
			finally
				if f <> NIL then f.Destroy;
			end;
		end;
	end;

	procedure _ReadHeader(const src : TFileLoader; paletteonly : boolean);
	var	name : UTF8string;
		ppalinfo, compressed_bytes : dword;
		i, j : dword;
	begin
		if BEtoN(src.ReadDword) <> $50433938 then raise DecompException.Create('missing pc98 sig');
		i := BEtoN(src.ReadDword);
		j := BEtoN(src.ReadDword);
		if ((i <> $29475043) and (i <> $29475041)) or (j <> $46494C45) then
			raise DecompException.Create('missing gpcfile sig');
		inc(src.readp, 4);
		frameinfo := NIL;

		if paletteonly then
			inc(src.readp, 4)
		else begin
			isGPA := (i = $29475041);
			name := upcase(basename);
			if isGPA then name := name + '.A';
			metadata := CopyOrCreateGraphicFile(name);

			if isGPA then
				metadata.frameCount := LEtoN(src.ReadDword)
			else begin
				metadata.frameCount := 1;
				setlength(frameinfo, 1);
				i := LEtoN(src.ReadDword);
				if i > 128 then raise DecompException.Create('bad interlace-v ' + strdec(i));
				if i = 0 then i := 1;
				frameinfo[0].interlace_v := i;
			end;
		end;

		ppalinfo := LEtoN(src.ReadDword);
		if ppalinfo = 0 then
			_ImportPalette(src)
		else with tempimage do begin
			if ppalinfo <> $30 then raise DecompException.Create('bad ppalinfo $' + strhex(ppalinfo));
			src.ofs := ppalinfo;
			if LEtoN(src.ReadWord) <> 16 then raise DecompException.Create('pal not 16 colors');
			if LEtoN(src.ReadWord) <> 2 then raise DecompException.Create('bpc not 2');

			setlength(pal, 16);
			for i := 0 to high(pal) do with pal[i] do begin
				j := src.ReadByte;
				b := (j and $F) * 17;
				r := (j shr 4) * 17;
				g := src.ReadByte * 17;
				a := $FF;
			end;
		end;
		if paletteonly then exit;
		if (pal = NIL) and (decomp_param.verbose) then decomp_logger('[!] no palette');

		// Count the image info pointers. Just one for GPC, multiple for GPA.
		src.ofs := $18;
		i := 0;
		repeat
			j := LEtoN(src.ReadDword);
			if (j < src.ofs) or (j + 16 > src.fullFileSize) then break;
			inc(i);
		until FALSE;
		if i = 0 then raise DecompException.Create('bad pimageinfo $' + strhex(j));
		if i <> metadata.frameCount then begin
			if decomp_param.verbose then // many GPC files in Nike do this, ignore
				decomp_logger(strcat('[!] framecount % != % numInfoBlocks', [metadata.frameCount, i]));
		end;

		with maxbounds do begin
			left := high(left);
			top := high(top);
			right := 0;
			bottom := 0;
		end;

		// Read the image info blocks.
		src.ofs := $18;
		setlength(frameinfo, metadata.frameCount);
		for i := 0 to metadata.frameCount - 1 do begin
			j := src.ofs;
			src.ofs := LEtoN(dword(src.readp^));
			with frameinfo[i] do begin
				if isGPA then begin
					interlace_v := LEtoN(src.ReadWord);
					ofsx := LEtoN(src.ReadWord);
					ofsy := LEtoN(src.ReadWord);
					width := LEtoN(src.ReadWord);
					height := LEtoN(src.ReadWord);
				end
				else begin
					width := LEtoN(src.ReadWord);
					height := LEtoN(src.ReadWord);
					compressed_bytes := LEtoN(src.ReadWord);
					if (compressed_bytes = 0) or (src.ofs + compressed_bytes > src.fullFileSize + 6) then
						raise DecompException.Create('bad compressed size: ' + strdec(compressed_bytes));
					inc(src.readp, 4);
					ofsx := LEtoN(src.ReadWord);
					ofsy := LEtoN(src.ReadWord);
					inc(src.readp, 2);
				end;
				startofs := src.ofs;
				if (width > 640) or (height > 400) or (width = 0) or (height = 0) then
					raise DecompException.Create(strcat('sus image size %x%, frame %', [width, height, i]));
				if (ofsx > 639) or (ofsy > 399) then begin
					if decomp_param.verbose then decomp_logger(strcat('[!] sus image ofs %,%', [ofsx, ofsy]));
					if ofsx > 639 then ofsx := 0;
					if ofsy > 399 then ofsy := 0;
				end;
				padright := (8 - (width and 7)) and 7;
				// Hack: This image width must not be padded for whatever reason.
				if (game = gid.Mai) and (metadata.graphicName = 'TLOGO') then padright := 0;
				inc(width, padright);

				// Track the largest bounds.
				with maxbounds do begin
					if ofsx < left then left := ofsx;
					if ofsy < top then top := ofsy;
					if ofsx + width > right then right := ofsx + width;
					if ofsy + height > bottom then bottom := ofsy + height;
				end;
			end;
			src.ofs := j + 4;
		end;

		with metadata do begin
			origSizeXP := maxbounds.right - maxbounds.left;
			origFrameHeightP := maxbounds.bottom - maxbounds.top;
			origSizeYP := origFrameHeightP * frameCount;
			framebytesize := dword(((origSizeXP + 7) and $FF8) shr 1) * origFrameHeightP;
			origOfsXP := maxbounds.left;
			origOfsYP := maxbounds.top;

			tempimage := mcg_bitmap.Create();
			with tempimage do begin
				bitDepth := 4;
				bitmapFormat := MCG_FORMAT_INDEXED;
				bitmap := NIL;
				palette := pal; pal := NIL;
				sizeXP := origSizeXP;
				sizeYP := origSizeYP;
				stride := (sizeXP + 1) shr 1;
				setlength(bitmap, framebytesize * frameCount);
			end;
			// Prepare the work buffy. Needs full bitmap size, +1 interlace byte for each row, +8 leeway (hidden).
			i := framebytesize + origSizeYP;
			getmem(buffy, i + 8);
			bufendp := buffy + i;
		end;
	end;

	procedure _CombinePlanes(frameindex : dword);
	var startp, writep : pointer;
		plane : array[0..3] of pointer;
		x, y, i, j, row : dword;
	begin
		with tempimage do begin
			startp := @bitmap[0] + frameindex * framebytesize;
			with frameinfo[frameindex] do begin
				// If this image needs any padding, init the output region by filling with 0 (transparent).
				if (maxbounds.left < ofsx) or (maxbounds.right > ofsx + width)
				or (maxbounds.top < ofsy) or (maxbounds.bottom > ofsy + height) then begin
					fillbyte(startp^, framebytesize, 0);
					i := (ofsy - maxbounds.top) * sizeXP + (ofsx - maxbounds.left);
					inc(startp, ((i + 7) and $FF8) shr 1);
				end;

				i := rowbytewidth shr 2; j := 0; row := 0;

				for y := 0 to height - 1 do begin
					plane[0] := buffy + (y * (rowbytewidth + 1)) + 1;
					plane[1] := plane[0] + i;
					plane[2] := plane[1] + i;
					plane[3] := plane[2] + i;
					writep := startp + row * (sizeXP shr 1);
					for x := i - 1 downto 0 do begin
						dword(writep^) := ExplodeBits(byte(plane[0]^)) + ExplodeBits(byte(plane[1]^)) shl 1
							+ ExplodeBits(byte(plane[2]^)) shl 2 + ExplodeBits(byte(plane[3]^)) shl 3;
						inc(plane[0]); inc(plane[1]); inc(plane[2]); inc(plane[3]);
						inc(writep, 4);
					end;

					inc(row, interlace_v);
					if row >= height then begin
						inc(j);
						row := j;
					end;
				end;
			end;
		end;
	end;

	procedure _Delace(frameindex : dword);
	var p, pv, pend : pointer;
		x, y : dword;
		i, j, a : byte;
		notfirst : boolean = FALSE;
	begin
		p := buffy;
		Assert(rowbytewidth and 3 = 0); // all images have a byte width multiple of 4, so can use dwords
		for y := frameinfo[frameindex].height - 1 downto 0 do begin
			// Get horizontal interlacing mode byte. If not 0, XOR every i'th byte with the previously touched byte,
			// wrapping around until all bytes done.
			// Note! The XOR carries through wrap-around, so it must be implemented like this.
			i := byte(p^); inc(p);
			//if i <> 0 then decomp_logger(strhex(i));
			if (i <> 0) and (i < rowbytewidth) then begin
				pv := p;
				pend := p + rowbytewidth;

				a := 0;
				for j := 0 to i - 1 do begin
					pv := p + j;
					while pv < pend do begin
						a := a xor byte(pv^);
						byte(pv^) := a;
						inc(pv, i);
					end;
				end;
			end;

			if notfirst then begin
				// XOR every byte with the same byte on previous row.
				pv := p - rowbytewidth - 1;
				for x := (rowbytewidth shr 2) - 1 downto 0 do begin
					dword(p^) := dword(p^) xor dword(pv^);
					inc(p, 4);
					inc(pv, 4);
				end;
			end
			else begin
				notfirst := TRUE;
				inc(p, rowbytewidth);
			end;
		end;
	end;

	procedure _Unpack(const src : TFileLoader; frameindex : dword);
	var writep, endp : pointer;
		flagA, flagAbits, flagB, flagBbits : byte;
	begin
		src.ofs := frameinfo[frameindex].startofs;
		if frameindex = dword(high(frameinfo)) then
			endp := src.endp
		else
			endp := src.readp + frameinfo[frameindex + 1].startofs - frameinfo[frameindex].startofs;
		rowbytewidth := ((frameinfo[frameindex].width + 7) and $FF8) shr 1;
		writep := buffy;
		flagA := 0; flagB := 0; flagAbits := 1; flagBbits := 0;
		while writep < bufendp do begin
			if flagBbits <> 0 then begin
				if flagB and $80 = 0 then
					byte(writep^) := 0
				else if src.readp >= endp then
					break
				else
					byte(writep^) := src.ReadByte;
				inc(writep);
				dec(flagBbits);
				flagB := byte(flagB shl 1);
				continue;
			end;

			flagA := byte(flagA shl 1);
			dec(flagAbits);
			if flagAbits = 0 then begin
				if src.readp >= endp then break;
				flagA := src.ReadByte;
				flagAbits := 8;
			end;

			if flagA and $80 = 0 then begin
				// Flag A bit clear: output 00 x 8
				qword(writep^) := 0;
				inc(writep, 8);
			end
			else begin
				// Flag A bit set: read flag B
				if src.readp >= endp then break;
				flagB := src.ReadByte;
				flagBbits := 8;
			end;
		end;
		// Zero out any leftover space in the buffer.
		fillbyte(writep^, bufendp - writep, 0);

		_Delace(frameindex);
		_CombinePlanes(frameindex);
		// Should perhaps paste current frame in multiframe image over previous frame, when more than 1 frame pos.

		// Remove padding if any.
		with frameinfo[frameindex] do
			if padright <> 0 then tempimage.Crop(0, padright, 0, 0);
	end;

var transparentindex, n : dword;
begin
	result := FALSE;
	if loader.fullFileSize < 80 then raise DecompException.Create('file too tiny');

	try
		_ReadHeader(loader, FALSE);
		for n := 0 to metadata.frameCount - 1 do _Unpack(loader, n);
		freemem(buffy); buffy := NIL;

		with metadata do begin
			if isGPA then
				srcFilePath := PathCombine([outdir, basename], 'a.png')
			else
				srcFilePath := PathCombine([outdir, basename], 'png');

			{$ifdef enable_decomp_hacks}
			// Hack: fix bad palette.
			if (game = gid.YokohamaElegy) and (graphicName = 'RB_41P6') then _ImportPalette(loader);
			{$endif}
		end;

		transparentindex := SelectTransparentIndex(CN_GPC, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(buffy, n);
		SaveFile(metadata.srcFilePath, buffy, n);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
	end;
	result := TRUE;
end;

