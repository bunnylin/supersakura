### Graphic converters

For graphic format documentation, see [/doc/gfx](../../doc/gfx).

- ada: Fairytale, Cocktail Soft, Elf etc `ADA`, `MDA`, `ANI`, `PR6`, `PR7`, `PA7`, `PD7`,
	`PD8`, `PIC`
- as2: Himeya, Forest, Foster `AS2`
- bmp: Mostly uncompressed bitmaps `BMP`, `ZBM`, `CWL`, `EW`, `ACH`, `C24`, `C25`, `TIFF`
- da1: Himeya, C's Ware, Forest `DA1`, `DAD`, `GDT`
- ggd: Ikura GDL engine graphics `GG`, `GGD`, `GGP`, `GG0`, `GG1`, `GG2`, `GG3`,
	`GAD`, `GAN`, `DRG`
- gpc: Fairytale, Cocktail Soft `GPC`, `GPA`
- greatpac: Great `PAC`
- jast-img-brge: JAST's custom graphics `IMG`, `B+R+G+E`, `BC+RC+GC+EC`
- makichan: Woody Rinn's popular MAKI and MAG graphic formats `MAG`, `MAX`, `MKI`, `SGV`
- mrs: DO, Foster, ZyX, Parsley `MRS`, `VRS`, `XMG`
- msv: Silence, Sogna `MSV`, `SCR`, `ANM`, `SZH`
- ot: Fairy Dust `OT`
- ozm: Apple Pie `OZM`
- pi: Yanagisawa-san's powerful `PI` format
- png: Modern `PNG` variants, Crowd's `CWP`
- prs: Fairytale's old `PRS`
- vdf: Panda House, Melody, Cat's Pro `VDF`
