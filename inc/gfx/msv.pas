{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_SilenceMSV(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a MSV, SCR, or ANM graphic. Adds its metadata to graphicFiles[] and saves the
// bitmap in outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
// These were used by Silence/Sogna PC-98 games.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	imagebytesize : dword;

	procedure _LoadBits(const src : TFileLoader);
	var writep : pointer;
		i : dword;
		cols, rows : word;
		format : byte;
		multiframe : boolean = FALSE;
		needsalpha : boolean = FALSE;

		function _ReadFrameInfo : boolean;
		var j, datastartofs : dword;
			ofsx, ofsy : word;
			left, right, top, bottom : word;
			frame : byte;
		begin
			result := FALSE;
			if src.fullFileSize < $88 then begin
				if decomp_param.verbose then decomp_logger('file too tiny');
				exit;
			end;
			frame := 0;
			while src.readp < src.endp do begin
				i := src.ReadByte;
				if i = 0 then break;
				if i > dword(frame + 1) then begin
					if decomp_param.verbose then decomp_logger('frame indexes ascending too hard');
					exit;
				end;
				if i > frame then frame := i;
			end;
			if src.ofs > $80 then begin
				if decomp_param.verbose then decomp_logger('frame indexes overflow');
				exit;
			end;
			if frame = 0 then exit;
			metadata.frameCount := frame;
			if src.fullFileSize < dword($80 + frame * 8) then begin
				if decomp_param.verbose then decomp_logger('frameinfo oob');
				exit;
			end;

			// Find the largest frame bounds, to calculate a bitmap size that fits all frames.
			left := $FFFF; top := $FFFF; right := 0; bottom := 0;
			src.ofs := $80;
			with metadata do begin
				datastartofs := frameCount * 4 + $80;
				origSizeXP := 0; origFrameHeightP := 0;

				while frame <> 0 do begin
					i := LEtoN(src.ReadWord);
					i := i * 16 + LetoN(src.ReadWord);
					if (i < datastartofs) or (i + 4 > src.fullFileSize) then begin
						if decomp_param.verbose then decomp_logger('frame start oob');
						exit;
					end;

					ofsx := 0; ofsy := 0;
					if src.ReadWordFrom(i) = 0 then begin
						// Extended header.
						j := LEtoN(src.ReadWordFrom(i + 2));
						ofsx := (j mod 80) shl 3;
						ofsy := j div 80;
						if (ofsx > 620) or (ofsy > 380) then begin
							if decomp_param.verbose then decomp_logger(strcat('sus frame ofs %,%', [ofsx, ofsy]));
							exit;
						end;
						inc(i, 4);
					end;
					if ofsx < left then left := ofsx;
					if ofsy < top then top := ofsy;

					cols := LEtoN(src.ReadWordFrom(i));
					if (cols > 191) or (cols = 0) then begin
						if decomp_param.verbose then decomp_logger('sus framewidth ' + strdec(cols));
						exit;
					end;
					cols := (cols shl 3) + ofsx;
					if cols <> right then begin
						if frame <> frameCount then needsalpha := TRUE;
						if cols > right then right := cols;
					end;

					rows := LEtoN(src.ReadWordFrom(i + 2));
					if (rows > 1344) or (rows = 0) then begin
						if decomp_param.verbose then decomp_logger('sus frameheight ' + strdec(rows));
						exit;
					end;
					inc(rows, ofsy);
					if rows <> bottom then begin
						if frame <> frameCount then needsalpha := TRUE;
						if rows > bottom then bottom := rows;
					end;

					dec(frame);
				end;
				origOfsXP := left;
				origOfsYP := top;
				origSizeXP := right - left;
				origFrameHeightP := bottom - top;
				origSizeYP := origFrameHeightP * frameCount;

				if needsalpha then with tempimage do begin
					bitDepth := 8;
					setlength(palette, 17);
					palette[16].FromRGBA($FF00FF00);
				end;
			end;
			result := TRUE;
		end;

		procedure _UnpackColumns; // format 0, MSV
		var startp : pointer;
			x, y, reps, new_value, prev_out : dword;
			bitplane, prev_value : byte;
		begin
			// This decompression implementation expands each 1bpp 8px column slice into a bitmasked 4bpp 8px slice,
			// then directly ORs those into the output buffer at the proper final location. This is simpler and faster
			// than generating all bitplane columns and only merging and rotating them at the end.
			startp := writep;
			x := cols; y := rows; bitplane := 0; prev_out := 0;
			prev_value := NOT byte(src.readp^);

			while src.readp < src.endp do begin
				new_value := src.ReadByte;
				if new_value <> prev_value then begin
					prev_value := new_value;
					reps := 1;
				end
				else begin
					reps := src.ReadByte;
					if reps = 0 then reps := 256 + src.ReadByte;
					// A third repeated value shall not trigger a repeat.
					prev_value := NOT byte(src.readp^);
				end;
				new_value := ExplodeBits(new_value) shl bitplane;
				while reps <> 0 do begin
					prev_out := prev_out xor new_value;
					dword(writep^) := dword(writep^) or prev_out;

					dec(y);
					if y <> 0 then
						inc(writep, tempimage.stride)
					else begin
						inc(bitplane);
						if bitplane >= 4 then begin
							dec(x);
							if x = 0 then exit;
							bitplane := 0;
							inc(startp, 4);
						end;
						// The first byte in a new plane/column is always a literal.
						prev_value := NOT byte(src.readp^);
						prev_out := 0;
						writep := startp;
						y := rows;
					end;

					dec(reps);
				end;
			end;
		end;

		procedure _MonoCols; // format 2, ANM
		var x, y, reps, new_value, prev_out : dword;
			startp : pointer;
			prev_value : byte;
		begin
			startp := writep;
			x := cols; y := rows; prev_out := 0;
			prev_value := NOT byte(src.readp^);

			while src.readp < src.endp do begin
				new_value := src.ReadByte;
				if new_value <> prev_value then begin
					prev_value := new_value;
					reps := 1;
				end
				else begin
					reps := src.ReadByte;
					if reps = 0 then reps := 256 + src.ReadByte;
					prev_value := NOT byte(src.readp^);
				end;
				new_value := ExplodeBits(byte(new_value)) * $F;

				while reps <> 0 do begin
					prev_out := prev_out xor new_value;
					dword(writep^) := dword(writep^) or prev_out;

					dec(y);
					if y <> 0 then
						inc(writep, tempimage.stride)
					else begin
						dec(x);
						if x = 0 then exit;
						y := rows;
						inc(startp, 4);
						writep := startp;
						// The first byte in a new plane/column is always a literal.
						prev_value := NOT byte(src.readp^);
						prev_out := 0;
					end;
					dec(reps);
				end
			end;
		end;

		procedure _UnpackRows; // format 3, SCR
		var startp : pointer;
			x, y : dword;
			bitplane : byte = 0;
		begin
			startp := writep;
			x := cols; y := rows;
			while src.readp < src.endp do begin
				dword(writep^) := dword(writep^) or (ExplodeBits(src.ReadByte) shl bitplane);
				dec(x);
				if x <> 0 then
					inc(writep, 4)
				else begin
					inc(bitplane);
					if bitplane >= 4 then begin
						dec(y);
						if y = 0 then exit;
						bitplane := 0;
						inc(startp, tempimage.stride);
					end;
					writep := startp;
					x := cols;
				end;
			end;
		end;

		procedure _MonoRows; // format 4, SCR
		var startp : pointer;
			x, y : dword;
		begin
			startp := writep;
			x := cols; y := rows;
			while src.readp < src.endp do begin
				dword(writep^) := ExplodeBits(src.ReadByte) * byte($F);
				dec(x);
				if x <> 0 then
					inc(writep, 4)
				else begin
					dec(y);
					if y = 0 then exit;
					x := cols;
					inc(startp, tempimage.stride);
					writep := startp;
				end;
			end;
		end;

		procedure _Unpack15; // format 1, 5, ANM
		var pixelrow : qword;
			x, y, reps, prev_value : dword;
			startp : pointer;
		begin
			startp := writep;
			x := cols; y := rows;
			prev_value := not LEtoN(dword(src.readp^));
			while src.readp < src.endp do begin
				i := LEtoN(src.ReadDword);
				if i <> prev_value then begin
					prev_value := i;
					reps := 1;
				end
				else begin
					reps := src.ReadByte;
					if reps = 0 then reps := 256 + src.ReadByte;
					prev_value := NOT LEtoN(dword(src.readp^));
				end;
				pixelrow := ExplodeBitsQ(byte(i)) or (ExplodeBitsQ(byte(i shr 8)) shl 1)
					or (ExplodeBitsQ(byte(i shr 16)) shl 2) or (ExplodeBitsQ(byte(i shr 24)) shl 3);

				while reps <> 0 do begin
					qword(writep^) := pixelrow;
					dec(y);
					if y <> 0 then
						inc(writep, tempimage.stride)
					else begin
						dec(x);
						if x = 0 then exit;
						y := rows;
						inc(startp, 8);
						writep := startp;
						prev_value := NOT LEtoN(dword(src.readp^));
					end;
					dec(reps);
				end
			end;
		end;

		procedure _UnpackWithAlpha; // format 6, 7
		var pixelrow : qword;
			startp : pointer;
			x, y, reps, prev_value : dword;
			mask, prev_mask, j : byte;
		begin
			startp := writep;
			x := cols; y := rows;
			prev_value := not BEtoN(dword(src.readp^));
			prev_mask := 0;
			while src.readp < src.endp do begin
				i := BEtoN(src.ReadDword);
				mask := src.ReadByte;
				if (i <> prev_value) or (mask <> prev_mask) then begin
					prev_value := i;
					prev_mask := mask;
					reps := 1;
				end
				else begin
					reps := src.ReadByte;
					if reps = 0 then reps := 256 + src.ReadByte;
					prev_value := NOT BEtoN(dword(src.readp^));
				end;

				if mask = 0 then
					pixelrow := $1010101010101010
				else begin
					i := ExplodeBits(byte(i shr 24)) or (ExplodeBits(byte(i shr 16)) shl 1)
						or (ExplodeBits(byte(i shr 8)) shl 2) or (ExplodeBits(byte(i)) shl 3);
					i := NtoBE(i);
					pixelrow := 0;
					for j := 7 downto 0 do begin
						pixelrow := pixelrow shl 8;
						if mask and 1 = 0 then
							pixelrow := pixelrow or $10
						else
							pixelrow := pixelrow or (i and $F);
						mask := mask shr 1;
						i := i shr 4;
					end;
				end;

				while reps <> 0 do begin
					qword(writep^) := pixelrow;
					dec(y);
					if y <> 0 then
						inc(writep, tempimage.stride)
					else begin
						dec(x);
						if x = 0 then exit;
						y := rows;
						inc(startp, 8);
						writep := startp;
						prev_value := NOT BEtoN(dword(src.readp^));
					end;
					dec(reps);
				end
			end;
		end;

		procedure _UnpackFrames; // only if an explicit multiframe header was found
		var ofsx, ofsy, framebytes : dword;
			frame : byte;
			usedalpha : boolean;
		begin
			with metadata do with tempimage do begin
				usedalpha := format in [6,7];
				framebytes := (origSizeXP * origFrameHeightP * bitDepth) shr 3;
				for frame := 0 to frameCount - 1 do begin
					src.ofs := $80 + frame * 4;
					i := LEtoN(src.ReadWord);
					src.ofs := i * 16 + LEtoN(src.ReadWord);
					ofsx := 0; ofsy := 0;
					if word(src.readp^) = 0 then begin // extended header
						inc(src.readp, 2);
						i := LEtoN(src.ReadWord);
						ofsx := ((i mod 80) shl 3) - origOfsXP;
						ofsy := i div 80 - origOfsYP;
					end;
					cols := LEtoN(src.ReadWord);
					rows := LEtoN(src.ReadWord);
					if decomp_param.verbose then decomp_logger(strcat('frame % ofs %,% size %x%',
						[frame, longint(ofsx) + origOfsXP, longint(ofsy) + origOfsYP, cols shl 3, rows]));

					writep := @bitmap[frame * framebytes];
					// Less than full-size frame? Copy previous, then draw new with an offset.
					// Copy previous screws some multiframe images, have to be more discreet when to do it...
					if (cols * 8 < origSizeXP) or (rows < origFrameHeightP) then begin
						{if frame <> 0 then
							move(bitmap[(frame - 1) * framebytes], bitmap[frame * framebytes], framebytes)
						else}
						if length(palette) <= 16 then
							filldword(writep^, framebytes shr 2, 0)
						else begin
							filldword(writep^, framebytes shr 2, $10101010);
							usedalpha := TRUE;
						end;
					end;
					inc(writep, ofsy * tempimage.stride + (ofsx * bitDepth) shr 3);

					case format of
						0: _UnpackColumns; // MSV
						1,5: _Unpack15; // ANM
						2: _MonoCols;
						3: _UnpackRows; // SCR
						4: _MonoRows; // SCR-4
						6,7: _UnpackWithAlpha;
						else raise Exception.Create('bad format ' + strdec(format));
					end;
				end;

				if (length(palette) > 16) and (NOT usedalpha) then setlength(palette, 16);
			end;
		end;

		procedure _MoreSpace;
		var newwidthp, framebytes, newbytewidth, voidwidth, y : dword;
			newbmp : array of byte = NIL;
			srcp, destp : pointer;
		begin
			with tempimage do with metadata do begin
				newwidthp := cols shl 3;
				if newwidthp <= sizeXP then begin
					framebytes := stride * origFrameHeightP;
					inc(imagebytesize, framebytes);
					setlength(bitmap, imagebytesize);
				end
				else begin
					newbytewidth := (newwidthp * bitDepth) shr 3;
					voidwidth := newbytewidth - stride;
					framebytes := newbytewidth * origFrameHeightP;
					imagebytesize := framebytes * frameCount;
					setlength(newbmp, imagebytesize);
					srcp := @bitmap[0];
					destp := @newbmp[0];
					for y := sizeYP - 1 downto 0 do begin
						move(srcp^, destp^, stride);
						inc(destp, stride);
						fillbyte(destp^, voidwidth, 0);
						inc(srcp, stride);
						inc(destp, voidwidth);
					end;

					setlength(bitmap, 0); bitmap := newbmp; newbmp := NIL;
					stride := newbytewidth;
					sizeXP := newwidthp;
					origSizeXP := newwidthp;
				end;
				inc(origSizeYP, origFrameHeightP);
				sizeYP := origSizeYP;
				writep := @bitmap[imagebytesize - framebytes];
			end;
		end;

	begin
		tempimage := mcg_bitmap.Create();
		with tempimage do begin
			bitDepth := 4;
			bitmapFormat := MCG_FORMAT_INDEXED;
			bitmap := NIL;
			palette := NIL;
		end;

		with tempimage do begin
			// Footer.
			src.ofs := src.fullFileSize - 4;
			if BEtoN(dword(src.readp^)) and $FFFFFF <> $524742 then begin
				// Footer may be omitted in earlier games.
				case game of
					gid.GuynaRock:
					begin
						SetupPalette(tempimage, 16); // initing with incorrect colors
						palette[8].FromRGBA($FF55AAFF);
						palette[9].FromRGBA($FFAA55FF);
						palette[10].FromRGBA($AAFF55FF);
						palette[14].FromRGBA($FFFFFFFF);
						palette[15].FromRGBA($FF0000FF);
					end;
					gid.GuynaRockMini:
					begin
						SetupPalette(tempimage, 16); // initing with incorrect colors
						palette[3].FromRGBA($DDFFEEFF);
						palette[7].FromRGBA($DDFFEEFF);
						palette[15].FromRGBA($DDFFEEFF);
					end;
					gid.ValKaizer:
					begin
					end;
					else raise DecompException.Create('missing "RGB" at eof');
				end;
				format := 0;
			end
			else begin
				src.ofs := src.fullFileSize - 46;
				case game of
					gid.GuynaRock, gid.GuynaRockMini, gid.ValKaizer:
					begin
						format := 0;
						inc(src.readp);
					end;
					else format := src.ReadByte;
				end;
				if format >= 8 then begin
					if lowercase(ExtractFileExt(src.fileName)) = '.msv' then
						format := 0
					else
						raise DecompException.Create('bad format byte $' + strhex(format));
				end;

				if format in [0,2,3,4] then
					setlength(palette, 16)
				else begin
					bitDepth := 8;
					setlength(palette, 17); // one extra for transparency
					palette[16].FromRGBA($FF00FF00);
				end;
				dword(palette[0]) := $FF000000; // always black
				dword(palette[15]) := $FFFFFFFF; // always white
				// Actually red or grey...
				if (game = gid.GuynaRock) and (NOT (basename.StartsWith('mop'))) then
					palette[15].FromRGBA($FF0000FF)
				else if (game = gid.ValKaizer) and (basename.StartsWith('frame')) then begin
					palette[0].FromRGBA($888888FF);
					palette[15].FromRGBA($CCCCCCFF);
				end;

				for i := 1 to 14 do with palette[i] do begin
					if LEtoN(dword(src.readp^)) and $F0F0F0 <> 0 then
						raise DecompException.Create('nonzero high nibble in palette');
					r := src.ReadByte * $11;
					g := src.ReadByte * $11;
					b := src.ReadByte * $11;
					a := $FF;
				end;
			end;
		end;

		metadata := CopyOrCreateGraphicFile(upcase(basename));
		metadata.srcFilePath := PathCombine([outdir, basename], 'png');

		with tempimage do with metadata do begin
			src.ofs := 0;

			if (format in [1, 2, 5..7])
			or ((byte(src.readp^) = $01) and (byte((src.readp + 1)^) in [0..2]) and (byte((src.readp + 1)^) in [0..3])
			and (src.ReadByteFrom($80) >= $08)) then
				multiframe := _ReadFrameInfo;
			if NOT multiframe then begin
				// Header.
				origOfsXP := 0;
				origOfsYP := 0;
				frameCount := 1;
				src.ofs := 0;
				if word(src.readp^) = 0 then begin
					// Extended header.
					inc(src.readp, 2);
					i := LEtoN(src.ReadWord);
					origOfsXP := (i mod 80) shl 3;
					origOfsYP := i div 80;
					if (origOfsXP > 620) or (origOfsYP > 380) then
						raise DecompException.Create(strcat('sus image ofs %,%', [origOfsXP, origOfsYP]));
				end;
				cols := LEtoN(src.ReadWord);
				rows := LEtoN(src.ReadWord);
				{$ifdef enable_decomp_hacks}
				// Hack: The file declares a single-height image but actually has a double-height image.
				if (game = gid.GuynaRock2) and (graphicName = '108') then rows := rows * 2;
				{$endif}
				if (cols > 80) or (rows > 2050) or (cols = 0) or (rows = 0) then
					raise DecompException.Create(strcat('sus image size %x%', [cols shl 3, rows]));
				origSizeXP := cols shl 3;
				origSizeYP := rows;
				origFrameHeightP := rows;
			end;
			sizeXP := origSizeXP;
			sizeYP := origSizeYP;

			stride := (sizeXP * bitDepth + 7) shr 3;
			imagebytesize := stride * sizeYP;
			setlength(bitmap, imagebytesize);
			writep := @bitmap[0];

			if multiframe then
				_UnpackFrames // ANM
			// There may be more than one frame even without an explicit multiframe header (eg. GuynaRock).
			// In this case, just have to keep expanding the result bitmap and extracting more stuff.
			else while TRUE do begin
				case format of
					0: _UnpackColumns; // MSV
					1,5: _Unpack15;
					2: _MonoCols;
					3: _UnpackRows; // SCR
					4: _MonoRows; // SCR-4
					6,7: _UnpackWithAlpha;
					else raise DecompException.Create('bad format ' + strdec(format));
				end;

				// Does an extra frame follow?
				if (src.RemainingBytes < $60) then break;
				if BEtoN(src.ReadDwordFrom(src.ofs + 41)) and $FFFFFF = $524742 then begin
					if decomp_param.verbose then decomp_logger('extra palette, skip');
					inc(src.readp, 45);
				end;
				for i := 14 downto 0 do begin
					cols := LEtoN(word(src.readp^));
					rows := LEtoN(word((src.readp + 2)^));
					if (cols <> 0) and (rows <> 0) and (cols <= 80) and (rows <= origFrameHeightP) then break;
					inc(src.readp);
				end;
				if (cols = 0) or (rows = 0) or (cols > 80) or (rows > origFrameHeightP) then begin
					decomp_logger(strcat('[!] more data remaining at $& but bad frame size at start', [src.ofs - 15]));
					break;
				end;

				inc(frameCount);
				if decomp_param.verbose then
					decomp_logger(strcat('frame % from $&, % cols, % rows', [frameCount, src.ofs, cols, rows]));
				inc(src.readp, 4);
				_MoreSpace;
			end;
			//decomp_logger(strcat('%'#9'%'#9'format %'#9'frames %'#9'multiframe %', [graphicName, ExtractFileExt(src.fileName), format, frameCount, multiframe]));
		end;
	end;

var buffy : pointer = NIL;
	transparentindex : dword = high(dword);
begin
	result := FALSE;
	if loader.fullFileSize < 54 then raise DecompException.Create('file too tiny');

	try
		_LoadBits(loader);

		if length(tempimage.palette) > 16 then with tempimage do begin
			transparentindex := 16;
			bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
		end;
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(buffy, imagebytesize);
		SaveFile(metadata.srcFilePath, buffy, imagebytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_SognaANM(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an ANM graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
// These were used by Sogna's Windows games.
type anmframe = record
		startofs : dword;
		ofsx, ofsy, width, height : word;
		bmp : array of byte;
	end;
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	frames : array of anmframe = NIL;
	pal : TSRGBPalette = NIL;
	transparentindex : dword = high(dword);

	procedure _Finalise;
	var buffy : pointer = NIL;
		imagebytesize : dword;
	begin
		try
			// The transparent color is always index 12, but save without transparency if no such pixels in image.
			transparentindex := high(dword);
			if FindColorInImage(tempimage, 12) >= 0 then begin
				transparentindex := 12;
				tempimage.bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
				tempimage.palette[12].a := 0;
			end;
			PostProcessGraphic(tempimage, metadata, game, transparentindex);

			tempimage.MakePNG(buffy, imagebytesize);
			SaveFile(metadata.srcFilePath, buffy, imagebytesize);

			AddOrReplaceGraphicFile(metadata);
			metadata := NIL;
		finally
			if buffy <> NIL then freemem(buffy);
			if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
			if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		end;
	end;

	procedure _Read(const src : TFileLoader);
	var framep : pointer;
		i, size : dword;
		same : boolean = TRUE;

		procedure _Unpack(w, h : dword);
		var startp, writep : pointer;
			cols, rows, prev, reps : dword;
		begin
			startp := framep;
			for cols := (w shr 2) - 1 downto 0 do begin
				prev := NOT (dword(src.readp^));
				writep := startp;
				inc(startp, 4);

				rows := h;
				while rows <> 0 do begin
					if src.readp >= src.endp then begin
						decomp_logger('[!] out of data');
						exit;
					end;
					if dword(src.readp^) <> prev then begin
						// Output literal quad.
						prev := src.ReadDword;
						dword(writep^) := prev;
						inc(writep, tempimage.stride);
						dec(rows);
					end
					else begin
						// Output RLE repeats.
						inc(src.readp, 4);
						reps := src.ReadByte;
						if reps = 0 then reps := src.ReadByte + 256;
						if reps <= rows then
							dec(rows, reps)
						else begin
							decomp_logger('[!] rep overflow col '+strdec((w shr 2) - 1 - cols));
							reps := rows;
							rows := 0;
						end;
						while reps <> 0 do begin
							dword(writep^) := prev;
							inc(writep, tempimage.stride);
							dec(reps);
						end;
						prev := NOT (dword(src.readp^)); // no repeat after this even if a third such quad follows
					end;
				end;
			end;
		end;

	begin
		// Palette always seems to start with 33 zeroes, check for that.
		for i := 0 to 7 do if src.ReadDword <> 0 then raise DecompException.Create('non-0 in early palette');
		src.ofs := 0;
		setlength(pal, 256);
		for i := 0 to 255 do with pal[i] do begin
			b := src.ReadByte;
			g := src.ReadByte;
			r := src.ReadByte;
			a := $FF;
		end;

		src.ofs := $300;
		i := LEtoN(src.ReadDword);
		if (i = 0) or (i > 64) then raise DecompException.Create('sus framecount');
		setlength(frames, i);

		for i := 0 to high(frames) do begin
			with frames[i] do begin
				startofs := $304 + LEtoN(src.ReadDword);
				if startofs + 4 > src.fullFileSize then raise DecompException.Create('frame oob');
				ofsx := LEtoN(src.ReadWordFrom(startofs));
				ofsy := LEtoN(src.ReadWordFrom(startofs + 2));
				width := LEtoN(src.ReadWordFrom(startofs + 4));
				height := LEtoN(src.ReadWordFrom(startofs + 6));
				size := width * height;
				if (size = 0) or (width and 3 <> 0) or (ofsx + width > 2500) or (ofsy + height > 2500) then
					raise DecompException.Create(strcat('sus framesize: %x% @ %,%', [width, height, ofsx, ofsy]));
				inc(startofs, 8);
				setlength(bmp, size);
			end;
			if (same) and (i <> 0) then
				same := (frames[i].ofsx = frames[0].ofsx) and (frames[i].ofsy = frames[0].ofsy) and
					(frames[i].width = frames[0].width) and (frames[i].height = frames[0].height);
		end;

		if same then begin
			// Frames are the same size, can easily put them in the same bitmap.
			tempimage := mcg_bitmap.Init(frames[0].width, frames[0].height * length(frames), MCG_FORMAT_INDEXED, 8);
			setlength(tempimage.palette, 256);
			move(pal[0], tempimage.palette[0], 1024);
			framep := @tempimage.bitmap[0];
			metadata := CopyOrCreateGraphicFile(upcase(basename));
			with metadata do begin
				srcFilePath := PathCombine([outdir, basename], 'png');
				origSizeXP := tempimage.sizeXP;
				origSizeYP := tempimage.sizeYP;
				origFrameHeightP := frames[0].height;
				frameCount := length(frames);
				origOfsXP := frames[0].ofsx;
				origOfsYP := frames[0].ofsy;
			end;

			for i := 0 to high(frames) do with frames[i] do begin
				src.ofs := startofs;
				_Unpack(width, height);
				inc(framep, tempimage.stride * metadata.origFrameHeightP);
			end;

			_Finalise;
		end
		else begin
			// Frames have different properties, write each in a separate file.
			for i := 0 to high(frames) do with frames[i] do begin
				tempimage := mcg_bitmap.Init(width, height, MCG_FORMAT_INDEXED, 8);
				setlength(tempimage.palette, 256);
				move(pal[0], tempimage.palette[0], 1024);
				framep := @tempimage.bitmap[0];
				metadata := CopyOrCreateGraphicFile(upcase(basename) + '.' + strdec(i));
				with metadata do begin
					srcFilePath := PathCombine([outdir, basename + '.' + strdec(i)], 'png');
					origSizeXP := width;
					origSizeYP := height;
					origFrameHeightP := height;
					frameCount := 1;
					origOfsXP := ofsx;
					origOfsYP := ofsy;
				end;
				src.ofs := startofs;
				_Unpack(width, height);
				_Finalise;
			end;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 784 then raise DecompException.Create('file too tiny');

	try
		_Read(loader);
	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
	end;
	result := TRUE;
end;

