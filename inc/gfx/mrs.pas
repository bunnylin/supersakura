{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_MRS(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an MRS-type graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	buffy : pointer = NIL;
	transparentindex, bytesize : dword;
	isXMG : boolean = FALSE;

	procedure _DecryptHeader;
	// XMG images used by ZyX have an encrypted header, otherwise it's the same MRS format.
	var i : dword;
		key : byte = 0;
	begin
		for i := 779 downto 0 do begin
			byte(loader.readp^) := ((byte(loader.readp^) - key) and $FF) xor $F3;
			inc(loader.readp);
			key := (key + 7) and $FF;
		end;
		loader.ofs := 0;
		isXMG := TRUE;
	end;

	procedure _LoadBitsFrom(const src : TFileLoader);
	var writep, imgendp : pointer;
		i, dist, w, h : dword;
		topcolor : byte = 0;
	begin
		i := LEtoN(src.ReadDword);
		if (not isXMG) and (i <> $4443) and (i <> $4F44) then raise DecompException.Create('missing CD/DO sig');

		w := LEtoN(src.ReadWord);
		h := LEtoN(src.ReadWord);

		if (w > 640) or (h > 400) or (w = 0) or (h = 0) then
			raise DecompException.Create(strcat('sus image size %x%', [w, h]));
		i := LEtoN(src.ReadDword);
		if (i <> 0) and (decomp_param.verbose) then decomp_logger('[!] non-0 fifth value: $' + strhex(i));

		tempimage := mcg_bitmap.Init(w, h, MCG_FORMAT_INDEXED, 8);
		metadata := CopyOrCreateGraphicFile(upcase(basename));

		with tempimage do with metadata do begin
			origOfsXP := 0;
			origOfsYP := 0;
			origSizeXP := sizeXP;
			origSizeYP := sizeYP;
			srcFilePath := PathCombine([outdir, basename], 'png');

			// Get the palette.
			setlength(palette, 256);
			for i := 0 to 255 do begin
				palette[i].b := src.ReadByte;
				palette[i].r := src.ReadByte;
				palette[i].g := src.ReadByte;
				palette[i].a := $FF;
			end;

			// The compressed image stream should immediately follow the section sizes.
			setlength(bitmap, sizeXP * sizeYP);
			stride := sizeXP;
			writep := @bitmap[0];
			imgendp := writep + length(bitmap);
			Assert(src.ofs = 780);
			while (src.readp < src.endp) and (writep < imgendp) do begin
				i := src.ReadByte;
				case i and $C0 of
					// Literal bytes.
					$00:
					begin
						if i = 0 then i := src.ReadByte + 64;
						if writep + i > imgendp then break;
						while i <> 0 do begin
							byte(writep^) := src.ReadByte;
							if byte(writep^) > topcolor then topcolor := byte(writep^);
							inc(writep);
							dec(i);
						end;
					end;

					// Copy previous byte.
					$40:
					begin
						if writep = @bitmap[0] then begin
							LogError('copy prev out of bounds @ $' + strhex(src.ofs));
							break;
						end;
						i := (i and $3F) + 1;
						if i = 1 then i := src.ReadByte + 65;
						if writep + i > imgendp then break;
						fillbyte(writep^, i, byte((writep - 1)^));
						inc(writep, i);
					end;

					// Copy sequence.
					else begin
						dist := ((i and $F) shl 8) + src.ReadByte + 1;
						if dist > dword(writep - @bitmap[0]) then begin
							LogError('copy seq out of bounds @ $' + strhex(src.ofs));
							break;
						end;
						i := (i shr 4) and 7 + 2;
						if i = 2 then i := src.ReadByte + 10;
						if writep + i > imgendp then break;
						MemCopy(writep - dist, writep, i);
						inc(writep, i);
					end;
				end;
			end;
			setlength(palette, topcolor + 1);
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 800 then raise DecompException.Create('file too tiny');

	try
		if (game = gid.TwilightHotel_w) and (upcase(ExtractFileExt(loader.filename)) = '.XMG') then _DecryptHeader
		else if BEtoN(loader.ReadWordFrom(2)) = $0108 then begin
			bytesize := BEtoN(word(loader.readp^));
			if (bytesize = $F3FA) or (bytesize = $ABB2) then _DecryptHeader; // XMG graphics
		end;
		_LoadBitsFrom(loader);
		freemem(buffy); buffy := NIL;

		transparentindex := SelectTransparentIndex(CN_MRS, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(buffy, bytesize);
		SaveFile(metadata.srcFilePath, buffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
	end;
	result := TRUE;
end;

