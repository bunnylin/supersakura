{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_DA1(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a DA1 graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	buffy : pointer = NIL;
	plane, sectionbasep : array[0..4] of pointer;
	sectionsizes : array[0..3] of word;
	maxbounds : record
		left, right, top, bottom : dword;
	end;
	i, j : dword;
	bytesize, transparentindex : dword;
	imageflags : byte;

	function _GetBaseGraphic(const imgname : UTF8string; out paletteonly : boolean; imghaspal : boolean) : UTF8string;
	var suffix : string[3];
	begin
		paletteonly := FALSE;
		result := ''; suffix := 'gdt';
		case game of
			gid.Amy98:
			case imgname of
				'008N': result := '008';
				'008NB': ;
				'011B': result := '011N';
				'100C','100D': result := '100B';
				'160D','160E': result := '160';
				'190': result := '190B';
				'190B': ;
				'660B': ;
				'750_3': result := '750_2';
				'MENU3','MENU4': result := 'MENU2';
				'TAITORU': result := 'J10';
				'TOBE': ;
				else
				if (imgname.Length > 2) and (imgname[imgname.Length] in ['B'..'J'])
				and (imgname[imgname.Length - 1] in ['0'..'9','B','N'])
				then begin
					result := copy(imgname, 1);
					if imgname[imgname.Length] = 'B' then
						setlength(result, result.Length - 1)
					else
						dec(byte(result[result.Length]));
				end;
			end;

			gid.Bacta1:
			begin
				suffix := 'da1';
				result := imgname;
				case imgname of
					'B-02.2','B-02.3','B-02.4': result := 'B-02.1';
					'B-05.2','B-05.3','B-05.4': dec(byte(result[result.Length]));
					'B-06.1','B-06.2','B-06.3','B-06.4','B-06.5','B-06.6': result := 'B-06.0';
					'B-08-1','B-08-2','B-09-1','B-13-1': setlength(result, result.Length - 2);
					'B-14.3': result[6] := '1';
					'B-14B.2','B-14B.3': begin result[7] := '0'; paletteonly := TRUE; end;
					'B-24.3': result[6] := '1';
					'B-30.2': result[6] := '0';
					'B-35.2','B-35.3': result[6] := '0';
					'B-38.3': begin result[6] := '1'; paletteonly := TRUE; end;
					'TITLE.2','TITLE.4','TITLE.6','TITLE.8': dec(byte(result[result.Length]), 2);
					else begin
						result := '';
						if NOT imghaspal then begin
							if copy(imgname, 1, 3) = 'END' then begin
								result := 'END.12';
								paletteonly := TRUE;
							end
							else begin
								result := imgname;
								dec(byte(result[result.Length])); // use the previous numbered image
							end;
						end;
					end;
				end;
			end;

			gid.Desire98:
			case imgname of
				'030C': result := '030';
				'150C': begin result := '150'; paletteonly := TRUE; end;
				'1100C': begin result := '1100'; paletteonly := TRUE; end;
				'1260C': begin result := '1260'; paletteonly := TRUE; end;
				'420C': begin result := '420'; paletteonly := TRUE; end;
				'420D': result := '420';
				'MENU2','MENU3','MENU4': result := 'MENU1';
				'TITLE2': begin result := 'TITLE'; paletteonly := TRUE; end;
				'350B','645B','1400B': ;
				else
				if (imgname.Length > 2) and (imgname[imgname.Length] in ['B'..'J'])
				and (imgname[imgname.Length - 1] in ['0'..'9','B','N'])
				then begin
					result := copy(imgname, 1);
					if imgname[imgname.Length] = 'B' then
						setlength(result, result.Length - 1)
					else
						dec(byte(result[result.Length]));
				end;
			end;

			gid.EtsuGaku98:
			case imgname of
				'040C', '050C': ;
				'060E': result := '060D';
				'300C': result := '300B';
				else
				if (imgname.Length > 2) and (imgname[imgname.Length] in ['B'..'F'])
				and (imgname[imgname.Length - 1] in ['0'..'9']) then
					result := copy(imgname, 1, imgname.Length - 1);
			end;

			gid.EveBE98:
			case imgname of
				'0005H','001B','001C','006B','006C','007B','007C','007D','009B','009C','009D','010B','010C','010D',
				'011B','011E','011F','0140B','0140C','014B','014D','0150B','018B','018C','028B','028C','028D','028E',
				'0290B','029B','029C','030B','034B','034C','034D','035B','0357B','0450B','0490B','0490E','0490F',
				'0540C','0565B','0680B','0690C','1035B','1035C','2010B','2010F','3010B','3070B': ;
				// 0610B is used in c02 on top of 0610, but it looks identical... and on its own, it's a null screen.
				'007E': result := '007C';
				'007F': result := '007D';
				'007G': result := '007';
				'002E': result := '002';
				'0005B','0005C','0005D','0005E','0005F','0005G','0005J': result := '0005';
				'0140': result := '0140B';
				'014C': result := '014';
				'0150': result := '0150B';
				'0280C': result := '0280';
				'029D': result := '029';
				'030C': result := '030';
				'031C': result := '031';
				'032-2C': result := '032-2';
				'0450C': result := '0450';
				'0490D': result := '0490';
				'0690': result := '0690C';
				'1070C': result := '1070';
				'1070E': result := '1070B';
				else
				if (imgname.Length > 2) and (imgname[imgname.Length] in ['B'..'J'])
				and (imgname[imgname.Length - 1] in ['0'..'9'])
				then begin
					result := copy(imgname, 1);
					if imgname[imgname.Length] = 'B' then
						setlength(result, result.Length - 1)
					else
						dec(byte(result[result.Length]));
				end;
			end;

			gid.Xenon98:
			case imgname of
				'1010B': result := '1010';
				'1010C': begin result := '1010'; paletteonly := TRUE; end;
				'BI_BA_B','FA_BA_B','MAI_BA_B','MEG_BA_B','RA_BA_B','RYO_BA_B','SA_BA_B','TO_BA_B':
				result := copy(imgname, 1, imgname.Length - 2) + 'ST';
				'0040B', '0100B','0100BB','BI02B','RA&TO03B','RA&TO03C','RYO01B': ;
				'MEG01C': result := 'MEG01';
				else
				if (imgname.Length > 2) and (imgname[imgname.Length] in ['B'..'E'])
				and (imgname[imgname.Length - 1] in ['0'..'9','S'])
				then begin
					result := copy(imgname, 1);
					if imgname[imgname.Length] = 'B' then
						setlength(result, result.Length - 1)
					else
						dec(byte(result[result.Length]));
				end;
			end;

			gid.YES:
			begin
				suffix := 'da1';
				case imgname of
					'DCH_2': result := 'DCH_1';
					'TIT2': result := 'TIT1';
					'TIT3': result := 'TIT2';
				end;
			end;
		end;
		if result <> '' then result := result + '.' + suffix;
	end;

	procedure _UnpackSection8(
		const src : TFileLoader; startp, sectionendp, planep, writep, planeendp : pointer; singlecolumn : boolean);
	var d : dword;
		a, b : byte;
	begin
		// If only doing a single column, assume writep already has offsets applied.
		if NOT singlecolumn then inc(writep, (metadata.origOfsXP shr 3) * 400 + metadata.origOfsYP);
		src.readp := startp;
		while src.readp < sectionendp do begin
			a := src.ReadByte;
			case a of
				// 0x and 00 xx - clear all bits for xx bytes
				// 2x and 20 xx - set all bits for xx bytes
				$00..$3F:
				begin
					b := 0;
					if a >= $20 then b := $FF;

					a := a and $1F;
					if a = 0 then a := src.ReadByte;

					fillbyte(writep^, a, b);
					inc(writep, a);
				end;

				// 4x and 40 xx - import all bits from plane 0 for xx bytes
				// 6x and 60 xx - import all bits from plane 1 for xx bytes
				// 8x and 80 xx - import all bits from plane 2 for xx bytes
				$40..$9F:
				begin
					b := a and $E0;
					a := a and $1F;
					if a = 0 then a := src.ReadByte;
					case b of
						$40: MemCopy(plane[0] + (writep - planep), writep, a);
						$60: MemCopy(plane[1] + (writep - planep), writep, a);
						$80: MemCopy(plane[2] + (writep - planep), writep, a);
					end;
					inc(writep, a);
				end;

				// Ax and A0 xx - import from 16 bytes ago
				// Bx and B0 xx - import from 8 bytes ago
				// Cx and C0 xx - import from 4 bytes ago
				// Dx and D0 xx - import from 2 bytes ago
				// Ex and E0 xx - import from 2 columns ago
				$A0..$EF:
				begin
					b := a and $F0;
					a := a and $F;
					if a = 0 then a := src.ReadByte;
					case b of
						$A0: MemCopy(writep - 16, writep, a);
						$B0: MemCopy(writep - 8, writep, a);
						$C0: MemCopy(writep - 4, writep, a);
						$D0: MemCopy(writep - 2, writep, a);
						$E0: MemCopy(writep - 400 * 2, writep, a);
					end;
					inc(writep, a);
				end;

				// Fx and F0 xx - direct values for xx bytes
				$F0..$F8:
				begin
					a := a and $F;
					if a = 0 then a := src.ReadByte;
					while a <> 0 do begin
						dec(a);
						byte(writep^) := src.ReadByte;
						inc(writep);
					end;
				end;

				// F9 xx - skip xx bytes (use bits from base image)
				$F9: inc(writep, src.ReadByte);

				// FA xx yy - direct value yy for xx bytes
				$FA:
				begin
					a := src.ReadByte;
					fillbyte(writep^, a, src.ReadByte);
					inc(writep, a);
				end;

				// FB xx - import xx bytes from plane 0 or plane 1, inverted
				$FB:
				begin
					a := src.ReadByte;
					b := a shr 7;
					a := a and $7F;
					while a <> 0 do begin
						dec(a);
						byte(writep^) := byte((plane[b] + (writep - planep))^) xor $FF;
						inc(writep);
					end;
				end;

				// FC 0x - import x bytes from plane 2, inverted
				// FC 8y vw - repeat byte pair ww+vv y times
				$FC:
				begin
					a := src.ReadByte;
					if a and $80 = 0 then begin
						while a <> 0 do begin
							dec(a);
							byte(writep^) := byte((plane[2] + (writep - planep))^) xor $FF;
							inc(writep);
						end;
					end
					else begin
						b := src.ReadByte;
						a := a and $7F;
						// Using LSB! b = vw -> d = vvww
						d := (word(b and $F) + word(b shl 4)) + dword((b and $F0) shl 8);
						fillword(writep^, a, NtoLE(word(d)));
						inc(writep, a * 2);
					end;
				end;

				// FD 0x yy zz - repeat byte pair yy+zz x times
				// FD 8x yz - repeat byte quartet zz+yy+(zz+yy rotated by two) x times
				// FD Cx yz vw - repeat byte quartet zz+yy+ww+vv x times
				$FD:
				begin
					a := src.ReadByte;
					if a and $80 = 0 then begin
						fillword(writep^, a, src.ReadWord);
						inc(writep, a * 2);
					end
					else begin
						b := src.ReadByte;
						// Using LSB!
						// b = yz -> d = yyzz
						d := dword(word(b and $F) + word(b shl 4)) + dword((b and $F0) shl 8);
						if a and $C0 = $80 then begin
							// yyzz -> YYZZyyzz (top two bytes: rotate each by two within itself)
							d := d + ((d and $3F3F) shl 18) + ((d and $C0C0) shl 10);
						end
						else begin
							// yyzz + (vw -> vvww) = vvwwyyzz
							b := src.ReadByte;
							d := d + (dword((b and $F) shl 16) + dword(b shl 20)) + dword((b and $F0) shl 24);
						end;
						a := a and $3F;
						filldword(writep^, a, NtoLE(d));
						inc(writep, a * 4);
					end;
				end;

				// FE 4x - import x bytes from plane 0 bitwise AND plane 1
				// FE 8x - import x bytes from plane 0 bitwise AND plane 2
				// FE Cx - import x bytes from plane 1 bitwise AND plane 2
				// FE xx aabbccdd - repeat byte quartet x times
				$FE:
				begin
					a := src.ReadByte;
					b := a and $C0;
					if b = 0 then begin
						filldword(writep^, a, src.ReadDword);
						inc(writep, a * 4);
					end
					else begin
						a := a and $3F;
						b := b shr 6;
						d := writep - planep;
						while a <> 0 do begin
							dec(a);
							byte(writep^) := byte((plane[(b and 1)] + d)^) and byte((plane[(b and 2)] + d)^);
							inc(writep);
							inc(d);
						end;
					end;
				end;

				// FF end of column
				$FF: if singlecolumn then break else inc(writep, 400 - metadata.origSizeYP);
			end;
			if writep > planeendp then break;
		end;

		if (NOT singlecolumn) and (src.readp + 4 < sectionendp) then raise DecompException.Create(
			'Plane done, but input bit stream still has ' + strdec(sectionendp - src.readp) + ' bytes');
	end;

	procedure _UnpackSection16(const src : TFileLoader; startp, sectionendp, planep, planeendp : pointer);
	var copyp, copyp2, writep : pointer;
		d, columnsleft : dword;
		w : word;
		a, b : byte;

		procedure _Error;
		begin
			LogError(strcat('UNK CODE $&-& srcofs $&', [a, b, src.ofs - 2]));
		end;

	begin
		src.readp := startp;
		writep := planep + (metadata.origOfsXP shr 3) * 400 + metadata.origOfsYP;
		columnsleft := metadata.origSizeXP shr 3; // using 8px columns for output regardless of compressed mode

		// If the start offset isn't at a 16px boundary, then there's one 8px column at the start of the byte stream.
		if metadata.origOfsXP and 8 <> 0 then begin
			_UnpackSection8(src, startp, sectionendp, planep, writep, writep + metadata.origSizeYP + 2, TRUE);
			inc(writep, 400);
			dec(columnsleft);
		end;
		// Byte stream at 16px layout must be aligned to word boundary.
		if src.ofs and 1 <> 0 then inc(src.readp);
		// An image of 8-16px width could be taken care of by one or two 8px columns right away.
		if columnsleft = 1 then
			_UnpackSection8(src, src.readp, sectionendp, planep, writep, writep + metadata.origSizeYP + 2, TRUE);

		if columnsleft > 1 then while src.readp < sectionendp do begin
			a := src.ReadByte;
			b := src.ReadByte;
			case b of
				// 0x ab - x repeats of 2-row pattern
				// [bb] [bb]
				// [aa] [aa]
				$01..$7F:
				begin
					w := (byte(a and $F) + word((a and $F0) shl 4)) * 17; // ab -> 0a0b -> aabb
					w := NtoLE(w);
					fillword(writep^, b, w);
					fillword((writep + 400)^, b, w);
					inc(writep, b * 2);
				end;

				// 8x ab - x repeats of 4-row rotated pattern, eg. A1 -> 11 AA 44 AA
				// [bb] [bb]
				// [aa] [aa]
				// [dd] [dd], d = b rol 2
				// [cc] [cc], c = a rol 2
				$81..$BF:
				begin
					w := (byte(a and $F) + (a and $F0) shl 4); // ab -> 0a0b
					// 0a0b -> 0c0d0a0b (low nibbles of top two bytes: rotate each by two within itself)
					d := w + (w and $0303) shl 18 + (w and $0C0C) shl 14;
					d := NtoLE(d * (1 + 16)); // 0c0d0a0b -> ccddaabb
					b := b and $3F;
					filldword(writep^, b, d);
					filldword((writep + 400)^, b, d);
					inc(writep, b * 4);
				end;

				// Cx yy - yy rows of xx
				// [xx] [xx]
				$C0..$CF:
				begin
					if a = 0 then begin _Error; break; end;
					b := (b and $F) * (1 + 16);
					fillbyte(writep^, a, b);
					fillbyte((writep + 400)^, a, b);
					inc(writep, a);
				end;

				// D0 xx - xx rows of direct values
				$D0..$D1:
				begin
					d := (a + (b and 1) shl 8);
					while d <> 0 do begin
						byte(writep^) := src.ReadByte;
						byte((writep + 400)^) := src.ReadByte;
						inc(writep);
						dec(d);
					end;
				end;

				// D2 xx - skip xx rows (use bits from base image)
				$D2:
				inc(writep, a);

				// D3..F2 xx - copy xx rows from earlier
				$D3..$F2:
				begin
					if a = 0 then begin _Error; break; end;
					case b of
						//D2: d := 24;
						$D3: d := 16; // earlier in same column
						$D4: d := 12;
						$D5: d := 8;
						$D6: d := 4;
						$D7: d := 2;
						$D8: d := 1;
						$D9: d := 800 + 8; // previous column
						$DA: d := 800 + 4;
						$DB: d := 800 + 2;
						$DC: d := 800 + 1;
						$DD: d := 800;
						$DE: d := 800 - 1;
						$DF: d := 800 - 2;
						$E0: d := 800 - 4;
						$E1: d := 800 - 8;
						$E2: d := 1600 + 8; // two columns ago
						$E3: d := 1600 + 4;
						$E4: d := 1600 + 2;
						$E5: d := 1600 + 1;
						$E6: d := 1600;
						$E7: d := 1600 - 1;
						$E8: d := 1600 - 2;
						$E9: d := 1600 - 4;
						$EA: d := 1600 - 8;
						$EB: d := 2400 + 4; // three columns ago
						$EC: d := 2400 + 2;
						$ED: d := 2400 + 1;
						$EE: d := 2400;
						$EF: d := 2400 - 1;
						$F0: d := 2400 - 2;
						$F1: d := 2400 - 4;
						$F2: d := 3200; // four columns ago
					end;
					MemCopy(writep - d, writep, a);
					MemCopy(writep - d + 400, writep + 400, a);
					inc(writep, a);
				end;

				// F3..F5 xx - copy xx rows from plane 0..2
				// F6..F8 xx - same, inverted
				// F9..FB xx - same, but plane AND plane (0+1, 0+2, 1+2)
				$F3..$FB:
				begin
					if a = 0 then begin _Error; break; end;
					copyp := plane[(b - $F3) mod 3] + (writep - planep);
					if b <= $F5 then begin
						MemCopy(copyp, writep, a);
						MemCopy(copyp + 400, writep + 400, a);
						inc(writep, a);
					end
					else if b >= $F9 then begin
						dec(b, $F8);
						copyp := plane[b and 1] + (writep - planep);
						copyp2 := plane[b and 2] + (writep - planep);
						while a <> 0 do begin
							byte(writep^) := byte(copyp^) and byte(copyp2^);
							byte((writep + 400)^) := byte((copyp + 400)^) and byte((copyp2 + 400)^);
							inc(writep);
							inc(copyp);
							inc(copyp2);
							dec(a);
						end;
					end
					else while a <> 0 do begin
						byte(writep^) := byte(copyp^) xor $FF;
						byte((writep + 400)^) := byte((copyp + 400)^) xor $FF;
						inc(writep);
						inc(copyp);
						dec(a);
					end;
				end;

				// FC yy abcd - yy rows of abcd
				// [ab] [cd]
				$FC:
				begin
					fillbyte(writep^, a, src.ReadByte);
					fillbyte((writep + 400)^, a, src.ReadByte);
					inc(writep, a);
				end;

				// FD 0x aabb - x repeats of 2-row pattern aaaa bbbb
				// [aa] [aa]
				// [bb] [bb]
				// FD 8x abcd efgh - x repeats of 2-row pattern abcd efgh
				// [ab] [cd]
				// [ef] [gh]
				$FD:
				begin
					case a of
						$01..$7F:
						begin
							w := src.ReadWord;
							fillword(writep^, a, w);
							fillword((writep + 400)^, a, w);
							inc(writep, a * 2);
						end;
						$81..$FF:
						begin
							a := a and $7F;
							w := byte(src.readp^) + byte((src.readp + 2)^) shl 8;
							fillword(writep^, a, NtoLE(w));
							w := byte((src.readp + 1)^) + byte((src.readp + 3)^) shl 8;
							fillword((writep + 400)^, a, NtoLE(w));
							inc(writep, a * 2);
							inc(src.readp, 4);
						end;
						else begin _Error; break; end;
					end;
				end;

				// FE 4x abcd - x repeats of 4-row pattern bbbb aaaa dddd cccc
				// [bb] [bb]
				// [aa] [aa]
				// [dd] [dd]
				// [cc] [cc]
				// FE Cx abcd efgh ijkl mnop - x repeats of 4-row pattern
				// [ab] [cd]
				// [ef] [gh]
				// [ij] [kl]
				// [mn] [op]
				$FE:
				begin
					b := a and $3F;
					case a of
						$41..$7F:
						begin
							w := LEtoN(src.ReadWord); // first byte ab, second byte cd -> cdab
							d := w + (w and $FF00) shl 8; // cdab -> 00 cd 00 ab
							d := d and $000F000F + (d and $00F000F0) shl 4; // -> 0c 0d 0a 0b
							d := d * (1 + 16); // -> cc dd aa bb
							d := NtoLE(d);

							filldword(writep^, b, d);
							filldword((writep + 400)^, b, d);
						end;
						$C1..$FF:
						begin
							d := byte(src.readp^) + byte((src.readp + 2)^) shl 8 + byte((src.readp + 4)^) shl 16 + byte((src.readp + 6)^) shl 24;
							filldword(writep^, b, NtoLE(d));
							d := byte((src.readp + 1)^) + byte((src.readp + 3)^) shl 8 + byte((src.readp + 5)^) shl 16 + byte((src.readp + 7)^) shl 24;
							filldword((writep + 400)^, b, NtoLE(d));
							inc(src.readp, 8);
						end;
						else begin _Error; break; end;
					end;
					inc(writep, b * 4);
				end;

				// FF - end of column
				$FF:
				begin
					inc(writep, 800 - metadata.origSizeYP);
					dec(columnsleft, 2);
					if columnsleft = 0 then break;
					// If the image's right edge isn't at a 16px boundary, the last column uses 8px layout.
					if columnsleft = 1 then begin
						_UnpackSection8(
							src, src.readp, sectionendp, planep, writep, writep + metadata.origSizeYP + 2, TRUE);
						break;
					end;
				end;

				else begin _Error; break; end;
			end;
			if writep > planeendp then break;
		end;

		if src.readp + 4 < sectionendp then raise DecompException.Create(
			'Plane done, but input bit stream still has ' + strdec(sectionendp - src.readp) + ' bytes');
	end;

	procedure _LoadBitsFrom(const src : TFileLoader; _paletteonly : boolean);
	var dependency : UTF8string;
		deploader : TFileLoader = NIL;
		i, j : dword;
		paletteonly : boolean;
	begin
		if LEtoN(src.ReadDword) <> $314144 then raise DecompException.Create('no da1 sig');
		if LEtoN(src.ReadDword) > src.fullFileSize then raise DecompException.Create('size in header > filesize');

		dependency := _GetBaseGraphic(upcase(basename), paletteonly, src.ReadByteFrom(src.ofs + 7) >= $80);
		if dependency <> '' then begin
			if decomp_param.verbose then decomp_logger('loading base graphic ' + dependency);
			try
				deploader := TFileLoader.Open(Pathcombine([ExtractFileDir(src.filename), dependency]));
				try
					_LoadBitsFrom(deploader, paletteonly);
				finally
					deploader.Destroy;
				end;
			except
				on e : Exception do LogError(e.Message);
			end;
		end;

		if _paletteonly then
			inc(src.readp, 8)
		else begin
			metadata := CopyOrCreateGraphicFile(upcase(basename));
			with metadata do begin
				srcFilePath := PathCombine([outdir, basename], 'png');
				frameCount := 0; // pass through MarkAnims in post
				origFrameHeightP := 0;
				origOfsXP := src.ReadByte * 8;
				origSizeXP := src.ReadByte * 8;
				origOfsYP := LEtoN(src.ReadWord);
				origSizeYP := LEtoN(src.ReadWord);

				if (origSizeXP > 640) or (origSizeYP > 400) or (origSizeXP = 0) or (origSizeYP = 0) then
					raise DecompException.Create(strcat('sus image size %x%', [origSizeXP, origSizeYP]));
				if (origOfsXP > 639) or (origOfsYP > 399) then
					raise DecompException.Create(strcat('sus image ofs %,%', [origOfsXP, origOfsYP]));
			end;

			// Stash the furthest base graphic bounds, must have the whole composite for clean beautifying.
			with maxbounds do begin
				if dword(metadata.origOfsXP) < left then left := metadata.origOfsXP;
				if dword(metadata.origOfsYP) < top then top := metadata.origOfsYP;
				i := metadata.origOfsXP + metadata.origSizeXP;
				if i > right then right := i;
				i := metadata.origOfsYP + metadata.origSizeYP;
				if i > bottom then bottom := i;
			end;

			inc(src.readp); // unknown byte
			imageflags := src.ReadByte;
			if NOT (imageflags in [$0F, $4F, $8F, $CF]) then
				raise DecompException.Create('unknown flag value $' + strhex(imageflags));
		end;

		tempimage := mcg_bitmap.Create(); // only gets fully inited in _CombinePlanes
		with tempimage do begin
			bitmap := NIL;
			palette := NIL;
		end;

		// Get palette if it is present. Saved as three nibbles each, BRG order.
		if (_paletteonly) or (imageflags and $80 <> 0) then begin
			setlength(tempimage.palette, 16);
			i := 0;
			while i < 16 do begin
				j := src.ReadByte;
				tempimage.palette[i].b := j and $F0 + j shr 4;
				tempimage.palette[i].r := j and $F + (j shl 4) and $F0;
				j := src.ReadByte;
				tempimage.palette[i].g := j and $F0 + j shr 4;
				tempimage.palette[i].a := $FF;
				inc(i);
				tempimage.palette[i].b := j and $F + (j shl 4) and $F0;
				j := src.ReadByte;
				tempimage.palette[i].r := j and $F0 + j shr 4;
				tempimage.palette[i].g := j and $F + (j shl 4) and $F0;
				tempimage.palette[i].a := $FF;
				inc(i);
			end;
			if _paletteonly then exit;
		end;

		// The compressed image is split in four sections, each containing one bitplane of the final image.
		// The first plane is bit 0, the last is bit 3. When combined, each pixel has 4 bits.
		for i := 0 to 3 do sectionsizes[i] := LEtoN(src.ReadWord);
		sectionbasep[0] := src.readp;
		for i := 0 to 3 do sectionbasep[i + 1] := sectionbasep[i] + sectionsizes[i];
		if sectionbasep[4] > src.endp then raise DecompException.Create('section out of bounds');

		// The compressed image stream should immediately follow the section sizes.
		if imageflags and $40 = 0 then
			for j := 0 to 3 do _UnpackSection16(src, sectionbasep[j], sectionbasep[j + 1], plane[j], plane[j + 1])
		else
			for j := 0 to 3 do
				_UnpackSection8(src, sectionbasep[j], sectionbasep[j + 1], plane[j], plane[j], plane[j + 1], FALSE);
	end;

	procedure _CombinePlanes;
	var writep : pointer;
		numsrcbytes, x, y, i : dword;
	begin
		with tempimage do with metadata do begin
			// Use the largest ofs and size that includes all base images.
			origOfsXP := maxbounds.left;
			origOfsYP := maxbounds.top;
			origSizeXP := maxbounds.right - maxbounds.left;
			origSizeYP := maxbounds.bottom - maxbounds.top;
			x := (origOfsXP shr 3) * 400 + origOfsYP;
			for i := 3 downto 0 do inc(plane[i], x);

			bitDepth := 4;
			bitmapFormat := MCG_FORMAT_INDEXED;
			sizeXP := origSizeXP;
			sizeYP := origSizeYP;
			stride := sizeXP shr 1;
			numsrcbytes := (sizeXP shr 3) * sizeYP;
			setlength(bitmap, numsrcbytes * 4);
			writep := @bitmap[0];

			x := 0; y := sizeYP;
			// 8px columns: Combine a byte from all four planes into a dword in the final image.
			while numsrcbytes <> 0 do begin
				dec(numsrcbytes);
				dword(writep^) := ExplodeBits(byte(plane[0]^)) + ExplodeBits(byte(plane[1]^)) shl 1
					+ ExplodeBits(byte(plane[2]^)) shl 2 + ExplodeBits(byte(plane[3]^)) shl 3;
				inc(plane[0]); inc(plane[1]); inc(plane[2]); inc(plane[3]);

				dec(y);
				if y <> 0 then
					inc(writep, stride)
				else begin
					y := sizeYP;
					inc(x, 4);
					for i := 3 downto 0 do inc(plane[i], 400 - sizeYP);
					writep := @bitmap[0] + x; // not @bitmap[x], goes out of range if image height 1
				end;
			end;

			// Hack: Some images have an extra row at the bottom.
			if (game = gid.Desire98) and (sizeYP = 265) then begin
				dec(sizeYP);
				dec(origSizeYP);
			end;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 28 then raise DecompException.Create('file too tiny');

	// Reserve a single buffer that can fit all 4 bit planes for the entire display.
	i := 640 * 400 div 2;
	getmem(buffy, i);
	fillbyte(buffy^, i, 0);
	i := i shr 2;
	plane[0] := buffy;
	for j := 1 to 4 do plane[j] := plane[j - 1] + i;

	with maxbounds do begin
		left := high(dword);
		top := high(dword);
		right := 0;
		bottom := 0;
	end;

	try
		_LoadBitsFrom(loader, FALSE);
		_CombinePlanes;
		freemem(buffy); buffy := NIL;

		transparentindex := SelectTransparentIndex(CN_DA1, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(buffy, bytesize);
		SaveFile(metadata.srcFilePath, buffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_DAD(const loader : TFileLoader; const basename, outputdir : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a DAD collection of DA1 graphics. The individual images are splatted into a temp
// directory and forwarded to the DA1 converter.
// Throws DecompException for bad failures, returns TRUE if successful.
var subloader : TFileLoader = NIL;
	filename : UTF8string = '';
	itemsig, da1size, i : dword;
begin
	result := FALSE;
	if loader.fullFileSize < 28 then raise DecompException.Create('file too tiny');

	if LEtoN(loader.ReadDword) <> $444144 then raise DecompException.Create('no dad sig');

	i := 0;
	while loader.readp + 8 < loader.endp do begin
		itemsig := LEtoN(dword(loader.readp^));
		if itemsig = $475242 then begin
			if decomp_param.verbose then decomp_logger('dad BRG chunk');
			inc(loader.readp, 28);
			continue;
		end;
		if itemsig <> $314144 then raise DecompException.Create('unknown dad content sig: ' + strhex(itemsig));

		da1size := LEtoN(loader.ReadDwordFrom(loader.ofs + 4));
		if loader.readp + da1size > loader.endp then
			raise DecompException.Create('DA1 out of bounds in dad @ $' + strhex(loader.ofs));

		filename := PathCombine([outputdir, 'temp', strcat('%.%.da1', [basename, i])]);
		SaveFile(filename, loader.readp, da1size);

		subloader := TFileLoader.FromMemory(loader.readp, da1size, FALSE);
		subloader.fileName := filename;
		if decomp_param.verbose then decomp_logger('converting index ' + strdec(i));

		try try
			if Decomp_DA1(subloader, gfxpath, basename + '.' + strdec(i), game) then begin
				// Keep track of newly-added graphics. The converter already added its metadata to graphicFiles.
				if newGfxCount >= newGfxList.Length then
					setlength(newGfxList, newGfxCount + newGfxCount shr 2 + 64);
				newGfxList[newGfxCount] := basename;
				inc(newGfxCount);
			end;
		except
			on e : DecompException do LogError(e.Message);
		end;
		finally
			subloader.Destroy; subloader := NIL;
		end;

		inc(loader.readp, da1size);
		inc(i);
	end;

	result := TRUE;
end;

