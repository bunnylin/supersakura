{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_AS2(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an AS2 graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	buffy : pointer = NIL;
	flag1, flag2 : byte;

type TBTree = array[0..31] of record
		truebranch, falsebranch : word;
		parent : byte;
		trueend, falseend, done : boolean;
	end;
var repetree, litetree : TBTree;
	table17a7 : array[0..31] of byte; // inited at start, one of each 0..1f in some order

	procedure _UnpackColumns(const src : TFileLoader);

		function _ReadBValue(const tree : TBTree) : word;
		// Read bit; if that branch is an end, return value; else branch to next node.
		begin
			result := 0;
			while TRUE do
				with tree[result] do
					if src.ReadBitW then begin
						result := truebranch;
						if trueend then exit;
					end
					else begin
						result := falsebranch;
						if falseend then exit;
					end;
		end;

		function _ReadVarLen : word;
		var i : byte;
		begin
			i := flag2 and 1;
			while (i < 8) and (src.ReadBitW = FALSE) do inc(i);
			result := longint(1 shl i) + longint(src.ReadBitsW(i)) - (flag2 and 1);
		end;

	var destp, endp : ^byte;
		i, copycount : word;
		lit : byte;
	begin
		destp := buffy; endp := destp + (metadata.origSizeXP shr 1) * metadata.origSizeYP;
		while destp < endp do begin
			i := _ReadBValue(repetree);
			if i = 0 then begin // repeat from 0 ago means output a literal
				if litetree[0].parent = 0 then // branchless
					destp^ := litetree[0].falsebranch
				else begin
					lit := _ReadBValue(litetree) shl 1; // risky to put both reads on one line, resolve order?
					destp^ := lit or _ReadBValue(litetree);
				end;
				inc(destp);
			end
			else begin
				copycount := _ReadVarLen;
				if i > destp - buffy then raise Exception.Create(strcat('copyfrom % > ofs %', [i, destp - buffy]));
				if destp + copycount > endp then
					raise Exception.Create(strcat('copycount % > remaining %', [copycount, endp - destp]));
				if i = 1 then
					fillbyte(destp^, copycount, (destp - 1)^)
				else
					MemCopy(destp - i, destp, copycount);
				inc(destp, copycount);
			end;
		end;
	end;

	procedure _BuildBTree(const src : TFileLoader; var tree : TBTree);
	const copydistlut : array[0..31] of record c : byte; r : shortint; end = (
		// positive = ago, negative = ahead
		(c:0; r:0),
		(c:1; r:0), (c:2; r:0), (c:3; r:0), (c:4; r:0), (c:8; r:0),
		(c:0; r:1), (c:1; r:1), (c:1; r:-1),
		(c:0; r:2), (c:1; r:2), (c:2; r:2), (c:4; r:2), (c:1; r:-2), (c:2; r:-2), (c:4; r:-2),
		(c:0; r:4), (c:2; r:4), (c:4; r:4), (c:2; r:-4), (c:4; r:-4),
		(c:0; r:8), (c:2; r:8), (c:4; r:8), (c:2; r:-8), (c:4; r:-8),
		(c:0; r:16),
		(c:2; r:1), (c:2; r:-1),
		(c:0; r:3), (c:1; r:3), (c:1; r:-3));
	var i, count : dword;
		node, freenode, bitdepth : byte;
		workbranch : boolean;
	begin
		node := 0; freenode := 0; workbranch := FALSE;
		if @tree = @repetree then bitdepth := 5 else bitdepth := 4;
		count := src.ReadBitsW(bitdepth);

		// Rarely 0-size trees are used, which always return the same value without reading any bits.
		if count = 0 then begin
			decomp_logger('[!] tree size 0');
			tree[0].parent := 0; // indicate branchless
			if bitdepth = 5 then begin
				raise Exception.Create('0-size repetree not implemented');
			end
			else begin
				// Literals tree.
				i := src.ReadBitsW(4);
				i := (i and 1) or ((i and 2) shl 1) or ((i and 4) shl 2) or ((i and 8) shl 3); // 00001111 -> 01010101
				tree[0].falsebranch := i * 3; // 01010101 -> 11111111
			end;
			exit;
		end;

		while count <> 0 do begin
			with tree[node] do begin
				if workbranch then begin
					truebranch := freenode;
					trueend := FALSE;
					done := TRUE;
					dec(count);
					workbranch := FALSE;
				end
				else begin
					falsebranch := freenode;
					falseend := FALSE;
				end;
				tree[freenode].parent := node;
				tree[freenode].done := FALSE;
			end;
			node := freenode;
			inc(freenode);
			if src.ReadBitsW(bitdepth) <> 0 then continue;

			repeat
				i := src.ReadBitsW(bitdepth);
				if bitdepth = 5 then with copydistlut[table17a7[i]] do
					i := longint(c * metadata.origSizeYP) + r // cast avoids range check error from negative r??
				else begin
					// $1 = $01: 0000 0001
					// $5 = $11: 0001 0001
					// $8 = $40: 0100 0000
					// $D = $51: 0101 0001
					i := (i and 3) or ((i and $C) shl 2); // 1111 -> 00110011
					i := (i and 17) or ((i and 34) shl 1); // -> 01010101
				end;

				with tree[node] do
					if workbranch then begin
						truebranch := i;
						trueend := TRUE;
						dec(count); // done := TRUE is unnecessary, we won't come back to this node
						if count = 0 then break;
						repeat node := tree[node].parent; until tree[node].done = FALSE;
					end
					else begin
						falsebranch := i;
						falseend := TRUE;
						workbranch := TRUE;
					end;
			until src.ReadBitsW(bitdepth) <> 0;
		end;
		tree[0].parent := $FF;
	end;

	procedure _FinaliseImage;
	var writep, readp : ^byte;
		numsrcbytes, x, y : dword;

		function _Delace(input : byte) : byte; inline;
		// 0123 4567 -> 0246 1357
		begin
			result := ((input and 2) shl 3)
				or ((input and 8) shl 2)
				or ((input and 32) shl 1)
				or ((input and 129))
				or ((input and 64) shr 3)
				or ((input and 16) shr 2)
				or ((input and 4) shr 1);
		end;

	begin
		with tempimage do begin
			numsrcbytes := stride * sizeYP;
			writep := @bitmap[0];
			readp := buffy;

			x := 0; y := sizeYP;
			while numsrcbytes <> 0 do begin
				dec(numsrcbytes);
				writep^ := _Delace(readp^);
				inc(readp);

				dec(y);
				if y <> 0 then
					inc(writep, sizeXP shr 1)
				else begin
					y := sizeYP;
					inc(x);
					writep := @bitmap[0] + x; // not @bitmap[x], goes out of range if image height 1
				end;
			end;
		end;
	end;

	procedure _LoadBitsFrom(const src : TFileLoader);
	var x, y, w, h, i, j : dword;
	begin
		if LEtoN(src.ReadDword) <> $325341 then raise DecompException.Create('missing AS2 sig');
		if LEtoN(src.ReadDword) > src.fullFileSize then raise DecompException.Create('size in header > filesize');

		x := src.ReadByte * 8;
		w := src.ReadByte * 8;
		y := LEtoN(src.ReadWord);
		h := LEtoN(src.ReadWord);

		if (w > 640) or (h > 400) or (w = 0) or (h = 0) then
			raise DecompException.Create(strcat('sus image size %x%', [w, h]));
		if (x > 639) or (y > 399) then
			raise DecompException.Create(strcat('sus image ofs %,%', [x, y]));

		flag1 := src.ReadByte; // $80 - palette present
		flag2 := src.ReadByte; // $01 - select varlen algorithm variant
		if (flag1 and $3F <> 0) or (flag2 > 1) then
			raise DecompException.Create(strcat('unknown flag $& $&', [flag1, flag2]));

		tempimage := mcg_bitmap.Init(w, h, MCG_FORMAT_INDEXED, 4);
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			srcFilePath := PathCombine([outdir, basename], 'png');
			origOfsXP := x;
			origOfsYP := y;
			origSizeXP := w;
			origSizeYP := h;
		end;

		// Get palette if it is present. Saved as three nibbles each, BRG order.
		if flag1 and $80 <> 0 then with tempimage do begin
			setlength(palette, 16);
			i := 0;
			while i < 16 do begin
				j := src.ReadByte;
				palette[i].b := j and $F0 + j shr 4;
				palette[i].r := j and $F + (j shl 4) and $F0;
				j := src.ReadByte;
				palette[i].g := j and $F0 + j shr 4;
				palette[i].a := $FF;
				inc(i);
				palette[i].b := j and $F + (j shl 4) and $F0;
				j := src.ReadByte;
				palette[i].r := j and $F0 + j shr 4;
				palette[i].g := j and $F + (j shl 4) and $F0;
				palette[i].a := $FF;
				inc(i);
			end;
		end;

		// Read the copy distances table.
		j := src.ReadBitsW(5);
		if j < 2 then raise DecompException.Create('expected initial 1F, got ' + strhex(j));
		for i := 0 to j do table17a7[i] := src.ReadBitsW(5);
		if j < $1F then fillbyte(table17a7[j + 1], $1F - j, 0);

		_BuildBTree(src, repetree);
		_BuildBTree(src, litetree);

		getmem(buffy, (metadata.origSizeXP shr 1) * metadata.origSizeYP);

		// The compressed image is saved as 8-bit columns. Every group of 4 columns is merged into an 8px 4bpp column.
		_UnpackColumns(src);
		_FinaliseImage;
	end;

var bytesize : dword;
begin
	result := FALSE;
	if loader.fullFileSize < 28 then raise DecompException.Create('file too tiny');

	try
		_LoadBitsFrom(loader);
		freemem(buffy); buffy := NIL;

		PostProcessGraphic(tempimage, metadata, game, high(dword));

		tempimage.MakePNG(buffy, bytesize);
		SaveFile(metadata.srcFilePath, buffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
	end;
	result := TRUE;
end;

