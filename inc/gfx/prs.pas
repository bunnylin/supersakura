{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_PRSMC(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a Micro Cabin PRS graphic. Adds its metadata to graphicFiles[] and saves the
// bitmap in outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;

	procedure _LoadBits(const src : TFileLoader);
	var writep, endp : pointer;
		i : dword;
		w, h : word;
		b : byte;
	begin
		w := LEtoN(src.ReadWord);
		if w >= $30 then raise DecompException.Create('sus 1st word');
		w := LEtoN(src.ReadWord);
		h := LEtoN(src.ReadWord);
		i := LEtoN(src.ReadWord);
		if (w = 0) or (h = 0) or (w > 80) or (h > 200) or (i >= 16000) then
			raise DecompException.Create('bad size or ofs');

		tempimage := mcg_bitmap.Init(w shl 3, h, MCG_FORMAT_INDEXED, 4);
		SetupPalette(tempimage, 8);
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do with tempimage do begin
			srcFilePath := PathCombine([outdir, basename], 'png');
			origSizeXP := w shl 3;
			origSizeYP := h;
			origFrameHeightP := h;
			origOfsXP := (i mod 80) shl 3;
			origOfsYP := i div 80;
			frameCount := 0; // pass through MarkAnims in post
		end;

		writep := @tempimage.bitmap[0];
		endp := writep + length(tempimage.bitmap);//w * h * 3;
		src.ofs := $C5; // skip the dict, to start of compressed data
		while writep < endp do begin
			b := src.ReadByte;
			w := b and 3;
			if w = 0 then begin
				w := src.ReadByte;
				if w = 0 then w := 256;
			end;
			b := b shr 2;
			if b = 63 then begin
				// Output a sequence of literals.
				if writep + w * 4 > endp then raise DecompException.Create('lits oob');
				while w <> 0 do begin
					i := ExplodeBits(src.ReadByte);
					i := i or (ExplodeBits(src.ReadByte) shl 1);
					i := i or (ExplodeBits(src.ReadByte) shl 2);
					dword(writep^) := i;
					inc(writep, 4);
					dec(w);
				end;
			end
			else begin
				// Output a repeat from the dictionary.
				if writep + w * 4 > endp then raise DecompException.Create('dics oob');
				i := ExplodeBits(src.ReadByteFrom(b + 8))
					or (ExplodeBits(src.ReadByteFrom(b + 71)) shl 1)
					or (ExplodeBits(src.ReadByteFrom(b + 134)) shl 2);
				filldword(writep^, w, i);
				inc(writep, w * 4);
			end;
		end;
		if src.RemainingBytes > 1 then
			raise DecompException.Create('Bmp full, but remaining input ' + strdec(src.RemainingBytes));
	end;

var buffy : pointer = NIL;
	imagebytesize : dword;
begin
	result := FALSE;
	if loader.fullFileSize < $C5 then raise DecompException.Create('file too tiny');

	try
		_LoadBits(loader);

		PostProcessGraphic(tempimage, metadata, game, high(dword));

		tempimage.MakePNG(buffy, imagebytesize);
		SaveFile(metadata.srcFilePath, buffy, imagebytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
	end;
	result := TRUE;
end;

function Decomp_PRS(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a Fairytale/Cocktail Soft PRS graphic. Adds its metadata to graphicFiles[] and
// saves the bitmap in outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	imagebytesize, transparentindex : dword;

	procedure _LoadBits(const src : TFileLoader);
	var cmdp, colendp, writep : pointer;
		datap : ^byte;
		lithistory : array[0..255] of dword;
		i, x, y, w, h : dword;
		pat1, pat2, defaultpat : dword;
		bitplane, bitplanemask, reps, historyindex : byte;

		procedure _OutputBytes;
		var tmp : dword;
		begin
			if writep + reps * tempimage.stride > colendp then //reps := (colendp - writep) div stride;
				raise DecompException.Create('column oob');
			while reps <> 0 do begin
				dword(writep^) := dword(writep^) or pat1;
				tmp := pat1; pat1 := pat2; pat2 := tmp;
				inc(writep, tempimage.stride);
				dec(reps);
			end;
		end;

		function _ChangeBitplane : boolean;
		// After increasing the bitplane, call this. Moves column ahead if needed, skips masked bitplanes.
		begin
			while TRUE do begin
				if bitplane = 3 then begin
					inc(x, 4);
					if x >= tempimage.stride then exit(TRUE);
					bitplane := 0;
					inc(colendp, 4);
				end;
				if bitplanemask and (1 shl bitplane) <> 0 then break;
				inc(bitplane);
				inc(writep, metadata.origSizeYP); // not needed?
			end;
			writep := @tempimage.bitmap[x];
			result := FALSE;
		end;

	begin
		w := src.ReadByte * 8;
		h := src.ReadByte;
		x := src.ReadByte * 8;
		y := src.ReadByte;
		if (w > 640) or (h > 200) or (w = 0) or (h = 0) then
			raise DecompException.Create(strcat('sus image size %x%', [w, h]));
		if (x > 639) or (y > 199) then raise DecompException.Create(strcat('sus image ofs %,%', [x, y]));

		bitplanemask := src.ReadByte;
		i := bitplanemask shr 4;
		if NOT (
			((bitplanemask and $F) in [1..7]) and (i in [0..7])
			)
			then raise DecompException.Create('bad bitplanemask $' + strhex(bitplanemask));
		if i <> 0 then transparentindex := i;

		i := LEtoN(src.ReadWord) + 8;
		if (i < src.fullFileSize shr 1) or (i + 1 >= src.fullFileSize) then
			raise DecompException.Create('bad cmdp $' + strhex(i));
		defaultpat := ExplodeBits(src.ReadByte);
		datap := src.readp;
		src.ofs := i;
		cmdp := src.readp;

		tempimage := mcg_bitmap.Init(w, h, MCG_FORMAT_INDEXED, 4);
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			origOfsXP := x;
			origOfsYP := y;
			origSizeXP := w;
			origSizeYP := h;
			srcFilePath := PathCombine([outdir, basename], 'png');
		end;

		with tempimage do begin
			SetupPalette(tempimage, 8);
			imagebytesize := stride * sizeYP;
			filldword(bitmap[0], imagebytesize shr 2, 0);
			lithistory[0] := 0; // silence compiler
			filldword(lithistory[0], 256, 0);
			src.bitSelect := 1;

			// This decompression implementation expands each 1bpp 8px column into a bitmasked 4bpp 8px column, then
			// directly ORs those into the output buffer at the proper final location. This is simpler and faster than
			// generating all bitplane columns and only merging and rotating them at the end.
			x := 0; bitplane := 0; historyindex := 0;
			_ChangeBitplane;
			writep := @bitmap[0]; // unnecessary!?
			colendp := writep + imagebytesize;
			while (datap <= cmdp) and (src.readp < src.endp) do begin
				if NOT src.ReadBit2 then begin
					// 0: literal xx
					pat1 := ExplodeBits(datap^); inc(datap);
					lithistory[historyindex] := pat1;
					historyindex := byte(historyindex + 1);
					dword(writep^) := dword(writep^) or (pat1 shl bitplane);
					inc(writep, stride);
				end
				else if NOT src.ReadBit2 then begin
					// 10: output yy byte xx times, or copy x bytes from earlier
					reps := datap^; inc(datap);
					if reps >= $F0 then begin
						reps := byte(reps + $F) and $F + 1; // rep count 0 = 16 bytes
						if writep + reps * stride > colendp then raise DecompException.Create('column oob');
						pat1 := datap^; inc(datap); // history read index
						while reps <> 0 do begin
							dword(writep^) := dword(writep^) or (lithistory[pat1] shl bitplane);
							pat1 := (pat1 + 1) and $FF;
							inc(writep, stride);
							dec(reps);
						end;
					end
					else begin
						pat1 := ExplodeBits(datap^) shl bitplane; inc(datap);
						pat2 := pat1;
						_OutputBytes;
					end;
				end
				else if NOT src.ReadBit2 then begin
					// 110: output yy,zz for xx bytes
					reps := datap^; inc(datap);
					pat1 := ExplodeBits(datap^) shl bitplane; inc(datap);
					pat2 := ExplodeBits(datap^) shl bitplane; inc(datap);
					_OutputBytes;
				end
				else if NOT src.ReadBit2 then begin
					// 1110: output single 00 byte (ie. output nothing, buffer already inited to 0)
					lithistory[historyindex] := 0;
					historyindex := byte(historyindex + 1);
					inc(writep, stride);
				end
				else begin
					// 1111: output xx default bytes
					reps := datap^; inc(datap);
					pat1 := defaultpat shl bitplane;
					pat2 := pat1;
					_OutputBytes;
				end;

				if writep >= colendp then begin
					// next column!
					inc(bitplane);
					if _ChangeBitplane then exit;
				end;
			end;
		end;
	end;

var buffy : pointer = NIL;
begin
	result := FALSE;
	if loader.fullFileSize < 40 then raise DecompException.Create('file too tiny');

	try
		_LoadBits(loader);

		if transparentindex = high(transparentindex) then
			transparentindex := SelectTransparentIndex(CN_PRS, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(buffy, imagebytesize);
		SaveFile(metadata.srcFilePath, buffy, imagebytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
	end;
	result := TRUE;
end;

