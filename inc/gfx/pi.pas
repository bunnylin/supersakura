{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

procedure ConvertJastAnimData(animp : pointer; metadata : TGraphicFile; game : gid);
// Takes a pointer to a single unit of JAST/Tiare animation data, translates it into metadata.
// Does not release animp. This can be found in GRA file headers and embedded in game executables.
var i, j, framewidth : dword;
begin
	with metadata do begin
		// Get the sequence length.
		seqLen := LEtoN(word(animp^)); inc(animp, 2);
		if seqLen > 32 then raise DecompException.Create('seqlen too big: ' + strdec(seqLen));
		inc(animp, 36); // skip the name...
		origSizeXP := LEtoN(word(animp^)); inc(animp, 2);
		origSizeYP := LEtoN(word(animp^)); inc(animp, 2);

		// Sort the other data into metadata.
		origOfsXP := LEtoN(word(animp^)) * 8; inc(animp, 2);
		origOfsYP := LEtoN(word(animp^)); inc(animp, 2);
		if (origOfsXP > 640) or (origOfsYP > 400) then
			raise DecompException.Create(strcat('sus anim ofs: %,%', [origOfsXP, origOfsYP]));
		framewidth := LEtoN(word(animp^)) * 8; inc(animp, 2);
		origFrameHeightP := LEtoN(word(animp^)); inc(animp, 2);
		if (framewidth > 640) or (origFrameHeightP > 400) then
			raise DecompException.Create(strcat('sus framesize: %x%', [framewidth, origFrameHeightP]));
		frameCount := 0; // autocount in post
		setlength(sequence, 32);

		{$ifdef enable_decomp_hacks}
		// This mostly subtracts viewframes from animation coords. Has to be done as close as possible to data source.
		case game of
			gid.AngelsC1:
			if (graphicName <> 'T1_00E0') and (graphicName <> 'T3_00E0') then begin
				// Hack: Discount viewframe's offset for animations.
				dec(origOfsXP, 24);
				dec(origOfsYP, 23);
			end;

			gid.AngelsC2:
			begin
				// Hack: Discount viewframe's offset for animations.
				dec(origOfsXP, 80);
				dec(origOfsYP, 20);
			end;

			gid.Runaway, gid.Runaway98:
			if graphicName <> 'OP_013A0' then begin
				// Hack: Discount viewframe's offset for animations.
				dec(origOfsXP, 80);
				dec(origOfsYP, 15);
			end;

			gid.Sakura, gid.Sakura98:
			begin
				// Hack: add a missing sequence length
				if graphicName = 'CT14IA1' then seqLen := 7;
				// Hack: Make Seia blink a tiny bit more slowly (adds extra delay to between sequence[0] and [1])
				if copy(graphicName, 1, 4) = 'CT07' then inc(word((animp + 116)^), 32);
			end;

			gid.SanShimai, gid.SanShimai98, gid.Transfer98:
			begin
				// Hack: Discount viewframe's offset for animations.
				dec(origOfsXP, 80);
				dec(origOfsYP, 15);
			end;

			gid.Setsujuu:
			begin
				// Hack: Discount viewframe's offset for animations.
				dec(origOfsXP, 24);
				dec(origOfsYP, 28);
			end;

			gid.Vanish:
			begin
				// Hack: Discount viewframe's offset for animations.
				dec(origOfsXP, 16);
				dec(origOfsYP, 8);
			end;
		end;
		{$endif enable_decomp_hacks}

		// Sequence storage format:
		// [n] [xx][frame number 13 bits] [xx][delay 14 bits]
		for i := 0 to 31 do begin
			sequence[i] := byte(animp^) shl 16;
			inc(animp, 2);
		end;
		// Convert to ~millisecs, shift delays to intuitively correct frames.
		for i := 1 to 32 do begin
			j := LEtoN(word((animp + i * 2)^)) * 11;
			if j = 0 then j := 16;
			sequence[i - 1] := sequence[i - 1] + j;
		end;
		i := seqLen - 1;
		j := dword(LEtoN(word(animp^)) * 11);
		sequence[i] := (sequence[i] and $FFFF0000) + j;
		// If sequence length = 1, enforce a stopped animation.
		if seqLen = 1 then sequence[0] := sequence[0] or $FFFF;
	end;
end;

function Decomp_Pi(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as a Pi graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;

	procedure _ReadHeader;
	var i, j, l : dword;
		anim : boolean;
	begin
		// The header starts with a comment block that terminates with $1A.
		repeat
			if (loader.readp + 16 >= loader.endp) or (loader.ofs > $170) then begin
				loader.ofs := 0;
				raise DecompException.Create('Comment block $1A not found');
			end;
		until loader.ReadByte = $1A;

		// The header starts after the first 0 after the 1A.
		repeat
			if (loader.readp + 16 >= loader.endp) or (loader.ofs > $170) then begin
				loader.ofs := 0;
				raise DecompException.Create('Header start 0 not found');
			end;
		until loader.ReadByte = 0;

		// If the comment block went up to offset $168, it probably contains company-specific animation data. This
		// is saved using only the top nibbles of each byte. We'll process it after validating the rest of the header.
		anim := loader.ofs = $168;

		// Mode byte: should be 00, could be FF.
		i := loader.ReadByte;
		if NOT (i in [0, $FF]) then raise DecompException.Create('Unknown mode ' + strdec(i));

		// Screen ratio. Is this ever not 0 or 1:1?
		i := loader.ReadByte;
		j := loader.ReadByte;
		if (i or j) > 1 then raise DecompException.Create(strcat('Non-zero ratio %:%', [i, j]));

		// Bitdepth: 4 or FF for 16 colors, 8 for 256 colors.
		j := loader.ReadByte;
		if NOT (j in [4, 8, $FF]) then
			raise DecompException.Create('bad bitdepth ' + strdec(j));

		// Compressor model string. Usually only ascii chars.
		i := LEtoN(loader.ReadDword);
		// "Ese" in hiragana Shift-JIS is possible...
		if (i <> $B982A682) then
			// Any char >= $80 can't be ascii...
			if (i and $80808080 <> 0)
			// Any char < $20 can't be ascii...
			or (i and $60000000 = 0)
			or (i and $00600000 = 0)
			or (i and $00006000 = 0)
			or (i and $00000060 = 0)
			then raise DecompException.Create('bad compressor model $' + strhex(i));

		// Compressor-specific data, prefixed by length word, MSB first. Ignore.
		i := BEtoN(loader.ReadWord);
		if i > 5 then raise DecompException.Create('sus compressor-specific data length ' + strdec(i));
		inc(loader.readp, i);

		with metadata do begin
			// Next two words are image width and height, MSB first.
			origSizeXP := BEtoN(loader.ReadWord);
			origSizeYP := BEtoN(loader.ReadWord);
			if (origSizeXP > 640) or (origSizeYP > 800) or (origSizeXP < 2) or (origSizeYP < 2) then
				raise DecompException.Create(strcat('sus image size %x%', [origSizeXP, origSizeYP]));

			// The actual image data is automatically unpacked at 8bpp.
			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_INDEXED, 8);
			with tempimage do begin
				// Read the palette. Since PC98 systems of this era only used 4 bits per channel, each palette byte
				// must copy its top nibble to its bottom nibble.
				if j = 8 then
					setlength(palette, 256)
				else
					setlength(palette, 16);

				for i := 0 to high(palette) do with palette[i] do begin
					r := loader.ReadByte;
					g := loader.ReadByte;
					b := loader.ReadByte;
					dword(palette[i]) := dword(palette[i]) and $F0F0F0F0;
					dword(palette[i]) := dword(palette[i]) or (dword(palette[i]) shr 4);
					a := $FF;
				end;
			end;
		end;

		if anim then begin
			l := loader.ofs;
			// Repack $164 nibbles into 178 bytes.
			loader.ofs := 2;
			for i := 1 to 178 do begin
				j := LEtoN(loader.ReadWord);
				byte(loader.PtrAt(i)^) := (j and $F0) or (j shr 12);
			end;
			// Process it.
			ConvertJastAnimData(loader.PtrAt(1), metadata, game);
			loader.ofs := l;
		end;
	end;

	procedure _MiniHeader;
	// If the full header couldn't be validated, maybe it's a Pi file with a reduced header variant.
	var i, j : dword;
		pal : TSRGBPalette = NIL;
		valid : boolean = TRUE;
		alpha : boolean = FALSE;
	begin
		loader.ofs := 0;
		// Palette first? DPC or P image. Check if 3 specific bits always 0 in the first 16 words.
		j := 0;
		setlength(pal, 16);
		with metadata do begin
			for i := 0 to 15 do with pal[i] do begin
				j := LEtoN(loader.ReadWord);
				if j and $842 <> 0 then valid := FALSE;
				g := j shr 8;
				r := byte(j shr 3);
				b := byte(j shl 2);
				a := $FF;
				if j and 1 <> 0 then begin a := 0; alpha := TRUE; end;
			end;

			if valid then begin
				origOfsXP := LEtoN(loader.ReadWord);
				origOfsYP := LEtoN(loader.ReadWord);
				origSizeXP := LEtoN(loader.ReadWord);
				origSizeYP := LEtoN(loader.ReadWord);
				// P images use 8x width/ofsx.
				if (loader.fileName.Length > 2)
				and (byte(loader.fileName[loader.fileName.Length]) or $20 = ord('p'))
				and (loader.fileName[loader.fileName.Length - 1] = '.') then begin
					origOfsXP := origOfsXP shl 3;
					origSizeXP := origSizeXP shl 3;
				end;

				if (origOfsXP + origSizeXP > 640) or (origOfsYP + origSizeYP > 1200)
				or (origSizeXP < 2) or (origSizeYP < 2) then
					raise DecompException.Create(strcat('sus ofs %,% size %x%', [origOfsXP, origOfsYP, origSizeXP, origSizeYP]));
			end

			else begin
				// Size first, palette second? G image or LSP in Wakuwaku2.
				loader.ofs := 0;
				origSizeXP := LEtoN(loader.ReadWord);
				origSizeYP := LEtoN(loader.ReadWord);
				if (origSizeXP > 640) or (origSizeYP > 800) then begin
					origSizeXP := SwapEndian(origSizeXP);
					origSizeYP := SwapEndian(origSizeYP);
				end;
				if (origSizeXP > 640) or (origSizeYP > 800) or (origSizeXP < 2) or (origSizeYP < 2) then
					raise DecompException.Create(strcat('sus image size %x%', [origSizeXP, origSizeYP]));
				for i := 0 to high(pal) do with pal[i] do begin
					r := loader.ReadByte;
					g := loader.ReadByte;
					b := loader.ReadByte;
					a := $FF;
				end;
			end;

			tempimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_INDEXED, 8);
			if alpha then tempimage.bitmapFormat := MCG_FORMAT_INDEXEDALPHA;

			for i := high(pal) downto 0 do begin
				dword(pal[i]) := dword(pal[i]) and $F0F0F0F0;
				dword(pal[i]) := dword(pal[i]) or (dword(pal[i]) shr 4);
			end;
			tempimage.palette := pal;
		end;
	end;

	procedure _UnpackPi(const _src : TFileLoader; const _dest : mcg_bitmap);
	var destp, endp, startp : pointer;
		dtable : array of array of byte;
		i, j : dword;
		lastbyteout, lastreptype : byte;
		doingrepetition : boolean;

		function _TranslateDeltaCode : byte; inline;
		// Translates a variable-bit-length delta code into a normal number.
		begin
			result := 0;
			// safety
			if _src.readp + 2 >= _src.endp then begin
				_src.readp := _src.endp;
				exit;
			end;

			if _src.ReadBit then begin // 1x
				if _src.ReadBit then result := 1;
			end
			else begin // 0...
				if _src.ReadBit then begin // 01...
					if _src.ReadBit then begin // 011...

						if length(dtable) = 256 then begin
							if _src.ReadBit then begin // 0111...
								if _src.ReadBit then begin // 01111...
									if _src.ReadBit then begin // 011111...
										if _src.ReadBit then begin
											result := 128 + _src.ReadBits(7); // 0111111xxxxxxx
										end
										else result := 64 + _src.ReadBits(6); // 0111110xxxxxx
									end
									else result := 32 + _src.ReadBits(5); // 011110xxxxx
								end
								else result := 16 + _src.ReadBits(4); // 01110xxxx
							end
							else result := 8 + _src.ReadBits(3); // 0110xxx
						end
						else result := 8 + _src.ReadBits(3); // 011xxx
					end
					else result := 4 + _src.ReadBits(2); // 010xx
				end
				else begin // 00x
					if _src.ReadBit then
						result := 3
					else
						result := 2;
				end;
			end;
		end;

		function _GetLengthCode : dword; inline;
		// Returns a variable-bit-length number used for repetition lengths.
		var bitprefixcount : word;
		begin
			bitprefixcount := 0;
			while (_src.readp < _src.endp) and (_src.ReadBit) do inc(bitprefixcount);
			result := 1 shl bitprefixcount;
			while bitprefixcount <> 0 do begin
				dec(bitprefixcount);
				if (_src.readp < _src.endp) and (_src.ReadBit) then
					result := dword(result + dword(1 shl bitprefixcount));
			end;
			// Safety, for unreasonably long repeats.
			if result >= $10000000 then result := $FFFFFFF;
		end;

		function _GetRepetitionType : byte; inline;
		// Returns one of five bitpacked repetition type codes.
		begin
			result := 0;
			// safety
			if _src.readp + 2 >= _src.endp then begin
				_src.readp := _src.endp;
				exit;
			end;

			if _src.ReadBit then begin
				if _src.ReadBit then begin
					if _src.ReadBit then
						result := 111
					else
						result := 110;
				end
				else result := 10;
			end
			else begin
				if _src.ReadBit then result := 1;
			end;
		end;

		procedure _CopyBytes(replen, repofs : dword; oobfiller : word);
		// Copies replen bytes from (destp - repofs)^ to destp^. Where the source offset is before the start of the
		// image, fill with oobfiller instead.
		var oobcount : longint;
		begin
			oobcount := repofs - (destp - startp);
			if oobcount > 0 then begin
				if replen < dword(oobcount) then oobcount := replen;
				fillword(destp^, oobcount shr 1, oobfiller);
				inc(destp, oobcount);
				dec(replen, oobcount);
				// Handle odd fill lengths.
				if oobcount and 1 <> 0 then byte((destp - 1)^) := byte(oobfiller);
			end;

			MemCopy(destp - repofs, destp, replen);
			inc(destp, replen);
		end;

		procedure _ProcessDeltaCode;
		// Reads a delta code from the input bit stream, adjusts the delta table, and outputs a single byte.
		var deltaindex, colorbyte : byte;
		begin
			if destp >= endp then exit;

			deltaindex := _TranslateDeltaCode;
			// Move the new color byte to the front of its table row.
			colorbyte := dtable[lastbyteout][deltaindex];
			while deltaindex <> 0 do begin
				dtable[lastbyteout][deltaindex] := dtable[lastbyteout][deltaindex - 1];
				dec(deltaindex);
			end;
			dtable[lastbyteout][0] := colorbyte;
			// Output the color byte, and remember it for the next delta code.
			byte(destp^) := colorbyte;
			inc(destp);
			lastbyteout := colorbyte;
		end;

		procedure _ProcessRepetitionCode(minusreps : byte);
		var replength : dword;
			bytequad : dword;
			bytepair : word;
			reptype : byte;
		begin
			// If the new repetition type is the same as the last one, stop doing repeats and expect delta codes again.
			reptype := _GetRepetitionType;
			if lastreptype = reptype then begin
				doingrepetition := FALSE;
				lastbyteout := byte((destp - 1)^);
				exit;
			end;
			lastreptype := reptype;

			// Read the number of repetitions required. Subtract minusreps, which is always 0 except on the first
			// forced repeat where it is 1. The repetitions are specified as byte pairs in the stream, so multiply by
			// two to get the byte length.
			replength := dword(_GetLengthCode - minusreps) * 2;

			// Safety.
			if destp + replength >= endp then replength := endp - destp;

			if replength <> 0 then
				case reptype of
					// Special repeat, "Location 0" in the spec. Normally repeats the last 4 bytes, unless the last two
					// bytes are equal or we're only two bytes into the image, in which case repeat the last 2 bytes.
					00:
					begin
						bytepair := word((destp - 2)^);
						if (destp < startp + 4) or (bytepair and $FF = bytepair shr 8) then begin
							// Repeat last two bytes.
							fillword(destp^, replength shr 1, bytepair);
							inc(destp, replength);
						end
						else begin
							// Repeat last four bytes.
							bytequad := dword((destp - 4)^);
							filldword(destp^, replength shr 2, bytequad);
							inc(destp, replength);
							// Handle odd number of repeats.
							if replength and 2 <> 0 then word((destp - 2)^) := word(bytequad);
						end;
					end;

					// Repeat type "Location 1" in the spec. Copy bytes from exactly one row above. While out of
					// bounds, copy only the top left word.
					01: _CopyBytes(replength, _dest.sizeXP, word(startp^));

					// Repeat type "Location 2" in the spec. Copy bytes from exactly two rows above. While out of
					// bounds, copy only the top left word.
					10: _CopyBytes(replength, _dest.sizeXP * 2, word(startp^));

					// Repeat type "Location 3" in the spec. Copy bytes from one row above, 1 byte ahead. While out of
					// bounds, copy the top left word, reversed.
					110: _CopyBytes(replength, _dest.sizeXP - 1, swap(word(startp^)));

					// Repeat type "Location 4" in the spec. Copy bytes from one row above, 1 byte back. While out of
					// bounds, copy the top left word, reversed.
					111: _CopyBytes(replength, _dest.sizeXP + 1, swap(word(startp^)));
				end;
		end;

	begin
		// The image will be directly decompressed into an 8bpp buffer, even if the palette is only 16 colors. The
		// algorithm doesn't care about the actual palette values or presence/absence of alpha; we're only dealing with
		// an input bit stream and an output byte stream.
		i := _dest.sizeXP * _dest.sizeYP;
		setlength(_dest.bitmap, i);

		// Set up an access pointer and some important position markers.
		startp := @_dest.bitmap[0];
		destp := startp;
		endp := startp + i;

		_src.bitSelect := 0;
		lastbyteout := 0;
		lastreptype := 255;
		doingrepetition := TRUE;

		// Set up a color delta table.
		dtable := NIL;
		setlength(dtable, length(_dest.palette));
		for i := 0 to high(dtable) do begin
			setlength(dtable[i], length(dtable));
			for j := 0 to high(dtable[i]) do
				dtable[i][j] := (dword(length(dtable)) + i - j) and byte(high(dtable));
		end;

		// Start with two delta codes.
		_ProcessDeltaCode;
		_ProcessDeltaCode;
		// Forced repetition, with length reduced by one.
		_ProcessRepetitionCode(1);

		while destp < endp do begin
			if doingrepetition then
				_ProcessRepetitionCode(0)
			else begin
				_ProcessDeltaCode;
				_ProcessDeltaCode;
				// If the next bit is not set, we'll do a repetition; else two more delta codes will follow.
				if _src.ReadBit = FALSE then begin
					doingrepetition := TRUE;
					lastreptype := 255;
				end;
			end;

			if _src.readp + 2 > _src.endp then raise DecompException.Create(
				'image incomplete, input ran out, output still needs ' + strdec(endp - destp) + ' bytes');
		end;
		if (_src.RemainingBytes > 8) and (metadata.graphicName <> 'GAOWIN') then raise DecompException.Create(
			'image complete, but input still has ' + strdec(_src.RemainingBytes) + ' bytes');
	end;

	{$ifdef enable_decomp_hacks}
	procedure _Hack;
	var sideloader : TFileLoader = NIL;
		img2 : mcg_bitmap = NIL;
		n : UTF8string;
		i : dword;
	begin
		try
			n := loader.fileName;
			n[n.Length - 4] := 'm';
			sideloader := TFileLoader.Open(n);
			if sideloader.fullFileSize < $8000 then exit;
			if decomp_param.verbose then decomp_logger('appending ' + n);
			img2 := mcg_bitmap.Create;
			with img2 do begin
				sizeXP := $280;
				stride := sizeXP;
				sizeYP := $C8;
				bitmapFormat := MCG_FORMAT_INDEXED;
				palette := tempimage.palette;
			end;
			sideloader.ofs := $34; // skip header
			_UnpackPi(sideloader, img2);
			i := length(tempimage.bitmap);
			setlength(tempimage.bitmap, i + length(img2.bitmap));
			move(img2.bitmap[0], tempimage.bitmap[i], length(img2.bitmap));
		finally
			if sideloader <> NIL then sideloader.Destroy;
			if img2 <> NIL then img2.Destroy;
		end;
	end;
	{$endif}

var pngbuffy : pointer = NIL;
	bytesize, transparentindex : dword;
begin
	result := FALSE;
	// Nocturne has some palette-only overlays, 59 or 61 bytes. Yuugiri can go as low as 48.
	if loader.fullFileSize < 48 then raise DecompException.Create('file too tiny');

	// Specification-compliant Pi files should always end the encoded image stream with "32 bits of 0", ie. one dword
	// of 0. Not all files respect this: some Deep images end with only 3 zero-bytes, some Tasogare images end with
	// only 2. "Sansi" in 3sis ends with 0000 001A, so accept the eof mark.
	if (word((loader.endp - 2)^) <> 0) and (LEtoN(dword((loader.endp - 4)^)) <> $1A000000) then
		raise DecompException.Create('expected 00 00 at file end');

	try
		metadata := CopyOrCreateGraphicFile(upcase(basename));
		metadata.srcFilePath := PathCombine([outdir, basename], 'png');
		// Because data.inf may have had bogus metadata, scrap it all before loading this new file.
		// The only time metadata should not be wiped for the image being loaded is if valid metadata was loaded for
		// this file from the game executable, which is only the case for animated images from Jast v2 games.
		with metadata do case game of
			gid.AngelsC1, gid.AngelsC2, gid.Runaway, gid.Runaway98, gid.SanShimai, gid.SanShimai98, gid.Setsujuu,
			gid.Transfer98, gid.Vanish:
			if length(sequence) = 0 then ResetMeta;
			else ResetMeta;
		end;

		try
			_ReadHeader;
		except on e : DecompException do begin
			if decomp_param.verbose then decomp_logger(e.Message);
			metadata.ResetMeta;
			_MiniHeader;
		end; end;
		_UnpackPi(loader, tempimage);
		{$ifdef enable_decomp_hacks}
		if decomp_param.doHacks then case game of
			// Hack: These two images need corresponding m files appended to them, and the buffers are irregular...
			gid.GaoGao2: if (basename = 'pw042h') or (basename = 'pw045h') then _Hack;
		end;
		{$endif}

		transparentindex := SelectTransparentIndex(CN_PI, metadata, tempimage, game);
		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

