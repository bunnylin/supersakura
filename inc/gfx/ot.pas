{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_OT(const loader : TFileLoader; const outdir, basename : UTF8string; game : gid) : boolean;
// Tries to read the loader buffer as an OT graphic. Adds its metadata to graphicFiles[] and saves the bitmap in
// outdir/basename as a normal PNG.
// Throws DecompException for bad failures, returns TRUE if successful.
var tempimage : mcg_bitmap = NIL;
	metadata : TGraphicFile = NIL;
	pngbuffy : pointer = NIL;
	bytesize, transparentindex : dword;
	dtable : array of array of byte;

const copyfromtree : TBinTree = (
4, 144,
4, 80,	// 1:	"0"
4, 40,	// 2:	"00"
4, 32,	// 3:	"000"
4, 8,	// 4:	"0000"
0,$102,	// 5:	"00000" = code 4 = 2,-1
4, 16,	// 6:	"00001"
4, 8,	// 7:	"000010"
0,$2F8,	// 8:	"0000100" = code $17 = -8,-2
0,$110,	// 9:	"0000101" = code $16 = 16,-1
0,$202,	// 10:	"000011" = code $E = 2,-2
0,$1FF,	// 11:	"0001" = code 3 = -1,-1
4, 24,	// 12:	"001"
4, 16,	// 13:	"0010"
4, 8,	// 14:	"00100"
0,$0F8,	// 15:	"001000" = code $D = -8,0
0,$1FC,	// 16:	"001001" = code $C = -4,-1
0,$1FE,	// 17:	"00101" = code 6 = -2,-1
4, 8,	// 18:	"0011"
0,$200,	// 19:	"00110" = code 7 = 0,-2
0,$0FE,	// 20:	"00111" = code 5 = -2,0
4, 56,	// 21:	"01"
4, 32,	// 22:	"010"
4, 8,	// 23:	"0100"
0,$201,	// 24:	"01000" = code 8 = 1,-2
4, 16,	// 25:	"01001"
4, 8,	// 26:	"010010"
0,$1F8,	// 27:	"0100100" = code 0x14 = -8, 1
0,$210,	// 28:	"0100101" = code 0x18 = 16,-2
0,$2FE,	// 29:	"010011" = code 0x10 = -2,-2
4, 16,	// 30:	"0101"
4, 8,	// 31:	"01010"
0,$204,	// 32:	"010100" = code 0x11 = 4,-2
0,$108,	// 33:	"010101" = code 0xF = 8,-1
0,$2FF,	// 34:	"01011" = code 0xA = -1,-2
0,$0FF,	// 35:	"011" = code 1 = -1,0
4, 64,	// 36:	"1"
4, 56,	// 37:	"10"
4, 24,	// 38:	"100"
4, 16,	// 39:	"1000"
4, 8,	// 40:	"10000"
0,$2FC,	// 41:	"100000" = code 0x13 = -4,-2
0,$0F0,	// 42:	"100001" = code 0x12 = -16,0
0,$104,	// 43:	"10001" = code 0xB = 4,-1
4, 24,	// 44:	"1001"
4, 16,	// 45:	"10010"
4, 8,	// 46:	"100100"
0,$1F0,	// 47:	"1001000" = code 0x19 = -16,-1
0,$2F0,	// 48:	"1001001" = code 0x1A = -16,-2
0,$208,	// 49:	"100101" = code 0x15 = 8,-2
0,$0FC,	// 50:	"10011" = code 9 = -4,0
0,$101,	// 51:	"101" = code 2 = 1,-1
0,$100	// 52:	"11" = code 0 = 0,-1
);

const copylentree : TBinTree = (
4, 424,
4, 8,	// 1:	"0"
0, $2,	// 2:	"00" = 2
4, 128,	// 3:	"01"
4, 120,	// 4:	"010"
4, 112,	// 5:	"0100"
4, 64,	// 6:	"01000"
4, 8,	// 7:	"010000"
0, $D,	// 8:	"0100000" = $D
4, 8,	// 9:	"0100001"
0, $12,	// 10:	"01000010" = $12
4, 24,	// 11:	"01000011"
4, 16,	// 12:	"010000110"
4, 8,	// 13:	"0100001100"
0, $35,	// 14:	"01000011000" = $35
0, $30,	// 15:	"01000011001" = $30
0, $24,	// 16:	"0100001101" = $24
4, 8,	// 17:	"010000111"
0, $25,	// 18:	"0100001110" = $25
4, 8,	// 19:	"0100001111"
0, $2F,	// 20:	"01000011110" = $2F
0, $39,	// 21:	"01000011111" = $39
4, 40,	// 22:	"010001"
4, 8,	// 23:	"0100010"
0, $13,	// 24:	"01000100" = $13
4, 8,	// 25:	"01000101"
0, $1A,	// 26:	"010001010" = $1A
4, 8,	// 27:	"010001011"
0, $26,	// 28:	"0100010110" = $26
4, 8,	// 29:	"0100010111"
0, $31,	// 30:	"01000101110" = $31
0, $33,	// 31:	"01000101111" = $33
0, $E,	// 32:	"0100011" = $E
0, $7,	// 33:	"01001" = 7
0, $5,	// 34:	"0101" = 5
4, 208,	// 35:	"011"
4, 160,	// 36:	"0110"
4, 8,	// 37:	"01100"
0, $A,	// 38:	"011000" = $A
4, 24,	// 39:	"011001"
4, 8,	// 40:	"0110010"
0, $14,	// 41:	"01100100" = $14
4, 8,	// 42:	"01100101"
0, $1D,	// 43:	"011001010" = $1D
0, $1B,	// 44:	"011001011" = $1B
4, 32,	// 45:	"0110011"
4, 24,	// 46:	"01100110"
4, 16,	// 47:	"011001100"
4, 8,	// 48:	"0110011000"
0, $34,	// 49:	"01100110000" = $34
0, $36,	// 50:	"01100110001" = $36
0, $27,	// 51:	"0110011001" = $27
0, $1C,	// 52:	"011001101" = $1C
4, 64,	// 53:	"01100111"
4, 56,	// 54:	"011001110"
4, 8,	// 55:	"0110011100"
0, $3C,	// 56:	"01100111000" = $3C
4, 16,	// 57:	"01100111001"
4, 8,	// 58:	"011001110010"
0, $40,	// 59:	"0110011100100" = $40
0, $0,	// 60:	"0110011100101" = 0
4, 24,	// 61:	"011001110011"
4, 16,	// 62:	"0110011100110"
4, 8,	// 63:	"01100111001100"
0, $100,	// 64:	"011001110011000" = $100
0, $140,	// 65:	"011001110011001" = $140
0, $C0,	// 66:	"01100111001101" = $C0
0, $80,	// 67:	"0110011100111" = $80
0, $28,	// 68:	"0110011101" = $28
4, 16,	// 69:	"011001111"
4, 8,	// 70:	"0110011110"
0, $3A,	// 71:	"01100111100" = $3A
0, $32,	// 72:	"01100111101" = $32
4, 8,	// 73:	"0110011111"
0, $37,	// 74:	"01100111110" = $37
0, $3B,	// 75:	"01100111111" = $3B
4, 8,	// 76:	"01101"
0, $B,	// 77:	"011010" = $B
4, 8,	// 78:	"011011"
0, $F,	// 79:	"0110110" = $F
4, 8,	// 80:	"0110111"
0, $15,	// 81:	"01101110" = $15
4, 16,	// 82:	"01101111"
4, 8,	// 83:	"011011110"
0, $2B,	// 84:	"0110111100" = $2B
0, $2C,	// 85:	"0110111101" = $2C
0, $1E,	// 86:	"011011111" = $1E
4, 8,	// 87:	"0111"
0, $8,	// 88:	"01110" = 8
4, 48,	// 89:	"01111"
4, 8,	// 90:	"011110"
0, $10,	// 91:	"0111100" = $10
4, 8,	// 92:	"0111101"
0, $16,	// 93:	"01111010" = $16
4, 24,	// 94:	"01111011"
4, 8,	// 95:	"011110110"
0, $29,	// 96:	"0111101100" = $29
4, 8,	// 97:	"0111101101"
0, $3D,	// 98:	"01111011010" = $3D
0, $38,	// 99:	"01111011011" = $38
0, $1F,	// 100:	"011110111" = $1F
4, 16,	// 101:	"011111"
4, 8,	// 102:	"0111110"
0, $17,	// 103:	"01111100" = $17
0, $18,	// 104:	"01111101" = $18
0, $11,	// 105:	"0111111" = $11
4, 112,	// 106:	"1"
4, 8,	// 107:	"10"
0, $4,	// 108:	"100" = 4
4, 8,	// 109:	"101"
0, $6,	// 110:	"1010" = 6
4, 88,	// 111:	"1011"
4, 8,	// 112:	"10110"
0, $C,	// 113:	"101100" = $C
4, 40,	// 114:	"101101"
4, 16,	// 115:	"1011010"
4, 8,	// 116:	"10110100"
0, $20,	// 117:	"101101000" = $20
0, $22,	// 118:	"101101001" = $22
4, 16,	// 119:	"10110101"
4, 8,	// 120:	"101101010"
0, $2A,	// 121:	"1011010100" = $2A
0, $2D,	// 122:	"1011010101" = $2D
0, $21,	// 123:	"101101011" = $21
4, 32,	// 124:	"1011011"
4, 8,	// 125:	"10110110"
0, $23,	// 126:	"101101100" = $23
4, 16,	// 127:	"101101101"
4, 8,	// 128:	"1011011010"
0, $3F,	// 129:	"10110110100" = $3F
0, $3E,	// 130:	"10110110101" = $3E
0, $2E,	// 131:	"1011011011" = $2E
0, $19,	// 132:	"10110111" = $19
0, $9,	// 133:	"10111" = 9
0, $3	// 134:	"11" = 3
);

	procedure _InitDelta;
	// The delta table must be reset before the main decompression, and before the first anim frame decompression.
	// Subsequent animation frames carry the state.
	var i, j : byte;
	begin
		for i := 0 to high(dtable) do begin
			setlength(dtable[i], length(dtable));
			for j := 0 to high(dtable[i]) do
				dtable[i][j] := (dword(length(dtable)) + j + i + 1) and byte(high(dtable));
		end;
	end;

	procedure _Unpack(const src : TFileLoader; const img : mcg_bitmap);
	var writep, endp, startp, rowendp : pointer;
		notfirstrow : boolean = FALSE;

		function _ReadLitCode : byte;
		begin
			if NOT src.ReadBit then exit($F); // 0
			if src.ReadBit then exit(0); // 11
			if src.ReadBit then exit(1); // 101

			// 100
			if src.ReadBit then begin // 100-1
				if src.ReadBit then begin // 100-11
					if src.ReadBit then begin // 100-111
						if src.ReadBit then begin // 100-1111
							if src.ReadBit then begin // 100-11111
								if src.ReadBit then exit($C); // 100-111111
								if src.ReadBit then exit($E); // 100-1111101
								exit($D); // 100-1111100
							end else exit(9); // 100-11110
						end else exit(7); // 100-1110
					end else exit(5); // 100-110
				end else exit(3); // 100-10
			end;

			// 100-0
			if src.ReadBit then begin // 100-01
				if src.ReadBit then begin // 100-011
					if src.ReadBit then begin // 100-0111
						if src.ReadBit then begin // 100-01111
							if src.ReadBit then exit($B); // 100-011111
							exit($A); // 100-011110
						end else exit(8); // 100-01110
					end else exit(6); // 100-0110
				end else exit(4); // 100-010
			end else exit(2); // 100-00
		end;

		function _OutLit(up1, up2, upleft1, upleft2, left1, left2, xleft : byte) : byte;
		var drow, code : byte;
		begin
			// Use -2y by default.
			drow := up2;

			// If -1y is not the same as -2y, and...
			if (up1 <> up2)
			// -2y is not the same as -2x -2y, or -2y is not the same as -2x...
			and ((up2 <> upleft2) or (up2 <> left2)) then begin
				// If -2x (or -3x for 2nd/4th px) is the same as -1x, use them. Else use -1x -1y.
				if xleft = left1 then
					drow := left1
				else
					drow := upleft1;
			end;

			code := _ReadLitCode;
			result := dtable[drow][code];
			//decomp_logger(strcat('lit & -> &', [code, result]));
			if code in [0, $F] then exit;
			// Any code 1..E needs to be shifted to index 0 when fetched.
			repeat
				dtable[drow][code] := dtable[drow][code - 1];
				dec(code);
			until code = 0;
			dtable[drow][0] := result;
		end;

		procedure _DoLiteral;
		var up1, up2, up3, up4 : byte;
			left1, left2, left3, left4 : byte;
			upleft1, upleft2, upleft3, upleft4 : byte;
			this1, this2, this3, this4 : byte;
			p : pointer;
			notfirstcol : boolean;
		begin
			// This will output 4 literals as a 2x2 group. Each comes from a delta table reference, but each pixel
			// can use a different table row, and that's determined based on surrounding pixels. So first have to get
			// the surrounding pixels.
			// Up1-4 are the 4 pixels in the above 2x2 group, left1-4 in the left 2x2 group, etc.
			notfirstcol := (rowendp - writep < longint(img.stride));

			if notfirstrow then begin
				p := writep - img.stride * 2;
				up1 := byte(p^);
				up3 := byte((p + img.stride)^);
				inc(p);
				up2 := byte(p^);
				up4 := byte((p + img.stride)^);
			end
			else begin
				up1 := 0; up2 := 0; up3 := 0; up4 := 0;
			end;

			if (notfirstrow) and (notfirstcol) then begin
				p := writep - img.stride * 2 - 2;
				upleft1 := byte(p^);
				upleft3 := byte((p + img.stride)^);
				inc(p);
				upleft2 := byte(p^);
				upleft4 := byte((p + img.stride)^);
			end
			else begin
				upleft1 := 0; upleft2 := 0; upleft3 := 0; upleft4 := 0;
			end;

			if notfirstcol then begin
				p := writep - 1;
				left2 := byte(p^);
				left4 := byte((p + img.stride)^);
				dec(p);
				left1 := byte(p^);
				left3 := byte((p + img.stride)^);
			end
			else begin
				left1 := 0; left2 := 0; left3 := 0; left4 := 0;
			end;

			this1 := _OutLit(up3, up1, upleft4, upleft1, left2, left1, left1);
			this2 := _OutLit(up4, up2, up3, upleft2, this1, left2, left1);
			this3 := _OutLit(this1, up3, left2, upleft3, left4, left3, left3);
			this4 := _OutLit(this2, up4, this1, upleft4, this3, left4, left3);

			// Output the 2x2 pixels.
			byte(writep^) := this1;
			byte((writep + img.stride)^) := this3;
			inc(writep);
			byte(writep^) := this2;
			byte((writep + img.stride)^) := this4;
			inc(writep);
		end;

		procedure _DoRepeat;
		var copydist, copylen : dword;
			xpos, xofs, overflow : longint;
			yofs : byte;
		begin
			copydist := ReadBinaryCode(src, copyfromtree, @src.ReadBit);
			{$push}{$R-}
			yofs := copydist shr 8;
			xofs := shortint(copydist);
			copydist := dword(yofs * img.stride) - xofs;
			{$pop}
			copydist := copydist * 2;
			if src.ReadBit then
				copylen := 1
			else begin
				copylen := ReadBinaryCode(src, copylentree, @src.ReadBit);
				if copylen >= $40 then begin
					if NOT src.ReadBit then
						inc(copylen, ReadBinaryCode(src, copylentree, @src.ReadBit))
					else
						inc(copylen);
				end;
			end;
			//decomp_logger('len ' + strdec(copylen));
			copylen := copylen * 2;
			if writep + copylen > rowendp then begin
				LogError('copy past row end @ $' + strhex(src.ofs));
				copylen := rowendp - writep;
			end;
			//decomp_logger(strcat('copy % words from % words ago, ofs %,%', [copylen shr 1, copydist shr 1, xofs, yofs]));

			if (longint(yofs * img.stride * 2) > writep - startp) then begin
				// Copying from above top edge: black fill.
				FillWord(writep^, copylen shr 1, 0);
				FillWord((writep + img.stride)^, copylen shr 1, 0);
			end
			else begin
				xpos := img.stride - (rowendp - writep);
				// Copying only the previous word: efficient fill.
				if (copydist = 2) and (xpos + xofs >= 0) then begin
					FillWord(writep^, copylen shr 1, word((writep - 2)^));
					FillWord((writep + img.stride)^, copylen shr 1, word((writep + img.stride - 2)^));
				end
				else begin
					xofs := xofs * 2;
					overflow := -xofs - xpos;
					// Copying from before left edge: black fill.
					if overflow > 0 then begin
						if dword(overflow) > copylen then overflow := copylen;
						FillWord(writep^, overflow shr 1, 0);
						FillWord((writep + img.stride)^, overflow shr 1, 0);
						inc(writep, overflow);
						inc(xpos, overflow);
						dec(copylen, overflow);
					end;
					if copylen <> 0 then begin
						overflow := xpos + xofs + longint(copylen - img.stride);
						if overflow > 0 then begin
							if dword(overflow) <= copylen then
								dec(copylen, overflow)
							else begin
								overflow := copylen;
								copylen := 0;
							end;
						end;
						// Normal copy from before.
						if copylen <> 0 then begin
							MemCopy(writep - copydist, writep, copylen);
							MemCopy(writep + img.stride - copydist, writep + img.stride, copylen);
							inc(writep, copylen);
							copylen := 0;
						end;
						// Copying from after right edge: black fill.
						if overflow > 0 then begin
							FillWord(writep^, overflow shr 1, 0);
							FillWord((writep + img.stride)^, overflow shr 1, 0);
							inc(writep, overflow);
						end;
					end;
				end;
			end;

			inc(writep, copylen);
		end;

	begin
		// The image will be directly decompressed into an 8bpp buffer.
		// Set up an access pointer and some important position markers.
		startp := @img.bitmap[0];
		rowendp := startp + img.stride;
		writep := startp;
		endp := startp + length(img.bitmap);

		//decomp_logger('unpack starts at $' + strhex(src.ofs));

		while src.readp <= src.endp do begin
			if src.ReadBit then
				_DoRepeat
			else
				_DoLiteral;
			if writep >= rowendp then begin
				inc(writep, img.stride);
				if writep >= endp then break;
				inc(rowendp, img.stride * 2);
				notfirstrow := TRUE;
			end;
		end;

		if (writep < endp) and (src.readp >= src.endp) then raise DecompException.Create(
			'image incomplete, input ran out, output still needs ' + strdec(endp - writep) + ' bytes');
	end;

	procedure _ReadHeader(const src : TFileLoader);
	var i, j, x, y : dword;
	begin
		if dword(src.readp^) <> BEtoN(dword($4F542120)) then raise DecompException.Create('no ot sig');

		src.ofs := 5;
		x := LEtoN(src.ReadWord);
		y := LEtoN(src.ReadWord);
		i := LEtoN(src.ReadWord);
		j := LEtoN(src.ReadWord);
		if (i > 1080) or (j > 800) or (i < 2) or (j < 2) or ((i or j) and 1 <> 0) then // width and height must be even
			raise DecompException.Create(strcat('sus image size %x%', [i, j]));
		if (x + i > 1080) or (y + j > 800) then
			raise DecompException.Create(strcat('sus image ofs %x%', [x, y]));

		tempimage := mcg_bitmap.Init(i, j, MCG_FORMAT_INDEXED, 8);

		metadata := CopyOrCreateGraphicFile(upcase(basename));
		with metadata do begin
			frameCount := 0; // pass through MarkAnims in post
			srcFilePath := PathCombine([outdir, basename], 'png');
			origSizeXP := i;
			origFrameHeightP := j;
			origSizeYP := j;
			origOfsXP := x;
			origOfsYP := y;
		end;

		i := LEtoN(src.ReadWord); // palette bytes length?
		if i <> $30 then raise DecompException.Create('sus 5th word $' + strhex(i));

		i := LEtoN(src.ReadWord);
		if i > 1 then raise DecompException.Create('sus 6th word $' + strhex(i));
		i := LEtoN(src.ReadWord);
		if i <> 0 then raise DecompException.Create('sus 7th word $' + strhex(i));
		i := LEtoN(src.ReadWord);
		if i <> 0 then raise DecompException.Create('sus 8th word $' + strhex(i));

		with tempimage do begin
			setlength(palette, 16);

			for i := 0 to high(palette) do begin
				// 2nd and 3rd bits are swapped!
				j := (i and 9) or ((i and 2) shl 1) or ((i and 4) shr 1);
				with palette[j] do begin
					g := src.ReadByte;
					r := src.ReadByte;
					b := src.ReadByte;
					a := $FF;
				end;
			end;
		end;

		dtable := NIL;
		setlength(dtable, length(tempimage.palette));
		_InitDelta;
		src.bitSelect := 0;
	end;

	procedure _ReadAni(const src : TFileLoader);
	var objs : array of record
			ameta : TGraphicFile;
			aimage : mcg_bitmap;
		end;
		i, d, seqlen : dword;
		numseq : word;
		b, obj : byte;

		procedure _Embed(frame : byte);
		var srcp, destp : pointer;
			y : dword;
		begin
			with objs[frame] do begin
				srcp := @aimage.bitmap[0];
				destp := @tempimage.bitmap[0] + ameta.origOfsYP * longint(tempimage.stride) + ameta.origOfsXP;
				for y := ameta.origFrameHeightP - 1 downto 0 do begin
					move(srcp^, destp^, aimage.stride);
					inc(srcp, aimage.stride);
					inc(destp, tempimage.stride);
				end;
			end;
		end;

	begin
		inc(src.readp, 4); // skip sig
		i := LEtoN(src.ReadWord);
		if NOT (i in [1..9]) then begin
			LogError('sus ani numobjs ' + strdec(i));
			exit;
		end;
		objs := NIL;
		setlength(objs, i);

		numseq := LEtoN(src.ReadWord);
		if NOT (numseq in [1..9]) then begin
			LogError('sus ani numseq ' + strdec(numseq));
			exit;
		end;

		try
			for i := 0 to high(objs) do with objs[i] do begin
				ameta := CopyOrCreateGraphicFile(metadata.graphicName + 'A' + strdec(i));
				with ameta do begin
					srcFilePath := PathCombine([outdir, basename + 'a' + strdec(i)], 'png');
					if decomp_param.verbose then decomp_logger(graphicName);
					frameCount := LEtoN(src.ReadWord);
					if frameCount > 10 then begin
						LogError('sus ani numframes ' + strdec(frameCount));
						exit;
					end;
					origOfsXP := LEtoN(src.ReadWord);
					origOfsYP := LEtoN(src.ReadWord);
					origSizeXP := LEtoN(src.ReadWord);
					origFrameHeightP := LEtoN(src.ReadWord);
					if (frameCount = 0) or (origSizeXP = 0) or (origFrameHeightP = 0) then begin
						ameta.Destroy; ameta := NIL;
						continue;
					end;
					if (origSizeXP > 640) or (origFrameHeightP > 400) then begin
						LogError(strcat('sus size %x% frame %', [origSizeXP, origFrameHeightP, i]));
						exit;
					end;
					if (origOfsXP + origSizeXP > 640) or (origOfsYP + origSizeYP > 400) then begin
						LogError(strcat('sus ofs %x% frame %', [origOfsXP, origOfsYP, i]));
						origOfsXP := 0; origOfsYP := 0;
					end;
					origSizeYP := origFrameHeightP * frameCount;

					aimage := mcg_bitmap.Init(origSizeXP, origSizeYP, MCG_FORMAT_INDEXED, 8);
					aimage.palette := tempimage.palette;
				end;
			end;

			seqlen := LEtoN(src.ReadWord);
			for i := high(objs) downto 0 do with objs[i] do if ameta <> NIL then begin
				ameta.seqLen := seqlen;
				setlength(ameta.sequence, seqlen);
			end;
			for i := 0 to seqlen - 1 do begin
				d := byte((src.readp + 8)^) * 25; // delay after sequence step
				for b := 3 downto 0 do begin
					obj := src.ReadByte;
					if (obj <> 0) and (obj <= length(objs)) then
						objs[obj - 1].ameta.sequence[i] := byte(src.readp^) shl 16 + d;
					inc(src.readp);
				end;
				inc(src.readp);
			end;
			// Skip additional sequences.
			dec(numseq);
			while numseq <> 0 do begin
				seqlen := LEtoN(src.ReadWord);
				inc(src.readp, seqlen * 9);
				dec(numseq);
			end;

			_InitDelta;
			src.bitSelect := 0;

			for i := 0 to high(objs) do with objs[i] do
				if ameta <> NIL then _Unpack(src, aimage);

			// Embed each object's first frame on the base image, and save the frame graphics.
			for i := high(objs) downto 0 do with objs[i] do if ameta <> NIL then begin
				_Embed(i);
				aimage.PackBitDepth(1);
				aimage.MakePNG(pngbuffy, bytesize);

				SaveFile(ameta.srcFilePath, pngbuffy, bytesize);
				freemem(pngbuffy); pngbuffy := NIL;

				AddOrReplaceGraphicFile(ameta);
				ameta := NIL;
			end;

		finally
			for i := high(objs) downto 0 do with objs[i] do begin
				if ameta <> NIL then ameta.Destroy;
				if aimage <> NIL then aimage.Destroy;
			end;
			if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 720 then raise DecompException.Create('file too tiny');

	try
		_ReadHeader(loader);
		_Unpack(loader, tempimage);

		transparentindex := SelectTransparentIndex(CN_OT, metadata, tempimage, game);

		if loader.bitselect <> 0 then inc(loader.readp); // discard leftover bits in current byte
		if (loader.RemainingBytes > 100) and (dword(loader.readp^) = BEtoN(dword($414E493A))) then // "ANI:"
			_ReadAni(loader);

		PostProcessGraphic(tempimage, metadata, game, transparentindex);

		tempimage.MakePNG(pngbuffy, bytesize);
		SaveFile(metadata.srcFilePath, pngbuffy, bytesize);

		AddOrReplaceGraphicFile(metadata);
		metadata := NIL;

	finally
		if tempimage <> NIL then begin tempimage.Destroy; tempimage := NIL; end;
		if metadata <> NIL then begin metadata.Destroy; metadata := NIL; end;
		if pngbuffy <> NIL then begin freemem(pngbuffy); pngbuffy := NIL; end;
	end;
	result := TRUE;
end;

