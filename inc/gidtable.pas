unit gidtable;
{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

// SuperSakura decompilable game ID list.

{$mode objfpc}
{$codepage UTF8}
{$unitpath ../lib/moonlibs}
{$librarypath ../lib/moonlibs}

interface

uses sysutils, mccommon;

// (The interface function definitions are found close to the end of the file.)

// Every game has a unique shortname (game ID). Enum limitation: names can't begin with a number.
// Where multiple versions of the same game exist, remakes and ports get a unique name, patched/modded versions don't
// as long as the data files are convertable without significant changes in converter code.
// Further metadata for each recognised game is in the table after this, and in each game's newdata.inf file.

{$scopedEnums on}
type gid = (
unknown,

// JAST
AngelsEve1, // Tenshitachi no Gogo
AngelsEve1B, // Tenshitachi no Gogo - Bangaihen
AngelsEve2, // Tenshitachi no Gogo 2 - Minako
AngelsEve2B, // Tenshitachi no Gogo 2 - Bangaihen
AngelsEve3, // Tenshitachi no Gogo 3 - Ribbon
AngelsEve3B, // Tenshitachi no Gogo 3 - Bangaihen
AngelsEve3H, // Tenshitachi no Gogo 3 - Bangaihen Hanseiban
AngelsEve4, // Tenshitachi no Gogo 4 - Yuuko
AngelsEve5, // Tenshitachi no Gogo 5 - Nerawareta Tenshi
AngelsEve6, // Tenshitachi no Gogo 6 - My Fair Teacher
AngelsC1, // Tenshitachi no Gogo Collection 1
AngelsC2, // Tenshitachi no Gogo Collection 2
AngelsoftheNight, // Yoru no Tenshitachi ~Shitetsu Ensen Satsujin Jiken~
AngelsSpecial1, // Gomenne Angel - Yokohama Monogatari (Tenshitachi no Gogo Special 1)
AngelsSpecial2, // Tenshitachi no Gogo Special 2
Erika,
Derringer,
CosmosClub,
Maririn, // Super Urutora Mucchin Puri^2 Cyborg Maririn DX
// (Obviously mangled title, but in line with the silly nature of the game, Maririn sounds funnier than Marilyn.)
Deep,
Setsujuu, // Yuki Neko - Setsujuu - Yuganda Kioku (the game's title is given differently in different places...)
Transfer98, // Tenshitachi no Gogo - Tenkousei
BakkonStreet1, // Totsugeki! Back on the Street
BakkonStreet2, // Totsugeki! Back on the Street 2 - Hunting Roulette
SanShimai, // The Three Sisters' Story
SanShimai98,
Eden, // Eden no Kaori
Hohoemi, // Tenshitachi no Hohoemi, Angelsmile
Hohoemi_w,
FromH, // Shuukan From H
Eroden,
PrettyParfait, // Pom-pom Pretty Parfait Okosamayou ("for children", as in kids' menu)
Transfer_w, // Transfer Student (Win) Monmon Gakuen Tenkousei
JastMemo, // Jast Memorial Collection, split into three games when unpacking?

// Tiare
Vanish, // Vanishing Point - Tenshi no Kieta Machi
Runaway, // Runaway City
Runaway98, // Meisou Toshi
Sakura, // Season of the Sakura
Sakura98, // Sakura no Kisetsu
Majokko, // Majokko Paradise
Majokko_w,
Tasogare, // Tasogare no Kyoukai - Edge of Twilight?
TalesNights, // Tales Nights ~Yumekyou no Miko~ or ~Mukyou no Miko~ (depending on if it's the kun or on reading; probably the latter but I'm not 100% sure)
SexyParfait, // Muchimuchi Sexy Parfait Oneesamayou ("for older sister", as in no longer a kids' menu at all)
Izayoi,
TenshiNingyou,
YouAndI,
JusticeSlave, // Justice Slave ~Hoshi to Nare!! Akusoshiki~

// C's Ware
KinKetsu98, // Kindan no Ketsuzoku
KinKetsu_w,
KinKetsu_e, // Fatal Relations
EtsuGaku98, // Etsuraku no Gakuen
EtsuGaku_w,
EtsuGaku_e, // Love Potion
Desire98, // Desire - Haitoku no Rasen (1994)
Desire_w, // Desire Complete Edition (1998), DVD (1999)
Desire_e, // Himeya's localisation (1999)
Desire_r, // Desire Remaster Version (2017)
Desire_ra, // Desire Remaster A Version (2017)
Xenon98, // Xenon - Mugen no Shitai
Xenon98cd,
Xenon_w,
Amy98, // Amy to Yobanaide
Amy_w,
Amy_e, // Amy's Fantasies
EveBE98, // EVE - Burst Error (1995)
EveBE_w, // Japanese Windows version with new graphics and voice? (1997, DVD 1999)
EveBE_e, // Himeya's localisation (1999)
EveBE_r, // 2003 remake
EveBE_r98, // "pc98ver" bundled with 2003 remake
Gloria98, // Gloria (Kinketsu)
Gloria_w,
Gloria_e,
Rabyni,
Kotobuki,
Kotobuki_e,
MaidStory, // Maid Monogatari
MaidStory_e,
AigaMie, // Ai ga Mie Hajimetara
CDGirls, // CD Girls - Utahime-tachi no Koi Monogatari
DiviDead,
DiviDead_e,
EveLost, // Eve - The Lost One
LuvWave,
ReLeaf,
Chiruhana, // Chiruhana (Kinketsu)
AcidWare,
LoveProducer,
AdamDF, // ADAM - The Double Factor
AdamDF_e, // Himeya localisation (2003)
KazeotoChirin,
Vist,
AngelSnow,
EveZero, // Eve Zero ~Ark of the Matter~
ShoKi, // Sho-Ki
Fugue, // Fugue ~Kimi to Boku no Uta~
ShinoTsuki, // Shinobu - Tsukikage no Meikyuu
GakuenKitan, // Gakuen Ojou-sama Kitan (CD and DVD versions are the same, better quality sound files on DVD)
HeartBlade, // Heart & Blade
Reversible, // Reversible Himitsu Nikki
ParaHarmony, // Parallel Harmony ~Getsuyou no Tobira wa Suiyou no Mukou ni~
EveFatal, // Eve - The Fatal Attraction
Mikan,
KuronoSaidan, // Kuro to Kuro to Kuro no Saidan ~Kodoku~
EveNewGen, // Eve ~New Generation X~

// Himeya Soft
Bacta1,
Bacta2,
Phobos,
DArk, // D'Ark
DArkGaiden,
DBaBos, // D'BaBos - D'Ark Bacta Phobos additional disk
YES, // Youthful Eager Stories
YESHG, // YES Hi-Grade Hyper Graphics
Ash,
Zenith, // Full Animation Adventure Series #1
NanaEi1, // Nana Eiyuu Monogatari
NanaEi_w,
NanaEi2,
DeFaNa, // De.FaNa - Full Animation AVG Series 2

// Forest
MetalLace1, // Ningyou Tsukai
MarginalStories, // Marginal Storys
HopStep,
KuronoKen, // Kuro no Ken
MetalLace2,

// Megatech
CobraMission_e,
MetalLace_e, // Battle of the Robo-Babes
DK3_e, // Knights of Xentar
PowerDolls_e,

// Foster
KokoRaku1, // Koko wa Rakuensou, Paradise Heights
KokoRaku1_w,
KokoRaku1_e,
RinkanGakkou,
RinkanGakkou_w,
HananoKioku1,
HananoKioku1_w,
KokoRaku2,
KokoRaku2_w,
KokoRaku2_e,
KouChou, // Kousoku Choujin
KouChou_w,
TSMako, // Time Stripper Mako-chan
TSMako_w,
TSMako_e,
HananoKioku2,
HananoKioku2_w,
Mukougawa, // Sayonara no Mukougawa
Mukougawa_w,
HananoKioku3,
MaigonoKimochi,
KokoRaku3,
ManianaOnna1,
HananoKioku4,
KokoronoKakera,
Akogare,
ManianaOnna2,
HananoKioku5,
HananoKioku5demo,
CalendarGirl,
HananoKioku123, // Hana no Kioku One Two Three compilation
Kanawanu, // Kanawanu Koi no Monogatari
HananoKioku6,
Shikaeshi,
TanteiShounen, // Tantei Shounen A
HananoKioku7,
HananoKioku456, // Hana no Kioku Yon Go Roku compilation
TuneUp, // developed by Mutsu-gumi
Stage,

// ZyX
Trigger,
Raidy1, // Ikazuchi no Senshi Raidy
Raidy1_w,
Raidy1_e, // Lightning Warrior Raidy, G-Collections localisation (2008)
Nekketsu, // Takamisawa Kyousuke Nekketsu! Kyouiku Kenshuu
RingOut98, // Ring Out! Pro Lesring
RingOut_w,
RingOut_e,
Trigger2,
GakuBakuTen, // Gakuen Bakuretsu Tenkousei!
GakuBakuTen_w,
Raidy2,
Raidy2_w,
Raidy2_e, // G-Collections localisation (2010)
AngelCrisis,
TwilightHotel,
TwilightHotel_w,
TwilightHotel_r, // remake Yonaki no Uta (2000)
NinpouChou, // Youki Toubatsu Ninpou Chou
LetsPirates98, // Let's! Pirates Trouble Vacance
LetsPirates,
CrazyKnuckle1, // Kyouken Densetsu Crazy Knuckle
InnaiKansen,
CrazyKnuckle2,
InnaiKansenNurse, // Innai Kansen ~Mayonaka no Nurse Call~
Sanatorium,
InnaiKansen2,
InnaiKansen2Nurse, // Innai Kansen 2 ~Nariyamanu Nurse Call~
AmeKise, // Ameiro no Kisetsu
HakotenJan,
InnaiKan3, // Innai Kansen Gozen 3 Ji no Shujutsushitsu
HornyBun1, // Ecchi na Bunny-san wa Kirai?
HornyBun1_e, // Do you like Horny Bunnies? G-Collections localisation (2003)
Chain, // Chain Ushinawareta Ashiato
Chain_e, // Chain - The Lost Footprints, G-Collections localisation (2002)
Tsuki,
Tsuki_e, // Tsuki - Possession, G-Collections localisation (2003)
Fortuna,
Shukketsubo, // Shukketsubo ~Senseki no Oetsu~
VRoster, // Virgin Roster, G-Collections localisation (2003)
InnaiKansenPCK, // Innai Kansen Shinya ni Hibiku Pon Chin Kan
HornyBun2,
HornyBun2_e, // G-Collections localisation (2004)
Shikomi,
InnaiKansenOwaUta, // Innai Kansen Owaranu Utage no Nurse Call
Sagara, // Sagara-sanchi no Etsuraku Life
Sagara_e, // The Sagara Family, G-Collections localisation (2005)
Kadowakashi,
Musuemon, // Mirai Daughter Musuemon
BineKyouCherry, // Binetsu Kyoushi Cherry
BineKyouCherry_e, // Amorous Professor Cherry, G-Collections localisation (2008)
Rouraku, // Rouraku ~Ingi ni Ochiru~

// Crowd
Fukushuu98,
Fukushuu,
XChange1,
XChange1_e, // X-Change
Kyoushi, // Kyou * shi ~Nerawareta Seifuku~
Checkin, // Tokimeki Check-in!
Checkin_e,
XChange2,
XChange2_e,
Doushin, // Doushin - Sanshimai no Etude
Doushin_e, // Doushin - Same Heart
FukuRyou, // Fukushuu - Ryoujoku no Yaiba
BraveSoul,
BraveSoul_e,
MissYou,
ChikanJigoku, // Ryoujoku Chikan Jigoku
Obbligato,
Owarinaki, // Owarinaki Maidtachi no Yoru
XChange3,
XChange3_e,
FudolHealth, // How to Fudol ~Health Hen~
XChangeAlt, // Yin-Yang! X Change Alternative
XChangeAlt_e, // Yin-Yang! X Change Alternative
Erodake, // Heisei Nenkin Gekijou: Erodake
Kunoichiban,

// ScooP
SchoolGirls,
SchoolGirls_w,
ImmoralStudy1,
ImmoralStudy1_w,
ImmoralStudy1_e,
ImmoralStudy2,
ImmoralStudy2_w,
ImmoralStudy2_e,
ImmoralStudy3,
ImmoralStudy3_w,
MahouShoujoBko,
MahouShoujoBko_w, // renewal
DeliLunchPack, // Delicious Lunch Pack
DeliLunchPack_w,
DeliLunchPackNeo, // 2002 remake
BountyHunterLudy,
BountyHunterLudy_w,
OjousamaoNerae,
ChaosQueen1, // Chaos Queen Ryouko Story #1
ChaosQueen2,
ChaosQueen3,
Success,
ChaosBaby1, // Chaos Baby Yoshimi ~Takase Haruna Hen~
YougenTennyo,
Yuuwaku1,
ChaosBaby2,
Yuuwaku2,
ChaosQueen4,
TsuBoMi,

// D.O.
Kaerimichi, // Kaerimichi wa Kiken ga Ippai
YoujuuClub1_w,
Exterlien,
HoshinoSuna1, // Hoshi no Suna Monogatari
Branmarker,
YoujuuClub2_w,
DOR1,
DOR2,
HoshinoSuna2,
DOR3,
DORSakigake, // DOR Special Edition Sakigake
CollectorD,
CollectorDB, // Collector D Bangai Hen
YoujuuSenki2048, // Youjuu Senki -A.D. 2048-
YoujuuSenki2048_w,
YoujuuSenki2048_r, // Youjuu Senki -A.D. 2048- ~Shin Setsu Jo Shou~ (2006 remake)
YoujuuSenki2048_e, // Pretty Soldier Wars
DOR93, // DOR Special Edition '93
DORSpecial_w, // DOR Special Edition (Sakigake+93, Win)
ZokuYoujuu, // Zoku Youjuu Senki -Suna no Mokushiroku-
ZokuYoujuu_w,
ZokuYoujuu_r, // Zoku Youjuu Senki -Shinsetsu Suna no Mokushiroku- (2006 remake)
CrystalRinal, // Crystal Rinal - Ouma no Meikyuu -
CrystalRinal_w,
Zatsuon, // Zatsuon Ryouiki
Zatsuon_w,
Zatsuon_r, // (2001 remake)
CybernoidAlpha, // Cybernoid Alpha Premium
TouchMyHeart, // D.O. Kanshuu Premium Box Touch My Heart
HoshinoSuna3,
HoshinoSuna3_w,
Branmarker2,
YoujuuSenki2, // Youjuu Senki 2 -Reimei no Senshitachi-
YoujuuSenki2_w,
DORBestJoukan_w, // DOR Best Selection Joukan (1+2)
DORBestGekan_w, // DOR Best Selection Gekan (3)
RougenoDen, // Rouge no Densetsu
RougenoDen_w,
HoshinoSuna12_w, // Hoshi no Suna Value Collection (1+2)
Toriko,
CrySweeper,
CrySweeper_w,
Toriko2,
KiminiSteady,
Sensei1,
KanaImouto,
Sensei2,
Sensei2_e,
Hoshizora, // Hoshizora Planet
Honeymoon, // Plastic Honeymoon
Crescendo, // Crescendo ~Eien da to Omotte Ita Ano Koro~
Crescendo_e,
KazoKei, // Kazoku Keikaku
KazoKei_e, // Family Project
GakuRenChuu, // Gakuen Ren'Ai Chuuihou
Destiny,
Sumeragi, // Sumeragi no Mikotachi
Sensei3,
Restore,
Yukizaku, // Yukizakura
Yukizaku_e, // Snow Sakura
HardLove,
UminoMichi,
SakinoLove, // Saki no Love Love
Hitoshizuku, // Natsu no Hitoshizuku
EigouKaiki,
Toshiaki,
ReijouClub, // Reijou Club ~Dajoku no Ryoshuutachi~

// Apple Pie
TokyoKyonyuu, // E [i:] Tokyo Kyonyuu Story
Choubaku,
DennouGarou,
DengekiDiv, // Dengeki Division
Lakers1, // Sei Shoujo Sentai Lakers
Meena, // Metamorph Meena
ElementalO, // Elemental O
Lakers2,
Lakers12_w,
JanshiMu, // Pretty Mahjong Soldier Mu
Jastrike, // Metal Mover Jastrike
PresentDuo, // Present 1 + 2 (developed by Orange House)
RusMahjong, // Russian Mahjong Harashou
Charade,
MittsunoNegai, // 3tsu no Negai
TwinRoad, // Reijuu - Twin Road
Lakers3,
Lakers3_w,
JankiMahjong, // Janki ~Ladies Mahjong League~ (developed by Succubus)
Taboo_w, // Kinki Taboo (developed by Succubus)
Datenshi, // Datenshi no Sumika
OniCoach, // Oni Coach!! ~Shouri e no Ryoujoku~
Utagoe, // Utagoe (developed by Succubus)
Robokko, // Love Love Robokko ~Sono ko, Koutetsu!!~
Fern, // Fern ~Dakara Kimi no Koto~
SeishunOuka,
MizuTenbin, // Mizu Tenbin ~Nageki no Namida~
CyberFairy, // Dennou Yousei ~Cyber Fairy~

// FMC
YokohamaElegy,
GlassnoUnmei,

// Santa Fe
BishoujoZukan, // there's also a '91 edition, which is identical afaict
ApproachShot, // 101-kaime no Approach Shot
BishoujoAudition, // Bishoujo Audition - Idol o Sagase!

// Game Technopolis
OtomeParty,
Ligarued,
DragonEyes, // Dragon Eyes - The Space Opera
Tsukunechan, // Tsukune-chan no Daibouken Dotou no Karimenkyo Hen
KimiDake, // Kimi Dake ni Ai wo...
HimiHana, // Himitsu no Hanazono
Tesera, // Tesera IV -Kimi wa, Yogoreta Tenshi ka Seinaru Majo ka!?-
HatsuMono, // Hatsukoi Monogatari
DigiAnge, // Dennou Tenshi: Digital Ange

// Fairytale
Fairytale,
AbunaiJosei, // Abunai Josei Shinrigaku Nyuumon
HotMilk,
KoroshinoDress1,
LipstickAdv1,
DragoonArmor, // Dragoon Armor for Adult
KoroshinoDress2,
LipstickAdv2,
UrotsukiDouji,
Xna, // X-na
DragonCity,
WatashioGolf, // Watashi o Golf ni Tsuretette
KounaiShasei1, // Kounai Shasei vol. 1 - Yonimo H na Monogatari
KounaiShasei2,
KounaiShasei3,
Saori, // Saori -Bishoujotachi no Yakata-
XIX, // Gize! XIX
Mai, // Mai - Funifuni Papyuun to Iwasecharu!
KareiNaruJin1, // Karei Naru Jinsei - Mina-san no Okage Desu
LamMal, // Lam-Mal
DeadBrain1, // Dead of the Brain ~Shiryou no Sakebi~
KuruKaji, // Kurutta Kajitsu
Yumeji, // Yumeji - Asakusa-Kitan
ShinjukuMono, // Shinjuku Monogatari
Dracula, // Dracula Hakushaku
KoroshinoDress3,
AkikoPremium,
MarinePhilt,
LipstickAdv3,
ToumeiNingen, // Toumei Ningen - Arawaru Arawaru
Naomi, // Naomi ~Bishoujotachi no Yakata~
GokuMandala, // Gokuraku Mandala
Chiemi,
DeadBrain2,
Miho,
Jankirou,
Arabesque, // Arabesque! ~Shoujotachi no Orinasu Ai no Monogatari~
Necronomicon,
HimiShi19191, // Himitsu Shirei 1919 Dai 1 wa Ojousama o Shibariagero: Erika...
HimiShi19192, // Himitsu Shirei 1919 Dai 2 wa Miko o Kudokiotose: Chizuru...
SayakaGibo,
KareiNaruJin2, // Karei Naru Jinsei 2
AkikoGold,
VirtuaCall1,
Ballade, // Maria ni Sasageru Ballade
RomaTsuKa1, // Romance wa Tsurugi no Kagayaki - The Last Crusader
JinmonYuugi,
Manami, // Manami ~Ai to Koukan no Hibi~
Moongate,
VirtuaCall2,
Kanako,
XGirl,
BlueEyes,
Dousoukai, // Dousoukai ~Yesterday Once More~
Dousoukai_M, // Dousoukai Memorial
JoiMinako,
AkikoHard,
VirtuaCall3,
Natural, // Natural ~Mi mo Kokoro mo~
Palette,
LipstickAdvEx,
HeartniHi, // Heart ni Hi o Tsukete...
RomaTsuKa2,
TwinkleRevue,
TenninKyoushi,
GiriShimai,
Echo,
Natural2,
Sakuranbo, // Sakuranbo Kaigan
BBJonny, // B.B. Oh, yes! Jonny!
WhiteXmas, // White Christmas ~Yuki Furu Yoru no Koibitotachi~
NaturalZero,
LipstickAdv4,
TokiMori, // Toki no Mori no Monogatari ~Tear~
KenkyuuNisshi,
HimaSaku, // Himawari no Saku Machi
ItaHime, // Itazura Hime
HitsuYuu, // Hitsujitachi no Yuuutsu ~Concrete ni Utsuru Kage~
HelloGoodbye, // Hello Goodbye! ~Otoko Tomodachi Onna Tomodachi~

// Cocktail Soft
CanCanB1, // Can Can Bunny
HareNochiOosawagi,
SailorFukuSeFe, // Sailor Fuku Senshi Ferris/Felis
CanCanB2, // Can Can Bunny Superior
SekaideIchiban, // Sekai de Ichiban Kimi ga Suki
Illumina,
Nike,
CanCanB3, // Can Can Bunny Spirits
CosmicPsycho,
SotsugyouShashin,
Miki,
BishoujoTsuushin, // Bishoujo Tsuushin - Chat no Susume
CanCanB4, // Can Can Bunny Premiere
DengekiNurse1,
Mayumi,
Kerakera, // Kerakera Hoshi
WillnoDengon,
Jyb, // Jyb ~Mehame Haruuga wa Himitsu no Jumon~
CanCanB5, // Can Can Bunny Extra
KurukuruParty, // Kurukuru Party ~Princess Quest~
DenwanoBellga,
CustomMate1,
Angel,
DemonCity,
Halfmoon, // Halfmoon ni Kawaru made ~Ramiya Ryou no Niji-iro Tamatebako~
DengekiNurse2,
CustomMate2,
CanCanB5half, // Can Can Bunny Limited 5 1/2
TrueHeart,
DokidokiVacation, // Dokidoki Vacation ~Kirameku Kisetsu no Naka de~
Angel120, // Noushuku Angel 120%
ItsukaDokokade,
DoradoraEmotion, // Doradora Emotion ~Seihaiden~
HareNochiMunasawagi,
CustomMate3,
CustomMate3_w,
NyanNyanTyphoon,
MixCandy,
TravelJunction,
PiaCarrot, // Pia Carrot e Youkoso! ~We've Been Waiting for You~
CanCanBP2, // Can Can Bunny Premiere 2
MixCandy2,
HareNochiTokidoki, // Hare Nochi Tokidoki Munasawagi
PiaCarrot2,
MixCandy3,
WithYou,
PiaCarrotToybox,
WithYouToybox,
PrincessMemory,
LoveNavi, // Love Love Navigation ~Koi no Menkyo Koushuu~
CanCanB6, // Can Can Bunny I <3 Mail
Canvas, // Canvas ~Sepia-iro no Motif~
UnivKoi, // Univ ~Koi Hajimaru Yo~
UnivAi, // Univ ~Ai Omatase~
TenkuunoSym, // Tenkuu no Symphonia
Kakubako, // Cocktail Soft Special Box
PiaCarrotGO, // Pia Carrot ~Grand Open~
TenkuunoSym2,
PiaCarrotGOToybox,
PiaJong,
PiaCarrotGOToybox2,
ThiruaPanic,
PiaCarrotGP, // Pia Carrot ~Gakuen Princess~

// Dez Climax
InjuuGakuen, // Injuu Gakuen ~La*Blue Girl~
YoujuuKyou, // Youjuu Kyoushitsu
ShinYoujuuKyou1,
ShinYoujuuKyou2,
ShinYoujuuKyou3,
InjuuGakuenEX,

// Cat's Pro
CatsPart1,
Nova,
HHG, // Heart Heat Girls
Feti, // or Fechi
Ikenie, // Ikenie ~Kyouraku no Shinden~
BineJoune, // Binetsu Jounetsu
Spark,

// Melody
Escalation95, // (published by Fairy Dust)
Mesuneko, // Mesuneko Hishoshitsu
Mesuneko_w,
NightSlave,
Kurayami,
Kurayami_w,
Melody, // Melody ~Koi no Messenger Girl~
Collector, // Collector ~Seifuku no Mezameru Toki~
Kurayami2, // Kurayami 2 ~Tozasareta Meikyuu~
EscalationAH, // Escalation -Aoi Houyou-
LoveLesson,
Rendezvous,
Yasoumu,
Katekyo, // Katekyo ~Akaruku Tanoshiku Koku Ecchi~
MesuNekoYuugi,

// Elf
ShutterChance, // Dokidoki Shutter Chance!
PrivateSchool,
AngelHearts,
Pinky1, // Pinky Ponky
Pinky2,
Pinky3,
RunRunConcerto, // Run Run Kyousoukyoku
DK1, // Dragon Knight 1
Foxy1,
RayGun,
DeJa1,
DK2,
Foxy2,
Elle,
DK3,
Shangrlia,
TenshinRanma,
DeJa2,
JanJakaJan,
Doukyuusei,
CurseofCastle,
MetalEye1,
MetalEye1Special,
WordsWorth,
WordsWorthSpecial,
Shangrlia2,
Shangrlia2Special,
DK4,
DK4Special,
MetalEye2,
MetalEye2Special,
Doukyuusei2,
Doukyuusei2Special,
Isaku,
IsakuSpecial,
Kakyuusei,
KakyuuseiSpecial,
YuNo, // Kono Yo no Hate de Koi o Utau Shoujo YU-NO
YuNoSpecial,
Shuusaku,
RefrainBlue,
Kisaku,
Hagesaku,
AshiYuki, // Ashita no Yukinojou
Hyakki, // Hyakki ~Inmoku Sareta Haikyo~
AshiYuki2, // Masaru: Ashita no Yukinojou 2
LimeIroSen, // Lime Iro Senkitan
KawaIchi2, // Kawarazakike no Ichizoku 2
LimeIroJan, // Lime Iro Jankitan
Kakyuusei2,
LimeIroRyuu, // Lime Iro Ryuukitan
HanatoHebi,

// Silky's
ShijuuhachiYa, // 48 Night Story
Premium1,
IkuikuPakkun,
Premium2,
OhPai,
Crescent,
KawaIchi, // Kawarazakike no Ichizoku
Reira, // REIRA - Slave Doll
NonoByou, // Nonomura Byouin no Hitobito
AiShimai, // Ai Shimai ~Futari no Kajitsu~
Birthdays,
UshiRaku, // Ushinawareta Rakuen
Koihime,
MobiusRoid, // Möbius Roid
Figure, // Figure ~Ubawareta Houkago~
Jack, // Jack ~Haitoku no Megami~
Fermion, // Fermion ~Mirai kara no Houmonsha~
Birthdays2, // Valentine Kiss: Birthdays 2
Beyond, // Be-Yond

// Aaru
RoseBlood98,
RoseBlood_w,
RoseBlood_e,
MHard98,
MHard_w,
ZestFan98, // Zest to Fantasy
ZestFan_w,
RabidHelix,
LoftyForm,
LAG, // Love and Grapple
Kokoro1, // Kokoro...
Guardian,
Enomoto, // LOE ~Legend of Enomoto~
KouteiHeika, // Koutei Heika ni Narou!
Kokoro2, // Kokoro... II
DiG, // D.i.G
AkaUta, // Akairo no Utage
SenMusu, // Senryaku Musume
NakedEdge,
Flowers,
Kokoro0, // Kokoro... 0

// Leaf
DR2NightJanki,
Shizuku98,
Shizuku_w,
Kizuato98,
Kizuato_w,
SaorintoIssho,
ToHeart,

// AIL
Skirmish,
MahaBarata,
DualSoul98,
DualSoul_w,
InmaSeiGa98, // Inma Seifuku Gari
InmaSeiGa_w,
MajoGari98, // Majo Gari no Yoru ni
MajoGari_w,
Kyouhaku98,
Kyouhaku_w,
RuriironoYuki,
Ryoujoku, // Ryoujoku ~Suki desu ka?~
MEM, // M.E.M. ~Yogosareta Junketsu~
ShinRuriiro, // Shin Ruriiro no Yuki ~Furimukeba Tonari ni~
OnceMoreAgain,
SakuranoShizuku,

// Silence, Technobrain
GuynaRock, // Ginsei Senshin Guyna-Rock
GuynaRockMini, // Minimum Guyna-Rock
ValKaizer, // Kikou Shoushin Val-Kaizer
GuynaRock2,
Lime1, // Houma Hunter Lime, or Jewel BEM Hunter Lime
Lime2,
Lime3,
Lime4,
Lime5,
Lime6,
Lime7,
Lime8,
Lime9,
Lime10,
Lime11,
Lime12,
ExcitingMilk1,
ExcitingMilk2,

// Sogna
// (There are a lot of versions of all Viper games, hard to keep them straight...)
AnimahjongV3,
ViperV6,
ViperV6Turbo, // voice samples plus quality of life
ViperV6RS, // somehow different from Turbo?
ViperV6_w,
ViperV6R, // windows DVD release
ViperV8,
ViperV8RS, // voice samples etc
ViperV8_w,
ViperV10,
ViperV10_w,
AnimahjongX,
ViperGTS,
ViperGTS_w,
ViperV12,
ViperV12_w,
ViperV16,
ViperV16_w,
ViperBTR,
ViperCTR, // ~Asuka~
ViperCTR_w,
ViperF40,
GuynaRockR,
GuynaRockR_w,
ViperM1,
ViperIsland1,
ViperIsland2,
ViperF50,
ViperIsland3,
ViperIsland4,
ViperIsland5,
ViperIsland6,
ViperGTB,
ViperGT1,
TypingViper,
ViperM5,
ViperRSR,
ViperM3,

// Fairy Dust
StarTrap87, // Cream Lemon - Star Trap (developed by Jast, based on a notable Cream Lemon episode)
Kuroneko, // Kuroneko Kan
KazeTachinu, // Ami - Kaze Tachinu (developed by Apple Pie)
KoukanNikki1,
Rall3, // Rall III - Kakusei Hen
GoJikanme, // 5 Jikanme no Venus (Tou Moriyama Special)
LemonAngel,
Virgin2,
KoukanNikki2, // Koukan Nikki Dai 2 Shou - Seiya o Anata to
StarTrap, // (remake)
AkaiSuishou, // Akai Suishou no Hitomi
MaDoll,
MaDoll_w,
Ukiuki, // Uki Uki Island
AmiShoushin, // Ami - Shoushin no Tenshi
Happy,

// AIC Spirits
PrettySammy1, // Mahou Shoujo Pretty Sammy zenpen (part 1)
PrettySammy1_w,
PrettySammy2, // Mahou Shoujo Pretty Sammy kouhen (part 2)
PrettySammy2_w,
ElHazard, // Shinpi no Sekai El-Hazard

// Banpresto
AhMegamisama, // Ah! Megami-sama (Oh! My Goddess), developed by AIC etc
TenchiMuyou, // Tenchi Muyou! Ryo-Ohki, developed by AIC etc
Slayers, // developed by AIC

// Ange
Rose,
Kiss,
Like,
KaiketsuNikki,
SchoolFest, // School Festival ~Cosmos Sai Kitan~
SExpress,
Arbeit, // Arbeit ~Futari no Omoi~
Leap, // Leap Toki ni Sarawareta Shoujo
Coin,
Coin_w,
Card,
ChaosBlood,
Keisei,
RakuennoNatsu,
Kohakuiro, // Kohakuiro no Namida
SweetLoveCon, // Sweet Love Concerto
Kaleidoscope, // Kaleidoscope ~Mangekyou~
TenohiranoYuki,
Purepure,
TopBreeder,
Life, // Life ~Omoide no Hashi ni~
Kurukuru, // Kurukuru Tenki wa Hanamaru Moyou
Eternity,
Nyuunyuu,

// Cuisse
MoreAndMore,

// Great
Foreigner,
Quintia2, // Quintia Road II
Mission,

// Sorciere
GeoSlave, // Geo Slave ~Aidotachi no Shouzou~
Persona, // Persona ~Ingyaku no Kamen~
StrangeWorld,
StrangeWorld_w,
StrangeWorld2, // Taxi Genmutan ~Strange World Act 2~
StrangeWorld2_w,
HomuranoMatsuri,
GrayJudge,
Karei,
MaeStro,

// Mercure
GimaiHitomi, // Gimai - Hitomi
GimaiHitomi_e, // Hitomi -My Stepsister-
Mensetsukan, // The Mensetsukan

// Guilty
ReijouSera,
HiToMi, // Hi-To-Mi
OniichanE,
Kokuhaku,
DoreiClub, // Bishoujo Dorei Club
IngokunoGakuen, // Ingoku no Gakuen - Biseito Choukyou
ReijouCaster, // Reijou Caster Mariko
Shiyouzumi, // Shiyouzumi ~Condom~
DennouMDorei, // Dennou M Dorei
Shiyouchuu, // Shiyouchuu ~W.C.~
Shiyouchuu_e, // Water Closet: The Forbidden Chamber
Ryoujokuki,
Miboujin, // ~Miboujin~ Niku Dorei
Gibo,
Gibo_e, // Gibo: Stepmother's Sin
BishiKutsu, // Bishimai Kutsujoku no Heya

// ASCII
GitenMegaTen, // Giten Megami Tensei Tokyo Mokushiroku

// Fournine
Lixus,
NekoManmaEx, // although pronounced "mamma", it means "kitty kibble" or "cat food"
NightShifter,
GaoGao1, // Radical Sequence
GaoGao2, // Pandora no Mori
GaoGao3, // Wild Force
WakuWakuMP, // Waku Waku Mahjong Panic! ~Shikigami Denshou~
//WakuWakuMP_e, // Legend of Fairies, flash port...
Lilith,
AngelNight, // Angel*Night ~Yamiyo o Kakeru Tenshitachi no Monogatari~
WakuWakuMP2, // Waku Waku Mahjong Panic! 2 ~Kokushi Musou~
//WakuWakuMP2_e, // Fairy Nights, flash port...
GaoGao4, // Canaan ~Yakusoku no Chi~

// Desire
HPlus, // H+
Mayclub98, // VR Date - May Club Gogatsu Kuchibi (PC-98)
Mayclub_w,
Mayclub_e, // May Club (Win)
MayclubDX,
Yuugiri, // Yuugiri - Ningyoushi no Isan
Yuugiri_w,
SaintDiary, // Saint Diary Kiyoka-chan no Nikki
SaintDiary_w,
Mayclub2,
SeikiTai, // Seikimatsu Taimaden Tsumugi-chan SOS
SumoHaku, // Sumomo Hakusho ~Anata to Itsu made mo~
Fuyajou, // Fuyajou ~Hikisakareta Junketsu~
JumokuHime,
ChinuHana, // W ~Chinurareta Hanayome~
OtoOmo, // Otona no Omochaya-san
SeiInma, // Sei*Inma ~Ijimete Inku-chan~
SumoHakuB, // Sumomo Hakusho Bangai Hen ~Yawaraka na Hikari no Naka de~

// Software House Parsley
Lime,
Image1,
Image2,
Panorama, // Panorama Club
Venus,
Farce, // Farce - Yuuwaku Hakusho
MugR, // Mug-R
TrueLove98, // Junai Monogatari
TrueLove_w,
TrueLove_e,
Reno98, // RE-NO Stayin' Alive
Reno_w,
MugenMeikyuu1, // Desert Time Mugen no Meikyuu
MugenMeikyuu2, // Phantom Knight Mugen no Meikyuu 2
SonicPrincess,
MugenMeikyuu3, // Mugen no Meikyuu 3 Type S

// Apricot
Nocturn98, // Mugen Yasoukyoku (PC-98)
Nocturn_w,
Nocturn_e, // Nocturnal Illusion (Win)
Nocturn_r // Renewal
);
{$scopedEnums off}

// Use ChibiCRC to calculate a CRC for an EXE to try to identify it.
type ECompat = (GP_None = 0, GP_Resources = 1, GP_Playable = 2, GP_Completable = 3, GP_Excellent = 4);
type TGameRecord = record
		crc : dword;
		g : gid;
		desc : UTF8string;
		level : ECompat;
	end;
type PGameRecord = ^TGameRecord;

const GameRecordTable : array of TGameRecord = (

(crc : 0; g : gid.unknown;
desc : 'unknown';
level : GP_NONE),

(crc : $A0E0B145; g : gid.AdamDF_e;
desc : 'ADAM - The Double Factor (Win, en)';
level : GP_None),

(crc : $550C9407; g : gid.AkaiSuishou;
desc : 'Akai Suishou no hitomi (PC98)';
level : GP_Resources),

(crc : $6C76F1C1; g : gid.AkikoPremium;
desc : 'Akiko Premium (PC98)';
level : GP_Resources),

(crc : $2E3EB01E; g : gid.Akogare;
desc : 'Akogare (Win)';
level : GP_Resources),

(crc : $6FA361E7; g : gid.AmeKise;
desc : 'Ameiro no Kisetsu (Win)';
level : GP_Resources),

(crc : $23C66BF5; g : gid.Amy98;
desc : 'Amy to Yobanaide (PC98)';
level : GP_Completable),

(crc : $3C6F5A22; g : gid.Amy_w;
desc : 'Amy to Yobanaide (Win, jp)';
level : GP_Resources),

(crc : $432DDC85; g : gid.Amy_e;
desc : 'Amy''s Fantasies (Win, en)';
level : GP_Resources),

(crc : $43FD02AF; g : gid.AngelCrisis;
desc : 'Angel Crisis (Win)';
level : GP_Resources),

(crc : $E3D57777; g : gid.AngelHearts;
desc : 'Angel Hearts (PC98)';
level : GP_Resources),

(crc : $FA4A84E4; g : gid.AngelNight;
desc : 'Angel Night (PC98)';
level : GP_Resources),

(crc : $65F71A2C; g : gid.AngelsC1;
desc : 'Tenshitachi no Gogo Collection (PC98)';
level : GP_Completable),

(crc : $9AC5B678; g : gid.AngelsC2;
desc : 'Tenshitachi no Gogo Collection 2 (PC98)';
level : GP_Completable),

(crc : $806A6D9F; g : gid.AngelsEve3H;
desc : 'Tenshitachi no Gogo 3 Bangaihen Hanseiban (PC98)';
level : GP_Resources),

(crc : $AD41C242; g : gid.AngelsEve5;
desc : 'Tenshitachi no Gogo 5 (PC98)';
level : GP_Resources),

(crc : $004D8450; g : gid.AngelsSpecial2;
desc : 'Tenshitachi no Gogo Special 2 (PC98)';
level : GP_Resources),

(crc : $7C1715D3; g : gid.AnimahjongV3;
desc : 'Animahjong V3 (PC98)';
level : GP_Resources),

(crc : $D878C700; g : gid.ApproachShot;
desc : '101 Kaime no Approach Shot (PC98)';
level : GP_Resources),

(crc : $36A4A588; g : gid.Arabesque;
desc : 'Arabesque! ~Shoujotachi no Orinasu Ai no Monogatari~ (PC98)';
level : GP_Resources),

(crc : $0A236E0C; g : gid.Ash;
desc : 'Ash (PC98)';
level : GP_Resources),

(crc : $7DABF645; g : gid.Bacta1;
desc : 'Bacta 1 (PC98)';
level : GP_Resources),

(crc : $6A8175AF; g : gid.Bacta2;
desc : 'Bacta 2 (PC98)';
level : GP_Resources),

(crc : $798BCD07; g : gid.BakkonStreet1;
desc : 'Totsugeki! Back on the Street (PC98)';
level : GP_Resources),

(crc : $F80482BB; g : gid.BakkonStreet2;
desc : 'Totsugeki! Back on the Street 2 - Hunting Roulette (PC98)';
level : GP_Resources),

(crc : $DADBED50; g : gid.BineKyouCherry_e;
desc : 'Amorous Professor Cherry (Win)';
level : GP_Resources),

(crc : $CFCB8AD1; g : gid.BishoujoAudition;
desc : 'Bishoujo Audition - Idol o Sagase! (PC98)';
level : GP_Resources),

(crc : $F01E12C8; g : gid.BishoujoTsuushin;
desc : 'Bishoujo Tsuushin - Chat no Susume (PC98)';
level : GP_Resources),

(crc : $9B85A8AF; g : gid.BishoujoZukan;
desc : 'Disk Bishoujo Daizukan (PC98)';
level : GP_Resources),

(crc : $935B4662; g : gid.CanCanB2;
desc : 'Can Can Bunny Superior (PC98)';
level : GP_Resources),

(crc : $806CAE65; g : gid.CanCanB3;
desc : 'Can Can Bunny Spirits (PC98)';
level : GP_Resources),

(crc : $BB13B189; g : gid.CanCanB4;
desc : 'Can Can Bunny Premiere (PC98)';
level : GP_Resources),

(crc : $19DCF4EA; g : gid.CanCanB5;
desc : 'Can Can Bunny Extra (PC98)';
level : GP_Resources),

(crc : $3D955542; g : gid.CanCanB5half;
desc : 'Can Can Bunny Limited (PC98)';
level : GP_Resources),

(crc : $AE4634C1; g : gid.CatsPart1;
desc : 'Cat''s part 1 (PC98)';
level : GP_Resources),

(crc : $41C5626E; g : gid.Chain_e;
desc : 'Chain - Lost Footprints (Win, en)';
level : GP_Resources),

(crc : $805589FF; g : gid.Checkin_e;
desc : 'Tokimeki Check-In! (Win, en)';
level : GP_Resources),

(crc : $B8E3438D; g : gid.Chiemi;
desc : 'Chiemi (PC98)';
level : GP_Resources),

(crc : $3A5A6BA6; g : gid.Choubaku;
desc : 'Chou Baku (PC98)';
level : GP_Resources),

(crc : $A75FF10F; g : gid.CobraMission_e;
desc : 'Cobra Mission (DOS)';
level : GP_None),

(crc : $B464EADB; g : gid.CosmicPsycho;
desc : 'Cosmic Psycho (PC98)';
level : GP_Resources),

(crc : $42237633; g : gid.CrazyKnuckle2;
desc : 'Crazy Knuckle 2 (Win)';
level : GP_Resources),

(crc : $E55175A9; g : gid.CurseofCastle;
desc : 'Curse of Castle (PC98)';
level : GP_Resources),

(crc : $3E7554D2; g : gid.DArk;
desc : 'D''ark (PC98)';
level : GP_Resources),

(crc : $ACA30EEF; g : gid.DArkGaiden;
desc : 'D''ark Gaiden (PC98)';
level : GP_Resources),

(crc : $D57D9567; g : gid.DBaBos;
desc : 'D''BaBos (PC98)';
level : GP_Resources),

(crc : $D2FF5792; g : gid.DeadBrain1;
desc : 'Dead of the Brain ~Shiryou no Sakebi~ (PC98)';
level : GP_Resources),

(crc : $B0F222E1; g : gid.DeadBrain2;
desc : 'Dead of the Brain 2 (PC98)';
level : GP_Resources),

(crc : $FD5B952C; g : gid.Deep;
desc : 'Deep (PC98)';
level : GP_Resources),

(crc : $6399DE5B; g : gid.DeFaNa;
desc : 'De.FaNa (PC98)';
level : GP_Resources),

(crc : $6D5D1923; g : gid.DeJa1;
desc : 'De-Ja 1 (PC98)';
level : GP_Resources),

(crc : $4FD1D481; g : gid.DeJa2;
desc : 'De-Ja 2 (PC98)';
level : GP_Resources),

(crc : $BB13B255; g : gid.DengekiNurse1;
desc : 'Dengeki Nurse (PC98)';
level : GP_Resources),

(crc : $7B6BF87A; g : gid.DennouGarou;
desc : 'Dennou Garou (PC98)';
level : GP_Resources),

(crc : $EA47523F; g : gid.Desire98;
desc : 'Desire - Haitoku no Rasen (PC98)';
level : GP_Playable),

(crc : $EA5061F4; g : gid.Desire_e;
desc : 'Desire - Haitoku no Rasen (win, en)';
level : GP_None),

(crc : $7CEDA56E; g : gid.Desire_w;
desc : 'Desire - Haitoku no Rasen (win, jp)';
level : GP_None),

(crc : $0EEC8798; g : gid.DigiAnge;
desc : 'Dennou Tenshi: Digital Ange (PC98)';
level : GP_Resources),

(crc : $692B6055; g : gid.DiviDead;
desc : 'Divi-Dead (Win, jp)';
level : GP_Resources),

(crc : $B8333D8E; g : gid.DiviDead_e;
desc : 'Divi-Dead (Win, en)';
level : GP_Resources),

(crc : $E9157779; g : gid.DK1;
desc : 'Dragon Knight 1 (PC98)';
level : GP_Resources),

(crc : $D80805E9; g : gid.DK2;
desc : 'Dragon Knight 2 (PC98)';
level : GP_Resources),

(crc : $64333C33; g : gid.DK3;
desc : 'Dragon Knight 3 (PC98)';
level : GP_Resources),

(crc : $2847BEC6; g : gid.DK3_e;
desc : 'Knights of Xentar (DOS)';
level : GP_None),

(crc : $F0D0D719; g : gid.DK4;
desc : 'Dragon Knight 4 (PC98)';
level : GP_Resources),

(crc : $F0D0D71A; g : gid.DK4Special;
desc : 'Dragon Knight 4 Special Disk (PC98)';
level : GP_Resources),

(crc : $CDC8D361; g : gid.Doukyuusei;
desc : 'Doukyuusei (PC98)';
level : GP_Resources),

(crc : $E11D1175; g : gid.Doukyuusei2;
desc : 'Doukyuusei 2 (PC98)';
level : GP_None),

(crc : $D108AA55; g : gid.Doukyuusei2Special;
desc : 'Doukyuusei 2 Special Disk (PC98)';
level : GP_None),

(crc : $34BEF3A5; g : gid.Doushin_e;
desc : 'Doushin - Same Heart (Win, en)';
level : GP_Resources),

(crc : $E6326645; g : gid.Dracula;
desc : 'Dracula Hakushaku (PC98)';
level : GP_Resources),

(crc : $49DF8550; g : gid.DragonCity;
desc : 'Dragon City (PC98)';
level : GP_Resources),

(crc : $A0368807; g : gid.DragonEyes;
desc : 'Dragon Eyes - The Space Opera (PC98)';
level : GP_Resources),

(crc : $041AE064; g : gid.DragoonArmor;
desc : 'Dragoon Armor (PC98)';
level : GP_Resources),

(crc : $AF8463CA; g : gid.DualSoul98;
desc : 'Dual Soul (PC98)';
level : GP_Resources),

(crc : $38C8034D; g : gid.Eden;
desc : 'Eden no Kaori (PC98)';
level : GP_Completable),

(crc : $BA79A8F8; g : gid.Elle;
desc : 'Elle (PC98)';
level : GP_Resources),

(crc : $BE0A6B75; g : gid.Enomoto;
desc : 'Legend of Enomoto (Win)';
level : GP_Resources),

(crc : $AEE96540; g : gid.Eroden;
desc : 'Eroden (Win)';
level : GP_Resources),

(crc : $C5B18AEE; g : gid.Escalation95;
desc : 'Escalation ''95 (PC98)';
level : GP_Resources),

(crc : $BF0E569D; g : gid.EtsuGaku98;
desc : 'Etsuraku no Gakuen (PC98)';
level : GP_Playable),

(crc : $88BB724D; g : gid.EtsuGaku98;
desc : 'Etsuraku no Gakuen (Win, jp)';
level : GP_Resources),

(crc : $B80A1F76; g : gid.EtsuGaku_e;
desc : 'Love Potion (Win, en)';
level : GP_Resources),

(crc : $C63D845A; g : gid.EveBE98;
desc : 'EVE - Burst Error (PC98)';
level : GP_Playable),

(crc : $043F169A; g : gid.EveBE_e;
desc : 'EVE - Burst Error 1999 (Win, en)';
level : GP_None),

(crc : $5DAFDCD3; g : gid.EveBE_r;
desc : 'EVE - Burst Error 2003 (Win, jp)';
level : GP_None),

(crc : $1F76452F; g : gid.EveBE_r98;
desc : 'EVE - Burst Error pc98ver (Win, jp)';
level : GP_None),

(crc : $B1551C63; g : gid.ExcitingMilk1;
desc : 'Exciting Milk (PC98)';
level : GP_Resources),

(crc : $B1551C64; g : gid.ExcitingMilk2;
desc : 'Exciting Milk 2 (PC98)';
level : GP_Resources),

(crc : $99E9E737; g : gid.Feti;
desc : 'Feti (PC98)';
level : GP_Resources),

(crc : $F3531958; g : gid.Foreigner;
desc : 'Foreigner (PC98)';
level : GP_Playable),

(crc : $760903F5; g : gid.Fortuna; // fortdvd.exe
desc : 'Fortuna (Win)';
level : GP_Resources),

(crc : $B01B65BC; g : gid.Foxy1;
desc : 'Foxy (PC98)';
level : GP_Resources),

(crc : $4EF40825; g : gid.Foxy2;
desc : 'Foxy 2 (PC98)';
level : GP_Resources),

(crc : $CE363C50; g : gid.FromH;
desc : 'Shyuukan From H (PC98)';
level : GP_Playable),

(crc : $D81BFE78; g : gid.GakuBakuTen;
desc : 'Gakuen Bakuretsu Tenkousei (Win)';
level : GP_Resources),

(crc : $BF17EF40; g : gid.GaoGao1;
desc : 'Gao Gao! 1st (PC98)';
level : GP_Resources),

(crc : $AD502F00; g : gid.GaoGao2;
desc : 'Gao Gao! 2nd (PC98)';
level : GP_Resources),

(crc : $80A34115; g : gid.GaoGao3;
desc : 'Gao Gao! 3rd (PC98)';
level : GP_Resources),

(crc : $F37D61D6; g : gid.GaoGao4;
desc : 'Gao Gao! 4th - Canaan (PC98)';
level : GP_Resources),

(crc : $7B8182DE; g : gid.GimaiHitomi_e;
desc : 'Hitomi -My Stepsister- (Win, en)';
level : GP_Resources),

(crc : $56F93669; g : gid.GitenMegaTen;
desc : 'Giten Megami Tensei Tokyo Mokushiroku (PC98)';
level : GP_Resources),

(crc : $3780A15A; g : gid.GlassnoUnmei;
desc : 'Glass no Unmei (PC98)';
level : GP_Resources),

(crc : $FFAFD8E1; g : gid.Gloria98;
desc : 'Gloria ~Kindan no Ketsuzoku~ (PC98)';
level : GP_Resources),

(crc : $AA4387EE; g : gid.Gloria_w;
desc : 'Gloria ~Kindan no Ketsuzoku~ (Win, jp)';
level : GP_Resources),

(crc : $A27807D4; g : gid.Gloria_e;
desc : 'Gloria ~Kindan no Ketsuzoku~ (Win, en)';
level : GP_Resources),

(crc : $B417F90F; g : gid.GoJikanme;
desc : '5 Jikanme no Venus (PC98)';
level : GP_None),

(crc : $AFEC8EB1; g : gid.GokuMandala;
desc : 'Gokuraku Mandala (PC98)';
level : GP_Resources),

(crc : $A0D7925D; g : gid.Guardian;
desc : 'Guardian (Win)';
level : GP_Resources),

(crc : $B7192B69; g : gid.GuynaRock;
desc : 'Ginsei Senshin Guyna-Rock (PC98)';
level : GP_Resources),

(crc : $688A1CD2; g : gid.GuynaRockMini;
desc : 'Minimum Guyna-Rock (PC98)';
level : GP_Resources),

(crc : $A556005B; g : gid.GuynaRock2;
desc : 'Ginsei Senshin Guyna-Rock 2 (PC98)';
level : GP_Resources),

(crc : $AF7B6372; g : gid.HananoKioku1;
desc : 'Hana no Kioku (PC98)';
level : GP_Resources),

(crc : $AF7B6373; g : gid.HananoKioku2;
desc : 'Hana no Kioku 2 (PC98)';
level : GP_Resources),

(crc : $A5174E6B; g : gid.HananoKioku5demo;
desc : 'Hana no Kioku 5 demo (Win)';
level : GP_None),

(crc : $FB13B1AA; g : gid.HatsuMono;
desc : 'Hatsukoi Monogatari (PC98)';
level : GP_Resources),

(crc : $A0929DA3; g : gid.HHG;
desc : 'Heart Heat Girls (PC98)';
level : GP_Resources),

(crc : $DDAD5ED0; g : gid.HimiHana;
desc : 'Himitsu no Hanazono (PC98)';
level : GP_Resources),

(crc : $EBD54729; g : gid.Hohoemi;
desc : 'Tenshitachi no Hohoemi (PC98)';
level : GP_Playable),

(crc : $99A38802; g : gid.Hohoemi_w;
desc : 'Tenshitachi no Hohoemi (Win)';
level : GP_Resources),

(crc : $065050E9; g : gid.HopStep;
desc : 'Hop Step Jump (PC98)';
level : GP_Resources),

(crc : $EA0E569A; g : gid.HornyBun1_e;
desc : 'Do You Like Horny Bunnies? (Win, en)';
level : GP_Resources),

(crc : $3D2498DF; g : gid.HornyBun2_e;
desc : 'Do You Like Horny Bunnies? 2 (Win, en)';
level : GP_Resources),

(crc : $A73A87C8; g : gid.HoshinoSuna12_w;
desc : 'Hoshi no Suna Monogatari Value Collection (Win)';
level : GP_Resources),

(crc : $52561CFC; g : gid.HPlus;
desc : 'H+ (PC98)';
level : GP_Resources),

(crc : $C2E490DF; g : gid.Ikenie;
desc : 'Ikenie (PC98)';
level : GP_Resources),

(crc : $5FBD3A16; g : gid.Illumina;
desc : 'Illumina (PC98)';
level : GP_Resources),

(crc : $37AD1902; g : gid.InjuuGakuen;
desc : 'Injuu Gakuen ~La*Blue Girl~ (PC98)';
level : GP_Resources),

(crc : $5A7F90AD; g : gid.InmaSeiGa98;
desc : 'Inma Seifuku Gari (PC98)';
level : GP_Resources),

(crc : $A5E99A88; g : gid.Izayoi;
desc : 'Izayoi (Win)';
level : GP_None),

(crc : $71467890; g : gid.JanJakaJan;
desc : 'Jan Jaka Jan (PC98)';
level : GP_Resources),

(crc : $0ECCF1C8; g : gid.Jankirou;
desc : 'Jankirou (PC98)';
level : GP_Resources),

(crc : $CB12C8C8; g : gid.JastMemo; // jast.exe, if installed
desc : 'Jast Memorial Collection (Win, en)';
level : GP_Resources),

(crc : $DC217274; g : gid.Kadowakashi;
desc : 'Kadowakashi (Win)';
level : GP_Resources),

(crc : $D9363554; g : gid.KaiketsuNikki;
desc : 'Kaiketsu Nikki (PC98)';
level : GP_None),

(crc : $9446AE24; g : gid.KareiNaruJin1;
desc : 'Karei Naru Jinsei (PC98)';
level : GP_Resources),

(crc : $149B2D37; g : gid.KimiDake;
desc : 'Kimi Dake ni Ai wo... (PC98)';
level : GP_Resources),

(crc : $55564E96; g : gid.KinKetsu98;
desc : 'Kindan no Ketsuzoku (PC98)';
level : GP_Playable),

(crc : $5AC67732; g : gid.KinKetsu_w;
desc : 'Kindan no Ketsuzoku (Win, jp)';
level : GP_Resources),

(crc : $38F0DABB; g : gid.KinKetsu_e;
desc : 'Fatal Relations (Win, en)';
level : GP_Resources),

(crc : $9B7B7F9D; g : gid.KokoRaku1;
desc : 'Koko wa Rakuensou (PC98)';
level : GP_Resources),

(crc : $3F94D81B; g : gid.KokoRaku1_w;
desc : 'Koko wa Rakuensou (Win, jp)';
level : GP_Resources),

(crc : $D1075FA5; g : gid.KokoRaku1_e;
desc : 'Paradise Heights 1 (Win, en)';
level : GP_Resources),

(crc : $AF7B6355; g : gid.KokoRaku2;
desc : 'Koko wa Rakuensou 2 (PC98)';
level : GP_Resources),

(crc : $0070A69E; g : gid.KokoRaku2_w;
desc : 'Koko wa Rakuensou 2 (Win, jp)';
level : GP_Resources),

(crc : $75DA35C2; g : gid.KokoRaku2_e;
desc : 'Paradise Heights 2 (Win, en)';
level : GP_Resources),

(crc : $F1168321; g : gid.KokoRaku3;
desc : 'Koko wa Rakuensou 3 (Win)';
level : GP_Resources),

(crc : $9D49AE76; g : gid.Kokoro1;
desc : 'Kokoro... (Win)';
level : GP_Resources),

(crc : $AFF51FA9; g : gid.KokoronoKakera;
desc : 'Kokoro no Kakera (Win)';
level : GP_Resources),

(crc : $D6801361; g : gid.KoroshinoDress1;
desc : 'Koroshi no Dress 1 (PC98)';
level : GP_Resources),

(crc : $C80AC614; g : gid.KoroshinoDress2;
desc : 'Koroshi no Dress 2 (PC98)';
level : GP_Resources),

(crc : $1D3CBA1D; g : gid.KoroshinoDress3;
desc : 'Koroshi no Dress 3 (PC98)';
level : GP_Resources),

(crc : $B4B4FBAE; g : gid.Kotobuki_e;
desc : 'Kotobuki (Win, en)';
level : GP_Resources),

(crc : $AF7B63C2; g : gid.KouChou;
desc : 'Kousoku Choujin (PC98)';
level : GP_Resources),

(crc : $394FFCF6; g : gid.KouChou_w;
desc : 'Kousoku Choujin (Win)';
level : GP_Resources),

(crc : $8506B397; g : gid.KoukanNikki1;
desc : 'Koukan Nikki (PC98)';
level : GP_Resources),

(crc : $1E471222; g : gid.KoukanNikki2;
desc : 'Koukan Nikki 2 (PC98)';
level : GP_Resources),

(crc : $A9AA67F1; g : gid.KounaiShasei1;
desc : 'Kounai Shasei vol. 1 - Yonimo H na Monogatari (PC98)';
level : GP_Resources),

(crc : $89E7CD83; g : gid.KounaiShasei2;
desc : 'Kounai Shasei vol. 2 (PC98)';
level : GP_Resources),

(crc : $E192AFCF; g : gid.KounaiShasei3;
desc : 'Kounai Shasei vol. 3 (PC98)';
level : GP_Resources),

(crc : $89EA5F51; g : gid.Kurayami;
desc : 'Kurayami (PC98)';
level : GP_Resources),

(crc : $ABB2988F; g : gid.KuronoKen;
desc : 'Kuro no Ken (PC98)';
level : GP_Resources),

(crc : $1F3DB039; g : gid.KuruKaji;
desc : 'Kurutta Kajitsu (PC98)';
level : GP_Resources),

(crc : $74CBC458; g : gid.Kyouhaku98;
desc : 'Kyouhaku (PC98)';
level : GP_Resources),

(crc : $5CB7B54B; g : gid.LAG;
desc : 'L.A.G. (Win)';
level : GP_Resources),

(crc : $D4623385; g : gid.Lakers1;
desc : 'Sei Shoujo Sentai Lakers (PC98)';
level : GP_Resources),

(crc : $E6790E30; g : gid.Lakers2;
desc : 'Sei Shoujo Sentai Lakers 2 (PC98)';
level : GP_Resources),

(crc : $25EFAA87; g : gid.LamMal;
desc : 'Lam-Mal (PC98)';
level : GP_Resources),

(crc : $11EA2AC0; g : gid.LetsPirates;
desc : 'Let''s! Pirates Trouble Vacance (Win)';
level : GP_Resources),

(crc : $DEEA1A52; g : gid.Like;
desc : 'LIKE (PC98)';
level : GP_None),

(crc : $815AE9FA; g : gid.Lilith;
desc : 'Lilith (PC98)';
level : GP_Resources),

(crc : $94A76EFE; g : gid.Lime1;
desc : 'Jewel BEM Hunter Lime 1 (PC98)';
level : GP_Completable),

(crc : $8CBA7E27; g : gid.Lime2;
desc : 'Jewel BEM Hunter Lime 2 (PC98)';
level : GP_Resources),

(crc : $922CDDA9; g : gid.Lime3;
desc : 'Jewel BEM Hunter Lime 3 (PC98)';
level : GP_Resources),

(crc : $69A6891A; g : gid.Lime4;
desc : 'Jewel BEM Hunter Lime 4 (PC98)';
level : GP_Resources),

(crc : $0BCE0E18; g : gid.Lime5;
desc : 'Jewel BEM Hunter Lime 5 (PC98)';
level : GP_Resources),

(crc : $D9E6A8F7; g : gid.Lime6;
desc : 'Jewel BEM Hunter Lime 6 (PC98)';
level : GP_Resources),

(crc : $7F1FCF67; g : gid.Lime7;
desc : 'Jewel BEM Hunter Lime 7 (PC98)';
level : GP_Resources),

(crc : $F6D9A6DD; g : gid.Lime8;
desc : 'Jewel BEM Hunter Lime 8 (PC98)';
level : GP_Resources),

(crc : $2081F2FB; g : gid.Lime9;
desc : 'Jewel BEM Hunter Lime 9 (PC98)';
level : GP_Resources),

(crc : $F5150389; g : gid.Lime10;
desc : 'Jewel BEM Hunter Lime 10 (PC98)';
level : GP_Resources),

(crc : $B9047466; g : gid.Lime11;
desc : 'Jewel BEM Hunter Lime 11 (PC98)';
level : GP_Resources),

(crc : $2D50FB1D; g : gid.Lime12;
desc : 'Jewel BEM Hunter Lime 12 (PC98)';
level : GP_Resources),

(crc : $BEE576CC; g : gid.LipstickAdv1;
desc : 'Lipstick ADV 1 (PC98)';
level : GP_Resources),

(crc : $88B32D06; g : gid.LipstickAdv2;
desc : 'Lipstick ADV 2 (PC98)';
level : GP_Resources),

(crc : $E7C3296F; g : gid.LipstickAdv3;
desc : 'Lipstick ADV 3 (PC98)';
level : GP_Resources),

(crc : $3003F0BA; g : gid.Lixus;
desc : 'Lixus (PC98)';
level : GP_Resources),

(crc : $FD4E3FE8; g : gid.LoftyForm;
desc : 'Lofty Form (Win)';
level : GP_Resources),

(crc : $C1EF9162; g : gid.MaDoll;
desc : 'Ma Doll (PC98)';
level : GP_Completable),

(crc : $1E0C1D33; g : gid.MaDoll_w;
desc : 'Ma Doll (Win)';
level : GP_Resources),

(crc : $78E2BFC3; g : gid.MahaBarata;
desc : 'Maha Barata (PC98)';
level : GP_Resources),

(crc : $2DCA1EC5; g : gid.Mai;
desc : 'Mai - Funifuni Papyuun to Iwasecharu! (PC98)';
level : GP_Resources),

{(crc : 0; g : gid.MaidStory;
desc : 'Maid Monogatari (Win, jp)';
level : GP_None),}

(crc : $5767F7F6; g : gid.MaidStory_e;
desc : 'The Maid Story (Win, en)';
level : GP_None),

(crc : $4822AB89; g : gid.MaigonoKimochi;
desc : 'Maigo no Kimochi (Win)';
level : GP_Resources),

(crc : $CFF4CCFA; g : gid.MajoGari98;
desc : 'Majogari no Yoru ni (PC98)';
level : GP_Resources),

(crc : $81A1F0C2; g : gid.Majokko;
desc : 'Majokko Paradise (PC98)';
level : GP_Completable),

(crc : $1FE126F8; g : gid.Majokko_w;
desc : 'Majokko Paradise (Win)';
level : GP_Resources),

(crc : $3AA1098F; g : gid.ManianaOnna1;
desc : 'Mania na Onna (Win)';
level : GP_Resources),

(crc : $7C918A4D; g : gid.ManianaOnna2;
desc : 'Mania na Onna 2 (Win)';
level : GP_Resources),

(crc : $E68175AE; g : gid.MarginalStories;
desc : 'Marginal Stories (PC98)';
level : GP_Resources),

(crc : $0E099A5B; g : gid.MarinePhilt;
desc : 'Marine Philt (PC98)';
level : GP_Resources),

(crc : $8476FBE0; g : gid.Maririn;
desc : 'Super Ultra Mucchin Puripuri Cyborg Maririn DX (PC98)';
level : GP_Resources),

(crc : $CD50066E; g : gid.Mayclub98;
desc : 'May Club (PC98)';
level : GP_Completable),

(crc : $CFB08395; g : gid.Mayclub_e;
desc : 'May Club (Win, en)';
level : GP_Completable),

(crc : $E5CD6663; g : gid.Meena;
desc : 'Metamorph Meena (PC98)';
level : GP_Resources),

(crc : $496E5075; g : gid.Mesuneko;
desc : 'Mesuneko Hishoshitsu (PC98)';
level : GP_Resources),

(crc : $C08DADE2; g : gid.MetalEye1;
desc : 'Metal Eye (PC98)';
level : GP_Resources),

(crc : $B541B17E; g : gid.MetalEye1Special;
desc : 'Metal Eye Special Disk (PC98)';
level : GP_Resources),

(crc : $F289ECFF; g : gid.MetalEye2;
desc : 'Metal Eye 2 (PC98)';
level : GP_Resources),

(crc : $DC7EC2F3; g : gid.MetalEye2Special;
desc : 'Metal Eye 2 Special Disk (PC98)';
level : GP_Resources),

(crc : $4FAB23EC; g : gid.MetalLace1;
desc : 'Ningyou Tsukai (PC98)';
level : GP_Resources),

(crc : $4C286277; g : gid.MetalLace2;
desc : 'Ningyou Tsukai 2 (PC98)';
level : GP_Resources),

(crc : $B1071D51; g : gid.MHard98;
desc : 'M Hard (PC98)';
level : GP_Completable),

(crc : $439AE2ED; g : gid.MHard_w;
desc : 'M Hard (Win)';
level : GP_Completable),

(crc : $BF6C178B; g : gid.Miho;
desc : 'Miho (PC98)';
level : GP_Resources),

(crc : $80E15BED; g : gid.Miki;
desc : 'Miki (PC98)';
level : GP_Resources),

(crc : $1FAC7314; g : gid.Mission;
desc : 'Mission (PC98)';
level : GP_None),

(crc : $E3C2514E; g : gid.MoreAndMore;
desc : 'More and More (PC98)';
level : GP_None),

(crc : $AF7B63D0; g : gid.Mukougawa;
desc : 'Sayonara no Mukougawa (PC98)';
level : GP_Resources),

(crc : $AC5DBB45; g : gid.Mukougawa_w;
desc : 'Sayonara no Mukougawa (Win)';
level : GP_Resources),

(crc : $BF4D2746; g : gid.NanaEi1;
desc : 'Nana Eiyuu Monogatari (PC98)';
level : GP_Resources),

(crc : $1F37F16B; g : gid.NanaEi_w;
desc : 'Nana Eiyuu Monogatari (Win)';
level : GP_Resources),

(crc : $417AFCBA; g : gid.NanaEi2;
desc : 'Nana Eiyuu Monogatari 2 (PC98)';
level : GP_Resources),

(crc : $06CCD1F8; g : gid.Naomi;
desc : 'Naomi ~Bishoujotachi no Yakata~ (PC98)';
level : GP_Resources),

(crc : $66ECD700; g : gid.Necronomicon;
desc : 'Necronomicon (PC98)';
level : GP_Resources),

(crc : $6EEB793D; g : gid.NekoManmaEx;
desc : 'Neko Manma EX (PC98)';
level : GP_Resources),

(crc : $ADA8030E; g : gid.NightShifter;
desc : 'Night Shifter (PC98)';
level : GP_Resources),

(crc : $F60029C7; g : gid.NightSlave;
desc : 'Night Slave (PC98)';
level : GP_Resources),

(crc : $95926FD8; g : gid.Nike;
desc : 'Nike (PC98)';
level : GP_Resources),

(crc : $F911F9CD; g : gid.Nocturn98;
desc : 'Mugen Yasoukyoku (PC98)';
level : GP_Completable),

(crc : $EE24DA1B; g : gid.Nocturn_e;
desc : 'Nocturnal Illusion (Win, en)';
level : GP_Completable),

(crc : $DE70D702; g : gid.Nocturn_r;
desc : 'Nocturnal Illusion Renewal (Win, jp)';
level : GP_Resources),

(crc : $F0AA32D7; g : gid.Nova;
desc : 'Nova (PC98)';
level : GP_Resources),

(crc : $188C8859; g : gid.Phobos;
desc : 'Phobos (PC98)';
level : GP_Resources),

(crc : $D511ED29; g : gid.PiaCarrot;
desc : 'Pia Carrot e Youkoso! ~We''ve Been Waiting for You~ (PC98)';
level : GP_Resources),

(crc : $20AA4DF7; g : gid.Pinky1;
desc : 'Pinky Ponky 1 (PC98)';
level : GP_Resources),

(crc : $CB26C88D; g : gid.Pinky2;
desc : 'Pinky Ponky 2 (PC98)';
level : GP_Resources),

(crc : $E6A9041B; g : gid.Pinky3;
desc : 'Pinky Ponky 3 (PC98)';
level : GP_Resources),

(crc : $0408332D; g : gid.PrettyParfait;
desc : 'Pom-pom Pretty Parfait Okosamayou (Win)';
level : GP_Resources),

(crc : $CE2D6179; g : gid.Quintia2;
desc : 'Quintia Road 2 (PC98)';
level : GP_Resources),

(crc : $22E2897D; g : gid.RabidHelix;
desc : 'Rabid Helix (Win)';
level : GP_Resources),

(crc : $A76D7EBB; g : gid.Rabyni;
desc : 'Rabyni (PC98)'; // actual "slaves"? or biblical "maid servants", or "chattel"?
level : GP_Resources),

(crc : $93443B25; g : gid.RayGun;
desc : 'Ray Gun (PC98)';
level : GP_Resources),

(crc : $E91E1854; g : gid.ReijouClub;
desc : 'Reijou Club ~Dajoku no Ryoshuutachi~ (Win)';
level : GP_None),

(crc : $05B8AA01; g : gid.Reno98;
desc : 'Re-No (PC98)';
level : GP_None),

(crc : $077C2A2C; g : gid.RingOut98;
desc : 'Ring Out! Pro Lesring (PC98)';
level : GP_None),

(crc : $41257758; g : gid.RingOut_e;
desc : 'Ring Out! Pro Lesring (Win)';
level : GP_Resources),

(crc : $AF7B67BB; g : gid.RinkanGakkou;
desc : 'Rinkan Gakkou (PC98)';
level : GP_Resources),

(crc : $FDA3F0A2; g : gid.RinkanGakkou_w;
desc : 'Rinkan Gakkou (Win)';
level : GP_Resources),

(crc : $8B6B61F1; g : gid.RoseBlood98;
desc : 'Rose Blood (PC98)';
level : GP_Completable),

(crc : $C4C52C75; g : gid.RoseBlood_w;
desc : 'Rose Blood (Win)';
level : GP_Completable),

(crc : $054CAF7F; g : gid.Rouraku;
desc : 'Rouraku Ingi ni Ochiru (Win)';
level : GP_Resources),

(crc : $617C7CE9; g : gid.Runaway;
desc : 'Runaway City (DOS)';
level : GP_Completable),

(crc : $CFBFCBC2; g : gid.Runaway98;
desc : 'Meisou Toshi (PC98)';
level : GP_Playable),

(crc : $86557779; g : gid.RunRunConcerto;
desc : 'Run Run Kyousoukyoku (PC98)';
level : GP_Resources),

(crc : $A15BF86A; g : gid.RuriiroNoYuki;
desc : 'Ruriiro no Yuki (PC98)';
level : GP_Resources),

(crc : $0AE0CF49; g : gid.Sagara_e;
desc : 'The Sagara Family (Win, en)';
level : GP_Resources),

(crc : $38DA53F9; g : gid.SaintDiary;
desc : 'Saint Diary - Kiyoka-chan no Nikki (PC98)';
level : GP_Resources),

(crc : $0E5B98C7; g : gid.Sakura;
desc : 'Season of the Sakura (DOS)';
level : GP_Completable),

(crc : $C19433D2; g : gid.Sakura98;
desc : 'Sakura no Kisetsu (PC98)';
level : GP_Completable),

(crc : $4C45C681; g : gid.Sanatorium;
desc : 'Sanatorium (Win)';
level : GP_Resources),

(crc : $AD6265D6; g : gid.SanShimai;
desc : 'The Three Sisters'' Story (DOS)';
level : GP_Completable),

(crc : $90371AFB; g : gid.SanShimai98;
desc : 'San Shimai (PC98)';
level : GP_Playable),

(crc : $860A6791; g : gid.Saori;
desc : 'Saori (PC98)';
level : GP_Resources),

(crc : $E6430557; g : gid.SchoolFest;
desc : 'School Festival ~Cosmos Sai Kitan~ (PC98)';
level : GP_None),

(crc : $0368F8F8; g : gid.SekaideIchiban;
desc : 'Sekai de Ichiban (PC98)';
level : GP_Resources),

(crc : $CD8F028C; g : gid.Sensei2_e;
desc : 'Sensei 2 (Win)';
level : GP_Resources),

(crc : $799265F3; g : gid.Setsujuu;
desc : 'Yuki Neko - Setsujuu - Yuganda Kioku (PC98)';
level : GP_Completable),

(crc : $5B879F23; g : gid.SexyParfait;
desc : 'Muchimuchi Sexy Parfait Oneesamayou (Win)';
level : GP_Resources),

(crc : $8763B67B; g : gid.Shangrlia;
desc : 'Shangrlia (PC98)';
level : GP_Resources),

(crc : $E5CD26FF; g : gid.Shangrlia2;
desc : 'Shangrlia 2 (PC98)';
level : GP_Resources),

(crc : $AAFBFC33; g : gid.Shangrlia2Special;
desc : 'Shangrlia 2 Special Disk (PC98)';
level : GP_Resources),

(crc : $BF7D2343; g : gid.Shikomi;
desc : 'Shi Ko Mi (Win)';
level : GP_Resources),

(crc : $3254FB2B; g : gid.ShinjukuMono;
desc : 'Shinjuku Monogatari (PC98)';
level : GP_Resources),

(crc : $AB3EE49E; g : gid.Shizuku98;
desc : 'Shizuku (PC98)';
level : GP_Resources),

(crc : $974541B0; g : gid.Skirmish;
desc : 'Skirmish (PC98)';
level : GP_Resources),

(crc : $649FDCC4; g : gid.Slayers;
desc : 'Slayers (PC98)';
level : GP_Resources),

(crc : $4F0A3EB6; g : gid.SotsugyouShashin;
desc : 'Sotsugyou Shashin (PC98)';
level : GP_Resources),

(crc : $7C754F06; g : gid.TalesNights;
desc : 'Tales Nights (Win)';
level : GP_Resources),

(crc : $A876CC7E; g : gid.Tasogare;
desc : 'Tasogare no Kyoukai (PC98)';
level : GP_Playable),

(crc : $617F3ABA; g : gid.TenshiNingyou;
desc : 'Tenshi Ningyou (Win)';
level : GP_None),

(crc : $C2FECFB7; g : gid.TenshinRanma;
desc : 'Tenshin Ranma (PC98)';
level : GP_Resources),

(crc : $BB13B1F5; g : gid.Tesera;
desc : 'Tesera IV -Kimi wa, Yogoreta Tenshi ka Seinaru Majo ka!?- (PC98)';
level : GP_Resources),

(crc : $AF7B632E; g : gid.TSMako;
desc : 'Time Stripper Mako-chan (PC98)';
level : GP_Resources),

(crc : $89F84285; g : gid.TSMako_e;
desc : 'Time Stripper Mako-chan (Win, en)';
level : GP_Resources),

(crc : $EFCA8EFD; g : gid.ToumeiNingen;
desc : 'Toumei Ningen Arawaru Arawaru (PC98)';
level : GP_Resources),

(crc : $A0E6E952; g : gid.Transfer98;
desc : 'Tenshitachi no Gogo - Tenkousei (PC98)';
level : GP_Completable),

(crc : $0E3CC250; g : gid.TrueLove98;
desc : 'True Love (PC98)';
level : GP_None),

(crc : $AED4390C; g : gid.TrueLove_e;
desc : 'True Love (Win, en)';
level : GP_Resources),

(crc : $075C4151; g : gid.Tsuki_e;
desc : 'Tsuki - Possession (Win, en)';
level : GP_Resources),

(crc : $D4D27C5E; g : gid.TwilightHotel_r;
desc : 'Twilight Hotel - Yonaki no Uta (Win)';
level : GP_Resources),

(crc : $940A436A; g : gid.TwilightHotel_w;
desc : 'Twilight Hotel (Win)';
level : GP_Resources),

(crc : $38359DEE; g : gid.Ukiuki;
desc : 'Uki Uki Island (PC98)';
level : GP_Resources),

(crc : $3774BDA2; g : gid.UrotsukiDouji;
desc : 'Urotsuki Douji (PC98)';
level : GP_Resources),

(crc : $3106F072; g : gid.ValKaizer;
desc : 'Val-Kaizer (PC98)';
level : GP_Resources),

(crc : $CB59CF9A; g : gid.Vanish;
desc : 'Vanishing Point (PC98)';
level : GP_Resources),

(crc : $C778B59A; g : gid.Venus;
desc : 'Venus (PC98)';
level : GP_None),

(crc : $8C827627; g : gid.ViperV6;
desc : 'Viper V6 (PC98)';
level : GP_Resources),

(crc : $7274E97B; g : gid.ViperV6Turbo;
desc : 'Viper V6 Turbo (PC98)';
level : GP_Resources),

(crc : $19903D21; g : gid.ViperV6RS;
desc : 'Viper V6 RS (PC98)';
level : GP_Resources),

(crc : $A33C9D96; g : gid.ViperV16;
desc : 'Viper V16 (PC98)';
level : GP_Resources),

(crc : $F4383E00; g : gid.ViperV16_w;
desc : 'Viper V16 (Win)';
level : GP_Resources),

(crc : $770D1B60; g : gid.Virgin2;
desc : 'Virgin 2 (PC98)';
level : GP_None),

(crc : $4A14CAB8; g : gid.VRoster;
desc : 'Virgin Roster - Shukketsubo (Win)';
level : GP_Resources),

(crc : $594D31BB; g : gid.WakuWakuMP;
desc : 'Waku Waku Mahjong Panic! (PC98)';
level : GP_None),

(crc : $FB3FD71D; g : gid.WakuWakuMP2;
desc : 'Waku Waku Mahjong Panic! 2 (PC98)';
level : GP_Resources),

(crc : $63E4A250; g : gid.WatashioGolf;
desc : 'Watashi o Golf ni Tsuretette (PC98)';
level : GP_Resources),

(crc : $33D323C6; g : gid.WordsWorth;
desc : 'Words Worth (PC98)';
level : GP_Resources),

(crc : $46FAFA10; g : gid.WordsWorthSpecial;
desc : 'Words Worth Special Disk (PC98)';
level : GP_Resources),

(crc : $B34B864E; g : gid.YokohamaElegy;
desc : 'Yokohama Elegy (PC98)';
level : GP_Resources),

(crc : $55BF593B; g : gid.XChange1_e;
desc : 'X-Change (Win, en)';
level : GP_Resources),

(crc : $22EE81E1; g : gid.XChange2_e;
desc : 'X-Change 2 (Win, en)';
level : GP_Resources),

(crc : $39402409; g : gid.XChangeAlt_e;
desc : 'Yin Yang! X-Change Alternative (Win, en)';
level : GP_Resources),

(crc : $C0CBDEEC; g : gid.Xenon98;
desc : 'Xenon - Mugen no Shitai (PC98)';
level : GP_Playable),

(crc : $12C9C902; g : gid.Xenon_w;
desc : 'Xenon - Mugen no Shitai (Win)';
level : GP_None),

(crc : $61B1230A; g : gid.XGirl;
desc : 'X-Girl (PC98)';
level : GP_Resources),

(crc : $C22A2B83; g : gid.XIX;
desc : 'Gize! XIX (PC98)';
level : GP_Resources),

(crc : $43EDC281; g : gid.Xna;
desc : 'X-na (PC98)';
level : GP_Resources),

(crc : $369B0760; g : gid.YES;
desc : 'Youthful Eager Stories (PC98)';
level : GP_Resources),

(crc : $8268D344; g : gid.YESHG;
desc : 'Youthful Eager Stories HG (PC98)';
level : GP_Resources),

(crc : $2FFB9BBA; g : gid.YoujuuClub1_w;
desc : 'Youjuu Club (Win)';
level : GP_None),

(crc : $6528E07E; g : gid.NinpouChou;
desc : 'Youki Toubatsu Ninpou Chou (Win)';
level : GP_Resources),

(crc : $02CC490F; g : gid.Yuugiri;
desc : 'Yuugiri - Ningiyushi no Isan (PC98)';
level : GP_Resources),

(crc : $C6FFA3AB; g : gid.Zatsuon_w;
desc : 'Zatsuon Ryouiki (Win)';
level : GP_Resources),

(crc : $A794DE5B; g : gid.Zenith;
desc : 'Zenith (PC98)';
level : GP_Resources),

(crc : $CB272975; g : gid.ZestFan98;
desc : 'Zest to Fantasy (PC98)';
level : GP_Completable),

(crc : $40089CB1; g : gid.ZestFan_w;
desc : 'Zest to Fantasy (Win)';
level : GP_Completable),

(crc : $6489FD52; g : gid.ZokuYoujuu_r;
desc : 'Zoku Youjuu Senki -Shinsetsu Suna no Mokushiroku-';
level : GP_Resources)

);

type TGameAlt = record
		crc : dword;
		g : gid;
	end;
const GameAltTable : array of TGameAlt = (
	(crc : $37969263; g : gid.AmeKise), // ameirot.exe
	(crc : $2C19D627; g : gid.AngelCrisis), // angt.exe
	(crc : $998BE287; g : gid.BishoujoZukan), // Daizukan '91, looks exactly the same, just a new exe
	(crc : $049912E2; g : gid.Chain_e), // chainust.exe
	(crc : $1FF13080; g : gid.DiviDead_e),
	(crc : $A4333DBA; g : gid.DiviDead_e), // English ISO
	(crc : $E7357779; g : gid.DK1), // Retronomicon, Baba-patched
	(crc : $C2147322; g : gid.EtsuGaku_w), // orig
	(crc : $7609002D; g : gid.Fortuna), // orig
	(crc : $5C5CEC2F; g : gid.Fortuna), // fortdvdt.exe
	(crc : $CF5950B5; g : gid.GoJikanme), // evenus.exe
	(crc : $6BD915D8; g : gid.Hohoemi), // alt crc reported on 31 March 2021, different version of game?
	(crc : $C9BB8DA3; g : gid.HornyBun1_e), // bunnyust.exe
	(crc : $6B9CB04F; g : gid.HornyBun2_e), // bny2ust.exe
	(crc : $036C9176; g : gid.HornyBun2_e), // _crk
	(crc : $4AB2A8DE; g : gid.JastMemo), // setup.exe, on disc
	(crc : $335F2DE6; g : gid.Kadowakashi), // kadot.exe
	(crc : $60E941A0; g : gid.KinKetsu_e),
	(crc : $EB57A4CF; g : gid.Kotobuki_e),
	(crc : $2B9CAED8; g : gid.LetsPirates), // lp2_95t.exe
	(crc : $94A7EEFE; g : gid.Lime1), // Baba-patched
	(crc : $93B1B8B4; g : gid.NinpouChou), // youki95t.exe
	(crc : $E16A0F8D; g : gid.NinpouChou), // youki31.exe
	(crc : $1F7FB546; g : gid.NinpouChou),
	(crc : $6E33FCBD; g : gid.Nocturn_r), // exc.exe
	(crc : $6E1A44CD; g : gid.Nocturn_r), // mgr.exe
	(crc : $372AD2D0; g : gid.ReijouClub), // reiyout.exe
	(crc : $EE9A37D3; g : gid.Rouraku), // rourakut.exe
	(crc : $8D4E0347; g : gid.Shikomi), // shikovt.exe
	(crc : $B69E0805; g : gid.TalesNights), // ayuanx
	(crc : $160ECF0B; g : gid.Tsuki_e), // hyouiust.exe
	(crc : $14B14389; g : gid.TwilightHotel_r), // twhotel.exe
	(crc : $EE92C9B6; g : gid.TwilightHotel_r), // hotel_tz.exe
	(crc : $A15B289C; g : gid.TwilightHotel_r), // hotelt.exe
	(crc : $34A723C1; g : gid.TwilightHotel_r), // _crk
	(crc : $1E5FE109; g : gid.VRoster), // bleedust.exe
	(crc : $A7EC4E48; g : gid.VRoster), // _crk
	(crc : $FF5D11EF; g : gid.YoujuuClub1_w), // yjclubt.exe
	(crc : $1B69988C; g : gid.ZokuYoujuu_r) // zyjwrt.exe
);

function ChibiCRC(const filepath : UTF8string) : dword;
function ShortName(const game : gid) : string; inline;
function MatchGid(const txt : UTF8string) : PGameRecord;
function MatchCRC(crc : dword) : PGameRecord;
procedure PrintGameList(filter : TStringBunch = NIL; showlevels : byte = 0);

implementation

uses mcfileio;

function ChibiCRC(const filepath : UTF8string) : dword;
// Calculates a Chibi-CRC for the given file. Throws an exception in case of errors.
var data : array[0..255] of byte;
	i, ofs, crcfilesize : dword;
	crcfile : file;
	txt : TStringBunch;
begin
	// Hack: some Fairytale games have a tiny com that offloads to advbios.exe etc.
	if (filepath.Length >= 12) and (lowercase(copy(filepath, filepath.Length - 11)) = 'advshell.com') then begin
		txt := FindFiles_Caseless(PathCombine([ExpandFileName(ExtractFilePath(filepath)), 'adv98v', 'advbios.exe']), FALSE, TRUE);
		if txt <> NIL then begin
			result := ChibiCRC(txt[0]);
			exit;
		end;
	end;

	data[0] := 0; // silence compiler
	result := 0; i := 0;
	assign(crcfile, filepath);
	filemode := 0; reset(crcfile, 1); // read-only
	i := IOresult;
	if i <> 0 then raise Exception.Create(errortxt(i) + ' opening ' + filepath);

	try
		crcfilesize := filesize(crcfile);
		result := $ABBACACA + crcfilesize;
		if crcfilesize > $FFF then begin
			ofs := $100;
			while ofs + 4 < crcfilesize do begin
				seek(crcfile, ofs);
				blockread(crcfile, i, 4);
				result := RorDWord(result xor LEtoN(i), 3);
				inc(ofs, ofs shr 2);
			end;
		end
		else begin
			i := crcfilesize shr 8;
			while i <> 0 do begin
				blockread(crcfile, data[0], 256);
				ofs := 256;
				while ofs <> 0 do begin
					dec(ofs, 4);
					result := RorDWord(result xor LEtoN(dword((@data[ofs])^)), 3);
				end;
				dec(i);
			end;
			i := crcfilesize and $FF;
			if i <> 0 then begin
				blockread(crcfile, data[0], i);
				for ofs := i - 1 downto 0 do
					result := RorDWord(result xor data[ofs], 3);
			end;
		end;
		i := IOresult;
		if i <> 0 then raise Exception.Create(errortxt(i) + ' reading ' + filepath);

		// Hacks for small identical com/exe files.
		case result of
			$7B13B1FF: result := ChibiCRC(ExtractFilePath(filepath) + 'adv98v.ini'); // Fairytale games, tiny .com
			$AF7B6372: if pos('2', ExtractFileNameWithoutExt(filepath)) > 0 then inc(result); // Hana no Kioku 1/2
			$B1551C63: if FindFiles_caseless(ExtractFilePath(filepath) + 'm202.szh') <> NIL then inc(result); // Exciting Milk 1/2
			$D3366ED7: result := ChibiCRC(ExtractFilePath(filepath) + 'start.mes'); // Elf's Pinky Ponkies
			$E11D1175: if FindFiles_caseless(ExtractFilePath(filepath) + 'ai5sp.exe') <> NIL then result := $D108AA55; // Doukyuusei 2 Special
			$F0D0D719: if FindFiles_caseless(PathCombine([ExtractFilePath(filepath), 'a', 'quiz.mes'])) <> NIL then inc(result); // Dragon Knight 4 Special
		end;

	finally
		close(crcfile);
		while IOresult <> 0 do; // flush
	end;
end;

function ShortName(const game : gid) : string; inline;
begin
	result := strenum(typeinfo(game), @game)
end;

function MatchGid(const txt : UTF8string) : PGameRecord;
// Returns the game record that matches the given string. The string must be in lowercase before calling.
// Returns NIL if nothing matched.
var i : dword;
begin
	for i := high(GameRecordTable) downto 0 do begin // for-in creates a copy, can't return pointer to counter var
		result := @GameRecordTable[i];
		if txt = lowercase(ShortName(result^.g)) then exit;
	end;
	result := NIL;
end;

function MatchCRC(crc : dword) : PGameRecord;
// Returns the game record that matches the given CRC, or NIL if nothing matched.
var i, j : dword;
	game : gid;
begin
	for i := high(GameRecordTable) downto 0 do
		if GameRecordTable[i].CRC = crc then
			exit(@GameRecordTable[i]);
	for i := high(GameAltTable) downto 0 do
		if GameAltTable[i].CRC = crc then begin
			game := GameAltTable[i].g;
			for j := high(GameRecordTable) downto 0 do
				if GameRecordTable[j].g = game then
					exit(@GameRecordTable[j]);
		end;
	result := NIL;
end;

procedure PrintGameList(filter : TStringBunch = NIL; showlevels : byte = 0);
var totals : array[1..4] of dword;
	i, j : longint;
	match : boolean;
	name : string;
begin
	if showlevels = 0 then showlevels := $FF else showlevels := showlevels shl 1;
	for i := 1 to 4 do totals[i] := 0;
	if filter.Length <> 0 then for i := high(filter) downto 0 do filter[i] := '*' + filter[i] + '*';
	writeln;
	writeln('Supported games:');
	for i := 1 to high(GameRecordTable) do with GameRecordTable[i] do begin
		if ((1 shl byte(level)) and showlevels) = 0 then continue;

		name := ShortName(g);
		if filter.Length <> 0 then begin
			match := FALSE;
			for j := high(filter) downto 0 do
				if (WildcardMatch(filter[j], name)) or (WildcardMatch(filter[j], desc)) then begin
					match := TRUE;
					break;
				end;
			if NOT match then continue;
		end;
		case level of
			GP_None: continue; //write('      ');
			GP_Resources: write('[*]'#9);
			GP_Playable: write('[**]'#9);
			GP_Completable: write('[***]'#9);
			GP_Excellent: write('[###]'#9);
			else write('[?]'#9);
		end;
		writeln('[', name, ']'#9, desc);
		if byte(level) in [1..4] then inc(totals[byte(level)]);
	end;
	if (totals[1] or totals[2] or totals[3] or totals[4]) = 0 then
		writeln('No shortnames or descriptions match the filter.')
	else begin
		writeln;
		writeln(strcat('[*] % resources only  [**] % playable  [***] % completable  [###] % excellent',
			[totals[1], totals[2], totals[3], totals[4]]));
	end;
	writeln;
end;

initialization
finalization
end.

