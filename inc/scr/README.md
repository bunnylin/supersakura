### Script converters

For script format documentation, see [/doc/scr](../../doc/scr).

- ange-adv: Ange ADVIZ `ADV`
- applepie: Apple Pie `BIN`
- csware-ab: C's Ware, Aaru `AB`
- csware-cc: C's Ware `CC`, `CMD`
- cuisse-sin: Cuisse `SIN`
- elf-mes: Elf AI1-AI5 `MES`
- fairydust-scp: Fairy Dust `SCP`
- fairydust-scr: Fairy Dust `SCR`
- great-sd: Great `SD`
- isf: Ikura GDL engine `ISF`
- jast-ovl: JAST/Tiare `OVL`
- leaf-dat: Leaf `DAT`
- panda-mdr: Panda House/Melody `MDR`, `CMF`
- parsley-txb: Parsley `TXB`
- sys98-s: Excellents, Apricot, Desire System98 `S`
