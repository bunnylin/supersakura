{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_CsWareAB(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Himeya/C'sWare/Aaru bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var buffy : pointer = NIL;
	scriptname : UTF8string;
	_loader : TFileLoader = NIL;

{$ifdef enable_decomp_hacks}
procedure _ApplyHacks(const src : TFileLoader);
begin
	case game of
		gid.RoseBlood98:
		case scriptname of
			// Hack: Fix broken string.
			'ROSE_05': if src.ReadDwordFrom($352) = BEtoN(dword($83560087)) then byte(src.PtrAt($354)^) := $83;
			// Hack: Fix broken jump.
			'ROSE_06': if src.ReadDwordFrom($BD) = LEtoN(dword($D3)) then byte(src.PtrAt($BE)^) := 1;
			// Hack: Fix broken jump and choice.
			'ROSE_10B':
			if src.ReadDwordFrom($2C9) = LEtoN(dword($E2)) then dword(src.PtrAt($2C9)^) := BEtoN(dword($E2020100));
		end;
	end;
end;
{$endif}

procedure _TranslateBytecode(const src : TFileLoader);
var int1, int2, int3, int4 : longint;
	cmd, choiceofs : word;
	txt : UTF8string;
	choices : array of record
		txt : UTF8string;
		addr : dword;
	end;
	is16bit : boolean; // jump addresses are 16-bit on PC98, 32-bit on Windows
	night : boolean = FALSE;
	viewframe : boolean = TRUE;

	procedure _DoTextOutput;
	var s : UTF8string;
		l : longint;
		w : word;
		face : boolean = FALSE;
	begin
		if LEtoN(word(src.readp^)) < $20 then exit; // not actually text, some other command
		while BEtoN(word(src.readp^)) = $8140 do inc(src.readp, 2); // skip whitespace

		with ScriptWriter do begin
			// Handle Zest to Fantasy \G portrait picture.
			if BEtoN(word(src.readp^)) = $5C47 then begin
				inc(src.readp, 2);
				l := IndexChar(src.readp^, 10, '.');
				if l >= 0 then begin
					if viewframe then begin
						s := '';
						setlength(s, l);
						if l <> 0 then begin
							move(src.readp^, s[1], l);
							WriteBuf('show "' + s + '" name:face' + LineEnding);
							face := TRUE;
						end;
					end;
					inc(src.readp, l + 1);
				end;
			end;

			WriteBuf('"');
			// Handle Rose Blood dialogue title: [Person]\N<corner quote>...<corner quote>
			if BEtoN(word(src.readp^)) = $8179 then begin // open square bracket
				inc(src.readp, 2);
				while (src.readp < src.endp) and (byte(src.readp^) <> 0) do begin
					w := BEtoN(word(src.readp^));
					inc(src.readp, 2);
					if w = $817A then break;
					WriteBuf(GetUTF8(w));
				end;
				WriteBuf('\:');
				if BEtoN(word(src.readp^)) = $5C4E then inc(src.readp, 2); // skip "\N" after dialogue title
				if BEtoN(word(src.readp^)) = $8175 then inc(src.readp, 2); // skip open corner quote
			end;

			s := src.ReadString;
			// Skip end corner quote in most games.
			if (game <> gid.ZestFan98) and (game <> gid.ZestFan_w)
			and (s.Length > 2) and (byte(s[s.Length - 1]) = $81) and (byte(s[s.Length]) = $76) then
				setlength(s, s.Length - 2);
			// Skip end dialogue portrait clear.
			if (s.Length > 4) and (char(s[s.Length - 1]) = 'G') and (char(s[s.Length]) = '.') then
				setlength(s, s.Length - 3);
			// Convert yen-N to \n.
			s := GetUTF8(s).Replace(#$C2#$A5'N', '\n');

			// Handle M Hard dialogue title: Person\N<corner quote>...<corner quote>
			case game of
				gid.MHard98, gid.MHard_w, gid.ZestFan98, gid.ZestFan_w:
				begin
					l := pos('\n', s);
					if (l > 0) and (dword(l + 6) < s.Length) then
						if LEtoN(dword((@s[l + 2])^)) and $FFFFFF = $8C80E3 then begin
							WriteBuf(s, l);
							if (game = gid.ZestFan98) or (game = gid.ZestFan_w) then
								inc(l, 2)
							else
								inc(l, 5);
							s := ':' + copy(s, l);
						end;
				end;
			end;

			WriteBuf(s);
			WriteBuf('"' + LineEnding + '...');
			if viewframe and face then WriteBuf(LineEnding + 'gfx.remove face');
			WriteBufLn('');
		end;
	end;

	procedure _Error(const s : UTF8string);
	begin
		inc(decomp_stats.numErrors);
		decomp_logger('[!] ' + s);
	end;

	function _ReadAddress : dword;
	begin
		if is16bit then
			result := LEtoN(src.ReadWord)
		else
			result := LEtoN(src.ReadDword);
	end;

	procedure _Transition(t : dword);
	begin
		with ScriptWriter do begin
			WriteBuf('(transition ');
			case t of
				0: WriteBuf('f'); // fade
				1: WriteBuf('i'); // blinds
				3: WriteBuf('i'); // interlaced
				4: WriteBuf('w'); // ragged wipe, in-place, not full-view pan
				else WriteBuf('0');
			end;
			WriteBuf(') ');
		end;
	end;

begin
	{$ifdef enable_decomp_hacks} _ApplyHacks(src); {$endif}
	ScriptWriter.Init(src);
	choiceofs := 0;
	choices := NIL;
	is16bit := (game = gid.Gloria98) or (game = gid.RoseBlood98) or (game = gid.MHard98) or (game = gid.ZestFan98);

	while src.readp < src.endp do with ScriptWriter do begin
		while (length(choices) <> 0) and (choices[high(choices)].addr <= src.ofs) do begin
			{$ifdef enable_decomp_hacks}
			// Hack: Drop fullscreen context explicitly.
			if game = gid.ZestFan98 then begin
				if ((scriptname = 'MANISH01') and (src.ofs = $330))
				or ((scriptname = 'NIGHT02') and (src.ofs = $A20))
				then viewframe := TRUE;
			end;
			{$endif}
			WriteBufLn('// ### ' + choices[high(choices)].txt);
			setlength(choices, high(choices));
		end;

		// See doc/aaru.md for documentation on most bytecodes.
		cmd := LEtoN(src.ReadWord);
		case cmd of
			0: _DoTextOutput;

			1:
			begin
				int1 := _ReadAddress; // onchoice jump address
				if is16bit then inc(src.readp, 2); // text length? not always correct
				while BEtoN(word(src.readp^)) = $8140 do inc(src.readp, 2); // skip whitespace
				txt := GetUTF8(src.ReadString);
				WriteBufLn('choice.set "' + txt + '" "' + strhex(int1, 4) + '"');
				AddJump(int1);
				if dword(int1) >= src.ofs then begin
					// Insert the choice text in choices[], sorted so nearest landing ofs is highest on list.
					int3 := length(choices);
					setlength(choices, int3 + 1);
					while (int3 <> 0) and (choices[int3 - 1].addr < dword(int1)) do begin
						choices[int3] := choices[int3 - 1];
						dec(int3);
					end;
					choices[int3].addr := int1;
					choices[int3].txt := txt;
				end;
			end;

			2:
			begin
				int1 := _ReadAddress;
				if dword(int1) > src.fullFileSize then _Error('jump oob @ $' + strhex(src.ofs - 2));
				AddJump(int1);
				WriteBufLn('goto ."' + strhex(int1, 4) + '"');
			end;

			3:
			begin
				int1 := LEtoN(src.ReadWord);
				int2 := LEtoN(src.ReadWord);
				int3 := LEtoN(src.ReadWord);
				WriteBufLn(strcat('//dummy $03 set multiple flags? &-&-&', [int1, int2, int3]));
			end;

			4:
			begin
				int1 := LEtoN(src.ReadWord);
				int2 := src.ReadByte;
				int3 := LEtoN(src.ReadWord);
				WriteBuf('$v' + strhex(int1) + ' ');
				case char(int2) of
					'=': WriteBuf(':');
					'+','-': WriteBuf(char(int2));
					else begin
						_Error('unk set op $' + strhex(int2) + ' @ $' + strhex(src.ofs - 4));
						WriteBuf('?');
					end;
				end;
				WriteBufLn('= ' + strdec(int3));
			end;

			5: WriteBufLn('waitkey');

			6:
			begin
				WriteBufLn('choice.reset');
				AddJump(src.ofs);
				choiceofs := src.ofs;
				setlength(choices, 0);
			end;

			7: WriteBufLn('choice.go');

			$0A: WriteBufLn('goto ."' + strhex(choiceofs, 4) + '" // $0A');

			$0E:
			begin
				int1 := LEtoN(src.ReadWord); // or should it take no arguments at all?
				int2 := LEtoN(src.ReadWord);
				WriteBufLn(strcat('// $0E section marker? &-&', [int1, int2]));
			end;

			$0F:
			begin
				int1 := LEtoN(src.ReadWord);
				int2 := LEtoN(src.ReadWord);
				WriteBufLn(strcat('// $0F section marker? &-&', [int1, int2]));
			end;

			$10:
			begin
				int1 := LEtoN(src.ReadWord);
				int2 := src.ReadByte;
				int3 := LEtoN(src.ReadWord);
				int4 := _ReadAddress;
				if dword(int4) > src.fullFileSize then _Error('condjump oob @ $' + strhex(src.ofs - 2));
				AddJump(int4);
				WriteBuf('if $v' + strhex(int1) + ' ');
				case char(int2) of
					'=': WriteBuf('!=');
					'{': WriteBuf('>');
					'}': WriteBuf('<');
					else begin
						_Error('unk comp op $' + strhex(int2) + ' @ $' + strhex(src.ofs - 6));
						WriteBuf('?');
					end;
				end;
				WriteBufLn(' ' + strdec(int3) + ' then goto ."' + strhex(int4, 4) + '" end');
			end;

			$11: WriteBufLn('sleep ' + strdec(LEtoN(src.ReadWord) * 12));

			$13:
			begin
				txt := GetUTF8(src.ReadString);
				// Hack: Draw night variants of backgrounds if the next command sets the night palette.
				if LEtoN(word(src.readp^)) = $33 then
					if (txt <> 'H_10') and (txt <> 'H_11') then txt := txt + '_N';

				if game = gid.ZestFan98 then begin
					if copy(txt, 1, 5) = 'GAMEN' then begin
						viewframe := (txt <> 'GAMEN_B');
						if viewframe then
							WriteBuf('call main.enableviewframe //')
						else
							WriteBuf('call main.disableviewframe //');
					end;
				end
				else if txt = 'GAMEN' then WriteBuf('//');

				WriteBufLn('(transition) (gfx.clearall) show ' + txt + ' bkg // $13');
			end;

			$14:
			begin
				int1 := LEtoN(src.ReadWord);
				_Transition(int1);
				WriteBufLn('sleep // $14-' + strhex(int1));
			end;

			$15: WriteBufLn('//dummy $15-' + strhex(LEtoN(src.ReadWord)));

			$16:
			begin
				txt := GetUTF8(src.ReadString);
				WriteBufLn('(transition) show ' + txt);
			end;

			$17:
			begin
				int1 := LEtoN(src.ReadWord);
				int2 := LEtoN(src.ReadWord);
				_Transition(int2);
				WriteBufLn('(gfx.clearall) sleep // $17-' + strhex(int1) + '-' + strhex(int2));
			end;

			$18: WriteBufLn('goto ' + GetUTF8(src.ReadString));

			$19: WriteBufLn('sys.restartgame');

			$1E: WriteBufLn('mus.play ' + GetUTF8(src.ReadString) + ' // $1E');

			$20: WriteBufLn('//dummy $20-' + strhex(LEtoN(src.ReadWord)));

			$24: WriteBufLn('transition // $24-' + strhex(LEtoN(src.ReadWord)));

			$26: WriteBufLn('mus.play ' + GetUTF8(src.ReadString));

			$27: WriteBufLn('//dummy $27 true music play?');

			$28: WriteBufLn('mus.stop 500');

			$29: WriteBufLn('mus.stop 5000');

			$2D:
			case game of
				gid.ZestFan98: WriteBufLn('//dummy $2D-' + strhex(src.ReadByte));
				else WriteBufLn('//dummy $2D-' + strhex(LEtoN(src.ReadWord)));
			end;

			$30:
			begin
				int1 := LEtoN(src.ReadWord);
				int2 := LEtoN(src.ReadWord);
				int3 := LEtoN(src.ReadWord);
				case game of
					gid.RoseBlood98, gid.RoseBlood_w, gid.ZestFan_w:
					begin
						int4 := LEtoN(src.ReadWord);
						WriteBuf('(transition f) (gfx.clearall) sleep' + LineEnding);
						WriteBuf('viewport.setparams viewport:1, ratiox=640, ratioy=480'); // fullscreen context
						WriteBufLn(strcat(' // $30 %, %, %, %', [int1, int2, int3, int4]));
					end;
					else begin
						WriteBufLn(strcat('//dummy $30-&-&-&', [int1, int2, int3]));
					end;
				end;
			end;

			$31:
			begin
				if is16bit then
					int1 := src.ReadByte
				else
					int1 := LEtoN(src.ReadWord);
				if (game = gid.ZestFan_w) or (game = gid.ZestFan98) then
					WriteBufLn('call main.end' + strdec(int1) + ' // $31')
				else
					WriteBufLn('//dummy $31 unlock or win? $' + strhex(int1));
			end;

			$32:
			case game of
				gid.RoseBlood98, gid.RoseBlood_w, gid.ZestFan98, gid.ZestFan_w:
				begin
					WriteBufLn('// $32 remove night palette'); night := FALSE;
				end;
				else WriteBufLn('//dummy $32');
			end;

			$33:
			case game of
				gid.RoseBlood98, gid.RoseBlood_w, gid.ZestFan98, gid.ZestFan_w:
				begin
					WriteBufLn('// $33 set night palette'); night := TRUE;
				end;
				else WriteBufLn('//dummy $33');
			end;

			$46:
			begin
				txt := GetUTF8(src.ReadString);
				if game = gid.ZestFan_w then begin
					if copy(txt, 1, 5) = 'GAMEN' then begin
						viewframe := (txt <> 'GAMEN_B');
						if viewframe then
							WriteBuf('call main.enableviewframe //')
						else
							WriteBuf('call main.disableviewframe //');
					end;
				end
				else if txt = 'GAMEN' then WriteBuf('//');
				WriteBufLn('(transition) show ' + txt + ' viewport:1 z:-9 // $46');
			end;

			$47:
			begin
				txt := GetUTF8(src.ReadString);
				// Hack: Draw night variants of backgrounds if a previous command set the night palette.
				if night then begin
					if (txt <> 'H_10') and (txt <> 'H_11') then txt := txt + '_N';
					night := FALSE;
				end;
				WriteBufLn('(transition) (gfx.clearall) show ' + txt + ' bkg // $47');
			end;

			$48: WriteBufLn('(transition) show ' + GetUTF8(src.ReadString));

			$49: WriteBufLn('call main.titleseq // $49');

			else raise DecompException.Create(strcat('unknown $& @ $&', [cmd, src.ofs - 2]));
		end;
	end;
end;

var compressedsize, bufsize : dword;
begin
	result := FALSE;
	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		compressedsize := LEtoN(dword(loader.readp^));
		bufsize := LEtoN(dword((loader.readp + 4)^));
		if (compressedsize + 8 = loader.fullFileSize)
		and (bufsize > compressedsize) and (bufsize < compressedsize shl 2) then begin
			inc(loader.readp, 8);
			Decompress_LZSS(loader.readp, loader.endp, buffy, bufsize, 18);
			_loader := TFileLoader.FromMemory(buffy, bufsize, FALSE);
			if decomp_param.keepInt then SaveFile(copy(outputfile, 1, outputfile.Length - 3) + 'bin', buffy, bufsize);

			_TranslateBytecode(_loader);
		end
		else begin
			_TranslateBytecode(loader);
		end;
	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
		if _loader <> NIL then begin _loader.Destroy; _loader := NIL; end; // ref'd by scriptwriter
	end;

	result := TRUE;
end;

