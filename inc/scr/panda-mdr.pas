{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_PandaMDR(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Panda/Melody MDR bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var scriptname : UTF8string;

	procedure _TranslateBytecode(const src : TFileLoader);
	var com : word;
		txt : UTF8string;
	begin
		with ScriptWriter do while src.readp < src.endp do begin
			com := LEtoN(src.ReadWord);

			case com of
				0: WriteBufLn('return');
				1: WriteBufLn('(transition) show ' + src.ReadString);
				2: WriteBufLn('//dummy $0002-' + strhex(LEtoN(src.ReadWord)));
				3:
				begin
					txt := src.ReadString;
					if txt.Length and 1 <> 0 then inc(src.readp);
					inc(src.readp);
					txt := txt.Replace(#$D, ' ').Replace(#$81#$67, '"').Replace(#$81#$68, '"');
					WriteBufLn('`' + GetUTF8(txt) + '`');
					WriteBufLn('...');
				end;
				4: WriteBufLn('waitkey');
				5: WriteBufLn('mus.play ' + src.ReadString);
				6: WriteBufLn('mus.stop 1000');
				7: WriteBufLn('sleep ' + strdec(LEtoN(src.ReadWord) * 20));
				else raise DecompException.Create('unknown code $' + strhex(com));
			end;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 500 then raise DecompException.Create('file too tiny');

	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		ScriptWriter.Init(loader);
		_TranslateBytecode(loader);
	finally
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
	end;

	result := TRUE;
end;

function Decomp_PandaCMF(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Panda/Melody CMF bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var scriptname : UTF8string;

	procedure _TranslateBytecode(const src : TFileLoader);
	var com : word;
	begin
		with ScriptWriter do while src.readp < src.endp do begin
			com := LEtoN(src.ReadWord);

			case com of
				0: WriteBufLn('return');
				else raise DecompException.Create('unknown code $' + strhex(com));
			end;
		end;
	end;

begin
	raise SkipException.Create('not implemented');
	result := FALSE;
	if loader.fullFileSize < 100 then raise DecompException.Create('file too tiny');

	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		ScriptWriter.Init(loader);
		_TranslateBytecode(loader);
	finally
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
	end;

	result := TRUE;
end;

