{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_ISF(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Ikura ISF bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.

const lut_shortjis : array[0..127] of word = (
$8140, $8140, $8141, $8142, $8145, $8148, $8149, $8169,
$816A, $8175, $8176, $824F, $8250, $8251, $8252, $8253,
$8254, $8255, $8256, $8257, $8258, $82A0, $82A2, $82A4,
$82A6, $82A8, $82A9, $82AA, $82AB, $82AC, $82AD, $82AE,
$8140, $82B0, $82B1, $82B2, $82B3, $82B4, $82B5, $82B6,
$82B7, $82B8, $82B9, $82BA, $82BB, $82BC, $82BD, $82BE,
$82BF, $82C0, $82C1, $82C2, $82C3, $82C4, $82C5, $82C6,
$82C7, $82C8, $82C9, $82CA, $82CB, $82CC, $82CD, $82CE,
$82D0, $82D1, $82D3, $82D4, $82D6, $82D7, $82D9, $82DA,
$82DC, $82DD, $82DE, $82DF, $82E0, $82E1, $82E2, $82E3,
$82E4, $82E5, $82E6, $82E7, $82E8, $82E9, $82EA, $82EB,
$82ED, $82F0, $82F1, $8341, $8343, $8345, $8347, $8349,
$834A, $834C, $834E, $8350, $8352, $8354, $8356, $8358,
$835A, $835C, $835E, $8360, $8362, $8363, $8365, $8367,
$8369, $836A, $82AF, $836C, $836D, $836E, $8371, $8374,
$8377, $837A, $837D, $837E, $8380, $8381, $8382, $8384);

var scriptname : UTF8string;
	i, baseofs : dword;

	procedure _Decrypt;
	// ROR every byte by 2. Use dwords for efficiency.
	var l, x : dword;
	begin
		for l := loader.RemainingBytes shr 2 - 1 downto 0 do begin
			x := dword(loader.readp^);
			dword(loader.readp^) := ((x and $03030303) shl 6) or ((x and $FCFCFCFC) shr 2);
			inc(loader.readp, 4);
		end;
		while loader.readp < loader.endp do begin
			byte(loader.readp^) := RorByte(byte(loader.readp^), 2);
			inc(loader.readp);
		end;
	end;

	procedure _TranslateBytecode(const src : TFileLoader);
	var jumplist : array of dword = NIL;
		txt, valref : UTF8string;
		d : dword;
		comlen : longint;
		com, b : byte;

		procedure _Error(const msg : UTF8string);
		begin
			LogError(msg + ' @ $' + strhex(src.ofs - 1));
		end;

		function _ReadBytes(count : byte) : dword;
		begin
			if count > src.RemainingBytes then begin
				_Error('oob during final com $' + strhex(com));
				count := src.RemainingBytes;
			end;
			dec(comlen, count);
			case count of
				1: result := src.ReadByte;
				2: result := LEtoN(src.ReadWord);
				4: result := LEtoN(src.ReadDword);
				else result := $69696969;
			end;
		end;

		function _ReadJump : dword;
		begin
			result := _ReadBytes(2);
			if result > dword(high(jumplist)) then begin
				_Error(strdec(result) + ' outside jumplist');
				exit(0);
			end;
			result := jumplist[result];
		end;

		function _ReadValRef : UTF8string;
		var v : dword;
			l : longint;
			flag : byte;
		begin
			v := _ReadBytes(4);
			flag := v shr 29;
			l := v and $1FFFFFFF;
			if flag and 5 = 5 then begin
				_Error('valref neg var $v' + strhex(l));
				exit;
			end;
			if flag and 1 <> 0 then dword(l) := dword(l) or $E0000000; // negative value! extend sign
			if flag and 4 <> 0 then
				result := '$v' + strhex(l)
			else
				result := strdec(l);
			if flag and 2 <> 0 then result := 'rnd(' + result + ')';
		end;

		function _ReadStr : UTF8string;
		begin
			result := src.ReadString; // this has built-in range checking so can't go out of bounds
			dec(comlen, result.Length + 1);
			result := GetUTF8(result);
		end;

		function _ExpandStr(const input : UTF8string) : UTF8string;
		// This expands printable shortjis strings into UTF-8.
		var readp, endp, outp : pointer;
			c : string;
		begin
			result := '';
			if input = '' then exit;
			setlength(result, input.Length * 4);
			readp := @input[1];
			endp := readp + input.Length;
			outp := @result[1];

			while readp < endp do begin
				if byte(readp^) < $80 then begin
					c := GetUTF8(lut_shortjis[byte(readp^)]);
					inc(readp);
				end
				else begin
					c := GetUTF8(BEtoN(word(readp^)));
					inc(readp, 2);
				end;
				dword(outp^) := dword((@c[1])^);
				inc(outp, c.Length);
			end;

			// Cut string buffer to exact size.
			setlength(result, outp - @result[1]);
		end;

		procedure _DoText;
		var prefix : UTF8string = '';
			buf : byte;
		begin
			buf := _ReadBytes(1);
			//if buf <> 0 then _Error('non0 text target buf');
			// Unknown initial bytes; text starts after a $FF.
			while comlen > 0 do begin
				buf := _ReadBytes(1);
				if buf = $FF then break;
				prefix := prefix + ' ' + strhex(buf);
			end;

			if comlen = 0 then begin
				ScriptWriter.WriteBufLn('// $' + strhex(com) + prefix + ' no text?');
				exit;
			end;
			txt := src.ReadString;
			dec(comlen, txt.Length + 1);
			txt := '"' + _ExpandStr(txt) + '" //' + prefix;
			prefix := ' /';
			while comlen > 0 do prefix := prefix + ' ' + strhex(_ReadBytes(1));
			ScriptWriter.WriteBufLn(txt + prefix);
		end;

		procedure _Calc;
		var varnum : dword;
			op : byte;
		begin
			varnum := _ReadBytes(2);
			op := _ReadBytes(1);
			with ScriptWriter do begin
				WriteBuf(strcat('$v& := ', [varnum]));
				if op <> 0 then _Error('non0 init op');
				while op < 5 do begin
					WriteBuf(_ReadValRef);
					op := _ReadBytes(1);
					case op of
						0: WriteBuf(' + ');
						1: WriteBuf(' - ');
						2: WriteBuf(' * ');
						3: WriteBuf(' / ');
						4: WriteBuf(' mod ');
					end;
				end;
				WriteBufLn('');
			end;
		end;

		procedure _CondJump;
		begin
			with ScriptWriter do begin
				WriteBuf('if (');
				while TRUE do begin
					txt := _ReadValRef + ' ';
					b := _ReadBytes(1);
					case b of
						0: txt := txt + '==';
						1: txt := txt + '<';
						2: txt := txt + '<=';
						3: txt := txt + '>';
						4: txt := txt + '>=';
						5: txt := txt + '!=';
						else _Error('bad cond op ' + strhex(b));
					end;
					WriteBuf(txt + ' ' + _ReadValRef + ') ');

					b := _ReadBytes(1);
					case b of
						0:
						begin
							WriteBuf('then goto "' + strhex(_ReadJump, 4) + '" end');
							b := _ReadBytes(1);
							if b <> $FF then _Error('non-FF after condjump');
							break;
						end;

						1:
						begin
							d := _ReadBytes(2);
							txt := _ReadValRef;
							WriteBuf('then $v' + strhex(d) + ' := ' + txt + ' end');
							b := _ReadBytes(1);
							if b <> $FF then _Error('non-FF after condset');
							break;
						end;

						2:
						begin
							WriteBuf('and (');
						end;

						else _Error('bad cond res ' + strhex(b));
					end;
				end;
				WriteBufLn('');
			end;
		end;

		procedure _LoadColor;
		var slot : UTF8string;
			b, g, r : byte;
		begin
			slot := _ReadValRef;
			b := _ReadBytes(1) shr 4;
			g := _ReadBytes(1) shr 4;
			r := _ReadBytes(1) shr 4;
			ScriptWriter.WriteBufLn(strcat('$slot% := ."|%%%F"', [slot, hextable[r], hextable[g], hextable[b]]));
		end;

		procedure _Draw;
		var slot, slot2, x, y, w, h, destx, desty : UTF8string;
			style : byte;
		begin
			style := _ReadBytes(1);
			slot := _ReadValRef;
			with ScriptWriter do begin
				x := _ReadValRef;
				y := _ReadValRef;
				w := _ReadValRef;
				h := _ReadValRef;
				slot2 := _ReadValRef;
				destx := _ReadValRef;
				desty := _ReadValRef;

				WriteBufLn(strcat('show $slot% // %,% %x% -> %; %,%', [slot, x, y, w, h, slot2, destx, desty]));

				case style of
					$F..$12:
					begin
						x := _ReadValRef;
						y := _ReadValRef;
						lineList[lineIndex - 1] := lineList[lineIndex - 1] + strcat(' // extra params % %', [x, y]);
					end;
					$15:
					begin
						x := _ReadValRef;
						lineList[lineIndex - 1] := lineList[lineIndex - 1] + strcat(' // extra param %', [x]);
					end;
					$1C, $1D:
					begin
						x := _ReadValRef;
						y := _ReadValRef;
						w := _ReadValRef;
						h := _ReadValRef;
						lineList[lineIndex - 1] := lineList[lineIndex - 1]
							+ strcat(' // extra params % % % %', [x, y, w, h]);
					end;
				end;
			end;
		end;

	begin
		ScriptWriter.Init(src);

		// Read the jump list.
		setlength(jumplist, (baseofs - src.ofs) shr 2);
		if length(jumplist) <> 0 then for d := 0 to high(jumplist) do begin
			i := LEtoN(src.ReadDword);
			jumplist[d] := baseofs + i;

			if jumplist[d] > src.fullFileSize then raise DecompException.Create('jump oob: $' + strhex(i));
			if (d <> 0) and (jumplist[d] < jumplist[d - 1]) then
				raise DecompException.Create('jump not strictly ascending: $' + strhex(i));

			ScriptWriter.AddJump(jumplist[d]);
		end;

		with ScriptWriter do while src.readp + 2 < src.endp do begin
			// Bytecode comes in the form <command> <com + args length> [args].
			com := src.ReadByte;
			comlen := src.ReadByte;
			if comlen < 2 then begin
				_Error('too short comlen');
				comlen := 2;
			end
			// Commands can have very long arguments. If top bit set, length := multiply rest by 256 + the next byte.
			else if comlen >= $80 then comlen := ((comlen and $7F) shl 8) or src.ReadByte - 1;
			dec(comlen, 2);
			if comlen > src.RemainingBytes then comlen := src.RemainingBytes;

			case com of
				0:;

				1, 2:
				begin
					if com = 1 then WriteBufLn('//scr.pop'); // need to keep original return point?..
					WriteBufLn('call "' + _ReadStr + '."');
				end;

				3: WriteBufLn('scr.return');

				4, 5:
				begin
					if com = 4 then txt := 'goto' else txt := 'call';
					WriteBufLn(txt + ' "' + strhex(_ReadJump, 4) + '"');
				end;

				6: WriteBufLn('return');

				7,8:
				begin
					if com = 7 then WriteBuf('casegoto') else WriteBuf('casecall');
					WriteBuf(' ' + _ReadValRef + ' ."');
					b := _ReadBytes(1);
					while b <> 0 do begin
						WriteBuf(strhex(_ReadJump, 4));
						dec(b);
						if b <> 0 then WriteBuf(':');
					end;
					WriteBufLn('"');
				end;

				$29: WriteBufLn('// $29 open window ' + strdec(_ReadBytes(1)));

				$2A: WriteBufLn('// $2A close window ' + strdec(_ReadBytes(1)));

				$2B, $2C: _DoText;

				$31:
				begin
					d := _ReadBytes(2);
					b := _ReadBytes(1);
					case b of
						0: txt := '0';
						1: txt := '1';
						2: txt := '!$f' + strhex(d);
						else _Error('unk $31 flag ' + strhex(b));
					end;
					WriteBufLn('$f' + strhex(d) + ' := ' + txt);
				end;

				$33:
				begin
					WriteBuf('if $f' + strhex(_ReadBytes(2)) + ' then goto "');
					WriteBufLn(strhex(_ReadJump, 4) + '" end');
				end;

				$38:
				begin
					d := _ReadBytes(2);
					WriteBufLn(strcat('//dummy $38 save persistent flags $f[&..&]', [d, _ReadBytes(2)]));
				end;

				$3A:
				begin
					b := _ReadBytes(1);
					WriteBuf('if $_');
					case b of
						1: WriteBuf('audiomuted == !' + strdec(_ReadBytes(1)));
					end;
					WriteBufLn(strcat(' then goto "%" end', [strhex(_ReadJump, 4)]));
				end;

				$44: _Calc;

				$47: _CondJump;

				$53: _LoadColor;

				$55:
				begin
					valref := _ReadValRef;
					WriteBuf(strcat('(gfx.transition time:%) ', [valref]));
					txt := 'show "|000F"';
					txt[8] := hextable[_ReadBytes(1) shr 4];
					txt[9] := hextable[_ReadBytes(1) shr 4];
					txt[10] := hextable[_ReadBytes(1) shr 4];
					WriteBufLn(txt);
				end;

				$56:
				begin
					valref := _ReadValRef;
					WriteBufLn(strcat('$slot% := ."%"', [valref, _ReadStr]));
				end;

				$57: _Draw;

				$70:
				begin
					txt := _ReadStr;
					WriteBufLn(strcat('mus.play % // &', [txt, _ReadBytes(1)]));
				end;

				$72: WriteBufLn('mus.stop ' + _ReadValRef); // fade out

				$73: WriteBufLn('mus.stop // immediate');

				$74:
				begin
					txt := _ReadStr;
					WriteBufLn(strcat('//snd.play % // chn %', [txt, _ReadValRef]));
				end;

				$75:
				begin
					valref := _ReadValRef;
					// If $20 is set, play from start? If $40 is set, don't?
					WriteBufLn('//dummy $75 trigger sound effect, chn ' + valref + ' & 0x1F');
				end;

				$77:
				begin
					txt := _ReadStr;
					WriteBufLn(strcat('//sfx.play "%", %', [txt, _ReadBytes(1)]));
				end;

				$79: WriteBufLn('//sfx.stop');

				$7A: WriteBufLn('//sfx.pause?');

				$F0:
				begin
					b := _ReadBytes(1);
					WriteBufLn(strcat('//dummy $F0-&: %', [b, _ReadStr]));
				end;

				$F1: // begin a sleep effect but don't wait for it; basically, prepare to sleep but not yet
				begin
					valref := _ReadValRef;
					WriteBufLn('$wait := ' + valref);
				end;

				$F2: WriteBufLn('wait $wait');

				$F3:
				begin
					valref := _ReadValRef;
					WriteBuf(strcat('//play video: %,%', [valref, _ReadValRef]));
					valref := _ReadValRef;
					txt := _ReadValRef;
					WriteBufLn(strcat(' %x% %', [valref, txt, _ReadStr]));
				end;

				$F5: WriteBufLn('// $F5 allowsaving:=' + strdec(_ReadBytes(1)));

				else begin
					txt := '';
					if comlen <> 0 then begin
						setlength(txt, comlen * 3);
						d := 1;
						repeat
							b := src.ReadByte;
							txt[d] := hextable[b shr 4]; inc(d);
							txt[d] := hextable[b and $F]; inc(d);
							txt[d] := ' '; inc(d);
							dec(comlen);
						until comlen = 0;
						setlength(txt, length(txt) - 1);
					end;
					WriteBufLn('//dummy $' + strhex(com) + '(' + txt + ')');
				end;
			end;

			if comlen <> 0 then _Error(strcat('com $& argbytes left %', [com, comlen]));
			inc(src.readp, comlen);
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 16 then raise DecompException.Create('file too tiny');
	// The first header dword is the end ofs for the script entries array, must be divisible by 4.
	baseofs := LEtoN(loader.ReadDword);
	if baseofs = $4F44414B then begin // "KADOWAKA" extra sig
		inc(loader.readp, 4);
		baseofs := LEtoN(loader.ReadDword) + 8;
	end;
	if (baseofs and 3 <> 0) or (baseofs < 8) or (baseofs >= loader.fullFileSize) then
		raise DecompException.Create('bad base');

	i := loader.ReadDword;
	if (i <> 0) and (i <> LEtoN(dword($9795))) then raise DecompException.Create('bad isf sig');
	if i <> 0 then begin
		i := loader.ofs;
		_Decrypt;
		loader.ofs := i;
	end;

	// Dump the raw bytecode for analysis.
	if decomp_param.keepInt then SaveFile(outputfile + '.bin', loader.readp - 8, loader.RemainingBytes + 8);

	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		//{$ifdef enable_decomp_hacks} _ApplyHacks(loader); {$endif}
		_TranslateBytecode(loader);
	finally
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
	end;

	result := TRUE;
end;

