{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_AngeADV(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Ange ADV bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var scriptname : UTF8string;

	procedure _TranslateBytecode(const src : TFileLoader);
	var com, b : byte;
		w : word;

		function _DecryptString : UTF8string;
		// Printable strings are in Shift-JIS, simply encrypted. This converts them to UTF8.
		var startp : pointer;
			key, value : byte;
		begin
			startp := src.readp;
			key := com;
			while src.readp < src.endp do begin
				value := byte(src.readp^) xor key;
				byte((src.readp)^) := value;
				inc(src.readp);
				if value = 0 then break;
				{$push}{$R-}inc(key, value);{$pop}
			end;
			result := GetUTF8(startp, src.readp - startp - 1);
		end;

	begin
		with ScriptWriter do while src.readp < src.endp do begin
			com := src.ReadByte;
			if (com = $30) and (game = gid.Like) or (com = $58) and (game <> gid.Like) then begin
				WriteBufLn('"' + _DecryptString + '"');
				continue;
			end;

			case com of
				$1C: WriteBufLn('//dummy $1C - push execution ofs on stack so next jump is returnable?');
				$1E: WriteBufLn('return // $1E');
				$80, $81, $A0, $C0, $C1, $E0: WriteBufLn(strcat('//dummy $&-%', [com, strhex(LEtoN(src.ReadWord), 4)]));
				$82, $C2, $C4: WriteBufLn(strcat('//dummy $&-&', [com, src.ReadByte]));
				$84:
				begin
					b := src.ReadByte;
					if b <> $F then
						WriteBufLn('//dummy $84-' + strhex(b))
					else begin
						w := LEtoN(src.ReadWord);
						AddJump(w);
						WriteBufLn('//dummy $84-0F > ' + strhex(w, 4));
					end;
				end;
				$88:
				begin
					w := LEtoN(src.ReadWord);
					AddJump(w);
					WriteBufLn('//dummy $88 > ' + strhex(w, 4));
				end;
				$90, $D0: WriteBufLn(strcat('//dummy $& on var $&', [com, LEtoN(src.ReadWord)]));
				else WriteBufLn('//dummy $' + strhex(com));
			end;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 52 then raise DecompException.Create('file too tiny');

	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		ScriptWriter.Init(loader);
		_TranslateBytecode(loader);
	finally
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
	end;

	result := TRUE;
end;

