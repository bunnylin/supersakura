{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_MES(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Elf MES bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var buffy : pointer = NIL;
	scriptname : UTF8string;

procedure _TranslateBytecode(const src : TFileLoader);
var stack : array[0..47] of dword;
	command, cmd, stackofs : byte;

	procedure _DoTextOutput;
	var printstr : UTF8string = '';
		printofs : dword = 2;
		u8c : string[4];
		quoted : boolean;
		j : byte;
	begin
		if command <> 0 then ScriptWriter.WriteBuf(' ');
		setlength(printstr, 256);
		printstr[1] := '"';

		quoted := cmd = ord('"');
		if NOT quoted then dec(src.readp);

		with ScriptWriter do while src.readp < src.endp do begin
			if printofs + 4 >= printstr.Length then setlength(printstr, printstr.Length * 2);

			// Hacky newline in the English patch. We have soft linebreaks so translate to a space.
			if dword(src.readp^) = BEtoN(dword($22819322)) then begin
				printstr[printofs] := ' '; inc(printofs);
				inc(src.readp, 4);
				continue;
			end;

			j := src.ReadByte;
			case j of
				// Single-byte ASCII.
				$20..$7A:
				begin
					byte(printstr[printofs]) := j;
					inc(printofs);
					if (quoted) and (j = ord('"')) then break;
				end;

				// Double-byte JIS.
				$81..$9F, $E0..$EF:
				begin
					u8c := GetUTF8(j shl 8 + src.ReadByte);
					move(u8c[1], printstr[printofs], length(u8c));
					inc(printofs, length(u8c));
				end;

				// Some other command, assume implicit string termination.
				else begin
					dec(src.readp);
					break;
				end;
			end;
		end;

		if printofs <= 2 then exit;
		dec(printofs);
		if printstr[printofs] <> '"' then begin
			inc(printofs);
			printstr[printofs] := '"';
		end;
		setlength(printstr, printofs);
		ScriptWriter.WriteBuf(printstr);
		if command = 0 then ScriptWriter.WriteBufLn('');
	end;

	procedure _OutputCmd;
	var i : byte;
	begin
		with ScriptWriter do begin
			if stackofs <> 0 then for i := 0 to stackofs - 1 do begin
				WriteBuf(' ');
				if stack[i] and $10000 <> 0 then
					WriteBuf('[reg ' + strdec(stack[i]) + ']')
				else if stack[i] and $40000 <> 0 then
					WriteBuf('$' + char(stack[i] and $FF))
				else if stack[i] and $80000 <> 0 then
					WriteBuf(chr(stack[i] and $FF))
				else
					WriteBuf(strdec(stack[i]));
			end;
			WriteBufLn('');
		end;
		command := 0;
		stackofs := 0;
	end;

begin
	ScriptWriter.Init(src);
	command := 0;
	stackofs := 0;

	with ScriptWriter do while src.readp < src.endp do begin
		// See doc/elf.md for notes on MES files.
		cmd := src.ReadByte;
		if (command <> 0) and (cmd in [ord('"'), $7B..$EF]) then begin
			if (cmd <> ord('"')) or (NOT (command in [$A2, $A3, $AD, $AE, $B7, $B9])) then _OutputCmd;
		end;

		case cmd of
			// Single-byte reg or imm.
			0, $10:
			begin
				stack[stackofs] := ((cmd xor $10) shl 15) or src.ReadByte;
				inc(stackofs);
			end;

			// Direct reg or imm.
			1..7, $11..$17:
			begin
				stack[stackofs] := (cmd and $F) or (((cmd and $10) xor $10) shl 15);
				inc(stackofs);
			end;

			// Double-byte reg or imm.
			8, $18:
			begin
				stack[stackofs] := ((cmd xor $18) shl 15) or BEtoN(src.ReadWord);
				inc(stackofs);
			end;

			// Variable refs.
			$40..$5A:
			begin
				stack[stackofs] := cmd or $40000;
				inc(stackofs);
			end;

			// Remaining low bytes except " and letters.
			$21, $23..$3F, $5B..$5F:
			begin
				stack[stackofs] := cmd or $80000;
				inc(stackofs);
			end;

			$7B, $7D: WriteBufLn(chr(cmd));

			ord('"'), $81..$98, $E0..$EF: _DoTextOutput;

			$99..$DF:
			begin
				case cmd of
					$9D: WriteBuf('if');
					$A2: WriteBuf('goto');
					$A3: WriteBuf('call');
					$A9: WriteBuf('."\C"');
					$B6: WriteBuf('mus.play');
					else WriteBuf('//dummy $' + strhex(cmd));
				end;
				command := cmd;
			end;

			else WriteBufLn('//unknown $' + strhex(cmd));
		end;
	end;
end;

begin
	result := FALSE;
	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		_TranslateBytecode(loader);
	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
	end;

	result := TRUE;
end;

