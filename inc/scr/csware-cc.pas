{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_CsWareCC(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated C's Ware bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var buffy : pointer = NIL;
	bufsize : dword;
	scriptname : UTF8string;
	_loader : TFileLoader = NIL;

{$ifdef enable_decomp_hacks}
procedure _ApplyHacks(const src : TFileLoader);
begin
	case game of
		gid.EveBE98:
		case scriptname of
			// Hack: Dummy out an invalid save-load subchoice
			'B029': fillbyte(src.PtrAt($3647)^, 45, $0B);
			// Hack: Dummy out an invalid save-load subchoice
			'B030': fillbyte(src.PtrAt($95C2)^, 45, $0B);
		end;
	end;
end;
{$endif}

procedure _TranslateBytecode(const src : TFileLoader);
type TOptNode = record
	showtxt : UTF8string;
	labeltxt : string[15];
	id : word;
	children : array of TOptNode;
	parent : ^TOptNode;
end;
var optionroot : TOptNode;
	optionnode : ^TOptNode = NIL;
	optiondefineline, optionenableline : dword;
	gfxlist : TStringBunch = NIL;
	currentsection : integer;
	overlaytxt : record x, y : word; end;
	thennesting : byte = 0;
	txtcolor : byte = $F;
	palette : array[0..$F] of RGBAquad;
	cmd : byte;
	ifstatement, singlethen, optionsetup : boolean;
const VNUMBER : byte = $FB;
	VVAR : byte = $FE;
	VOPTION : byte = $FF;
	saveloadstr : array of UTF8string = (
		'window_off',
		#$E3#$82#$BB + #$E3#$83#$BC + #$E3#$83#$96 + #$E3#$83#$BB, // saveload
		#$E8#$A8#$98 + #$E9#$8C#$B2, // record
		#$E3#$82#$BB + #$E3#$83#$BC + #$E3#$83#$96 + #$E3#$81#$99 + #$E3#$82#$8B // セーブする (xenon save)
		);
	xenosav = #$E3#$82#$B2 + #$E3#$83#$BC + #$E3#$83#$A0 + #$E3#$82#$92 + #$E7#$B6#$9A; // ゲームを続 (xenon, no save)

	procedure _AddNiceIndent;
	begin
		with ScriptWriter do
			if (thennesting <> 0) and (currentLen = 0) then
				WriteBuf(StringOfChar(#9, thennesting));
	end;

	function _ExpectString : UTF8string;
	var len : byte;
	begin
		if src.RemainingBytes < 3 then raise DecompException.Create('string past script end');
		len := src.ReadByte;
		if len <> $FD then raise DecompException.Create(strcat('got $& instead of FD @ $&', [len, src.ofs - 1]));
		len := src.ReadByte;
		result := src.ReadString;
	end;

	function _ExpectValue(valuetype : byte) : integer;
	begin
		if src.RemainingBytes < 3 then raise DecompException.Create('value past script end');
		result := src.ReadByte;
		if result <> valuetype then
			raise DecompException.Create(strcat('got $& instead of & @ $&', [result, valuetype, src.ofs - 1]));
		result := LEtoN(src.ReadWord);
	end;

	procedure _PrintOptionDefines(const node : TOptNode);
	var child : TOptNode;
		l : byte;
	begin
		if length(node.children) <> 0 then
			for child in node.children do _PrintOptionDefines(child)
		else if node.parent <> NIL then with ScriptWriter do begin
			{$ifdef enable_decomp_hacks}
			// Hack: immediately go to Xenon "Don't Save" option.
			if node.showtxt.StartsWith(xenosav) then begin
				lineList[optiondefineline] := lineList[optiondefineline] + 'goto ' + node.labeltxt + ' //';
			end else
			// Hack: comment out Save-Load options and box hiding.
			for l := high(saveloadstr) downto 0 do
				if node.showtxt.StartsWith(saveloadstr[l]) then begin
					lineList[optiondefineline] := lineList[optiondefineline] + '//';
					break;
				end;
			{$endif}
			lineList[optiondefineline] := lineList[optiondefineline] + 'choice.set "' + node.showtxt + '" ' + node.labeltxt + #$A;
		end;

		// When done with root node, and there was at least one option defined, cut out the last newline.
		if (node.parent = NIL) and (length(node.children) <> 0) then with ScriptWriter do
			setlength(lineList[optiondefineline], lineList[optiondefineline].Length - 1);
	end;

	procedure _NewOption(const optiontxt : UTF8string);
	begin
		setlength(optionnode^.children, length(optionnode^.children) + 1);
		with optionnode^.children[high(optionnode^.children)] do begin
			parent := optionnode;
			id := _ExpectValue(VOPTION);

			if optionnode^.showtxt = '' then
				showtxt := optiontxt
			else
				showtxt := optionnode^.showtxt + ':' + optiontxt;
			labeltxt := optionnode^.labeltxt + '-' + strhex(id);

			// Convert to an option enable/disable command if converting an optionlist and in a singlethen statement.
			// The current line's conditional statement can be moved to be the choice.on value.
			if singlethen then with ScriptWriter do begin
				currentLine[currentLen] := #0;
				lineList[optionenableline] := lineList[optionenableline]
					+ 'choice.on "' + showtxt + '" ' + PChar(currentLine) + ')'#$A;
				currentLen := 0;
				_AddNiceIndent;
				singlethen := FALSE;
			end;
		end;
	end;

	procedure _EndOutcome;
	begin
		if optionnode^.parent <> NIL then optionnode := optionnode^.parent;
		//raise DecompException.Create('end outcome without choice @ $' + strhex(src.ofs - 1));
		// With multiple 08 choice pops in a row, or after 06, no point printing goto more than once.
		if (NOT (byte((src.readp - 2)^) in [6,8])) and (length(optionroot.children) <> 0) then
			ScriptWriter.WriteBuf('goto choice' + strhex(currentsection) + ' ');
		ScriptWriter.WriteBufLn('// 08 end outcome block');
	end;

	procedure _NewOutcome(outcomeid : longint);
	var i, l : dword;
	begin
		if length(optionnode^.children) <> 0 then for i := high(optionnode^.children) downto 0 do
			with optionnode^.children[i] do
				if id = outcomeid then with ScriptWriter do begin
					optionnode := @optionnode^.children[i];
					WriteBufLn('');
					{$ifdef enable_decomp_hacks}
					// Hack: comment out Save-Load outcomes
					for l := high(saveloadstr) downto 0 do
						if showtxt.StartsWith(saveloadstr[l]) then begin
							WriteBuf('//');
							break;
						end;
					{$endif}
					WriteBufLn(strcat('@%: // % @ &', [labeltxt, showtxt, src.ofs - 1]));
					exit;
				end;

		// No match? Sometimes an outcome level end is omitted, try to go back a level...
		// If already on parent level, this is an inaccessible outcome, should be ignored.
		if optionnode^.parent = NIL then begin
			if decomp_param.verbose then
				decomp_logger('[!] ' + strcat(
					'undeclared option %-& @ $&', [optionnode^.labeltxt, outcomeid, src.ofs - 1]));
			// Generate a fake option for this that's always hidden.
			ScriptWriter.WriteBuf('(1==0');
			singlethen := TRUE;
			dec(src.readp, 3);
			_NewOption('Fake' + strhex(outcomeid));
			_NewOutcome(outcomeid);
		end
		else begin
			optionnode := optionnode^.parent;
			ScriptWriter.WriteBufLn('// implicit end outcome block');
			_NewOutcome(outcomeid);
		end;
	end;

	procedure _AddGfx;
	var imgname, s : UTF8string;
	begin
		imgname := _ExpectString;
		ScriptWriter.WriteBufLn('// load ' + imgname + ' //$' + strhex(cmd));
		// Clear displayables if it's a regular load command, not animated.
		if (cmd = $C) and (byte(src.readp^) <> $2E) then setlength(gfxlist, 0);

		{$ifdef enable_decomp_hacks}
		if (imgname.Length > 3) and (copy(imgname, 1, 4) = 'waku') then exit;
		{$endif}

		case cmd of
			$32: imgname := '"' + imgname + 'A" anim';
			$33: imgname := '"' + imgname + '" bkg';
			else imgname := '"' + imgname + '"';
		end;

		for s in gfxlist do if s = imgname then exit;
		setlength(gfxlist, gfxlist.Length + 1);
		gfxlist[high(gfxlist)] := imgname;

		{$ifdef enable_decomp_hacks}
		// Hack: Let Ryoko-sensei blink in the very first Xenon scene.
		if (imgname = '"1010b"') and (scriptname = 'S00') then begin
			setlength(gfxlist, gfxlist.Length + 1);
			gfxlist[high(gfxlist)] := '"1010b_m"';
		end;
		{$endif}
	end;

	procedure _ShowGfx;
	var s : UTF8string;
	begin
		with ScriptWriter do for s in gfxlist do WriteBuf('(show ' + s + ') ');
		setlength(gfxlist, 0);
	end;

	procedure _DoTextOutput;
	var printstr : UTF8string = '';
		waitkey : UTF8string = '...';
		printofs, j, len : dword;
		u8c : string[4];
		l : longint;
		bracketchar : word;
		centertext : boolean = FALSE;
		sleep : byte = 0;

		{$ifdef enable_decomp_hacks}
		procedure _SkipSpaces;
		// Skip past any spaces, newlines, 05's, or diamond bullets in the bytecode string.
		begin
			// If a string starts with a couple spaces, it's probably intended to be center-aligned.
			if (NOT (centertext or optionsetup)) and (len > 10) then begin
				if (BEtoN(dword(src.readp^)) = $81408140) or (word(src.readp^) = $2020) then begin
					centertext := TRUE;
					printstr[printofs] := '\'; inc(printofs);
					printstr[printofs] := 'C'; inc(printofs);
				end;
			end;

			while len > 1 do begin
				if char(src.readp^) in [#5, ' '] then begin
					inc(src.readp);
					dec(len);
				end
				else if (BEtoN(word(src.readp^)) = $8140) or (BEtoN(word(src.readp^)) = $819F) then begin
					inc(src.readp, 2);
					dec(len, 2);
				end
				else break;
			end;
		end;
		{$endif}

	begin
		if src.RemainingBytes < 3 then raise DecompException.Create('string past script end');
		len := src.ReadByte + 1;
		if (len <> $100) and (dword(src.RemainingBytes) < len) then
			raise DecompException.Create('string past script end');
		setlength(printstr, len * 2);
		printofs := 1;
		if NOT optionsetup then begin
			printstr[1] := '"';
			printofs := 2;
		end;
		// Handle initial newlines; sometimes they're valid, sometimes not.
		while (len <> 0) and (byte(src.readp^) = $A) do begin
			dec(len);
			inc(src.readp);
			printstr[printofs] := '\'; inc(printofs);
			printstr[printofs] := 'n'; inc(printofs);
		end;
		if printofs > 6 then printofs := 6; // cap at 2 newlines max

		{$ifdef enable_decomp_hacks}
		// Hack: Skip initial newlines during intro wall of text.
		if (game = gid.Amy98) and (scriptname = 'A01') and (currentsection = 2) then printofs := 2;

		_SkipSpaces;
		if len < 2 then begin
			// No visible output in string, treat as waitkey.
			inc(src.readp, len);
			ScriptWriter.WriteBufLn('waitkey');
			exit;
		end;

		case game of
			gid.Xenon98, gid.Xenon98cd:
			if BEtoN(word(src.readp^)) = $8277 then begin // big X, title text, should be centered
				printstr[printofs] := '\'; inc(printofs);
				printstr[printofs] := 'C'; inc(printofs);
			end;
		end;

		// Catch Shift-JIS dialogue titles, generally written as "[name]", with either ASCII or Shift-JIS brackets.
		bracketchar := BEtoN(word(src.readp^));
		if (NOT optionsetup) and ((bracketchar = $8179) or (bracketchar = $816D)) then begin
			j := 2;
			inc(bracketchar);

			// The special code @@ is a custom player name. Output "\$pname;" for that.
			if (game = gid.EtsuGaku98) and (word((src.readp + 2)^) = $4040) then begin
				inc(j, 2);
				dword((@printstr[printofs])^) := NtoLE(dword($6E70245C)); inc(printofs, 4);
				dword((@printstr[printofs])^) := NtoLE(dword($3B656D61)); inc(printofs, 4);
			end;

			while (j <= 20) and (BEtoN(word((src.readp + j)^)) <> bracketchar) do begin
				l := byte((src.readp + j)^); inc(j);
				// Double-byte?
				if l in [$81..$84,$88..$9F,$E0..$EA] then begin
					l := l shl 8 + byte((src.readp + j)^);
					inc(j);
				end;
				// Add each character to printstr.
				u8c := GetUTF8(l);
				if printofs + 8 >= printstr.Length then setlength(printstr, printstr.Length + 128);
				move(u8c[1], printstr[printofs], length(u8c));
				inc(printofs, length(u8c));
			end;

			if BEtoN(word((src.readp + j)^)) = bracketchar then begin
				inc(src.readp, j + 2); dec(len, j + 2);

				printstr[printofs] := '\'; inc(printofs);
				printstr[printofs] := ':'; inc(printofs);
				if BEtoN(word(src.readp^)) = $8146 then begin // ":"
					inc(src.readp, 2);
					dec(len, 2);
				end
				else if byte(src.readp^) = $3A then begin // ":"
					inc(src.readp);
					dec(len);
				end;
				if byte(src.readp^) = $A then begin // linebreak
					inc(src.readp);
					dec(len);
				end;
			end;
		end;

		_SkipSpaces;
		{$endif}

		while len <> 0 do begin
			j := src.ReadByte;
			// Expand the string variable if necessary.
			if printofs + 8 >= printstr.Length then setlength(printstr, printstr.Length + 128);

			case j of
				0: break;

				1..2,6..9,$B,$D..$1F:
				begin
					printstr[printofs] := '\'; inc(printofs);
					printstr[printofs] := chr(j + 48); inc(printofs);
				end;

				$03: // big text, display as bold
				begin
					printstr[printofs] := '\'; inc(printofs);
					printstr[printofs] := 'B'; inc(printofs);
				end;

				$04:
				begin
					sleep := src.ReadByte;
					if sleep >= $30 then dec(sleep, $30);
					dec(len);
					waitkey := '';
				end;

				$05: ;//begin printstr[printofs] := '%'; inc(printofs); end;

				$0A:
				begin
					// Kinketsu has dialogue titles as "name, linebreak, bracket, text, end bracket".
					// Some titles in Desire route C are also such.
					bracketchar := BEtoN(word(src.readp^));
					if ((game = gid.KinKetsu98) or (game = gid.Desire98))
					and (printofs < 16) and (len > 8) and (bracketchar = $8175) then begin
						inc(src.readp, 2);
						dec(len, 2);
						printstr[printofs] := '\'; inc(printofs);
						printstr[printofs] := ':'; inc(printofs);
					end
					else begin
						printstr[printofs] := '\'; inc(printofs);
						printstr[printofs] := 'n'; inc(printofs);
						if byte(src.readp^) = $A then begin // can have more than one newline in a row
							inc(src.readp);
							printstr[printofs] := '\'; inc(printofs);
							printstr[printofs] := 'n'; inc(printofs);
							//if byte(src.readp^) = $A then inc(src.readp);
						end;
						{$ifdef enable_decomp_hacks}
						_SkipSpaces;
						{$endif}
					end;
				end;

				$0C:
				begin
					if optionsetup then raise DecompException.Create('$0C in option string @ $' + strhex(src.ofs - 1));
					printstr[printofs] := '"';
					setlength(printstr, printofs);
					ScriptWriter.WriteBufLn(printstr);
					_AddNiceIndent;
					ScriptWriter.WriteBufLn('waitkey noclear:1');
					_AddNiceIndent;
					printofs := 2;
					printstr[1] := '"';
				end;

				// Double-quotes and backslashes must be escaped.
				$22,$5C:
				begin
					printstr[printofs] := '\'; inc(printofs);
					printstr[printofs] := chr(j); inc(printofs);
				end;

				// Double-byte JIS.
				$81..$9F, $E0..$EF:
				begin
					u8c := GetUTF8(j shl 8 + src.ReadByte); dec(len);
					move(u8c[1], printstr[printofs], length(u8c));
					inc(printofs, length(u8c));
				end;

				// Single-byte katakana.
				$A1..$DF:
				begin
					u8c := GetUTF8(j);
					move(u8c[1], printstr[printofs], length(u8c));
					inc(printofs, length(u8c));
				end;

				// Single-byte plain ASCII.
				else begin
					// The special code @@ is a custom player name. Output "\$pname;" for that.
					if (game = gid.EtsuGaku98) and (j = $40) and (byte(src.readp^) = $40) then begin
						inc(src.readp);
						dword((@printstr[printofs])^) := NtoLE(dword($6E70245C)); inc(printofs, 4);
						dword((@printstr[printofs])^) := NtoLE(dword($3B656D61)); inc(printofs, 4);
					end
					else begin
						printstr[printofs] := chr(j); inc(printofs);
					end;
				end;
			end;

			dec(len);
		end;
		// Ignore Kinketsu's dialogue end bracket (drop three UTF-8 bytes).
		if ((game = gid.KinKetsu98) or (game = gid.Desire98))
		and (bracketchar = $8175) and (BEtoN(word((src.readp - 3)^)) = $8176) then
			dec(printofs, 3);
		if printofs <= 2 then exit;

		if optionsetup then begin
			setlength(printstr, printofs - 1);
			_NewOption(printstr);
		end
		else with ScriptWriter do begin
			// Hack: Add a newline and noclear waitkey after wall of text lines.
			if (game = gid.Amy98) and (scriptname = 'A01') and (currentsection = 2) then begin
				printstr[printofs] := '\'; inc(printofs);
				printstr[printofs] := 'n'; inc(printofs);
				if waitkey <> '' then waitkey := 'waitkey noclear:1';
			end;

			printstr[printofs] := '"';
			setlength(printstr, printofs);
			if printstr = '"\n"' then exit; // a standalone newline does more harm than good
			WriteBufLn(printstr);

			if waitkey <> '' then begin
				_AddNiceIndent;
				WriteBufLn(waitkey);
			end
			else if sleep <> 0 then begin
				_AddNiceIndent;
				WriteBufLn('(sleep ' + strdec(sleep * 1024) + ') tbox.clear');
			end;
		end;
	end;

var int1, int2, int3, int4, int5, int6 : integer;
begin
	ScriptWriter.Init(src);
	ifstatement := FALSE;
	optionsetup := FALSE;
	singlethen := FALSE;
	overlaytxt.x := 0;
	overlaytxt.y := 320;
	dword(palette[0]) := 0; // silence compiler
	palette[0].a := $FF;
	palette[1].FromRGBA4($76DF); // used by Xenon s0101 from image 0040
	palette[2].FromRGBA4($C45F); // same
	palette[4].FromRGBA4($D77F); // Xenon s0101 from image fa01
	palette[5].FromRGBA4($989F); // Xenon s0109 from image 1010c
	palette[6].FromRGBA4($FB6F); // Xenon s0106 from image 0110
	palette[7].FromRGBA4($666F); // Xenon s00 from image 0010
	palette[$F].FromRGBA4($FFFF);
	currentsection := 0;
	optionnode := @optionroot;
	optionroot.parent := NIL;
	optionroot.children := NIL;

	while src.readp < src.endp do with ScriptWriter do begin
		// See doc/csware.md for documentation on most bytecodes.
		cmd := src.ReadByte;
		case cmd of
			$23, $25, $27, $2B, $2D, $3D, $41, $47, $4D:
			WriteBufLn('//dummy $' + strhex(cmd));

			$01:
			begin
				WriteBufLn(#$A'// ' + StringOfChar('-', 65));
				currentsection := _ExpectValue(VNUMBER);
				WriteBufLn('@section' + strhex(currentsection) + ': choice.reset // @ ' + strhex(src.ofs - 1));
				setlength(optionroot.children, 0);
				optionnode := @optionroot;
				optionroot.labeltxt := 'c' + strhex(currentsection);
			end;

			$02:
			begin
				if thennesting <> 0 then begin
					currentLen := 0;
					dec(thennesting);
					_AddNiceIndent;
					WriteBufLn('end // implicit then-end');
				end;
				if length(optionroot.children) = 0 then
					WriteBufLn('// end section, no choices')
				else
					WriteBufLn('goto choice' + strhex(currentsection) + ' // end section');
				_PrintOptionDefines(optionroot);
				setlength(gfxlist, 0); // unlikely to carry over pending graphic state
			end;

			$04:
			begin
				optionsetup := TRUE;
				if optionnode^.parent = NIL then begin
					optiondefineline := lineIndex;
					optionenableline := lineIndex + 2;
					WriteBufLn(''); // <-- space for defines
					WriteBufLn(strcat('@choice&: // @ &', [currentsection, src.ofs - 1]));
					WriteBufLn(''); // <-- space for enables
				end;
			end;

			$05:
			begin
				if NOT optionsetup then
					raise DecompException.Create('option setup end without begin @ $' + strhex(src.ofs - 1));
				optionsetup := FALSE;
				if optionnode^.parent = NIL then WriteBufLn('choice.go');
			end;

			$06: WriteBufLn('goto section' + strhex(_ExpectValue(VNUMBER)));

			$07: WriteBufLn('call ' + _ExpectString + '.');

			$08: _EndOutcome;

			$09: WriteBufLn('//dummy $09 end game, return to main menu?');

			$0A: inc(thennesting); // then-block begin

			$0B:
			if thennesting = 0 then
				WriteBufLn('// end-if without begin')
			else begin
				if (NOT optionsetup) and (length(optionroot.children) <> 0) then
					WriteBufLn('goto choice' + strhex(currentsection));
				currentLen := 0;
				dec(thennesting);
				_AddNiceIndent;
				WriteBufLn('end');
			end;

			$0C: _AddGfx;

			$0D:
			begin
				WriteBuf('(transition) '); // instant, no sleep
				if overlaytxt.y < 310 then WriteBuf('(tbox.clear OVERLAYBOX) ');
				_ShowGfx;
				WriteBufLn('// $0D');
			end;

			$0E: WriteBufLn('$mus := ."' + _ExpectString + '"');

			$0F: WriteBufLn('mus.play $mus');

			$10, $31: WriteBufLn('mus.stop 1000 // $' + strhex(cmd));

			$11:
			begin
				setlength(gfxlist, 0);
				int1 := 0;
				if byte(src.readp^) = $FB then int1 := _ExpectValue(VNUMBER);
				case int1 of
					0: WriteBuf('(transition)'); // instant
					1: WriteBuf('(transition i)'); // interlaced
					else raise DecompException.Create(strcat('unk transition $& @ $&', [int1, src.ofs - 3]));
				end;
				WriteBufLn(' (gfx.clearall) sleep // $11-' + strhex(int1));
			end;

			$12:
			if ifstatement then
				WriteBuf(' == ' + strdec(_ExpectValue(VNUMBER)))
			else
				WriteBufLn(' := ' + strdec(_ExpectValue(VNUMBER)));

			$13:
			begin
				if optionsetup then
					WriteBuf('(')
				else
					WriteBuf('if (');
				ifstatement := TRUE;
			end;

			$14:
			begin
				singlethen := byte(src.readp^) <> $A;
				if NOT singlethen then inc(src.readp);

				if NOT optionsetup then begin
					currentLine[currentLen] := #0;
					if PChar(currentLine) = 'if ($v0 == 0' then begin
						currentLen := 0;
						WriteBuf('// if (0=0');
					end
					else
						inc(thennesting);
					WriteBufLn(') then');
				end
				else if NOT singlethen then
					raise DecompException.Create('non-singleline-then in option defines @ $' + strhex(src.ofs - 1));
				ifstatement := FALSE;
			end;

			$15:
			begin
				if NOT ifstatement then raise DecompException.Create('if-and without if @ $' + strhex(src.ofs - 1));
				WriteBuf(') & (');
			end;

			$16: _AddGfx;

			$19: WriteBufLn('// invoke save game dialog');

			$1B: WriteBufLn('// $1B push choice level?');

			$1C: WriteBufLn('// invoke load game dialog');

			$1D: WriteBufLn('// request game disk "' + GetUTF8(_ExpectString) + '"');

			$1E:
			begin
				int1 := _ExpectValue(VNUMBER);
				WriteBuf('(transition i) ');
				_ShowGfx;
				WriteBufLn('sleep // $11-' + strhex(int1));
			end;

			$1F: WriteBufLn('sys.restartgame // $1F');

			$20: WriteBufLn('sleep ' + strdec(_ExpectValue(VNUMBER) * 17));

			$21:
			begin
				int1 := 0;
				if byte(src.readp^) = $FB then int1 :=_ExpectValue(VNUMBER) * 240;
				WriteBufLn('(transition f ' + strdec(int1) + ') (gfx.remove BLACKOUT) sleep');
			end;

			$22:
			begin
				int1 := 0;
				if byte(src.readp^) = $FB then int1 :=_ExpectValue(VNUMBER) * 240;
				WriteBufLn('(transition f ' + strdec(int1) + ') (show "|F" overlay name:BLACKOUT w:1.0 h:1.0 z:9) sleep');
			end;

			$24:
			begin
				int1 := 1;
				if byte(src.readp^) = $FB then int1 := _ExpectValue(VNUMBER);
				case int1 of
					0: WriteBuf('(transition f) ');
					1: WriteBuf('(transition i) ');
					else raise DecompException.Create('unk transition @ $' + strhex(src.ofs - 3));
				end;
				_ShowGfx;
				WriteBufLn('sleep // $24-' + strhex(int1));
			end;

			$26:
			begin
				setlength(gfxlist, 0);
				WriteBufLn('(tbox.clear) gfx.clearall // $26 remove all incl viewframe');
			end;

			$28: _AddGfx;

			$29:
			if byte(src.readp^) = $FB then
				WriteBufLn('//dummy $29 title animation ' + strhex(_ExpectValue(VNUMBER)))
			else
				WriteBufLn('call main.title');

			$2A: _AddGfx;

			$2E:
			begin
				int1 := _ExpectValue(VNUMBER) * 8;
				int2 := _ExpectValue(VNUMBER);
				int3 := _ExpectValue(VNUMBER) * 8;
				int4 := _ExpectValue(VNUMBER);
				int5 := _ExpectValue(VNUMBER) * 8;
				int6 := _ExpectValue(VNUMBER);
				if gfxlist.Length <> 0 then begin
					if copy(gfxlist[gfxlist.Length - 1], 1, 6) = '"bast_' then begin
						gfxlist[gfxlist.Length - 1] := gfxlist[gfxlist.Length - 1] + strcat(' frame:% x:% y:%',
							[(int1 div 104) * 3, (int3 shl 15 + 272) div 544, (int4 shl 15 + 144) div 288]);
					end;
					WriteBuf('show ' + gfxlist[gfxlist.Length - 1] + ' anim ');
					setlength(gfxlist, gfxlist.Length - 1);
				end;
				WriteBufLn(strcat('// $2E %,%; full ofs %,%; framesize %x%', [int1, int2, int3, int4, int5, int6]));
			end;

			$2F: WriteBufLn('gfx.clearanims');

			$30:
			begin
				int1 := 0;
				if byte(src.readp^) = $FB then int1 :=_ExpectValue(VNUMBER);
				if int1 <> 0 then
					int2 := ((127 * 250) div int1) // fade should take about 127/int1 quarterseconds
				else
					int2 := 2000;
				WriteBufLn(strcat('mus.stop % // $30 music fade to stop at rate %', [int2, int1]));
			end;

			$32:
			begin
				if byte(src.readp^) = $FB then
					WriteBufLn('//dummy $32 screen/box ofs change? by $' + strhex(_ExpectValue(VNUMBER)))
				else
					_AddGfx;
			end;

			$33:
			begin
				setlength(gfxlist, 0);
				WriteBuf('(transition) (gfx.clearall) ');
				_AddGfx;
			end;

			$34: _AddGfx;

			$35: WriteBufLn('//dummy $35 flush graphics');

			$36:
			begin
				int1 := _ExpectValue(VNUMBER);
				int2 := _ExpectValue(VNUMBER);
				int3 := _ExpectValue(VNUMBER);
				int4 := _ExpectValue(VNUMBER);
				if (gfxlist.Length = 1) and (gfxlist[0] = '"pasi2"') then begin
					WriteBuf('(transition) (gfx.remove BLACKOUT) ');
					WriteBufLn('gfx.setsolidblit ' + gfxlist[0] + ' 0x' + hextable[int2] + hextable[int3] + hextable[int4] + 'F');
				end
				else begin
					if int1 < 16 then begin
						int5 := (int2 and $F) shl 12 + (int3 and $F) shl 8 + (int4 and $F) shl 4 + $F;
						palette[int1].FromRGBA4(int5);
						if txtcolor = int1 then WriteBuf(strcat(
							'(tbox.setparam OVERLAYBOX textcolor 0x%) ."\c%;" ', [strhex(int5, 1), strhex(int5)]));
					end;
					WriteBufLn(strcat('// $36 pal % := &,&,&', [int1, int2, int3, int4]));
				end;
			end;

			$37: WriteBufLn('//dummy $37 chapter completed: $' + strhex(_ExpectValue(VNUMBER)));

			$38:
			begin
				int1 := _ExpectValue(VNUMBER);
				int2 := _ExpectValue(VNUMBER);
				int3 := _ExpectValue(VNUMBER);
				int4 := _ExpectValue(VNUMBER);
				WriteBufLn(strcat('// $38 set box1 at %,%; box2 at %,%', [int1 * 8, int2, int3 * 8, int4]));
				case game of
					gid.Amy98:
					begin
						// Always use the first coordinate pair.
						if int2 < 310 then begin
							if int2 < overlaytxt.y then begin
								int5 := (int2 shl 15) div 400; // to 32k
								_AddNiceIndent;
								WriteBuf('($_mainbox := OVERLAYBOX) (tbox.setparam OVERLAYBOX textalign 0');
								if int1 > 12 then WriteBuf('.5'); // sneaky center
								WriteBufLn(') tbox.move OVERLAYBOX y:' + strdec(int5));
							end;
							overlaytxt.y := int2;
						end
						else if overlaytxt.y < 310 then begin
							overlaytxt.y := int2;
							WriteBufLn('$_mainbox := MAINBOX');
						end;
					end;

					else begin
						// Use whichever has the lower y.
						if int4 < int2 then int2 := int4;
						if int3 < int1 then int1 := int3;
						int5 := (int2 shl 15) div 400;

						if int2 < 310 then begin
							_AddNiceIndent;
							if overlaytxt.y >= 310 then begin
								overlaytxt.y := int2;
								WriteBufLn('($_mainbox := OVERLAYBOX) tbox.setloc OVERLAYBOX y:' + strdec(int5));
							end
							else begin
								if int2 > overlaytxt.y then
									WriteBufLn('print OVERLAYBOX ."\n"')
								else begin
									WriteBufLn('tbox.clear OVERLAYBOX');
									if int2 < overlaytxt.y then begin
										_AddNiceIndent;
										WriteBufLn('tbox.setloc OVERLAYBOX y:' + strdec(int5));
									end;
								end;
								overlaytxt.y := int2;
							end;
						end
						else if overlaytxt.y < 310 then begin
							overlaytxt.y := int2;
							_AddNiceIndent;
							WriteBufLn('$_mainbox := MAINBOX');
						end;
					end;
				end;
			end;

			$39:
			begin
				int1 := _ExpectValue(VNUMBER);
				int2 := _ExpectValue(VNUMBER);
				if int1 < 16 then begin
					txtcolor := int1;
					int3 := palette[int1].ToRGBA4;
					WriteBuf('(tbox.setparam OVERLAYBOX textcolor 0x' + strhex(int3, 1) + ') ');
					if game <> gid.EveBE98 then WriteBuf('."\c' + strhex(int3) + ';" ');
				end;
				WriteBufLn(strcat('// $39 text pal %,%', [int1, int2]));
			end;

			$3A: WriteBufLn('//dummy $3A-' + strhex(_ExpectValue(VNUMBER)));

			$3B: _AddGfx;

			$3C: WriteBufLn('waitkey // $3C');

			$3E: WriteBufLn('//dummy $3E-' + strhex(_ExpectValue(VNUMBER)));

			$40:
			begin
				if (game = gid.Desire98) and (gfxlist.Length <> 0) then
					WriteBuf('(show .' + gfxlist[gfxlist.Length - 1] + ') sleep 800 ');
				WriteBufLn('// $40 trigger/wait for anim?');
			end;

			$42: // whiteout
			begin
				int1 := _ExpectValue(VNUMBER) * 200;
				WriteBufLn('(transition f ' + strdec(int1) + ') (show "|FFFF" overlay name:BLACKOUT w:1.0 h:1.0 z:9) sleep');
			end;

			$43: WriteBufLn('//dummy $43-' + strhex(_ExpectValue(VNUMBER)));

			$44: WriteBufLn('//dummy $44-' + strhex(_ExpectValue(VNUMBER)));

			$45: WriteBufLn('// play sound effect $' + strhex(_ExpectValue(VNUMBER)));

			$46: WriteBufLn('//dummy $46 end looping sound effect');

			$48: begin
				WriteBuf('sleep 1000 //dummy $48');
				if byte(src.readp^) = $FB then WriteBuf('-' + strhex(_ExpectValue(VNUMBER)));
				WriteBufLn(' wait for music flag');
			end;

			$49: WriteBufLn('//dummy $49 clear story unlock vars in memory');

			$4A: WriteBufLn('//dummy $4A flush story unlock vars to disk');

			$4B: WriteBufLn('//dummy $4B load story unlock vars from disk');

			$FA: WriteBufLn('//dummy $FA show hardcoded title screen');

			$FB:
			begin
				dec(src.readp);
				WriteBufLn('//dummy $FB value $' + strhex(_ExpectValue(VNUMBER)));
			end;

			$FD:
			_DoTextOutput;

			$FE:
			begin
				dec(src.readp);
				WriteBuf('$v' + strdec(_ExpectValue(VVAR)));
			end;

			$FF:
			begin
				dec(src.readp);
				_NewOutcome(_ExpectValue(VOPTION));
			end;

			else raise DecompException.Create(strcat('Unknown code $& @ $&', [cmd, src.ofs - 1]));
		end;

		if (singlethen) and (byte((src.readp - 1)^) <> $14) then begin
			singlethen := FALSE;
			dec(thennesting);
			WriteBufLn('end');
		end;
		_AddNiceIndent;
	end;
end;

begin
	result := FALSE;
	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		if upcase(ExtractFileExt(loader.fileName)) = '.CC' then begin
			if LEtoN(loader.ReadDword) and $FFFFFF <> $5A434C then raise DecompException.Create('missing sig');
			loader.ofs := $16;
			if loader.ReadWord <> 0 then raise DecompException.Create('expected 00 00 at $16');

			Decompress_LZSS(loader.readp, loader.endp, buffy, bufsize, 18);
			_loader := TFileLoader.FromMemory(buffy, bufsize, FALSE);
			if decomp_param.keepInt then SaveFile(copy(outputfile, 1, outputfile.Length - 3) + 'bin', buffy, bufsize);

			{$ifdef enable_decomp_hacks} _ApplyHacks(_loader); {$endif}
			_TranslateBytecode(_loader);
		end
		else begin
			{$ifdef enable_decomp_hacks} _ApplyHacks(loader); {$endif}
			_TranslateBytecode(loader);
		end;
	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
		if _loader <> NIL then begin _loader.Destroy; _loader := NIL; end; // ref'd by scriptwriter
	end;

	result := TRUE;
end;

