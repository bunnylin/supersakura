{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_FairyDustSCR(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Fairy Dust SCR script file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var scriptname : UTF8string;
	cmdbase, choicebase, strbase : dword;
	i : dword;
	cmdcount, choicecount : word;
	choicesize : byte;
	isscr1 : boolean;

	procedure _TranslateBytecode(const src : TFileLoader);
	var cmdp : pointer;
		com, b, lastimgslot : byte;
		havebkg : boolean = FALSE;
		whiteout : boolean = FALSE;
		blackout : boolean = FALSE;
		gfxpending : boolean = FALSE;
		s, lastimg : string;

		procedure _Dummy;
		begin
			with ScriptWriter do begin
				if currentLen = 0 then WriteBuf('//dummy');
				WriteBuf(strcat(' $& & ', [com, byte((cmdp + 1)^)]));
				b := byte((cmdp + 2)^);
				WriteBuf(strcat('& & ', [b, byte((cmdp + 3)^)]));
				b := byte((cmdp + 4)^);
				WriteBufLn(strcat('& & ', [b, byte((cmdp + 5)^)]));
			end;
		end;

		function _ExpandStr(fromofs : dword) : UTF8string;
		var readp : pointer;
			w : word;
		begin
			readp := src.PtrAt(strbase + fromofs);
			// Find the terminating null for this string, set intermediate array to a fitting length.
			i := IndexByte(readp^, src.endp - readp, 0);
			result := '';
			setlength(result, i * 3 + 32);

			i := 0;
			while readp < src.endp do begin
				case byte(readp^) of
					0: break;

					1:;

					2: // name reference
					with gameConst do begin
						inc(readp);
						b := byte(readp^);
						if b = $30 then
							s := '\$s0;'
						else if (b > $30) and (b < $30 + hardcodedTxt.Length * 2) then begin
							dec(b, $30);
							if b >= hardcodedTxt.Length then
								s := hardcodedTxt[b - hardcodedTxt.Length + 1] + '\:'
							else
								s := hardcodedTxt[b];
						end
						else
							s := '\$s' + strhex(b) + ';';

						move(s[1], result[i + 1], s.Length);
						inc(i, s.Length);
					end;

					// Ignore linebreak.
					$A:;

					$20..$7F, $A1..$DE:
					begin
						w := byte(readp^) shl 1;
						w := BEtoN(word((@gameConst.hardcodedTxt[0][w])^));
						if w and $FF = 0 then w := w shr 8; // single-byte char after all
						// Ignore [ bracket, whitespace after a dialogue marker.
						if (w = $8179) or ((w = $8140) and (i > 2) and (copy(result, i - 1, 2) = '\:')) then begin
							inc(readp); continue;
						end;

						if w <> $817A then
							s := GetUTF8(w)
						else
							s := '\:'; // convert ] bracket to dialogue marker

						dword((@result[i + 1])^) := dword((@s[1])^);
						inc(i, s.Length);
					end;

					$81..$9F, $E0..$EA:
					begin
						w := BEtoN(word(readp^));
						s := GetUTF8(w);
						dword((@result[i + 1])^) := dword((@s[1])^);
						inc(i, s.Length);
						inc(readp, 2);
						continue;
					end;
				end;
				inc(readp);
			end;
			setlength(result, i);
		end;

		procedure _DoText;
		var //charid, anim, gfxid : byte;
			txt : UTF8string;
		begin
			{charid := byte((cmdp + 1)^);
			anim := byte((cmdp + 2)^);
			gfxid := byte((cmdp + 3)^);}
			txt := _ExpandStr(LEtoN(word((cmdp + 4)^)));

			with ScriptWriter do if txt <> '' then
				WriteBufLn('"' + txt + '"' + LineEnding + '...')
			else
				WriteBufLn('tbox.clear');
		end;

		procedure _DoChoice(index, count : word);
		var choicedata : pointer;
			txtofs : dword;
			jumpto : word;
		begin
			with ScriptWriter do begin
				if index + count > choicecount then begin
					LogError('choice oob @ $' + strhex(src.ofs));
					_Dummy;
					exit;
				end;

				WriteBuf('choice.reset' + LineEnding);
				choicedata := src.PtrAt(choicebase + index * choicesize);
				while count <> 0 do begin
					txtofs := LEtoN(word((choicedata + 2)^));
					jumpto := dword(LEtoN(word((choicedata + 4)^)) * 6) + cmdbase;
					WriteBuf(strcat('choice.set "%" ."%" // &' + LineEnding,
						[_ExpandStr(txtofs), strhex(jumpto, 4), LEtoN(word(choicedata^))]));
					AddJump(jumpto);
					inc(choicedata, choicesize);
					dec(count);
				end;
				WriteBufLn('choice.go');
			end;
		end;

	begin
		ScriptWriter.Init(src);
		lastimg := ''; lastimgslot := 0;
		with ScriptWriter do while cmdcount <> 0 do begin
			cmdp := src.readp;
			inc(src.readp, 6);
			com := byte(cmdp^);
			case com of
				0: WriteBufLn(strcat('$v& := %', [byte((cmdp + 1)^), LEtoN(word((cmdp + 2)^))]));

				1:
				begin
					WriteBuf(strcat('$v& += % //', [byte((cmdp + 1)^), smallint(LEtoN(word((cmdp + 2)^)))]));
					_Dummy;
				end;

				2: _DoText;

				3: WriteBufLn('tbox.clear');

				4,5:
				case game of
					gid.MaDoll, gid.MaDoll_w:
					begin
						WriteBuf('show ' + lastimg + 'A0 //');
						_Dummy;
						for b := 1 to 3 do begin
							s := lastimg + 'A' + strdec(b);
							if GetGraphicFile(s) <> 0 then WriteBufLn('show ' + s);
						end;
					end;
					else _Dummy;
				end;

				6:
				case game of
					gid.MaDoll, gid.MaDoll_w: begin WriteBuf('gfx.clearanims //'); _Dummy; end;
					else _Dummy;
				end;

				7:
				begin
					b := byte((cmdp + 2)^);
					if b <> 0 then
						WriteBufLn('mus.play ' + gameConst.songList[b - 1])
					else
						WriteBufLn('//mus.play song 0?');
				end;

				8: WriteBufLn('//mus.stop? 1600'); // maybe that's 0F instead

				9:
				case game of
					gid.MaDoll, gid.MaDoll_w:
					begin
						i := LEtoN(word((cmdp + 2)^)) + strbase;
						s := src.ReadStringFrom(i);
						i := byte((cmdp + 1)^); // slot

						// Under some conditions, do an implicit fade to black first.
						if (NOT (whiteout or blackout or gfxpending)) and (s <> '000')
						and ((havebkg) and (i = 0))
						then
							WriteBuf('(transition f) (gfx.clearall) (sleep) ')
						else if i = 0 then begin
							WriteBuf('(transition f) (gfx.clearall) ');
							gfxpending := TRUE;
						end;

						if NOT gfxpending then WriteBuf('(transition f) ');
						WriteBuf('show ."' + s + '"');
						if i = 0 then begin
							WriteBuf(' bkg');
							havebkg := TRUE;
						end;
						lastimg := s;
						lastimgslot := i;
						WriteBuf(' //'); _Dummy;
						gfxpending := TRUE;
					end;
					else _Dummy;
				end;

				$A,$B:
				case game of
					gid.MaDoll, gid.MaDoll_w:
					begin
						whiteout := (com = $B);
						if NOT gfxpending then WriteBuf('(transition f) ');
						if whiteout then
							WriteBufLn('(show "|FFFF" overlay name:WHITEOUT w:1.0 h:1.0) (sleep) sleep 690')
						else
							WriteBufLn('gfx.remove WHITEOUT');
						gfxpending := NOT whiteout;
					end;
					else _Dummy;
				end;

				$E,$F:
				case game of
					gid.MaDoll, gid.MaDoll_w:
					begin
						blackout := (com = $F);
						if blackout then begin
							WriteBufLn('(transition f) (gfx.clearall) sleep');
							havebkg := FALSE;
						end;
						gfxpending := NOT blackout;
					end;
					else _Dummy;
				end;

				$10:
				case game of
					gid.MaDoll, gid.MaDoll_w:
					begin
						WriteBuf('(transition f) (gfx.');
						if byte((cmdp + 1)^) = 0 then begin
							WriteBuf('clearall');
							havebkg := FALSE;
						end
						else if byte((cmdp + 1)^) = lastimgslot then
							WriteBuf('remove ."' + lastimg + '"')
						else
							WriteBuf('clearkids');
						WriteBuf(') sleep //');
						_Dummy;
						gfxpending := FALSE;
						lastimg := ''; lastimgslot := 0;
					end;
					else _Dummy;
				end;

				$11:
				case game of
					gid.MaDoll, gid.MaDoll_w:
					begin
						{$ifdef enable_decomp_hacks}
						// Hack: Use waitkeys instead of timed text in the opening.
						if (scriptname = 'STY_OP') or (scriptname = 'WSTY_OP') then
							if lineList[lineIndex - 1].EndsWith('...') then WriteBuf('//');
						{$endif}
						WriteBufLn('sleep ' + strdec(byte((cmdp + 2)^) * 1000));
					end;

					else _Dummy;
				end;

				$13:
				case game of
					gid.MaDoll, gid.MaDoll_w:
					begin
						i := dword(LEtoN(word((cmdp + 2)^)) * 6) + cmdbase;
						AddJump(i);
						WriteBufLn('goto ."' + strhex(i, 4) + '"');
					end;

					else _Dummy;
				end;

				$14..$17:
				case game of
					gid.MaDoll, gid.MaDoll_w:
					begin
						i := dword(LEtoN(word((cmdp + 4)^)) * 6) + cmdbase;
						AddJump(i);
						case com of
							$14: s := '==';
							$15: s := '!=';
							$16: s := '>';
							$17: s := '<';
						end;
						WriteBuf(strcat('if $v& % % then goto ."%" end //',
							[byte((cmdp + 1)^), s, byte((cmdp + 2)^), strhex(i, 4)]));
						_Dummy;
					end;
					else _Dummy;
				end;

				$1B:
				case game of
					gid.MaDoll, gid.MaDoll_w: _DoChoice(LEtoN(word((cmdp + 2)^)), LEtoN(word((cmdp + 4)^)));

					else _Dummy;
				end;

				$1D:
				case game of
					gid.MaDoll, gid.MaDoll_w:
					begin
						i := LEtoN(word((cmdp + 2)^)) + strbase;
						s := src.ReadStringFrom(i);
						if (s.Length > 4) and (s[s.Length - 3] = '.') then s := copy(s, 1, s.Length - 4); // trim .scr
						WriteBufLn('call ' + s);
					end;

					else _Dummy;
				end;

				$1E:
				case game of
					gid.MaDoll, gid.MaDoll_w: WriteBufLn('gfx.bash time:520 freq:75000 amp:4000 angle:16384');

					else _Dummy;
				end;

				$1F:
				case game of
					gid.MaDoll, gid.MaDoll_w: begin WriteBuf('gfx.flash //'); _Dummy; end;
					else _Dummy;
				end;

				$20:
				case game of
					gid.MaDoll, gid.MaDoll_w: begin WriteBuf('//special effect: letterbox on eyes closeup //'); _Dummy; end;
					else _Dummy;
				end;

				else _Dummy;
			end;

			if (gfxpending) and (NOT (byte(src.readp^) in [9, $A, $E, $13])) then begin
				lineList[lineIndex - 1] := lineList[lineIndex - 1] + LineEnding + 'sleep';
				gfxpending := FALSE;
			end;

			dec(cmdcount);
		end;
		ScriptWriter.WriteBufLn('return');
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 300 then raise DecompException.Create('file too tiny');

	i := loader.ReadDword;
	if (i <> BEtoN(dword($53435230))) and (i <> BEtoN(dword($53435231))) then
		raise DecompException.Create('no scrx sig');
	isscr1 := (byte((loader.readp - 1)^) = ord('1'));

	cmdcount := LEtoN(loader.ReadWord);
	if (cmdcount = 0) or (cmdcount * 6 + 8 > loader.RemainingBytes) then raise DecompException.Create('sus cmdcount');
	// Number of multiple choice definitions - omitted in MaDoll SCR0 scripts only, otherwise present even if == 0.
	choicecount := 0;
	if (isscr1) or ((game <> gid.MaDoll) and (game <> gid.MaDoll_w)) then
		choicecount := LEtoN(loader.ReadWord);

	cmdbase := loader.ofs;
	choicebase := cmdbase + dword(cmdcount * 6);

	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	// Determine byte size of each choice, total size of choice section.
	choicesize := 6;
	case game of
		gid.KoukanNikki1: if scriptname = 'META' then choicesize := 12;
		gid.KoukanNikki2: if NOT isscr1 then choicesize := 10;
		gid.MaDoll, gid.MaDoll_w: if NOT isscr1 then choicesize := 0;
	end;
	strbase := choicebase + choicecount * choicesize;
	//DumpBuffer(loader.PtrAt(choicebase), strbase-choicebase);
	if strbase > loader.fullFileSize then raise DecompException.Create('sus choicecount');
	i := LEtoN(loader.ReadWordFrom(strbase));
	inc(strbase, 2);
	if strbase + i <> loader.fullFileSize then raise DecompException.Create('sus str section size');

	// The strings section is encrypted with XOR $7F.
	XorBuffer(loader.PtrAt(strbase), loader.endp, $7F);

	// Dump the raw bytecode for analysis.
	if decomp_param.keepInt then SaveFile(outputfile + '.bin', loader.PtrAt(0), loader.fullFileSize);

	try
		//{$ifdef enable_decomp_hacks} _ApplyHacks(loader); {$endif}
		_TranslateBytecode(loader);
	finally
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
	end;

	result := TRUE;
end;

