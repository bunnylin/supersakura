{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_ApplePieBin(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Apple Pie bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var scriptname : UTF8string;
	filetable : TFileLoader = NIL;

{$ifdef enable_decomp_hacks}
procedure _ApplyHacks(const src : TFileLoader);
begin
	{case game of
		gid.EveBE98:
		case scriptname of
			// Hack: Dummy out an invalid save-load subchoice
			'B029': fillbyte(src.PtrAt($3647)^, 45, $0B);
		end;
	end;}
end;
{$endif}

procedure _ReadTable;
// Resource file names in scripts use index numbers against this table.
var fn : UTF8string;
	found : TStringBunch;
begin
	fn := PathCombine([ExtractFilePath(loader.fileName), 'file.def']);
	found := FindFiles_caseless(fn, FALSE, TRUE);
	if found = NIL then raise DecompException.Create('could not find ' + fn);
	filetable := TFileLoader.Open(found[0]);
end;

procedure _TranslateBytecode(const src : TFileLoader);

	function _LookUpFile(index : dword) : UTF8string;
	begin
		if index >= filetable.fullFileSize shr 2 then
			raise DecompException.Create(strcat('bad file ref % @ $&', [index, src.ofs - 1]));
		index := LEtoN(filetable.ReadWordFrom(index shl 1));
		if index >= filetable.fullFileSize + 3 then
			raise DecompException.Create(strcat('file ref ofs oob $& @ $&', [index, src.ofs - 1]));
		result := filetable.ReadStringFrom(index + 1);
		result := ExtractFileNameWithoutExt(result);
	end;

	procedure _GfxTransition(number : longint);
	begin
		case number of
			$10000: number := integer(ETransitionType.Instant);
			0: number := integer(ETransitionType.Crossfade);
			1: number := integer(ETransitionType.Interlaced);
			else raise DecompException.Create(strcat('unknown transition % @ $&', [number, src.ofs - 3]));
		end;
		ScriptWriter.WriteBufLn('gfx.transition ' + strdec(number));
	end;

	procedure _ShowImage;
	var imgname : UTF8string;
		x, y : word;
	begin
		imgname := src.ReadString;
		imgname := upcase(ExtractFileNameWithoutExt(imgname));
		ScriptWriter.WriteBufLn('gfx.transition 4');
		{$ifdef enable_decomp_hacks}
		if (imgname.Length > 3) and (copy(imgname, 1, 4) = 'WAKU') then ScriptWriter.WriteBuf('//');
		{$endif}
		x := LEtoN(src.ReadWord);
		if x >= 32 then dec(x, 32);
		y := LEtoN(src.ReadWord);
		if y >= 16 then dec(y, 16);
		with ScriptWriter do begin
			WriteBuf('gfx.show "' + imgname + '" bkg');
			if x <> 0 then WriteBuf(' x=' + strdec(x));
			if y <> 0 then WriteBuf(' y=' + strdec(y));
			WriteBufLn(' // $' + strhex(src.ReadByte));
			WriteBufLn('sleep');
		end;
	end;

	function _ReadStringRemoveSpaces : UTF8string;
	var u8c : string[4];
		l : dword = 1;
		c : byte;
	begin
		result := '';
		setlength(result, 80);
		repeat
			c := src.ReadByte;
			case c of
				0: break;

				// Double-byte JIS. (Ignore spaces.)
				$81..$9F, $E0..$EF:
				if (c = $81) and (byte(src.readp^) = $40) then
					inc(src.readp)
				else begin
					u8c := GetUTF8(c shl 8 + src.ReadByte);
					move(u8c[1], result[l], length(u8c));
					inc(l, length(u8c));
				end;

				// Single-byte katakana.
				$A1..$DF:
				begin
					u8c := GetUTF8(c);
					move(u8c[1], result[l], length(u8c));
					inc(l, length(u8c));
				end;

				// Single-byte plain ASCII.
				else begin
					result[l] := chr(c);
					inc(l);
				end;
			end;
			if l >= result.Length then raise DecompException.Create('too long string @ $' + strhex(src.ofs - l));
		until FALSE;
		setlength(result, l - 1);
	end;

	procedure _TextOutput;
	var u8c : string[4];
		printstr : UTF8string = '';
		l : dword;
		j : word;
		c, c2 : byte;
		indialog : word = 0;

		procedure _SkipIndent;
		begin
			while (src.readp + 1 < src.endp) and (BEtoN(word(src.readp^)) = $8140) do inc(src.readp, 2);
		end;

		procedure _EndLine;
		begin
			dec(l);
			if l <> 0 then ScriptWriter.WriteBufLn('"' + copy(printstr, 1, l) + '"');
			l := 1;
		end;

	begin
		// Shift-JIS text, not null-terminated but instead uses \escapes, where \E ends the string.
		// Dialogue appears in simple corner brackets. Title is alone on first line.
		// Narration often starts with an indented first line.

		setlength(printstr, 120);
		l := 1;
		_SkipIndent;
		while src.readp < src.endp do begin
			if l + 8 >= printstr.Length then setlength(printstr, l + 80);
			c := src.ReadByte;
			case c of
				0..31: raise DecompException.Create(strcat('unexpected $& in string @ $&', [c, src.ofs - 1]));

				ord('\'):
				begin
					c := src.ReadByte;
					if c = ord('E') then begin
						// End string, but if another string immediately follows, ignore and keep going.
						if byte(src.readp^) <> $F then break;
						inc(src.readp);
						_SkipIndent;
					end
					else begin
						c2 := src.ReadByte;
						if c = ord('C') then begin
							if c2 = ord('R') then begin
								// Ignore linebreaks unless at start of string, don't need manual word wrapping.
								if src.ReadByteFrom(src.ofs - 4) <> $F then begin
									_SkipIndent;
									continue;
								end;
								printstr[l] := '\'; inc(l);
								printstr[l] := 'n'; inc(l);
							end
							else raise DecompException.Create(strcat('unexpected \C% @ $&', [chr(c2), src.ofs - 1]));
						end
						else if (c = ord('H')) and (c2 = ord('A')) then begin
							_EndLine;
							if (src.readp + 2 < src.endp) and (byte(src.readp^) = ord('\'))
							and (LEtoN(word((src.readp + 1)^)) = $4C43) then begin // "\HA\CL"
								ScriptWriter.WriteBufLn('...');
								inc(src.readp, 3);
							end
							else
								ScriptWriter.WriteBufLn('waitkey noclear:1');
							if LEtoN(word(src.readp^)) <> $455C then
								raise DecompException.Create('\HA without immediate \E');
						end
						else begin
							printstr[l] := '\'; inc(l);
							byte(printstr[l]) := c; inc(l);
							byte(printstr[l]) := c2; inc(l);
						end;
					end;
				end;

				// Double-byte JIS.
				$81..$9F, $E0..$EF:
				begin
					j := c shl 8 + src.ReadByte;
					// 8175 = [, 8176 = ], 8177 = [[, 8178 = ]], 8168 = ", 8148 = ?, 8149 = !, 8142 = .
					// Ignore closing square bracket that matches previous open one, if in dialogue.
					if indialog = j then begin
						indialog := 0;
						continue;
					end;

					// Convert open square bracket right after a newline to a dialogue title marker.
					if (l > 2) and ((j = $8175) or (j = $8177)) and (printstr[l - 2] = '\') and (printstr[l - 1] = 'n')
					then begin
						printstr[l - 1] := ':';
						indialog := j + 1;
						continue;
					end;

					u8c := GetUTF8(j);
					move(u8c[1], printstr[l], length(u8c));
					inc(l, length(u8c));
				end;

				// Single-byte katakana.
				$A1..$DF:
				begin
					u8c := GetUTF8(c);
					move(u8c[1], printstr[l], length(u8c));
					inc(l, length(u8c));
				end;

				// Single-byte plain ASCII.
				else begin
					printstr[l] := chr(c);
					inc(l);
				end;
			end;
		end;
		_EndLine;
	end;

var vars : array[0..255] of word;
	i : dword;
	w : word;
	cmd, b : byte;
begin
	ScriptWriter.Init(src);
	vars[0] := 0; // silence compiler
	filldword(vars[0], length(vars) shr 1, 0);

	// See /doc/scr/applepie.md for documentation on most bytecodes.
	while src.readp < src.endp do with ScriptWriter do begin
		cmd := src.ReadByte;
		case cmd of
			// Conditional jump.
			$00:
			begin
				b := src.ReadByte;
				w := LEtoN(src.ReadWord);
				i := src.ReadByte;
				WriteBufLn(strcat('if $v% == % then goto ."%" end // $v%', [b, smallint(w), strhex(vars[i], 4), i]));
				AddJump(vars[i]);
			end;

			// Unconditional jump.
			$06:
			begin
				b := src.ReadByte;
				WriteBufLn('goto ."' + strhex(vars[b], 4) + '" // $v' + strdec(b));
				AddJump(vars[b]);
			end;

			// Poke variable.
			$07:
			begin
				b := src.ReadByte;
				w := LEtoN(src.ReadWord);
				WriteBuf('$v' + strdec(b) + ' := ');
				if smallint(w) > 9 then
					WriteBufLn('$' + strhex(smallint(w)))
				else
					WriteBufLn(strdec(smallint(w)));
				vars[b] := w;
				if (w < src.fullFileSize) and (w > 3) then AddJump(w);
			end;

			$0F: _TextOutput;

			// Print strings at a location.
			$10:
			begin
				i := src.ReadByte * 8;
				w := src.ReadByte * 8;
				b := src.ReadByte;
				WriteBufLn(strcat('//print at %,% [$&] color %', [i, w, b, src.ReadByte]));
				b := src.ReadByte;
				while b <> 0 do begin
					WriteBufLn('"' + GetUTF8(src.ReadString) + '"');
					dec(b);
				end;
			end;

			$11:
			begin
				i := src.ReadByte * 8;
				w := src.ReadByte * 8;
				WriteBuf(strcat('//dummy $11 draw box at %,% [$&]', [i, w, src.ReadByte]));
				i := src.ReadByte * 16;
				w := src.ReadByte * 16;
				WriteBufLn(strcat(' size %x% [$&]', [i, w, src.ReadByte]));
			end;

			$12:
			begin
				b := src.ReadByte;
				WriteBufLn(strcat('//dummy $12-&', [b]));
			end;

			// Draw graphic normally.
			$13:
			begin
				i := LEtoN(src.ReadWord);
				w := LEtoN(src.ReadWord);
				b := src.ReadByte;
				WriteBufLn(strcat('gfx.show % // &,&', [_LookUpFile(i), w, b]));
			end;

			// Draw face graphic.
			$14:
			begin
				b := src.ReadByte;
				w := LEtoN(src.ReadWord);
				i := LEtoN(src.ReadWord);
				WriteBufLn(strcat('gfx.show % name:FACE% x:% y:%', [_LookUpFile(i), src.ReadByte, b * 8, w * 8]));
			end;

			$15:
			begin
				b := src.ReadByte;
				WriteBufLn(strcat('//dummy $15-&', [b]));
			end;

			$16:
			begin
				b := src.ReadByte;
				while b <> 0 do begin
					w := LEtoN(src.ReadWord);
					WriteBuf(strcat('//dummy $16 highlightable %,%', [w * 8, LEtoN(src.ReadWord)]));
					w := LEtoN(src.ReadWord);
					WriteBufLn(strcat(' %x%', [w * 8, LEtoN(src.ReadWord)]));
					dec(b);
				end;
				WriteBufLn('$v0 := (choice.get)');
			end;

			$1A: WriteBufLn('mus.play ' + _LookUpFile(LEtoN(src.ReadWord)));

			$1B: WriteBufLn('mus.stop');

			$1C: WriteBufLn('sfx.play $' + strhex(src.ReadByte));

			$1E: WriteBufLn('call ' + _LookUpFile(LEtoN(src.ReadWord)) + '.');

			$1F: WriteBufLn('//dummy $1F roll up standard graphics window?');

			$20:
			begin
				b := src.ReadByte;
				w := src.ReadByte;
				i := src.ReadByte;
				WriteBuf(strcat('gfx.clearkids // at %,% size %x%', [b * 8, w * 8, i * 8, src.ReadByte * 8]));
				b := src.ReadByte;
				w := src.ReadByte;
				i := src.ReadByte;
				WriteBufLn(strcat(' color % // &,&', [b, w, i]));
			end;

			// Draw large graphic normally.
			$21:
			begin
				w := LEtoN(src.ReadWord);
				WriteBufLn('gfx.show ' + _LookUpFile(w));
				inc(src.readp, 2); // ignore palette ref
			end;

			$22: WriteBufLn('//dummy $22');

			$23:
			begin
				b := src.ReadByte;
				w := src.ReadByte;
				i := src.ReadByte;
				WriteBufLn(strcat('//dummy $23, set palette? &,&,&,&', [b, w, i, src.ReadByte]));
			end;

			$24:
			begin
				w := LEtoN(src.ReadWord);
				b := src.ReadByte; assert(b = 0);
				b := src.ReadByte; assert(b = 0);
				b := src.ReadByte; assert(b <> 0);
				b := src.ReadByte; assert(b <> 0);
				b := src.ReadByte;
				i := src.ReadByte;
				WriteBufLn(strcat('gfx.show % x=% y=%', [_LookUpFile(w), b, i]));
				b := src.ReadByte;
				if b <> $FF then raise DecompException.Create('expected 24...FF, got $' + strhex(b));
			end;

			$25:
			begin
				inc(src.readp, 2); // ignore file
				b := src.ReadByte;
				w := LetoN(src.ReadWord);
				case b of
					0: WriteBufLn('gfx.transition 0');
					1: WriteBufLn('//dummy $25-01 fade from black over ' + strdec(w));
					2: WriteBufLn('//dummy $25-02 fade to black over ' + strdec(w));
					3: WriteBufLn('//dummy $25-03 fade from white to black over ' + strdec(w));
					else raise DecompException.Create(strcat('unknown $25-$ at @&', [b, src.ofs - 1]));
				end;
			end;

			$26:
			begin
				w := LEtoN(src.ReadWord);
				if w = $FFFF then
					WriteBufLn('waitkey')
				else
					WriteBufLn('sleep ' + strdec(w * 10));
			end;

			$28: WriteBufLn('//dummy $28 draw standard empty textbox?');
			$29: WriteBufLn('//dummy $29 clear standard textbox?');

			{ $2A:
			begin
				w := LEtoN(src.ReadWord);
				b := src.ReadByte;
				i := src.ReadByte;
				WriteBuf(strcat('//dummy $2A gfx.show $&, source ofs %,%', [w * 8, b * 8, i * 8]));
				b := src.ReadByte;
				w := src.ReadByte;
				i := src.ReadByte;
				WriteBufLn(strcat(' target %,% size %x%', [b * 8, w * 8, i * 16, src.ReadByte * 8]));
			end;}

			$2E: WriteBufLn('//dummy load dialog');

			$2F: WriteBufLn('//dummy $2F-' + strhex(src.ReadByte));

			$31: WriteBufLn('$v0 := 0 // dummy $31, game completion check');

			$FF: WriteBufLn('return');

			else begin
				DumpBuffer(src.readp - 3, 16);
				raise DecompException.Create(strcat('Unknown $& @ $&', [cmd, src.ofs - 1]));
			end;
		end;
	end;
end;

begin
	result := FALSE;
	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		_ReadTable;
		{$ifdef enable_decomp_hacks} _ApplyHacks(loader); {$endif}
		_TranslateBytecode(loader);
	finally
		if filetable <> NIL then begin filetable.Destroy; filetable := NIL; end;
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
	end;

	result := TRUE;
end;

