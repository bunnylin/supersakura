{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_CuisseSIN(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Cuisse SIN script file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var scriptname : UTF8string;

	procedure _Decrypt;
	var l : dword;
		x : byte = 0;
	begin
		for l := loader.fullFileSize - 1 downto 0 do begin
			byte(loader.readp^) := byte(loader.readp^) xor x;
			inc(loader.readp);
			{$push}{$R-}inc(x);{$pop}
		end;
		loader.ofs := 0; // back to the start
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 1000 then raise DecompException.Create('file too tiny');

	_Decrypt;

	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		//{$ifdef enable_decomp_hacks} _ApplyHacks(loader); {$endif}
		//_TranslateScript(loader);
	finally
		{if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;}
		// Don't know how to convert these yet, just dump the decrypted script!
		SaveFile(outputfile, loader.PtrAt(0), loader.fullFileSize);
	end;

	result := TRUE;
end;

