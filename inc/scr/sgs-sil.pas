{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_SognaSIL(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Silence/Sogna SGS SIL bytecode file, saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var scriptname : UTF8string;
	isascii : byte = $FF;

	procedure _CheckIfAscii(const src : TFileLoader);
	// An English-patched game may have ASCII instead of JIS. For each script, when the first string is seen, call this
	// to scan the string for suspicious bytes that prove the script is JIS or not.
	var p : ^char;
	begin
		p := src.readp;
		while (p < src.endp) and (p^ <> #0) do begin
			if (p^ in ['$','%']) or ((p + 1)^ in ['$','%']) then begin
				isascii := 0;
				exit;
			end;
			inc(p, 2);
		end;
		isascii := 1;
	end;

	{$ifdef enable_decomp_hacks}
	procedure _ApplyHacks(const src : TFileLoader);
	const mes1hook : array of word = ($1D3B, $2607, $247F); // original, baba-patched, baba 1.1
	const mes2hook : array of word = ($2CF0, $3216, $31E8);
	var w : word;
	begin
		with ScriptWriter do case game of
			gid.Lime1:
			case scriptname of
				// Hack: Trap and subvert choice selection.
				'MES1':
				for w in mes1hook do
					if (src.fullFileSize > w) and (BEtoN(src.ReadWordFrom(w)) = $40FA) then begin
						byte(src.PtrAt(w)^) := $CC;
						word(src.PtrAt((w + 1))^) := $1818;
						break;
					end;
				'MES2':
				for w in mes2hook do
					if (src.fullFileSize > w) and (BEtoN(src.ReadWordFrom(w)) = $40FA) then begin
						byte(src.PtrAt(w)^) := $CC;
						word(src.PtrAt((w + 1))^) := $1818;
						break;
					end;
			end;
		end;
	end;
	{$endif}

	procedure _Menu(const src : TFileLoader);
	var txt, subtxt : string;
		hidestr : UTF8string = '';
		{resultvar,} topchoices, subchoices, hidevar : byte;
		i, j : byte;
	begin
		inc(src.readp);//resultvar := src.ReadByte;
		topchoices := src.ReadByte;
		if (topchoices > 40) or (topchoices = 0) then begin
			decomp_logger('[!] sus choice count ' + strdec(topchoices));
			if topchoices = 0 then exit;
		end;

		with ScriptWriter do begin
			WriteBufLn('choice.reset');
			for i := 0 to topchoices - 1 do begin
				subchoices := src.ReadByte;
				if (subchoices > 20) or (subchoices = 0) then
					decomp_logger('[!] sus subchoice count ' + strdec(subchoices));
				if subchoices <> 0 then dec(subchoices);
				hidevar := src.ReadByte;

				txt := '';
				while (src.readp < src.endp) and (byte(src.readp^) <> 0) do begin
					if isascii = 0 then
						txt := txt + ConvJIS(BEtoN(src.ReadWord))
					else
						txt := txt + char(src.ReadByte);
				end;
				inc(src.readp);
				if subchoices = 0 then WriteBufLn('choice.set "' + txt + '"');
				if hidevar <> 0 then hidestr := strcat('(choice.on "%" $v&)', [txt, hidevar]);

				if subchoices <> 0 then for j := 0 to subchoices - 1 do begin
					hidevar := src.ReadByte;
					subtxt := txt + ':';
					while (src.readp < src.endp) and (byte(src.readp^) <> 0) do begin
						if isascii = 0 then
							subtxt := subtxt + ConvJIS(BEtoN(src.ReadWord))
						else
							subtxt := subtxt + char(src.ReadByte);
					end;
					inc(src.readp);
					WriteBufLn('choice.set "' + subtxt + '"');
					if hidevar <> 0 then hidestr := hidestr + strcat('(choice on "%" $v&)', [subtxt, hidevar]);
				end;
			end;
		end;
	end;

	procedure _TextOutput(const src : TFileLoader; autowait : boolean);
	var baseofs, w : word;
		b : byte;
		esc, hadtext, hadtitle : boolean;

		procedure _Finalise;
		begin
			with ScriptWriter do begin
				if currentLine[currentLen - 1] <> '"' then begin
					if hadtitle then begin
						if currentLine[currentLen - 1] = ' ' then dec(currentLen);
						WriteBuf('\:');
						hadtitle := FALSE;
					end;
					WriteBufLn('"');
				end
				else begin
					dec(currentLen);
					if currentLen <> 0 then WriteBufLn();
				end;
			end;
		end;

	begin
		hadtext := FALSE;
		hadtitle := FALSE;
		if isascii = $FF then _CheckIfAscii(src);
		with ScriptWriter do begin
			WriteBuf('"');
			baseofs := src.ofs - 1;
			while (src.readp < src.endp) and (byte(src.readp^) <> 0) do begin
				w := BEtoN(src.ReadWord);
				if w shr 8 = ord('!') then begin
					esc := TRUE;
					case char(w) of
						'0':
						begin
							_Finalise;
							WriteBufLn('tbox.clear');
							WriteBuf('"');
							baseofs := src.ofs;
						end;
						'o': WriteBuf(' '); // convert linebreaks to spaces, we do our own soft linebreaking
						't':
						begin
							_Finalise;
							//WriteBufLn('sleep 1000'); // could also invoke the blinking "more" arrow
							hadtext := TRUE;
							WriteBuf('"');
							//baseofs := src.ofs;
						end;
						's':
						begin
							inc(src.readp); // skip #
							WriteBuf('\$s' + char(src.ReadByte) + ';');
						end;
						'p': inc(src.readp, 2); // print speed toggle, ignore
						'w':
						begin
							inc(src.readp); // skip #
							b := src.ReadByte;
							w := (b and 1) or ((b and 2) shl 7) or ((b and 4) shl 2);
							WriteBuf('\c' + strhex(w * $F, 3) + 'F;');
						end;
						else esc := FALSE;
					end;
					if esc then continue;
				end;
				hadtext := TRUE;
				if isascii = 0 then begin
					if (w = $215A) or (w = $215B) or (w = $2154) or (w = $2155) then // dialogue brackets
						hadtitle := TRUE
					else
						WriteBuf(ConvJIS(w));
				end
				else begin
					b := w shr 8;
					case b of
						$5B, $5D: hadtitle := TRUE; // dialogue brackets
						$22: WriteBuf('\"');
						else WriteBuf(char(b));
					end;
					b := w and $FF;
					case b of
						$5B, $5D: hadtitle := TRUE;
						$22: WriteBuf('\"');
						else WriteBuf(char(b));
					end;
				end;
			end;
			inc(src.readp); // skip terminator
			_Finalise;
			if autowait and hadtext then begin
				SetLineOfs(baseofs);
				WriteBufLn('...');
			end;
		end;
	end;

	procedure _TranslateBytecode(const src : TFileLoader);
	var	cmd, b : byte;
		w, destx, desty : word;

		procedure _SkipDrivePrefix;
		begin
			if (char(src.readp^) in ['A','B']) and (char((src.readp + 1)^) = ':') then inc(src.readp, 2);
		end;

	begin
		destx := 0; desty := 0;
		with ScriptWriter do while src.readp < src.endp do begin
			cmd := src.ReadByte;

			case cmd of
				0: _Menu(src);

				1,3:
				begin
					if cmd = 1 then
						WriteBuf('//dummy $01 set menu loc')
					else
						WriteBuf('//dummy $03 set text loc');
					for b := 0 to 3 do WriteBuf(' ' + strdec(src.ReadByte));
					WriteBufLn('');
				end;

				2:
				begin
					_TextOutput(src, TRUE);
					{$ifdef enable_decomp_hacks}
					case byte(src.readp^) of
						$19:
						begin
							// Next is a sleep, probably a timed dialogue line. Waitkey is preferable, omit the sleep.
							WriteBuf('//');
						end;
						$2A:
						begin
							// Next is a timed animation; omit the waitkey in this case.
							dec(lineIndex);
							currentLen := 0;
						end;
					end;
					{$endif}
				end;

				4: WriteBufLn('//dummy $04 set skip delay $' + strhex(LEtoN(src.ReadWord)));

				5:
				begin
					w := LEtoN(src.ReadWord);
					AddJump(w);
					if labelList[lineIndex] <> w then
						WriteBufLn('goto ."' + strhex(w, 4) + '"')
					else
						WriteBufLn('(waitkey) sys.restartgame // infinite loop at game end');
				end;

				6:
				begin
					_SkipDrivePrefix;
					WriteBufLn('goto ' + src.ReadString + ' // 06');
				end;

				7:
				begin
					b := src.ReadByte;
					_SkipDrivePrefix;
					WriteBufLn(strcat('$g% := ."%"', [b, src.ReadString]));
				end;

				8:
				begin
					b := src.ReadByte;
					WriteBuf('(transition) ');
					if (destx > 112) and (destx < 500) then
						WriteBufLn(strcat('show $g% x:% y:%',
							[b, (destx - 112) / gameConst.baseResXP, (desty - 8) / gameconst.baseResYP]))
					else
						WriteBufLn('(gfx.clearkids) show $g' + strdec(b) + ' bkg');
				end;

				9:
				begin
					b := src.ReadByte;
					WriteBuf('(transition f) ');
					case src.ReadByte of
						0: WriteBuf('(gfx.remove BLACKOUT) (show $g' + strdec(b) + ' bkg');
						1: WriteBuf('(gfx.clearkids) (show "|000F" name:BLACKOUT w:1.0 h:1.0 z:9');
						2: WriteBuf('(gfx.clearkids) (show "|FFFF" name:WHITEOUT w:1.0 h:1.0 z:9');
						3: WriteBuf('(gfx.remove WHITEOUT) (show $g' + strdec(b) + ' bkg');
					end;
					WriteBufLn(') sleep');
				end;

				$C:
				begin
					b := src.ReadByte;
					WriteBuf(strcat('//dummy $0C pal effect: $g% type=%', [b, src.ReadByte]));
					w := LEtoN(src.ReadWord);
					b := src.ReadByte;
					WriteBuf(strcat(' dura=% numcolors=%', [w, b]));
					while b <> 0 do begin
						cmd := src.ReadByte;
						w := src.ReadByte;
						WriteBuf(strcat(' &:&', [cmd, w]));
						cmd := src.ReadByte;
						w := src.ReadByte;
						WriteBuf(strcat('&&', [cmd, w]));
						dec(b);
					end;
					WriteBufLn('');
				end;

				$D: WriteBufLn('//dummy $0D set anim delay $' + strhex(LEtoN(src.ReadWord)));
				$E: WriteBufLn('mus.stop');
				$F:
				begin
					_SkipDrivePrefix;
					if game <> gid.GuynaRock2 then
						WriteBuf('mus.play time:200')
					else
						WriteBuf('$song :=');
					WriteBufLn(' ."' + src.ReadString + '"');
				end;
				$10: WriteBufLn('mus.play $song time:200');
				$11: WriteBufLn('//dummy $11 sound effect $' + strhex(src.ReadByte));

				$12, $13:
				begin
					b := src.ReadByte;
					WriteBuf('if $v' + strhex(b) + ' ');
					if cmd = $12 then WriteBuf('= ') else WriteBuf('!= ');
					b := src.ReadByte;
					if b <> 25 then WriteBuf(strdec(b)) else WriteBuf('-1');
					w := LEtoN(src.ReadWord);
					WriteBufLn(' then goto ."' + strhex(w, 4) + '" end');
					AddJump(w);
				end;

				// Set accumulator to variable value.
				$14: WriteBufLn('$0 := $v' + strhex(src.ReadByte));

				$15:
				begin
					b := src.ReadByte;
					w := LEtoN(src.ReadWord);
					WriteBuf('if $0 != ');
					if b <> 255 then WriteBuf(strdec(b)) else WriteBuf('-1');
					WriteBufLn(' then goto ."' + strhex(w, 4) + '" end');
					AddJump(w);
				end;

				$16:
				begin
					b := src.ReadByte;
					WriteBufLn(strcat('$v& := %', [b, src.ReadByte]));
				end;
				$17:
				begin
					w := LEtoN(src.ReadWord);
					WriteBufLn('//$_interrupt := ."' + strhex(w, 4) + '"');
					AddJump(w);
				end;

				$19: WriteBufLn('sleep ' + strdec(LEtoN(src.ReadWord) * 26) + ' // $19');

				$1B:
				begin
					WriteBuf('//$error' + strdec(src.ReadByte) + ' := ');
					if BEtoN(word(src.readp^)) = $2130 then inc(src.readp, 2); // skip clearbox
					_TextOutput(src, FALSE);
				end;

				$1C:
				begin
					w := LEtoN(src.ReadWord);
					destx := (w mod 80) shl 3;
					desty := w div 80;
					WriteBufLn(strcat('//dummy $1C set gfx loc %,%', [destx, desty]));
				end;

				$1E:
				begin
					w := LEtoN(src.ReadWord);
					b := src.ReadByte;
					WriteBufLn(strcat('(gfx.clearall) show "|0" bkg //$1E-&-&-&', [w, b, LEtoN(src.ReadWord)]));
				end;

				$1F:
				begin
					b := src.ReadByte;
					WriteBuf('(gfx.clearkids) (tbox.clear) (show $g' + strdec(b));

					w := src.ReadByte; // pan type
					if w and 1 = 0 then // bottom to top
						WriteBuf(' y:1.0 ay:1.0');

					inc(src.readp, 2); // window height, ignored

					WriteBuf(') move $g' + strdec(b));
					if w and 1 = 0 then
						WriteBuf(' y:0 ay:0')
					else
						WriteBuf(' y:1.0 ay:1.0');

					b := (w shr 1) + 1; // step size
					w := LEtoN(src.ReadWord) * 22; // step delay
					// Don't know what the graphic height is here without severe hackery! Make a wild guess.
					WriteBufLn(' time:' + strdec(gameConst.baseResYP * w div b));

					WriteBufLn('sleep');
				end;

				$20, $21:
				begin
					if cmd = $20 then WriteBuf('// load') else WriteBuf('// save');
					WriteBufLn(' game: ' + src.ReadString);
				end;

				$22:
				begin
					b := src.ReadByte;
					WriteBufLn(strcat('$v& := $v& | %', [b, b, src.ReadByte]));
				end;

				$23:
				begin
					w := LEtoN(src.ReadWord);
					AddJump(w);
					WriteBufLn('call ."' + strhex(w, 4) + '"');
				end;

				$24: WriteBufLn('return');

				$26:
				begin
					b := src.ReadByte;
					WriteBuf('$s' + char(ord('A') + b) + ' := :');
					_TextOutput(src, FALSE);
				end;

				$2A:
				begin
					if destx > 480 then WriteBuf('//'); // viewframe element, ignore
					b := src.ReadByte;
					WriteBufLn(strcat('(transition) show $g% frame:%', [b, src.ReadByte - 1]));
					w := LEtoN(src.ReadWord);
					if w <> 0 then WriteBufLn('sleep ' + strdec(w * 26) + ' // $2A');
				end;

				$2B:
				begin
					b := src.ReadByte;
					cmd := src.ReadByte;
					WriteBufLn(strcat('//dummy $2B set all pal: B=& R=& G=&', [b, cmd, src.ReadByte]));
				end;

				$2C:
				begin
					b := src.ReadByte;
					WriteBuf('(tbox.clear) show $g' + strdec(b) + ' anim');
					if (destx > 112) and (destx < 500) then
						WriteBuf(strcat(' x:% y:%',
							[(destx - 112) / gameConst.baseResXP, (desty - 8) / gameconst.baseResYP]));
					if game <> gid.GuynaRock2 then begin
						w := src.ReadByte;
						if w <> 0 then
							WriteBufLn(' // looping $' + strhex(w))
						else
							WriteBufLn(' // one-off');
						while TRUE do begin
							w := src.ReadByte;
							if w = 0 then break;
							dec(w);
							WriteBufLn(strcat('(gfx.setframe $g% %) sleep %', [b, w, src.ReadByte * 26]));
						end;
					end
					else WriteBufLn('');
				end;

				$2E: WriteBufLn('//dummy $2E restore palette or stop cycling');

				$31:
				begin
					w := LEtoN(src.ReadWord);
					b := src.ReadByte;
					WriteBuf(strcat('//dummy $31 draw colored rect &-&', [w, b]));
					w := LEtoN(src.ReadWord);
					b := src.ReadByte;
					WriteBufLn(strcat('-&-&', [w, b]));
				end;

				$39: WriteBufLn('yield // $39 end anims');

				$3E: WriteBufLn('//dummy $3E $v' + strhex(src.ReadByte) + ' get user input');

				$40:
				begin
					b := src.ReadByte;
					WriteBufLn(strcat('$v& := $v&', [b, src.ReadByte]));
				end;

				$43,$44:
				begin
					b := src.ReadByte;
					w := src.ReadByte;
					WriteBuf(strcat('//dummy $&-&-&-', [cmd, b, w]));
					w := LEtoN(src.ReadWord);
					WriteBufLn(strhex(w, 4));
					AddJump(w);
				end;

				$46: WriteBufLn('$v' + strhex(src.ReadByte) + ' += 1');

				$47:
				begin
					WriteBufLn('($1 := 0x64) while $1 < 0x6F do ($(v + tohex($1)) := 0) $1 += 1 end');
				end;

				$4D:
				begin
					w := LEtoN(src.ReadWord);
					AddJump(w);
					WriteBufLn(strcat('// conditional jump $&-%', [cmd, strhex(w, 4)]));
				end;

				$4F: WriteBufLn('waitkey // $4F');

				$50:
				begin
					b := src.ReadByte;
					w := src.ReadByte;
					WriteBuf(strcat('show $g% frame:$v& - 1', [b, w]));
					if (destx < 20) and (desty > 250) then // dialogue portrait
						WriteBuf(' y:1.0 ay:1.0');
					WriteBufLn(strcat(' // $50-&-&-&', [b, w, LEtoN(src.ReadWord)]));
				end;

				$51: WriteBufLn('$v' + strhex(src.ReadByte) + ' -= 1');

				$52:
				begin
					b := src.ReadByte;
					WriteBufLn('$v' + strhex(b) + ' += ' + strdec(src.ReadByte));
				end;

				$56,$57: WriteBufLn('//dummy $' + strhex(cmd) + ' persistent save? ' + src.ReadString);

				$58: WriteBufLn('(transition f time:4000) sleep // $58-' + strhex(LEtoN(src.ReadWord)));

				{$ifdef enable_decomp_hacks}
				$CC:
				begin
					WriteBufLn('(call choices) return // HACK');
					while byte(src.readp^) = $18 do inc(src.readp);
				end;
				{$endif}

				$B,$1A,$25,$2D,$32,$42,$4C,$55: WriteBufLn('//dummy $' + strhex(cmd));
				$A,$28,$29,$41,$4E,$54,$5A: WriteBufLn(strcat('//dummy $&-&', [cmd, src.ReadByte]));
				$1D,$27: WriteBufLn(strcat('//dummy $&-&', [cmd, LEtoN(src.ReadWord)]));

				else raise DecompException.Create(strcat('Unknown code $& @ $&', [cmd, src.ofs - 1]));
			end;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 1200 then raise DecompException.Create('file too tiny');

	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		ScriptWriter.Init(loader);
		{$ifdef enable_decomp_hacks} _ApplyHacks(loader); {$endif}
		_TranslateBytecode(loader);
	finally
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
	end;

	result := TRUE;
end;

