{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_ParsleyTXB(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Parsley TXB script bunch, and saves it in outputfile as a plain text sakurascript files.
// Throws an exception in case of errors.
var scriptname : UTF8string;
	fileindex, endofs : dword;
	src : TFileLoader = NIL;
	buf : pointer = NIL;

	procedure _Unpack;
	var packedsize, fullsize : word;
		gotsize : dword;
	begin
		fullsize := LEtoN(loader.ReadWord);
		packedsize := LEtoN(loader.ReadWord);
		if packedsize > loader.RemainingBytes then raise DecompException.Create('packedsize oob');
		Decompress_LZSS(loader.readp, loader.readp + packedsize, buf, gotsize, 18);
		if fullsize <> gotsize then
			decomp_logger(strcat('[!] sus size %, expected %, index % ', [gotsize, fullsize, fileindex]));
		src := TFileLoader.FromMemory(buf, gotsize, FALSE);
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 200 then raise DecompException.Create('file too tiny');

	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	getmem(buf, 65536);
	fileindex := 0;
	endofs := LEtoN(dword(loader.readp^));
	while endofs < loader.fullFileSize do begin
		loader.ofs := endofs;
		endofs := LEtoN(loader.ReadDwordFrom(fileindex shl 2 + 4));
		if endofs > loader.fullFileSize then raise DecompException.Create('fileofs oob');
		if endofs <= loader.ofs then raise DecompException.Create('fileofs not strictly ascending');

		try
			if dword(loader.readp^) = BEtoN(dword($464C5A30)) then begin
				inc(loader.readp, 4);
				_Unpack;
				// Dump the raw bytecode for analysis.
				if decomp_param.keepInt then
					SaveFile(strcat('%.%.bin', [outputfile, fileindex]), src.readp, src.fullFileSize);
			end
			else begin
				if decomp_param.keepInt then
					SaveFile(strcat('%.%.bin', [outputfile, fileindex]), loader.readp, endofs - loader.ofs);
			end;


		finally
			if src <> NIL then begin src.Destroy; src := NIL; end;
			freemem(buf); buf := NIL;
		end;
		inc(fileindex);
	end;

	try
		//{$ifdef enable_decomp_hacks} _ApplyHacks(loader); {$endif}
		//_TranslateScript(loader);
	finally
		{if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;}
	end;

	result := TRUE;
end;

