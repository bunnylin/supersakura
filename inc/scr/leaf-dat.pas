{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_LeafDat(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Leaf DAT bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var buffy1 : pointer = NIL;
	buffy2 : pointer = NIL;
	buf1size, buf2size : dword;
	block1start, block2start, block1size, block2size : dword;
	scriptname : UTF8string;
	_loader : TFileLoader = NIL;
begin
	result := FALSE;
	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		block1start := LEtoN(loader.ReadWord);
		if block1start <> 1 then raise DecompException.Create('expected 0001 as 1st word');
		block2start := LEtoN(loader.ReadWord);
		if block2start > $FF then raise DecompException.Create('expected low 2nd word');
		if (loader.ReadDword or loader.ReadDword or loader.ReadDword) <> 0 then
			raise DecompException.Create('expected dwords 1..3 to be empty');

		block1size := LEtoN(loader.ReadDword);
		if (block1size = 0) or (loader.readp + block1size shr 1 > loader.endp) then
			raise DecompException.Create('bad block1');

		block2start := block2start shl 4;
		XorBuffer(loader.readp, loader.PtrAt(block2start), $FF);
		Decompress_LZSS(loader.readp, loader.PtrAt(block2start), buffy1, buf1size, 18, TRUE, FALSE);
		SaveFile(outputfile + '.bin1', buffy1, buf1size);

		loader.ofs := block2start;
		block2size := LEtoN(loader.ReadDword);
		if (block2size = 0) or (loader.readp + block2size shr 1 > loader.endp) then
			raise DecompException.Create('bad block2');

		XorBuffer(loader.readp, loader.endp, $FF);
		Decompress_LZSS(loader.readp, loader.endp, buffy2, buf2size, 18, TRUE, FALSE);
		SaveFile(outputfile + '.bin2', buffy2, buf2size);

		//_loader := TFileLoader.FromMemory(buffy, bufsize, FALSE);
		//{ifdef enable_decomp_hacks} _ApplyHacks(_loader); {endif}
		//_TranslateBytecode(_loader);
	finally
		if buffy1 <> NIL then begin freemem(buffy1); buffy1 := NIL; end;
		if buffy2 <> NIL then begin freemem(buffy2); buffy2 := NIL; end;
		{if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;}
		if _loader <> NIL then begin _loader.Destroy; _loader := NIL; end; // ref'd by scriptwriter
	end;

	result := TRUE;
end;

