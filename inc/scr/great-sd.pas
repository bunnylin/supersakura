{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_GreatSD(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Great bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var buffy : pointer = NIL;
	scriptname : UTF8string;
const dynamicoverlay = {'show "|000F"'}' name:OVER overlay w:1.0 h:1.0';

procedure _TranslateBytecode(const src : TFileLoader);
var choiceverb : UTF8string = '';
	outcomelabel : UTF8string = '_';
	implicitwaitkey : boolean = FALSE;
	blackedout : boolean = FALSE;

	procedure _ShowImage;
	var imgname : UTF8string;
		x, y : word;
	begin
		imgname := src.ReadString;
		imgname := upcase(ExtractFileNameWithoutExt(imgname));
		with ScriptWriter do begin
			{$ifdef enable_decomp_hacks}
			if (imgname.Length > 3) and (copy(imgname, 1, 4) = 'WAKU') then WriteBuf('//');
			{$endif}
			WriteBuf('(transition i) ');
			x := LEtoN(src.ReadWord);
			if x >= 32 then dec(x, 32);
			y := LEtoN(src.ReadWord);
			if y >= 16 then dec(y, 16);
			WriteBuf('show "' + imgname + '" bkg');
			if x <> 0 then WriteBuf(' x:' + strdec(x));
			if y <> 0 then WriteBuf(' y:' + strdec(y));
			WriteBuf(' // $' + strhex(src.ReadByte));
			if NOT blackedout then
				WriteBufLn(LineEnding + 'sleep')
			else
				WriteBufLn('');
		end;
	end;

	function _ReadStringRemoveSpaces : UTF8string;
	var u8c : string[4];
		l : dword = 1;
		c : byte;
	begin
		result := '';
		setlength(result, 80);
		repeat
			c := src.ReadByte;
			case c of
				0: break;

				// Double-byte JIS. (Ignore spaces.)
				$81..$9F, $E0..$EF:
				if (c = $81) and (byte(src.readp^) = $40) then
					inc(src.readp)
				else begin
					u8c := GetUTF8(c shl 8 + src.ReadByte);
					move(u8c[1], result[l], length(u8c));
					inc(l, length(u8c));
				end;

				// Single-byte katakana.
				$A1..$DF:
				begin
					u8c := GetUTF8(c);
					move(u8c[1], result[l], length(u8c));
					inc(l, length(u8c));
				end;

				// Single-byte plain ASCII.
				else begin
					result[l] := chr(c);
					inc(l);
				end;
			end;
			if l >= result.Length then raise DecompException.Create('too long string @ $' + strhex(src.ofs - l));
		until FALSE;
		setlength(result, l - 1);
	end;

	procedure _DoTextOutput(rows : byte);
	var u8c : string[4];
		printstr : UTF8string = '';
		l : dword;
		j : word;
		r, c : byte;
	begin
		if rows = 0 then with ScriptWriter do begin
			WriteBufLn('tbox.clear');
			exit;
		end;
		// Text appears possibly multiple rows at a time, each string hard-linebroken and null-terminated.
		// Narration is printed normally from the left. Dialogue has a title at the start on the left, then the text
		// is hard-indented with spaces. Linebreaks are often, but not always, at punctuation in text. Where there's
		// no punctuation, the rows should be concatenated to maximise translation context.
		// Dialogue appears in simple corner brackets. Narration can appear on the same row as dialogue...
		// Corner-bracketed text may come without a name, in which case it's an unknown speaker.
		// If there are more than two rows, the row count is normally bumped up to 6, with the extra rows containing
		// just one space. Should ignore such empty rows.
		// If row count is above 6, a waitkey+clear should be injected after the 6th row.

		setlength(printstr, 80);
		l := 1; r := 0;
		while src.readp < src.endp do begin
			if l + 8 >= printstr.Length then setlength(printstr, l + 80);
			c := src.ReadByte;
			case c of
				0:
				begin
					dec(rows);
					inc(r);
					if l > 1 then
						if (rows = 0) or (r = 6) then begin
							ScriptWriter.WriteBufLn('"' + copy(printstr, 1, l - 1) + '"');
							if rows <> 0 then ScriptWriter.WriteBufLn('...');
							l := 1;
							r := 0;
						end;
					if rows = 0 then break;
					// Skip indenting spaces.
					while BEtoN(word(src.readp^)) = $8140 do inc(src.readp, 2);
				end;

				// Double-byte JIS.
				$81..$9F, $E0..$EF:
				begin
					j := c shl 8 + src.ReadByte;
					// 8175 = [, 8176 = ], 8168 = ", 8148 = ?, 8149 = !, 8142 = .
					if j = $8175 then begin
						if l = 1 then begin printstr[l] := '?'; inc(l); end;
						printstr[l] := '\'; inc(l);
						printstr[l] := ':'; inc(l);
						continue;
					end;
					if j = $8176 then begin
						// Closed square bracket but more on line or not last line? Inject break.
						if (byte(src.readp^) <> 0) or (rows > 1) then begin
							ScriptWriter.WriteBufLn('"' + copy(printstr, 1, l - 1) + '"' + LineEnding + '...');
							l := 1;
							r := 0;
						end;
						continue;
					end;
					if j = $8140 then continue;

					u8c := GetUTF8(j);
					move(u8c[1], printstr[l], length(u8c));
					inc(l, length(u8c));
				end;

				// Single-byte katakana.
				$A1..$DF:
				begin
					u8c := GetUTF8(c);
					move(u8c[1], printstr[l], length(u8c));
					inc(l, length(u8c));
				end;

				// Single-byte plain ASCII.
				else begin
					// The special code 5C24 ¥$ is a custom player name. Output "\$pname;" for that.
					if (c = $5C) and (byte(src.readp^) = $24) then begin
						inc(src.readp);
						dword((@printstr[l])^) := NtoBE(dword($5C24706E)); inc(l, 4);
						dword((@printstr[l])^) := NtoBE(dword($616D653B)); inc(l, 4);
						continue;
					end;
					printstr[l] := chr(c);
					inc(l);
				end;
			end;
		end;
		// Sometimes there's a graphic change right after text output and the waitkey should only go after that.
		// This flag causes waitkey to be output at the start of the bytecode read loop, depending on next command.
		implicitWaitkey := TRUE;
	end;

	procedure _Choices04;
	var j : byte;
	begin
		with ScriptWriter do begin
			// First number is the menu heading title ("main menu", "look/examine", "data" etc)
			// Second onward are predefined choice verbs until 00 is found.
			// Additional choices can be piled on beside these.
			choiceverb := '';
			outcomelabel := strhex(src.ofs shr 8) + strhex(src.ofs and $FF) + '_choice';
			j := src.ReadByte;
			if j = $0B then
				WriteBufLn('// $04-0B change equipment? character $v' + strdec(src.ReadByte))
			else if j = $10 then
				WriteBufLn('// $04-10 save/load? $' + strhex(src.ReadByte))
			else with gameConst do begin
				if j = 0 then WriteBuf('choice.remove // $04');
				WriteBufLn('');
				if j <> 0 then begin
					if (j >= hardcodedTxt.Length) or (hardcodedTxt[j] = '') then
						raise DecompException.Create('bad option title @ $' + strhex(src.ofs - 1));
					choiceverb := hardcodedTxt[j] + ':';
				end;
				repeat
					j := src.ReadByte;
					if j = 0 then begin WriteBufLn(''); break; end;
					if (j >= hardcodedTxt.Length) or (hardcodedTxt[j] = '') then
						raise DecompException.Create('bad option @ $' + strhex(src.ofs - 1));
					if j in [7,8] then WriteBuf('// meta // ');
					WriteBufLn(strcat('choice.set "%%" "%" // $&', [choiceverb, hardcodedTxt[j], outcomelabel, j]));
				until FALSE;
			end;
		end;
	end;

var txt : string;
	i : dword;
	w : word; //cury : word;
	cmd, b : byte;
begin
	ScriptWriter.Init(src);

	while src.readp < src.endp do with ScriptWriter do begin
		// See doc/great.md for documentation on most bytecodes.
		cmd := src.ReadByte;

		if (implicitWaitkey) and (NOT (cmd in [$01, $09, $0A, $17, $18, $1F, $2C, $2E, $31])) then begin
			// Append a waitkey to the previous command.
			lineList[lineIndex - 1] := lineList[lineIndex - 1] + LineEnding + '...';
			implicitWaitkey := FALSE;
		end;

		case cmd of
			$01: _ShowImage;

			$02:
			begin
				b := src.ReadByte;
				w := src.ReadByte;
				i := src.ReadByte;
				WriteBufLn(strcat('//dummy $02 enter dungeon 0%; $v% and $v%', [b, w, i]));
			end;

			$03:
			begin
				WriteBuf('//dummy $03 map #' + strdec(src.ReadByte) + ', locations');
				while TRUE do begin
					b := src.ReadByte;
					if b = 0 then break;
					WriteBuf(' ' + strdec(b));
				end;
				WriteBufLn('; -> $v' + strdec(src.ReadByte));
			end;

			$04: _Choices04;

			$05:
			begin
				b := src.ReadByte;
				case b of
					$01: WriteBufLn('//dummy 05-01 show quick party status in the choice box');
					$02: WriteBufLn('//dummy 05-02 show character stats window for $v' + strdec(src.ReadByte));
					$03: WriteBufLn('//dummy 05-03 show party status/combat params window');
					$04: WriteBufLn('//dummy 05-04 show equipment window');
					$05: WriteBufLn('//dummy 05-05 show item window');
					else raise DecompException.Create(strcat('unknown $05 subcode $& @ $&', [b, src.ofs]));
				end;
			end;

			$06:
			WriteBufLn('//dummy 06 ...');

			// 07: 03; 02 01, 3E 01, 04 01; 01 03
			// aa = number of enemy stacks
			// [bb, cc] = [id, number of enemies in stack]
			// two bytes at end...
			$07:
			begin
				i := src.ReadByte;
				WriteBuf('//dummy $07 fight:');
				txt := '"Fight!';
				while i <> 0 do begin
					b := src.ReadByte;
					WriteBuf(' mon' + strdec(b) + ' x ' + strdec(src.ReadByte) + ';');
					dec(i);
					txt := txt + ' Mon' + strdec(b) + ' appears.';
				end;
				b := src.ReadByte;
				WriteBufLn(' $' + strhex(b) + ' ' + strhex(src.ReadByte));
				WriteBufLn(txt + ' (You win!)"' + LineEnding + '...');
			end;

			$08:
			begin
				b := src.ReadByte;
				w := src.ReadByte;
				i := src.ReadByte;
				WriteBufLn(strcat('//dummy $08 Random encounter!! $& & &', [b, w, i]));
			end;

			// $31 returns cursor to origin? or shows triangle in bottom right box corner?
			$09, $1F, $2C: WriteBufLn('//dummy $' + strhex(cmd));

			$0A:
			begin
				b := src.ReadByte;
				WriteBuf('//dummy $0A highlightable ' + strdec(b) + ', $' + strhex(src.ReadByte));
				w := LEtoN(src.ReadWord);
				WriteBuf(' loc=' + strdec(w) + ',' + strdec(LEtoN(src.ReadWord)));
				w := LEtoN(src.ReadWord);
				WriteBuf(' size=' + strdec(w) + 'x' + strdec(LEtoN(src.ReadWord)));
				WriteBufLn(' $' + strhex(src.ReadByte));
			end;

			$0B:
			begin
				WriteBuf('choice.go');
				if choiceverb <> '' then WriteBuf(' parent:"' + copy(choiceverb, 1, choiceverb.Length - 1) + '"');
				WriteBufLn('');
				WriteBufLn('@' + outcomelabel + ':');
				WriteBuf('$v' + strdec(src.ReadByteFrom(src.ofs + 5)) + ' := 1 + (choice.gethighlight)');
				WriteBuf(' //');
				for i := 4 downto 0 do WriteBuf(' ' + strhex(src.ReadByte));
				WriteBufLn('');
				inc(src.readp);
			end;

			$0C:
			begin
				b := src.ReadByte;
				if b < 60 then
					txt := 'i ' + strdec(b * 240)
				else
					txt := 'f ' + strdec(b * 320);
				WriteBufLn('(transition ' + txt + ') (gfx.remove OVER) sleep // fade in from black');
				blackedout := FALSE;
			end;

			$0D:
			begin
				b := src.ReadByte;
				WriteBufLn(strcat('(transition f %) (show "|000F"%) sleep // fade to black $&',
					[b * 320, dynamicoverlay, src.ReadByte]));
				blackedout := TRUE;
			end;

			$0E:
			begin
				WriteBufLn(strcat('(transition f %) (gfx.remove OVER) sleep // fade in from white', [src.ReadByte * 320]));
				blackedout := FALSE;
			end;

			$0F:
			begin
				b := src.ReadByte;
				w := src.ReadByte;
				if w <> 0 then
					WriteBufLn(strcat('//dummy $0F menu rect clear or highlight? $& &', [b, w]))
				else
					WriteBufLn(strcat('(transition f %) (show "|FFFF"%) sleep // fade to white $&',
						[b * 320, dynamicoverlay, w]));
				blackedout := TRUE;
			end;

			$10:
			begin
				w := LEtoN(src.ReadWord);
				ScriptWriter.AddJump(w);
				txt := strhex(w);
				WriteBufLn('call "' + strhex(w shr 8) + strhex(w and $FF) + '"');
			end;

			$11:
			begin
				w := LEtoN(src.ReadWord);
				ScriptWriter.AddJump(w);
				txt := strhex(w);
				WriteBufLn('if $bool then (jump "' + strhex(w shr 8) + strhex(w and $FF) + '") end $bool := !$bool');
			end;

			$12:
			begin
				WriteBuf('$bool := ');
				while TRUE do begin
					b := src.ReadByte;
					if b and $80 = 0 then decomp_logger('[!] $12 param has $80 clear @ $' + strhex(src.ofs));
					WriteBuf('$v' + strdec(b and $7F) + ' ');
					b := src.ReadByte;
					case b of
						$92: WriteBuf('!=');
						$93: WriteBuf('<=');
						$94: WriteBuf('>=');
						$95: WriteBuf('<');
						$96: WriteBuf('>');
						$97: WriteBuf('==');
						else raise DecompException.Create(strcat('unknown comparison $& @ $&', [b, src.ofs]));
					end;
					b := src.ReadByte;
					if b and $80 = 0 then decomp_logger('[!] $12 param has $80 clear @ $' + strhex(src.ofs));
					WriteBuf(' ' + strdec(b and $7F) + ' ');
					b := src.ReadByte;
					if b and $80 = 0 then decomp_logger('[!] $12 param has $80 clear @ $' + strhex(src.ofs));
					if b = $82 then break;
					if b = $80 then WriteBuf('and ')
					else if b = $81 then WriteBuf('or ')
					else raise DecompException.Create(strcat('unknown $12 end $& @ $&', [b, src.ofs]));
				end;
				WriteBufLn('');
			end;

			$17:
			begin
				b := src.ReadByte;
				if b = 0 then
					WriteBufLn('mus.stop')
				else
					WriteBufLn('mus.play ' + strdec(b));
			end;

			$1A:
			begin
				b := src.ReadByte;
				WriteBufLn(strcat('//dummy $1A $&, $v%', [b, src.ReadByte]));
			end;

			$1B:
			begin
				// Most common: ofs 32,16 size 400,250 $00 = black out game view
				// Also a few instances of filling with color index 02 or 0A.
				// There's one instance of filling at 448,16 which is the menu UI area. Ignore this.
				inc(src.readp, 2 + 2 + 2 + 2);
				WriteBufLn('gfx.clearbkg // $1B fill rect with $' + strhex(src.ReadByte));
			end;

			$1E: WriteBufLn('sleep ' + strdec(src.ReadByte * 20));

			$21:
			begin
				b := src.ReadByte;
				WriteBuf('$v' + strdec(b) + ' := ');
				b := src.ReadByte;
				if b >= 80 then begin
					WriteBuf('$v');
					b := b and $7F;
				end;
				WriteBufLn(strdec(b));
			end;

			$22:
			begin
				b := src.ReadByte;
				WriteBufLn('$v' + strdec(b) + ' += ' + strdec(src.ReadByte));
			end;

			$23:
			begin
				b := src.ReadByte;
				WriteBufLn('$v' + strdec(b) + ' -= ' + strdec(src.ReadByte));
			end;

			$24:
			begin
				b := src.ReadByte;
				WriteBuf('casegoto ($v' + strdec(b) + ' - 1) ."');
				b := src.ReadByte;
				if b = 0 then raise DecompException.Create('$24 multijump with 0 options @ $' + strhex(src.ofs));
				for i := b - 1 downto 0 do begin
					w := LEtoN(src.ReadWord);
					AddJump(w);
					WriteBuf(strhex(w shr 8) + strhex(w and $FF) + ':');
				end;
				currentLine[currentLen - 1] := '"';
				WriteBufLn();
			end;

			$26:
			begin
				WriteBufLn('return' + LineEnding);
			end;

			$28:
			begin
				cmd := src.ReadByte;
				case cmd of
					$07: WriteBufLn('//dummy $28-07 restore full party HP');
					$08:
					begin
						b := src.ReadByte;
						WriteBufLn(strcat('//dummy $28-08 load game $& $v%', [b, src.ReadByte]));
					end;
					$0D, $12: WriteBufLn('//dummy $28-' + strhex(cmd));
					$0E: WriteBufLn('//dummy $28-0E message speed selection?');
					$0F: WriteBufLn('//dummy $28-0F weapon store');
					$10: WriteBufLn('//dummy $28-10 general store');
					$11: WriteBufLn('goto ' + src.ReadString + '.');
					$13: WriteBufLn('//dummy $28-13 add party member ' + strdec(src.ReadByte));
					$14: WriteBufLn('//dummy $28-14 del party member ' + strdec(src.ReadByte));
					$15: WriteBufLn('//dummy $28-15 add money ' + strdec(LEtoN(src.ReadWord)));
					$16: WriteBufLn('//dummy $28-16 add xp ' + strdec(LEtoN(src.ReadWord)));
					$17: WriteBufLn('//dummy $28-17 party re-order');
					$18: begin
						b := src.ReadByte;
						WriteBufLn(strcat('//dummy $28-18 disk request? $& $&', [b, src.ReadByte]));
					end;

					else raise DecompException.Create(strcat('Unknown $28-& @ $&', [cmd, src.ofs - 1]));
				end;
			end;

			$29:
			begin
				w := LEtoN(src.ReadWord);
				ScriptWriter.AddJump(w);
				txt := strhex(w shr 8) + strhex(w and $FF);
				WriteBufLn('goto "' + txt + '"' + LineEnding);
			end;

			$2E:
			begin
				//curx := src.ReadByte * 8;
				inc(src.readp);
				//cury := src.ReadByte * 2;
				inc(src.readp);
			end;

			$2F:
			begin
				// Topmost choice begins at Y coord 52, each choice is 16px tall.
				//WriteBufLn('choice.set ' + strdec((cury - 52) shr 4) + ' "' + _ReadStringRemoveSpaces + '"');
				WriteBufLn('choice.set "' + choiceverb + _ReadStringRemoveSpaces + '" "' + outcomelabel + '"');
				// One exception in the start script, a game title is printed in the middle of the game window.
			end;

			$30:
			_DoTextOutput(src.ReadByte);

			$31: // shows the "more text" arrow?
			labelList[lineIndex] := src.ofs; // adjust label ofs for next line

			$32:
			begin
				b := src.ReadByte;
				w := src.ReadByte;
				i := src.ReadByte;
				WriteBufLn(strcat('//dummy $32 $& & &', [b, w, i]));
			end;

			$34:
			WriteBufLn('$v' + strdec(src.ReadByte) + ' := 127 //dummy get party member choice 0-2');

			$36:
			begin
				WriteBufLn('// $36, eof');
				break;
			end;

			else raise DecompException.Create(strcat('Unknown $& @ $&', [cmd, src.ofs - 1]));
		end;
	end;
end;

begin
	result := FALSE;
	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	try
		_TranslateBytecode(loader);
	finally
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
	end;

	result := TRUE;
end;

