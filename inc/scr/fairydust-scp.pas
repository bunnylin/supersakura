{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_FairyDustSCP(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated Fairy Dust SCP script file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var scriptname : UTF8string;
begin
	result := FALSE;
	if loader.fullFileSize < 3 then raise DecompException.Create('file too tiny');

	// Decrypt, XOR all with AA.
	XorBuffer(loader.readp, loader.endp, $AA);

	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	// Dump the raw bytecode for analysis.
	if decomp_param.keepInt then SaveFile(outputfile + '.bin', loader.PtrAt(0), loader.fullFileSize);

	try
		//{$ifdef enable_decomp_hacks} _ApplyHacks(loader); {$endif}
		//_TranslateScript(loader);
	finally
		{if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;}
	end;

	result := TRUE;
end;

