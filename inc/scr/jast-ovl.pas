{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_JastOvl(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated JAST/Tiare bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var scriptname : UTF8string;

	bufsection : record
		resultsPtr, optionsPtr, picsPtr : word;
		scriptPtr, endPtr : word;
	end;

	i, j, l : dword;
	m : word;
	bitness, implicitWaitkey : byte;
	persistSprites, isBlackedOut, hasChoices, stashActive : boolean;
	txt : string;
	drawGfx : record
		drawCmd : array[0..69] of UTF8string; // sexyparfait really bangs on these, otherwise [0..15]
		drawCmdCount, gfxStyle, gfxOfsXP : dword;
		transition : char;
		pending : boolean;
	end;
	stringCache : array[0..15] of string[63]; // for 0B-4B
	choiceCombo : array[0..63] of record
		verbText, subjectText : string[32];
		id : word;
		jumpResult : array[0..31] of word;
	end;
	optionList : array[1..63] of record
		address : word;
		verbText : string[32];
		subjectText : array[1..24] of string[32];
	end;
	pictureList : array[0..63] of record
		gfxName : string[8];
		transitionStyle : ETransitionType;
		blackoutFirst, isSprite : boolean;
	end;
	animSlot : array[0..8] of record
		animOfsXP, animOfsYP : word;
		animGfxName : string[15];
		displayed : boolean;
	end;
	numCombos, numOptions, numPictures : byte;

{$ifdef enable_decomp_hacks}
procedure _ApplyHacks(const src : TFileLoader);
var d : dword;
begin
	case game of
		gid.AngelsC2:
		begin
			// Hack: change weird 8757 Shift-JIS to a visually similar double-slash
			if scriptname = 'S2_007' then word(src.PtrAt($349)^) := $2F2F;
			// Hack: clip unreachable code
			if scriptname = 'S4_016' then src.buffySize := $61D;
			if scriptname = 'S4_021' then src.buffySize := $C4;
			if scriptname = 'S4_022' then src.buffySize := $112;
			if scriptname = 'S4_027' then src.buffySize := $3F;
			if scriptname = 'S4_031' then src.buffySize := $25C;
			if scriptname = 'S4_035' then src.buffySize := $74;
			if scriptname = 'S4_036' then src.buffySize := $2BB;
			if scriptname = 'S4_043' then src.buffySize := $109;
			if scriptname = 'S4_044' then src.buffySize := $66F;
			if scriptname = 'S4_051' then src.buffySize := $1C5;
			if scriptname = 'S4_052' then src.buffySize := $182;
			if scriptname = 'S4_064' then src.buffySize := $62B;
			if scriptname = 'S4_071' then src.buffySize := $17E;
			if scriptname = 'S4_072' then src.buffySize := $1C9;
			if scriptname = 'S4_073' then src.buffySize := $6F9;
			// Hack: erase problems in reachable but redundant code
			if scriptname = 'S4_067' then dword(src.PtrAt($30C)^) := 0;
			if scriptname = 'S4_068' then dword(src.PtrAt($3E0)^) := 0;
			if scriptname = 'S4_070' then dword(src.PtrAt($1C9)^) := 0;
		end;

		gid.Deep:
		begin
			// Hack: restore incorrectly missing actions... even if they were disabled on purpose, it's throwing my
			// header parser off.
			if scriptname = 'H03_08' then word(src.PtrAt($22)^) := $008D;
			if scriptname = 'H04_03' then word(src.PtrAt($2A)^) := $00AE;
		end;

		gid.Eden:
		case scriptname of
			// Hack: disable "go to town" choice like the original engine inexplicably does
			'JO2101': word(src.PtrAt($3C)^) := $FFFF;
			// Hack: keep blackout active over new background
			'JO21026': begin
				dword(src.PtrAt($6E)^) := NtoBE(dword($45425F30)); // shift draw bkg left by one byte
				dword(src.PtrAt($72)^) := NtoBE(dword($31315F31));
				dword(src.PtrAt($76)^) := NtoBE(dword($00140332)); // replace 13-0F with 14-03-32 to keep blackout
			end;
			// Hack: resume normal music after departing crypt
			'JO2102O': begin
				dword(src.PtrAt($169)^) := NtoBE(dword($06101106));
				byte(src.PtrAt($16D)^) := 1;
			end;
			// Hack: resume normal music after departing flowerbed
			'JO2102P': begin
				dword(src.PtrAt($9C0)^) := NtoBE(dword($06101106));
				byte(src.PtrAt($9C4)^) := 1;
			end;
			// Hack: remove a dummy 09 opcode by adding a space and shifting text
			'JO4101': begin
				dword(src.PtrAt($D1)^) := $79810120;
				dword(src.PtrAt($D5)^) := $87835783;
			end;
		end;

		gid.FromH:
		begin
			// Hack: Correct 0B-4B item count, enables unaccessible code
			if scriptname = 'AT_002' then inc(byte(src.PtrAt($1041)^));
			// Hack: Correct 0B-4B item count
			if scriptname = 'MT_004' then inc(byte(src.PtrAt($3A1F)^));
			// Hack: Correct 0B-4B item count
			if scriptname = 'RT_001' then inc(byte(src.PtrAt($95A)^));
			// Hack: Correct 0B-4B item count
			if scriptname = 'RT_001B' then inc(byte(src.PtrAt($958)^));
		end;

		gid.Hohoemi:
		begin
			// Hack: premature slot call fix
			if scriptname = 'A_IBENT' then animSlot[1].animGfxName := 'AT_KA0';
		end;

		gid.Majokko:
		case scriptname of
			// Hack: separate two exit commands so the latter can be jumped to
			'MP7809': byte(src.PtrAt($FAF)^) := 1;
			'MP780E': byte(src.PtrAt($1083)^) := 1;
			'MP7A09': byte(src.PtrAt($10DA)^) := 1;
			'MP7A0E': byte(src.PtrAt($102E)^) := 1;
			'MP7C08': byte(src.PtrAt($FD9)^) := 1;
			'MP7C0D': byte(src.PtrAt($FD5)^) := 1;
		end;

		gid.Runaway:
		case scriptname of
			// Hack: change a dash into a long dash
			'MT_0101': byte(src.PtrAt($1F9C)^) := $2D;
			// Hack: fix typo
			'MT_0116': word(src.PtrAt($99)^) := $524F;
			// Hack: remove stray waitkey
			'MT_0201': if src.ReadByteFrom($64F) = 1 then byte(src.PtrAt($64F)^) := $A;
			// Hack: separate two dialogue lines
			'MT_0208': byte(src.PtrAt($942)^) := 1;
			// Hack: remove a waitkey
			'MT_0214': char(src.PtrAt($B49)^) := ' ';
			// Hack: split dialogue lines
			'MT_0218':
			begin
				byte(src.PtrAt($7C)^) := 1;
				byte(src.PtrAt($AD)^) := 1;
			end;
			// Hack: remove a waitkey
			'MT_0221': if src.ReadByteFrom($6DC) = 1 then byte(src.PtrAt($6DC)^) := $A;
			// Hack: remove a waitkey
			'MT_0304': char(src.PtrAt($E43)^) := ' ';
			// Hack: particularly ugly, two separate verbs GO, change one to G0
			'MT_0305': char(src.PtrAt($C0)^) := '0';
			// Hack: remove waitkeys
			'MT_0314': if src.ReadByteFrom($986) = 1 then byte(src.PtrAt($986)^) := $A;
			'MT_0412': if src.ReadByteFrom($1524) = 1 then byte(src.PtrAt($1524)^) := $A;
			'MT_0413': if src.ReadByteFrom($E0C) = 1 then byte(src.PtrAt($E0C)^) := $A;
			// Hack: separate dialogue lines
			'MT_0501': if src.ReadByteFrom($343) = $A then byte(src.PtrAt($343)^) := 1;
			// Hack: remove waitkey
			'MT_0622': if src.ReadByteFrom($9D7) = 1 then byte(src.PtrAt($9D7)^) := $A;
			'MT_0803':
			begin
				// Hack: fix stray capital
				if src.ReadByteFrom($489) = ord('U') then char(src.PtrAt($489)^) := 'u';
				// Hack: remove waitkey
				if src.ReadByteFrom($143B) = 1 then byte(src.PtrAt($143B)^) := $A;
			end;
			// Hack: remove waitkeys
			'MT_0808':
			begin
				if src.ReadByteFrom($CA2) = 1 then byte(src.PtrAt($CA2)^) := $A;
				if src.ReadByteFrom($1091) = 1 then byte(src.PtrAt($1091)^) := $A;
			end;
			// Hack: separate dialogue lines
			'MT_0815':
			if src.ReadByteFrom($563) = ord('H') then begin
				txt := #1'Hiroaki:';
				move(txt[1], src.PtrAt($563)^, length(txt));
			end;
			// Hack: remove waitkeys
			'MT_0816': if src.ReadByteFrom($1E5) = 1 then byte(src.PtrAt($1E5)^) := $A;
			'MT_0819': if src.ReadByteFrom($20A) = 1 then byte(src.PtrAt($20A)^) := $A;
			'MT_0905': if src.ReadByteFrom($276) = 1 then byte(src.PtrAt($276)^) := $A;
			'MT_0913':
			begin
				// Hack: add hook for fixing a missing graphic
				ScriptWriter.AddJump($79);
				// Hack: remove waitkey
				if src.ReadByteFrom($2AC) = 1 then byte(src.PtrAt($2AC)^) := $A;
			end;
			'MT_0916':
			begin
				// Hack: add hook for fixing a missing graphic
				ScriptWriter.AddJump($6E);
				// Hack: remove waitkey
				if src.ReadByteFrom($1B5) = 1 then byte(src.PtrAt($1B5)^) := $A;
			end;
			// Hack: remove a double-quote
			'MT_1002': char(src.PtrAt($1EB7)^) := ' ';
			// Hack: another one, PROCEED x2, change one to Pr0ceed
			'MT_1003': char(src.PtrAt($CA)^) := '0';
			// Hack: remove waitkeys
			'MT_1102':
			begin
				if src.ReadByteFrom($A88) = 1 then byte(src.PtrAt($A88)^) := $A;
				if src.ReadByteFrom($AC4) = 1 then byte(src.PtrAt($AC4)^) := $A;
			end;
			// Hack: fix choices, 2x Yes and 2x No
			'MT_1103':
			begin
				if src.ReadByteFrom($A4) = ord('S') then char(src.PtrAt($A4)^) := '5';
				if src.ReadByteFrom($A8) = ord('O') then char(src.PtrAt($A8)^) := '0';
				// Hack: remove waitkeys
				if src.ReadByteFrom($EF3) = 1 then byte(src.PtrAt($EF3)^) := $A;
				if src.ReadByteFrom($10A4) = 1 then byte(src.PtrAt($10A4)^) := $A;
				if src.ReadByteFrom($10F0) = 1 then byte(src.PtrAt($10F0)^) := $A;
			end;
			'MT_1104': if src.ReadByteFrom($7EB) = 1 then byte(src.PtrAt($7EB)^) := $A;
			'MT_1107': if src.ReadByteFrom($854) = 1 then byte(src.PtrAt($854)^) := $A;
			'MT_1111': if src.ReadByteFrom($497) = 1 then byte(src.PtrAt($497)^) := $A;
			// Hack: unreachable code causes parsing errors, just snip it out
			'MT_1118': src.buffySize := $38A;
			'MT_1119':
			begin
				src.buffySize := $8BD;
				// Hack: fix a colon
				if src.ReadByteFrom($736) = ord('?') then word(src.PtrAt($736)^) := BEtoN(word($203F));
			end;
			// Hack: shift ENDINGS hook a bit earlier
			'MT_9906': word(src.PtrAt($2BA)^) := $0010;
		end;

		gid.Sakura:
		case scriptname of
			// Hack: split two lines from one dialogue entry, twice
			'CS103':
			begin
				byte(src.PtrAt($9FC)^) := 1;
				byte(src.PtrAt($A36)^) := 1;
			end;
			// Hack: change %100 to %001
			'CS104': dword(src.PtrAt($11C6)^) := $20313030;
			// Hack: change odd transition to a crossfade
			'CS208': byte(src.PtrAt($51A)^) := $3B;
			// Hack: remove trailing space from TALK
			'CS211': byte(src.PtrAt($3D)^) := 0;
			// Hack: change THINK + GIRL to just THINK (inconsistent scripting)
			'CS212': word(src.PtrAt($67)^) := $FFFF;
			// Hack: change TALK + BAGS to just TALK
			'CS403': word(src.PtrAt($78)^) := $FFFF;
			'CS501':
			begin
				// Hack: split two lines from one dialogue entry
				byte(src.PtrAt($419)^) := 1;
				// Hack: add a waitkey to keep lines apart
				byte(src.PtrAt($5CB)^) := 1;
				// Hack: change "Shuji..." to "%001... "
				dword(src.PtrAt($CEB)^) := $31303025;
				dword(src.PtrAt($CEF)^) := $202E2E2E;
			end;
			// Hack: fix inconsistent clothing change
			'CS510_11': char(src.PtrAt($172)^) := 'F';
			// Hack: fix an animation slot
			'CS514_E': byte(src.PtrAt($DCA)^) := 3;
			// Hack: split two lines from one dialogue entry
			'CS601': byte(src.PtrAt($BFB)^) := 1;
			// Hack: eliminate a false positive dialogue title
			'CS605': char(src.PtrAt($36A)^) := '!';
			// Hack: split two lines from one dialogue entry
			'CS702': byte(src.PtrAt($4C4)^) := 1;
			// Hack: split two lines from one dialogue entry
			'CS704': byte(src.PtrAt($67C)^) := 1;
			// Hack: split two lines from one dialogue entry, and combine three titles
			'CS705':
			begin
				byte(src.PtrAt($374)^) := 1;
				txt := '/Kiyomi/Mio:  %001!!' + space(18);
				move(txt[1], src.PtrAt($643)^, length(txt));
				// Hack: translation error, should be Hidemi instead of Meimi
				txt := 'Hidemi:';
				move(txt[1], src.PtrAt($C52)^, length(txt));
			end;
			// Hack: combine 3 titles, split dialogue pair
			'CS707':
			begin
				txt := '/Kiyomi/Mio:  %001!!' + space(15);
				move(txt[1], src.PtrAt($670)^, length(txt));
				byte(src.PtrAt($FFD)^) := 1;
			end;
			// Hack: split two lines from one dialogue entry, premature slot call fix
			'CS801_3':
			begin
				byte(src.PtrAt($96A)^) := 1;
				animSlot[6].animOfsXP := 96; animSlot[6].animGfxName := 'CT03A0';
				animSlot[5].animGfxName := 'CT02IA0';
			end;
			'CS801_4':
			begin
				// Hack: combine three synchronized exclamations
				txt := '/Kiyomi/Mio:  %001!' + space(15);
				move(txt[1], src.PtrAt($A38)^, length(txt));
				// Hack: remove unintended exit, enabling unreachable code
				byte(src.PtrAt($40A)^) := $20;
			end;
			// Hack: split Reiko/Mio lines properly
			'CS804':
			begin
				byte(src.PtrAt($12F)^) := 1;
				txt := '/Mio:  ';
				move(txt[1], src.PtrAt($199)^, length(txt));
				move(txt[1], src.PtrAt($3E6)^, length(txt));
				txt := '.......';
				move(txt[1], src.PtrAt($1B4)^, length(txt));
				move(txt[1], src.PtrAt($405)^, length(txt));
			end;
			// Hack: split Meimi/Seia line
			'CS810': byte(src.PtrAt($DB4)^) := 1;
			// Hack: fix original animation slot bugs
			'CS810_1':
			begin
				byte(src.PtrAt($E8)^) := 1;
				byte(src.PtrAt($1C3)^) := 7;
			end;
			// Hack: combine *pantpant* lines
			'CS811':
			begin
				txt := '/Aki:  *pant pant*' + space(14);
				move(txt[1], src.PtrAt($4B0)^, length(txt));
				move(txt[1], src.PtrAt($51B)^, length(txt));
				txt := '/Girl:  *pant pant*' + space(14);
				move(txt[1], src.PtrAt($53F)^, length(txt));
				txt := '/%001:  *pant pant pant*' + space(19);
				move(txt[1], src.PtrAt($6E3)^, length(txt));
				txt := '/Girl:  *pant pant pant*' + space(19);
				move(txt[1], src.PtrAt($712)^, length(txt));
				move(txt[1], src.PtrAt($7F3)^, length(txt));
				move(txt[1], src.PtrAt($83D)^, length(txt));
				move(txt[1], src.PtrAt($9A1)^, length(txt));
				move(txt[1], src.PtrAt($AA6)^, length(txt));
			end;
			// Hack: fix a dialogue title
			'CS819':
			begin
				txt := ':  ';
				move(txt[1], src.PtrAt($AA4)^, length(txt));
			end;
			// Hack: make a waitkey nonclearing
			'CS824': byte(src.PtrAt($77E)^) := 8;
			// Hack: split dialogue lines
			'CS901':
			begin
				byte(src.PtrAt($2A7)^) := 1;
				byte(src.PtrAt($31A)^) := 1;
				byte(src.PtrAt($383)^) := 1;
			end;
			// Hack: skip over useless code
			'CS904_A':
			begin
				dword(src.PtrAt($72)^) := $008D360B;
				fillbyte(src.PtrAt($76)^, 10, 0);
			end;
			// Hack: remove an extraneous waitkey
			'CS904_H': byte(src.PtrAt($743)^) := 32;
			// Hack: change a waitkey to noclear
			'CSA01': byte(src.PtrAt($534)^) := 8;
			// Hack: force a linefeed
			'CSA10': byte(src.PtrAt($847)^) := $A;
			// Hack: force linebreaks in read mini-letters, signature tries to be sort of right-aligned by using lots
			// of whitespace.
			'CSB05_':
			begin
				byte(src.PtrAt($D4B)^) := 10;
				byte(src.PtrAt($EB2)^) := 10;
				byte(src.PtrAt($1037)^) := 10;
			end;
			// Hack: Replace some exits with returns for consistency
			'CSC01_A':
			begin
				byte(src.PtrAt($7E8)^) := 3;
				byte(src.PtrAt($908)^) := 3;
				byte(src.PtrAt($A3F)^) := 3;
				byte(src.PtrAt($B47)^) := 3;
			end;
			'CSC01_C':
			begin
				byte(src.PtrAt($789)^) := 3;
				byte(src.PtrAt($80E)^) := 3;
				byte(src.PtrAt($944)^) := 3;
				byte(src.PtrAt($A24)^) := 3;
			end;
			'CSC01_F':
			begin
				byte(src.PtrAt($69B)^) := 3;
				byte(src.PtrAt($6A1)^) := 3;
				byte(src.PtrAt($7A2)^) := 3;
				byte(src.PtrAt($7CC)^) := 3;
				byte(src.PtrAt($85C)^) := 3;
				byte(src.PtrAt($890)^) := 3;
				byte(src.PtrAt($970)^) := 3;
			end;
			// Hack: separate dialogue lines, although they're supposed to be overlapping...
			'CSC03_A': if src.ReadByteFrom($2FD) = $A then byte(src.PtrAt($2FD)^) := 1;
			'CSE_ABAD':
			begin
				// Hack: change "Shuji..." to "%001...."
				dword(src.PtrAt($88)^) := $31303025;
				byte(src.PtrAt($8C)^) := $2E;
			end;
			// Hack: fix a title
			'CSE_BBAD': char(src.PtrAt($2FA)^) := ':';
			// Hack: remove stray backslash
			'CSE_E_6': if src.ReadByteFrom($18A) = ord('\') then char(src.PtrAt($18A)^) := ' ';
			// Hack: force a linebreak
			'CSE_E_7': byte(src.PtrAt($A72)^) := 10;
			// Hack: fix anim slot 20 to something smaller
			'CSE_FBAD': byte(src.PtrAt($4A2)^) := 8;
			// Hack: split dialogue lines
			'CSE_H_1':
			begin
				byte(src.PtrAt($2A5)^) := 1;
				byte(src.PtrAt($2CC)^) := 1;
			end;
		end;

		gid.Sakura98:
		begin
			// Hack: premature slot call fix
			if scriptname = 'CS801_3' then begin
				animSlot[6].animOfsXP := 96; animSlot[6].animGfxName := 'CT03A0';
				animSlot[5].animGfxName := 'CT02IA0';
			end;
			// Hack: fix original animation slot bugs
			if scriptname = 'CS810_1' then begin
				byte(src.PtrAt($E0)^) := 1;
				byte(src.PtrAt($1EF)^) := 7;
			end;
			// Hack: Replace some exits with returns for consistency
			if scriptname = 'CSC01_A' then begin
				byte(src.PtrAt($7F8)^) := 3;
				byte(src.PtrAt($906)^) := 3;
				byte(src.PtrAt($A62)^) := 3;
				byte(src.PtrAt($BA1)^) := 3;
			end;
			if scriptname = 'CSC01_C' then begin
				byte(src.PtrAt($887)^) := 3;
				byte(src.PtrAt($93E)^) := 3;
				byte(src.PtrAt($AC6)^) := 3;
				byte(src.PtrAt($BCB)^) := 3;
			end;
			if scriptname = 'CSC01_F' then begin
				byte(src.PtrAt($784)^) := 3;
				byte(src.PtrAt($78A)^) := 3;
				byte(src.PtrAt($88C)^) := 3;
				byte(src.PtrAt($8C6)^) := 3;
				byte(src.PtrAt($994)^) := 3;
				byte(src.PtrAt($9DC)^) := 3;
				byte(src.PtrAt($AB9)^) := 3;
			end;
		end;

		gid.SanShimai:
		case scriptname of
			// Hack: split a thought due to a jump into the middle of a string
			'SK_103': byte(src.PtrAt($9EE)^) := 1;
			// Hack: replace waitkey.noclear with plain waitkey, also remove a space
			'SK_105': word(src.PtrAt($715)^) := $0120;
			'SK_108':
			begin
				// Hack: remove a hyphen
				txt := 'tution scandal.  ';
				move(txt[1], src.PtrAt($470)^, length(txt));
			end;
			// Hack: add a waitkey
			'SK_112': word(src.PtrAt($BB9)^) := $0121;
			'SK_121':
			begin
				// Hack: add a waitkey that's supposed to be there
				txt := #1'%001:';
				move(txt[1], src.PtrAt($2D0)^, length(txt));
			end;
			// Hack: change a choice variable to use a unique tracking variable
			'SK_212': byte(src.PtrAt($1BA)^) := $10;
			// Hack: separate two dialogue lines.
			'SK_221': if src.ReadByteFrom($3DB) = $A then byte(src.PtrAt($3DB)^) := 1;
			'SK_222':
			if src.ReadByteFrom($1338) = ord('-') then begin
				// Hack: remove a hyphen
				txt := 'ment near my place.  ';
				move(txt[1], src.PtrAt($1338)^, length(txt));
			end;
			// Hack: fix bug in original, jumps to exit instead of start of string
			'SK_213': inc(byte(src.PtrAt($907)^));
			// Hack: remove stray waitkey.
			'SK_520': if src.ReadByteFrom($37A) = 1 then byte(src.PtrAt($37A)^) := $A;
			'SK_737':
			begin
				// Hack: cut common ending sequence, replace with call to ENDINGS (2)
				txt := #1#$C#3#$FF#2#4'ENDINGS'#0;
				fillbyte(src.PtrAt($88E)^, 1996, 0);
				move(txt[1], src.PtrAt($88C)^, length(txt));
			end;
			'SK_738':
			begin
				// Hack: change a final IF into a catch-all ELSE
				dword(src.PtrAt($123)^) := $00162336;
				// Hack: eliminate unnecessary 5-second sleeps
				txt := #1#$B#$36#$7C#$19#0;
				move(txt[1], src.PtrAt($4BE)^, length(txt));
				move(txt[1], src.PtrAt($84D)^, length(txt));
				move(txt[1], src.PtrAt($BDD)^, length(txt));
				move(txt[1], src.PtrAt($F6C)^, length(txt));
				move(txt[1], src.PtrAt($12BF)^, length(txt));
				move(txt[1], src.PtrAt($161B)^, length(txt));
				move(txt[1], src.PtrAt($1974)^, length(txt));
				// Hack: cut common ending sequence, replace with call to ENDINGS (1)
				txt := #$C#3#$FF#1#4'ENDINGS'#0;
				move(txt[1], src.PtrAt($197C)^, length(txt));
				src.buffySize := $197C + length(txt);
			end;
			'SK_743':
			begin
				// Hack: add a missing waitkey
				txt := #1'%001:';
				move(txt[1], src.PtrAt($6B9)^, length(txt));
				// Hack: cut common ending sequence, replace with call to ENDINGS (0)
				txt := #$C#3#$FF#0#4'ENDINGS'#0;
				move(txt[1], src.PtrAt($756)^, length(txt));
				src.buffySize := $756 + length(txt);
			end;
		end;

		gid.SanShimai98:
		begin
			// Hack: replace a newline with a waitkey
			if scriptname = 'SK_106' then byte(src.PtrAt($1BF)^) := 1;
			// Hack: fix a jump into the middle of a string
			if scriptname = 'SK_406' then word(src.PtrAt($A1)^) := $5F4;
		end;

		gid.Setsujuu:
		begin
			// Hack: change erroneous choice.off to a repeat of previous
			// Maybe incorrect? Check again.
			if scriptname = 'SMG_S031' then byte(src.PtrAt($17B)^) := 2;
		end;

		gid.Tasogare:
		begin
			// Hack: streamline 0F ending minicode segments
			if scriptname = 'TA_ED01' then begin
				byte(src.PtrAt($637)^) := 0;
				byte(src.PtrAt($C46)^) := 0;
				src.buffySize := src.buffySize - 1;
			end;
			if scriptname = 'TA_ED02' then begin
				txt := #$F#1'YE_041'#0#3;
				move(txt[1], src.PtrAt($600)^, length(txt));
				dword(src.PtrAt($7C7)^) := $8103040F;
				byte(src.PtrAt($830)^) := 0;
				byte(src.PtrAt($EC5)^) := 0;
				txt[8] := '0';
				move(txt[1], src.PtrAt($134A)^, length(txt));
				byte(src.PtrAt($185F)^) := 0;
			end;
			if scriptname = 'TA_00DG' then begin
				// Hack: fix incorrect variable reference, triggers some new strings
				inc(byte(src.PtrAt($2C)^));
				// Hack: add a missing exit
				dword(src.PtrAt($84E)^) := $00000300;
			end;
			if scriptname = 'TA_0801' then begin
				// Hack: correct 0B-4B item count, enables previously unreachable code, although all that does is an
				// 11-04 and return to the same choices.
				byte(src.PtrAt($DA6)^) := 3;
				// Hack: avoid a double-waitkey by jumping a byte further
				inc(byte(src.PtrAt($471)^));
			end;
			// Hack: add a missing script name
			if scriptname = 'TA_1402' then begin
				src.buffySize := src.buffySize + 3;
				txt := 'TA_1100'#0;
				move(txt[1], src.PtrAt($134A)^, length(txt));
			end;
			// Hack: eliminate a troublesome lone linebreak
			if scriptname = 'TA_2403' then word(src.PtrAt($413)^) := $0413;
			// Hack: correct 0B-4B item count
			if scriptname = 'TA_3803' then dec(byte(src.PtrAt($2DBE)^));
			if scriptname = 'TA_4001' then begin
				// Hack: correct 0B-4B item count, enables previously unreachable code
				byte(src.PtrAt($1580)^) := 3;
				// Hack: correct bug in said unreachable code
				for d := $16A0 to $16B1 do
					byte(src.PtrAt(d)^) := src.ReadByteFrom(d + 1);
			end;
			// Hack: eliminate a troublesome lone linebreak
			if scriptname = 'TA_4908' then dword(src.PtrAt($901)^) := $01011620;
		end;

		gid.Transfer98:
		begin
			// Hack: separate dialogue lines
			if scriptname = 'TEN_S007' then byte(src.PtrAt($1E4)^) := 1;
			// Hack: fix graphic type
			if scriptname = 'TEN_S069' then byte(src.PtrAt($33)^) := $4E;
			// Hack: fix graphic type
			if scriptname = 'TEN_S105' then byte(src.PtrAt($51)^) := $4E;
			// Hack: fix graphic type
			if scriptname = 'TEN_S106' then byte(src.PtrAt($3E)^) := $4E;
		end;

		gid.Vanish:
		begin
			// Hack: remove unaccessable code
			if scriptname = 'EB020' then src.buffySize := $6B;
			// Hack: remove unaccessable code
			if scriptname = 'EB040' then src.buffySize := $71;
			// Hack: remove unaccessable code
			if scriptname = 'EB045' then src.buffySize := $68;
			// Hack: remove unaccessable code
			if scriptname = 'EB046' then src.buffySize := $68;
			// Hack: remove unaccessable code
			if scriptname = 'EB049' then src.buffySize := $68;
		end;

	end;
end;
{$endif enable_decomp_hacks}

function _CapSize(const intxt : UTF8string) : UTF8string;
// Capitalizes the first character and lowercases the rest.
// Also a ton of special cases. These are choice verbs or subjects, which in the originals are in all capitals.
var i : dword;

	{$ifdef enable_decomp_hacks}
	procedure _UpInitial(const list : array of string);
	var s : string;
		j : longint;
	begin
		for s in list do begin
			j := pos(s, result);
			if j <> 0 then begin
				if s[1] = ' ' then inc(j);
				result[j] := upcase(result[j]);
				exit;
			end;
		end;
	end;
	{$endif enable_decomp_hacks}

begin
	result := Trim(intxt);
	if result = '' then exit;

	// Capitalise appropriately if English text.
	case game of
		gid.SanShimai, gid.Runaway, gid.Sakura:
		begin
			{$ifdef enable_decomp_hacks}
			result := upcase(result[1]) + lowercase(copy(result, 2));
			case game of
				gid.SanShimai:
				begin
					if (scriptname = 'SK_215') or (scriptname = 'SK_315')
					or (scriptname = 'SK_614') or (scriptname = 'SK_517')
					then begin
						// Make the number selections more verbose in this script, for clarity.
						if result[1] in ['1'..'3'] then result := ([
							'Focus harder', 'Ignore being watched', 'Enjoy being watched'])
							[ord(result[1]) - 49];
					end;
					if result = 'Kaisan corp.' then result := 'Kaisan Corporation' else
					if result = 'Tairiku indust.' then result := 'Tairiku Industries' else
					if result = 'R. gymnastics' then result := 'Rhythmic gymnastics' else

					_UpInitial([
						'okamura', 'emi', 'risa', 'yuki', ' mana', 'eiichi', 'taihei',
						'zaibat', 'akimoto', 'makino', 'uchimura', 'fujimura',
						' i ', ' i''m', 'd card']);
				end;

				gid.Runaway:
				begin
					if scriptname = 'MT_0208' then begin
						// Make the number selections more verbose in this script, for clarity.
						if result[1] in ['1'..'5'] then result := ([
							'She was going inside?', 'She slept there?', 'She was forced there?',
							'Just a coincidence?', 'She lives there?'])
							[ord(result[1]) - 49];
					end;
					if (result = 'Vcr') or (result = 'Tv') then result := upcase(result) else
					if result = 'Projector tv' then begin result[11] := 'T'; result[12] := 'V'; end else
					if copy(result, 1, 3) = 'S&m' then result[3] := 'M' else

					_UpInitial([
						'choko', 'yumirin', 'fujiko', 'yume', 'buddha', 'ninja', 'snake', 'completion', 'bait', ' i?']);
				end;

				gid.Sakura:
				begin
					if result = 'V-ninja ii' then result := 'Virtual Ninja II' else
					if result = 'New years day' then result := 'New Year''s Day' else
					if result = 'Theighs' then result := 'Thighs' else
					_UpInitial(['makoto', 'ruri', 'denki', 'meimi']);
				end;
			end;

			// Common items in English.
			// Hack: sometimes two separate "go" verbs in same script, or two "yes" and two "no".
			if (result = 'G0') or (result = 'Pr0ceed') then result := 'Move'
			else if result = 'Ye5' then result := 'Yes!'
			else if result = 'N0' then result := 'No!';

			// trailing capital: Plan A, Plan B... 1-A, 2-B, 3-D...
			if (result.Length >= 3) and (result[result.Length - 1] in [' ','-']) then
				result[result.Length] := upcase(result[result.Length]);
			// single-quoted speech
			if result[1] = '''' then result[2] := upcase(result[2]);
			// forward slash, suggesting multiple speakers together
			i := pos('/', result);
			if i <> 0 then result[i + 1] := upcase(result[i + 1]);
			// period+space, probably "Mr. something" or "Dr. Negishi"
			i := pos('. ', result);
			if i <> 0 then result[i + 2] := upcase(result[i + 2]);
			// someone & someone else
			i := pos(' & ', result);
			if i <> 0 then result[i + 3] := upcase(result[i + 3]);
			{$endif enable_decomp_hacks}
		end;

		else begin
			// Choice strings in Japanese games need to be converted to UTF-8.
			result := GetUTF8(@result[1], result.Length);
			if (result <> '') and (result[1] = #0) then raise DecompException.Create(copy(result, 2));

			case game of
				gid.Setsujuu:
				begin
					// Trim unimportant spaces from Japanese text. Setsujuu particularly uses spaces to fake centering.
					i := result.Length;
					while i <> 0 do begin
						// simple space
						if result[i] = ' ' then
							result := copy(result, 1, i - 1) + copy(result, i + 1)
						// ideographic space
						else if (i + 1 < result.Length) and (byte(result[i]) = $E3)
							and (byte(result[i + 1]) = $80) and (byte(result[i + 2]) = $80)
							then result := copy(result, 1, i - 1) + copy(result, i + 3);
						dec(i);
					end;
				end;
			end;
		end;
	end;
end;

procedure _WriteThwomp(numthwomp : byte);
// Writes out 1 + numthwomp vertical bashes.
var i : byte;
begin
	with ScriptWriter do begin
		WriteBufLn('gfx.bash time:1200 freq:111000 amp:0.13 angle:0.5');
		i := numthwomp;
		while i <> 0 do begin
			dec(i);
			WriteBufLn('sleep ' + strdec(240 + byte(numthwomp - i) shl 7));
			WriteBufLn('gfx.bash ' + strdec(1024 + byte(numthwomp - i) shl 8) + ' 111000 0.14 0.5');
		end;
	end;
end;

procedure _WriteBash(numbash : byte);
// Writes out 1 + numbash mostly horizontal bashes.
begin
	with ScriptWriter do begin
		WriteBufLn('$angle := rnd 8192 + 4096');
		WriteBufLn('if rnd 2 = 0 then $angle := -$angle end');
		WriteBufLn('$amp := rnd 2048 + 5000');
		WriteBufLn('gfx.bash time:1200 freq:111000 amp:$amp angle:$angle');
		while numbash <> 0 do begin
			WriteBufLn('sleep ' + strdec(480 + numbash shl 7));
			WriteBufLn('$angle := -$angle + rnd 2000 - 1000');
			WriteBufLn('$amp := rnd 2048 + 6000');
			WriteBufLn('gfx.bash 1200 111000 $amp $angle');
			dec(numbash);
		end;
	end;
end;

procedure _AutoloadAnims(const aninamu : string);
// 3sis-era games automatically load animations when a sprite is loaded.
// Some sprites have no animations, most have 1, a few have more than 1.
// If the animation metadata comes from the executable, can check which ones exist by name, otherwise must hardcode.
var a : byte = 0;
	c : char = 'A';
begin
	if game = gid.AngelsC1 then c := 'E';
	while GetGraphicFile(aninamu + c + char(48 + a)) <> 0 do begin
		ScriptWriter.WriteBufLn('show ' + aninamu + c + strdec(a));
		inc(a);
	end;
end;

const dynamicTB000 = 'show "|000F" name:TB_000 overlay w:1.0 h:1.0';

procedure _ShowPicture(index : byte; sleep : boolean = TRUE);
// Shows one of the pictures defined in the header. Used by v2 engine only.
begin
	with pictureList[index] do with ScriptWriter do begin
		if blackoutFirst then begin
			WriteBufLn('transition interlaced');
			WriteBufLn(dynamicTB000);
			WriteBufLn('sleep');
			isBlackedOut := TRUE;
		end;
		WriteBufLn('transition ' + strdec(byte(transitionStyle)));
		if isBlackedOut then WriteBufLn('gfx.remove TB_000');
		// 06 vs 11-02 sprite persistence is not handled correctly, needs more research; see mt_0821 for example
		if NOT persistSprites then WriteBufLn('gfx.clearkids');
		if gfxName <> 'TB_000' then begin
			WriteBuf('show ' + gfxName);
			if isSprite then WriteBufLn('') else WriteBufLn(' bkg');
		end
		else WriteBufLn(dynamicTB000);
		if sleep then WriteBufLn('sleep');

		_AutoloadAnims(gfxName);

		isBlackedOut := (gfxName = 'TB_000');
	end;
end;

procedure _EnableDrawGfx;
begin
	with drawGfx do with ScriptWriter do begin
		if pending then exit;
		pending := TRUE;
		drawCmdCount := 0;
		transition := '!';
	end;
end;

procedure _FinishDrawGfx;
var m : byte;
begin
	with drawGfx do with ScriptWriter do begin
		Assert(pending);
		pending := FALSE;
		if drawCmdCount = 0 then exit;
		if transition <> '!' then WriteBuf('transition ' + transition + LineEnding);
		m := 0;
		while dword(m + 1) < drawCmdCount do begin
			WriteBuf(drawCmd[m] + LineEnding);
			inc(m);
		end;
		WriteBuf(drawCmd[m]);
		if transition <> '!' then WriteBuf(LineEnding + 'sleep');
		WriteBufLn('');
	end;
end;

procedure _0B;
// Commands for local variable operations.
var m : dword;
	com : byte;
begin
	inc(loader.readp);
	m := loader.RemainingBytes;
	if bitness = 2 then begin
		// 16-bit references
		j := loader.ReadWordFrom(loader.ofs + 0); // yy
		if m >= 4 then l := loader.ReadWordFrom(loader.ofs + 2); // zz
		if m >= 6 then m := loader.ReadWordFrom(loader.ofs + 4); // aa
	end
	else begin
		// 8-bit references
		j := loader.ReadByteFrom(loader.ofs + 0); // yy
		if m >= 2 then l := loader.ReadByteFrom(loader.ofs + 1); // zz
		if m >= 3 then m := loader.ReadByteFrom(loader.ofs + 2); // aa
	end;

	com := loader.ReadByteFrom(loader.ofs - 1);
	with ScriptWriter do case com of
		// xx
		1:
		begin
			inc(loader.readp, bitness + bitness);
			AddLocalVar(j);
			if bitness = 1 then
				longint(l) := shortint(l)
			else
				longint(l) := integer(l);
			WriteBuf('$v' + strhex(j));
			if longint(l) >= 0 then
				WriteBufLn(' += ' + strdec(l))
			else
				WriteBufLn(' -= ' + strdec(-longint(l)));
		end;

		2:
		begin
			inc(loader.readp, bitness + bitness);
			WriteBufLn('$v' + strhex(j) + ' += $v' + strhex(l));
		end;

		3:
		begin
			inc(loader.readp, bitness + bitness);
			WriteBufLn('$v' + strhex(j) + ' -= $v' + strhex(l));
		end;

		4:
		begin
			inc(loader.readp, bitness + bitness);
			WriteBufLn('$v' + strhex(j) + ' := $v' + strhex(l));
		end;

		5:
		begin
			AddLocalVar(j);
			inc(loader.readp, bitness + bitness);
			WriteBufLn('$v' + strhex(j) + ' := ' + strdec(l));
		end;

		6:
		begin
			inc(loader.readp, dword(bitness * 3));
			WriteBufLn('$v' + strhex(j) + ' := $v' + strhex(l) + ' - $v' + strhex(m));
		end;

		7:
		begin
			inc(loader.readp, dword(bitness * 3));
			WriteBufLn('$v' + strhex(j) + ' := $v' + strhex(l) + ' - ' + strdec(m));
		end;

		8:
		begin
			inc(loader.readp, dword(bitness * 3));
			WriteBufLn('$v' + strhex(j) + ' := $v' + strhex(l) + ' and $v' + strhex(m));
		end;

		9:
		begin
			inc(loader.readp, dword(bitness * 3));
			WriteBufLn('$v' + strhex(j) + ' := $v' + strhex(l) + ' or $v' + strhex(m));
		end;

		$A:
		begin
			inc(loader.readp, bitness);
			WriteBuf('if $v' + strhex(j) + ' == 0 then ');
			if hasChoices then
				WriteBufLn('choice.go end')
			else
				WriteBufLn('return end');
		end;

		$B,$C:
		begin
			WriteBuf('$v' + strhex(l) + ' := $v' + strhex(l));
			inc(loader.readp, bitness + bitness);
			dec(j);
			while j <> 0 do begin
				if bitness = 1 then
					m := loader.ReadByte
				else
					m := loader.ReadWord;

				if com = $B then
					WriteBuf(' AND $v' + strhex(m))
				else
					WriteBuf(' OR $v' + strhex(m));
				dec(j);
			end;
			WriteBufLn('');
		end;

		$F, $10:
		begin
			WriteBuf(strcat('//dummy $0B-& var/val $& interacts with vars', [com, j]));
			inc(loader.readp, bitness + bitness);
			while l <> 0 do begin
				if bitness = 1 then
					WriteBuf(' $v' + strhex(loader.ReadByte))
				else
					WriteBuf(' $v' + strhex(loader.ReadWord));
				dec(l);
			end;
			WriteBufLn('');
		end;

		$14:
		begin
			inc(loader.readp, bitness + bitness);
			WriteBufLn('$v' + strhex(j) + ' := rnd ' + strdec(l));
		end;

		$15:
		begin
			inc(loader.readp, bitness);
			WriteBufLn('//dummy $0B-15 play song var ' + strhex(j));
		end;

		$1D:
		begin
			inc(loader.readp, dword(bitness * 3));
			WriteBufLn('//dummy $0B 1D // $' + strhex(j) + ' $' + strhex(l) + ' $' + strhex(m));
		end;

		$32..$35:
		begin
			WriteBuf('if $v' + strhex(j) + ' ');
			case com of
				$32: WriteBuf('>');
				$33: WriteBuf('==');
				$34: WriteBuf('<');
				$35: WriteBuf('<>');
			end;
			inc(loader.readp, bitness);
			l := loader.ReadWord;
			txt := strhex(l, 4);
			WriteBufLn(' 0 then goto "' + txt + '" end');
			AddJump(l);
		end;

		$36:
		begin
			txt := strhex(loader.ReadWordFrom(loader.ofs), 4);
			WriteBufLn('goto "' + txt + '"');
			AddJump(loader.ReadWord);
			if (loader.readp < loader.endp) and (loader.ReadByteFrom(loader.ofs) <> 0) then
				WriteBufLn('');
		end;

		$37:
		begin
			i := 0; m := $FFFF;
			inc(loader.readp, bitness);
			WriteBuf('// 0B-37 multijump' + LineEnding + 'casegoto $v' + strhex(j) + ' ."');
			repeat
				// Checks for undetected array ends.
				case game of
					gid.Hohoemi: begin
						if scriptname = 'M_H' then
							if (loader.ofs = $F66) or (loader.ofs = $1715) then break;
						if scriptname = 'K_H' then
							if loader.ofs = $1828 then break;
					end;
					gid.Tasogare: begin
						l := valx(copy(scriptname, 4, 4));
						if (l = 4831) and (loader.ofs = $2C)
						or (l = 4252) and (loader.ofs = $2E)
						or (l = 4250) and (loader.ofs = $2E)
						or (l = 2101) and (loader.ofs = $1D34)
						or (l = 1953) and (loader.ofs = $2E)
						then break;
					end;
				end;
				l := loader.ReadWordFrom(loader.ofs);
				// To find end of array, must test each new word address for validity. Also, see if we've
				// hit the closest of the jump addresses given, since that means the array ended already.
				if (l < bufsection.scriptPtr) or (l >= loader.buffySize) or (loader.ofs = m) then break;
				if (l < m) and (l > loader.ofs) then m := l;
				txt := strhex(l, 4);
				if i <> 0 then WriteBuf(':');
				WriteBuf(txt);
				inc(i); inc(loader.readp, 2);

				AddJump(l);

			until (loader.readp >= loader.endp);
			WriteBufLn('"' + LineEnding);
		end;

		$38:
		begin
			txt := strhex(loader.ReadWordFrom(loader.ofs), 4);
			AddJump(loader.ReadWord);

			WriteBufLn('call "' + txt + '" // 0B-38');
			if loader.ReadByteFrom(loader.ofs) <> 0 then WriteBufLn('');
		end;

		$39:
		begin
			inc(loader.readp, bitness);
			l := loader.ReadWord;
			m := 0;
			WriteBuf('casecall $v' + strhex(j) + ' ."');
			while l <> $FFFF do begin
				txt := strhex(l, 4);
				if m <> 0 then WriteBuf(':');
				WriteBuf(txt);
				AddJump(l);

				l := loader.ReadWord;
				inc(m);
			end;
			WriteBufLn('" // 0B-39');
		end;

		$46, $47, $4A:
		begin
			// Followed by l word addresses pointing to strings; get user choice of them, put result in var j.
			WriteBuf('choice.reset // 0B-' + strhex(com) + ' immediate choice' + LineEnding);
			inc(loader.readp, bitness + bitness);

			l := l shl 16; // stick for loop tracking variable in hiword
			while l and $FFFF < l shr 16 do begin
				i := loader.ReadWordFrom(loader.ofs + (l and $FFFF) * 2);

				txt := loader.ReadStringFrom(i + 1);
				if txt = '' then begin
					// fetch string from cache
					for m := high(stringCache) downto 0 do
						if word((@stringCache[m][62])^) = word(i) then break;
					txt := stringCache[m];
				end
				else begin
					// cache string, overwrite with zeroes in code
					for m := high(stringCache) downto 0 do
						if stringCache[m] = '' then break;
					stringCache[m] := txt;
					word((@stringCache[m][62])^) := word(i);
					fillbyte(loader.PtrAt(i)^, length(txt) + 2, 0);
				end;

				txt := _CapSize(txt);
				WriteBuf('choice.set "' + txt + '"' + LineEnding);
				inc(l);
			end;
			l := l shr 16;
			inc(loader.readp, l + l);
			WriteBufLn('$v' + strhex(j) + ' := (choice.get)');
		end;

		$48:
		begin
			inc(loader.readp, bitness);
			WriteBufLn('$v' + strhex(j) + ' := (call sub.guibuttons) // 0B-48');
		end;

		$49:
		begin
			// Four variable numbers, followed by l word addresses pointing to strings; get user choice of
			// them, put result in var j. Masked choices must still count in index numbering!
			inc(loader.readp, bitness * 3);
			WriteBuf('choice.reset // 0B-49 bitmasked immediate choice' + LineEnding);
			// m is already the 3rd variable, but it's always set to 0 and is never checked afterward, so
			// ignore it.
			// we grab the 4th var into bufsection.endPtr. It's the choice bitmask.
			if bitness = 1 then
				bufsection.endPtr := loader.ReadByte
			else
				bufsection.endPtr := loader.ReadWord;

			l := l shl 16; // stick for loop tracking variable in hiword
			while l and $FFFF < l shr 16 do begin
				i := loader.ReadWordFrom(loader.ofs + (l and $FFFF) * 2);

				txt := loader.ReadStringFrom(i + 1);
				if txt = '' then begin
					// fetch string from cache
					for m := high(stringCache) downto 0 do
						if word((@stringCache[m][62])^) = word(i) then break;
					txt := stringCache[m];
				end
				else begin
					// cache string, overwrite with zeroes in code
					for m := high(stringCache) downto 0 do
						if stringCache[m] = '' then break;
					stringCache[m] := txt;
					word((@stringCache[m][62])^) := word(i);
					fillbyte(loader.PtrAt(i)^, length(txt) + 2, 0);
				end;

				txt := _CapSize(txt);
				WriteBuf('choice.set "' + txt + '"' + LineEnding +
					strcat('if $v& AND % == 0 then (choice.off "%") end' + LineEnding,
					[bufsection.endPtr, 1 shl (l and $FFFF), txt]));
				inc(l);
			end;
			l := l shr 16;
			inc(loader.readp, l + l);
			WriteBufLn('$v' + strhex(j) + ' := (choice.get)');
		end;

		$4B:
		begin
			// followed by yy pairs of word addresses
			inc(loader.readp, bitness);
			WriteBuf('choice.reset // 0B-4B immediate choice' + LineEnding);
			while j <> 0 do begin
				l := loader.ReadWord;
				txt := loader.ReadStringFrom(l + 1);
				if txt = '' then begin
					// fetch string from cache
					for m := high(stringCache) downto 0 do
						if word((@stringCache[m][62])^) = word(l) then break;
					txt := stringCache[m];
				end
				else begin
					// cache string, overwrite with zeroes in code
					for m := high(stringCache) downto 0 do
						if stringCache[m] = '' then break;
					stringCache[m] := txt;
					word((@stringCache[m][62])^) := word(l);
					fillbyte(loader.PtrAt(l)^, length(txt) + 2, 0);
				end;
				txt := _CapSize(txt);
				l := loader.ReadWord;
				WriteBuf('choice.set "' + txt + '" ."');
				txt := strhex(l, 4);
				WriteBuf(txt + '"' + LineEnding);
				AddJump(l);

				dec(j);
			end;
			WriteBufLn('choice.go');
		end;

		else raise DecompException.Create(
			strcat('Unknown $B subcode $& @ $&', [loader.ReadByteFrom(loader.ofs - 1), loader.ofs - 2]));
	end;
end;

procedure _DoTextOutput;
var printstr : UTF8string = '';
	printofs, j, l : dword;
	u8c : string[4];
	maybetitle : boolean = TRUE;
begin
	printofs := 1;
	setlength(printstr, 256);
	dec(loader.readp); // step back to the character that triggered _DoTextOutput

	{$ifdef enable_decomp_hacks}
	case game of
		gid.Sakura:
		begin
			// Hack: don't interpret colon as a dialogue title
			if (scriptname = 'CSA08') and (loader.ofs = $92F) then maybetitle := FALSE;
		end;
	end;

	// Special case: single dot followed by a brief sleep or something.
	// These should be marked as global since they're ubiquitous.
	if (loader.RemainingBytes >= 3) and (BEtoN(word(loader.readp^)) = $8145) and (byte((loader.readp + 2)^) < $20)
	then ScriptWriter.WriteBuf(':');
	ScriptWriter.WriteBuf('"');

	case game of
		gid.AngelsC1, gid.AngelsC2, gid.Eden, gid.FromH, gid.Majokko, gid.Runaway98, gid.Sakura98, gid.SanShimai98,
		gid.Setsujuu, gid.SexyParfait, gid.Tasogare, gid.Transfer98, gid.Vanish:
		begin
			// Catch Shift-JIS dialogue titles, written as "[name]:"
			if BEtoN(word(loader.readp^)) = $8179 then begin
				j := 2;
				while (j <= 20) and (BEtoN(word((loader.readp + j)^)) <> $817A) do begin
					l := byte((loader.readp + j)^); inc(j);
					// double-byte?
					if l in [$81..$84,$88..$9F,$E0..$EA] then begin
						l := l shl 8 + byte((loader.readp + j)^);
						inc(j);
					end;
					// add each character to printstr, except in-text spaces in Setsujuu
					if (game <> gid.Setsujuu) or (l <> $8140) then begin
						u8c := GetUTF8(l);
						if printofs + 8 >= printstr.Length then setlength(printstr, printstr.Length + 128);
						move(u8c[1], printstr[printofs], length(u8c));
						inc(printofs, length(u8c));
					end;
				end;

				if BEtoN(word((loader.readp + j)^)) = $817A then begin
					inc(loader.readp, j + 2);

					if copy(printstr, 1, 2) = '%0' then
						ScriptWriter.WriteBuf('\$s' + strdec(valx(printstr)) + ';')
					else
						ScriptWriter.WriteBuf(printstr, printofs - 1);
					ScriptWriter.WriteBuf('\:');
					if BEtoN(word(loader.readp^)) = $8146 then inc(loader.readp, 2) // ":"
					else if byte(loader.readp^) = $3A then inc(loader.readp); // ":"
					if byte(loader.readp^) = $A then inc(loader.readp); // linebreak
					maybetitle := FALSE;
					implicitWaitkey := 2;
				end;
			end;
		end;
	end;

	// Eliminate useless starting spaces.
	if game = gid.FromH then begin
		if (BEtoN(word(loader.readp^)) = $8140) and (BEtoN(word((loader.readp + 2)^)) <> $8140) then
			inc(loader.readp, 2);
	end;
	{$endif enable_decomp_hacks}

	printofs := 1;
	j := byte(loader.readp^);
	while j in [$0A, $20..$EF] do begin

		// Expand the string variable if necessary.
		if printofs + 8 >= printstr.Length then setlength(printstr, printstr.Length + 128);

		{$ifdef enable_decomp_hacks}
		// Catch "--" and replace it with a long dash.
		case game of
			gid.SanShimai, gid.Runaway, gid.Sakura:
			if word(loader.readp^) = $2D2D then begin
				byte(printstr[printofs]) := $E2; inc(printofs);
				byte(printstr[printofs]) := $80; inc(printofs);
				byte(printstr[printofs]) := $93; inc(printofs);
				inc(loader.readp, 2);
				j := byte(loader.readp^);
				continue;
			end;
		end;
		{$endif enable_decomp_hacks}

		case j of
			$0A: // linefeed CR
			begin
				if (game = gid.SanShimai) or (game = gid.Runaway) or (game = gid.Sakura) then begin
					// Reading as latin alphabet.
					// write nothing if there's a hyphen or space already.
					if (char((loader.readp - 1)^) <> '-')
					and (char((loader.readp - 1)^) <> ' ')
					and (char((loader.readp + 1)^) <> ' ')
					then begin
						// replace it with a space in most cases, unless it's the string's last character.
						if (byte((loader.readp + 1)^) < 32) then begin
							if printofs > 1 then begin
								printstr[printofs] := '\'; inc(printofs);
								printstr[printofs] := 'n'; inc(printofs);
								inc(loader.readp);
								break;
							end;
						end
						else begin
							printstr[printofs] := ' '; inc(printofs);
						end;
					end;
				end
				else begin
					// Reading as shift-JIS.
					printstr[printofs] := '\'; inc(printofs);
					printstr[printofs] := 'n'; inc(printofs);
					inc(loader.readp);
					break;
				end;
			end;

			// Double-quotes and backslashes must be escaped.
			$22,$5C:
			begin
				printstr[printofs] := '\'; inc(printofs);
				printstr[printofs] := char(j); inc(printofs);
			end;

			{$ifdef enable_decomp_hacks}
			// Change string var %00x to use our escape code + end mark.
			$25:
			begin
				l := LEtoN(dword(loader.readp^));
				if l and $FFFF = $3025 then begin
					l := ((l shr 24) and $F) + ((l shr 16) and $F * 10);
					inc(loader.readp, 3);
					printstr[printofs] := '\'; inc(printofs);
					printstr[printofs] := '$'; inc(printofs);
					printstr[printofs] := 's'; inc(printofs);
					u8c := strdec(l);
					move(u8c[1], printstr[printofs], length(u8c));
					inc(printofs, length(u8c));
					printstr[printofs] := ';'; inc(printofs);
					// Hack: If there's a :- after the string, skip the stray colon.
					if (loader.readp + 2 < loader.endp) and (word((loader.readp + 1)^) = BEtoN(word($3A2D))) then
						inc(loader.readp);
				end
				else begin
					printstr[printofs] := char(j); inc(printofs);
				end;
			end;

			// Change ":  " and ": " to a title identifier mark, but only if we are still close to the line beginning,
			// and there hasn't been a " a " in the string so far, and there haven't been many spaces, or a bracket.
			$3A:
			begin
				maybetitle := maybetitle and (char((loader.readp + 1)^) = ' ') and (printofs < 33) and (printstr[1] <> '(');
				if maybetitle then
					maybetitle := pos(' a ', copy(printstr, 1, printofs)) = 0;
				if maybetitle then
					maybetitle := copy(printstr, 1, 3) <> 'HIT';
				if maybetitle then
					maybetitle := copy(printstr, 1, 4) <> 'And ';
				if maybetitle then
					maybetitle := copy(printstr, 1, 4) <> 'Sudd';
				if maybetitle then
					maybetitle := copy(printstr, 1, 5) <> 'Just ';
				if maybetitle then
					maybetitle := copy(printstr, 1, 7) <> 'Caution';
				if maybetitle then begin
					j := 0;
					for l := 1 to printofs - 1 do if printstr[l] = ' ' then inc(j);
					maybetitle := j <= 3;
				end;
				if maybetitle then begin
					maybetitle := FALSE;
					inc(loader.readp);
					while (loader.readp + 1 < loader.endp) and (char((loader.readp + 1)^) = ' ') do inc(loader.readp);
					if printstr[1] = ' ' then
						ScriptWriter.WriteBuf(copy(printstr, 2, printofs - 2))
					else
						ScriptWriter.WriteBuf(printstr, printofs - 1);
					ScriptWriter.WriteBuf('\:');
					printofs := 1;
					implicitWaitkey := 2;
				end
				else begin
					printstr[printofs] := ':'; inc(printofs);
				end;
			end;
			{$endif enable_decomp_hacks}

			// Double-byte JIS.
			$81..$9F, $E0..$EF:
			begin
				inc(loader.readp);
				u8c := GetUTF8(j shl 8 + byte(loader.readp^));
				move(u8c[1], printstr[printofs], length(u8c));
				inc(printofs, length(u8c));
			end;

			// Single-byte katakana.
			$A1..$DF:
			begin
				u8c := GetUTF8(j);
				move(u8c[1], printstr[printofs], length(u8c));
				inc(printofs, length(u8c));
			end;

			// Single-byte plain ASCII.
			else begin
				printstr[printofs] := char(j); inc(printofs);
			end;
		end;

		inc(loader.readp);
		{$ifdef enable_decomp_hacks}
		// Jump into the middle of a string... add a string break.
		if (game = gid.AngelsC1) and (scriptname = 'SCB12') and (loader.ofs = $20C)
		or (game = gid.AngelsC2) and (scriptname = 'S2_009') and (loader.ofs = $18B)
		or (game = gid.Vanish) and (scriptname = 'EB049_01') and (loader.ofs = $9A6)
		then break;
		{$endif enable_decomp_hacks}
		j := byte(loader.readp^);
	end;

	// If there are > 4 spaces on the left, assume the string is meant to be centered.
	if (printofs > 4) and (dword((@printstr[1])^) = $20202020) then begin
		j := 4;
		while (j < printofs) and (printstr[j] = ' ') do inc(j);
		printstr[1] := '\'; printstr[2] := 'C';
		MemCopy(@printstr[j], @printstr[3], printofs - j);
		dec(printofs, j - 3);
	end;

	// Write out the print command, unless it's an empty string.
	dec(printofs);
	with ScriptWriter do if printofs <> 0 then begin
		// If a non-trivial printable ends in a comma, assume it should be a full stop.
		if (printofs > 4) and (printstr[printofs] = ',') then printstr[printofs] := '.';

		WriteBuf(printstr, printofs);
		WriteBufLn('"');
		implicitWaitkey := 2;
	end
	else currentLen := 0;
end;

procedure _HandleHeaders;
var i, j, m : dword;
begin
	case game of
		gid.Maririn:
		begin
			bufsection.resultsPtr := word(loader.readp^);
			bufsection.scriptPtr := word((loader.readp + word((loader.readp + 2)^) )^);
			ScriptWriter.WriteBufLn('// ID list:');
			loader.ofs := bufsection.resultsPtr;
			l := loader.ReadWordFrom(bufsection.resultsPtr);
			while (loader.readp < loader.endp) and (loader.ofs < l) do begin
				i := loader.ReadWord; // current word pointer
				if loader.ofs < l then
					j := word(loader.readp^)
				else
					j := bufsection.scriptPtr;
				dec(j, i);
				ScriptWriter.WriteBuf('//');
				for m := 1 to j do begin
					txt := strhex(loader.ReadByteFrom(i));
					if length(txt) = 1 then txt := '0' + txt;
					ScriptWriter.WriteBuf(' ' + txt);
					inc(i);
				end;
				ScriptWriter.WriteBufLn('');
			end;
			loader.ofs := bufsection.scriptPtr;
			exit;
		end;

		gid.Deep:
		begin
			// Read constant pointers
			bufsection.resultsPtr := loader.ReadWordFrom(0);
			bufsection.scriptPtr := loader.ReadWordFrom(2);
			bufsection.picsPtr := loader.ReadWordFrom(4);
			bufsection.endPtr := loader.ReadWordFrom(6);
			if bufsection.resultsPtr in [0,6,8] = FALSE then
				raise DecompException.Create('Script header starts with $' + strhex(bufsection.resultsPtr) + '??');

			if bufsection.resultsPtr = 6 then begin
				// alternative tiny header skips first array
				bufsection.endPtr := bufsection.picsPtr;
				bufsection.picsPtr := bufsection.scriptPtr;
				bufsection.scriptPtr := bufsection.resultsPtr;
			end
			else if bufsection.resultsPtr = 0 then begin
				// alternative bloated header with extra 0000
				bufsection.resultsPtr := bufsection.scriptPtr;
				bufsection.scriptPtr := bufsection.picsPtr;
				bufsection.picsPtr := bufsection.endPtr;
				bufsection.endPtr := loader.ReadWordFrom(8);
			end;
			// Read local var list? or action mapping? from fourth array
			l := loader.ReadWordFrom(bufsection.resultsPtr);
			if l = 0 then l := loader.ReadWordFrom(bufsection.scriptPtr);
			ScriptWriter.WriteBuf('// Fourth array:');
			while (bufsection.endPtr < l) do begin
				ScriptWriter.WriteBuf(' ' + strdec(loader.ReadByteFrom(bufsection.endPtr)));
				inc(bufsection.endPtr);
			end;
			ScriptWriter.WriteBufLn('');
			loader.ofs := loader.ReadWordFrom(bufsection.scriptPtr); // script main entry point
			// Read click action list, from first array.
			if loader.ReadWordFrom(bufsection.resultsPtr) <> 0 then
				while (bufsection.resultsPtr < bufsection.scriptPtr) do begin
					ScriptWriter.WriteBuf('// ');
					bufsection.endPtr := loader.ReadWordFrom(bufsection.resultsPtr);
					case loader.ReadByteFrom(bufsection.endPtr) of
						// action type
						1: ScriptWriter.WriteBuf('Look');
						2: ScriptWriter.WriteBuf('Go');
						3: ScriptWriter.WriteBuf('Talk');
						4: ScriptWriter.WriteBuf('Hit');
						5: ScriptWriter.WriteBuf('Push');
						6: ScriptWriter.WriteBuf('Open');
						7: ScriptWriter.WriteBuf('Touch');
						8: ScriptWriter.WriteBuf('Lick');
						9: ScriptWriter.WriteBuf('Kiss');
						10: ScriptWriter.WriteBuf('Drill');
						else ScriptWriter.WriteBuf('Unknown action ' + strdec(loader.ReadByteFrom(bufsection.endPtr)));
					end;
					inc(bufsection.endPtr); // local var number
					ScriptWriter.WriteBuf(': $v' + strhex(loader.ReadByteFrom(bufsection.endPtr)));
					inc(bufsection.endPtr);
					while (bufsection.endPtr < loader.ReadWordFrom(bufsection.resultsPtr + 2)) do begin
						i := loader.ReadByteFrom(bufsection.endPtr); // jump addresses
						if i = 0 then raise DecompException.Create('0!!!');
						dec(i); // make it 0-based
						txt := strhex(loader.ReadWordFrom(bufsection.scriptPtr + i * 2), 4);
						ScriptWriter.WriteBuf('; ' + txt);
						inc(bufsection.endPtr);
					end;
					ScriptWriter.WriteBufLn('');
					inc(bufsection.resultsPtr, 2);
				end;
			exit;
		end;

		gid.Tasogare:
		if scriptname = 'TA_00DG' then begin
			ScriptWriter.WriteBufLn('// OVL is divided in four sections, so work around it with a case jump');
			ScriptWriter.WriteBufLn('casegoto $v512 SHR 8; ."noarray:array2:array4:array6"');
			ScriptWriter.WriteBufLn('');
			bufsection.scriptPtr := loader.ReadWordFrom(0);
			i := 2;
			while i < bufsection.scriptPtr do begin
				ScriptWriter.WriteBufLn('');
				ScriptWriter.WriteBufLn('@array' + strdec(i) + ':');
				ScriptWriter.WriteBuf('casegoto $v512 AND 0xFF; ."');
				j := loader.ReadWordFrom(i);
				loader.ofs := j;
				l := loader.ReadWordFrom(j);
				while loader.ofs < l do begin
					if loader.ofs <> j then ScriptWriter.WriteBuf(':');
					m := loader.ReadWord;
					txt := strhex(m, 4);
					ScriptWriter.WriteBuf(txt);
				end;
				ScriptWriter.WriteBufLn('"');
				fillbyte(loader.PtrAt(j)^, loader.ofs - j, 0);
				inc(i, 2);
			end;
			ScriptWriter.WriteBufLn('');
			ScriptWriter.WriteBufLn('@noarray:');
			loader.ofs := 8;
			exit;
		end;
	end;

	// Standard header handler for all other games.
	// If option lists exist, handle them; sometimes it's plain code, no lists
	if loader.ReadWordFrom(0) in [$0008, $000A, $000C] then begin
		// Read constant pointers
		bufsection.resultsPtr := loader.ReadWordFrom(0);
		bufsection.scriptPtr := loader.ReadWordFrom(2);
		bufsection.optionsPtr := loader.ReadWordFrom(4);
		bufsection.picsPtr := loader.ReadWordFrom(6);
		bufsection.endPtr := loader.ReadWordFrom(8);

		// Get the address of the bytecode entrypoint
		bufsection.scriptPtr := loader.ReadWordFrom(bufsection.scriptPtr);

		// First word array must have non-zero length, and the first offset must be
		// valid, otherwise there are no choices to be made.
		loader.ofs := loader.ReadWordFrom(bufsection.resultsPtr);
		if (bufsection.resultsPtr < loader.ReadWordFrom(2))
		and (loader.ofs > bufsection.endPtr) and (loader.ofs < bufsection.scriptPtr)
		then begin
			// Read the choice combination records.
			repeat
				if numCombos > high(choiceCombo) then
					raise DecompException.Create('choiceCombo overflow @ $' + strhex(loader.ofs));
				// verb pointer
				i := loader.ReadWord;
				if (i <= bufsection.endPtr) or (i >= bufsection.scriptPtr) then break; // validity check
				choiceCombo[numCombos].verbtext := _CapSize(loader.ReadStringFrom(i + 1));
				// subject pointer
				i := loader.ReadWord;
				if (i <= bufsection.endPtr) or (i >= bufsection.scriptPtr) then // validity check
					choiceCombo[numCombos].subjecttext := ''
				else
					choiceCombo[numCombos].subjecttext := _CapSize(loader.ReadStringFrom(i + 1));
				// variable ID
				choiceCombo[numCombos].ID := loader.ReadWord;
				// Jump addresses, read up to first invalid address or start of bytecode.
				l := 0;
				repeat
					i := loader.ReadWordFrom(loader.ofs);
					if (i < bufsection.scriptPtr) or (loader.ofs >= bufsection.scriptPtr) then break;
					if l >= high(choiceCombo[1].jumpresult) then
						raise DecompException.Create('choiceCombo jumpresult overflow @ $' + strhex(loader.ofs));

					choiceCombo[numCombos].jumpresult[l] := i;
					inc(l); inc(loader.readp, 2);
				until (loader.ofs >= bufsection.scriptPtr);
				// Mark end of results.
				choiceCombo[numCombos].jumpresult[l] := $FFFF;
				// If the next word is a zero, skip it.
				if loader.ReadWordFrom(loader.ofs) = 0 then inc(loader.readp, 2);

				inc(numCombos);
			until loader.ofs >= bufsection.scriptPtr;
		end;

		if numCombos <> 0 then begin
			// Read option list addresses.
			loader.ofs := bufsection.optionsPtr;
			while (loader.ofs < bufsection.picsPtr) do begin
				i := loader.ReadWord;
				if (i <= bufsection.endPtr) or (i >= bufsection.scriptPtr) then continue; // in data section?
				inc(numOptions);
				if numOptions > high(optionList) then
					raise DecompException.Create('optionList overflow @ $' + strhex(loader.ofs));

				optionList[numOptions].address := i;
			end;

			i := 1;
			while i <= numOptions do begin
				loader.ofs := optionList[i].address;
				// Read the verb string.
				optionList[i].verbtext := _CapSize(loader.ReadStringFrom(loader.ReadWord + 1));
				// Read subject strings, until FFFF.
				l := loader.ReadWord;
				j := 0;
				while l <> $FFFF do begin
					inc(j);
					optionList[i].subjecttext[j] := _CapSize(loader.ReadStringFrom(l + 1));
					l := loader.ReadWord;
				end;
				inc(i);
			end;
		end;

		// Read picture list.
		pictureList[0].isSprite := FALSE; // silence the compiler
		if (loader.ReadWordFrom(loader.ReadWordFrom(bufsection.picsPtr)) <> 0) then begin
			loader.ofs := bufsection.picsPtr;
			while loader.ofs < bufsection.endPtr do begin
				if numPictures > high(pictureList) then
					raise DecompException.Create('Picture list overflow @ $' + strhex(loader.ofs));
				j := loader.ReadWordFrom(loader.ofs);

				with pictureList[numPictures] do begin
					i := loader.ReadByteFrom(j); inc(j);
					l := loader.ReadByteFrom(j); inc(j);
					gfxName := upcase(loader.ReadStringFrom(j));
					ScriptWriter.WriteBufLn(strcat('# Gfx %: style $& type $& %', [numPictures, i, l, gfxName]));

					// Translate swipe styles to uniform values.
					case i of
						6, 7: transitionStyle := ETransitionType.WipeFromLeft;
						11: transitionStyle := ETransitionType.RaggedWipe;
						4, 5, 8, 10: transitionStyle := ETransitionType.Interlaced;
						9: transitionStyle := ETransitionType.Crossfade;
						else transitionStyle := ETransitionType.Instant;
					end;

					blackoutFirst := FALSE; isSprite := FALSE;
					case l of
						$38: begin isSprite := TRUE; transitionStyle := ETransitionType.Crossfade; end;
						$50: if (transitionStyle <> ETransitionType.Instant) and (gfxName <> 'TB_000') then
							blackoutFirst := TRUE;
						$03, $42, $4E:;
						else raise DecompException.Create(strcat('Unknown image type $&: %', [l, gfxName]));
					end;

					{$ifdef enable_decomp_hacks}
					// Hack: Turn all instant transitions into crossfades.
					if ((game = gid.AngelsC1) or (game = gid.AngelsC2))
					and (transitionStyle = ETransitionType.Instant)
					then transitionStyle := ETransitionType.Crossfade;
					{$endif}
				end;

				inc(numPictures); inc(loader.readp, 2);
			end;
			ScriptWriter.WriteBufLn('');
			{$ifdef enable_decomp_hacks}
			case game of
				gid.SanShimai:
				if scriptname = 'SK_406' then begin
					// Hack: add a missing graphic that seems to crash even the original.
					pictureList[2].gfxName := 'TB_000';
					pictureList[2].transitionStyle := ETransitionType.WipeFromLeft;
					pictureList[2].isSprite := FALSE;
					pictureList[2].blackoutFirst := FALSE;
					numPictures := 3;
				end;
				gid.Transfer98:
				if scriptname = 'TEN_S108' then begin
					// Hack: fix a missing graphic definition
					pictureList[2].gfxName := 'TT_02';
					pictureList[2].transitionStyle := ETransitionType.Crossfade;
					pictureList[2].isSprite := TRUE;
					pictureList[2].blackoutFirst := FALSE;
					numPictures := 3;
				end;
			end;
			{$endif}
		end;

		// In Sakura's code, the options list is occasionally in a different order than the goto-results list.
		// The options list has the correct order, so the results must be shifted to conform.
		// But I can't be bothered.
		// (see the following scripts:
		// CS212 (hack fix), CS401, CS403 (hack fix), CS501, CS701, CS802, CS822)

		// output readable option lists
		if numCombos <> 0 then with ScriptWriter do begin

			//for l := 0 to combos - 1 do with choiceCombo[l] do writeln(l,': ',ID,' ',verbtext,' ',subjecttext);
			WriteBufLn('choice.reset');

			for i := 0 to numCombos - 1 do begin
				// If an option has no jump results, don't print it.
				// (eliminates a few odd duplicate combos, see MT_1002, MT_1003, MT_0814)
				if choiceCombo[i].jumpresult[0] <> $FFFF then
					with choiceCombo[i] do begin
						WriteBuf('choice.set "' + verbtext);
						if subjecttext <> '' then WriteBuf(':' + subjecttext);
						WriteBuf('" ."');
						j := 0;
						while (j < dword(length(jumpresult))) and (jumpresult[j] <> $FFFF) do begin
							AddJump(jumpresult[j]);
							txt := strhex(jumpresult[j], 4);
							if j <> 0 then WriteBuf(':');
							WriteBuf(txt);
							inc(j);
						end;
						WriteBuf('"');
						if j > 1 then begin
							WriteBuf(' v' + strhex(ID));
							AddLocalVar(ID);
						end;
						WriteBufLn('');
						hasChoices := TRUE;
					end;
			end;
		end;

		loader.ofs := bufsection.scriptPtr;
	end;
end;

begin
	result := FALSE;
	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	ScriptWriter.Init(loader);

	animSlot[0].animOfsXP := 0;
	choiceCombo[0].id := 0;
	fillbyte(animSlot[0], length(animSlot) * sizeof(animSlot[0]), 0);
	drawGfx.drawCmdCount := 0;
	drawGfx.pending := FALSE;

	for i := 1 to high(optionList) do begin
		optionList[i].verbtext := '';
		for j := 1 to dword(high(optionList[1].subjecttext)) do
			optionList[i].subjecttext[j] := '';
	end;
	hasChoices := FALSE;
	persistSprites := FALSE;
	numCombos := 0; numOptions := 0; numPictures := 0;
	for i := high(stringCache) downto 0 do stringCache[i] := '';

	// Variable references can be 8-bit or 16-bit, though the vars themselves at runtime are always 16-bit signed.
	case game of
		gid.Eroden, gid.Hohoemi_w, gid.Izayoi, gid.Majokko_w, gid.PrettyParfait, gid.SexyParfait, gid.TalesNights,
		gid.TenshiNingyou: bitness := 2;
		else bitness := 1;
	end;

	{$ifdef enable_decomp_hacks} _ApplyHacks(loader); {$endif}

	bufsection.scriptPtr := 0;

	_HandleHeaders;
	ScriptWriter.WriteBufLn(''); // empty line before code starts

	case game of
		// Some games have one or two non-compliant OVL files.
		gid.Tasogare:
		if scriptname = 'OP_M2' then begin
			l := 0;
			ScriptWriter.WriteBufLn('mus.play TK_19');
			ScriptWriter.WriteBufLn('#event.create.interrupt');
			ScriptWriter.WriteBufLn('sleep 1000');
			while loader.ofs < loader.buffySize do begin
				i := loader.ReadByte;
				case i of
					1:
					begin
						txt := upcase(loader.ReadString);
						ScriptWriter.WriteBufLn('show ' + txt);
						if loader.ReadWordFrom(loader.ofs) = 2 then ScriptWriter.WriteBufLn('sleep 100');
					end;
					2:
					if l = 0 then
						ScriptWriter.WriteBufLn('gfx.clearall')
					else begin
						l := 0;
						ScriptWriter.WriteBufLn('tbox.clear');
					end;
					3:
					begin
						j := loader.ReadByte;
						ScriptWriter.WriteBufLn('sleep ' + strdec(j * 100));
					end;
					4:
					begin
						ScriptWriter.WriteBufLn('gfx.clearall');
						ScriptWriter.WriteBufLn('gfx.flash 2');
						ScriptWriter.WriteBufLn('sleep');
					end;
					5:
					begin
						m := 0; // track longest row, 16 chars is about half scr width
						j := loader.ReadByte;
						while j <> 0 do begin
							dec(j);
							txt := loader.ReadString;
							if m < txt.Length then m := txt.Length;
							ScriptWriter.WriteBuf('print "' + GetUTF8(@txt[1], txt.Length));
							if j <> 0 then ScriptWriter.WriteBuf('\n');
							ScriptWriter.WriteBufLn('"');
						end;
						if m > 63 then m := 63;
						ScriptWriter.WriteBufLn('tbox.move MAINBOX ' + strdec(dword(16384 - m shl 8)) + ' 12000');
						l := 1;
					end;
					6:
					begin
						j := loader.ReadByte;
						while j <> 0 do begin
							dec(j);
							txt := loader.ReadString;
							if (txt[1] = #$81) and (txt[2] = #$40) then
								txt := copy(txt, 3, $FF); // cut initial whitespace
							ScriptWriter.WriteBuf('print TITLEBOX "' + GetUTF8(@txt[1], txt.Length));
							if j <> 0 then ScriptWriter.WriteBuf('\n');
							ScriptWriter.WriteBufLn('"');
						end;
						ScriptWriter.WriteBufLn('sleep 1500');
						ScriptWriter.WriteBufLn('tbox.clear TITLEBOX');
						ScriptWriter.WriteBufLn('sleep 500');
					end;
					7:
					begin
						txt := upcase(loader.ReadString);
						ScriptWriter.WriteBufLn('transition d');
						ScriptWriter.WriteBufLn('show ' + txt);
						ScriptWriter.WriteBufLn('#gfx.solidblit ' + txt + '; $FFFF');
						ScriptWriter.WriteBufLn('sleep 50');
						ScriptWriter.WriteBufLn('#gfx.solidblit ' + txt + '; 0');
						ScriptWriter.WriteBufLn('sleep');
					end;
					10:
					begin
						ScriptWriter.WriteBufLn('gfx.flash 4');
						ScriptWriter.WriteBufLn('sleep');
						ScriptWriter.WriteBufLn('@movingon: event.remove.interrupt');
						ScriptWriter.WriteBufLn('//event.exit');
						ScriptWriter.WriteBufLn('gfx.clearall');
						ScriptWriter.WriteBufLn('return');
					end;
				end;
			end;
			loader.ofs := loader.buffySize;
		end;

		gid.Transfer98:
		if scriptname = 'TKEXE' then begin
			j := 0; l := 0;
			while loader.ofs < loader.buffySize do begin
				i := loader.ReadByte;
				case i of
					0:
					begin
						ScriptWriter.WriteBufLn('return');
						ScriptWriter.WriteBufLn('');
						ScriptWriter.WriteBuf('@');
						inc(j);
						if j < 10 then ScriptWriter.WriteBuf('0');
						ScriptWriter.WriteBufLn(strcat('%: // $&', [j, loader.ofs]));
					end;
					1: ScriptWriter.WriteBufLn('waitkey');
					2: ScriptWriter.WriteBufLn('call INTRO.QUESTIONS');
					3: ScriptWriter.WriteBufLn('//dummy 03');
					6:
					begin
						ScriptWriter.WriteBufLn('transition d');
						ScriptWriter.WriteBufLn('gfx.clearall // 06');
						if l in [1,2] then ScriptWriter.WriteBufLn('show OP_1');
						inc(l);
						ScriptWriter.WriteBufLn('sleep');
					end;
					else _DoTextOutput;
				end;
			end;
			loader.ofs := loader.buffySize;
		end;
	end;

	// 3sis-version engines automatically draw the first listed graphic.
	isBlackedOut := FALSE; stashActive := FALSE;
	if numPictures <> 0 then begin
		_ShowPicture(0);
		persistSprites := TRUE;
	end;
	implicitWaitkey := 0;

	// Loader.ofs was set to bufsection.scriptPtr at the end of header handling.
	// If header was not present, loader.ofs is still 0.
	with ScriptWriter do while loader.readp < loader.endp do begin

		// Print an informative line at choice jump targets.
		if numCombos <> 0 then
			for i := numCombos - 1 downto 0 do
				with choiceCombo[i] do begin
					for j := 0 to dword(high(jumpresult)) do begin
						if jumpresult[j] = $FFFF then break;
						if jumpresult[j] = loader.ofs then begin
							WriteBuf('### ' + verbtext + ' : ');
							if subjecttext <> '' then WriteBuf(subjecttext + ' : ');
							WriteBufLn('v' + strhex(ID) + '=' + strhex(j));
							break;
						end;
					end;
				end;

		{$ifdef enable_decomp_hacks}
		case game of
			gid.Eden:
			begin
				// Hack: keep blackout until delayed sprite draw
				if (scriptname = 'JO21026') and (loader.ofs = $22B) then isBlackedOut := TRUE;
			end;

			gid.Sakura:
			begin
				if scriptname = 'CS904_A' then begin
					// Hack: add a snow effect
					if loader.ofs = $A8 then begin
						WriteBufLn('//$particles := 48');
						WriteBufLn('//fx.precipitate.init SNOW1; SNOW2; snow; 20');
					end
					else if loader.ofs = $262 then begin
						WriteBufLn('//fx.precipitate.end');
						WriteBufLn('//$particles := 20');
					end
					// Hack: redraw oddly missing sprite
					else if loader.ofs = $39C then begin
						WriteBufLn('show x:0.25 CT01C');
						WriteBufLn('show x:0.25 CT01R');
					end
					else if loader.ofs = $1AD then WriteBuf('//');
				end;
			end;
		end;
		{$endif enable_decomp_hacks}

		// See SAKURA.MD for documentation on bytecodes.
		i := loader.ReadByte;

		// Output implicit waitkey before exiting its originating label.
		if implicitWaitkey = 2 then
			if (i in [0,2..5])
			or (i = $0B) and (byte(loader.readp^) in [$A, $32..$37, $39])
			then begin
				WriteBuf('... // implicit' + LineEnding);
				implicitWaitkey := 0;
			end;

		case i of
			// 00 [00] - end of bytecode section; get user input
			0:
			begin
				// If multiple 00 in a row, skip the rest.
				while (loader.readp < loader.endp) and (byte(loader.readp^) = 0) do inc(loader.readp);
				if hasChoices then
					WriteBufLn('choice.go')
				else // if no choices have been defined, pop the script
					WriteBufLn('return');
				persistSprites := FALSE;
			end;

			// 01 - Wait for keypress, then clear message area
			1:
			if implicitWaitkey = 0 then
				WriteBufLn('tbox.clear // waitkey')
			else begin
				WriteBufLn('...');
				implicitWaitkey := 0;
			end;

			// 02 - Jump to new OVL by number
			2:
			case game of
				gid.AngelsC1, gid.Setsujuu, gid.Transfer98:
				begin
					j := loader.ReadByte;
					txt := strdec(j);
					while length(txt) < 3 do txt := '0' + txt;
					case game of
						gid.AngelsC1:
						WriteBufLn('call ' + copy(scriptname, 1, 3) + txt[2] + txt[3] + '.');

						gid.Setsujuu: WriteBufLn('call SMG_S' + txt + '.');
						gid.Transfer98: WriteBufLn('call TEN_S' + txt + '.');
					end;
					if loader.ReadByteFrom(loader.ofs) <> 0 then WriteBufLn('');
				end;

				gid.Deep:
				begin
					j := loader.ReadByte;
					WriteBufLn('//dummy $02 ' + strhex(j) + ' // unlock map square, go to script H01_xx?');
				end;

				gid.Maririn:
				begin
					j := loader.ReadByte;
					WriteBufLn('//dummy $02 ' + strhex(j));
				end;

				gid.Tasogare:
				begin
					// enter the dungeon!
					j := loader.ReadByte;
					l := loader.ReadByte;
					m := loader.ReadByte;
					WriteBufLn(strcat('// dungeon romp! map % coords %,%', [j, l, m]));
					if (loader.readp < loader.endp) and (loader.ReadByteFrom(loader.ofs) <> 0) then
						WriteBufLn('');
				end;

				else raise DecompException.Create('Unknown code $02 @ $' + strhex(loader.ofs));
			end;

			// 03 - Return to wherever last jumped from
			3:
			begin
				case game of
					gid.SexyParfait: WriteBufLn('return // or exit? 03');

					gid.Tasogare:
					begin
						if scriptname = 'TA_00DG'
							then WriteBufLn('return // back to dungeon mode!')
						else
							if copy(scriptname, scriptname.Length - 1, 2) = 'MP' then
								WriteBufLn('goto mapentry')
							else
								WriteBufLn('return');
					end;

					else WriteBufLn('return');
				end;

				if (loader.readp < loader.endp) and (loader.ReadByteFrom(loader.ofs) <> 0) then
					WriteBufLn('');
			end;

			// 04 - Jump to new OVL by name
			4:
			case game of
				gid.Deep:
				begin
					WriteBuf('//dummy runscript 04');
					for j := 2 downto 0 do WriteBuf(', ' + strdec(loader.ReadByte));
					WriteBufLn('');
					if loader.ReadByteFrom(loader.ofs) in [0,3] = FALSE then WriteBufLn('');
				end;

				else begin
					txt := loader.ReadString;
					j := pos('.', txt);
					if j <> 0 then txt := copy(txt, 1, j - 1);
					{$ifdef enable_decomp_hacks}
					// Hack: replace calls to GOVER.OVL with a unified ENDINGS script
					if (game = gid.SanShimai) and (txt = 'GOVER') then begin
						WriteBufLn('$v512 := 255');
						txt := 'ENDINGS';
					end;
					{$endif}
					WriteBufLn('call ' + txt + '.');
					if (loader.readp < loader.endp) and (loader.ReadByteFrom(loader.ofs) <> 0) then
						WriteBufLn('');
				end;
			end;

			// 05 - Jump to new OVL by name
			5:
			case game of
				gid.Hohoemi, gid.SexyParfait, gid.Tasogare:
				begin
					txt := loader.ReadString;
					j := pos('.', txt);
					if j <> 0 then txt := copy(txt, 1, j - 1);
					WriteBufLn('call ' + txt + '. // 05');
				end;

				gid.Deep:
				begin
					txt := strdec(loader.ReadByte);
					if length(txt) < 2 then txt := '0' + txt;
					WriteBuf('call E' + txt + '.');
					if loader.ReadByte <> 0 then WriteBuf('x // depends which character is selected');
					WriteBufLn('');
					if NOT (loader.ReadByteFrom(loader.ofs) in [0,3]) then WriteBufLn('');
				end;

				gid.Maririn:
				begin
					j := loader.ReadByte;
					WriteBufLn('//dummy $05 ' + strhex(j));
				end;

				else raise DecompException.Create('Unknown code $05 @ $' + strhex(loader.ofs));
			end;

			// 06 - [Sakura] play song xx / [3sis] show graphic xx (no persistence)
			6:
			case game of
				gid.AngelsC1, gid.AngelsC2, gid.Runaway, gid.Runaway98, gid.SanShimai, gid.SanShimai98, gid.Setsujuu,
				gid.Transfer98, gid.Vanish:
				begin
					if implicitWaitkey = 0 then inc(implicitWaitkey);
					j := loader.ReadByte;
					if j >= numPictures then
						raise DecompException.Create(strcat('Graphic draw index % out of bounds @ $&', [j, loader.ofs]));
					_ShowPicture(j, TRUE);
					persistSprites := FALSE;
				end;

				gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Sakura, gid.Sakura98, gid.Tasogare:
				begin
					// Play song
					j := loader.ReadByte;
					if j in [0,$FF] then
						WriteBufLn('mus.stop')
					else
						if j > dword(length(gameConst.songList)) then
							WriteBufLn('// song outside list: ' + strdec(j))
							//raise DecompException.Create('Song outside list @ $' + strhex(loader.ofs)) else
						else
							WriteBufLn('mus.play ' + gameConst.songList[j - 1]);
				end;

				gid.SexyParfait:
				begin
					// unknown
					j := loader.ReadByte;
					l := loader.ReadByte;
					WriteBufLn('//dummy 06 // $' + strhex(j) + ' $' + strhex(l));
				end;

				else raise DecompException.Create('Unknown code $06 @ $' + strhex(loader.ofs));
			end;

			// 07 - 3sis/Runaway play song xx
			7:
			begin
				j := loader.ReadByte;

				case game of
					gid.AngelsC1, gid.AngelsC2, gid.Deep, gid.Runaway, gid.Runaway98, gid.SanShimai, gid.SanShimai98,
					gid.Setsujuu, gid.Vanish:
					begin
						if j in [0,$FF] then
							WriteBufLn('mus.stop')
						else
							if j > dword(length(gameConst.songList)) then
								WriteBufLn(strcat('// song outside list: %/%', [j, length(gameConst.songList)]))
								//raise DecompException.Create('Song outside list @ $' + strhex(loader.ofs)) else
							else
								WriteBufLn('mus.play ' + gameConst.songList[j - 1]);
					end;

					gid.Transfer98:
					if j > 38 then
						WriteBufLn('mus.stop')
					else
						if j < 10 then
							WriteBufLn('mus.play TEN00' + strdec(j))
						else
							WriteBufLn('mus.play TEN0' + strdec(j));

					gid.Hohoemi, gid.Maririn:
					begin
						WriteBufLn('//dummy $07 ' + strhex(j));
					end;

					gid.FromH, gid.Tasogare:
					begin
						WriteBufLn('print \$v' + strhex(j));
					end;

					else raise DecompException.Create('Unknown code $07 @ $' + strhex(loader.ofs));
				end;
			end;

			// 08 - Wait for keypress, don't clear message area afterward
			8:
			case game of
				gid.Setsujuu, gid.Transfer98, gid.Vanish:
				begin
					// Play sound effect, one data byte.
					WriteBufLn('// sound effect ' + strdec(loader.ReadByte));
				end;

				gid.Deep:
				begin
					WriteBufLn('// begin fight #' + strdec(loader.ReadByte));
				end;

				gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Sakura, gid.Sakura98, gid.Tasogare:
				begin
					if char(loader.readp^) in [#$A, ' '..'z'] then
						WriteBufLn('waitkey noclear=1')
					else begin
						if implicitWaitkey = 0 then WriteBuf('// ');
						WriteBufLn('...');
					end;
					implicitWaitkey := 0;
				end;

				else raise DecompException.Create('Unknown code $08 @ $' + strhex(loader.ofs));
			end;

			// 09 - Wait for keypress, don't clear message area afterward (or sleep)
			9:
			case game of
				gid.SanShimai, gid.SanShimai98:
				begin
					if implicitWaitkey = 0 then WriteBuf('// ');
					WriteBufLn('...');
					implicitWaitkey := 0;
				end;

				gid.Deep, gid.Hohoemi, gid.Majokko, gid.SexyParfait, gid.Tasogare:
				begin
					// interruptable pause
					WriteBufLn('sleep ' + strdec(loader.ReadByte * 100) + ' // 09');
				end;

				gid.FromH:
				begin
					// weird strings
					WriteBuf('//dummy $09 "');
					while loader.ReadByteFrom(loader.ofs) in [32..122] do WriteBuf(char(loader.ReadByte));
					WriteBufLn('"');
				end;

				gid.Transfer98:
				begin
					// redundant runscript command
					inc(loader.readp);
					txt := strdec(loader.ReadWord);
					while length(txt) < 3 do txt := '0' + txt;
					WriteBufLn('call TEN_S' + txt + '.');
				end;

				else raise DecompException.Create('Unknown code $09 @ $' + strhex(loader.ofs));
			end;

			// 0A - Linebreak, handled with other text output

			// 0B - Local variable functions
			$B: _0B;

			// 0C - Global variable functions
			$C:
			begin
				case loader.ReadByte of
					1: WriteBuf(strcat('$v& := $g&', [loader.ReadByte, loader.ReadByte]));
					2: WriteBuf(strcat('$g& := $v&', [loader.ReadByte, loader.ReadByte]));
					3: WriteBuf(strcat('$g& := %', [loader.ReadByte, loader.ReadByte]));

					else raise DecompException.Create(
						strcat('Unknown $C subcode $& @ $&', [loader.ReadByte, loader.ofs - 1]));
				end;
				WriteBufLn('');
			end;

			// $0D - various more modern graphic functions
			$0D:
			case game of
				gid.Deep:
				begin
					WriteBuf('//dummy 0D');
					for j := 2 downto 0 do begin
						WriteBuf('-$' + strhex(loader.ReadByte));
					end;
					WriteBufLn('');
				end;

				gid.SexyParfait:
				begin
					case loader.ReadByte of
						// xx
						$02:
						begin
							WriteBuf('//dummy 0D-02 // data words');
							for j := 3 downto 0 do begin
								txt := strhex(loader.ReadWord, 4);
								WriteBuf(' $' + txt);
							end;
							WriteBufLn('');
						end;

						$09:
						begin
							WriteBuf('//dummy 0D-09 // transition? $' + strhex(loader.ReadByte));
							WriteBufLn('');
						end;

						$0A:
						begin
							txt := loader.ReadString;
							m := loader.ReadWord; // unk
							j := loader.ReadWord; // locx
							l := loader.ReadWord; // locy
							WriteBuf('show ' + txt + ' anim');
							if j <> 0 then WriteBuf(' x:' + strdec(j));
							if l <> 0 then WriteBuf(' y:' + strdec(l));
							WriteBuf(' // unk word $' + strhex(m));
							inc(loader.readp, 4);
							repeat
								j := loader.ReadWord;
							until j = $FFFF;
							WriteBufLn('');
						end;

						$0B: WriteBufLn('//dummy 0D-0B // gfx.clearkids?');

						else raise DecompException.Create(
							strcat('Unknown $D subcode $& @ $&', [loader.ReadByteFrom(loader.ofs - 1), loader.ofs - 2]));
					end;
				end;

				else raise DecompException.Create('Unknown code $0D @ $' + strhex(loader.ofs - 1));
			end;

			$0E:
			case game of
				// graphic panning

				gid.AngelsC1, gid.AngelsC2, gid.Transfer98:
				begin
					j := loader.ReadByte;
					WriteBufLn('// fx.move $' + strhex(j));
				end;

				gid.Majokko:
				begin
					j := loader.ReadByte;
					WriteBufLn('//dummy $0E ' + strhex(j) + ' // pan image');
				end;

				gid.Deep:
				begin
					j := loader.ReadByte;
					WriteBufLn('//dummy $0E ' + strhex(j) + ' // pan image?');
				end;

				gid.SexyParfait:
				begin
					j := loader.ReadWord;
					WriteBufLn('//dummy $0E ' + strhex(j) + ' // transition? pan?');
				end;

				else raise DecompException.Create('Unknown code $0E @ $' + strhex(loader.ofs));
			end;

			// 0F xx - happy ending!
			$0F:
			case game of
				gid.AngelsC2, gid.Eden, gid.Hohoemi, gid.Majokko, gid.Sakura, gid.Sakura98:
				begin
					j := loader.ReadByte;
					if game <> gid.Hohoemi then if j = 0 then dec(loader.readp);
					WriteBufLn('$v512 := ' + strdec(j));
					WriteBufLn('call ENDINGS.');
				end;

				gid.FromH:
				begin
					txt := loader.ReadString;
					WriteBufLn('transition d');
					WriteBufLn('gfx.clearkids');
					WriteBufLn('show ' + txt + ' bkg');
					WriteBufLn('sleep');
					WriteBufLn('call ENDINGS.');
				end;

				gid.AngelsC1, gid.Runaway, gid.Runaway98, gid.SanShimai, gid.SanShimai98, gid.Setsujuu, gid.Transfer98,
				gid.Vanish:
				begin
					// vanilla end
					WriteBufLn('$v512 := 0');
					WriteBufLn('call ENDINGS.');
				end;

				gid.Tasogare:
				begin
					// ending credits sequence
					WriteBufLn('$v900 := 2000');
					repeat
						j := loader.ReadByte;
						WriteBufLn('transition d');
						WriteBufLn('gfx.clearkids');
						case j of
							1: WriteBuf('show type:bkg ');
							2: WriteBuf('show type:sprite ');
							4: continue;
							else break;
						end;
						txt := loader.ReadString;
						WriteBufLn(txt);
						WriteBufLn('sleep 7000');
					until loader.readp >= loader.endp;
				end;

				else raise DecompException.Create('Unknown code $0F @ $' + strhex(loader.ofs));
			end;

			// 10 - unhappy ending!
			$10:
			begin
				WriteBufLn('$v512 := 255');
				WriteBufLn('call ENDINGS.');
				if loader.ofs + 1 < loader.buffySize then WriteBufLn('');
			end;

			// 11 xx - lots of commands
			$11:
			if game = gid.AngelsC1 then begin
				WriteBufLn('//dummy $11');
			end
			else begin
				j := loader.ReadByte;
				case j of
					$02: case game of
						gid.SanShimai, gid.SanShimai98, gid.Runaway, gid.Runaway98:
						begin
							if implicitWaitkey = 0 then inc(implicitWaitkey);
							j := loader.ReadByte;
							if j >= numPictures then
								raise DecompException.Create(
									strcat('Graphic draw request % out of bounds @ $&', [j, loader.ofs]));
							_ShowPicture(j, FALSE);
							persistSprites := TRUE;
						end;

						gid.Vanish:
						begin
							l := loader.ReadByte;
							WriteBufLn('// battle? #' + strdec(l));
						end;

						else raise DecompException.Create('Unknown code $11-02 @ $' + strhex(loader.ofs));
					end;

					$03:
					case game of
						gid.Vanish:
						begin
							WriteBuf('//dummy $11-03-' + strhex(loader.ReadByte) + ': go to map location?');
							WriteBufLn('');
						end;

						else raise DecompException.Create('Unknown code $11-03 @ $' + strhex(loader.ofs));
					end;

					$04:
					case game of
						gid.Tasogare:
						begin
							WriteBufLn('// sys.savegame');
						end;

						gid.Vanish:
						begin
							WriteBuf('//dummy $11-04: var ' + strdec(loader.ReadByte) +
								' with value ' + strdec(loader.ReadByte) + '?');
							WriteBufLn('');
						end;

						else raise DecompException.Create('Unknown code $11-04 @ $' + strhex(loader.ofs));
					end;

					$05:
					case game of
						gid.FromH, gid.Tasogare:
						begin
							WriteBufLn('// <-- name entry!');
						end;

						gid.Vanish:
						begin
							WriteBufLn('sys.quit');
						end;

						gid.Hohoemi:
						begin
							WriteBufLn('//dummy $11-05 // return to main menu?');
						end;

						else raise DecompException.Create('Unknown code $11-05 @ $' + strhex(loader.ofs));
					end;

					$06:
					case game of
						gid.Eden:
						begin
							WriteBufLn('$map := ' + strdec(loader.ReadByte));
							WriteBufLn('call NEWMAP.');
						end;

						gid.FromH, gid.Tasogare:
						begin
							WriteBuf('// mark graphic ' + strdec(loader.ReadByte) + ' as seen');
							WriteBufLn('');
						end;

						gid.Hohoemi:
						begin
							WriteBufLn('// save the game $11-06');
						end;

						else raise DecompException.Create('Unknown code $11-06 @ $' + strhex(loader.ofs - 2));
					end;

					$07:
					case game of
						gid.FromH, gid.Tasogare:
						begin
							j := loader.ReadByte;
							l := loader.ReadByte;
							WriteBuf(strcat('//$v& := gfx.IsSeen(v%)', [j, l]));
							WriteBufLn('');
						end;

						gid.Hohoemi: WriteBufLn('call sub.showstats ' + strdec(loader.ReadByte));

						else raise DecompException.Create('Unknown code $11-07 @ $' + strhex(loader.ofs));
					end;

					$08:
					case game of
						gid.Tasogare:
						begin
							l := loader.ReadByte;
							WriteBufLn('// fade to black and back over ' + strdec(l) + ' desisecs');
						end;

						else raise DecompException.Create('Unknown code $11-08 @ $' + strhex(loader.ofs));
					end;

					$09:
					case game of
						// clickable map!
						gid.FromH:
						begin
							l := loader.ReadByte;
							WriteBufLn('// 11-09 data=' + strdec(l));
							for m := 0 to 11 do begin
								WriteBuf('$maploc' + strdec(m) + ' := ');
								l := loader.ReadWordFrom(loader.ofs + m + m);
								if l = 0 then
									WriteBufLn('0')
								else begin
									WriteBufLn('1');
									WriteBufLn(strcat('$s% := :"%"', [11 + m, GetUTF8(loader.ReadStringFrom(l))]));
								end;
							end;
							WriteBufLn('$map := (call NEWMAP.)');
							WriteBuf('casegoto $map ."');
							for m := 0 to 11 do begin
								l := loader.ReadWordFrom(loader.ofs);
								if l = 0 then
									l := bufsection.scriptPtr
								else begin
									while (l < loader.buffySize) and (loader.ReadByteFrom(l) <> 0) do begin
										byte(loader.PtrAt(l)^) := 0;
										inc(l);
									end;
									inc(l);
								end;

								AddJump(l);

								txt := strhex(l, 4);
								inc(loader.readp, 2);
								if m <> 0 then WriteBuf(':');
								WriteBuf(txt);
							end;
							WriteBufLn('"');
						end;

						gid.Tasogare:
						begin
							WriteBufLn('// 11-09 map');
							for m := 0 to 12 do begin
								WriteBuf('$maploc' + strdec(m) + ' := ');
								l := loader.ReadWordFrom(loader.ofs + m + m);
								if loader.ReadByteFrom(l) = $FF then
									WriteBufLn('0')
								else begin
									WriteBufLn('1');
									WriteBufLn(strcat('$s% := :"%"', [11 + m, GetUTF8(loader.ReadStringFrom(l + 1))]));
								end;
							end;
							WriteBufLn('@mapentry: $map := (call NEWMAP.)');
							WriteBuf('casegoto $map ."');
							for m := 0 to 12 do begin
								l := loader.ReadWordFrom(loader.ofs);
								j := $FFFF;
								if loader.ReadByteFrom(l) = $FF then j := bufsection.scriptPtr;
								byte(loader.PtrAt(l)^) := 0;
								inc(l);

								while (l < loader.buffySize) and (loader.ReadByteFrom(l) <> 0) do begin
									byte(loader.PtrAt(l)^) := 0;
									inc(l);
								end;
								inc(l);
								if j <> $FFFF then l := j;

								AddJump(l);

								txt := strhex(l, 4);
								inc(loader.readp, 2);
								if m <> 0 then WriteBuf(':');
								WriteBuf(txt);
							end;
							WriteBufLn('"');
						end;

						gid.Hohoemi:
						begin
							WriteBufLn('call sub.refreshframe');
						end;

						else raise DecompException.Create('Unknown code $11-09 @ $' + strhex(loader.ofs - 1));
					end;

					$0A:
					case game of
						gid.Hohoemi:
						begin
							WriteBufLn('call sub.randomisecalendar');
						end;

						else raise DecompException.Create('Unknown code $11-0A @ $' + strhex(loader.ofs - 1));
					end;

					$0B:
					case game of
						gid.Hohoemi:
						begin
							WriteBufLn('call sub.showcalendar');
						end;

						gid.Tasogare:
						begin
							WriteBufLn('gfx.clearkids // 11-0B');
						end;

						else raise DecompException.Create('Unknown code $11-0B @ $' + strhex(loader.ofs - 1));
					end;

					$0C:
					case game of
						gid.Hohoemi:
						begin
							WriteBufLn('call sub.refreshtime');
						end;

						gid.Tasogare:
						begin
							WriteBufLn('//dummy $11-0C // push player one square backward');
						end;

						else raise DecompException.Create('Unknown code $11-0C @ $' + strhex(loader.ofs - 1));
					end;

					$0D:
					case game of
						gid.Hohoemi:
						begin
							WriteBufLn('call sub.fadeandrefresh // 11-0D-' + strhex(loader.ReadByte));
						end;

						else raise DecompException.Create('Unknown code $11-0D @ $' + strhex(loader.ofs - 1));
					end;

					$0E:
					case game of
						gid.Hohoemi:
						begin
							WriteBuf('// mark graphic ' + strdec(loader.ReadByte) + ' as seen');
							WriteBufLn('');
						end;

						else raise DecompException.Create('Unknown code $11-0E @ $' + strhex(loader.ofs - 1));
					end;

					$0F:
					case game of
						gid.Hohoemi:
						begin
							WriteBufLn('//dummy $11-0F // unknown, value $' + strhex(loader.ReadByte));
						end;

						else raise DecompException.Create('Unknown code $11-0F @ $' + strhex(loader.ofs - 1));
					end;

					$10:
					case game of
						gid.Hohoemi:
						begin
							l := loader.ReadByte;
							j := loader.ReadByte;
							WriteBufLn(strcat('//dummy $11-10 // unknown, values $& and $&', [l, j]));
						end;

						gid.Tasogare:
						begin
							l := loader.ReadByte;
							j := loader.ReadByte;
							WriteBuf('//dummy $11-10 // change cell ' + strdec(j) + ',' + strdec(l) +
								' to $' + strhex(loader.ReadByte));
							WriteBufLn('');
						end;

						else raise DecompException.Create('Unknown code $11-10 @ $' + strhex(loader.ofs - 1));
					end;

					$11, $12:
					case game of
						gid.Hohoemi:
						begin
							if j = $11 then raise DecompException.Create('Unknown code $11-11 @ $' + strhex(loader.ofs - 1));
							WriteBufLn('// return to title screen $11-12');
						end;

						gid.Tasogare:
						begin
							m := loader.ReadByte;
							l := loader.ReadByte;
							WriteBufLn(strcat('// 11-&, data bytes: $&, &', [j, m, l]));
							txt := upcase(loader.ReadString);
							if txt[length(txt)] = 'X' then begin
								txt := copy(txt, 1, length(txt) - 2);
								WriteBufLn('show ' + txt + ' sprite');
								txt := txt + 'A0';
							end;
							WriteBufLn('show ' + txt + ' sprite');
						end;

						else raise DecompException.Create(strcat('Unknown code $11-& @ $&', [j, loader.ofs - 1]));
					end;

					$13:
					case game of
						gid.Hohoemi:
						begin
							j := loader.ReadByte;
							l := loader.ReadByte;
							m := loader.ReadByte;
							WriteBufLn(strcat('//dummy $11-13 // unknown, maybe var $& with vars/values $&, $&, $&',
								[j, l, m, loader.ReadByte]));
						end;

						else raise DecompException.Create(strcat('Unknown code $11-& @ $&', [j, loader.ofs - 1]));
					end;

					$14:
					case game of
						gid.Hohoemi:
						begin
							j := loader.ReadByte;
							l := loader.ReadByte;
							m := loader.ReadByte;
							WriteBufLn(strcat('//dummy $11-14 // unknown, values $&, $&, $&, $&',
								[j, l, m, loader.ReadByte]));
						end;

						else raise DecompException.Create(strcat('Unknown code $11-& @ $&', [j, loader.ofs - 1]));
					end;

					$15:
					case game of
						gid.Hohoemi:
						begin
							WriteBufLn('//dummy $11-15 // unknown');
						end;

						else raise DecompException.Create('Unknown code $11-15 @ $' + strhex(loader.ofs - 1));
					end;

					$16:
					case game of
						gid.Hohoemi:
						begin
							WriteBufLn('//dummy $11-16 // unknown');
						end;

						else raise DecompException.Create('Unknown code $11-16 @ $' + strhex(loader.ofs - 1));
					end;

					else raise DecompException.Create(strcat('Unknown code $11-& @ $&', [j, loader.ofs - 1]));
				end;

			end;

			// 12 xx - Option functions - at script init all options default to ON
			$12:
			begin
				j := loader.ReadByteFrom(loader.ofs + 1);
				l := loader.ReadByteFrom(loader.ofs + 2);
				if j > dword(high(optionList)) then
					raise DecompException.Create('$12 requested text outside optionList! @ $' + strhex(loader.ofs));

				if optionList[j].verbtext = '' then
					raise DecompException.Create(strcat('Choice verb $& not defined @ $&', [j, loader.ofs]));

				m := loader.ReadByteFrom(loader.ofs);
				case m of
					1:
					begin
						inc(loader.readp, 2);
						WriteBufLn('choice.off "' + optionList[j].verbtext + '"');
					end;
					2:
					begin
						inc(loader.readp, 2);
						WriteBufLn('choice.on "' + optionList[j].verbtext + '"');
					end;
					4:
					begin
						inc(loader.readp, 3);
						WriteBufLn('choice.off "' + optionList[j].verbtext + ':' + optionList[j].subjecttext[l] + '"');
					end;
					5:
					begin
						inc(loader.readp, 3);
						WriteBufLn('choice.on "' + optionList[j].verbtext + ':' + optionList[j].subjecttext[l] + '"');
					end;

					else raise DecompException.Create(strcat('Unknown code $12-& @ $&', [m, loader.ofs]));
				end;
			end;

			// 13 xx - Graphic functions (or smash in 3sis and Runaway)
			$13:
			case game of
				gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Sakura, gid.Sakura98, gid.Tasogare:
				with drawGfx do begin
					if implicitWaitkey = 0 then inc(implicitWaitkey);
					_EnableDrawGfx;
					if isBlackedOut then begin
						// conclusion for $14-03
						drawCmd[drawCmdCount] := 'gfx.remove TB_000';
						inc(drawCmdCount);
						isBlackedOut := FALSE;
					end;
					txt := '';
					gfxStyle := 0; gfxOfsXP := 0;

					// Parse the 13 xxxxx string.
					repeat
						j := loader.ReadByte;
						case j of
							$01..$04: gfxStyle := gfxStyle or dword(1 shl j);
							$11: gfxStyle := gfxStyle or $8000;
							$00, $05..$0A, $14: ;
							$0B,$0C: WriteBufLn('// 13 has ' + strhex(j));
							$0D: gfxOfsXP := loader.ReadWord * 8;
							$0E:
							begin
								l := loader.ReadByte;
								if l >= $32 then
									dec(l, $32)
								else
									if l = 0 then
										l := 10
									else
										raise DecompException.Create(strcat('Unknown transition $& @ $&', [l, loader.ofs]));
								byte(transition) := l;
							end;
							$0F:
							if byte(txt[0]) <> 0 then // (if appears in or after filename, ignore as junk)
								j := $FF
							else begin
								drawCmd[drawCmdCount] := 'gfx.clearkids';
								if stashActive then drawCmd[drawCmdCount] := drawCmd[drawCmdCount] + ' TB_008';
								drawCmd[drawCmdCount] := drawCmd[drawCmdCount] + ' // 13-0F restore gfx';
								inc(drawCmdCount);
								for l := high(animSlot) downto 0 do animSlot[l].displayed := FALSE;
								// Flush lone 13-0F immediately, don't batch. But still have to start with transition.
								if drawCmdCount = 1 then begin
									WriteBuf('(transition) ');
									_FinishDrawGfx;
								end;
							end;
							$10:
							begin
								drawCmd[drawCmdCount] := 'show "|0" overlay name:"TB_008" w:1.0 h:1.0 // 13-10 save gfx';
								inc(drawCmdCount);
								stashActive := TRUE;
							end;
							$21..$7E:
							begin
								// Graphic file name, stick into a string.
								inc(byte(txt[0]));
								txt[byte(txt[0])] := char(j);
							end;

							else raise DecompException.Create(strcat('Unknown code $13 & @ $&', [j, loader.ofs]));
						end;
					until j in [$00, $0F, $10];

					// Draw a background with no name = pop state.
					if (gfxStyle and 16 <> 0) and (txt = '') then begin
						drawCmd[drawCmdCount] := 'gfx.clearkids';
						if stashActive then drawCmd[drawCmdCount] := drawCmd[drawCmdCount] + ' TB_008';
						drawCmd[drawCmdCount] := drawCmd[drawCmdCount] + ' // 13-04 restore gfx';
						inc(drawCmdCount);
						for l := high(animSlot) downto 0 do animSlot[l].displayed := FALSE;
					end;

					// Code $13 11: draw the first animation frame as a sprite
					if gfxStyle and $8000 <> 0 then begin
						if game = gid.Tasogare then begin
							drawCmd[drawCmdCount] := 'gfx.clearkids ' + copy(txt, 1, 5) + ' // 13-xx change expr';
							inc(drawCmdCount);
						end;
						txt := txt + 'A0';
						// Neutral expression, draw nothing.
						if (game = gid.Tasogare) and (length(txt) = 7) then txt := '';
					end;

					if txt <> '' then begin
						// See if drawing a sprite over an animation -> auto-disable anim.
						if gfxStyle and 8 <> 0 then
							for l := high(animSlot) downto 0 do
								if (animSlot[l].displayed) and (animSlot[l].animOfsXP = gfxOfsXP) then begin
									animSlot[l].displayed := FALSE;
									drawCmd[drawCmdCount] := 'gfx.remove ' + animSlot[l].animGfxName + ' // auto-remove';
									inc(drawCmdCount);
								end;

						if gfxStyle and 16 <> 0 then begin
							drawCmd[drawCmdCount] := 'gfx.remove TB_008 // clearstate';
							inc(drawCmdCount);
							stashActive := FALSE;
						end;

						case txt of
							'NOT': drawCmd[drawCmdCount] := 'show "' + txt + '"';
							'TB_008': drawCmd[drawCmdCount] := '// show TB_008, skip useless';
							else drawCmd[drawCmdCount] := 'show ' + txt;
						end;
						if game = gid.Hohoemi then begin
							if txt[1] = 'B' then
								txt := 'SCENE'
							else
								gfxStyle := gfxStyle and $FD; // draw as sprite, not overlay
						end;

						if gfxStyle and 2 <> 0 then
							drawCmd[drawCmdCount] := drawCmd[drawCmdCount] + ' overlay ."' + txt + '.."'
						else if gfxStyle and 16 <> 0 then
							drawCmd[drawCmdCount] := drawCmd[drawCmdCount] + ' bkg';

						if gfxOfsXP <> 0 then begin
							j := gameConst.baseResXP;
							m := GetGraphicFile(txt);
							if (m <> 0) and (graphicFiles[m].origResXP <> 0) then j := graphicFiles[m].origResXP;
							drawCmd[drawCmdCount] := drawCmd[drawCmdCount] +
								' x:' + strdec((gfxOfsXP shl 15 + j shr 1) div j);
						end;
						inc(drawCmdCount);
					end;

					// If a transition is defined, deploy the graphic drawing commands.
					if transition <> '!' then begin
						{$ifdef enable_decomp_hacks}
						// HACK: Replace interlaced swipe with crossfade for 13 03 sprites and non-loader 13 02...03 00
						if (gfxStyle and 8 <> 0) and (byte(transition) = 10) then
							byte(transition) := 9
						// HACK: Replace instant appear with interlaced for 13 04 backgrounds.
						else if (gfxStyle and 16 <> 0) and (byte(transition) = 0) then
							byte(transition):= 10;
						{$endif enable_decomp_hacks}
						case byte(transition) of
							6,7: transition := 'w';
							11: transition := 'r';
							4,5,8,10: transition := 'i';
							9: transition := 'f';
							else transition := '0';
						end;
						_FinishDrawGfx;
					end;
				end;

				// weird graphics thing
				gid.SexyParfait:
				begin
					WriteBuf('//dummy $13');
					while loader.ofs < loader.buffySize do begin
						j := loader.ReadByteFrom(loader.ofs);
						if j > $20 then begin
							WriteBuf(' ');
							repeat
								j := loader.ReadByte;
								if j = 0 then break;
								WriteBuf(char(j));
							until loader.readp >= loader.endp;
						end
						else begin
							WriteBuf(' $' + strhex(j)); inc(loader.readp);
							case j of
								0, 1, $10: l := 1;
								2: l := 2;
								9: l := 0;
								$B: l := 4;
								$C: l := 5;
								$D: l := 4;
								$12: l := 2;
								else l := 0;
							end;

							while l <> 0 do begin
								txt := strhex(loader.ReadByte);
								if length(txt) = 1 then txt := '0' + txt;
								WriteBuf(' ' + txt);
								dec(l);
							end;
						end;

						if j in [0,1,$10,$33..$7F] then break;
					end;
					WriteBufLn('');
				end;

				// Smash/Flash, always combined in these older games.
				gid.Runaway, gid.Runaway98, gid.SanShimai, gid.SanShimai98:
				begin
					j := loader.ReadByte;
					WriteBufLn('gfx.flash ' + strdec(j));
					l := 2;
					while l * l < j do inc(l);
					l := (l - 1) shr 1;
					// In some scripts, a sideways bash is more appropriate than a vertical thwomp; in others, just
					// a flash is quite enough. Nominate the scripts manually here.
					j := valx(scriptname);
					case game of
						gid.SanShimai, gid.SanShimai98:
						case j of
							123,216,217,220,223,541,702,713,719,733,737,738: _WriteBash(l);
							104,110,117,206,306,402,508,605,725,731: _WriteThwomp(l);
						end;
					end;
				end;

				else raise DecompException.Create(strcat('Unknown code $13 & @ $&',
					[loader.ReadByteFrom(loader.ofs + 1), loader.ofs]));
			end;

			// 14 - Variable reset; or, kill the overlay, restore the background.
			$14:
			case game of
				gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Sakura, gid.Sakura98, gid.Tasogare:
				begin
					l := loader.ReadByte;
					case l of
						// Reset global variables.
						1:
						begin
							j := loader.ReadByte;
							while j <> 0 do begin
								WriteBufLn('$g' + strhex(loader.ReadByte) + ' := 0');
								dec(j);
							end;
							WriteBufLn('');
						end;
						// Black out screen for a bit.
						3:
						with drawGfx do begin
							_EnableDrawGfx;

							j := loader.ReadByte;
							if game = gid.Tasogare then transition := 'i'
							else case j of
								// Some black-out transitions are not standard.
								$32: transition := '0';
								$39: transition := 'w';
								$3B: if game = gid.Eden then transition := 'f' else transition := '0';
								$3D: transition := 'r';
								$36, $37, $38, $3A, $3C: transition := 'i';
								else raise DecompException.Create(strcat('Unknown transition $& @ $&', [j, loader.ofs]));
							end;

							drawCmd[drawCmdCount] := dynamicTB000;
							inc(drawCmdCount);
							_FinishDrawGfx;
							isBlackedOut := TRUE;
						end;

						5:
						WriteBufLn(
							'//dummy $14-05 // some graphical effect? value $' + strhex(loader.ReadByte));

						else raise DecompException.Create(
							strcat('Unknown code $14-& @ $&', [l, loader.ofs - 1]));
					end;
				end;

				gid.Runaway, gid.Runaway98, gid.SanShimai, gid.SanShimai98:
				begin
					j := loader.ReadByte;
					WriteBufLn('sleep ' + strdec(j * 100));
				end;

				else raise DecompException.Create('Unknown code $14 @ $' + strhex(loader.ofs));
			end;

			// 15 xx - Handle animations
			$15:
			case game of
				gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Sakura, gid.Sakura98, gid.SexyParfait:
				with drawGfx do begin
					_EnableDrawGfx;
					j := loader.ReadByte;
					case j of
						// 01 - Read anim into slot yy, with an offset inside viewframe
						// 02 - Read anim into slot yy, with an offset relative to whole screen
						$01, $02:
						begin
							l := loader.ReadByte mod length(animSlot);
							with animSlot[l] do begin
								if displayed then begin
									if game = gid.Majokko then _EnableDrawGfx;
									drawCmd[drawCmdCount] := 'gfx.remove ' + animGfxName + ' // old slot ' + strdec(l);
									inc(drawCmdCount);
								end;
								animOfsXP := 0;
								animOfsYP := 0;
								if byte(loader.readp^) = $0D then begin
									inc(loader.readp);
									animOfsXP := loader.ReadByte * 8;
									animOfsYP := loader.ReadByte;
								end;

								animGfxName := loader.ReadString;
								// Clean the animname string of illegal chars.
								m := 0;
								for j := 1 to length(animGfxName) do
									if animGfxName[j] in ['!'..'z'] then begin
										inc(m);
										animGfxName[m] := animGfxName[j];
									end;
								if m > 8 then m := 8;
								byte(animGfxName[0]) := m;

								drawCmd[drawCmdCount] := 'show ' + animGfxName + ' anim';
								case animGfxName of
									// Sakura is missing a lot of blinkies...
									'CT02RA0','CT03RA0','CT06RA0','CT08CA0','AE_007GA','AE_104GA':
									drawCmd[drawCmdCount] := '//' + drawCmd[drawCmdCount];
								end;

								if animOfsXP <> 0 then begin
									m := GetGraphicFile(animGfxName);
									if m <> 0 then m := graphicFiles[m].origResXP;
									if m = 0 then m := gameConst.baseResXP;
									drawCmd[drawCmdCount] := drawCmd[drawCmdCount] +
										' x:' + strdec((animOfsXP shl 15 + m shr 1) div m);
								end;
								if animOfsYP <> 0 then begin
									m := GetGraphicFile(animGfxName);
									if m <> 0 then m := graphicFiles[m].origResYP;
									if m = 0 then m := gameConst.baseResYP;
									drawCmd[drawCmdCount] := drawCmd[drawCmdCount] +
										' y:' + strdec((animOfsYP shl 15 + m shr 1) div m);
								end;
								drawCmd[drawCmdCount] := drawCmd[drawCmdCount] + ' // load slot ' + strdec(l);

								// Draw over existing anim. Just switching an animation merits a crossfade.
								if (displayed) and (game = gid.Majokko) then transition := 'd';

								// If the immediate next command wipes out this animation, don't bother showing it.
								displayed := NOT (
									(loader.readp + 1 < loader.endp) and (byte(loader.readp^) = $15)
									and (byte((loader.readp + 1)^) in [$04, $05]));
								if NOT displayed then drawCmd[drawCmdCount] := '// ' + drawCmd[drawCmdCount];

								inc(drawCmdCount);

								if displayed then _FinishDrawGfx;
							end;
						end;

						// 03 - Display animation from slot yy
						3:
						begin
							l := loader.ReadByte mod length(animSlot);
							with animSlot[l] do begin
								if animGfxName = '' then
									raise DecompException.Create(
										strcat('animSlot % used while undefined @ $&', [l, loader.ofs - 3]));

								drawCmd[drawCmdCount] := 'show ' + animGfxName + ' anim';
								if animOfsXP <> 0 then begin
									m := GetGraphicFile(animGfxName);
									if m <> 0 then m := graphicFiles[m].origResXP;
									if m = 0 then m := gameConst.baseResXP;
									drawCmd[drawCmdCount] := drawCmd[drawCmdCount] +
										' x:' + strdec((animOfsXP shl 15 + m shr 1) div m);
								end;
								if animOfsYP <> 0 then begin
									m := GetGraphicFile(animGfxName);
									if m <> 0 then m := graphicFiles[m].origResYP;
									if m = 0 then m := gameConst.baseResYP;
									drawCmd[drawCmdCount] := drawCmd[drawCmdCount] +
										' y:' + strdec((animOfsYP shl 15 + m shr 1) div m);
								end;
								drawCmd[drawCmdCount] := drawCmd[drawCmdCount] + ' // show slot ' + strdec(l);

								// If the immediate next command wipes out this animation, don't bother showing it.
								displayed :=
									(loader.readp + 1 >= loader.endp) or (byte(loader.readp^) <> $15)
									or (NOT byte((loader.readp + 1)^) in [$04, $05]);
								if NOT displayed then drawCmd[drawCmdCount] := '// ' + drawCmd[drawCmdCount];

								inc(drawCmdCount);
								if displayed then _FinishDrawGfx;
							end;
						end;

						// 04 and 05 - Probably stop ongoing animations
						4, 5:
						begin
							drawCmd[drawCmdCount] := 'gfx.removeanims // 15-0' + strdec(j);

							// If the immediate next command is the same, comment this one out.
							if (loader.ofs + 1 < loader.buffySize) and (byte(loader.readp^) = $15)
							and (byte((loader.readp + 1)^) in [$04, $05])
							then drawCmd[drawCmdCount] := '// ' + drawCmd[drawCmdCount];

							inc(drawCmdCount);
							for l := 0 to high(animSlot) do animSlot[l].displayed := FALSE;
						end;

						6:
						begin
							WriteBufLn('//dummy $15-06 // pop anims?');
						end;

						else raise DecompException.Create(
							strcat('Unknown code $15 & @ $&', [loader.ReadByteFrom(loader.ofs - 1), loader.ofs]));
					end;
				end;

				gid.Tasogare:
				begin
					j := loader.ReadByte;
					case j of
						// 01 - Read anim into slot yy, with an offset inside viewframe
						// 02 - Read anim into slot yy, with an offset relative to whole screen
						$01, $02:
						begin
							l := loader.ReadByte mod length(animSlot);
							with animSlot[l] do begin
								animOfsXP := 0;
								animOfsYP := 0;
								if byte(loader.readp^) = $0D then begin
									inc(loader.readp);
									animOfsXP := loader.ReadByte * 8;
									animOfsYP := loader.ReadByte;
								end;

								animGfxName := loader.ReadString;
								// Clean the animname string of illegal chars.
								m := 0;
								for j := 1 to length(animGfxName) do
									if animGfxName[j] in ['!'..'z'] then begin
										inc(m);
										animGfxName[m] := animGfxName[j];
									end;
								if m > 8 then m := 8;
								byte(animGfxName[0]) := m;

								// If graphics are already queued for transitioning, ignore animation attempts.
								if NOT drawGfx.pending then begin
									// Clear any previous expression sprite, which the character gob should've adopted.
									WriteBufLn('gfx.clearkids ' + copy(animGfxName, 1, 5) + ' // change expression');
									// Draw the new expression but only if it's not the neutral A0 but something else.
									if length(animGfxName) = 8 then begin
										WriteBuf('show ' + animGfxName);
										if animOfsXP <> 0 then begin
											m := GetGraphicFile(animGfxName);
											if m <> 0 then m := graphicFiles[m].origResXP;
											if m = 0 then m := gameConst.baseResXP;
											WriteBuf(' x:' + strdec((animOfsXP shl 15 + m shr 1) div m));
										end;
										if animOfsYP <> 0 then begin
											m := GetGraphicFile(animGfxName);
											if m <> 0 then m := graphicFiles[m].origResYP;
											if m = 0 then m := gameConst.baseResYP;
											WriteBuf(' y:' + strdec((animOfsYP shl 15 + m shr 1) div m));
										end;
										WriteBufLn('');
									end;
								end;
								displayed := TRUE;
							end;
						end;

						3: // ignore
						begin
							l := loader.ReadByte mod length(animSlot);
							WriteBufLn('// dummy 15-03 anim slot ' + strdec(l));
						end;

						4, 5: // ignore
						WriteBufLn('// dummy 15-' + strhex(j));

						else raise DecompException.Create(
							strcat('Unknown code $15 & @ $&', [loader.ReadByteFrom(loader.ofs - 1), loader.ofs]));
					end;
				end;

				else raise DecompException.Create('Unknown code $15 @ $' + strhex(loader.ofs));
			end;

			// 16 xx - Screen flashes xx times
			$16:
			case game of
				gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Sakura, gid.Sakura98, gid.Tasogare:
				begin
					j := loader.ReadByte;
					{$ifdef enable_decomp_hacks}
					case j of
						// reduce flashing, it's overdone at points
						2..4: dec(j);
						5, 6: j := 3;
					end;
					{$endif enable_decomp_hacks}
					WriteBufLn('gfx.flash ' + strdec(j));
					// In case of CS304, an implicit delay is expected after each flash.
					if (scriptname = 'CS304') and (loader.ofs > $1D00) then WriteBufLn('sleep 400');
				end;

				gid.SexyParfait:
				begin
					// some kinda graphics command?
					j := loader.ReadByte;
					if j <> $A then raise DecompException.Create('Unknown $16 subcode @ $' + strhex(loader.ofs));

					WriteBuf('//dummy $16 0A //');
					for l := 3 downto 0 do begin
						txt := strhex(loader.ReadWord, 4);
						WriteBuf(' $' + txt);
					end;
					for l := 4 downto 0 do begin
						txt := strhex(loader.ReadByte);
						if length(txt) = 1 then txt := '0' + txt;
						WriteBuf(' $' + txt);
					end;
					WriteBufLn('');
				end;

				else raise DecompException.Create('Unknown code $16 @ $' + strhex(loader.ofs));
			end;

			// 17 xx - Pause for xx desisecs (or until an impatient key pressed)
			$17:
			case game of
				gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Sakura, gid.Sakura98, gid.Tasogare:
				begin
					j := loader.ReadByte;
					WriteBufLn('sleep ' + strdec(j * 100));
				end;

				else raise DecompException.Create('Unknown code $17 @ $' + strhex(loader.ofs));
			end;

			// 18 xx - Screen shakes vertically xx times
			$18:
			case game of
				gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.Sakura, gid.Sakura98, gid.Tasogare:
				begin
					j := loader.ReadByte;
					l := 1;
					while l * l < j do inc(l);
					l := (l - 1) shr 1;
					if (scriptname = 'CSA09') or (scriptname = 'CS507_D') then
						_WriteBash(1)
					else
						_WriteThwomp(l);
				end;

				else raise DecompException.Create('Unknown code $18 @ $' + strhex(loader.ofs));
			end;

			// 19 - Clear textbox immediately
			$19:
			case game of
				gid.Eden, gid.FromH, gid.Hohoemi, gid.Majokko, gid.SanShimai, gid.SexyParfait, gid.Tasogare:
				begin
					WriteBufLn('tbox.clear');
					implicitWaitkey := 0;
				end;

				else raise DecompException.Create('Unknown code $19 @ $' + strhex(loader.ofs));
			end;

			// 1E xx - unknown
			$1E:
			case game of
				gid.Deep:
				begin
					j := loader.ReadByte;
					WriteBufLn('//dummy $1E v' + strhex(j));
				end;

				else raise DecompException.Create('Unknown code $1E @ $' + strhex(loader.ofs));
			end;

			// F3 xx - unknown
			$F3:
			case game of
				gid.Deep:
				begin
					j := loader.ReadByte;
					WriteBufLn('//dummy $F3-$' + strhex(j));
				end;

				else raise DecompException.Create('Unknown code $F3 @ $' + strhex(loader.ofs));
			end;

			// ASCII/Shift-JIS text output
			$0A, $20..$EF: _DoTextOutput;

			else raise DecompException.Create(strcat('Unknown code $& @ $&', [i, loader.ofs - 1]));
		end;

	end;

	ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);

	// Save the built script.
	SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);

	result := TRUE;
end;

