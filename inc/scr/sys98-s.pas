{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_Sys98_S(const loader : TFileLoader; const outputfile : UTF8string; game : gid) : boolean;
// Reads the indicated System-98 bytecode file, and saves it in outputfile as a plain text sakurascript file.
// Throws an exception in case of errors.
var scriptname : UTF8string;
	verb : UTF8string = '';
	startofs : dword;
	choicelist : array[0..9] of UTF8string; // remember the most recently printed choice string set
	choiceresult : array[0..$F] of record // remember first order choice landing targets, to attach to second order
		txt : UTF8string;
		ofs : dword;
	end;
	clcount, crcount : byte;
	firstorder : boolean = TRUE;
	isWindy : boolean = FALSE; // sys98 for Windows
	longvars : word; // below this var index, signed 16-bit; equal or above, signed 32-bit

	{$ifdef enable_decomp_hacks}
	procedure _ApplyHacks(const src : TFileLoader);
	var w : word;

		procedure _LoadHigh;
		begin
			with ScriptWriter do begin
				src.Pad($4000, 0);
				// Hack: Special processing for the set of entry jumps to make it nicer.
				src.ofs := $4100;
				SetLineOfs($4100);
				WriteBuf('casegoto $i ."');
				while (src.readp + 4 < src.endp) and (LEtoN(src.ReadWord) = $A) do begin
					w := LEtoN(src.ReadWord);
					AddJump(w);
					WriteBuf(strhex(w, 4) + ':');
				end;
				currentLine[currentLen - 1] := '"';
				WriteBufLn();
			end;
		end;

	begin
		with ScriptWriter do case game of
			gid.GaoGao3:
			case scriptname of
				// Hack: Forward-declare backward jump.
				'S0': AddJump($1AE);
			end;

			gid.Lilith:
			case scriptname of
				// Hack: Separate narration from dialogue line.
				'L0_00': if src.ReadByteFrom($22C7) = $D then byte(src.PtrAt($22C7)^) := 1;
				// Hack: Mark some unused script references and forward-declared stuff as noncode.
				'L0_02': AddNonCodeSection($92A);
				'LA_05': AddNonCodeSection($418);
				'LA_06': AddJump($10C);
				'LILITH':
				begin
					// Hack: Intercept main menu - "return" instead of goto. (We call from custom main.)
					if src.ReadDwordFrom($2CC) = BEtoN(dword($0A00E202)) then
						dword(src.PtrAt($2CC)^) := BEtoN(dword($55005500));
					AddJump($464);
				end;

				'LI_SUB': begin
					// Hack: Put shared code into high memory.
					_LoadHigh;
					// Start normal parsing after the jumps, graphic names, choice rects.
					startofs := $4886;
					// Hack: Forward-declare jumps.
					AddJump($6C57);
					AddJump($7854);
				end;

				'OPENDEMO': AddNonCodeSection($1124);
			end;

			gid.Mayclub98, gid.Mayclub_w, gid.Mayclub_e:
			begin
				if scriptname = 'MAY_SUB' then begin
					// Hack: Put shared code into high memory.
					_LoadHigh;
					// Start normal parsing after the jumps, graphic names, choice rects.
					if game = gid.Mayclub98 then
						startofs := $455F
					else
						startofs := $4562;
				end

				else case game of
					gid.Mayclub98:
					case scriptname of
						// Hack: Mark unused script references and forward-declared stuff as noncode.
						'B02': if LEtoN(src.ReadWordFrom($A90)) = $5A01 then AddNonCodeSection($A90);
						'H07': if LEtoN(src.ReadWordFrom($676)) = $5A01 then AddNonCodeSection($676);
						'Z01': begin
							if LEtoN(src.ReadWordFrom($1FC)) = $A992 then AddNonCodeSection($1FC);
							if LEtoN(src.ReadWordFrom($2BE)) = 8 then AddJump($2BE);
							if src.ReadDwordFrom($870) = BEtoN(dword($18000000)) then AddJump($870);
						end;
						// Hack: Intercept "start" exit - "return" instead of goto "open". (We call from custom main.)
						'START': if LEtoN(src.ReadWordFrom($56B)) = $1F then dword(src.PtrAt($56B)^) := NtoLE($550055);
					end;

					gid.Mayclub_e:
					case scriptname of
						// Hack: Mark some unused script references and forward-declared stuff as noncode.
						'B02': AddNonCodeSection($962);
						'G15': if src.ReadWordFrom($DC) = 0 then AddNonCodeSection($DC); // only applies to override script
						'H07': AddNonCodeSection($56A);
						'OPEN': begin AddNonCodeSection($17E1); AddNonCodeSection($1B8C); end;
						'Z01': begin AddNonCodeSection($102); AddJump($1A8); AddJump($704); end;
					end;
				end;
			end;

			gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
			begin
				if scriptname = 'MUG_SUB' then begin
					// Hack: Put shared code into high memory.
					_LoadHigh;
					// Start normal parsing after the jumps, graphic names, choice rects.
					startofs := $46A2;
				end

				else case game of
					gid.Nocturn98:
					case scriptname of
						'A_05': AddJump($120);
						'C_08': begin AddNonCodeSection($3CC); AddNonCodeSection($458); end;
						'SS2': AddJump($49E);
						'U600': begin AddNonCodeSection($1440); end;
						// Hack: Stop interstitial from re-printing weirdly. Shoddy scripting.
						'7_00', '8_00', '9_00', '9_34', 'A_00', 'C_00': WriteBufLn('$v12 := 14');
					end;

					gid.Nocturn_e:
					case scriptname of
						// Hack: Mark some unused script references and forward-declared stuff as noncode.
						'0_00': AddNonCodeSection($30);
						'6_34': AddNonCodeSection($540);
						'6_35': AddNonCodeSection($17E);
						'SS2': AddJump($3AA);
						'SS9': AddJump($698);
						'U600': begin AddJump($7C); AddNonCodeSection($156C); end;
						'U700': AddJump($3A);
						// Hack: Keep dialogue line separate from previous other text.
						'3_06': if src.ReadByteFrom($E88) = $D then byte(src.PtrAt($E88)^) := 1;
						'5_33': if src.ReadByteFrom($2AC) = $D then byte(src.PtrAt($2AC)^) := 1;
						// Hack: Fix overflowing interstitials.
						'9_34': if src.ReadByteFrom($2BE) = $20 then byte(src.PtrAt($2BE)^) := $D;
						'A_00': if src.ReadByteFrom($E4) = $46 then byte(src.PtrAt($E4)^) := $45;
						'B_00':
						begin
							AddNonCodeSection($1B1);
							if src.ReadByteFrom($42) = $B1 then byte(src.PtrAt($42)^) := $BB;
						end;
						'C_01': if src.ReadByteFrom($43F) = $20 then byte(src.PtrAt($43F)^) := $D;
						'C_02': if src.ReadByteFrom($2B7) = $20 then byte(src.PtrAt($2B7)^) := $D;
						'C_03': if src.ReadByteFrom($2B9) = $20 then byte(src.PtrAt($2B9)^) := $D;
						'C_04': if src.ReadByteFrom($2AB) = $20 then byte(src.PtrAt($2AB)^) := $D;
						'C_05': if src.ReadByteFrom($2F1) = $20 then byte(src.PtrAt($2F1)^) := $D;
						'C_06': if src.ReadByteFrom($637) = $20 then byte(src.PtrAt($637)^) := $D;
						'C_07': if src.ReadByteFrom($341) = $20 then byte(src.PtrAt($341)^) := $D;
						'C_08':
						begin
							AddNonCodeSection($2D2);
							if src.ReadByteFrom($5B5) = $20 then byte(src.PtrAt($5B5)^) := $D;
						end;
						// Hack: Stop interstitial from re-printing weirdly. Shoddy scripting.
						'7_00','8_00': WriteBufLn('$v12 := 14');
					end;
				end;
			end;
		end;
	end;
	{$endif}

	procedure _Deobfuscate(const src : TFileLoader);
	var endp : pointer;
	begin
		case game of
			// Todo: what do the Japanese Windows ports do?
			gid.Mayclub_e, gid.Nocturn_e: begin
				// The English ports do +1 and -1 on alternate bytes.
				endp := src.endp - 1;
				while src.readp < endp do begin
					{$push}{$R-}
					dec(byte(src.readp^));
					inc(src.readp);
					inc(byte(src.readp^));
					inc(src.readp);
					{$pop}
				end;
				if src.readp < src.endp then begin
					{$push}{$R-}
					dec(byte(src.readp^));
					{$pop}
				end;
			end
			else begin
				// The PC98 versions just do xor 1 on every byte.
				endp := src.endp - 4;
				while src.readp < endp do begin
					dword(src.readp^) := dword(src.readp^) xor $01010101;
					inc(src.readp, 4);
				end;
				while src.readp < src.endp do begin
					byte(src.readp^) := byte(src.readp^) xor 1;
					inc(src.readp);
				end;
			end;
		end;
	end;

	procedure _TranslateBytecode(const src : TFileLoader);
	var param : array[0..7] of word;
		u : UTF8string = '';
		d : dword;
		cmd, w : word;
		inchoice : boolean = FALSE;
		indrama : boolean = FALSE;
		choicedetour : word = $FFFF;
		q : byte = 0; // there are some commands in the middle of string output, so quote state must carry over (b01.s)

		procedure _ReadParams(num : byte);
		var i : byte;
		begin
			if src.readp + num * 2 > src.endp then
				raise DecompException.Create('oob for $' + strhex(cmd) + ' params');
			for i := 0 to num - 1 do
				param[i] := LEtoN(src.ReadWord);
		end;

		function _CommonRef(ofs : dword) : UTF8string;
		// Sys98 scripts can directly refer to strings in a shared "sub" script loaded in high memory. There's no
		// comfortable way to guarantee during a decomp call that we've seen the sub script yet, so have to hardcode.
		// But there are so many of these! How about converting every common ref to (call getref <address>), since
		// sakurascript can return values from calls... Still a lot of address to string mapping to do, a separate
		// label for each would be memory-wasteful but fast to fetch; may have to extend case/select fibercmd.
		{$note todo: hack commonrefs into script callable}
		begin
			case game of
				gid.Lilith:
				case ofs - $4100 of
					$60: exit('000');
					$F6: exit('003B');
					$120: exit('004D');
					$2A6: exit('021');
					$2AC: exit('022');
					$2B8: exit('024A');
					$2BE: exit('024B');
					$2C4: exit('024C');
					$2CA: exit('025');
					$2D6: exit('027');
					$2DC: exit('028');
					$2E2: exit('029');
					$2E8: exit('030');
					$2EE: exit('031');
					$2F4: exit('032');
					$2FA: exit('033');
					$300: exit('034');
					$312: exit('037');
					$318: exit('038');
					$31E: exit('039');
					$324: exit('040');
					$32A: exit('041');
					$330: exit('042');
					$33C: exit('044');
					$342: exit('045');
					$348: exit('046');
					$34E: exit('047');
					$354: exit('048');
					$35A: exit('049');
					$360: exit('050');
					$366: exit('051');
					$36C: exit('052');
					$384: exit('056');
					$38A: exit('057');
					$3A2: exit('061');
					$3B4: exit('062');
					$3C0: exit('064');
					$3CC: exit('065A');
					$3F0: exit('069');
					$402: exit('072');
					$408: exit('073');
					$42C: exit('079');
					$432: exit('080');
					$438: exit('081');
					$450: exit('085A');
					$47A: exit('091');
					$492: exit('095');
					$4C8: exit('104');
					$4CE: exit('105');
					$516: exit('113');
					$606: exit('132');
					$648: exit('E01');
				end;
				gid.Mayclub98, gid.Mayclub_w, gid.Mayclub_e:
				case ofs - $4100 of
					$60: exit('Z01');
					$84: exit('A03');
					$8A: exit('A04');
					$A2: exit('A06');
					$A8: exit('A07');
					$AE: exit('A08');
					$B4: exit('A09');
					$BA: exit('A10');
					$EA: exit('B12');
					$F0: exit('B13');
					$F6: exit('B14');
					$126: exit('B16');
					$12C: exit('B17a');
					$132: exit('B17b');
					$138: exit('B17c');
					$13E: exit('B18');
					$144: exit('B19');
					$174: exit('C21');
					$17A: exit('C22a');
					$180: exit('C22b');
					$186: exit('C23');
					$18C: exit('C24');
					$192: exit('C25');
					$198: exit('C26');
					$1BC: exit('D28');
					$1C2: exit('D29');
					$1C8: exit('D30');
					$1CE: exit('D31');
					$1D4: exit('D32a');
					$1DA: exit('D33a');
					$1E0: exit('D34');
					$20A: exit('E36');
					$210: exit('E37');
					$216: exit('E38a');
					$21C: exit('E38b');
					$222: exit('E39');
					$228: exit('E40');
					$22E: exit('E41');
					$258: exit('F36');
					$25E: exit('F37');
					$264: exit('F38');
					$26A: exit('F39');
					$270: exit('F40');
					$2A0: exit('G42');
					$2A6: exit('G43a');
					$2AC: exit('G43b');
					$2B2: exit('G43c');
					$2B8: exit('G44');
					$2BE: exit('G45');
					$2C4: exit('G46');
					$2F4: exit('H48');
					$2FA: exit('H49a');
					$300: exit('H50a');
					$306: exit('H50b');
					$30C: exit('H50c');
					$312: exit('H51');
					$318: exit('H52');
					$31E: exit('H53');
					$35A: exit('I55a');
					$360: exit('I55b');
					$36C: exit('I56');
					$372: exit('I57');
					$378: exit('I58');
					$37E: exit('I59');
					$384: exit('I60');
					$38A: exit('I61');
					$390: exit('I62');
					$402: exit('Z74');
					$420: exit('D32b');
					$426: exit('D33b');
					$42C: exit('H49b');
				end;

				gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
				case ofs - $4100 of
					$8C: exit('05N');
					$E7: exit('011');
					$EE: exit('11N');
					$15E: exit('17A');
					$165: exit('17B');
					$16C: exit('17C');
					$173: exit('018');
					$18F: exit('022');
					$196: exit('023');
					$19D: exit('024');
					$1E3: exit('027');
					$1F8: exit('30A');
					$1FF: exit('30B');
					$206: exit('30C');
					$20D: exit('30D');
					$214: exit('031');
					$21B: exit('032');
					$229: exit('034');
					$230: exit('035');
					$237: exit('036');
					$268: exit('038');
					$26F: exit('039');
					$276: exit('040');
					$27D: exit('041');
					$284: exit('042');
					$2A7: exit('044');
					$2AE: exit('045');
					$2B5: exit('046');
					$2BC: exit('047');
					$2C3: exit('048');
					$2DF: exit('050');
					$2E6: exit('051');
					$325: exit('055');
					$32C: exit('056');
					$333: exit('057');
					$34F: exit('059');
					$356: exit('60A');
					$35D: exit('60B');
					$364: exit('60C');
					$36B: exit('061');
					$372: exit('062');
					$379: exit('063');
					$39C: exit('065');
					$3A3: exit('066');
					$3AA: exit('67A');
					$3B1: exit('67B');
					$3B8: exit('67C');
					$3BF: exit('068');
					$3C6: exit('069');
					$3CD: exit('070');
					$3D4: exit('071');
					$3E2: exit('2A2');
					$3F0: exit('2B2');
					$3FE: exit('2C2');
					$40C: exit('2D2');
					$413: exit('073');
					$41A: exit('074');
					$421: exit('075');
					$428: exit('076');
					$42F: exit('077');
					$436: exit('078');
					$459: exit('080');
					$460: exit('081');
					$467: exit('082');
					$491: exit('085');
					$498: exit('86A');
					$49F: exit('86B');
					$4A6: exit('86C');
					$4AD: exit('86D');
					$4B4: exit('087');
					$4BB: exit('088');
					$4EC: exit('091');
					$4F3: exit('092');
					$4FA: exit('093');
					$508: exit('095');
					$50F: exit('96A');
					$516: exit('96B');
					$51D: exit('097');
					$52B: exit('098');
					$532: exit('099');
					$539: exit('100');
					$540: exit('TTL');
					$547: exit('WIN');
					$54E: exit('BK0');
					$55C: exit('97B');
					$58D: exit('101');
				end;
			end;
			raise DecompException.Create(strcat('unk common ref $& @ $&', [ofs, src.ofs - 2]));
		end;

		procedure _CaseJump(skipfirst : boolean);
		var maxp : pointer;
			nearjump : word = $FFFF;
			_noncode : word = $FFFF;
			i, j : word;
			b : byte = 0;
		begin
			maxp := src.readp + 32;
			_ReadParams(1);
			with ScriptWriter do begin
				WriteBuf('casegoto $v' + strhex(param[0]) + ' ."');
				try
					// There's no explicit end for this array of addresses, so have to apply some fuzzy logic...
					// Let nearjump := the closest already defined jump target, after the current offset.
					// This address array must end at or before that jump target.
					if jumpCount <> 0 then for i := jumpCount - 1 downto 0 do
						if jumpList[i] > src.ofs then nearjump := jumpList[i] else break;
					// If there's a known non-code section coming up, this array must end at or before that.
					if nonCodeCount <> 0 then _noncode := nonCode[0];
					// The first pointer may be totally invalid. For example, nocturn 1_02 Check:Desk tracks how many
					// times the action has been attempted, but increments the counter before the case jump, so the
					// counter can never be 0. Thus it would never use the first jump address, which is 0 or invalid.

					while src.readp + 2 <= src.endp do begin
						i := LEtoN(src.ReadWord);
						j := LEtoN(word(src.readp^));
						// If a target address points past script end, obviously can't be part of the array.
						if (i >= src.buffySize)
						// A word pair 19,AE is a common array end in GaoGao 3.
						or ((i = $19) and (j = $AE))
						// Two equal low addresses probably means a 0003 0003 etc command, probably end of array.
						or ((i < 4) and (i = j)) then begin dec(src.readp, 2); break; end;

						if skipfirst then
							skipfirst := FALSE
						else begin
							AddJump(i);
							// Remember the closest jump target seen in this array. If we reach it, must be end of array.
							if (i >= src.ofs) and (i < nearjump) then nearjump := i;

							case game of
								gid.Lilith, gid.Mayclub98, gid.Mayclub_w, gid.Mayclub_e, gid.Nocturn98,
								gid.Nocturn_w, gid.Nocturn_e:
								if (param[0] = $10) and (i <> 0) and (b < clcount) then begin
									AddComment('### ' + choicelist[b], i);
									if (firstorder) and (crcount < length(choiceresult)) then begin
										choiceresult[crcount].txt := choicelist[b] + ':';
										choiceresult[crcount].ofs := i;
										inc(crcount);
									end;
									inc(b);
								end;
							end;
						end;
						WriteBuf(strhex(i, 4) + ':');

						if (src.ofs >= nearjump) or (src.ofs >= _noncode) then break;
						// If we've gone past a reasonable 32 bytes and the word array isn't strictly ascending,
						// assume we've read too far. This array will need a terminator hack.
						if (src.readp > maxp) and (j < i) and (nearjump - src.ofs > 12)
						and ((game <> gid.Lilith) or ((scriptname <> 'L6_03') and (scriptname <> 'LI_SUB')))
						then begin
							currentLine[currentLen] := #0;
							raise DecompException.Create(strcat('%... too much @ $&', [PChar(currentLine), src.ofs]));
						end;
					end;
				finally
					currentLine[currentLen - 1] := '"';
					WriteBufLn();
				end;
			end;
			// Choice landing comments have been generated, don't need choicelist anymore.
			case game of
				gid.Lilith, gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e: if param[0] = $10 then clcount := 0;
			end;
		end;

		procedure _BeginDrama;
		begin
			ScriptWriter.WriteBufLn(
				'(tbox.clear) ($_mainbox := DRAMABOX) tbox.popin time:0');
			indrama := TRUE;
		end;

		procedure _EndDrama;
		begin
			indrama := FALSE;
			// Hack: Extra delay to let user read long drama text.
			if scriptname = '7_42' then ScriptWriter.WriteBuf('(sleep 6000) ');
			ScriptWriter.WriteBufLn('$_mainbox := MAINBOX');
		end;

		procedure _TextOutput(fromofs : dword);
		var txt : UTF8string = '';
			len, stashofs, labelofs, i : dword;
			b : byte;
			u8c : string[7];
			printed : boolean = FALSE;

			procedure _PrintOut;
			begin
				printed := TRUE;
				with ScriptWriter do begin
					if inchoice then begin
						WriteBuf('choice.set "');
						while (len <> 0) and (txt[len] = ' ') do dec(len); // trim right whitespace
						if clcount >= length(choicelist) then raise DecompException.Create('too many choices');
						// Sometimes a normal narration/dialogue line is misdetected as a choice option...
						if ((verb <> '') and (len > 42)) or (len > 80) then
							decomp_logger('[!] choice misdetect @ $' + strhex(stashofs - 2));
						choicelist[clcount] := verb + copy(txt, 1, len);
						WriteBuf(choicelist[clcount]);
						WriteBufLn('"');
						inc(clcount);
					end
					else if len <> 0 then begin
						WriteBuf('"');
						inc(len);
						txt[len] := '"';
						WriteBufLn(copy(txt, 1, len));
					end;
					SetLineOfs(labelofs);
					len := 0;
				end;
			end;

			procedure _EatQuote;
			begin
				q := 0;
				if (src.readp < src.endp) and (char(src.readp^) = '"') then begin
					q := 1;
					inc(src.readp);
				end
				else if (src.readp + 1 < src.endp) and (byte(src.readp^) = $81) then begin
					q := byte((src.readp + 1)^);
					if q in [$75, $77] then begin // Japanese begin quote, remember which was used
						inc(q);
						inc(src.readp, 2);
					end
					else q := 0;
				end;
			end;

		begin
			stashofs := src.ofs;
			src.ofs := fromofs;
			if fromofs > stashofs then ScriptWriter.AddNonCodesection(fromofs);
			with ScriptWriter do labelofs := labelList[lineIndex];

			len := 0; setlength(txt, 240);
			with ScriptWriter do while src.readp < src.endp do begin
				b := src.ReadByte;
				case b of
					$D:
					if inchoice then begin
						if len <> 0 then _PrintOut;
					end
					else begin
						// If the string so far is [name], it's a dialogue title.
						if (len > 1) and (txt[1] = '[') and (txt[len] = ']') then begin
							dec(len);
							i := 2;
							if game = gid.Mayclub_e then begin
								while (len <> 0) and (txt[len] = ' ') do dec(len); // trim right whitespace
								while (i < len) and (txt[i] = ' ') do inc(i); // skip left whitespace
							end;
							len := len - i + 1;
							MemCopy(@txt[i], @txt[1], len);
							inc(len); txt[len] := '\'; // [Bob] -> Bob\:
							inc(len); txt[len] := ':';

							// If a quote immediately follows, skip it.
							_EatQuote;
						end

						// If there's a string variable printout right after this, it's probably a new dialogue line.
						else if byte(src.readp^) = 4 then begin
							_PrintOut;
							WriteBufLn('...');
							SetLineOfs(labelofs);
						end

						// Keep the newline if it's the start of a string or we're doing a dramatic interstitial.
						else if (indrama) or (len = 0) then begin
							inc(len);
							txt[len] := '\';
							inc(len);
							txt[len] := 'n';
						end

						else begin
							// If there's a double-width colon soon after a newline, it's probably a dialogue line.
							case game of
								gid.Nocturn98:
								begin
									i := src.RemainingBytes;
									if i > 16 then i := 16;
									while i <> 0 do begin
										dec(i);
										if (char((src.readp + i)^) = #$81)
										and (char((src.readp + i + 1)^) = #$46)
										then begin
											_PrintOut;
											WriteBufLn('...');
											SetLineOfs(labelofs);
											while char(src.readp^) = ' ' do inc(src.readp);
											i := $FF;
											break;
										end;
									end;
									if i = $FF then continue;
								end;
							end;
							case game of
								// Treat newline as a space in English since we have automatic linewrapping.
								gid.Mayclub_e, gid.Nocturn_e:
								begin
									inc(len);
									txt[len] := ' ';
								end;
								// In Japanese, swallow the newline since kana don't need spaces.
							end;
						end;
					end;

					0..3, 6..7, 9, $A..$C, $E, $F:
					begin
						if len <> 0 then _PrintOut;
						case b of
							0: break;
							1:
							begin
								WriteBufLn('...');
								q := 0;
								// Skip newline directly after waitkey.
								if (src.readp < src.endp) and (byte(src.readp^) = $D) then inc(src.readp);
							end;
							2: WriteBufLn('waitkey noclear=1');
							3: begin b := src.ReadByte; WriteBufLn('//color ' + strdec(b)); end;
							6, $B, $C:
							begin
								b := src.ReadByte;
								WriteBuf('//show F');
								if b < 100 then WriteBuf('0');
								if b < 10 then WriteBuf('0');
								WriteBufLn(strdec(b) + ' name:P');
							end;
							7:
							begin
								WriteBufLn('gfx.remove P');
								// Skip newline directly after portrait clear.
								if (src.readp < src.endp) and (byte(src.readp^) = $D) then inc(src.readp);
							end;
							9: WriteBufLn('sleep 690');
							$A: ;
							$E: WriteBufLn('//dummy 0E in text, $' + strhex(src.ReadByte));
							$F: begin
								b := src.ReadByte;
								if b = 1 then begin
									if indrama then _EndDrama else _BeginDrama;
								end
								else begin
									WriteBuf('//text code 0F-' + strhex(b));
									if b = 0 then WriteBuf('-' + strhex(src.ReadByte));
									WriteBufLn('');
								end;
							end;
						end;
						SetLineOfs(labelofs);
					end;

					4, $1B: // print string variable, typically a character name
					begin
						if len + 8 >= txt.Length then setlength(txt, txt.Length + 128);
						b := src.ReadByte;
						inc(len); txt[len] := '\';
						inc(len); txt[len] := '$';
						inc(len); txt[len] := 's';
						inc(len); txt[len] := hextable[b shr 4];
						inc(len); txt[len] := hextable[b and $F];
						inc(len); txt[len] := ';';
						// If this was the first thing on a line, assume it's a dialogue title.
						if (NOT inchoice) and (len = 6) then begin
							inc(len); txt[len] := '\';
							inc(len); txt[len] := ':';
							if byte(src.readp^) = $D then inc(src.readp); // skip newline
							// If a quote immediately follows, skip it.
							_EatQuote;
						end;
					end;

					5: // print number variable
					begin
						if len + 8 >= txt.Length then setlength(txt, txt.Length + 128);
						i := LEtoN(src.ReadWord);
						inc(len); txt[len] := '\';
						inc(len); txt[len] := '$';
						inc(len); txt[len] := 'v';
						if i >= $1000 then begin inc(len); txt[len] := hextable[i shr 12]; end;
						if i >= $100 then begin inc(len); txt[len] := hextable[(i shr 8) and $F]; end;
						inc(len); txt[len] := hextable[(i shr 4) and $F];
						inc(len); txt[len] := hextable[i and $F];
						inc(len); txt[len] := ';';
					end;

					8, $10..$1A, $1C..$1F:
					raise DecompException.Create(
						strcat('unk $& in txt [%] @ $&', [b, copy(txt, 2, len - 2), src.ofs - 1]));

					ord('"'):
					if q = 0 then begin
						// Rarely a dialogue line may appear in this form: `Blob   : "Kyuuu!"`
						if (len in [8..17]) and (copy(txt, len - 2, 3) = ' : ') then begin
							dec(len, 3);
							while (len <> 0) and (txt[len] = ' ') do dec(len); // trim right whitespace
							inc(len);
							txt[len] := '\';
							inc(len);
							txt[len] := ':';
							q := 1;
							continue;
						end;

						if len + 2 >= txt.Length then setlength(txt, txt.Length + 128);
						inc(len);
						txt[len] := '\';
						inc(len);
						txt[len] := '"';
					end;

					else begin
						if len + 8 >= txt.Length then setlength(txt, txt.Length + 128);
						case b of
							// Emotes. Some are translated to characters, others use the shared _emotes from winterq.
							$85, $EB: begin
								i := (b shl 8) or src.ReadByte;
								case i of
									$854F: u8c := #$E5#$86#$86; // yen sign
									$EBB4, $EBB5, $EBB8: u8c := '!!';
									$EBB6: u8c := '!?';

									$EB9F, $EBC2: u8c := '\&3;'; // sweat drops arcing
									$EBA0, $EBC1: u8c := '\&5;'; // large sweat drop
									$EBC3: u8c := '\&9;'; // sweat beads
									$EBA1: u8c := '\&4;'; // star
									$EBA3: u8c := '\&6;'; // happy face ^_^
									$EBA4: u8c := '\&7;'; // crying face T.T
									$EBA5, $EBA6: u8c := '\&8;'; // angry cross-popping veins
									$EBA7, $EBE1, $EBE2: u8c := '\&14;'; // sunflower or sunny face
									$EBA9, $EBAA, $EBAB: u8c := '\&0;'; // heart
									$EBAD: u8c := '\&13;'; // middle finger
									$EBAE: u8c := '\&11;'; // thumb up
									$EBAF: u8c := '\&12;'; // peace sign V-fingers
									$EBB1, $EBB2: u8c := '\&1;'; // 8th note
									$EBB3: u8c := '\&2;'; // beamed pair of 8th notes
									$EBE0: u8c := '~\&1;'; // tilde and quarter note
									$EBBA: u8c := '\&15;'; // gloomy face
									$EBC4: u8c := '\&10;'; // huff cloud

									else u8c := '[' + strhex(i, 4) + ']';
								end;
								move(u8c[1], txt[len + 1], length(u8c));
								inc(len, length(u8c));
								// If there's a newline right after an emote in normal text, don't omit it.
								if (NOT inchoice) and (src.readp < src.endp) and (byte(src.readp^) = $D) then begin
									inc(src.readp);
									inc(len);
									txt[len] := '\';
									inc(len);
									txt[len] := 'n';
								end;
							end;

							// Double-byte JIS.
							$81..$84, $87..$9F, $E0..$EA:
							begin
								i := (b shl 8) or src.ReadByte;

								if b = $81 then case byte(i) of
									// Hack: Replace Japanese dialogue title.
									$46:
									begin
										while (len <> 0) and (txt[len] = ' ') do dec(len);
										inc(len); txt[len] := '\';
										inc(len); txt[len] := ':';
										_EatQuote;
										continue;
									end;
									$6D: i := ord('[');
									$6E: i := ord(']');
									// Hack: Ignore Japanese end quote.
									$76, $78: if q = byte(i) then continue;
									// Hack: Skip all double-width spaces, they're only really used for alignment.
									// Except in Lilith they're actually used for clarity.
									$40: if (NOT indrama) and ((inchoice) or (game <> gid.Lilith)) then continue;
								end;

								u8c := GetUTF8(i);
								move(u8c[1], txt[len + 1], length(u8c));
								inc(len, length(u8c));
							end;

							// Single-byte katakana.
							$A1..$DF:
							begin
								u8c := GetUTF8(b);
								move(u8c[1], txt[len + 1], length(u8c));
								inc(len, length(u8c));
							end;

							else begin
								// Plain ascii.

								{$ifdef enable_decomp_hacks}
								if b = ord(' ') then begin
									// In Lilith spaces are all hardcoded indentation, scrap em.
									// Also ignore all spaces at start of choice strings.
									if (game = gid.Lilith) or ((inchoice) and (len = 0)) then continue;

									// Scrap double-space in choice text, just an alignment hack.
									if (inchoice) and (src.readp < src.endp) and (char(src.readp^) = ' ') then
										inc(src.readp);
								end

								// Catch " -- " and " - " and replace it with a more stylish en-dash.
								else if (b = ord('-')) and (len <> 0) and (txt[len] = ' ') then begin
									if (char(src.readp^) = '-') and (char((src.readp + 1)^) = ' ') then inc(src.readp);
									if char(src.readp^) = ' ' then begin
										inc(len); byte(txt[len]) := $E2;
										inc(len); byte(txt[len]) := $80;
										inc(len); byte(txt[len]) := $93;
									end;
								end;
								{$endif enable_decomp_hacks}

								inc(len);
								byte(txt[len]) := b;
							end;
						end;
					end;
				end;
			end;
			if NOT printed then ScriptWriter.WriteBufLn('// print ""'); // sometimes needed for jump landing
			src.ofs := stashofs;
			if src.ofs >= choicedetour then begin choicedetour := $FFFF; inchoice := FALSE; end;
			ScriptWriter.SetLineOfs(stashofs);
		end;

	begin
		ScriptWriter.WriteBufLn('choice.reset');
		crcount := 0;
		with ScriptWriter do while src.readp + 1 < src.endp do begin
			if (nonCodeCount <> 0) and (nonCode[0] <= src.ofs) then begin
				Assert(src.ofs = nonCode[0], strcat('Noncode section from $&, readofs $&', [nonCode[0], src.ofs]));
				d := src.ofs;
				if PassNonCode(d) then break;
				src.ofs := d;
				SetLineOfs(d);
			end;

			if src.ofs >= choicedetour then inchoice := TRUE;

			w := BEtoN(word(src.readp^));
			cmd := LEtoN(src.ReadWord);

			// If we see a 01/02 followed by an alphanumeric, assume we're already in noncode.
			if ((byte(cmd) in [1,2]) and (byte(cmd shr 8) in [48..57, 65..90, 97..122]))
			// Or sometimes there's shift-jis without warning, assume we've stumbled into noncode.
			or ((w >= $8140) and (w <= $EAA4) and (w and $FF <> 0))
			// Or it could be an FF sequence for no reason.
			or (w = $FFFF)
			then begin
				if nonCodeCount = 0 then
					AddNonCodeSection(src.ofs)
				else
					nonCode[0] := src.ofs;
				continue;
			end;

			case cmd of
				0: WriteBufLn('//dummy $00, quit game');

				1: // Defining a textured textbox?
				begin
					_ReadParams(5);
					u := src.ReadStringFrom(param[4]);
					WriteBufLn(strcat('//dummy $01, box $&: $& $& $&, %', [param[0], param[1], param[2], param[3], u]));
				end;

				2: // Draw graphic.
				begin
					if isWindy then _ReadParams(7) else _ReadParams(6);
					if param[4] < src.buffySize then begin
						u := src.ReadStringFrom(param[4] + 1);
						if param[4] > src.ofs then AddNonCodeSection(param[4]);
					end else
						u := _CommonRef(param[4]);
					// Remove suffix if present, add quotes if name starts with a digit.
					d := pos('.', u);
					if d <> 0 then setlength(u, d - 1);
					if (u <> '') and (u[1] in ['0'..'9']) then u := '"' + u + '"';

					if param[5] = 0 then WriteBuf('(transition fade) gfx.clearall' + LineEnding);
					if u = 'BK0' then WriteBuf('//'); // don't draw viewframe
					WriteBuf('show ' + u);
					if param[5] = 0 then
						WriteBuf(' bkg')
					else if (param[2] = 0) and (param[3] = 2)
					or (((game = gid.Nocturn_e) or (game = gid.Nocturn98)) and (param[2] = 0)) then begin
						WriteBuf(' sprite');
						case game of
							gid.Mayclub98, gid.Mayclub_w, gid.Mayclub_e: WriteBuf(' name:NPC');
						end;
					end
					else if (game = gid.Nocturn_e) or (game = gid.Nocturn98) then // treat overlays as composite bkgs
						WriteBuf(' bkg')
					else
						WriteBuf(' overlay');
					case game of gid.Mayclub98, gid.Nocturn98: param[0] := param[0] shl 3; end; // x * 8 on pc98

					{$ifdef enable_decomp_hacks}
					// Drawing an in-frame image in fullscreen context: remove frame offset.
					// This is a scripting inconsistency in Nocturn 7_38 and 8_32.
					if (param[0] = 112) and (param[1] = 8) and (param[2] = 0) then begin
						param[0] := 0; param[1] := 0;
					end;
					{$endif enable_decomp_hacks}

					with gameConst do begin
						if (param[0] <> 0) and (baseResXP <> 0) then WriteBuf(strcat(' x:%', [param[0] / baseResXP]));
						if (param[1] <> 0) and (baseResYP <> 0) then WriteBuf(strcat(' y:%', [param[1] / baseResYP]));
					end;
					WriteBuf(strcat(' // & &, & &, %, &',
						[param[0], param[1], param[2], param[3], strhex(param[4], 4), param[5]]));
					if isWindy then WriteBuf(' ' + strhex(param[6]));
					WriteBufLn('');
				end;

				3: // Choose text print position, maybe init multiple choice.
				begin
					_ReadParams(6);
					WriteBufLn(strcat('// set box & rect: x=& y=&, w=& h=&, &',
						[param[0], param[1], param[2], param[3], param[4], param[5]]));

					case game of
						gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
						if (param[0] = 3) and (scriptname <> 'MUG_SUB') then begin
							_BeginDrama;
							continue;
						end;
					end;

					if indrama then begin
						_EndDrama;
					end;
					if (param[0] in [$C,$D]) or (param[2] = $13) then begin
						// Start of multiple choice?
						if (NOT inchoice) and (scriptname <> 'OPEN')
						and ((param[0] = $D) or (LEtoN(dword(src.readp^)) = $03E80022)) then begin
							inchoice := TRUE;
							firstorder := param[0] <> $D;
							if firstorder then begin
								crcount := 0;
								verb := '';
								WriteBufLn('choice.remove');
							end
							else if crcount <> 0 then begin
								// Get the nearest first-order choice below present ofs to be verb for upcoming nouns.
								cmd := 0; d := 0;
								for w := crcount - 1 downto 0 do
									if (choiceresult[w].ofs < src.ofs) and (choiceresult[w].ofs > d) then begin
										cmd := w;
										d := choiceresult[w].ofs;
									end;
								verb := choiceresult[cmd].txt;
							end
							else begin
								// One or more choices defined, but no case jump? Assume single-verb choice.
								if clcount <> 0 then verb := choicelist[0] + ':';
							end;
							clcount := 0;
						end;
					end;
				end;

				4:
				begin
					_ReadParams(1);

					WriteBuf('tbox.clear');
					case game of
						gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
						case param[0] of
							3: WriteBuf(' DRAMABOX');
							//B: WriteBuf(' FULLBOX');
							$C, $D: WriteBuf(' CHOICEBOX');
						end;
					end;
					WriteBufLn(' // 04-' + strhex(param[0]));
					// Start of multiple choice?
					if (NOT inchoice) and (LEtoN(dword(src.readp^)) = $03E80022) then begin
						WriteBufLn('choice.remove');
						crcount := 0;
						clcount := 0;
						verb := '';
						inchoice := TRUE;
					end;
				end;

				5:
				begin
					_ReadParams(1);
					case game of
						gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
						// Hack: Bail out from location string selection during clickable map, for use in our new map.
						if param[0] = $F then
							WriteBuf('return ')
						else if (param[0] = 3) or ((indrama) and (param[0] = $B)) then
							WriteBuf('(tbox.popout DRAMABOX time:3000) (sleep 3200) tbox.clear DRAMABOX ');
					end;
					WriteBufLn('// 05-' + strhex(param[0]));
				end;

				7,8:
				begin
					_ReadParams(2);
					if cmd = 7 then
						u := 'gfx.remove BLACKOUT'
					else begin
						d := $F;
						if param[0] <> 0 then d := $FFFF; // whiteout!
						u := 'show "|' + strhex(d, 4) + '" name:BLACKOUT parent:"" w:1.0 h:1.0 z:10';
						if indrama then
							WriteBufLn('(waitkey noclear:1) tbox.popout time:' + strdec(param[1] * 400));
					end;
					WriteBufLn(strcat(
						'transition fade time:%' + LineEnding + '% // $&-&' + LineEnding + 'sleep',
						[param[1] * 400, u, param[0], param[1]]));

					if indrama then _EndDrama;
				end;

				$A: // unconditional jump
				begin
					_ReadParams(1);
					AddJump(param[0]);
					WriteBufLn('goto ."' + strhex(param[0], 4) + '"');
				end;

				$B: // compare two variables
				begin
					_ReadParams(2);
					WriteBufLn(strcat('$1 := $v& - $v&', [param[0], param[1]]));
				end;

				$C: // compare variable and immediate
				begin
					_ReadParams(2);
					WriteBuf('$1 := $v' + strhex(param[0]));
					if smallint(param[1]) > 0 then
						WriteBuf(' - ' + strdec(param[1]))
					else if param[1] <> 0 then
						WriteBuf(' + ' + strdec(-smallint(param[1])));
					WriteBufLn('');
				end;

				$D..$12: // conditional jump
				begin
					_ReadParams(1);
					AddJump(param[0]);
					WriteBuf('if $1 ');
					case cmd of
						$D: WriteBuf('=');
						$E: WriteBuf('<');
						$F: WriteBuf('>');
						$10: WriteBuf('<=');
						$11: WriteBuf('>=');
						$12: WriteBuf('!=');
					end;
					WriteBufLn(' 0 then goto ."' + strhex(param[0], 4) + '" end');
					if inchoice then choicedetour := param[0];
				end;

				$13: _CaseJump(FALSE);

				$14:
				begin
					_ReadParams(4);
					WriteBufLn(strcat('//dummy $14 palette $v& := RGB $v&, $v&, $v&',
						[param[0], param[1], param[2], param[3]]));
				end;

				$15: // main text output
				begin
					_ReadParams(2);
					if NOT (param[0] in [0, 1, 3, 8..$F]) then
						raise DecompException.Create(strcat('unk $15-& @ $&', [param[0], src.ofs - 6]));
					if param[1] >= src.buffySize then
						raise DecompException.Create(strcat('oob text @ $&', [src.ofs - 6]));
					_TextOutput(param[1]);
				end;

				$18: // let var := immediate
				begin
					_ReadParams(2);
					WriteBufLn(strcat('$v& := %', [param[0], smallint(param[1])]));
					if param[0] >= longvars then raise DecompException.Create('setting high param');
				end;

				$19: // let var := other var
				begin
					_ReadParams(2);
					WriteBufLn(strcat('$v& := $v&', [param[0], param[1]]));
					if (param[0] or param[1]) >= longvars then raise DecompException.Create('setting high param');
				end;

				$1A:
				begin
					_ReadParams(1);
					if param[0] >= src.buffySize then
						raise DecompException.Create('oob song name @ $' + strhex(src.ofs - 4));
					AddNonCodeSection(param[0]);
					u := src.ReadStringFrom(param[0] + 1);
					// Remove suffix if present.
					d := pos('.', u);
					if d <> 0 then setlength(u, d - 1);
					WriteBufLn('mus.play ' + u + ' //' + strhex(src.ReadByteFrom(param[0])));
				end;

				$1B: WriteBufLn('mus.stop time:4000');

				$1C: WriteBufLn('mus.stop time:1200');

				$1D:
				begin
					_ReadParams(1);
					WriteBufLn('$v' + strhex(param[0]) + ' := 3 // music mode GS');
				end;

				$1E:
				begin
					_ReadParams(1);
					WriteBufLn('waitkey // $' + strhex(param[0]));
				end;

				$1F: // jump to start of different script
				begin
					_ReadParams(1);
					if param[0] >= src.buffySize then
						raise DecompException.Create('oob runscript target @ $' + strhex(src.ofs - 4));
					if param[0] >= src.ofs then AddNonCodeSection(param[0]);
					u := src.ReadStringFrom(param[0] + 1);
					// Remove suffix and directory if present.
					d := pos('.', u);
					if d <> 0 then setlength(u, d - 1);
					d := pos('\', u);
					if d <> 0 then u := copy(u, d + 1);

					// Hack: Use our own restart routine.
					case game of
						gid.Mayclub98, gid.Mayclub_w, gid.Mayclub_e, gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
						if u = 'open' then WriteBuf('sys.restartgame //');
						gid.GaoGao3:
						if u = 'wf_open' then WriteBuf('sys.restartgame //');
					end;
					if NOT (u[1] in ['0'..'9']) then
						WriteBuf('goto ' + copy(u, 1, u.Length))
					else
						WriteBuf('goto ."' + copy(u, 1, u.Length) + '"');
					WriteBufLn(' //' + strhex(src.ReadByteFrom(param[0])));
				end;

				$21:
				begin
					_ReadParams(1);
					WriteBufLn('sleep $v' + strhex(param[0]) + ' * 48');
				end;

				$22:
				begin
					_ReadParams(1);
					WriteBufLn('//set text speed $v' + strhex(param[0]));
					case param[0] of
						$3E8:
						if (NOT inchoice) and (LEtoN(word(src.readp^)) = $15) then begin
							// HACK: Subchoices in fullscreen are tricky to detect, so hardcode.
							if (crcount <> 0) and (scriptname = '3_10') then begin
								// Get the nearest first-order choice below present ofs to be verb for upcoming nouns.
								cmd := 0; d := 0;
								for w := crcount - 1 downto 0 do
									if (choiceresult[w].ofs < src.ofs) and (choiceresult[w].ofs > d) then begin
										cmd := w;
										d := choiceresult[w].ofs;
									end;
								verb := choiceresult[cmd].txt;
								firstorder := FALSE;
							end
							else begin
								WriteBufLn('choice.remove');
								verb := '';
								crcount := 0;
								clcount := 0;
								firstorder := TRUE;
							end;
							inchoice := TRUE;
						end;
						else inchoice := FALSE;
					end;
				end;

				$24, $25:
				begin
					_ReadParams(2);
					if cmd = $24 then d := ord('+') else d := ord('-');
					WriteBufLn(strcat('$v& %= %', [param[0], char(d), smallint(param[1])]));
					if param[0] >= longvars then raise DecompException.Create('setting high param');

					if (cmd = $24) and (param[1] = 1)
					and (LEtoN(word(src.readp^)) = $13)
					and (LEtoN(word((src.readp + 2)^)) = param[0])
					then begin
						inc(src.readp, 2);
						_CaseJump(TRUE);
					end;
				end;

				$27, $28:
				begin
					_ReadParams(2);
					if cmd = $27 then d := ord('+') else d := ord('-');
					WriteBufLn(strcat('$v& %= $v&', [param[0], char(d), param[1]]));
					if (param[0] or param[1]) >= longvars then raise DecompException.Create('setting high param');
				end;

				$29:
				begin
					_ReadParams(3);
					WriteBufLn(strcat('//dummy $29 reading from screen? $v&, $v&, $v&', [param[0], param[1], param[2]]));
				end;

				$2A:
				begin
					_ReadParams(3);
					WriteBufLn(strcat('//dummy $2A writing to screen? $v&, $v&, $v&', [param[0], param[1], param[2]]));
				end;

				$2D: // zero out a range of variables
				begin
					_ReadParams(2);
					if param[1] < param[0] then raise DecompException.Create(
						strcat('bad reset $v&..$v& @ $&', [param[0], param[1], src.ofs - 4]));
					WriteBufLn(strcat(
						'($i := 0x&) while $i <= 0x& do ($(v + tohex $i) := 0) $i += 1 end', [param[0], param[1]]));
				end;

				$2F:
				begin
					_ReadParams(8);
					case param[0] of
						0: WriteBuf('(transition f) gfx.clearall // 2F');
						2: WriteBuf('(transition f) gfx.clearkids // 2F');
						else WriteBuf('//blit rect 2F');
					end;
					for d := 1 to 7 do WriteBuf('-' + strhex(param[d]));
					WriteBufLn('');
				end;

				$33:
				begin
					_ReadParams(1);
					WriteBufLn('//plane masking? dummy $33-$v' + strhex(param[0]));
				end;

				$38:
				begin
					_ReadParams(2);
					// Supersakura's variables are longints, Sys98's are smallints. If the first variable starts out
					// negative, ANDing it with any 16-bit value wipes the top 16 bits, unless sign-extended.
					WriteBufLn(strcat('$v& := int16($v& AND $v&)', [param[0], param[0], param[1]]));
				end;

				$39:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('$v& := $v& | $v&', [param[0], param[0], param[1]]));
				end;

				$3A:
				begin
					_ReadParams(8);
					case game of
						// In Nocturn and Mayclub this is found in unnecessary low-level blitting functions.
						gid.Mayclub98, gid.Mayclub_w, gid.Mayclub_e, gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
						WriteBuf('return //hack');
					end;
					WriteBufLn(strcat('//dummy $3A blit from buffer $v& to $v&; ofs $v&,$v& -> $v&,$v&; size $v&,$v&',
						[param[0], param[5], param[1], param[2], param[6], param[7], param[3], param[4]]));
				end;

				$3C:
				begin
					_ReadParams(5);
					WriteBuf('//dummy $3C');
					for d := 0 to 4 do WriteBuf('-' + strhex(param[d]));
					WriteBufLn(' textbox clear?');
				end;

				$3D:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('$v& := int16($v& * $v&)', [param[0], param[0], param[1]]));
					if (param[0] or param[1]) >= longvars then raise DecompException.Create('setting high param');
				end;

				$3E:
				begin
					_ReadParams(2);
					WriteBufLn(strcat(
						'$0 := $v& div $v&' + LineEnding + '$v& := $v& mod $v&' + LineEnding + '$v& := $0',
						[param[0], param[1],	param[1], param[0], param[1],	param[0]]));
					if (param[0] or param[1]) >= longvars then raise DecompException.Create('setting high param');
				end;

				$3F:
				begin
					_ReadParams(2);
					WriteBufLn(strcat( // this is used around multiple choices, multiplication doesn't make sense...
						'$0 := $v& * $v&' + LineEnding + '$v& := $0 >> 16' + LineEnding + '$v& := $0 & 0xFFFF',
						[param[0], param[1],	param[1],	param[0]]));
				end;

				$40:
				begin
					_ReadParams(4);
					if param[2] > src.ofs then // pointer to choice rectangle data, usually at end of file? ignore...
						AddNonCodeSection(param[2]);
					AddJump(param[3]);
					u := '';
					if (NOT firstorder) and (verb <> '') then begin
						u := ' "' + verb;
						u[u.Length] := '"';
					end;
					WriteBufLn(strcat(
						'(choice.get%) $v& := (choice.gethighlight) + 1' + LineEnding +
						'$v& := $v& == 0' + LineEnding +
						'goto ."%" // data %',
						[u, param[0],
						param[1], param[0],
						strhex(param[3], 4), strhex(param[2], 4)]));
					inchoice := FALSE;
					if src.ofs >= choicedetour then choicedetour := $FFFF;
				end;

				$41:
				begin
					_ReadParams(4);
					AddJump(param[3]);
					WriteBufLn(strcat('// poll mouse: $v& := x, $v& := y, unknown 0x&, onclick: %',
						[param[0], param[1], param[2], strhex(param[3], 4)]));
				end;

				$42:
				begin
					_ReadParams(1);
					WriteBufLn('sleep ' + strdec(param[0] * 100));
				end;

				$43:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('//dummy $43, vars $v&, $v&', [param[0], param[1]]));
				end;

				$4A, $4B:
				begin
					_ReadParams(2);
					if dword(param[1] + 2) >= src.buffySize then
						raise DecompException.Create('$4A/B filename oob @ $' + strhex(src.ofs - 4));
					WriteBufLn(strcat(
						'//dummy $&-& "%", load memory data', [cmd, param[0], src.ReadStringFrom(param[1] + 1)]));
					if param[1] > src.ofs then AddNonCodeSection(param[1]);
				end;

				$4E:
				begin
					_ReadParams(1);
					WriteBufLn(strcat('//dummy $4E ssg sound effect? $&', [param[0]]));
				end;

				$4F:
				begin
					WriteBuf('//dummy $4F sound effect? $');
					case game of
						gid.GaoGao3, gid.GaoGao4, gid.Lilith, gid.Nocturn98:
						begin
							_ReadParams(1);
							WriteBufLn(strhex(param[0]));
						end;
						else begin
							_ReadParams(2);
							WriteBufLn(strcat('&, $&', [param[0], param[1]]));
						end;
					end;
				end;

				$51:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('$v& := int16($v& % 0x&)', [param[0], param[0], '&', param[1]]));
				end;

				$52:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('$v& := int16($v& | 0x&)', [param[0], param[0], param[1]]));
				end;

				$53:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('$s& := $s& //53', [param[1], param[0]]));
				end;

				$54: // Subroutine call, should be in known range and divisible by 4.
				begin
					w := LEtoN(src.ReadWord);
					if (w >= $4100) and (w <= $415C) and (w and 3 = 0) then begin
						d := (w - $4100) shr 2;
						WriteBufLn(strcat('call subhook % // &', [d, w])); // custom handler
					end
					else if w < src.buffySize then begin
						WriteBufLn('call ."' + strhex(w, 4) + '"');
						AddJump(w);
					end else
						WriteBufLn('//dummy $54-' + strhex(w));
					case game of
						gid.Nocturn98, gid.Nocturn_w: if (indrama) and (w = $4144) then _EndDrama;
					end;
				end;

				$55:
				begin
					WriteBufLn('return');
					// If this is followed by 01-string, that's a noncode section.
					if (src.readp + 2 < src.endp) and (byte(src.readp^) = 1) and (char((src.readp + 1)^) in ['a'..'z'])
					then begin
						AddNonCodeSection(src.ofs);
					end;
				end;

				$56:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('$cmp := $s& == $s&', [param[0], param[1]]));
				end;

				$57:
				begin
					_ReadParams(3);
					WriteBufLn(strcat('//dummy $57 $s& := $s&, only % bytes', [param[0], param[1], param[2]]));
				end;

				$58:
				begin
					_ReadParams(3);
					WriteBufLn(strcat('//dummy $58 $s& += $s&, only % bytes', [param[0], param[1], param[2]]));
				end;

				$59:
				begin
					_ReadParams(2);
					if (param[0] > 255) or (param[1] >= src.buffySize) then
						raise DecompException.Create('bad $59 string set @ $' + strhex(src.ofs - 4));
					u := src.ReadStringFrom(param[1]);
					case game of
						gid.Mayclub_e:;

						gid.Nocturn_e:
						if (u.Length > 2) and (NOT (u[1] in ['*',':'])) then begin
							// Hack: Trim dialogue markers. (Except a few strings are not dialogue markers.)
							w := u.Length;
							while (w <> 0) and (u[w] in [':',' ']) do dec(w);
							d := 1;
							while (d < w) and (u[d] = ' ') do inc(d);
							u := copy(u, d, w - d + 1);
						end;

						else begin
							// Hack: Remove dialogue marker brackets. (square bracket open, closed, wide space)
							d := 1;
							while d + 1 < u.Length do begin
								if u[d] = ' ' then
									inc(d)
								else if (u[d] = #$81) and (u[d + 1] in [#$40, #$6D, #$6E]) then
									inc(d, 2)
								else break;
							end;
							w := u.Length;
							while w > d + 1 do begin
								if u[w] = ' ' then
									dec(w)
								else if (u[w - 1] = #$81) and (u[w] in [#$40, #$6D, #$6E]) then
									dec(w, 2)
								else break;
							end;

							// Hack: Change terminating double-width colon to a dialogue marker.
							if (w - d > 3) and (u[w - 1] = #$81) and (u[w] = #$46) then begin
								dec(w, 2);
								while (w > d) and (u[w] = ' ') do dec(w);
								if (w > d + 1) and (u[w - 1] = #$81) and (u[w] = #$40) then dec(w, 2);
								while (w > d) and (u[w] = ' ') do dec(w);
							end;

							u := GetUTF8(@u[d], w - d + 1);
							if u[1] = #0 then u := '<emotes>';
						end;
					end;
					u := u.Replace(#$D, '\n');
					WriteBufLn(strcat('$s& += :"%"', [param[0], u]));
					if param[1] >= src.ofs then AddNonCodeSection(param[1]);
				end;

				$5A:
				begin
					_ReadParams(1);
					WriteBufLn(strcat('$s& := ""', [param[0]]));
				end;

				$5B:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('$s& := $s&', [param[0], param[1]]));
				end;

				$5E:
				begin
					_ReadParams(5);
					WriteBufLn(strcat('gfx.clearall // $5E buffer & @ %,% %x%',
						[param[0], param[1] shl 3, param[2], param[3] shl 3, param[4]]));
				end;

				$60:
				begin
					_ReadParams(3);
					if dword(param[2] + 2) >= src.buffySize then
						raise DecompException.Create('$60 filename oob @ $' + strhex(src.ofs - 6));
					WriteBufLn(strcat('//dummy $60-&-& "%"', [param[0], param[1], src.ReadStringFrom(param[2] + 1)]));
				end;

				$62:
				begin
					_ReadParams(2);
					case game of
						gid.Mayclub98, gid.Mayclub_w, gid.Mayclub_e, gid.Nocturn98, gid.Nocturn_w, gid.Nocturn_e:
						WriteBuf('gfx.bash time:1200 freq:111000 amp:0.16 angle:0.5 ');
					end;
					WriteBufLn(strcat('// move gfx $62-&-&', [param[0], param[1]]));
				end;

				$68:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('//dummy $68, $v&, $v&', [param[0], param[1]]));
				end;

				$6E:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('//dummy $6E string len calc? $v&, $s&', [param[0], param[1]]));
				end;

				$72:
				begin
					_ReadParams(1);
					if game = gid.GaoGao3 then WriteBuf('return//hack ');
					WriteBufLn('//dummy $72 counter := $v' + strhex(param[0]));
				end;

				$73:
				begin
					_ReadParams(1);
					WriteBufLn('//dummy $73, var $v' + strhex(param[0]));
				end;

				$7B:
				begin
					_ReadParams(2);
					AddJump(param[1]);
					WriteBufLn(strcat('if $v& = 0 then sleep 20 else (sleep $v& * 20) $v& := 1 end //dummy $7B $v&, jump:%',
						[param[0], param[0], param[0], param[0], strhex(param[1], 4)]));
				end;

				$7F:
				begin
					_ReadParams(1);
					WriteBufLn('//dummy $7F stash config $v' + strhex(param[0]));
				end;

				$80:
				begin
					_ReadParams(2);
					if dword(param[1] + 2) >= src.buffySize then
						raise DecompException.Create('$80 text input desc oob @ $' + strhex(src.ofs - 4));
					case game of
						gid.Nocturn98, gid.Nocturn_w: WriteBuf('($s0A := :"'#$E6#$85#$8E#$E4#$B8#$80'"');
						gid.Nocturn_e: WriteBuf('($s0A := :"Shinichi"');
						else WriteBuf('($s0A := :"Bob"');
					end;
					WriteBufLn(strcat(') ($s1E := $s0A) return //dummy $80 text input, $s&, "%"',
						[param[0], src.ReadStringFrom(param[1] + 1)]));
				end;

				$81:
				begin
					_ReadParams(2);
					WriteBufLn(strcat('//dummy $81, write $s& and $s& into %', [param[0], param[0] + 1, src.ReadStringFrom(param[1] + 1)]));
				end;

				$89:
				begin
					_ReadParams(1);
					AddNonCodeSection(param[0]);
					WriteBufLn('//dummy $89 set portrait info');
				end;

				$8F:
				begin
					_ReadParams(2);
					if dword(param[1] + 2) >= src.buffySize then
						raise DecompException.Create('$8F load ovl oob @ $' + strhex(src.ofs - 4));
					WriteBufLn(strcat('//dummy load ovl $8F-&, %', [param[0], src.ReadStringFrom(param[1] + 1)]));
					AddNonCodeSection(param[1]);
				end;

				$93:
				begin
					_ReadParams(1);
					WriteBufLn('//dummy $93, var $v' + strhex(param[0]));
				end;

				$95:
				begin
					_ReadParams(8);
					WriteBuf('sleep //transition $95');
					for d := 1 to 7 do WriteBuf('-' + strhex(param[d]));
					WriteBufLn('');
				end;

				$97: WriteBufLn('goto endings // $97');

				// Don't know what any of these do, dummy all out.
				6, $20, $36, $82, $8C..$8E, $90, $92, $94: WriteBufLn('//dummy $' + strhex(cmd));

				9, $34, $37, $45, $61, $83, $FD, $FE, // 1 param
				$32, $71, $74, $75, $78, $91, $96, // 2 params
				$26, $35, $63, $64, $69, $6A, $76, $77, $88:
				begin
					case cmd of
						9, $34, $37, $45, $61, $83, $FD, $FE: w := 1;
						$69, $88: w := 3;
						$6A: w := 4;
						$26: w := 5;
						$35: w := 6;
						$63, $64, $76, $77: w := 8;
						else w := 2;
					end;
					_ReadParams(w);
					WriteBuf('//dummy $' + strhex(cmd));
					for d := 0 to w - 1 do WriteBuf('-' + strhex(param[d]));
					WriteBufLn('');
				end;

				else begin
					DumpBuffer(src.readp - 2, 16);
					raise DecompException.Create(strcat('Unknown $& @ $&', [cmd, src.ofs - 2]));
				end;
			end;
		end;
	end;

begin
	result := FALSE;
	if loader.fullFileSize < 100 then raise DecompException.Create('file too tiny');
	// Some of these scripts start with $100 bytes of sig+junk (maybe theoretically used for decryption but they didn't
	// fill it with random values). If the first couple dwords are all the same, skip the first $100 bytes.
	startofs := loader.ReadDwordFrom(8);
	if (startofs = loader.ReadDwordFrom(12)) and (startofs = loader.ReadDwordFrom(16)) then
		startofs := $100
	else
		startofs := 0;
	loader.ofs := startofs;
	_Deobfuscate(loader);
	longvars := $FFFF;
	case game of
		gid.Mayclub_w, gid.Mayclub_e, gid.Nocturn_w, gid.Nocturn_e: isWindy := TRUE;
	end;

	scriptname := ExtractFileName(outputfile);
	scriptname := upcase(copy(scriptname, 1, scriptname.Length - length(ExtractFileExt(scriptname))));

	// Dump the raw bytecode for analysis.
	if decomp_param.keepInt then SaveFile(outputfile + '.bin', loader.PtrAt(startofs), loader.buffySize - startofs);

	try
		ScriptWriter.Init(loader);
		{$ifdef enable_decomp_hacks} _ApplyHacks(loader); {$endif}
		loader.ofs := startofs;
		ScriptWriter.SetLineOfs(startofs);
		_TranslateBytecode(loader);
	finally
		if ScriptWriter.lineIndex <> 0 then begin
			ScriptWriter.GenerateFinal(NOT decomp_param.debugLabels);
			if ScriptWriter.finalBufSize <> 0 then
				SaveFile(outputfile, @ScriptWriter.finalBuffy[0], ScriptWriter.finalBufSize);
		end;
	end;

	result := TRUE;
end;

