{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function Decomp_Exe(loader : TFileLoader; const basename : UTF8string; game : gid) : boolean;
// Reads an executable file and extracts useful data, like animation frames or music file lists.
// The executable's game ID must be known before calling.

	procedure _SongNumbers(const prefix, suffix : string; numsongs : dword);
	var i : dword;
	begin
		with gameConst do begin
			setlength(songList, numsongs);
			for i := high(songList) downto 0 do
				if i < 9 then
					songList[i] := prefix + '0' + chr(i + 49) + suffix
				else
					songList[i] := prefix + strdec(i + 1) + suffix;
		end;
	end;

	procedure _SongNames(songlistofs, numsongs : dword);
	var i : dword;
		c : byte;
		ext : boolean = FALSE;
	begin
		loader.ofs := songlistofs;
		with gameConst do begin
			setlength(songList, numsongs);
			for i := high(songList) downto 0 do
				byte(songList[i][0]) := 0;

			i := 0;
			while i < dword(length(songList)) do begin
				c := loader.ReadByte;
				case c of
					0: begin
						inc(i);
						ext := FALSE;
					end;
					ord('.'): ext := TRUE;
					else if NOT ext then begin
						inc(byte(songList[i][0]));
						byte(songList[i][length(songList[i])]) := c;
					end;
				end;
			end;

			if decomp_param.verbose then begin
				decomp_logger('Extracted songlist, ' + strdec(length(songList)) + ' entries.');
				for i := 0 to high(songList) do
					decomp_logger(strdec(i) + ':' + songList[i]);
			end;
		end;
	end;

	function _Strings(stringofs, strcount, maxlen : dword) : TStringBunch;
	// Extract shift-jis byte pair strings, separated by bytes that don't form valid byte pairs, except ignores both
	// single- and double-byte whitespace. Stringofs should point to the start of the first valid string. Maxlen should
	// be the longest expected string.
	var i, l : dword;
		bh, bl : byte;
	begin
		loader.ofs := stringofs;
		maxlen := (maxlen + 1) and $FFE; // force word width
		result := NIL;
		setlength(result, strcount);
		for i := 0 to strcount - 1 do begin
			// Scan ahead until first hopefully valid shift-jis found.
			repeat
				l := BEtoN(word(loader.readp^));
				if (l >= $8140) and (l <= $EAA4) then break;
				inc(loader.readp);
			until FALSE;

			l := 0;
			setlength(result[i], maxlen);
			repeat
				bh := loader.ReadByte;
				if bh = $20 then continue; // ignore space
				if (bh >= $81) and (bh <= $EA) then begin
					// Hopefully valid shift-jis.
					bl := loader.ReadByte;
					if (bh = $81) and (bl = $40) then continue; // ignore space
					inc(l); byte(result[i][l]) := bh;
					inc(l); byte(result[i][l]) := bl;
				end
				else break; // not a double-byte shift-jis, end string
			until l >= maxlen;
			// Convert captured bytes to UTF-8.
			result[i] := GetUTF8(@result[i][1], l);
			//decomp_logger(strhex(i) + ': [' + result[i] + ']');
		end;
	end;

	procedure _ReadBytes(fromofs, numbytes, expected : dword);
	begin
		with gameConst do begin
			setlength(hardBytes, length(hardBytes) + 1);
			setlength(hardBytes[high(hardBytes)], numbytes);
			if BEtoN(loader.ReadDwordFrom(fromofs)) <> expected then
				decomp_logger(strcat('[!] expected $& at % $&', [expected, basename, fromofs]));
			move(loader.PtrAt(fromofs)^, hardBytes[high(hardBytes)][0], numbytes);
		end;
	end;

	procedure _ReadFile(path : UTF8string);
	var f : TFileLoader;
	begin
		f := TFileLoader.Open(PathCombine([ExtractFilePath(loader.fileName), path]));
		try
			with gameConst do begin
				setlength(hardBytes, length(hardBytes) + 1);
				setlength(hardBytes[high(hardBytes)], f.fullFileSize);
				move(f.readp^, hardBytes[high(hardBytes)][0], f.fullFileSize);
			end;
		finally
			f.Destroy;
		end;
	end;

	procedure _AnimData(animdataofs : dword);
	var metadata : TGraphicFile;
		animname : string[12];
		i, namebase : dword;
	begin
		loader.ofs := animdataofs;

		// Some Jast games use a modified format.
		case game of
			gid.AngelsC1, gid.Setsujuu, gid.Transfer98:
			begin
				namebase := 0;
				case game of
					// Baseline address for name strings.
					gid.AngelsC1: namebase := $11580;
					gid.Setsujuu: namebase := $14340;
					gid.Transfer98: namebase := $13E50;
				end;

				repeat
					// First word is animation sequence length. Invalid value means we're done.
					if (LEtoN(word(loader.readp^)) > $FF) or (dword(loader.readp^) = 0) then break;

					// The second word is an offset to the animation name from baseline. Name is null-terminated.
					i := namebase + LEtoN(loader.ReadWordFrom(loader.ofs + 2));
					byte(animname[0]) := 9;
					move(loader.PtrAt(i)^, animname[1], 9);
					i := pos(chr(0), animname);
					if i <> 0 then byte(animname[0]) := i - 1;
					// An empty animation name means we're done.
					if animname = '' then break;

					// Shift the rest of the animation data backward to the expected position.
					MemCopy(loader.PtrAt(loader.ofs + 66), loader.PtrAt(loader.ofs + 38), 140);
					// Convert and save the animation data.
					animname := upcase(animname);
					// Hack: ignore non-existent files.
					if animname <> 'TH_066A1' then begin
						metadata := GetOrAddGraphicFile(animname);
						ConvertJastAnimData(loader.readp, metadata, game);
					end;

					inc(loader.readp, 206);
				until FALSE;
			end

			// Other games use a more common format.
			else begin
				repeat
					// 0 seqlen or 0 name, or otherwise invalid? We're done.
					if (NOT (LEtoN(word(loader.readp^)) in [1..32])) or (word((loader.readp + 2)^) = 0) then break;

					// Read the animation file name, hopefully null-terminated.
					byte(animname[0]) := 9;
					move((loader.readp + 2)^, animname[1], 9);
					i := pos(chr(0), animname);
					if i <> 0 then byte(animname[0]) := i - 1;
					animname := upcase(animname);

					// Convert and save the animation data.
					// Hack: ignore non-existent files.
					case animname of
						'T3_007A3', 'T3_026A0', 'T3_026A1', 'T3_026A2', 'T3_033A1', 'T4_026A0', 'T4_033A0': ;
						else begin
							metadata := GetOrAddGraphicFile(animname);
							ConvertJastAnimData(loader.readp, metadata, game);
						end;
					end;

					inc(loader.readp, 178);
				until FALSE;
			end;
		end;
	end;

begin
	result := FALSE;
	with gameConst do case game of

		gid.AngelsC1:
		if basename = 'ten_s' then begin
			_SongNumbers('T', '', 30);
			_AnimData($129A0); // first game
			_AnimData($13680); // second
			_AnimData($14360); // third
		end;

		gid.AngelsC2:
		if basename = 'tsp2' then
			_SongNames($137E8, 30)
		else if basename = 'main' then
			_AnimData($2804);

		gid.AngelsEve3H:
		if length(gameConst.hardBytes) = 0 then begin
			_ReadFile('haseetbl.bin');
			_ReadFile('ntbl.bin');
		end;

		gid.AngelsEve5:
		if length(gameConst.hardBytes) = 0 then begin
			_ReadFile('seentbl.bin');
			_ReadFile('ntbl.bin');
		end;

		gid.AngelsSpecial2:
		if length(gameConst.hardBytes) = 0 then begin
			_ReadFile('spseetbl.bin');
			_ReadFile('ntbl.bin');
		end;

		gid.Deep:
		if basename = 'deep' then begin
			_SongNames($1EB43, 47);
		end;

		gid.DualSoul98:
		if basename = 'adv' then begin
			_ReadBytes($13FC9, 15 * 4, $00002A08); // PMD offsets
		end;

		gid.Eden:
		if basename = 'j_adv2' then begin
			_SongNumbers('EK_', '', 20);
		end;

		gid.Foreigner:
		if basename = 'frmain' then begin
			hardcodedTxt := _Strings($19B16, 24, 32);
		end;

		gid.FromH:
		if basename = 'frm' then begin
			_SongNumbers('"', '"', 28);
		end;

		gid.Hohoemi:
		if basename = 'tenho' then begin
			_SongNames($1A2BC, 30);
		end;

		gid.InmaSeiGa98:
		if basename = 'adv' then begin
			_ReadBytes($15CC9, 10 * 4, $00008C07); // PMD offsets
		end;

		gid.Kyouhaku98:
		if basename = 'adv' then begin
			_ReadBytes($CB81, 16 * 4, $00008208); // PMD offsets
		end;

		gid.MaDoll:
		if basename = 'mdmain' then begin
			_SongNumbers('MAD', '', 34);
			// Constant strings references.
			hardcodedTxt := _Strings($9E59, 27, 16);
			hardcodedTxt[0] := '';
			setlength(hardcodedTxt[0], 512);
			// Single-byte to full character mappings.
			move(loader.PtrAt($A05B)^, hardcodedTxt[0][$20 * 2], $60 * 2); // 20..7F
			move(loader.PtrAt($A125)^, hardcodedTxt[0][$A1 * 2], $3E * 2); // A1..DE
		end;

		gid.MaDoll_w:
		if basename = 'madou32' then begin
			_SongNumbers('MAD', '', 36);
			// Constant strings references.
			hardcodedTxt := _Strings($2CC10, 27, 16);
			hardcodedTxt[0] := '';
			setlength(hardcodedTxt[0], 512);
			// Single-byte to full character mappings.
			move(loader.PtrAt($2C424)^, hardcodedTxt[0][$20 * 2], $60 * 2); // 20..7F
			move(loader.PtrAt($2C526)^, hardcodedTxt[0][$A1 * 2], $3E * 2); // A1..DE
		end;

		gid.MajoGari98:
		if basename = 'adv' then begin
			_ReadBytes($E565, 9 * 4, $00009007); // PMD offsets
		end;

		gid.Majokko:
		if basename = 'majo' then begin
			_SongNames($182E8, 30);
		end;

		gid.Maririn: _SongNumbers('MR_', '', 26);

		gid.Runaway:
		if basename = 'mei' then begin
			_SongNumbers('MT_', '', 22);
			_AnimData($162B0);
		end;

		gid.Runaway98:
		if basename = 'mei' then begin
			_SongNumbers('MT_', '', 22);
			_AnimData($15030);
		end;

		gid.Sakura:
		if basename = 't_adv1' then begin
			_SongNumbers('SK_', '', 33);
		end;

		gid.Sakura98:
		if basename = 't_adv1' then begin
			_SongNumbers('SK_', '', 33);
		end;

		gid.SanShimai:
		if basename = 'j_adv1' then begin
			_SongNumbers('SS_', '', 24);
			_AnimData($15AD0);
		end;

		gid.SanShimai98:
		if basename = 'j_adv1' then begin
			_SongNumbers('SS_', '', 24);
			_AnimData($146B0);
		end;

		gid.Setsujuu:
		if basename = 'setu' then begin
			_SongNames($1466F, 22);
			_AnimData($15816);
		end;

		gid.Skirmish:
		if basename = 'rpg' then begin
			_ReadBytes($2637F, 15 * 4, $0000560B); // PMD offsets
		end;

		gid.Tasogare:
		if basename = 'ta4' then begin
			_SongNames($1B3AA, 27);
		end;

		gid.Transfer98:
		if basename = 'tk' then begin
			_SongNumbers('TEN0', '', 40);
			_AnimData($159CC);
			//if game = gid.TRANSFER98 then gameConst.songList[39] := 'TRAIN';
			decomp_logger('Extracting bytecode from TK.EXE...');
			loader.ofs := $14FC6;
			loader.buffySize := $14FC6 + $9A5; // up to excluding $1596B
			Decomp_JastOvl(loader, PathCombine([decomp_param.outputdir, 'scr', 'tkexe.txt']), game);
		end;

		// The song list in Vanish exists in RPK.EXE at $19CDC, but it is out of order. The first song when the game
		// starts is definitely number 4.
		gid.Vanish:
		if basename = 'rpk' then
			_SongNumbers('VP0', '', 15)
		else if basename = 'main' then
			_AnimData($2804);

		else exit;
	end;

	result := TRUE;
end;

