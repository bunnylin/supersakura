unit recomp_core;

{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

{$mode objfpc}
{$inline on}
{$codepage UTF8}
{$iochecks off} // WARNING: if an IO action fails, all subsequent actions fail silently until IOresult is checked
{$unitpath ../lib/moonlibs}
{$librarypath ../lib/moonlibs}

{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

interface

uses sysutils, mccommon, mcfileio, gidtable, mcgloder, mcsassm;

var recomp_param : record
		projectName : UTF8string; // the project name, also default src/out
		parentName : UTF8string; // mods must have a parent project
		sourceDir : UTF8string; // the resources being built are read from here
		profileDir : UTF8string;
		allProjects : boolean;
	end;
	recomp_errors : dword;

function Recomp_Build : boolean;
var recomp_logger : procedure (const logtext : UTF8string) = NIL;

implementation

function InitBuild : boolean;
var txt, srcdir : UTF8string;
	sb : TStringBunch;
begin
	result := FALSE;
	recomp_errors := 0;
	with recomp_param do begin
		if sourceDir = '' then sourceDir := projectName;

		// Find the sourcedir, plain, under working directory, or in profile.
		recomp_logger('Trying source path: ' + sourcedir);
		sb := FindFiles_caseless(sourceDir, TRUE);
		if sb = NIL then begin
			srcdir := PathCombine(['data', sourceDir]);
			recomp_logger('Trying source path: ' + srcdir);
			sb := FindFiles_caseless(srcdir, TRUE);
			if sb = NIL then begin
				srcdir := PathCombine([profileDir, sourceDir]);
				recomp_logger('Trying source path: ' + srcdir);
				sb := FindFiles_caseless(srcdir, TRUE);
				if sb = NIL then begin
					recomp_logger('Source not found');
					exit;
				end;
			end;
		end;
		txt := sb[0];
		// -all builds anything that has a data.inf present.
		if (recomp_param.allProjects) and (NOT FileExists(PathCombine([txt, 'data.inf']))) then exit;
		// If sourcedir is the same as project name, use sourcedir's exact casing for the project name too.
		if lowercase(sourceDir) = lowercase(projectName) then
			projectName := ExtractFileName(txt);
		// Use the exact casing of the source directory.
		sourceDir := txt;

		recomp_logger('Project: ' + projectName);
		recomp_logger('Source directory: ' + sourceDir);
	end;

	with availableDats[0] do begin
		projectName := recomp_param.projectName;
		parentName := '';
		displayName := '';
		gameVersion := '';
		baseResX := 640; baseResY := 400;
		loaded := FALSE;
	end;

	result := TRUE;
end;

procedure GenerateDataFile;
// Try to create the dat in ./data/ or in profile.
var target : UTF8string;
begin
	recomp_logger(StringOfChar('-', 69));
	recomp_logger('Generating data file...');
	with recomp_param do try
		try
			target := PathCombine(['data', projectName + '.dat']);
			recomp_logger('Trying target path: ' + target);
			availableDats[0].WriteDat(target);
			exit;
		except on e : EAccessDenied do begin
			recomp_logger(e.Message);
			target := PathCombine([profileDir, projectName + '.dat']);
			recomp_logger('Trying target path: ' + target);
			availableDats[0].WriteDat(target);
		end; end;
	except on e : Exception do begin
		recomp_logger(e.Message + LineEnding + BacktraceStrFunc(ExceptAddr));
		inc(recomp_errors);
	end; end;
end;

function Recomp_Build : boolean;

	procedure _Build;
	var i : longint;
		uniquestringcount : dword;
	begin
		if NOT InitBuild then exit;
		try
			inc(recomp_errors, LoadFileTree(recomp_param.sourceDir));
		except on e : Exception do begin
			recomp_logger(e.Message + LineEnding + BacktraceStrFunc(ExceptAddr));
			exit;
		end; end;
		recomp_logger(strcat('Summary: % images, % script labels.', [graphicFileCount - 1, scriptObjectCount - 1]));

		uniquestringcount := 0;
		if scriptObjectCount > 1 then
			for i := 1 to scriptObjectCount - 1 do
				inc(uniquestringcount, dword(length(scriptObjects[i].stringTable)));

		recomp_logger(strcat('% global strings, % unique strings.',
			[length(scriptObjects[0].stringTable), uniquestringcount]));

		GenerateDataFile;

		recomp_logger(StringOfChar('-', 69));
		if recomp_errors <> 0 then recomp_logger(strdec(recomp_errors) + ' errors! See sakutool.log.');
	end;

var game : TGameRecord;
begin
	result := FALSE;
	Assert(recomp_logger <> NIL);
	Assert(recomp_param.profileDir <> '');

	if recomp_param.allProjects then begin
		for game in GameRecordTable do if game.g <> gid.unknown then begin
			recomp_param.projectName := ShortName(game.g);
			recomp_param.sourceDir := '';
			_Build;
			UnloadDats(TRUE);
		end;
	end
	else begin
		Assert(recomp_param.projectName <> '');
		_Build;
	end;

	result := TRUE;
end;

initialization
finalization
end.

