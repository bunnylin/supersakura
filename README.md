SuperSakura engine
------------------

SuperSakura is a modern visual novel engine that can run certain Japanese
games from the 90's. Many of these old titles were surprisingly good,
but were never localised. SuperSakura has tools to help localise games, and
supports enhanced graphics and a modernised user interface.

The engine is written in Free Pascal, uses SDL2, and targets Linux/Windows,
64/32-bit.

Note that this is just a game engine and a set of asset conversion tools.
The actual games themselves are under copyright and are not distributed with
this project. To run games on the SuperSakura engine, you need to convert the
game data from original files using these tools.

For a list of supported games, run `sakutool list`, or see
`inc/gidtable.inc`, or visit the main site at
[supersakura.net](https://supersakura.net/).

To see how the engine is progressing, see [CHANGELOG.md](CHANGELOG.md) and
[todo.md](doc/todo.md).


### Screenshots

![Front end screenshot](https://supersakura.net/gfx/sakutitle.png "Front end screenshot")

For more, see [here](https://supersakura.net/sscreens.php).


### Downloads

You can get a reasonably recent Win32 build of the engine and tools from
[supersakura.net](https://supersakura.net/). But for best results,
you can try to compile your own copy.


### Compiling

Please see [BUILDING.md](/doc/BUILDING.md).


### SDL2

SuperSakura needs SDL2 to run. This is a famous video/audio library used by many
modern games, so you may already have it. If not, it is easy to get.

If you've downloaded the SuperSakura Win32 build, that includes the SDL2 runtime DLL's,
so you don't need to do anything.

**64-bit Windows**: The precompiled 32-bit SuperSakura will work just fine on 64-bit
Windowses.

If you've built a 32-bit version of Supersakura, get the `win32-x86` SDL2.dll runtime from
[github.com/libsdl-org/SDL/releases](https://github.com/libsdl-org/SDL/releases),
and the `win32-x86` SDL2_ttf.dll font-rendering runtime from
[github.com/libsdl-org/SDL_ttf/releases](https://github.com/libsdl-org/SDL_ttf/releases).
Put both in your `\Windows\SysWOW64` directory (or in the same directory
where SuperSakura is, if you prefer).

If you've built a 64-bit version of SuperSakura, get instead the `win32-x64` runtimes
and put those in your `\Windows\System32` directory (or in the same directory where
SuperSakura is, if you prefer).

**32-bit Windows**: Get the `win32-x86` SDL2 runtimes.
Put both in your `\Windows\System32` directory (or in the same directory
where SuperSakura is, if you prefer).

**Linux**: Your distro's main software repository should have SDL2 and
SDL2_ttf. Install both through your normal package manager.
- Archlinux: `pacman -S sdl2 sdl2_ttf`
- Fedora: `dnf install SDL2-devel SDL2_ttf-devel`
- Debian/Ubuntu: `apt install libsdl2-dev libsdl2-ttf-dev`

You should end up with libSDL2*.so files somewhere under your `/usr/lib` or
`/usr/lib/x86_64-linux-gnu` directory. To build SuperSakura, the two files that
must be present are `libSDL2.so` and `libSDL2_ttf.so`, which are provided by the
`-dev` packages. The compiled binary should however actually link to a runtime
version of the library files, so you shouldn't need the `-dev` packages to just
run the engine. I intend to switch to a different library load mechanism that
will eliminate the need for `-dev` packages entirely, at some point.

If you have SDL3 with sdl2-compat, please note that still has backward compatibility bugs.
It's better to keep SDL2 for now.

Some useful information on setting up SDL2 for Linux is
[here](http://www.freepascal-meets-sdl.net/chapter-2-installation-configuration-linux-version/).
- If SDL has trouble creating a renderer, you may be able to override the
  renderer preference: [Overriding the SDL video driver](https://wiki.libsdl.org/FAQUsingSDL).
  Try something like `SDL_RENDER_DRIVER=software` or `SDL_VIDEODRIVER=x11`.
- If SDL_InitSubSystem throws a division by zero or invalid operation when
  setting up SDL_video, particularly in a virtual machine, see
  [this bug](https://github.com/PascalGameDevelopment/SDL2-for-Pascal/issues/56).
- If video-related failures persist, examine `saku.log` and try running
  `LIBGL_DEBUG=verbose ./supersakura` if on Linux for more information.


### Using the engine

To compile resources into a usable SuperSakura data file:

    sakutool make <projectname>

For example, to compile the included Winterquest ministory/testsuite:

    sakutool make winterq

To run any compiled game through a friendly front end:

    supersakura

Or, to run the console port, which doesn't depend on SDL:

    supersakura-con

The tools and engine print their log output into files: `saku.log` and
`sakutool.log`. By default, these are put in the program's working directory.
If the working directory is write-protected, then the logs are saved under
your profile directory.

You can add -h to any executable's commandline to see what other commandline
options are available.

The console port works best when playing games in English. If your terminal
is configured to correctly display UTF-8-encoded Japanese characters, you can
also play games with Japanese text. Linux terminals usually display Japanese
correctly automatically, while a modern Windows command prompt will probably
work fine as long as it has been set to use a font that supports Japanese glyphs,
such as MS Gothic.


### Converting game data

Sakutool reads individual files from original games, and converts and saves
them under SuperSakura's data directory in standard file formats, like game
scripts in plain text and graphics as PNG files. Note that although various
PC-98 games are supported, sakutool cannot yet extract the individual data files
from .HDI or .FDI images, so if you keep your PC-98 games in those, you will have
to first extract the files from the disk image manually. Possible tools for this:

* Ryo-Cokey's [FIVEC](https://www.pc98.org/project/fivec.html)
(Windows, GUI)
* [EditDisk](https://hp.vector.co.jp/authors/VA013937/editdisk/index_e.html)
(Windows, GUI)
* My own [98ripper](https://gitlab.com/bunnylin/98ripper) (Linux/Windows, commandline)

To convert resources:

    sakutool x <filename or directory>

For example, to convert and run the DOS version of The Three Sisters' Story
(the Windows port is not yet supported):

    sakutool x /mygames/threesistersstory/
    sakutool m sanshimai
    supersakura sanshimai

Caution: Although sakutool has a lot of fuzziness in autodetecting file types, in some
cases it needs to know which game's resources are being converted or it won't recognise
the game files. Normally it does this by checking a CRC of the game's main executable,
but if that isn't working, you can use `sakutool x <path> --id <game's shortname>`
to force detection. Known game shortnames can be seen with `sakutool list`.


### Using SakuraScript

If you just want to play the games, you don't need to understand SakuraScript.
This is the custom scripting language used by the engine. The syntax is both
described and verified by the `parsertest` project included in the `data`
directory; you can read the single script file there for details. A list of
recognised built-in functions can be found in `sakurascript-definition.pas`.
Then, if you're curious, you can examine the decompiled scripts from any
supported game and see what's happening behind the scenes.


### Translating games

Under EU law, translations count as derivative works. Therefore, game
translations generally can not be distributed with this project, since it
could be a copyright infringement to make them available to the public without
permission from the rightsholder. However, you can easily produce a basic
translation through machine translation, which is not an infringement as long as
it is only for your personal use.

An automatic translator hook is planned for the engine, but not fully working yet.
The mcsassm unit has functionality for importing and exporting a game's strings
in simple tab-separated spreadsheets, but this is not currently accessible
through sakutool's interface.


### Game modding

You can replace any game file by dropping another file by the same name in the
`data/<project>/override` directory, then restarting the game. This includes
any graphics, scripts, or a translated string table.

For a neater package, you can also create a mod data file. This will show up
as a separate item on the front end. When loaded, the engine loads the parent
dat first, then the mod dat, which replaces files from the parent using the
same override logic as above.

1. Create a new project directory under SuperSakura's data directory, for
example `walrus98-en`.

2. Put your modified files in the new project directory.

3. Create a `data.inf` file there. It should contain a description that can be
   shown on the front end, the mod's language, the project name of the game being
   modified, and any necessary metadata for modified graphics.

4. Compile the mod project normally: `sakutool make walrus98-en`.

Example data.inf file:
```
desc Warez Walrus (English patch) (PC98)
language English
parent walrus98
```


### Contributing

If you find bugs, please report them! It's better to have problems reported
multiple times than not at all, as a general rule. You can raise an issue on
[gitlab](https://gitlab.com/bunnylin/supersakura/-/issues), or e-mail me,
or [noot](https://social.linux.pizza/@mooncorebunny). (There is another Mastodon
account for SuperSakura announcements only, if you'd like to just track those:
[supersakura@mastodon.gamedev.place](https://mastodon.gamedev.place/@supersakura))

If you think you've followed the build instructions correctly but it still
won't build and run, that's also worth complaining about. Maybe it's genuinely
broken, maybe there's something unexpected in your system configuration,
maybe the instructions aren't clear enough...

If you're brave enough to attempt reverse engineering on a particularly interesting
old game, you can use any tools you're comfortable with, and share your findings
on how to correctly read some of these mysterious old files. I can always use
additional file format documentation! However, SuperSakura's scope tends toward
older games - if a game natively runs well on modern systems already and was
published after 2010, it's probably out of scope here.

If you can work with Pascal, you could try adding a new resource converter to
sakutool, in the style of the existing ones. There may also be some interesting
small tasks in the todo-file that technically anyone can do, or bugfixes etc.
You can do a merge request or just email me with code if you've written something.

It would be nice to offer an upgraded HD title graphic for supported games and
custom textboxes, like Sakura and SanShimai already have.
New graphics don't have to be super fancy, but should be more than just
a smooth-scaled or AI-imagined resize of the game's original graphics.
If you have some pixel art skills, this could be an interesting area to contribute in.
