program sakutool;
{                                                                           }
{ SuperSakura commandline tool                                              }
{ Copyright 2009-2025 :: Kirinn Bunnylin / Mooncore                         }
{ https://supersakura.net/                                                  }
{ https://gitlab.com/bunnylin/supersakura                                   }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{ ------------------------------------------------------------------------- }
{                                                                           }
{ Targets FPC 3.2.2 for Linux/Win 32/64-bit.                                }
{                                                                           }
{ Compilation dependencies:                                                 }
{ - Various moonlibs (ZLib license)                                         }
{   https://gitlab.com/bunnylin/moonlibs                                    }
{                                                                           }

{$mode objfpc}
{$ifdef WINDOWS}{$apptype console}{$endif}
{$codepage UTF8}
{$iochecks off} // WARNING: if an IO action fails, all subsequent actions fail silently until IOresult is checked
{$scopedEnums on}
{$inline on}
{$unitpath inc}
{$unitpath lib/moonlibs}
{$librarypath lib/moonlibs}

{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

// On case-sensitive filesystems, user experience can be improved by doing some extra case-insensitive checks.
{$ifndef WINDOWS}{$define caseshenanigans}{$endif}

uses
{$ifdef UNIX}cthreads,{$endif}
sysutils, math, paszlib,
mccommon, mcfileio, mcscriptwriter, mcmidiwriter, midi_instrument_matcher, mcbtree, mcgloder, mcscaler, mcsassm,
gidtable, sjisutf8, lz_algo,
decomp_core, recomp_core;

var basepath : UTF8string;
	inputs : TStringBunch;
	listlvl : byte = 0;
	{$push}{$scopedEnums off}
	mode : (none, extract, make, clean, id, list);
	{$pop}

// Override "sakutool" with "ssakura" since this tool is a part of ssakura.
// Used by GetAppConfigDir to decide on a good config directory.
function truename : ansistring;
begin
	truename := 'ssakura';
end;

{$include inc/version.inc}
{$include inc/sakurascript-compiler.pas}

// ------------------------------------------------------------------

procedure Log(const logtxt : UTF8string);
begin
	EnterCriticalSection(LogCritter);
	{$ifndef WINDOWS}
		// Emphasis for errors and warnings, using ANSI escapes.
		if (logtxt <> '') and (logtxt[1] = '[') and (logtxt[2] in ['!','e']) then begin
			write(#27'[1m');
			writeln(logtxt);
			write(#27'[0m');
		end else
	{$endif}
	writeln(logtxt);
	writeln(LogFile, logtxt);
	LeaveCriticalSection(LogCritter);
end;

function ParseCommands : boolean;
// Processes the commandline. Returns FALSE in case of errors etc.
var txt : UTF8string;
	inputcount : dword;
begin
	result := TRUE;
	inputcount := 0;
	inputs := NIL;
	setlength(inputs, ParamCount);

	txt := GetNextParam;
	case lowercase(txt) of
		'x','extract': mode := extract;
		'm','make': mode := make;
		'c','clean': mode := clean;
		'i','id': begin mode := id; decomp_param.onlyId := TRUE; end;
		'l','list': mode := list;
		else begin
			mode := none;
			param_index := 0;
		end;
	end;

	repeat
		txt := GetNextParam;
		case txt of
			chr(0): break;
			'', '-': ;

			// --- Xtract params ---
			//'-b', '-beautify': decomp_param.doBeautify := TRUE;
			'-g', '-game', '-gid', '-i', '-id', '-gameid':
			begin
				txt := lowercase(GetNextParam);
				decomp_param.gidOverride := MatchGid(txt);
				if decomp_param.gidOverride	= NIL then begin
					writeln('Unrecognised ID "' + txt + '". Use a short project name from sakutool list.');
					inc(decomp_stats.numErrors);
					result := FALSE;
				end;
			end;

			'-f', '-filter': decomp_param.fileFilter := GetNextParam;

			'-keepint': decomp_param.keepInt := TRUE;

			'-k', '-keeptemp': decomp_param.keepTemp := TRUE;

			'-t', '-temponly', '-onlytemp':
			begin
				decomp_param.tempOnly := TRUE;
				decomp_param.keepTemp := TRUE;
			end;

			'-nohack': decomp_param.doHacks := FALSE;

			'-d', '-labels', '-debuglabels': decomp_param.debugLabels := TRUE;

			'-l':
			if mode = list then
				listlvl := byte(valx(GetNextParam))
			else
				decomp_param.debugLabels := TRUE;

			//'-x', '-pngx': recomp_param.usePngx := TRUE;

			'-v', '-verbose': decomp_param.verbose := TRUE;

			'-version':
			begin
				writeln(GetSuperSakuraVersion);
				result := FALSE;
			end;

			else begin
				if txt[1] = '-' then begin
					writeln('Unrecognised option: ' + txt);
					result := FALSE;
				end
				else begin
					if (mode = make) and (lowercase(txt) = 'all') then
						recomp_param.allProjects := TRUE
					else begin
						if mode in [none, extract, id] then txt := ExpandFileName(txt);
						inputs[inputcount] := txt;
						inc(inputcount);
					end;
				end;
			end;
		end;
	until FALSE;

	// If no source files were specified, there's nothing to do; print help.
	if (result) and (mode <> list) and (NOT recomp_param.allProjects) and (inputcount = 0) then
		print_commandline_help := TRUE;
	setlength(inputs, inputcount);

	if NOT print_commandline_help then begin
		case mode of
			none, extract, id: decomp_param.inputPaths := inputs;
			make: recomp_param.projectName := inputs[0];
		end;
	end
	else begin
		result := FALSE;
		writeln;
		writeln('  SuperSakura commandline tool  ' + GetSuperSakuraVersion);
		writeln('---------------------------------------------------------------------');

		case mode of
			extract:
			begin
				writeln('Extracts and converts the specified game resource files to modern standard');
				writeln('formats. The converted files are saved under the output path. Converted');
				writeln('resources are editable, and can be repacked into a single DAT file with');
				writeln('sakutool''s "make" command. SuperSakura can then load and run the DAT.');
				writeln;
				writeln('Usage: sakutool x <input directories or files with wildcards> [-options]');
				writeln;
				writeln('You can convert individual files or everything under a directory, for example:');
				writeln('  sakutool x ~/Games/3sis/OVL/SK_9*.OVL');
				writeln('  sakutool x "E:\games\Sakura no Kisetsu\"');
				writeln;
				writeln('Options:');
				//writeln('-out=<directory>  Override the output directory');
				writeln('-id=<project>     Override game identification (see sakutool list for values)');
				writeln('-filter=<match>   Only extract matching files from archives; *? wildcards');
				//writeln('-beautify         Beautify input graphics while converting');
				writeln('-nohack           Save images unmodified, no hacks, cropping, or compositing');
				writeln('-keeptemp         Don''t delete temporary files');
				writeln('-temponly         Don''t convert, only extract/keep raw files from archives');
				writeln('-keepint          Dump interim files (decompressed/decrypted), if possible');
				writeln('-debuglabels      Output a script label for every source bytecode');
				writeln('-verbose          Verbose output');
			end;

			make:
			begin
				writeln('Makes a single data file from game resources. Resources must have been');
				writeln('extracted from the original game first, or been custom-created.');
				writeln('Reads scripts, graphics, and other files from the data/<project> path.');
				writeln('Exits with code 0 if a DAT was created. Use project "all" to make data');
				writeln('files for all projects.');
				writeln;
				writeln('Usage: sakutool m <project> [-options]');
				writeln;
				writeln('For example, to build EveBE98.dat from data/EveBE98/*:');
				writeln('  sakutool m evebe98');
				{writeln;
				writeln('Options:');
				writeln('-x     Use best Deflate and PNG compression. Saves some space, but slow.');
				writeln('-xx    Use fast Deflate+CSE and PNGX compression.'); // <-- default if through front end
				writeln('-xxx   Use best Deflate+CSE and PNGX compression. Very slow.');
				writeln('-1     No multithreading, run in a single thread.');
				}
			end;

			clean:
			begin
				writeln('Deletes all extracted resource files under the data/<project> path.');
				writeln('Does nothing if used on a project not in the recognised game list.');
				writeln('Will not delete the game''s built DAT file, save games, or anything in');
				writeln('data/<project>/override/. Use project "all" to clean all projects.');
				writeln;
				writeln('Usage: sakutool c <project>');
				writeln;
				writeln('Example:');
				writeln('  sakutool c guynarock');
			end;

			id:
			begin
				writeln('Identifies the game at the specified path. If a game was recognised, prints');
				writeln('the full game name and its short project name, and exits with code 0.');
				writeln('If no game was recognised or there was an error, exits with a non-zero code');
				writeln('and prints the reason.');
				writeln;
				writeln('Usage: sakutool i <input directory or file>');
				writeln;
				writeln('Example:');
				writeln('  sakutool i ~/Games/3sis');
				writeln('  sakutool i "E:\games\Sakura no Kisetsu\*.exe"');
			end;

			list:
			begin
				writeln('Prints a list of recognised and convertable games with compatibility levels.');
				writeln('The list is in TSV format.');
				writeln;
				writeln('Usage: sakutool l [-options] [filter] [filter ...]');
				writeln;
				writeln('For example, to list all games whose name or ID contains "love" or "dragon":');
				writeln('  sakutool list love dragon');
				writeln('The filter is case-insensitive and can use wildcards:');
				writeln('  sakutool list "angel?eve*B"');
				writeln;
				writeln('Options:');
				writeln('-l=<level>   Display only games with these compatibility levels:');
				writeln('             1 = resources, 2 = playable, 4 = completable, 8 = excellent');
			end;

			else begin
				writeln('Usage: sakutool [command] [input path with wildcards] [-options]');
				writeln;
				writeln('Commands:');
				writeln('  x <input directories or files with wildcards> [-options]');
				writeln('    Extract and convert the specified game resource files.');
				writeln;
				writeln('  m <project> [-options]');
				writeln('    Make a single data file from game resources.');
				writeln;
				writeln('  c <project> [-options]');
				writeln('    Clean all resource files under the data/<project> path.');
				writeln;
				writeln('  i <input directory or file>');
				writeln('    Identify the game at the specified path.');
				writeln;
				writeln('  l [-options] [filter]');
				writeln('    Print a list of recognised and convertable games.');
				writeln;
				writeln('If [command] is omitted, sakutool defaults to running in extract mode.');
				writeln('Run sakutool [command] -help for command-specific help.');
				writeln;
				writeln('You can convert individual files or everything under a directory, for example:');
				writeln('  sakutool x ~/Games/3sis/OVL/SK_9*.OVL');
				writeln('  sakutool x "E:\games\Sakura no Kisetsu\"');
			end;
		end;
		if NOT (mode in [id, list]) then begin
			writeln;
			writeln('Primary output path:');
			writeln('  ', PathCombine([basepath, 'data']));
			writeln('Fallback output path:');
			writeln('  ', decomp_param.profileDir);
		end;
	end;
end;

procedure Summary;
// Give the user a summary of what happened.
begin
	case mode of
		none, extract:
		begin
			if decomp_stats.numConverted + decomp_stats.numErrors = 0 then
				log('No convertable files found.')
			else begin
				log('Files found: ' + strdec(decomp_stats.numFiles));
				log('Files converted: ' + strdec(decomp_stats.numConverted));
				log('Files skipped: ' + strdec(decomp_stats.numSkipped));
				if decomp_stats.numErrors = 0 then
					log('No errors.')
				else
					log(strdec(decomp_stats.numErrors) + ' errors! See sakutool.log.');
			end;
		end;

		id:
		if decomp_stats.numConverted = 0 then begin
			log('No game recognised.');
			ExitCode := 1;
		end;
	end;
end;

function InitEverything : longint;
// Minimal setup. Returns true if all good, or false if anything went wrong.
var i : dword;
	txt : UTF8string;
begin
	recomp_errors := 0;
	basepath := ExtractFilePath(paramstr(0));
	// If the base directory is write-protected, fall back to an app-specific directory under the user's profile.
	OnGetApplicationName := @truename;

	fillbyte(decomp_stats, sizeof(decomp_stats), 0);
	fillbyte(decomp_param, sizeof(decomp_param), 0);
	fillbyte(recomp_param, sizeof(recomp_param), 0);
	with decomp_param do begin
		profileDir := GetAppConfigDir(FALSE);
		doHacks := TRUE;
	end;
	with recomp_param do begin
		profileDir := GetAppConfigDir(FALSE);
	end;

	if NOT ParseCommands then exit(1);

	// Set up a log file. Try the current directory first...
	txt := PathCombine([basepath, 'sakutool.log']);
	assign(LogFile, txt);
	filemode := 1; rewrite(LogFile); // write-only
	i := IOresult;
	if i = 5 then begin
		// Access denied! Try the user's profile directory...
		mkdir(decomp_param.profileDir);
		while IOresult <> 0 do ;
		txt := PathCombine([decomp_param.profileDir, 'sakutool.log']);
		assign(LogFile, txt);
		filemode := 1; rewrite(LogFile); // write-only
		i := IOresult;
	end;
	if i <> 0 then begin
		writeln(errortxt(i) + ' trying to create ' + txt);
		exit(i);
	end;

	// Create a data directory.
	if NOT DirectoryExists('data') then begin
		mkdir('data');
		if IOresult = 5 then begin
			// Access denied: fall back to the user's profile directory.
			mkdir(decomp_param.profileDir);
		end;
	end;
	while IOresult <> 0 do ; // flush

	decomp_logger := @Log;
	if NOT decomp_param.onlyId then begin
		if decomp_param.verbose then mim_logger := @Log else mim_logger := @NullLogger;
		recomp_logger := @Log;
		asman_logger := @Log;
		asman_CompileScript := @CompileScript;
	end;
	result := 0
end;

begin
	ExitCode := InitEverything;
	if ExitCode <> 0 then exit;

	try

		case mode of
			none, extract, id: Decomp_ScanFiles;
			make: Recomp_Build;
			clean: writeln('Clean not implemented yet');
			list: PrintGameList(inputs, listlvl); // from gidtable
		end;
		Summary;

	except on e : Exception do begin
		Log(e.Message);
		{$ifopt D+}
		Log(BacktraceStrFunc(ExceptAddr));
		{$endif}
		ExitCode := 1;
	end; end;
end.

