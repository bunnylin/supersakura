#!/usr/bin/env python3
import os
import sys

def PrintUsage():
	print("Makeico converts 32bpp RGBA PNGs to a single ICO file.")
	print("Usage: makeico.py <icon> <width> [<icon2> <width> ...]")
	print("Example: makeico.py icon256.png 256 icon48.png 48 icon32.png 32 icon24.png 24 icon16.png 16")
	# Icons are expected to be square. Each different size should be hand-optimised,
	# not just bilinearry auto-resized, which looks suboptimal.
	# This script doesn't check the PNG file contents, just takes your word for the image size.
	# The resulting ICO file is basically a header plus all PNGs appended.
	exit(1)

if (len(sys.argv) <= 1) or (len(sys.argv) & 1 == 0) or (len(sys.argv) > 100): PrintUsage()
if sys.argv[1].startswith("--"): sys.argv[1] = sys.argv[1][1:]
if (sys.argv[1] == "/?") or (sys.argv[1] == "/h") or (sys.argv[1] == "/help") or (sys.argv[1] == "-?") or (sys.argv[1] == "-h") or (sys.argv[1] == "-help"): PrintUsage()

argp = 1
pngfile = []
widths = []
while argp < len(sys.argv):
	if not os.path.exists(sys.argv[argp]):
		print(sys.argv[argp] + " not found")
		exit(2)
	pngfile.append(sys.argv[argp])
	argp += 1
	try: width = int(sys.argv[argp])
	except: width = -1
	if width < 0:
		print(sys.argv[argp] + " doesn't look like a valid width")
		exit(3)
	if width > 255: width = 0 # for widths 256 and above, ICO header should contain 0
	widths.append(width)
	argp += 1

imgofs = 6 + len(pngfile) * 16
outbuf = bytearray([0, 0, 1, 0, len(pngfile), 0])
for i in range(len(pngfile)):
	filesize = os.path.getsize(pngfile[i])
	outbuf += bytearray([widths[i], widths[i], 0, 0, 1, 0, 32, 0])
	outbuf += bytearray([filesize & 0xFF, (filesize >> 8) & 0xFF, (filesize >> 16) & 0xFF, (filesize >> 24) & 0xFF])
	outbuf += bytearray([imgofs & 0xFF, (imgofs >> 8) & 0xFF, (imgofs >> 16) & 0xFF, (imgofs >> 24) & 0xFF])
	imgofs += filesize

for filename in pngfile:
	with open(filename, 'rb') as f:
		outbuf += f.read()

parts = os.path.splitext(pngfile[0])
outfile = parts[0] + '.ico'
print("Saving as " + outfile)
with open(outfile, 'wb') as f:
	f.write(outbuf)

exit(0)
