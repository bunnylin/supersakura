CONST pi = 3.141592654#
OPEN "costab.txt" FOR OUTPUT AS #1
FOR i% = 0 TO 256
j& = CLNG((COS(i% * pi / 256) + 1) * 32767.5)
PRINT #1, STR$(j&) + ",";
IF (i% AND 7) = 7 THEN PRINT #1, ""
NEXT i%
CLOSE #1

