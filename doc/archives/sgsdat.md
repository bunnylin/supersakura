SGS.DAT archives
----------------

These were used by Sogna for their Windows games, which mostly means the
Viper series.

Header: (words are x86-native little-endian)
```
char[12] - signature "SGS.DAT 1.00"
uint32 - number of files in the archive
```

The file index follows the header immediately. Each entry is 32 bytes:
```
char[19] - null-padded filename, possibly including path, in the form ANM\LOGO.ANM
byte - file is compressed = 1, not compressed = 0
uint32 - compressed size in bytes
uint32 - uncompressed size in bytes
uint32 - start offset for file data
```

File data follows immediately after the index.

Each file may be compressed with a straightforward LZSS variant.

To decompress:
```
flag_byte := Read next byte

While the file's uncompressed size has not been output:

	If out of bits in flag_byte:
		flag_byte := Read next byte from input stream

	next_bit := Read a bit from flag_byte (reading from 0x80 to 0x01)

	If next_bit is FALSE:
		Read a byte from the input stream and output it as a literal
	Else:
		code := Read a uint16 (x86-native little-endian) from the input stream
		copy_length := (code >> 12) + 1
		copy_distance := code & 0xFFF
		Output copy_length bytes from copy_distance bytes ago
```

Copying from before the start of the output buffer is an error.

GuynarockR fails at PCM\GR200123.PCM, list 77F0
7817 compressed
8B00 uncompressed
4A9513E offset

At offset 540 (535A), attempts to copy 16 bytes from 3328 ago.
Magic word is F-D00. Even 0x500 would be 1280, no good. 
