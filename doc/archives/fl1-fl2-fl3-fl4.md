FL archives
-----------

These archive formats were used by Aaru. They were a Himeya spinoff company, so
their formats are closely related.


### FL1 archives

FL1 looks to be exactly the same as Himeya's DL1 archives, except with "FL1.0" signature.
These are used in:
- Rose Blood
- M Hard
- Zest to Fantasy
- Rabid Helix
- Lofty Form
- LAG
- Kokoro...


### FL2 archives

Very similar to FL1, but with variable-length filenames.
These are used in:
- Guardian
- LOE ~Legend of Enomoto~
- Koutei Heika ni Narou!

Header:
```
char[6] - signature "FL2.0" 00
uint16 - start offset for the first file, always 0x0020?
uint32 - total file count
uint32 - file index byte size
uint32 - file index start offset
char[12] - "FMT_NORMAL" 00 00
```

The file index lists all files in the archive, where each entry is:
```
uint32 - file size, or FFFFFFFF to end list
uint8 - filename byte length
char[] - filename, including dot and suffix
```

The first file starts at offset 0x20, and each file starts where the last ended.


### FL3 archives

These are used in:
- Akairo no Utage
- Senryaku Musume

Header:
```
char[8] - signature "FL3.0" 00 00 00
uint16 - start offset for the first file
uint32 - file index byte size
uint32 - file index start offset
uint16 - total file count
```

The file index is exactly the same as on FL2.


### FL4 archives

These are used in:
- Kokoro... II
- Flowers
- Kokoro... 0
