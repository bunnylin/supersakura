FA1
---

File archive format containing multiple files. Files can use generic compression.
Used by Himeya and Forest/Foster.

Unpacking code for this is in [/inc/decomp_bundles.pas](../../inc/decomp_bundles.pas).


##### Header

	char[4] - sig [FA1 ]
	uint32 - data size from start of sig to past end of content data
	uint16 - file count
	byte - disk/archive index, usually 0
	byte - flag, 0x80 = file list encrypted with XOR 0xFF


##### File list

The list of files contained in the archive can be found after the end of content
data in the fa1 file, at the start of the next 0x400 sector, until end of file.

	list start offset := (header.dataSize + 0x3FF) & 0xFFFFFC00

If the flag byte in the header has bit 0x80 set, this entire list is XOR-encrypted
with 0xFF.

Each file entry in the list has:

	char[8] - filename, padded with spaces
	char[3] - suffix, padded with spaces
	byte - attribute flag, 00 = normal file, 01 = compressed
	uint32 - file size in archive, maybe compressed
	uint32 - uncompressed file size


##### Extracting files

Go back to the end of the header, file offset 0xC. The file data runs from here, with
files in the same order as in the file list, all appended together. Each file begins
at an even uint16-boundary, so there is a single padding byte after any file whose size
is an odd number of bytes.

If the file's attribute flag is 00, just copy its bytes directly into an output file.
If the flag is 01, feed its bytes through a decompressor first.


##### Decompressing files

This is a 16-bit LZSS variant. The original 8-bit version uses sections of eight
actions per flag byte; this version uses a 16-bit flag word instead (little-endian),
and the copying rules are unnecessarily complex.

Source code for this is `Decompress_CsLZ16` in [/moonlibs/lz_algo.pas](../../lib/moonlibs/lz_algo.pas).

A stream usually starts with the flag word 0xFFFF (16 set bits), indicating
16 literal bytes follow. A new flag word is read from input whenever a new bit
needs to be read and the current flag word has no more bits.
Bits are read from 0x8000 to 0x0001 in each flag word.

The general decompression logic:

```
While the file's uncompressed size has not been output:
	If the flag word has no more bits, read a new flag word from input
	Read the next bit from the flag word

	If bit was set:
		Read and output a byte literal

	Else bit was not set:
		Get more bits until a valid copy command is recognised
		If applicable, read more bits to build a variable length value
		Copy and output bytes from earlier in the output, as specified
```

All command bit patterns:

- `1`: Read the next byte from input, and output it as a literal byte

- `010`:
  * `xx` := Read the next byte from input
  * Output 2 bytes copied from `xx + 1` bytes ago (value range 1..256)

- `011`:
  * `xx` := Read the next byte from input
  * `bbb` := Read the next 3 bits from input
  * If `xx` == 255 and `bbb` == 7, that's a special case: Don't output anything.
    Instead consume and discard one extra bit, and read the next command.
    (This is something to do with the original buffer implementation.)
  * Otherwise output 2 bytes copied from `256 + 8*xx + bbb + 1` ago (range 257..2303)

- `001`
  * `xx` := Read the next byte from input
  * `b` := Read the next 1 bit from input
  * `len` := Read a variable-width length value from input
  * Output `len` bytes copied from `2 * xx + b + 1` ago

- `0001`
  * `xx` := Read the next byte from input
  * `b` := Read the next 1 bit from input
  * `len` := Read a variable-width length value from input
  * Output `len` bytes copied from `2 * (xx + 256) + b + 1` ago

- `00001`
  * `xx` := Read the next byte from input
  * `bb` := Read the next 2 bits from input
  * `len` := Read a variable-width length value from input
  * Output `len` bytes copied from `4 * (xx + 256) + bb + 1` ago

- `000001`
  * `xx` := Read the next byte from input
  * `bbb` := Read the next 3 bits from input
  * `len` := Read a variable-width length value from input
  * Output `len` bytes copied from `8 * (xx + 256) + bbb + 1` ago

- `000000`
  * `xx` := Read the next byte from input
  * `bbbb` := Read the next 4 bits from input
  * `len` := Read a variable-width length value from input
  * Output `len` bytes copied from `16 * (xx + 256) + bbbb + 1` ago

Copy commands starting with `00` expect a variable length value to directly follow
the `b`-bits. Each variable length code starts with some number of `0`-bits, then
one `1`-bit, and finally a series of any-bits.

The below table shows how to interpret them, depending on how many `0`-bits there are
at the start of the code.

```
     1      (no 0-bits: no extra bits, varlen = 3)
    01      (one 0-bit: no extra bits, varlen = 4)
   001b     (two 0-bits: 1 extra bit, varlen = 5 + b)
  0001bb    (three 0-bits: 2 extra bits, varlen = 7 + bb)
 00001bbbb  (four 0-bits: 4 extra bits, varlen = 11 + bbbb)
00000       (five 0-bits: no extra bits, varlen = 27 + read next byte from input stream)
```

(Yes, this algorithm's bit patterns are a bit irregular.)

The copy area may overlap itself, so use a memory copy function that allows this.
For example, a series of six `37` bytes could be achieved by outputting one
`37` literal, then using `00100010+00` to copy 5 bytes from 1 byte ago.

Examples:

```
bit 1 (read xx now; output xx as a literal byte)
```

```
bits 011 (copy length is 2 bytes; read xx now)
bits 010 (copy distance is 256 + xx*8 + 2 + 1)
```

```
bits 00 (going to copy varlen bytes...)
bit 1 (read xx now)
bit 0 (copy distance is xx*2 + 0 + 1)
bit 1 (copy length is 3 bytes)
```

```
bits 00 (going to copy varlen bytes...)
bits 0000 (copy distance is 16 * 256 plus something bytes ago... read xx now)
bits 1101 (copy distance is 16 * (xx + 256) + 13 + 1)
bits 000111 (copy length is 10 bytes)
```

```
bits 00 (going to copy varlen bytes...)
bit 1 (read xx now)
bit 1 (copy distance is xx*2 + 1 + 1)
bits 00000 (read yy now; copy length is 27 + yy)
```


FA2
---

File archive format containing multiple files. It uses generic compression, mostly
identical to that in FA1.

FA2 was used by Foster for some of their Windows games.

Already implemented at GARbro:
https://github.com/morkt/GARbro/blob/master/ArcFormats/Foster/ArcFA2.cs

Unpacking code for this is in [/inc/decomp_bundles.pas](../../inc/decomp_bundles.pas).


##### Header

	char[4] - signature "FA2" 00
	uint32 - flags, 1 = compressed file list
	uint32 - file index start ofs, close to end of file
	uint32 - file count


##### File index

If the file index is compressed, use the same decompression algorithm as for
the files themselves, described below.

The file index is an array of 32-byte entries:

	char[15] - 8.3 filename, including dot, padded with nulls
	byte - attribute flag: 1 = not compressed, 2 = compressed
	uint32 - unknown, first byte always 00? somewhat random-looking, but not unique
	uint32 - unknown, pretty big numbers, not in series, not unique
	uint32 - uncompressed file size
	uint32 - file size in archive, compressed

The file data is all appended together directly after the FA2 header, from 0x10 onward,
in the same order as found in the file index. Each file begins at the next 16-byte
boundary after the previous file ended.


##### Decompressing files

For both the file index and the files themselves, use the same decompression algorithm
as FA1 uses, except:
- The flag word is now 32 bits, so read a uint32 each time you run out of bits;
  it's still processed from the top bit 0x8000_0000 downward.
- The special case `011 111` with `xx = 255` now means end of compressed data.
