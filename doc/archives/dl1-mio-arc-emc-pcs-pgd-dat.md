Himeya Soft and C's Ware archive formats
----------------------------------------

### MIO

Used in Nanaei 1, Ash.

Header:
```
char[12] - sig "MIODATA000" 0D 0A
uint16 - unknown, always 0000?
uint16 - number of files
```

The file list follows immediately, where each file is:
```
char[12] - filename, 8.3 format including dot-suffix, padded with zeroes
uint16 - file size
uint16 - starting sector (byte offset = sector * 0x400 + baseofs)
```

Baseofs is the first 0x400 boundary after the file list, usually 0x800.


### DL1

Used in Nanaei 2, YES-HG, Divi-Dead. Basically the same format as Aaru's FL1.
Documented by VileVN: http://vilevn.sourceforge.net/doxygen/classArchiveCWareDL1.html

Header:
```
char[8] - signature "DL1.0" 1A 00 00
uint16 - file count
uint32 - file index start offset
```

The file index lists all files in the archive, where each file is:
```
char[12] - filename, 8.3 format including dot-suffix, padded with zeroes
uint32 - file size
```

The first file starts at offset 0x10, and each file starts where the last ended.


### ARC, EMC

Used in more recent Windows titles: Chiruhana, Heart & Blade, Sho-Ki, Vist.
Starts with a game sig, and at 0x10 "EMSAC-Binary Archive-2", then
a file hierarchy including full source paths.
The files don't appear compressed or encrypted, just straight BMP files again.


### PCS

Used in more recent Windows titles: Chirin.
Starts with "PCCS" signature and simple file list.
Already documented at GARbro:
https://github.com/morkt/GARbro/blob/master/ArcFormats/CsWare/ArcPCS.cs


### PGD

Game script files archive, used in Love Producer.


### DAT

Used in Fugue. Uncompressed game resource archives.
Already documented at GARbro:
https://github.com/morkt/GARbro/blob/master/ArcFormats/CsWare/ArcDAT.cs

Looking at pic.dat, there are obvious BMP headers at 0x07CE, 0xA5448, 0xDF35D, 0x184AA8.
This yields first three file sizes of 0xA4C7A, 0x39F15, 0xA574B.
Seq.dat (game scripts) has "MMTM" signatures at 0x5F7, 0x283E, 0x4C25, 0x7772.
First three file sizes are 0x2247, 0x23E7, 0x2B4D.

Although the file size is part of the BMP header, it's not apparent in script
headers. Therefore either the sizes or start offsets must be in an index in the
archive header.

The archive starts with two uint32's. The first is presumably the number of files
in the archive, the second the byte size of the index chunk. This is directly followed
by the index. The index contents look like random bytes, although both pic and seq
dats start with the same three bytes. The index must be compressed or encrypted.
The GARbro code suggests it's plain zlib.

Seq: 0x5EF containing 0x9C items; Pic: 0x7C6 containing 0xBB items...
- 1519 / 156 = 9.737
- 1519 mod 156 = 115
- 1519 div 156 = 9, with 115 bytes left over
- 1990 / 187 = 10.642
- 1990 mod 187 = 120
- 1990 div 187 = 10, with 120 bytes left over

From this can be deduced that each file entry in the index must have a variable size.
There are not enough bytes to allow for a full filename, but if they each have
a short name like 52.BMP (maybe even omitting the suffix), that leaves space for
one or two uint32's per file designating the start offset and/or byte size.
