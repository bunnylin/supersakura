ScooP archive formats
---------------------

##### ScooPX archives

MindrustUK investigated this format. Thank you!

This is a very simple archive format containing multiple files of any type.
I don't know if it has a proper name, so I'll just call it ScooPX.
Words are x86-native little-endian.

Header:
```
byte - number of files in this archive
uint32[filecount + 1] - array of start offsets; the extra one should be equal to eof
```

In some Windows games, the header starts with a uint32 count, instead of a byte.
If the last start offset (whose value is the file bytesize) is aligned at 4 bytes,
the header started with a uint32; otherwise it must have started with a byte.

The contained files immediately follow the header.


##### Parrot archives

Used in later games. This has a recognisable signature, and the data is compressed.

Header:
```
char[10] - signature "PARROT1.0", null-terminated
uint16 - number of files
uint32 - image data section start offset
fileinfo[filecount] ...
	uint16 - unknown, 0002, file type?
	uint32 - start offset for null-terminated filename string
	uint32 - start offset for file data
	uint32 - compressed file byte size
	uint32 - uncompressed byte size
```

Haven't figured out what the compression scheme is yet.
