PCK archives
------------

### Panda MAX PCK

Panda House labels Cat's Pro and Melody used MAX PCK archives on some PC98 titles.

These are already well-documented by ValleyBell:
https://github.com/ValleyBell/PC98VNResearch/blob/master/panda_max/PCK-Format.txt

The PCK file is partially encrypted by bit rotation. To decrypt, rotate every uint16
left by 1, from the beginning of the file up to offset 0x4000.

The file begins with a 32-byte header that's mostly unused. The first 16 bytes are
the signature "PCK Version 1.00". Before decrypting, the first 8 bytes are always
`A8 21 25 90 AB 32 B9 39`, perhaps useful for file format identification.

From offset 0x20, the file index follows. Each file entry: (words are little-endian)
```
char[16] - file name including dot-suffix, null-terminated
uint32 - file content start offset, the first is always at 0x4000
uint32 - file byte size
byte[8] - apparently time stamp info?
```

After the last file, remaining entries up to 0x4000 have only spaces as the file name
and zeroes for the other values.

If file data begins with "MAXPACK", it needs additional decompression...


### Neural EVE PCK

A little-known company, Neural, produced an early Windows-native game authoring
system, recognisable by the presence of `EVE.EXE` and `EVE.PCK`. This was licensed
to various companies, such as Jast/Tiare and Panda House/Melody.


### Jast USA VFTech PCK

Used in the Jast USA Memorial collection of the three classic localised ADV games.

Header:
```
char[6] - signature VFTech or VFTECH
uint32 - file list start index offset, off by one
```

File data immediately follows the header from 0xA onward, but you should read the
file index first, which is always at the back end of the file. It starts at the
given offset minus 1. The first listed file should always appear with a start offset
of 0xB, but all file start offsets are also off, so minus 1 from each - therefore,
the first file is really at 0xA.

Index entry for each file:
```
uint32 - file data start offset, off by one
uint32 - file's byte size in this archive
char[] - file name including dot-suffix, null-terminated, variable length
char[] - time stamp string, null-terminated, variable length
char[] - some number string, uncompressed file size or whatever
char[] - another time stamp string
```

Every file's data begins with a uint32 containing the uncompressed byte size of the
following data. The rest of the file data is a Deflated stream. Just run it through
a standard zlib Inflate.
