Himeya/C's Ware ARC
-------------------

### A2/ARC2

Used by various localised Himeya titles and their original Windows ports:
Amy, Gloria, Kotobuki, Love Potion (Etsuraku no Gakuen), Nana Eiyuu Monogatari.

Already documented by vilevn:
http://vilevn.sourceforge.net/doxygen/classArchiveCWareARC2.html

Also found at GARbro:
https://github.com/morkt/GARbro/blob/master/ArcFormats/CsWare/ArcARC2.cs

Words are in x86-native little-endian order.

Header:
```
uint32 - signature "arc2"
uint32 - file count
uint32 - file list offset, always 0x10000?
```

File list starts at the given offset, where each item is 32 bytes:
```
char - file type
char[15] - file name, no suffix, padded with zeroes
uint32 - start offset
uint32 - file size
uint32 - encryption key 1
uint32 - encryption key 2
```

If the encryption keys are not 0, the file is encrypted.

The files are byte-aligned at 256 or even 64k bytes, wasting a lot of space.
Many files also exist in multiple copies, each individually encrypted, wasting
even more space. (Perhaps this was to improve seek times when the archive file
is on a slow CD drive.) It is safe to ignore duplicate files of the same type.

File types:
- a: ANM animation data?
- b: BMP graphic
- l: LVL level file? Something to do with wave samples
- m: MDS midi
- s: SCO scenario script
- w: WAV sound effects and voice lines
- x: SYX midi SysEx
- 0x01, 0x02, 0x03, 0x04: Level layout files for Kotobuki


##### Decrypting files

Start with the encrypted source file buffer, and the two encryption keys.

Pseudocode:
```
For each uint32 in source file buffer:
	combined_key := key1 + key2 (result remains a uint32, drop overflow)
	next output := next source uint32 - combined_key (result remains a uint32)
	key1 := key2
	key2 := combined_key
```

Example keys: 0x10000000 and 0xAC0FFEED (combined: 0xBC0FFEED)

Encrypted data: 80AAFF80, 3C0030D9, D2DB07D3
...decrypts to: C49B0093, D3E032FF, AEAB0B0C
```
80AAFF80 - BC0FFEED = C49B0093, keys now 0xAC0FFEED, 0xBC0FFEED -> 0x681FFDDA
3C0030D9 - 681FFDDA = D3E032FF, keys now 0xBC0FFEED, 0x681FFDDA -> 0x242FFCC7
D2DB07D3 - 242FFCC7 = AEAB0B0C, keys now 0x681FFDDA, 0x242FFCC7
```

### AR

Used only in the Windows port of Fatal Relations (Kindan no Ketsuzoku).
It has three AR-compressed file archives: EWA (graphics), SCA (scripts), WVA (audio).

Header: (words are little-endian)
```
uint16 - signature "AR"
uint8 - encryption key 1
uint8 - encryption key 2
uint32 - file list start offset, always 0xC
uint32 - file count
```

File list follows immediately, where each file:
```
char[8] - filename without suffix, padded with zeroes
uint32 - file start offset
uint32 - file size
```

##### Decrypting files

Same encryption scheme as ARC2, except using single-byte keys, and the same key
pair used for every file.

If the encryption keys are not 0, then every file is encrypted. Reset the keys to
initial values when starting to decrypt any file.

Pseudocode:
```
For each uint8 in source file buffer:
        combined_key := key1 + key2 (result remains a uint8, drop overflow)
        next output := next source uint8 - combined_key (result remains a uint8)
        key1 := key2
        key2 := combined_key
```

Example keys: 14, 70 (combined: 0x14 + 0x70 = 0x84)

encrypted data: D6 3D BE B2
...decrypts to: 52 49 46 46
```
D6 - 84 = 52, keys now 70, 84 -> F4
3D - F4 = 49, keys now 84, F4 -> 78
BE - 78 = 46, keys now F4, 78 -> 6C
B2 - 6C = 46, keys now 78, 6C
```


### ARC3 archives

Used in Adam Double Factor, Desire, Eve Burst Error, Eve Fatal Attraction,
Re-Leaf Plus, Xenon.

Implemented at GARbro:
https://github.com/morkt/GARbro/blob/master/ArcFormats/CaramelBox/ArcARC3.cs

Header: (words are big-endian!)
```
uint32 - signature "arc3"
uint32 - version? always 1?
uint32 - sector size, always 0x800 bytes; if it's different, probably should use 0x800 anyway
uint32 - file data base sector
uint32 - total sectors in file
uint32 - unknown, always 0?
uint32 - file list start sector
uint32 - file list byte size
```

The file list is found at `file list start sector * sector size`, but is not
straightforward to read, almost feels deliberately obfuscated.

```
While file list not fully read:
	i := Read next input byte
	While i == 0:
		i := Read next input byte

	If i == 0xFF:
		filename := previous filename, but increment the final character by 1
		file_sector := Read next 3 bytes as a big-endian uint24

	Else if i > 0xF0:
		name_length := i & 0x0F
		filename := Read next name_length chars
		file_sector := Read next 3 bytes as a big-endian uint24
		If (file_sector & 0x800000) == 0:
			Skip the next 3 bytes - not a valid offset or length, what else could it be?

	Else if i < 0xF0:
		name_offset := i >> 4
		name_length := i & 0x0F
		filename := chars [0..name_offset - 1] of previous filename
		filename := filename + Read next name_length chars
		file_sector := Read next 3 bytes as a big-endian uint24

	If the game is Adam Double Factor:
		While (file_sector & 0x800000) == 0:
			file_sector := Read next 3 bytes as a big-endian uint24
		file_sector := file_sector & 0x7FFFFF

	While the next input byte == 0xF0:
		Skip the 0xF0 byte
		sector := Read next 3 bytes as a big-endian uint24
		This is the starting sector for a complete duplicate of the previous file;
		files may have multiple duplicates
```

If file name length is <= 3, use it as is. Otherwise the filename's first three
characters are actually the suffix and should be moved to the end of the name
after the file list has been fully built. The suffix may be padded with spaces.

The file's data can be found at `(file data base sector + file sector) * sector size`.

Each file begins with a 32-byte header (big-endian words!):
```
uint32 - file size in sectors
uint32 - file size in bytes
uint32 - file size in bytes, same as the first?
uint32 - unknown
uint32 - unknown
uint32 - flags; 0 = not encrypted, 2 = encrypted
the rest - always 0?
```

The file data of given size directly follows the header.

If the encryption flag is set, all file data needs a XOR 0xFF.

If the file data starts with "lz", it is compressed. The next big-endian uint32 after that will
be the uncompressed data size, and the rest of the file data is the compressed bitstream.
Bits are read from 0x80 downward.

To decompress LZ-ZE:
```
Function ReadNumber():
	length := 0
	While length < 16:
		b := Read the next bit
		if b == 1: Break out of while-loop
		length := length + 1
	result := (1 << length) + (Read the next length bits)
	Return result
	(example: 0001,010: (1 << 3) + 2 == 10)

While output has not reached final uncompressed size:
	Read next 2 chars from input; these must be "ze"
	chunk_unpacked_size := Read next big-endian uint16 from input

	While chunk_unpacked_size not reached:
		i := ReadNumber()
		Output i bytes of literals, 8 bits each
		If reached chunk_unpacked_size in output, or no more input bits: Break out of while-loop

		copy_dist := ReadNumber()
		copy_len := ReadNumber()
		Output copy_len bytes from copy_dist bytes ago in the output
```

##### Problems

- Adam, English version, data_cd1.dat may be cut short, the last file isn't complete
