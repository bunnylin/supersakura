Excellents archive formats
--------------------------

### ARY archives

Starts with a file count uint32, then an array of absolute file start addresses,
the first always at 0x200. The maximum file count in one archive is 127.
There are no file names, so presumably files are accessed by index number.


### DAC archives

DAC is a "Desire Archive". The `DL.00?` files in Yuugiri are in this format as well.
There's a stray `bingo.arc` in Wakuwaku2 that's also a DAC archive.

These contain DPC "Desire Picture", DAM "Desire Animation", and DSR "Desire Script"
files. Also DPN, probably also an image format. Also HMM, OPI, and OVI music files.

The archive begins with a file index. Each entry is 16 bytes. The index ends with
an all 0 entry. The files are appended in order right after the last index entry.
Words are x86-native little-endian.

```
char[12] - filename including dot-extension, padded with nulls
uint32 - file byte size
```


### DLB archives

DLB is a "Desire Library"? Although these are found in Angel Night by Fournine...
Looks the same as DAC, except with an extra 16-byte header:
```
char[4] - signature "DLB" 0x1A
byte[8] - zeroes
uint32 - offset for start of first file data
```

After the signature, the rest of the header and file need a XOR 0xFF to deobfuscate.
After the header, the file list works the same as in DAC archives.


### CAT+LIB archives (PC98)

These are used on the PC98 for Nocturnal Illusion, Mayclub, GaoGao 3 and 4,
Lilith.

CAT is a file list for the accompanying LIB file.
Unpacking code for these is in [inc/decomp_bundles.pas](../../inc/decomp_bundles.pas),
function `Decomp_ExcellentLib()`.

Some files are compressed within the archive using what appears to be LZ77 Softdisk
Library compression.

CAT header: (words are x86-native little-endian)
```
char[4] - signature Cat1
uint16 - number of files listed
```

The rest of the file is the file list as an LZ-compressed stream.

Decompression pseudocode:

```
flag_byte := Read next input byte
While compressed data remaining:

	If no more bits in flag_byte: flag_byte := Read next input byte

	i := Read next bit from flag_byte (reading from 0x01 to 0x80)

	If i is set:
		Output next byte from input stream as a literal

	Else:
		x := Read next uint16 from input stream
		If x == 0: this signals end of input stream, we're done
		copy_length := (x & 0xF) + 3
		copy_from := (output_offset & 0xFFF000) + (x >> 4) - 1
		If copy_from >= output_offset: Subtract 0x1000 from copy_from

		If copy_from >= 0:
			Output copy_len bytes from output[copy_from] onward
		Else:
			Trying to copy from before start of output buffer,
			so output zeroes for the out of bounds area, then
			output remaining copy_len bytes from output[0] onward
```

The uncompressed CAT contains 22-byte file info structures:
```
char[12] - file name, possibly including dot-suffix, padded with spaces
uint16 - compression type: 0 = uncompressed, 1 = compressed file
uint32 - file size in bytes (compressed size, if compressed)
uint32 - file data starts at (this offset + 6) in the accompanying LIB
```

LIB header:
```
char[4] - signature Lib0
uint16 - number of files, must be equal to the number in the accompanying CAT
```

The rest of the LIB file after the header is file data, as listed in the CAT.
If a file is flagged as compressed, its data begins with the uncompressed file size
in bytes as a uint32, then the compressed data bytes. The algorithm is the same as
used for the CAT file.


### DAT+LST archives (Windows)

Nocturnal Illusion and Mayclub game resources are stored in these. For example
MUG0.DAT, with MUG0.LST an index for it.

The .G images use a pared down Pi format, same as Jast used, easily converted.
Scripts are stored in .S files, with basic obfuscation. There are also
SAVEx.DAT files, which are most likely empty savegames. This suggests that
the game engine looks for files first outside the .DAT, and if they are
not found, then grabs one from the .DAT. This allows easy patching just by
throwing a modified file into the game's directory, and the empty savegames
are overridden by real saves.

Music and sound effects are stored outside the .DAT file, as normal files.
Music is stored in normal midi files, just renamed to .M files.
Except N12.M, which for some reason is a PMD file.
Sound effects are in plain wave files.

The .G images start with two words, MSB first: width and height. Most are
480x304, or 01E0 0130. This is directly followed by the palette, 16 RGB
triplets, followed by the Pi bitstream.


##### .LST index

Header:
```
char[11] - signature: 44 5F 4C 69 62 20 2D 30 32 2D 20, or "D_Lib -02- "
uint16 - 03FF in Nocturne, 015C in Mayclub. Number of resources? Junk?
char[3] - header end: 20 0D 0A
```

The list index itself is a series of:
```
char[12] - null-terminated file name, padded with spaces. If the filename uses all
           12 characters, the resource extractor must add its own null at the end.
           The filename may have a subdirectory embedded, for example "END\1A.G".
uint32 - start offset of this resource in .DAT file
```

Each resource is considered to go from the given offset up to the next
resource's starting offset minus one byte. The list ends with a virtual
resource called "[[End]]     " with a concluding offset value. The .LST ends
with the byte value 1A, the ASCII end-of-file marker.


### DAT in Waku Waku Mahjong Panic 2

These are similar to the DAT+LST combination, but the index is appended at the end of
the file and lightly encrypted.

Read the file's last uint16, x86-native little-endian. This is the byte size of the
file index, which must be a multiple of 16. Subtract this from the total DAT file size
to find the start of the index.

The second-to-last uint16 in the file should be equal to `index start offset & 0xFFFF`.

Decrypt all bytes from the start of the index to the end of the file except the last
uint16 by XOR'ing with 0x99.

The list index has a special first and last entry, but in any case each entry is 16 bytes.

```
char[12] - file name with dot-suffix
uint32 - start offset of this resource in this file
```

The first entry is not a file; it's the signature, "D_Lib" and some Shift-JIS.

The last entry is "[[End]]     " and its offset field is the last two uint16's in
the file, which are the aforementioned file index size and offset.
