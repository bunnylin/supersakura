File system layout
------------------

Although modern software is expected to split its files across multiple locations,
it is objectively neater to keep the entire thing contained in a single directory.
The former is nevertheless the standard and is more secure. Best support both.
The XDG standard suggests keeping data files separate in a third location, away from
the binaries and config files. That's overkill, but I'll allow an override for it.

- Base directory: where the currently running executable is
- Profile directory: GetAppConfigDir(FALSE), typically ~/.config/supersakura
- Tool executables: in base/, but shell out could use environment path
- Configuration and log files: in base/, also in profile/
- Save files: primarily in base/save/, otherwise in profile/save/
- Data files: in a configurable directory, also in profile/, also in base/data/


User Interactions
-----------------

In general:

- Reading text: enter/left-click/confirm to continue.
- Textbox choice selection: highlight choice and enter/left-click/confirm.
- Imagemap choice selection: point & click, or highlight and enter/confirm.
  * To highlight, mouseover such imagemap areas, or move between them with
    cursor keys or directional buttons.
- Skip animations etc by triggering minor (enter/left-click/confirm) or
  major (esc/right-click/cancel) interrupt events.
- Meta-interaction through modal pop-up menu, and summonable dialogs.

While the game is paused, the game graphics are greyed out and all user input
is ignored, except:

- Pause/Ctrl-P or single-step pause
- Ctrl-Q or other attempt to quit
- Alt-Enter to toggle fullscreen
- Ctrl-R and Ctrl-W in console port

Below, input reactions are tested in the given order. As soon as any reaction
is triggered, the remaining items are skipped.


##### Keyboard

Enter
- If skip seen text mode is enabled, disable it.
- If textboxes are hidden, make them visible.
- If the debug console is visible, retrieve the last line, and if it's not
  empty, compile and run it as a minifiber.
- If a pageable box needs to show more content, scroll the box.
- If a mouseoverable without -mouseonly is highlighted and it has a trigger
  label, trigger it.
- If choicematic is active, select the highlighted choice.
- If a box from topmost is accepting text input, and any fibers are in waittyping
  state, resume the fibers.
- If any fibers are waiting for a keypress, resume the fibers.
- If a normal interrupt is defined, trigger it.

Esc
- If skip seen text mode is enabled, disable it.
- If textboxes are hidden, make them visible.
- If the dropdown console is in transcript mode, pop out the box.
- If choicematic is active, and user-cancellable at the current level, cancel.
- If choicematic is active, and not on top choice level, go up a level.
- If an esc-interrupt is defined, trigger it.
- If metastate is normal, summon the right-click popup menu.
- Else metastate is not normal: pop current metastate.

Context menu key, shift-F10
- If skip seen text mode is enabled, disable it.
- If an esc-interrupt is defined, ignore.
- If textboxes are hidden, make them visible, and don't stop at this step.
- If the dropdown console is in transcript mode, pop out the box, and don't stop yet.
- If metastate is normal, summon the right-click popup menu.
- Else metastate is not normal: ignore.

Normal text input
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If the box is accepting text input, add the character there.
  * If choicematic is active in that box, scan from current highlight onward to
    the first choice beginning with the entered character, and highlight that.
    Roll around the choice showlist as needed.

Delete
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If the box is accepting text input, delete some text in the box.

Backspace
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If the box is accepting text input, delete some text in the box.
  * If choicematic is active in that box, and not on top choice level, go up
    a level; or if user-cancellable at sub-level, cancel.

Home/end
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If the box is accepting text input, move the caret.
  * If choicematic is active in that box, move the highlight.
  * If the box is freely scrollable, scroll the box to top or bottom.
- If mouseoverables without -mouseonly exist, find the closest above or
  below to the left from current mousexy and move the cursor there.

Ctrl+Home/end
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If the box is freely scrollable, scroll the box to top or bottom.

Pageup/pagedown
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If choicematic is active in that box, move the highlight by a pageful.
  * If the box is freely scrollable, scroll the box by a pageful.
- If mouseoverables without -mouseonly exist, find the closest above or
  below to the right from current mousexy and move the cursor there.

Cursor keys left/right
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If the box is accepting text input, move the caret.
  * If choicematic is active in that box and has more than one column, move
    the highlight.
- If mouseoverables without -mouseonly exist, find the closest left/right
  from current mousexy and mouseon it, mouseoffing current overable if any.

Ctrl+cursor keys left/right
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If the box is accepting text input, move the caret.

Cursor keys up/down
- If textboxes are hidden, ignore.
- If the debug console is visible, bring up commands from command history.
- For each box from topmost...
  * If choicematic is active in that box, move the highlight.
  * If the box is freely scrollable, scroll the box up/down.
- If mouseoverables without -mouseonly exist, find the closest up/down from
  current mousexy and mouseon it, mouseoffing current overable if any.

Ctrl+cursor keys up/down
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If the box is freely scrollable, scroll the box up/down.

Alt-Enter
- Toggle fullscreen mode.

F2
- Toggle all sound mute.

Ctrl-Plus, Ctrl-Minus
- If textboxes are not hidden, adjust UI magnification level by a step.

Ctrl-0
- If textboxes are not hidden, reset UI magnification to 100%.

Ctrl-A
- Used to be enable/disable skip seen text mode, but too easy to confuse with the
  more universal standard of select all...

Ctrl-B
- If metastate is normal, hide/unhide textboxes.

Ctrl-C (SDL version only)
- If text input is active, copy selection to clipboard.

Ctrl-D
- If debug mode is not enabled, do nothing. (Ctrl-XYZZY to enable debug mode.)
- If metastate is confirm quit, do nothing.
- If the dropdown console is hidden, unhide it, and don't stop at this step.
- If the dropdown console is already in debug mode, slide it away.
- If the dropdown console is not present or is vanishing, slide it in, and
  don't stop at this step.
- Set the dropdown console to debug mode.

Ctrl-F
- If metastate is normal, enable/disable skip seen text mode.

Ctrl-I
- If metastate is not normal, do nothing.
- Summon the metainfo box.

Ctrl-L
- If metastate is not normal, do nothing.
- If skip seen text mode is enabled, disable it, and don't stop at this step.
- Enter saveload metastate, with load highlighted.

Ctrl-M
- If debug mode is enabled, toggle the autotest input generator.

Pause, Ctrl-P
- If metastate is normal or info, pause/unpause the game.

Shift-Pause, Ctrl-Shift-P, Ctrl-Alt-P
- If metastate is normal or info, enter single-step mode.

Ctrl-Q
- If metastate is already quit confirm, exit the game.
- Push quit confirm metastate on the HubStack.

Ctrl-R [console port only]
- Redraw the screen.

Ctrl-S
- If metastate is not normal, do nothing.
- If skip seen text mode is enabled, disable it, and don't stop at this step.
- Enter saveload metastate, with save highlighted.

Ctrl-T
- If not in normal metastate, do nothing.
- If skip seen text mode is enabled, disable it, and don't stop at this step.
- If the dropdown console is hidden, unhide it, and don't stop at this step.
- If the dropdown console is already in transcript mode, slide it away.
- If the dropdown console is not present or is vanishing, slide it in, and
  don't stop at this step.
- Set the dropdown console to transcript mode.

Ctrl-V
- If text input is active, paste from clipboard. (SDL version only)
- Otherwise behave like Ctrl-I.

Ctrl-W [console port only]
- Switches between RGB, LXY, and Truecolor palette mixing modes.

Ctrl-XYZZY
- Typing this enables debug mode.


##### Gamepad

These are used through SDL, so the console port can't use gamepads. The pad
buttons are mapped through SDL pretty well, and there's an environment
variable for overriding the mapping, so no need to provide a graphical
button configuration dialog in-engine. Although maybe a swap confirm/cancel
button option would be useful for people accustomed to the reverse.

Confirm button (low position)
- Same as enter key.

Cancel button (right position)
- If skip seen text mode is enabled, disable it.
- If textboxes are hidden, make them visible.
- If the dropdown console is in transcript mode, pop out the box.
- If choicematic is active, and user-cancellable at the current level, cancel.
- If choicematic is active, and not on top choice level, go up a level.
- If a pageable box needs to show more content, scroll the box.
- If any fibers are waiting for a keypress, resume the fibers.
- If a normal interrupt is defined, trigger it.
- If an esc-interrupt is defined, trigger it.
- If not in normal metastate, pop to previous metastate.

Menu button (high position)
- If skip seen text mode is enabled, disable it.
- If textboxes are hidden, make them visible.
- If the dropdown console is in transcript mode, pop out the box.
- If an esc-interrupt is defined, trigger it.
- If choicematic is active, and user-cancellable at the current level, cancel.
- If metastate is normal, enter the metamenu metastate.

Log button (left position)
- Same as Ctrl-T.

Start button
- Pause/unpause the game. This needs to be handled early by the input
  handler, same as keyboard Pause/Ctrl-P.

Option/select/back button
- Same as Ctrl-B.

Left/Right/Up/Down
- Same as cursor keys. Needs special handling for repeating the button press
  while the button is held down. 400ms delay after first, then 160ms.

Left/Right shoulder button
- Should be trappable for custom engine use!
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If choicematic is active in that box, move the highlight by a pageful.
  * If the box is freely scrollable, scroll the box by a pageful.

Left stick
- If textboxes are hidden, ignore.
- If still delaying after a previous stick action, ignore.
- If choicematic is active and the choice box is visible...
  * If the box has more than one column, and the stick points predominantly left
    or right, move the highlight. Delay between moves is a constant 400ms for
    first, 160ms for subsequent.
  * If the stick points predominantly up or down, move the highlight. Same delay.
- If mouseoverables without -mouseonly exist, find the closest in the pointed
  direction from current mousexy and mouseon it, mouseoffing current overable
  if any. Same delay.
- If a script polls the cursor key state for purposes of continuous movement,
  the left stick's position is used.
  (Actual cursor keys are checked first, then pad direction buttons, only
  then the stick. The poll command returns an axis value to the script, -32k
  to +32k, where cursor keys and direction buttons are always at digital
  extremes; the script can decide how to interpret the value for itself.)
- For each box from topmost...
  * If the box is freely scrollable, scroll the box up/down. Scroll speed should
    use squared stick values for best precision.

Right stick
- If textboxes are hidden, ignore.
- For each box from topmost...
  * If the box is freely scrollable, scroll the box up/down; squared stick values.


##### Mouse

The mouse interacts with only the topmost textbox that the cursor is over, in
case there are multiple boxes over each other.

Mouse movement
- If textboxes are visible, and the mouse is over a box, and the topmost
  mouseovered box has choices, and the mouse is over a choice, then highlight
  the choice.
- If textboxes are not visible, or the mouse is not over any box, trigger
  mouseon and mouseoff events on mouseoverables.

Left-click
- If skip seen text mode is enabled, disable it.
- If textboxes are hidden, make them visible.
- If the topmost mouseovered box has choices, and the mouse is over a choice,
  select it.
- If mouseovering any box, find the topmost pageable box and scroll it.
- If the mouse is on a mouseoverable which has a trigger label, trigger it.
- Scroll any pageable box regardless of mouse position.
- If any fibers are waiting for a keypress, resume the fibers.
- If a normal interrupt is defined, trigger it.

Right-click
- If skip seen text mode is enabled, disable it.
- If the dropdown console is in transcript mode, pop out the box.
- If boxes are not hidden, and choicematic is active, and user-cancellable at the
  current level, cancel.
- If boxes are not hidden, and choicematic is active, and not on top choice level,
  go up a level.
- If boxes are not hidden, and metastate is normal, and the cursor is over
  any box, hide all boxes.
- If textboxes are hidden, make them visible.
- If an esc-interrupt is defined, trigger it.
- If metastate is normal, enter the metamenu metastate.

Mouse wheel
- If textboxes are hidden, ignore.
- If the topmost mouseovered box is scrollable, scroll the box up/down. If
  a scroll effect is already live, scroll further at triple speed.
- If any other box is scrollable, scroll the box up/down likewise.
- If metastate is normal, and this is not the main script, and no box at all
  is even possibly scrollable, and wheeling up, summon the game log transcript.

Ctrl-wheel
- If textboxes are not hidden, adjust UI magnification level by a step.


Dialogs and metastates
----------------------

To allow gamepad controls, all menus and dialogs must be implemented in-engine.

- Normal: running the game normally
- Meta: running the metamenu, info box, or any meta dialog; game is suspended
- Confirm quit: showing the hardcoded quit yes/no box

Some engine components are shared between metastates, others are state-local.
- Local components are created anew and guaranteed reset to defaults on metalevel entry.
   * boxhub, choicematic, effecthub, eventmatic, fiberhub
- Inherited components are a unique copy from the previous level for continuity, but any
  changes to them don't affect the previous level. The copy is scrapped on state pop.
   * elehub, viewports
- Global components are singletons, the same instance used in all metastates.
   * fontmatic, rendermatic, varmon, savematic

The normal state begins by spawning a MAIN fiber starting at the game's main label.
When entering the META state, a META fiber is spawned at a relevant label;
when entering the QUIT state, a QUIT fiber is spawned. The code for these is in
sakurascript, provided as a _sakumeta script file along with the front end;
a DAT can override these to customise meta functionality.

The local components are in a stack structure, the HubStack. Changes in metastate
push and pop that stack.

The confirm quit state can always be entered from either other mode, and returns
control to that mode if cancelled. Confirm quit can be entered via script, or by
any standard method of the user attempting to close the game window, or Ctrl-Q.
Trying to enter the confirm quit state when already in it will immediately quit
the game. So even if the engine softlocked, trying to close the game window twice
still ought to exit cleanly.


##### Front end

This comes up when running supersakura without specifying a DAT, and is assumed to
be the normal way to get into games. Important to make a good first impression.
It should look nice, and it should be efficient so you can easily get into a game.

What do other engines do?
- Citra and Yuzu: large-iconed file browser, click to add location; functional but
  not pretty
- PPSSPP: tabbed interface, textless large-iconed recent games grid, custom
  filebrowser for adding a game with a grid or list layout and an extra settings
  dialog, animated background; functional, maybe moreso than necessary, pretty
  but the flat rectangle layout can look a bit disjointed
- ScummVM: iconless list or large-iconed grid, buttons for all available actions,
  filter and sort, rounded corners on everything; very functional, just a bit more
  than I'd bother with, and pretty like a good old interface.

Necessary elements:
- SuperSakura logo
- List of available games
- Easy way to add a new game

No splash screen or other deliberate delay. The game list shall be shown instantly.
The user is expected to spend minimal time on the front end, so no point wasting
time on elaborate animations or ambient music. Plain interface blip sounds maybe.

List or grid or both? It would be slow to load large icons for every game, so
text-only. A text grid doesn't look good, particularly with long game names,
so probably have to be a single-column list. However, when a game is highlighted,
the front end should show a banner image for the game, and gameplay stats like
time played and completion percentage. Print stats in the bottom left?

Include a silver star icon on the left side of every completed game, or a gold star
for 100% graphics. (It should only count backgrounds and overlays, not sprites.)

Search field? I don't see it would get much use. However, pressing a key should jump
to the first game in the list whose name starts with that letter. Choicematic ought
to support this across the board.

Game list sorting? It's always sorted bytewise by display name, should suffice.
But the topmost list item will be for adding a new game. This goes to a new dialog.

Load directly into a game? Messy, if the user has a hundred games, how to select
neatly from among saves? No, start the game first, then load.

Engine version information, license text, fullscreen switch? Accessible through the
metamenu as normal even on the front end. The version string should be as obvious
as possible: bottom right corner of the front end, while no banner is visible.


##### Metamenu

General contents of the metamenu:

    Save (Ctrl-S)
    Load (Ctrl-L)
    Text log (Ctrl-T)
    Skip seen text (Ctrl-A)
    Fullscreen (Alt-Enter)
    [Custom entries eg. Viewframe]
    ---
    Settings
    About (Ctrl-I)
    Return to title
    Quit (Ctrl-Q)

This is a pop-up menu that the user can bring up by pressing esc, or
right-clicking, or pressing the menu button. Beside standard functionality,
it also needs to have game-specific options, such as a viewframe toggle, so
some script control is unavoidable.

It needs to support a single column of one option per line. There should be
an optional keyboard shortcut on the same line with each option; in practice this
means a two-column layout with an empty second cell if there's no shortcut.
It needs to support a divider line - the escape code `\-` will draw this.

Are left-side icons needed? Some programs like Lazarus have action-associated
icons alongside menu items. For big menus, that may help visual navigability,
but for a small one like this it's not important.

Are checkmarks needed? Some programs show a tick on the left side of a togglable
menu item to indicate if it's enabled. This is user-friendly, but not necessary
for the options this menu offers. It is immediately obvious if fullscreen and
viewframe are currently enabled, so a tick won't communicate anything new. Skip
seen text is automatically disabled by any access to this menu, so it's always off.

Since the metamenu should be controllable by gamepad, it needs to be handled
in-engine. The simplest way to do this is to use the existing textbox system,
and hand control to a special metamenu script label after pushing a new
metastate on the HubStack.

When the metamenu or any other fiber it has spawned invokes the word of power
meta.exit, restore local hubs. Note that variables are not local; the metamenu
script can carry useful state information in normal script variables.


##### Settings menu

Take a screenshot of the existing game state, and blur it. Dynamic runtime gob.
Over this, a fullscreen gradient overlay, the same as used on the front end, at
50% alpha. Dialog elements are drawn over this.

Design considerations:
- Group settings using tabs or pages
- Needs good usability by all input methods
- Settings should have clear values, not mysterious icons or animated toggles
- Use greying out and clear highlighting to indicate active dialog area
- Use color on setting values but only for emphasis, must be readable even without
- Each setting has a description explaining what it does to minimise confusion
- Show description on highlight

Window size probably doesn't need to be editable graphically. The auto option
is good enough for most cases, and otherwise the user can edit the ini file.
Trying to set a window pixel size would be a bit annoying in a gamepad-driven
menu, due to the large number ranges involved. Fullscreen mode is also always
at the native resolution now, so no settings required for it.

Settings that won't be needed:
- autosave settings (defaults should be sufficient)
- censorship level (it's game-specific, easiest done upon newgame if at all)

Setting groups:
- Graphics
   * UI magnification: when selected, brings up a slider? Defaults to current
     size multiplier. Value should be displayed as a percentage, and since its
     useful values are exponential? logarithmic?, it may be best to do something
     like an x*x function from the slider's linear value and scale it to
     a reasonable minimum and maximum bounds, where 32k is 100%.
   * Pixel scaler: Hides the setting dialog, showing the existing game view with
     a new choicebox with available scalers. Currently selected ought to be in bold?
     On highlight of any choice, change scaler to that and rerender all. Selecting
     a choice makes that scaler permanent; cancelling out restores the previous.
   * Font selection: 2 comboboxes, for Latin and Japanese; when selected, brings
     up a list of enumerated font face names, default-highlighting the currently
     selected font.
   * VSync: enabled/disabled
- Audio
   * Sound: enabled/muted
   * Sound volume: slider
   * Effects volume: slider
   * Speech volume: slider
   * Interface sounds: enabled/disabled
   * Synthesizer: BunnySynth/FluidSynth/Windows; disable FluidSynth option if n/a
   * Soundfont: path, disable if not using FluidSynth
- System
   * Protagonist name override: string; don't really want to add this because it may
     ultimately require a virtual keyboard, but it would be intolerable not to be
     able to name your daughter in PM2 so a naming method is needed. For most games
     there's no explicit name select, it either uses the default or this override if
     specified; games like PM2 can always bring up a name select on new game.
   * Analog stick deadzone: slider
   * Internal translator: auto/enabled/disabled
   * Translator literations: enabled/disabled
   * External translator: enabled/disabled
   * Translator command: string
   * Test external translator

```
+-----+         +-----+ +-----+
| Gfx | +-----+ | Sys | | Ok! |
+-----+ | Aud | +-----+ +-----+
        +-----+
+-----------------------------+
| Tweakable thing:      value |
| Another one:         string |
| Slidable value:       value |
| <======@==================> |
| Togglable thing:    enabled |
|                             |
+-----------------------------+
+-----------------------------+
| Highlighted setting         |
| description, very helpful.  |
+-----------------------------+
```

When using a keyboard or gamepad, the tab row starts in focus. Selecting one
changes focus to the list of settings. Selecting a setting may make a further
focus change or change the view. When using a mouse, mouseovering either
section immediately switches focus. Focus is indicated by the highlight box,
which is either around a tab box, or around a setting line.

The top tab buttons ought to be four separate textboxes. At least one is always
selected, indicated by a bold font and color change, and the tab box is offset
downward slightly. Using the keyboard or gamepad immediately selects a new tab,
showing a new list of settings. Using the mouse, must click a tab to change, but
change the color halfway on mouseover. The middle two boxes are aligned on both
sides of the horizontal middle; the other two boxes snap to them, so the tab
collection is always centered. (Invisible spacer gobs to space out the buttons?)

The setting list should be presentable as a two-column choicebox. Set to scrollable
on demand, in case font is too big for all settings to fit. Sliders and other
possible widgets must be managed by the textbox. Can use OnHighlight callbacks and
GetHighlight to implement interaction. Plain choice select should also work, since
it'll go right back to the latest highlight on immediate choicematic reactivate so
looks as if it never deactivated. Must trap and handle left/right keys explicitly.

When tabs are in focus:
- Choicematic is disabled but settings list remains pre-printed
- Highlight box is directly controlled by script over the active tab
- Mouseover highlights a different tab
- Left/right or click selects a different tab (don't wrap horizontally),
  clears and prints relevant settings list
- Down/confirm moves focus to the settings list
- Mouseovering the settings list moves focus there
- Esc/cancel/right-click exits the settings menu
- Selecting the rightmost "OK!" tab also closes the menu

When settings list is in focus:
- Choicematic is enabled
- Up/down or mouseover changes highlight in choicematic, standard choice controls
- Confirm/click selects an option
- Right-click is ignored
- Esc/cancel moves focus back to the tabs
- Left/right moves focus back to the tabs and immediately selects a different tab
- Mouseovering the tabs area moves focus back there


##### Game converter dialog

The topmost option in the front end is "Add a game..." which goes to a game
converter dialog. This is effectively a directory browser, allowing to select
an image file or local directory to feed into sakutool.


##### Saving and loading

Since save and load dialogs are functionally very similar, they can be merged
into a single full-window dialog. The dialog can always be accessed for loading,
but saving is only available if script-controllable _allowsaving is true. This
should be false in the front end and a game's main title screen.

The engine's components must be able to serialise and deserialise their state.
If connected to simple quicksave and quickload commands, this is a simple
operation. But to manage multiple save slots, a graphical interface is required.

The lowest metastate is the game, so a save/load dialog on a higher metalevel
can still generate a savestate from the exact game metalevel. Loading similarly
replaces the game metalevel, popping off all other metalevels.

The _sakumeta.saveload script brings up a full-screen dialog containing on the
left a vertical choicebox with existing savegames and an empty slot for a new
save. The list box uses the full dialog height. To its right, an area for
a thumbnail and a transparent textbox for three lines of text. Below the details
area are an extra textbox for the active save directory, and finally four
mouseoverables lined horizontally: Save, Load, Delete, Cancel.

The savegame list contains all compatible saves found in two possible save paths.
The list is built on dialog spawn with sys.listsavestates. If anything beside
the base front end dat is loaded (sysvar.activeProjectName != supersakura) then
a save file's first dat must match the first currently loaded dat name; this
ensures only the active game's saves show up, but also allows attempting to share
saves between mod patches. If only the front end dat is loaded, then the list can
contain all found saves. The save list should be sorted, newest on top. The list
shows specifically the description field of each save file. If no valid save files
found, the list has an unselectable item to that effect.

Save files use the first loaded dat name, dot, ascending index number, and
suffix ".sav". Autosaves have an "auto" prefix before the index number. This makes
it easy for the user to copy or delete a particular game's saves in the file system.
The file specification is in sakusavematic.pas. The active save game paths can be
checked in the About dialog, and are also shown in the uneditable textbox at the
bottom of this dialog.

Pressing left/right moves between the action buttons, while pressing up/down
moves in the choicebox. The transparent textbox on the right side is seeded with
either the currently highlighted save's data, or with data that would be used if
you were to save a new game: first row has the activeProjectName (last dat) used
by the save file, second row has the save file name, third row has the file
modification timestamp. Above this box is a thumbnail associated with this save.
For a new save slot, the thumbnail says "new save".

If the dialog was entered by requesting a save, the Save action is initially
selected, otherwise the Load action.

Esc/right-click/cancel selects cancel; same again exits the dialog.

Selecting Save: generate a thumbnail render from the game metalevel, showing it
instead of the slot's previous thumbnail. It disables all buttons, leaving the
Save button visually pressed. It highlights the save details box, enabling user
typing in it. If this was an existing slot, the previous description is retained
as initial, or if a new slot, the field starts empty. The activeProjectName and
date-time are replaced by a line "Save description:" and another line for user
input. Two new buttons under the details box, Save and Cancel, mouse-only
overable and clickable. Enter/confirm selects Save, esc/cancel selects Cancel.
Saving over an existing slot brings up an extra confirmation box with OK and
Cancel buttons. If the description is left empty, use "Save with no name" as default.
Pop back to game metastate if saved with a message box confirming save worked.

Selecting Load: the "new save" item can't be loaded. If currently not in front end
and saving is allowed, bring up a confirmation box, else loads without confirming.
If current dat list has anything not listed in the save file's used dats list, unload
all dats except front end. Verify that currently loaded dat order is same as in save
file; if not, reload every dat starting with the first divergent. Failure to load
a dat shows error message and cancels load. After load completes, pop back to game
metastate.

Selecting Delete: the "new save" item can't be deleted. Bring up a confirmation box,
Delete and Cancel buttons. If deleted, saveload dialog needs to refresh the save
list accordingly. Either way, remain in the dialog.

A quicksave method can exist for writing autosaves. Also toggled by Ctrl-Shift-S?
Needs 2+ slots that alternate. Each time there's a gfx.transition command, and saves
are allowed, and this isn't the same label as was last autosaved, and it's been 2+
minutes, do a new autosave. Autosave period and slot count can be adjustable.
The autosave timer is reset on any DAT load, including at engine startup. Shouldn't
autosave too often to avoid unnecessary disk writes. Autosave files have an empty
description, which is displayed as "Autosave".

Autosave slot selection: list all autosave files; if total is fewer than allowed
slots, add new slot; else overwrite file with the oldest timestamp.

Seen strings in a game are written to the game's global save file whenever a save
state is otherwise created.


Font handling
-------------

SuperSakura uses SDL, which uses Freetype, which leaves font provisioning to be
the application's responsibility. However, operating systems tend to encourage using
their own font rendering instead of accessing font files directly. The typical solution
is to ship fonts with a game itself, which runs into copyright problems and potentially
pointless font duplication, or to use a common default font all users hopefully have,
but default fonts are different depending on platform and none are guaranteed present.
There's no widely-accepted tool bridging this gap...

Have to make a best effort to pick a reasonable font from whatever the user has
available, making it easy for the user to pick another one if they want.

The majority of relevant games happily used the system font, so it's reasonable for
SuperSakura to also just use a single default font across the board. Theoretically,
letting textboxes use script-selected fonts is useful, but in practice that's not
a common occurrence and it runs into font availability issues. It may be best to
define broad font families, where the user specifies their preferred default for
each family (or let the engine pick a font), while a script can select a font family
for each text box, with a font face name as a soft primary preference.

A further problem is that font face names are different from their file names, and
you can only get the face name by somehow parsing or loading the file. A good user
experience requires matching primarily against a face name.

On Linux, it's possible to call `fc-match -a` to get a list of all known font names.
This also shows the file name, but not file path. However, this can be used to match
a face name string to a file name, and then find the file in usual font locations.

On Windows, it seems all or most fonts are enumerated in the system registry, at
`HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts`. Again, it yields file names,
not paths, but the names can probably be found in usual font locations.

The user interface for choosing a font is primarily through the engine settings dialog.
This needs to be offer a list of face names. When one is chosen, it should show some
example text in a modal box and in another ask in the previous font if that looks fine,
with OK and Cancel choices. To keep the list short, filter out "Bold" and "Italic"
variants; putting those in separate files always seemed weird to me. If the user
explicitly wants one of those, they can use the secondary font selection method.

The secondary method is to edit the configuration file, `supersakura.ini`, and set
the font setting to an existing font file name (or face name).


##### Font sizes

A script must ask for a particular proportional font height when creating a textbox.
The user has a font size multiplier knob to modify that for bigger or smaller text,
for best comfort. The engine calculates the pixel size and gets SDL_TTF/FreeType.to
load a font file at the proper size.
