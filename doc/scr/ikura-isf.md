Ikura ISF bytecode
------------------

There is a some kind of Ikura script to Ren'Py converter (among other things) at:
- http://wks.arai-kibou.ru/ae.php?p=dl

Also see ViLE, Arctool, and xkazoku:
- http://vilevn.sourceforge.net/doxygen/classIkuraDecoder.html
- https://proger.me/vn/old/#arctool
- https://github.com/nonakap/xkazoku

Significant additional reverse engineering was done and shared by @MindrustUK. Thank you!

The script file starts with a short header, then a uint32 array of jump addresses,
and the bytecode immediately follows that.

Before the header, there may be an extra game signature that needs to be skipped to
be able to read the header. All script address references may need to be shifted
to account for that skip.

Header: (words are x86-native little-endian)
```
uint32 - base offset, end of the jump list
uint32 - Signature? 0x00009795 = the rest of the file is encrypted; 0 = not encrypted
uint32[] - jump addresses, up to just before the given base offset
```

If the file is encrypted, decrypt by bitwise-rotating every byte right by two bits.
Eg. `03 AF` -> `C0 ED`. Some games may apply an additional XOR beside this...

The jump addresses are in ascending or equal order, so each address is >= previous.
Jump address 0 is the base offset.
