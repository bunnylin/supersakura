Fairy Dust SRT bytecode
-----------------------

Used in Virgin 2.

These files use a simple encryption: XOR every byte with 0x01, at least for Shift-JIS.
Linebreaks `0D 0A` should be read as they are, and possibly other bytewise commands too?..
Thank to Alatalo for discovering this!

The script files appear to have 4-letter commands interspersed with Shift-JIS, with
some %-prefixed references, probably variables. This script form is likely related to
Fairy Dust's SCP format, but at least the command names are different.
