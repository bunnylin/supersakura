Elf MES bytecode
----------------

### AI1 and AI2 MES scripts

The script engine is pretty advanced for 1989, with similar basic capabilities as
SakuraScript has now, plus some low-level interaction for file and graphics actions.
It uses serial imperative bytecodes with stack-based expressions as parameters
following each command.

Stack-based expression parsing works by pushing operands (values) on a stack, then
an operator that pops one or more things from the stack, acts on them, and pushes
the result back. When no more operands or operators are available, what's left in
the stack can be consumed by the current command as parameters.

Example: the normal statement `let A := 1 + 2 * 3` could be stored as the stack-based
statement `A 1 2 3 * + :=`.

Script commands can take multiple parameters, which are clearly separated by commas.
Example: the above in AI1's hex bytecode would be `9A 41 2C 12 13 14 2A 2B`, or in plain
characters `<9A> A , <12> <13> <14> * +`.
- 9A is the variable set command that takes two parameters.
- The first parameter is variable "A".
- The second parameter is a five-token expression.
- The literal numbers 1, 2, and 3 are encoded as bytes <12>, <13>, and <14>.
- The multiply and sum operators reduce the three numbers to just one.
- No more tokens follow, so the variable set command lets variable A become 7.

It seems there's some kind of size limit to MES scripts, which is probably the reason
some location-specific scripts are split into two or three separate files. If you add
enough extra stuff, the engine hangs for like 30 seconds when attempting to exit the
script if an animation has been active. In DK2, `fl2_a.mes` experiences this at the
exact cutoff point of 24578 bytes of total file size. (That's 0x6002 bytes,
a suspicious number.) Adding extra string stuff at the very end of the file doesn't
cause a breakage, so possibly this isn't a total script size limit but an if-else chain
total size limit. However, nearly the whole script file is a big if-else chain, so...


##### Value codes

`00 xx`

Push single-byte register index.

`01` to `07`

Push register 0 to 6.

`08 xxxx`

Push two-byte register index. Big-endian.

`10 xx`

Push the single-byte number literal xx.

`11` to `17`

Push direct literal 0 to 6.

`18 xxxx`

Push the two-byte number literal xxxx.


##### Operators

`21` or `!`

Not equal to.

`22` or `"`

Double-quotes are used as string indicators.

`25` or `%`

Modulo, probably.

`26` or `&`

Some kind of AND.

`2A` or `*`

Multiply.

`2B` or `+`

Sum.

`2C` or `,`

Comma is used as a parameter or expression separator.

`2D` or `-`

Subtract.

`2F` or `/`

Divide, probably.

`3C` or `<`
`3D` or `=`
`3E` or `>`

Equality comparers.

`40` to `5A`, or `@` to `Z`

Variable reference.

`5C` or `\`

Maybe a bitwise shift and mask to get particular bits from a byte?

`5E` or `^`

Probably XOR.


###### Commands

`7B` or `{`

Block begin.

`7C` or `|`

Comment indicator?.. Shouldn't that be OR?

`7D` or `}`

Block end.

`81` to `98`, maybe `E0`to `EA`?

Shift-JIS game text, print immediately.
Note that `81 93`, a double-width percent sign, is used as a newline.

`99 10-31 , 12` reg 0x31 := 1

Set register?

`9A xx , yy`
`9A 57 , 12` W := 1
`9A 58 , 11` X := 0
`9A 59 , 10-11` Y := 0x11
`9A 49 , 40-10-24 \` I := $@ 0x24 \

Set variable xx to yy.

`9B 40 , 13 , 18-50 00` $@, 2, 0x0050 or 0x5000
`9B 40 , 12 , 18-B0 00` $@, 1, 0x00B0 or 0xB000
`9B 49 , 11 , 10-0A` $I, 0, 0xA
`9B 49 , 12 , 10-0A` $I, 1, 0xA
`9B 49 , 13 , 10-64` $I, 2, 0x64
`9B 49 , 14 , 10-0A` $I, 3, 0xA
`9B 49 , 15 , 11`    $I, 4, 0

Some variable operation... set array?

Set font maybe also?

Use stupid font:
#9b#40 #2c #10#07 #2c #18#ff#03
#9b#40 #2c #10#07 #2c #18#ff#05

Use normal font:
#9b#40 #2c #10#07 #2c #10#11

`9C ...`

Another array set?

`9D 49 10-0B 5C 11 21` if $I 0xB \ 0 != { ... }
`9D 00 48 12 3D` if reg0x48 1 == { ... }

Conditional statement. Must be followed by a {} begin-end block, unless it's
in an A1 choice definition.

A comma 0x2C directly after if-block end begins an else-statement.

`9E`

Start while loop. Should be followed by a `9D` for the while condition.

`9F`

Continue, back to loop start.

`A0`

Break from loop.

`A1 { text , text ... }`

Menu, comma-separated expressions in begin-end block. The expressions are usually
simple strings which are printed immediately and the user can pick one.
The 1-based choice index goes in variable S.
The expressions can also be 9D conditional statements, which may or may not print
a string.

`A2 "string"`

Go to another script.

`A3 "string"`

Call another script.

`A4 xx , { ... }`

Define a procedure. The begin-end block content can then be invoked by the given index xx.

`A5 xx`

Invoke defined procedure index xx.

`A6 18-01-90` wait 0x0190

Wait for some time... If no parameter, wait for keypress.

`A7 x1, y1, x2, y2`
`A7 11 , 11 , 10-4F , 10-C7` 0,0 4F,C7 = 79,199 = full 640x200 context
`A7 11 , 11 , 10-4F , 10-C7`
`A7 16 , 10-9B , 10-4A , 10-C7`

Window? From x1,y1 to x2,y2 inclusive. X coordinates in 8px columns.

`A8 10 3E , 17 "string"`

Select text position.

`A9 10-07`
`A9 14` color 3 (magenta)

Set text color to xx. Low nibble is text color, high nibble background color.

`AA`

Clear textbox?

`AB 49 17 5C` $I 6 \

Print number.

`AC 40 10-24 5C 10-78 13 2A 2B` $@ 0x24 \ 0x78 2 * +, or, ($@ 0x24 \) + (0x78 * 2)

Call, in current script?

`AC xx`

Print string number xx, 0x47 is the player's name in DK2.

`AD`
`AD "string"`

Load and show image? Or just show current.

`AE "image" , 40 13 5C` "image", $@ buffer 2 \
`AE "song" , 40 12 5C`

Load a file, could be anything. Don't display or play yet. The filename is in
double-quotes. The other trailing stuff might be low-level load parameters.

`AF`

Execute?

`B0`

Recover?

`B1 49 , { "string" , 10-7D , 11 }`

Set memory? Set string variable, maybe array of chars?

`B2 13` buffer 2
`B2 14` buffer 3

Dump image to screen from the specified buffer.

`B3 12 , { A2 "main.mes" }` B3 1 begin goto main.mes end

On player interrupt, execute the begin-end block.

`B4 11 , 11 , 18-03-FF` 0, 0, 0x3FF
`B4 12 , 11` 1, 0

Flag?

`B6 13` file in buffer 2
`B6 15 , 12` play sound effect while stuff happens?

Play the music or effect file loaded in the specified buffer.

`B7 13 , 11 , "string"`
`B7 15 , 11 , 14`
`B7 15 , 11 , 10 09`

Start animation.

`B8`

Slot?

`B9 13 , "font file"`

Load a font?

`BA 12 , 14`

Unknown, used identically prior to each jump to a special event at the inn.

`C0` to `FF`

Invoke defined procedure 0 to 0x3F.


### AI5 MES scripts

A logical evolution from previous MES versions. Words are little-endian.

AI5 MES scripts start with a uint16 indicating the start offset for script code. From 0x0002
up to that offset is the double-byte dictionary, for compressing game text. Example:
- If the first uint16 is 0x0002, there is no dictionary, and the script starts immediately.
- It the first uint16 is 0x0082, there are 64 double-byte characters before the script start.

Shift-jis strings are now encoded as follows:
- `61..7F xx` double-byte shift-jis literal, just add 0x20 to the first byte
- `80..FF` dictionary value from this index minus 0x80
- In this scheme, the Exxx shift-jis range seems only available through the dictionary
- Possibly in later games, the dictionary range is `D0..FF`, leaving the literals with
  the larger range of `61..CF` that allows encoding the Exxx range as literals too.

Ascii strings are now no longer in literal "quotes", but are encased in `06` bytes.
Other flow control commands have also been reassigned to low byte values.

Text printing now has a tweakable glyph width value. In AI1, ASCII would automatically
be printed with single-width spacing and high Shift-JIS in double-width spacing. In AI5,
the spacing can be set to single- or double-width regardless of the characters being
printed, which seems more a hassle than helpful. This is controlled by system variable
index 0x15, a uint16 whose low byte is line spacing in pixels, and high byte is width
spacing in multiples of 8.

To use double-width spacing:
- `0D 40 07-57-03 08-23-03 07-43 20 03`
- That is, in the system variables array (0x40), set index 0x15 to 0x200 + 0x10

To use single-width spacing:
- `0D 40 07-57-03 08-13-03 07-43 20 03`
- That is, set 0x40[0x15] to 0x100 + 0x10


##### Commands

`00`

Block end.

`01`

Block begin.

`02`

Parameter separator.

`03`

Value terminator?..

`04 ...`

This is a prefix used to change the next byte's meaning to a different command.

`04 13 01 string 02 string ... 00`

Show menu. The menu items are 02-separated strings in the 01-00 block.

`04 16 xx 03 [02 yy 03]`

If it's `16-33-03`, fade from black.
If it's `16-33-03-02-30-03`, fade to black.

`04 19 ...`

Blit?

`04 1C 06 file 06`

Load file, for example a music file.

`04 1D 06 image 06`

Show image.

`04 1E 06 script 06`

Jump to another MES script.

`04 1F 06 script 06`

Call another MES script.

`04 24 xx 03`

Sound?

`06 string 06`

Ascii string.

`07 xx`

Numeric literal, 1 byte. Only the top 6 bits are used. The bottom 2 bits must be all set.
The 1-byte literal can represent values in the range 0..63.
Examples:
- 07-7F = 31
- 07-9F = 39

`08 xxyy`

Numeric literal, 2 bytes. Only the top 6 bits of each byte are used. The bottom 2 bits
must be all set. That means a 2-byte literal can represent values in the range 0..4095.
Although this engine is natively little-endian, in this case the high byte comes first.
Examples:
- 08-07-3F = 79
- 08-13-DF = 311
- 08-1B-3F = 399; 1B3F = 110 (11) 001111 (11) = 110001111 = 0x18F = 399
- 08-9F-3F = 2511; 9F3F = 100111 (11) 001111 (11) = 1001 1100 1111 = 0x9CF = 2511

`09 xxyyzz`

Numeric literal, 3 bytes, same as above. The value range is 0..262143, although this
engine probably can't handle values above 64k, so the top two bits may also be wasted.

`0A xx yy`

Set RC register?

`0B`

Set RE register?

`0C`

Set var?

`0D ref index value [02, value ...]`

Poke values into the referenced array. The reference is a single character, most often
the @ symbol, which points at a system variables array. The values are uint16's, but
may encode shift-jis code points.

`0E`

Set AB array of booleans?

`0F expression 01 code... 00`

Conditional statement.

`10`

Begin while statement.

`11`

Continue from top of current loop.

`12`

Break from current loop.

`14`

Initialize menu?

`15`

Mouse?

`18 xx 03`

Set "box-inv" color to xx. (printing background color?)

`1A`

Blit-swap?

`1B`

Blit-mask?

`10 xx 03`

Set text color to xx.

`11`

Wait for a keypress.

`12 xx 03 02, 01 procedure content 00`

Define procedure xx.

`13 xx 03`

Invoke procedure xx.

`14`

Returnable call to somewhere.

`15 ref`

Print the number from the specified variable.

`16 [xx]`

Wait for keypress, or wait the specified duration.

`17`

Clear the current box? May produce an unexpected block fill if immediately followed by
an ascii string? In this case output a #61#40 first to trick it into clearing correctly.
However, that's seen in DK3 music mode, where the code is `16 17`, so maybe the 17 is
actually a parameter for 16 rather than its own command?..

`18`

Color??

`19`

Util??

`1A xx 03 [02, yy 03 02, zz 03]`

Show animation.

`21`

Flag?

`22`

Slot?

`23`

Click?

`26`

Field?

`30`..`3F`

Direct number literal 0 to F.

`40` to `5A`, or `@` to `Z`

Variable reference.

`61 00..7F FF` or `61 00..CF FF` in later games

Shift-JIS game text, print immediately. Add 0x20 to the first byte.
Note that `81 93`, a double-width percent sign, is used as a newline.

`80..FF` or `D0..FF` in later games

Print a character from the dictionary, where the range's low end corresponds with
dictionary index 0.


##### Operators

`20`

Sum.

`21`

Subtract.

`22`

Multiply.

`23`

Divide.

`24`

Modulo.

`25`

No idea... "//", whatever that does. Comment, division?

`26`

Some kind of AND.

`27`

Equals.

`28`

Does not equal.

`29`

Greater than.

`2A`

Less than.

`2B`

Not or xor or something.

`2C`

Not or xor or something.

`2D`

Colon?

`2E`

Double-colon?

`2F`

Question mark? Ternary or something? Random number?

`40` to `5A`, or `@` to `Z`

Variable reference.


### Doukyuusei research by Hyperkwu

There's a bunch of research on Doukyuusei here: https://hyperkwu.tripod.com/class.html

Since that site hasn't been updated for years, and is on Tripod, there's a risk the
research will be lost. So for safekeeping, here's a copy of the research, translated.

Doukyuusei room

In this room, we will analyze the PC-98 version of "Doukyuusei" for me!

If you have any information that would be useful for this section, please submit it here.

--------

Part 1: Data

In this first part, we will discuss data. Data here refers to files other than program
files. There are indeed more than 1000 files in Doukyuusei.

Data Files:
- MES: Message File
- PD8: Graphic File
- S4: Animation Data
- PAL: Palette File
- A6: Mouse Data
- C5: Map Data??
- MP: Map Data File
- M: Music File
- MOUSE.DAT: Mouse Cursor Data
- FLAG0: Saved Data File

The easiest to analyze is the PAL file, which contains the palette information as is.
PD8 is being analyzed, but only the coordinates and palette information are understood.

MES files are the core of the game, and are tough to analyze. I understand the first 2 bytes.
Usually, the message is 6x, but if you add 20 to every byte, it appears like solid SJIS,
but that's not enough.

S4 I've found out is animation data with a certain loader, but the internal details
remain unknown.

M files can be played in a shell by typing `PLAY5 XXX.M`. This shows it is a music file.
The format can't be understood without disassembling the program.

The format of A6 wasn't obvious, but I found it contains mouse information. Details will
be explained in part 6. MOUSE.DAT is mouse cursor information by its name, and contains
cursor shape data.

MP is probably map information.

There is a C5, but since the files share names with MP files, they could be MP movement
data (for example, moving a train).

FLAG0 is save data.

--------

Part 2: Programs

There are only four programs, including the installer.

Program Files:
- AI5.EXE: Main program
- AMD.COM: Animation Mouse Driver
- PLAY5.COM: BGM Player TSR
- INSTALL.EXE: Install program

AI5.EXE is the main program of Elf games, and AMD.COM and PLAY5.COM are normal TSRs.
It doesn't matter whether you have install.exe, if you already installed the game, so we
can ignore that in this part.
AI5.EXE could not be disassembled well by disassembler, and it is hard to follow it
by DEBUG.EXE because it is PACKED.

--------

Part 3: Save data

"+xx" means an offset address.

This is the most troublesome save data analysis. I don't know why I chose the hardest
save data as the first one. Once you know this, you don't even have to play the game.
It's laborious to save, exit, and check the file all the time, so I used the super famous
resident memory editor "STAND-DX" to examine the game's memory.

For more information on STAND-DX, please refer to the NON-STANDARD homepage.
HyperEGNet has also created a modification corner using STAND-DX. By the way, the
password near the save data is ID=AI5 PASSWD=0ECD0A.

The first few bytes (10 bytes) are the active MES file at the time of saving. In fact,
the current MES file is included in the game, but unless there is something unusual
(e.g., you saved with a trick save function), the saved data should be myroom.mes.

Next is 00. It seems file names are separated by this 00 in the game. Moving on,
there are flags, which are set when you click with the mouse. For example, when you click
on the desk in your room, the message changes between the first and second clicks.
That is because the first click changes the flag. I haven't studied this flag in detail,
but I think it is between +25H and +51H. This flag changes each time the scene changes,
so I haven't really looked into it too much.

The days of the week are 00 for Sunday, 01 for Monday, and 02 for Tuesday...
The time and date are a bit special: August 10 is 810, or 32A in hexadecimal.
The time is 1030 for 10:30 a.m., 406 in hexadecimal, and 22:30 for 10:30 p.m.,
recorded as 2230, which is 8B6 in hexadecimal, and so on.

Note: This is saved in LSB format, so the bytes will be in reverse: B6 08.

```
Offset  Data size  Content
+00     0AH        Fixed text myroom.mes
+0C     01H        Fixed character $
+25     2CH        Fixed 00 bytes
+52     08H        Likeability (love points?)
+228    02H        Money held
+22A    02H        Day of the week
+230    01H        Day of the month
+248    02H        Time
+250    02H        Mouse X + 8
+252    02H        Mouse Y + 8
+26A    02H        Mouse X
+26C    02H        Mouse Y
+31D    08H        Name (SJIS)
+BF9    07H        Fixed text TAM・TAM (halfwidth)
```

Data is saved in LSB byte order.

Likeability for each person:
- +53 high: 斎藤 亜子 Ako Saitou
- +53 low:  鈴木 美穂 Miho Suzuki
- +54 high: 黒川 さとみ Satomi Kurokawa
- +55 high: 成瀬 かおり Kaori Naruse
- +55 low: 田町 ひろみ Hiromi Tamachi
- +56 high: 正樹 夏子 Natsuko Masaki
- +56 low: 斎藤 真子 Mako Saitou
- +57 high: 桜木 舞 Mai Sakuragi
- +57 low: 芹沢 よし子 Yoshiko Serizawa
- +58 high: 田中 美沙 Misa Tanaka
- +58 low: 仁科 くるみ Kurumi Nishina
- +59 high: 真行寺 麗子 Reiko Shingyouji
- +59 low: 佐久間 ちはる Chiharu Sakuma
- +5A low: 草薙 やよい Yayoi Kusanagi

The space next to "Satomi Kurokawa" is empty, but it is assumed that this used to be
"Kyoko Sakuragi," which was rejected. The graphic is still present.

The following is a list of flags that indicate whether a person has appeared.
If this flag is set, you can see them on your computer, but please note that there
are other flags as well.

```
PC Flag           +77 10
Miho Suzuki       +75 01
Ako Saitou        +75 10
Satomi Kurokawa   +78 10
Hiromi Tamachi    +7B 10
Mako Saitou       +8E 10
Misa Tanaka       +8F 10
Natsuko Masaki    +95 11
Yoshiko Serizawa  +97 10
Mai Sakuragi      +9C 01
Kurumi Nishina    +9D 01
Kaori Naruse      +8C 10
Chiharu Sakuma    +107 10
Reiko Shingyouji  +135 01
Yayoi Kusanagi    +14B 01
```

Order of addresses & order in which they appear on the computer.

The flags in the game going forward are complicated and I will explain them together
with the MES file.

--------

Part 4: Palette file

A palette is a *.PAL file. There are five of them.

- ASA.PAL: morning
- HIRU.PAL: daytime
- YUGA.PAL: evening
- YORU.PAL: night
- PAL.PAL: ???

They are 48 bytes long, but they're not normal RGB files. The palette is in BRG order.
In fact, only 9 bytes from +1B to +24 are different. If you rearrange them into palette
numbers, only 3 of them (9 to B) have differences. However, I don't know where PAL.PAL
is used, so I'll update it when it's understood. The following is a program that reads
this palette and changes the colors on the screen. However, it is for PC-98, uses the
C language, and is not very easy to use. The executable file LP.EXE is here, 6950 bytes.

```
/*  LP.EXE  */
#include <stdio.h>
#include <stdlib.h>

#ifndef outportb
#define outportb(p, c)\
    _asm_c("\n\tOUT\tDX,AL", (char)(c), _asm_c, _asm_c, (unsigned)(p))
#endif

FILE *fp;

main(int argc, char **argv)
{
        char i=16;
        if(!argc) exit(0);
        *++argv;
        outportb(0x6A,0x01);
        outportb(0xA2,0x0D);
        if((fp = fopen(*argv,"rb")) == NULL) exit(-1);
        while(--i){
                outportb(0xA8,15-i);
                outportb(0xAE,fgetc(fp));
                outportb(0xAC,fgetc(fp));
                outportb(0xAA,fgetc(fp));
        }
        fclose(fp);
        exit(0);
}
```

Now you can run `A:> lp asa.pal` on the command line, and the palette will load nicely.
Some loaders do not load this PAL file, so you may want to use it after loading the graphics.

--------

Part 5: Mouse Cursor

The shape of the mouse cursor is in MOUSE.DAT. I would like to upload the graphic, but
I don't want to break the copyright. The format of this data is to read 4 bytes (32 bits)
at a time, and if the bit is 1, output a white dot; if the bit is 0, output a black dot;
and the bitmap will appear. The mouse cursor size is 32x32. 

```
Data (hexadecimal)   Cursor bitmap (binary)
00 00 00 00          --------------------------------
00 00 00 00          --------------------------------
20 00 00 00          --0-----------------------------
18 00 00 00          ---00---------------------------
1E 00 00 00          ---0000-------------------------
0F 80 00 00          ----00000-----------------------
0F E0 00 00          ----0000000---------------------
07 F8 00 00          -----00000000-------------------
```

This is a partial cursor, very easy to understand when viewed in binary.
I attached a sample program. As a sample program, it doesn't work as is, but the
executable file MOUSE.EXE is here, 9360 bytes.

```
#include <stdio.h>
#include <stdlib.h>

FILE *fp
/*  pset(unsigned int x, unsigned int y, char color);  */
main(){
    int c,i,j,k;
    if(NULL == (fp = fopen("mouse.dat","rb"))) exit(-1);

    // (...omitted...)

    for(i=0;i<32;i++){
        for(j=1;j<5;j++){
            c = fgetc(fp);
            for(k=0;k<8;k++){
                pset(j*8-k,i,(c & 1)*7);
                c = c >> 1;
            }
        }
    }
    getch();

    // (...omitted...)
}
```

Cursor bitmaps are in the order: normal 1, mask 1, normal 2, mask 2...
In the game, the mask data is displayed with AND and the normal data with OR.
Since these are animated cursors, there are two frame bitmaps, and when combined
with the masks, there is a total of four bitmaps grouped together.

--------

Part 6: A6 file

Speaking of A6, I had no idea what it was used for until recently, but I finally figured
it out. It is mouse position information. Normally, the mouse cursor is an arrow, but the
shape changes depending on location. A6 contains information about the locations.
This is hard to explain in text, so I tried to make a bullet list.

1. The first 2 bytes represent an ID number.
2. The next 2 bytes each are "upper left X", "upper left Y", "upper right X", "upper right Y".
3. If the next 2 bytes are the same as the previous ID, continue with that ID.
4. If not, the next coordinates are for the next object.
5. When FF is encountered, the file ends. 

Further explanation: An object is a clickable object on the screen. It can be a face or
an eye. An ID number is given to the object, abstractly. If the object is a rectangle,
it's easy to give it a single set of coordinates, but if it's a circle or something,
the object must be divided into several pieces, and each piece gets its own coordinates.
However, since it's a single object divided into several rectangles, they must still all
return the same message, so they have the same ID. The figure below illustrates this.

```
  ,-----,
 /       \
| +-+ +-+ |
| |3| |3| |
| +-+ +-+ |
|  +---+  |
|  | 1 |  |
 \ +---+ /
  '-----'
+---+-+---+
| / | | \ |
|2| | | | |
| | |2| | |
| | | | |2|
| \ | | / |
+---+-+---+
```

In this case, object 1 is singular, so we only need to specify coordinates for one rectangle.
Object 2 is divided into three parts, so we need to give the coordinates three times.
To give the coordinates three times, we have to give the coordinates of number 3 three times
in succession, as shown in step 3. Object 3 must be specified twice as well.

If you want to know more details, please e-mail me. (^^;;

--------

Part 7: S4 File for Animation

S4 contains coordinates and other data necessary for animation. Only the coordinates are
understood.

1. Find 53H in the file.
2. The next 1 byte x8 is the X coordinate of the upper left corner of the copy source.
3. The next 1 byte is the Y coordinate of the upper left corner of the copy source.
4. The next byte is unknown.
5. The next 1 byte x8 is the X coordinate of the lower right of the copy source.

However, as you can see when loading an image with a graphic loader, the graphic and
the animated graphic may overlap if both are displayed on the same screen
(e.g., L.PD8 and LAN.PD8). In that case, Step 1 is a little different. You have to
instead find 12H in the file.

--------

The information in this section is based on my own personal research,
so please do not contact elf. Please understand that I cannot be held responsible
for any mistakes that may have been made.

(end of Doukyuusei research by Hyperkwu)
