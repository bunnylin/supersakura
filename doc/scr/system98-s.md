System98 bytecode
-----------------

There's an old and new System98 engine. The new one was used in a couple games
by Excellents subsidiaries.

ValleyBell has done significant research on these:
https://github.com/ValleyBell/PC98VNResearch/tree/master/four-nine_system98


##### .S script files

These have basic obfuscation. For the Windows games, convert them by decreasing
even bytes' value by one, and increasing odd bytes' value by one. For the PC98
games, XOR every byte with 1. Some scripts start with 256 bytes of non-obfuscated
data, containing a signature and some other junk. Skip it if you see it.

Typically, script logic is at the start of a file, and game text is packed at
the end of the file. The logic calls into snippets of text-printing code when
needed. Bytecode commands are uint16's for some reason, LSB-first order.

The script variables are all of type signed int16. All variables are global, so
their value persists across script changes. Some variables are initialised in the
startup scripts, most notably character names, and constant numbers 0..23. For some
reason variables 3E8..3FF (1000..1023) are used instead of literal values when
passing low numbers as command parameters; you can see variable 3E8 all over the
scripts and it always means a literal zero value.

System-98 supports loading a scriptful of shared code persistently at a high
offset. All other scripts can reference strings or call code in the high area.
The high area is at 0x4000, and the first 0x100 bytes of a script are always useless.
The script loaded into high memory for each game is:
- Lilith: `li_sub.s`
- Mayclub: `may_sub.s`
- Nocturn: `mug_sub.s`

The engine seems to support 16 buffers that can be used for rendering, but maybe
also for other data. Buffers 0, 1, and 2 tend to be used for graphics; buffer 3
might be the same thing as textbox 3.


##### Commands

`0000`

Quit game.

`0001 bbbb xxxx yyyy zzzz aaaa`

Some text box thing.

`0002 xxxx yyyy ffff tttt aaaa cccc` (PC98)
`0002 xxxx yyyy ffff tttt aaaa cccc gggg` (Windows)

Show a graphic. The Windows engine version has an extra parameter word.
xxxx and yyyy are the X and Y pixel offset. On PC98, multiply x by 8.
ffff is 1 to draw without a viewframe (fullscreen), 0 to draw within a viewframe.
tttt is 0 or 1 if the image is a background/overlay, 2 for sprite or fullscreen thing.
aaaa points to a null-terminated graphic name, which may or may not have a suffix.
cccc is 0 for no transparency (background), 1 for transparent image (overlay, sprite)
gggg on Windows only is unknown; usually 1; it's 2 for BG01 and BG02 in start.s and open.s.

Examples:
- `0002 0000 0000 0000 0001 "Z64"  0000 0001` - draw background
- `0002 0000 0000 0000 0001 "Z64n" 0001 0001` - draw overlay over background
- `0002 0028 0000 0000 0002 "A05a" 0001 0001` - draw sprite over bkg
- `0002 0028 0000 0000 0002 "A05b" 0001 0001` - draw sprite over A05a
- `0002 00A0 0000 0000 0002 "B11a" 0001 0001` - draw sprite at about ofs 0,206 (0,$CE)
- `0002 0098 0000 0000 0002 "E35a" 0001 0001` - draw sprite at about ofs 160
- `0002 0000 0000 0001 0002 "E38a" 0000 0001` - draw fullscreen image
- `0002 0000 0000 0001 0002 "E38b" 0001 0001` - draw fullscreen image over another
- `0002 0000 0000 0001 0001 "E41"  0000 0001` - draw background without viewframe
- `0002 0000 0000 0000 0002 "011"  0000 0001` - draw background in Nocturn
- `0002 0000 0000 0001 0001 "045"  0000 0001` - draw fullscreen image in Nocturn
- `0002 0000 0000 0000 0001 "16A"  0001 0001` - draw sprite in Nocturn

In Nocturn, overlays are a slight problem: they are saved as deltas over the base
background they're modifying, and the original engine composites them on the fly in
a memory buffer with the 002F command. But since that implicitly overwrites anything
previously in the memory buffer, it must be treated as a command to clear all sprites
and previous overlays before drawing the new one, and that easily ends up wiping out
the new overlay while at it. So in Nocturn, show overlays as replacement backgrounds.

`0003 bbbb xxxx yyyy wwww hhhh nnnn`

Set text printing rectangle. The coordinates would seem to be in 16x16 characters,
not pixels. Box index is bbbb, where indexes < 8 are borderless, simple back-filled,
used in viewframed mode. Indexes >= 8 can have a border, used for fullscreen events
or the choice box.

Nocturn choicebox: The top level (verb) box is index C, xxxx is 0, nnnn is 1.
A secondary (object) box can appear on top of the first box: index D, xxxx is 2.
Other boxes:
mug_sub.5A68 sets box 3 to 3,13 22*06; nnnn is 0. (Subhook 12; main text box, viewframed.)
mug_sub.69EA sets box B to 3,12 23*07; nnnn is 4. (Main text box, fullscreen event.)
mug_sub.6B86 sets box F to 8,9 0D*03; nnnn is 1. (Clickable map location description box.)
the same also sets box 2 to 4,0 11*09; nnnn is 0. (unused?..)
mug_sub.79D8: sets box 2 to 4,0 11*09; nnnn is 0. (?)
0_00 sets box 3 to 7,A 1E*04; nnnn is 0. (Dramatic interstitial, middle of the screen.)

Mayclub choicebox: In this case the choice box is the main box, so you can't distinguish
the two by their coordinates.

`0003 0003 0003 0013 0022 0006 0000`

This usually marks the beginning of Mayclub choice definitions, followed by 0022-03E8.

`0003 0003 000x 000A 00xx 000x 0000`
`0003 0003 0007 000A 001E 0004 0000`

Nocturn: Prepare to print a dramatic interstitial in the middle of a black screen.
The next fade to black should fade out this text, returning to normal print mode.

`0004 bbbb`

Clear and hide textbox, box index bbbb. Index < 8 frameless boxes in viewframe,
>= 8 for bordered boxes used eg. in fullscreen events.
Nocturn: box 3 is the dramatic interstitial.

`0005 bbbb`

Clear textbox without hiding it?

`0006`

Some kind of palette effect?

`0007 xxxx tttt`

Fade from a blackout or whiteout; xxxx = 0 for black-to-normal, 1 for white-to-normal.
tttt is the duration, unit: about 400ms.

`0008 xxxx tttt`

Fade to a full blackout or whiteout; xxxx = 0 for fade-to-black, 1 for fade-to-white.
tttt is the duration, unit: about 400ms.

`0009 xxxx`

Some kind of palette effect? Lock a color to not be affected by fades?

`000A aaaa`

Jump to aaaa unconditionally.

`000B vvvv wwww`

Compare two variables. `$cmp := variable vvvv - variable wwww`
There should be one or more immediate 0D..12 conditional jumps after this.

`000C vvvv xxxx`

Compare variable and an immediate value. `$cmp := variable vvvv - xxxx`
There should be one or more immediate 0D..12 conditional jumps after this.
(Nocturn script 9_42 for example has two conditional jumps after a single compare.)

`000D aaaa` - Go to aaaa if equal (`$cmp = 0`)

`000E aaaa` - Go to aaaa if less than (`$cmp < 0`)

`000F aaaa` - Go to aaaa if more than (`$cmp > 0`)

`0010 aaaa` - Go to aaaa if less or equal (`$cmp <= 0`)

`0011 aaaa` - Go to aaaa if more or equal (`$cmp >= 0`)

`0012 aaaa` - Go to aaaa if not equal (`$cmp != 0`)

`0013 vvvv aaaa aaaa ...`

Jump to address index selected by value of variable vvvv. There's no indication of
how many valid jump addresses follow. Often the first jump address is immediately
at the end of this address array, so you can use it as an array terminator, but this
doesn't always work. Seeing a very low jump address probably means it's actually
a command, so the table has ended, but that's a bit fuzzy.

Furthermore, these case jumps are used to select from multiple messages when a user
takes an action repeatedly (the classic "try command until message repeats").
The counter variable for which message to display is incremented before the case jump,
and the last case code decrements the variable, so the last message keeps repeating.
But this means in these cases you never actually jump to index 0, and index 0 may be
an invalid address; index 1 is the first real case.

It's unclear what should happen if the specified index is negative. Fall through?
This is rare, but happens in Mayclub98 Z51.03EA, which sets v206 to -1 and calls
subhook 0, which jumps to MAY_SUB.5EFA... Interestingly, in Mayclub_e, the same sets
v206 to 4095. Does that mean the variables are 12-bit?

`0014 vvvc vvvr vvvg vvvb`

Edit palette: color index from variable vvvc; new color from the other variables.
Used in mayclub e09 to change hair color.

`0015 bbbb aaaa`

Print text sequence from aaaa in box index bbbb. Box index is < 8 for a frameless box
when there's a viewframe present, or >= 8 in fullscreen mode with a framed box.

If 0003 has been called, this prints newline-separated choice strings instead of
game text. 0040 lets the user select one, ending 0003's scope.

`0016 xxxx yyyy aaaa`

Also print text?

`0018 vvvv xxxx`

Set variable vvvv := immediate value xxxx. (signed int16, int32 if vvvv >= 0x400?)

Mayclub:
- 0126: number of tickets
- 0127: month
- 0128: day
- 0129: time of day
- 012B: weekday, sunday = 0, monday = 1, ... saturday = 6
- 0131: amount of money on bank account, only used on PC98

`0019 vvvv wwww`

Set variable vvvv := variable wwww.

`001A aaaa`

Play music, aaaa points to null-terminated filename.

`001B`

Fade out music?

`001C`

Stop music.

`001D vvvv`

Get user's chosen music device index into variable vvvv. This is normally followed by
a case jump to play a song file adapted for that device.
- 0: none
- 1: YM2203 OPN FM
- 2: YM2608 OPNA FM
- 3: GS midi

`001E vvvv`

Wait for a keypress. Keycode saved into variable vvvv?

`001F aaaa`

Go to script named at aaaa; name is null-terminated, preceded by index byte.

`0020`

Unknown.

`0021 vvvv`

Delay by var vvvv, each unit about 50ms?

`0022 vvvv`

Text speed, set delay after each character to the value of variable vvvv.

`0022 0120`

Often at the end of choice prints, indicating normal (slow) speed.

`0022 03E8`

Often at the beginning of choice prints (except sometimes not). If there's
an 0003 near before this, then definitely some choices follow. Also, if there's
an 0004-xxxx near before this, that also indicates choices will follow. Or, if
this is directly followed by 0015 printout, also likely choices.
The variable 3E8 is probably set to a constant value of 0, so instant text output.

`0024 vvvv xxxx`

Add immediate value xxxx to variable vvvv.

`0025 vvvv xxxx`

Subtract immediate value xxxx from variable vvvv.

`0026 xxxx yyyy wwww hhhh zzzz`

Unknown... text clearing?..

`0027 vvvv wwww`

Add variable wwww to variable vvvv.

`0028 vvvv wwww`

Subtract variable wwww from variable vvvv.

`0029 vvvv xxxx yyyy`

Reading from the screen at var xxxx, var yyyy into variable vvvv?..

`002A vvvv wwww zzzz`

Writing to the screen?.. All three parameters are variable references.
At the start of GaoGao3 wf_open, there's `2A: 900, 0, 0` which probably clears the screen.
In GaoGao3 wakuwaku, there's `2A: 1000, 0, 0`, probably the same...
Elsewhere there's `2A: 2400, 24, 0` before jumping to wf_moji...

`002B aaaa`

Save variables in file, aaaa points to null-terminated filename?

`002C aaaa`

Load variables from file?

`002D vvvv wwww`

Zero out all variables from vvvv to wwww, inclusive.

`002F ssss xxxx yyyy wwww hhhh dddd nnnn zzzz`

Blit graphics from source buffer rect to destination buffer rect.
In practice this is mostly used to clear sprites or backgrounds.
Ssss is the source buffer. 0 - background, 2 - sprites
Xxxx*8,yyyy are the top left corner coordinates within the source buffer.
Wwww*8,hhhh are the width and height. For example 0x3C,0x130 = 480,304
Dddd is the destination buffer.
Nnnn,zzzz look like x,y offset for the copy destination. Eg. used for map highlights.

2F-00-0E-08-3C-130-02-00-00 : clear existing bkg and overlay, black background
2F-01-00-00-50-190-02-00-00 : something in a 640x400 context, viewframe itself?
2F-01-0C-158-04-10-00-14-48 : remove outdoors "Mansion" map highlight (src 96,344)
2F-01-25-130-04-10-00-14-48 : draw outdoors "Mansion" map highlight (src 296,304)
2F-02-00-00-3C-130-01-00-00 : remove all sprites in viewframe?

`0032 bbbb vvvv`

Print variable vvvv into box index bbbb.

`0033 vvvv`

Unknown. Interspersed with repeated 0007 fade from black commands?
Write to port 0xA4? According to PC98 docs, that's related to color plane masking?

`0034 vvvv`

Unknown. Read from port 0xA4 into variable vvvv?

`0035 xxxx yyyy wwww hhhh zzzz aaaa`

Unknown, something with print output?

`0036`

Unknown.

`0037 aaaa`

Load a file?..

`0038 vvvv wwww`

Bitwise AND. `variable vvvv &= variable wwww`

`0039 vvvv wwww`

Bitwise OR. `variable vvvv |= variable wwww`

`003A ssss sxsx sysy wwww hhhh dddd dxdx dydy`

Blit stuff from one buffer to another. It's used a lot in GaoGao3. All parameters are
variable references.

Ssss is likely the source buffer, dddd the destination buffer. Buffer 1 likely contains
a loaded graphic as a source, buffer 0 is the screen buffer.

Sxsx,sysy are probably the blit source coordinates, and dxdx,dydy the destination.
X is in 8px columns, Y is in pixels.

Wwww is blit rectangle width in 8px columns. Hhhh is probably the height in pixels.

`003B 8*xxxx`

Unknown, must be another blitting command.

`003C xxxx yyyy wwww hhhh zzzz`

Unknown. Text clearing?

`003D vvvv wwww`

Multiply. `variable vvvv *= variable wwww`

`003E vvvv wwww`

Variable vvvv is divided and moduloed by var wwww. The div result goes in vvvv,
the modulo result goes in wwww.

`003F vvvv wwww`

Unknown...

`0040 vvvv wwww aaaa bbbb`

Get user choice. The choice index (1-based) goes in var vvvv; var wwww becomes 0;
jump immediately to bbbb.

In Nocturn, the result variable is always 0x10, except one instance in mug_sub, for
the clickable map floor selector.

If the user cancelled (right-click), var vvvv becomes 0, var wwww becomes 1.

It's unclear what the stuff at aaaa is, but that's always at the end of the file,
and is not executable. Choice rects coordinate data?

`0041 vvvv wwww zzzz aaaa`

Poll cursor location (waiting for imagemap choice). This places the current mouse
X,Y cursor coordinates into variables vvvv and wwww.

User action is placed in variable zzzz; probably 0 or 2 for left and right-click,
1 or 3 for pressing ESC or otherwise cancelling.

If the user has clicked or cancelled, jump to aaaa, otherwise continue execution
normally.

`0042 0001`

Unknown, sleep a time unit, show error, waitkey? These are found in infinite loops.

`0043 vvvv wwww`

Something with variables vvvv and wwww.

`004A 0000 aaaa`

Something with null-terminated filename at aaaa (memory.dat). Save all variables?

`004B 0000 aaaa`

Something with null-terminated filename at aaaa (memory.dat). Load all variables?

`004C vvvv wwww`

Unknown.

`004D vvvv`

Get music playback position into var vvvv?

`004E xxxx`

Play sound effect, SSG?

`004F xxxx` (PC98)
`004F 0000 0000` (Windows)

Play sound effect, FM?

`0050 vvvv`

Read music state into variable vvvv?..

`0051 vvvv xxxx`

Bitwise AND. `variable vvvv &= xxxx`
(These are signed int16's, so remember to sign-extend afterward if necessary!)

`0052 vvvv xxxx`

Bitwise OR. `variable vvvv |= xxxx`

`0053 ssss zzzz`

Copy string?? String zzzz := string ssss. Or concatenate?

`0054 xxxx`

Call subroutine xxxx; may_sub is loaded at 0x4000, so xxxx 4100..415C are calls
to the initial jumpoff points at the start of may_sub. Any xxxx addresses within
the current script work as normal returnable calls, mainly in open.s.

`0054 4100`

Transition in graphics.

Mayclub: blinds transition, top-down.
Preceded by v221=0, v222=0; if v222=1, show VR stamp.

Nocturn: dissolve transition.

`0054 4104`

Transition into blackout; preceded by v221=0, v222=0; if v222=1, show VR stamp.
Mayclub: blinds transition, bottom up
Nocturn: dissolve transition

`0054 4108`

Draw background.

Mayclub, Nocturn, Lilith: preceded by v220 = background index.

`0054 4110`

Remove sprite, preceded by v221=0, v222=0.
Mayclub: interlaced transition
Nocturn: dissolve transition

`0054 4114`

Transition from black with fullscreen graphics, after a draw command.
Mayclub: interlaced transition
Nocturn: dissolve transition (just use a crossfade)

`0054 4118`

Transition to black with fullscreen graphics.
Mayclub: interlaced transition
Nocturn: crossfade

`0054 411C`

Lilith: Draw an Mxx graphic indicated by v220. It's relative to the viewport, drawn at
offset v221,v222 where v221 is in 8px columns, v222 is in pixel rows.

`0054 4120`

Stop previous song, begin new song; preceded by v220=index.

`0054 4128`

Summon save/load/restart/quit metamenu.

`0054 412C`

Restore most recent background with a blinds top-down transition.

`0054 4130`

Always right before calling a text sequence, prep textbox?

`0054 4134`

Always right after returning from a text sequence, unprep textbox?

`0054 4138`

Clear all local variables.

`0054 413C`

Usually accompanies a "slap", flash viewport white?
Nocturn: horizontal shudder, 100ms per frame, duration 5 * 2 frames.

`0054 4140`

At start of special events, hide viewframe and enter fullscreen mode?

`0054 4144`

Mayclub: If 0204 != 1, draws the viewframe; always updates frame info graphics.
- Depending on $v128 and $v127, selects the month/day digits.
- Depending on $v129, selects one of four time of day images.
- Depending on $v12B, selects one of seven weekday images.

Nocturn: If 0204 == 0, clear the drama box; else fade everything to black, then
draw the viewframe and fade it in, with viewport contents cleared.

`0054 4148`

Draw sprite.

Mayclub: preceded by 0220=A, 0221=0, 0222=1.

The tens digit of $v220 selects the person, the ones digit the expression.
The sprite is immediately transitioned in, replacing any previous sprite.
The currently visible sprite index is saved in $v20C.

If $v221 is 2, print 16 ellipses as game text, during the transition.

If $v222 is 0, the transition rendering may be different or skipped?

Nocturn: $v20C is previous graphic ID, $v10 is the new one.

`0054 414C`

Always after a fullscreen draw command, but only during normal gameplay,
not when viewing omake. Draws the fullscreen-mode textbox with emptied content.

`0054 4150`

Nocturn: Restore viewframed mode if in fullscreen mode. Present the clickable
map, return user choice.

`0054 415C`

Mayclub: Update frame info graphic for remaining tickets, which is in $v126.

`0055`

Return from subroutine call.

`0056 ssss zzzz`

Compare two string variables. If string $ssss is equal to string $zzzz, set $cmp to 0,
otherwise to a non-zero. It probably uses a bytewise subtraction for each byte pair and
returns the first non-zero difference or 0 if both bytes are nulls. A conditional jump
commands likely follows this immediately.

`0057 ssss zzzz xxxx`

String copy?..

`0058 ssss zzzz xxxx`

String copy?..

`0059 ssss aaaa`

Concatenate the null-terminated string at aaaa to string variable ssss.

`005A ssss`

Clear string variable ssss.

`005B ssss zzzz`

Set string variable ssss to the same as string variable zzzz.

`005E bbbb xxxx yyyy wwww hhhh`

Zero out rect in buffer bbbb, top left coords xxxx*8,yyyy, size wwww*8 by hhhh pixels.
Typically clears out the whole 640 by 400 graphics buffer, ie. full screen clear.

`005F ssss`

Time-related string? Set to current timestamp? Used for savegame timestamps?

`0060 vvvv ssss aaaa`

Try to open a save from the external file named at aaaa. Just read the save
header, don't actually apply any changes. If it is a valid save file, set the
variable vvvv to 1, and string variable ssss to the save's timestamp.
If the save is invalid, set vvvv to 0 and leave ssss unchanged.

`0061 ssss`

Save file?..

`0062 vvvv wwww`

Move background, bash; var vvvv has the amount of vertical displacement.
Var wwww is something else...
This is used for a bash effect in Nocturn and Mayclub. Vvvv value is +/- 1, wwww is 0.
The back and forth shaking is done by shifting by +/- 1 in a loop.

This is used to move a graphic upward in Gao Gao 3's chapter titles in a single slide.
In that case vvvv is 0, while wwww is 300.

`0063 <8xuint16>`

Special drawing command, blits a rect from source buffer to dest buffer.
The Mayclub VR stamp in the image PRS is instructive. It is 56x36 pixels,
or 0x7*8 by 0x24 in hex. This matches the 4th and 5th words; that's the blit rect size.
The stamp is located at 0,288 in the image, or 0,0x120. The 3rd word is the source
Y coordinate. The stamp is drawn at 416,242 relative to the viewframe, which is
0x34*8,0xF2. That's the 7th and 8th words. Since we don't have a source X coord,
that's probably the 2nd word, but as a variable reference; makes it easier to
select a digit from the horizontal number frames. The remaining 1st and 6th words
are possibly buffer selectors...
- `0063 0001 0035 0120 0007 0028 0001 0034 00F2` - draw VR stamp
- `0063 0001 0045 0070 0002 0120 0001 0000 0000` - draw something entirely different
- `0063 0001 0047 0070 0001 0120 0001 001C 0000`
- `0063 0001 0047 0070 0001 0120 0001 001F 0000`
- `0063 0001 0048 0070 0002 0120 0001 003A 0000`

`0064 <8xuint16>`

Unknown.

`0067 xxxx yyyy zzzz aaaa`

Unknown.

`0068 vvvv wwww`

Unknown.

`0069 vvvv wwww xxxx`

Unknown.

`006A 0040 0140 0041 0167`

Unknown, g15.s, also various lilith scripts. Select choice box height or content size?

`006B vvvv wwww xxxx yyyy`

Unknown.

`006E vvvv ssss`

String length calculator? Used in Nocturn open.s after name entry, to pad the string.

`006F vvvv wwww`

Var vvvv := var wwww?

`0070 vvvv wwww`

Var vvvv := var wwww?

`0071 vvvv wwww`

Compare variables?...

`0072 xxxx`

Init loop counter to xxxx?

`0073 vvvv`

Seen in g15.s. Get loop counter into variable vvvv?

`0074 xxxx aaaa`

Go to offset aaaa if loop counter == xxxx? Seen in Nocturn open.s.

`0075 xxxx aaaa`

Unknown, in start.s, xxxx is an ascending series. Custom font from aaaa?

`0076 8*ref`

Unknown, in start.s; acts on the ascending series from 0075 just before.
Waitkey animation?

`0077 8*0004`

Unknown, in start.s. Waitkey anim frame times?

`0078 FFFE FFFF`

Unknown, in start.s. Wait key anim position? All over in Gao Gao 3 before text prints.

`0079 xxxx`

Unknown.

`007A vvvv`

Set loop counter from variable vvvv?

`007B vvvv aaaa`

Go to aaaa when loop counter is <=> var vvvv? This appears to be used typically for
yielding 1 or more frames during a visual effect, and the jump loops back to this
command until enough frames have passed. Perhaps ignore the jump and translate this
as a brief sleep, 20ms multiplied by the variable vvvv. However, since it counts frames,
some scripts keep increasing vvvv's value as the effect runs to match the running
frame counter. So reset variable vvvv to 0 at each rest.
Nocturn uses this for the dramatic interstitials, between every word printed.

`007F vvvv`

Stash value of variable vvvv as a configuration value. Used in Gao Gao 3 for the music
device choice (none, mono FM, stereo FM, GS midi).

`0080 ssss aaaa`

Prompt user to enter a string. Description is the null-terminated string at aaaa.
The string is saved in string variable ssss. And maybe ssss+1?
Or load strings from file whose name is at aaaa?

`0081 ssss aaaa`

Unknown. Some other string entry? Write to file?

`0082`

Unknown.

`0083 vvvv`

Unknown.

`0084`

Unknown.

`0085`

Unknown.

`0086 aaaa`

Unknown.

`0087 10*xxxx`

Unknown.

`0088 zzzz xxxx yyyy`

Unknown.

`0089 aaaa`

Unknown. Something with portraits.

`008A vvvv wwww xxxx yyyy`

Unknown.

`008B vvvv xxxx yyyy`

Unknown.

`008C`

Unknown.

`008D`

Unknown.

`008E`

Unknown.

`008F 0001 aaaa`

At start of start.s, load null-terminated filename aaaa (may_sub.s).

`0090`

Unknown, associated with returning to game's main menu.

`0091 vvvv xxxx`

Stash value of variable vvvv as system config xxxx? Used in Lilith to store sound device,
it is constantly available as $v400. Nocturn does something similar...

`0092`

Unknown, in Nocturn open.s.

`0093 vvvv`

Unknown, something var vvvv.

`0094`

Unknown, in Nocturn98 1_24.s.

`0095 bbbb cccc wwww hhhh xxxx yyyy zzzz nnnn`

Dissolve transition effect? Bbbb or cccc are unknown coords, not the active buffer.
Wwww*8 by hhhh is normally 480x304, the viewframe contents. Xxxx,yyyy are its coords.
The last two items might be transition type, or duration?..

Found in some Nocturn mug_sub calls. Preceded by 002F screen clearing...

`0096 xxxx 0008`

Unknown, xxxx is 0 or 1, start.s.

`0097`

Trigger ending credits? Prior to the Mayclub credit sequence, there's a final
game text: `"VR dating "May Club" ending #n`, which is printed normally in the
English port, but there's no obvious direct reference to the string in the
Japanese version, even though it does get printed.

Mayclub: Variable 0x150 holds the ending number when the credits are called.
This is 0-based, ranging from 0 to 8.

`00FD xxxx`

Unknown, in open.s; something to do with omake.

`00FE xxxx`

Unknown, in open.s, something to do with omake.


##### Multiple choice selection

Mayclub: the sequence is `0003, 0022-03E8, 0015, 0022-0120, 0040`.

- 0003 sets up the main text box for choice printing.
- 0022 03E8 marks the start of choice definitions.
- 0015 is the normal print command, but now produces newline-separated choices.
- 0022 0120 marks the end of choice definitions.
- 0040 lets the user select one of the choices, saves the 1-based value in a variable.

Example:
```
0003 0003 0003 0013 0023 0006 0000 - set up for choice printing
0022 03E8 - begin choices
0015 0003 06B7 - print sequence at 06B7: "<03><03>Pretty!<0D>Meh.<0D>Lol!<03><01><00>"
0022 0120 - end choices
0040 0010 0011 453C 006A - get user choice in variable 0010, jump to 006A
```

Using 0022 03E8 as a choice.reset point normally works well, but for example the
omake music player (open.s around 0x19D2..0x1C40) does this instead:
`0022-03E8, 0015, 0003, 0022-03E8, 0022-0120, 0040, 0022-0120`. The prints are
placed between two 0022-03E8's...

Nocturn:

```
0022 03E8
0015 000C 067A - sequence: "<0F><00><05> Look<0D> Check<0D> Talk<0D> Think<0D> Move<00>"
0022 0120
003F 0100 0108
0040 0010 0011 1C7D 00FF
```

##### Special commands while printing text

In the English ports, the text is plain ASCII. In the Japanese versions, Shift-JIS.
Game text includes occasional special command bytes.

`00 (or FF?)` - End text sequence, return to script execution

`01` - Wait for a keypress, clear textbox

`02` - Wait for a keypress, don't clear textbox

`03 <cc>` - Change text color to cc. 3=blue for choices, 1=normal black.

`04 <xx>` - Print the contents of string variable xx, typically a character name.

`05 <xx><yy>` - Print the contents of number variable yyxx.

`06 <xx>` - Draw dialogue portrait xx.

`07` - Clear dialogue portrait, if visible.

`09` - Wait a moment.

`0B <xx>` - Same as 06, draw portrait, but also applies the palette?

`0C <xx>` - Same as 06, draw portrait?

`0D` - Newline, or choice separator.

`0F ...` - Unknown, feels like this has more than one possible length.
`0F 00 05` - Unknown. Nocturn 0_00 and all over Mayclub.
`0F 00 06` - Unknown. Nocturn mug_sub.
`0F 01` - Nocturn a_00 etc. A pair always at non-interstitial dramatic text sections.
`0F 02` - Unknown. Nocturn mug_sub.
`0F 03` - Unknown. Seen in all Mayclub ?e? scripts, but also elsewhere.
`0F 0A` - Unknown. Nocturn 1_29.
`0F 0B` - Unknown. Nocturn 1_29.

`1B <xx>` - Reference to string variable xx, to be resolved upon printing. Same as 04!

Game text may also include emotes, encoded as invalid 85-xx and EB-xx code points.
These are mostly identical in all System98 games. The English ports stripped out some
emotes from the script; the PC98 originals use a wider range.
(these EBxx values actually have JIS X 0213 definitions, but are custom emoticons here)

Usage abbreviations:
A - Angel Night, W - GaoGao 3, C - GaoGao 4, L - Lilith, M - Mayclub, N - Nocturn

| Code  | Used in | Description                                     |
|:------|:-------:|:------------------------------------------------|
| 85 4F |         | 0 or yen sign
| EB 9F | M N     | two sweat drops, arcing down
| EB A0 | M N     | large sweat drop
| EB A1 | M N     | shiny star
| EB A2 |         | normal/serious face? could be a smirk
| EB A3 | M N     | happy face `(^o^)`
| EB A4 | M       | face with tears streaming from eyes, wailing mouth `(ToT)`
| EB A5 | M N     | angry cross-popping veins
| EB A6 | M       | angry slanted beast eyes with cross-popping vein
| EB A7 | M       | two concentric circles, outer ringed with half-circles; sunflower
| EB A8 | M       | dark parted lips with some "mwah" emanation lines
| EB A9 | M N     | big slightly embossed heart
| EB AA | M       | large heart over a smaller one, both flitting toward the right
| EB AB | M N     | shiny heart
| EB AC | M       | bowling ball face, big empty eyes and big empty mouth
| EB AD | M       | shiny middle finger
| EB AE | M N     | shiny thumb up
| EB AF | M N     | two fingers up, peace sign
| EB B0 |         | munch's "scream" face
| EB B1 | M N     | slanted eighth note (hockey stick with a single pennant from top)
| EB B2 |         | straight quarter note
| EB B3 | M N     | beamed pair of sixteenth notes (double beam, right blob lower)
| EB B4 |         | double-exclamation mark
| EB B5 | M N     | single extra-wide exclamation mark
| EB B6 |         | exclamation mark and question mark pair
| EB B7 | M       | squinted eyes?
| EB B8 | M N     | double-exclamation mark in a jagged speech bubble
| EB B9 | M       | bright idea light bulb
| EB BA | M N     | a gloomy skull or smoke clouds
| EB BB |         | paw print
| EB BC |         | single eye with small lashes, black iris, white pupil
| EB BD |         | three Z letters rising toward the top right
| EB BE |         | looks like a beaver
| EB BF |         | cat eye?
| EB C0 |         | nyan cat face
| EB C1 | M N     | trickle of sweat
| EB C2 | M N     | two sweat drops, arcing up
| EB C3 | M N     | five sweat beads
| EB C4 | M N     | huff cloud
| EB C5 |         | nothing
|EBC6-CD|         | animation frames for waitkey
|EBCE-D5|         | rectangle from bottom of glyph at different heights
|EBD6-D7|         | single thin lines at different heights
| EB D8 |         | two dark flames
| EB D9 |         | single dark flame
| EB DA |         | eight-pointed shiny star, slightly irregular, indicating a hit
| EB DB | M       | same eight-pointed shiny star but filled with black
| EB DC | M       | awkward grinning face with small sweatdrop `(^_^')`
| EB DD |         | tiny katakana "hihihi"
| EB DE | M       | tiny katakana "u hu" and a small heart
| EB DF | M       | face with mild disbelief, trickle of sweat `(','!)`
| EB E0 | M       | tilde and a quarter note, indicating a happy or innocent hum or whistle
| EB E1 | M N     | medium circle with three shine lines, sun?
| EB E2 | M N     | small circle with three shine lines
| EB E3 |         | dizzy face, with spiral eyes and wavy mouth `@_@`
| EB E4 | M       | large hiragana A with a dakuten, indicating anguished "agh"

The emotes are in the `fnt` files for the PC98 games, but it's not clear where the
Windows ports keep them. They're not at the end of the executable, that's just the
cursor and program icon.

Possible hex pattern to look for, shifted right by one bit, and shifted left by one bit:

	........ 1..1....	00 90	00 48	01 20
	.....111 ...1....	07 10	03 88	0E 20
	........ ....111.	00 0E	00 07	00 1C
	........ ........	00 00	00 00	00 00
	....111. ........	0E 00	07 00	1C 00
	.......1 ...111..	01 1C	00 8E	03 38

If it's stored as 16-pixel 1-bit scanlines, some of those byte pairs should be found.
But could not find anything matching this in the muge Windows exe... Also no luck
looking for just one byte column, or in LSB/MSB reverse order.

Mayclub98: There's an illegal "85 4F" in opmes.s, which the game prints as an extra
half-width zero-digit. This is probably a mistake, since it makes the money amounts
being discussed unrealistically large. The protag starts with 15000 or 150000 yen,
which buys him 15 tickets (plus 5 freebies). Fine, so far, but when the month changes,
protag gets money from his part-time work. The original Japanese doesn't appear to
say how much, but you can buy 30 tickets with it, so it's either 30000 or 300000 yen.
One month of part-time work does not yield a net pay of 300 thousand yen. Therefore
the "85 4F" should probably be a yen (89 7F) or other currency sign.


##### Dialogue style

Mayclub98:
```
$s0B := "[Protagonist]"
<04><0B><0D>"Yo!"<01>[Rando]<0D>"Urusee."<01>
```

Mayclub_e:
```
[ Protagonist ]<0D>"Hello, there!"<01>[ Rando ]<0D>"Get lost."<01>
```

Nocturn98:
```
$s0A := "  Protagonist  :"
$s0B := "   Mistress    :"
<04><0B>"So, the protagonist wakes up, at last."<0D><04><0A>"Oh no, not again."<01>
```

Nocturn_e:
```
$s0A := " Protagonist "
$s0B := " Mistress :"
<04><0B>"So, the protagonist wakes up, at last."<0D><04><0A>"Oh no, not again."<01>
 Miwuwu  : "... scary!"<01>
```


##### Nocturne clickable map

The map graphic is `mitori`. This contains multiple irregular frames, with individual
highlighted room rectangles.

MITORI regions:
```
0,0 - 264,136: floor 1 base
0,136 - 232,272: floor 2 base
0,272 - 232,400: floor 0 base
329,352 - 379,375: textbox base

264,0 - 322,49: floor 1 top left Mistress's room
264,51 - 322,78: floor 1 mid left
264,81 - 322,104: floor 1 bottom left
354,112 - 360,136: floor 1 Basement
328,0 - 376,40: floor 1 center Dining room
328,112 - 352,136: floor 1 top middle Kitchen
328,43 - 376,69: floor 1 bottom middle Lobby
302,160 - 384,208: floor 1 top right Bathroom
270,104 - 323,133: floor 1 mid right
302,136 - 355,158: floor 1 bottom right Maya's room

238,136 - 290,158: floor 2 top left Kusayama's room
238,161 - 290,184: floor 2 second left
238,187 - 290,212: floor 2 third left
238,217 - 290,237: floor 2 bottom left Misao's room
334,72 - 372,89: floor 2 bottom middle Down stairway
232,345 - 280,384: floor 2 center Study room
238,240 - 290,259: floor 2 top right Mystery room
238,265 - 290,288: floor 2 second right
238,291 - 290,316: floor 2 third right Yukina's room
238,321 - 290,341: floor 2 bottom right My room
343,261 - 351,279: floor 2 Up stairway

296,212 - 360,253: floor 0 bottom right Terrace
302,258 - 335,302: floor 0 top left Storehouse
296,307 - 324,320: floor 0 center Mansion
302,325 - 351,350: floor 0 bottom middle Gate
281,352 - 322,380: floor 0 top right Well
236,384 - 304,396: floor 0 top middle Backyard
```

The lobby stairs have special handling. If you select the lobby stairs from the upper
floor, or the mansion entrance from outside, you always go immediately to the ground
floor map. If you select the lobby on the ground floor, you always get a popup choice
for whether to move immediately to the outdoor or upper floor map, or the lobby itself.
This map floor changing is possible even when there's a special event that overrides
your requested destination, eg. meeting Maya when you first leave your room, no matter
where you tried to go.

Relevant variables:
- $v123 is a flag for blocking movement?
- $v124 indicates which location we're in?
- $v126 location description index, changes when map is mouseovered
- $v127 indicates which floor we're on: 0 = outside, 1 = ground floor, 2 = upper floor
- $v128 tracks story progression
- $v204 possibly 1 = fullscreen scene, 0 = viewframed scene

The subprocedure call 0054-4150 presents the map graphic and returns the user choice
in $v124. The logic for selecting which script to jump to based on that value is in
the `ss*` scripts, which get jumped to after the newmap.

The user map choice is taken through command 0041, at mug_sub @6BC4.
This is jumped to from 7744 and 77B6, which I guess are the floor change options.
It is fallen into from 6B86.
That is fallen into from 6B74, and jumped into from 6AC4.
6AC8..6B74 is probably the screen-darkening effect.

Get the location string into $s09:
`$v126 := description index; casecall $v127 "6CEE:6F60:731E"`

Get the location index into $v124, and some small value in $v12B:
`$v127 := floor; $v126 := location index; call 7546`
Or should it call 753A? Under some conditions ($v00 in 0,2) acts differently.

@753A is called onclick during the map.
For a positive movement, $v00 is 0 or 2, for cancelling, 1 or 3.
Positive movement forwards to 7546 for floor selection.

Negative hops to 77FC, which restores location to what it was before, etc.
$v129 := $v30
$v12A := $v31
$v127 := $v12D
$v126 := $v12E
$v8E := 255
if $v204 != 1 then call ."46A2" // sets an unusual palette
return
