Great SD bytecode
-----------------

There are 128 variables, referenced by index, probably all global.
The possible range of values seems to be 0-127 only, but it seems like
all variables are initialised to 1 at game start. (v32 is never explicitly
initialised, and only has two references in the open script, one of which
is a casegoto with two options, and the other sets v32 to 2. Therefore it
must have been 1 at script start.)

Conditionals work a little unusually in that there is a persistent
boolean conditional (BC) flag, which stores the result of the latest
comparison operation. A subsequent conditional jump can then refer
to the flag to decide whether to jump. Each conditional check also flips
the flag after checking, so a pair of conditionals acts as a then-else.
The flag starts out true.
(Or should the flag always be set to true after a failed conditional check,
rather than negated?)

`01 string 00 xxxx yyyy aa`

Show named image at given coordinates. The coordinates are a direct screen
location, so most graphics are offset by the viewframe: X + 32, Y + 16.
The image palette is applied immediately.
It's unclear what the final aa byte does, but it's always 0 or 1.
Display immediately, transition basically instant.

`02 nn vv ww`

Enter dungeon nn! This is 1-based, refers to image FORM0n. However, FORM03 does
not exist, even though for07 enters it?..
Return values go in variables vv and ww.
Dungeons are all a 5x5 tile map, where tile indexes are counted from the
top left, in scanline order. So 0 is top left tile, 24 is bottom right tile.

`03 nn location_bytes 00 vv`

Show map number nn. This is 1-based, refers to image FORT0n. Except 8 is the
image MAP, the world map, but it functions the same way.
Array of bytes has the index numbers of all currently open locations. Ends with 00.
Final vv is the output variable for the user's selection.
The map command handles the graphic drawing, location tagging, and selection
as a hardcoded routine, and only returns the user's selected destination index.
The map can't be cancelled out of; the user must select a valid destination.
After drawing the map image, each open location has a label printed at its
hardcoded location. The user can mouseover them, which highlights the text tag.
Click to select.

TMAP.DAT:
```
uint16 - map index, 1-based
char[19] x 8 - location tag text, null-terminated Shift-JIS strings
(uint8, uint8) x 8 - coordinate pairs for location tags, multiply x by 8
byte[98] - unknown
```

Example unknown data from first and second:
```
01 01 01 01 01 01 01 01
01 01 01 01 01 01 01 01
01 01 01 01 01 01 01 01
01 01 01 01 01 01 01 01
01 01 01 01 01 01 01 01
01 00
01 01 01 01 01 00
01 01 01 01 01 00
01 01 01 01 01 00
05 00 0A 00 32 00 64 00
F4 01 64 00 C8 00 C8 00
2C 01 00 00 D0 07 D0 07
D0 07 D0 07 00 00 00 00
00 00 00 00 00 00

00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00
00 00
00 00 00 00 00 00
00 00 00 00 00 00
00 00 00 00 00 00
05 00 0A 00 32 00 00 00
00 00 64 00 00 00 00 00
00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00
00 00 00 00 00 00
```

`04 0B vv`

Bring up equipment menu for character indicated by variable vv.

`04 10 xx`

Bring up a save/load menu?

`04 xx nouns 00`

Define a multiple choice menu. xx is the main level, either 00 for root
or a larger number for a specific verb. This is followed by an array of
bytes, each indicating a child noun under xx, or 00 to end.

The menu is not yet brought up by this command, only defined.

The choice verbs are hardcoded in FRMAIN.EXE from 0x19B16 onward, 24 null-terminated
strings, except the last ends with a $75.

```
0: root level
1: Look/Examine
2: Talk/Ask
3: Take/Touch
4: Think
5: Use/Items
6: Move
7: Data
8: System
9: Character stats
0A: List
0B: Special - equipment menu
0C: Party stats (Battle parameters)
0D: Equipment
0E: Items
0F: Message
10: Save
11: Load
12: Decision
13: Equip nothing
14: End (exit?)
15: Re-order party
16: Buy
17: Sell
```

`05 xx [vv]`

Show special dialog xx.
- 01 show quick party status in the multiple choice box
- 02 character status for character indicated by variable vv
- 03 party status/combat params window
- 04 equipment window
- 05 inventory

`06`

Wait for a keypress.

`07 xx enemy_data yy zz`

Begin combat. xx = number of enemy stacks. Enemy_data is an array of byte pairs,
one pair for each stack. Within the pair, the first byte is the monster ID index,
the second the number of monsters in the stack. Unknown what yy and zz are...

`08 aa bb cc`

Random enemy encounter? 08 03 01 3C gave 3xmon04 + 2xmon04, then without letup
a further 3xmon02.

`09`

Unknown.

`0A aa 01 xxxx yyyy wwww hhhh 00`

Define a multiple choice highlight rectangle. Choice index is aa.
The size is always 144x16, X coordinate 456.
Defining every rectangle like this in bytecode seems a little unnecessary,
since we only ever deal with single-column vertical menus in the game.

`0B 01 00 00 00 00 vv`

Activates the multiple choice menu, blocks until the user makes a choice.
Variable vv is set to the selected choice index (1-based), or 0x7F if cancelled.

`0C tt`

Fade in from black, duration tt/2 seconds.

`0D tt xx`

Fade out to black, duration tt/2 seconds. Uncertain what xx does.
Also fades out game text!
0D 00 01 instantly blacks out the palette.
0D 05 00 fades to black over 2 seconds.

`0E tt`

Fade in from white, duration tt/2 seconds.

`0F tt xx`

If xx = 0, fade out to white, duration tt/2 seconds.
If xx = 0E, this was probably preceded by an 0A highlight rect definition.
In that case, this is some menu row clear or highlight thing, not fade to white.

`10 aaaa`

Call absolute address aaaa in this script, can be returned from.

`11 aaaa`

Conditional jump to aaaa in this script. Jump if the BC flag is true.
Else negate the BC flag and continue execution.

`12 vv op nn xx`

Comparison expression, where vv is the variable, nn a numeric value,
and op the comparison operator. Finally, xx indicates if this should be
AND'd or OR'd with another expression, in which case a further `vv op nn xx`
follows immediately. The final true/false result goes into the BC flag.
Note that all bytes have the high bit set. This must be cleared for vv and nn.

Operators:
- 0x92: `!=`
- 0x93: `<=`
- 0x94: `>=`
- 0x95: `<`
- 0x96: `>`
- 0x97: `==`

Further actions:
- 0x80: AND
- 0x81: OR
- 0x82: end expression

Example: `12, 85 92 C4 81, 86 97 80 82` means `($v05 != 0x44) || ($v06 == 0x00)`

`17 xx`

Play song xx, or stop music if xx = 0.

`1A xx vv`

Unknown, vv is a variable reference. Found in scripts for12 and for13.

`1B xxxx yyyy wwww hhhh cc`

Low-level function to fill a rectangle on screen with color index cc.
Normally used with ofs 32,16 size 400x250 to black out the game viewport.
There's one instance of filling at 448,16, which colors in a multiple choice
menu box, ignorable if menu re-implemented.

`1E tt`

Sleep for tt*20 milliseconds.

`1F`

Unknown. Clear screen? Wipe target palette except set 15 as white?

`21 vv xx`

Set variable vv to value xx. There are a few places where xx is > 128, which
must mean something different. Possibly, set variable vv := variable (xx & 0x7F)?
For01: $v49 := 199 (would be $v71, not used anywhere else?)
For11: $v55 := 177 (would be $v49)

`22 vv xx`

Probably add value xx to variable vv.

`23 vv xx`

Probably subtract value xx from variable vv.

`24 vv nn addresses`

Case jump to one of given absolute addresses in this script.
Addresses is nn uint16 values. Select the one indicated by variable vv,
which is 1-based.

`26`

Return from call.

`28 ...`

Extra commands, as follows:

- `28 07`: restore full party HP instantly
- `28 08 xx vv`: load game
- `28 0D`: unknown
- `28 0E`: message speed selection?
- `28 0F`: weapon store
- `28 10`: general store
- `28 11 string 00`: jump to the start of the named script file
- `28 12`: unknown
- `28 13 xx`: add party member xx
- `28 14 xx`: delete party member xx (1 = Bayse, 2 = Miria, 3 = ?)
- `28 15 xxxx`: add money
- `28 16 xxxx`: add XP
- `28 17`: party member order selection
- `28 18 xx yy`: request to insert disk yy in drive xx? xx always 0, first drive

`29 aaaa`

Jump to absolute address aaaa in this script, not returnable.

`2C`

Unknown.

`2E xx yy`

Set the printing cursor at xx*8, yy*2. Both special text and choice items
are located using this.

`2F text 00`

Print text at the current cursor position. This is nearly always used
for printing choice items only. Since you know where the cursor was
set by a previous command, you can calculate which choice index is being
printed here. The topmost choice begins at Y 52, and each choice is 16 tall.

`30 nn ...`

Print nn rows of game text. If nn = 0, clear the game box. Otherwise, nn null-terminated
strings follow. If there are more than 6 rows, the engine implicitly pauses after each
6th row since that's the most the game text box can fit.

The text is Shift-JIS, with each string ending with an implicit linebreak. There is
a special code, 5C 24, which should be printed as the custom player name.

`31`

Shows the "more text" arrow?..

`32 xx yy zz`

Unknown.

`34 vv`

Shows the party member selection dialog, the player must pick one. The selection is
saved in variable vv, and will be 0, 1, or 2; or 0x7F if cancelled.

`36`

Exit, end script.
