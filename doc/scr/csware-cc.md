CC and CMD bytecode
-------------------

Used by C's Ware for PC98 games.

Kinketsu has CMD scripts. These are uncompressed, no header or anything.
CC (compressed command) script files are used in every other PC98 game.
These use standard Softdisk LZSS compression with a 17-byte offset.
After unpacking, they become the same CMD files as Kinketsu uses.


##### CC Header

```
ofs 0: 4C 43 5A - LCZ
ofs 20: dword uncompressed data size
ofs 24: start of compressed bytecode
```


##### CMD and uncompressed CC bytecode

`01 FB xxxx`

Begin big script section, clear choice possibilities.

`02`

End big script section, stop and show choices if got them.

`04`

Begin fresh choice list block.

`05`

End choice list block, immediate choice.go.

`06 FB xxxx`

Jump to start of big script section xx.

`07 FD len string 00`

Go to start of named script.

`08`

End choice outcome block, resume previous choice outcome block; immediate choice.go.

`09`

End game, return to main menu?

`0A`

Begin if-then block.

`0B`

End if-then block.

`0C FD len string 00`

Load the specified graphic, but don't show it yet. This doesn't support transparent images.
All loaded graphics are drawn at the next transition command: `0D`, `11`, `1E`, or `24`.
There may be other timed visual effects between the load and transition.
May be animated, in which case this will immediately or eventually be followed by `2E`,
which defines the animation parameters and starts showing the animation immediately
without waiting for a transition. The animation's display position is part of the
metadata in the image file.

`0D`

Instantly display the current graphic objects, no transition animation.

`0E FD len string 00`

Load named song, don't play it yet.

`0F`

Begin music playback at normal volume.

`10`

Fade out music over 1 second. Same as `31`?

`11 without FB`

Eyeblink transition to black background.

`11 FB xxxx`

If xxxx = 1, interleaved transition to black background.
If xxxx = 0, probably instant transition to black.

`12 FB xx yy`

Generic numeric value parameter, push into variable if not in if-statement.

`13`

Begin if-statement.

`13 FE xxyy 12 FB aabb`

Stash truth value for if var xxyy == aabb.

`14`

If truth value is true, trigger following then-statement; if next code is not 0A,
then then-block is only the next single bytecode, otherwise it's a whole then-block
from 0A to the next 0B code.

`15`

Boolean AND previous truth value with next if-statement.

`16 FD len string 00`

Show named overlay, modifying the existing graphic. There are various multi-stage
graphics, where each new stage is saved like a minimal diff from the previous stage.
This code means the current graphic must be unpacked into the display buffer where
the previous graphic is already present; the decompression algorithm will reference
some pixels from the display buffer to correctly unpack the new image. It may be
easiest to hardcode these image relationships so all images can be unpacked without
relying on this script code command. Reference: _GetBaseGraphic() in Decomp_DA1.

`17`, `18`, `1A`

Maybe other boolean operations?..

`19`

Bring up Save Game options.

`1B`

Begin list of choice strings, maybe just 1B? Clear choices? Push choice level stack?

`1C`

Bring up Load Game options.

`1D FD len string 00`

Request specified game disk in drive 2, range "B" to "F".

`1E FB xxyy`

Unknown, 0001 = interleaved transition to graphic just described?

`1F`

Only at end-like areas, return to main menu.

`20 FB xxyy`

Sleep for xxyy * 17 ms.

`21 FB xxyy`

Fullscreen palette fade to normal over xxyy * 200 ms, typically returning from
a blackout or whiteout.
(in amy/b09 without FB? instant, default duration?)

`22 FB xxyy`

Fullscreen palette fade to black over xxyy * 200 ms.
(in amy/o01 without FB? instant, default duration?)

`23`

Unknown.

`24 without FB`

Eyeblink transition to graphic just described.

`24 FB 00 00`

Dissolve transition to graphic just described.

`24 FB 01 00`

Interleaved transition to graphic just described.

`25`, `27`

Unknown.

`26`

Clear the whole screen? Remove the viewframe too.

`28 FD len string 00`

Seems exactly the same as `0C` - Create a graphic object. But only used with
animated images? `2E` must still follow before other further graphic commands.

`29 FB 0001`

Show Xenon fullscreen planet, rights message, phase in the "XENON" text.

`29 without FB`

Brings up the title card at the end of Desire a01. Fade to black, display "title"
(show the purple text first, then fade in the rest), play "title.m" brief fanfare,
display "title2" too, wait for a keypress.

`2A FD len string 00`

Show named event overlay? Possibly clear all other graphic objects. Always refers
to an object already loaded a little earlier with `0C`?

`2B`

Unknown, always paired with `2D`.

`2D`

Unknown, always paired with `2B`.

`2E [FB xxxx] x 6`

Animation info for the most recently loaded graphic object. There are six word arguments:
- Source offset x and y: use X offset to select the right frame column in Xenon bast_m01
  or bast_m02; in all other cases you can probably ignore these
- Display offset x and y in the game window: use this!
- Frame width and height in pixels

Animation timings appear to be hardcoded somewhere.

`2F`

Stop animations.

`30 FB xxxx`

Fade out and stop music at rate xxxx, that is, bring down the master volume by
xxxx every 250ms or so. Bigger number fades faster. Probably 127 is max volume.
Xenon s0104 has a rate 230 which jumps the volume up and doesn't fade? Decrease by -26?
Desire b160 has this without FB, stop music instantly?
Rate 3 = ~20 seconds, rate 5 = ~8 seconds, rate 20 = ~2-3 seconds?

`31`

Fade out music over 1 second. Same as `10`?

`32 FB xx xx`

Unknown, screen or box offset change? Might be a screen shake effect.

`32 FD len string 00`

Show named graphic. Same as `0C` and `28`?..
This is used in Desire a01 to load the 213a+b animation, after 213 already displayed.

`33 FD len string 00`

Clear all graphics. Create a graphic object as the new background image.

`34 FD len string 00`

Create a graphic object that is a named sprite. This is normally immediately followed
by `35` to transition everything on screen.

`35`

Always after a sprite show command, show sprite with interlaced transition.

`36 FB xxxx FB rrrr FB gggg FB bbbb`

Set color x to 4bpp palette R, G, B; printed text normally uses color 15.

A notable use of this is the whiplash visual at the start of Amy. The image
only uses color 14, which is immediately set to black on load. The whiplash
sound effect then combines with setting palette 14 to white, followed by
`22` to rapidly fade back to black.

Another common use is in conjunction with `39` to select a special text color.
The `39` may appear immediately before or after this.

`37 FB xxxx`

Mark chapter or route xxxx complete.

`38 FB xxxx FB yyyy FB aaaa FB bbbb`

Set text printing location to one of xx*8,yy or aa*8,bb.
Setting the print location does not clear previously printed text, but printing
over existing text replaces the glyphs cleanly.
Multiple print locations can be visible at the same time.

It's not clear to me how you should decide which coordinates to print at, but
generally, if both yy values are >= 310, print in the standard main box.
Otherwise print at the location with the smaller yy value.

The standard font is 16px tall, with 2px space between rows. But the printing grid
isn't fixed? Can start a row from Y offset 20...

Example usages:
- Amy a01: 17 lines of dialogue visible simultaneously, covers most of the game view.
  The print location is moved after every 3 lines. Always use the first pair of coordinates.
- Eve intro: date interstitials appear occasionally, only one at a time
- Eve a001_6, a001_7: computer terminal text printing all over
- Xenon intro: latin text in the middle of the screen, then Japanese text a couple
  lines below that

`39 FB xxxx FB bbbb`

Use palette index xxxx for text color, bbbb for background. The palette may be
assigned with an immediately following or preceding `36`, but it may also just use
the current palette determined by the displayed images.

`3A FB xxxx`

Unknown.

`3B FD len string 00`

Show named sprite, drawing over an existing sprite. Same deal as `16`?

`3C`

Special waitkey? no blinking "more" icon?

`3D`

Unknown.

`3E FB xx 00`

Unknown.

`40`

Unknown, always appears after 38 sets box to normal location, can appear on its own too.
In Desire a01, this seems to trigger the 213 graphic to run its animation 213a + 213b,
and waits for the animation to complete.

`41`

Unknown.

`42 FB xxxx`

Fullscreen whiteout over xxxx * 200ms. (Set palette to all FFF.)
Normally followed by `21` to return to the normal palette.

`43 FB xxxx`

Unknown. Not a flash or bash.

`44 FB xxxx`

Unknown. Not a flash or bash.

`45 FB xx 00`

Sound effect xx - replaces previous sound effect if still playing.

`46`

End looping sound effect.

`47`

Unknown.

`48 FB xxxx`

Probably wait for signal from music player, to synchronise game events with music.
PMD music files have a "Status1" event that's used for this kind of signal.
There's an increasing series amid the credits in Eve c02.
Appears multiple times without FB value in Desire a01 and c02.
Also once at the start of each Xenon title sequence variant.

`49`

Clear permanent story unlock variables in memory.

`4A`

Flush permanent story unlocks to disk.

`4B`

Load permanent story unlock variables from disk.

`4D`

`FA`

Play hardcoded title screen animation.

`FD len string 00`

Print text, Shift-JIS; if in 04 choice list block, expect each FD etc to be
followed by an FF option number - the string attaches to that option index.
If len is FF, ignore it and just print until the null.

`FE xxxx`

Select variable xxxx.

`FF xxxx`

Begin outcome block for option outcome xxxx, push to option stack.


###### Codes in printed text

`03`

Toggle big text, about double-height.

`04 xx`

No waitkey or blinking "more" arrow after printing this string. Instead, if xx >= 0x30,
delay for (xx - 0x30) seconds after printing. For 0x30, that means no delay. If there
was a delay, then automatically clear the textbox after?
This is most notably used in Desire's intro for automatic text progression.

`05`

Instant text printing instead of gradual, maybe?

`0A`

Newline. Can have more than one in a row.

`0C`

Stop printing text, waitkey noclear, continue printing.

If a printed string is a very short `\n `, it's probably a follow-up to an earlier
string printed without a waitkey. In this case, don't bother to print anything,
just output a waitkey.


##### Eve story unlock flags

In a few places on Kojiroh and Marina's routes, progress is gated until the other
route has reached a certain event. Also, a third route becomes available after both
of the other routes have been completed. These are controlled by unlock-related
variables that persist in a global save file.
Command 49 wipes all unlock-related variables in memory.
Command 4A saves the current unlock-related variables to disk.
Command 4B loads them from disk into memory.
When starting a new game or loading a saved one, the unlock values are always
updated to the latest from disk.

- $v2 is set in a010_2, needed in b024_3.
- $v3 is set in b008, needed in a001_4.
- $v4 is set in a005, needed in b005.
- $v5 is set in b022_4, needed in a001_5.
- $v6 is set in a001_6, needed in b022_4.
- $v7 is set in b022_4, needed in a001_6.
- $v8 is set in a001_6, needed in b022_4.
- $v9 is set in b022_4, needed in a001_7.
- $v10 is set in b026, needed in a026.
- $v11 is set in b035, needed in a034.
- $v12 is set in a029, needed in b035.
- $v13 is set in a034, needed in b034.
- $v15 may be set when both routes are clear, unlocking the third route.
- $v254 is set in c02 and ab01... maybe unlocks third route?
- $v255 is set in c02... maybe unlocks game completion?

This is a bit uncomfortable both for the engine and player. It's not clear when
you need to switch characters, so progress can feel frustratingly soft-locked.
Switching is also laborious since you have to exit the story first. Keeping these
extra flags requires special handling in the engine, but that's likely unavoidable.

Ideally, you'd have a button for switching characters during the game, perhaps
wherever there's a Save-Load option it could be enabled. That would need to stash
the current main fiber's label section and jump to the other route. The entry point
would be the route's current section's start. The route selection at game start can
still be used, and is the only way to access the third route.

Information box during character select:

"You will need to switch between Marina and Kojiroh to progress in the story. Use the
right-click menu to switch when in an appropriate safe place, like Kojiroh's office
or Marina's apartment (?)"


##### Eve SFX

	01 thud
	02 telephone loop
	03 miniclatter (receiver or door)
	04 phone keys bip-bip-bip then crackles and calling loop
	05 classic doorbell
	06 ka-boshhhh
	08 small gong
	09 longer gong as if hit by heavy metal tray
	0A bu-bip button press
	0C donk donk
	0D beeoo poke
	0F dop-dop-dop-drrrr-op-op
	11 ice spell glass shatter gashaan
	13 fax modem loop
	14 computer turns on with beep/drive noise and fan loop
	15 computer acknowledgement blililip
	1C light dop, something put on a table
	2C computer brief drive noise and fan loop


##### Typical script with choices

```
01 FB 0001 - begin script section 1
	FD 09 What now? 00 - print "What now?"
	1B - push choice level?
	04 - begin choice block
		FD 04 Look 00 FF 000C - choice index C is "Look"
		13 FE 0040 12 FB 1234 14 - if var $40 is $1234 then
			FD 05 Think 00 FF 000D - choice index D is "Think"
	05 - end choice block, get user choice
	FF 000D - begin outcome block for choice index D
		FD 0F You think hard. 00 - print "You think hard."
	08 - end outcome block, get user choice
	FF 000C - begin outcome block for choice index C
		1B - push choice level?
		04 - begin choice block
			FD 04 Self 00 FF 000B - choice index C:B is "Look:Self"
			FD 06 Kitten 00 FF 000C - choice index C:C is "Look:Kitten"
		05 end choice block, get user choice
		FF 000C - begin outcome block for choice index C:C
			FD 0B Cute kitty! 00 - print "Cute kitty!"
		08 - end outcome block, get user choice
		FF 000B - begin outcome block for choice index C:B
			FD 0E You look good! 00 - print "You look good!"
		08 - end outcome block, get user choice
	08 - end outcome block, get user choice
02 - end script section, get user choice
```

Normally each 01 section start is paired with an eventual 02 section end.
04 choice list start is paired with 05 choice list end.
If-then statements can be followed by a single command, or 0A-many commands-0B.
The 0B may be omitted but is implicit if a section end command appears instead.
An 0B may also appear without an 0A first, in which case ignore it.

A choice outcome starts with FF while not building a choice list.
Each FF should be paired with an 08 outcome end, but sometimes is not...

Eve a006 section 03 has an 見る・調べる:報告書 (Examine:Report) command which
lacks an 08 and instead is followed by 見る・調べる:机 (Examine:Desk) directly.
The messages don't imply fall through. The latter command ends with an extra 08,
but that's intended to end the Look-Examine outcome.

Eve a009 section 03 has two 04 聞く subitems $28 and $29. The 04 outcome section
starts with FF as expected, and each subitem is FF-stuff-08. But then immediately
follows an FF-0003 to begin 03 outcomes, without a final 08 for the 04 outcomes.

Eve b029 section 06 has FF suboptions for 04 Save-Load, even though Save-Load is
not listed as a choice in this section. Most likely a copypasta fail, ignore the
FF suboptions in this case.

Some choices can be conditional. The condition is checked every time the choices
are brought up, enabling or disabling the condition each time. A choice list can
have multiple conditional items. A choice outcome however will go straight back
to getting a new user choice at the end of the first executed conditional block.

Choice list indexes can be in any order and may share indexes with choices from
earlier or peer levels. Outcomes may appear in any order as well. Outcome indexes
appear to be unique only within their own level.

Etsugaku s03 section 3B has outcome 0A, with no corresponding option. The brief
code in this outcome tries to jump to section05, which doesn't exist. This doesn't
appear to be accessible in-game, so ignore it.

Etsugaku s05 section 40 has you guessing which club might be correct. The two
available choices are Sex Research and Swimming. Two additional wrong guesses exist
as invalid option outcomes: Track&Field and Manga Research. Ignore these.

Etsugaku s06 section 47 has only two options, one of which is invalid and has an
empty outcome. Ignore it.
