Fairytale MES/REA bytecode
--------------------------

From mills5_:

Most of the text is in REA files...

- In the .MES and .REA files, text strings begin with the bytes '6D 81'. I don't know how they're terminated, just that new messages begin with that signature. Two bytes after the '81' (skipping one byte, I don't know what that one does) are two bytes that indicate the name of the speaker. '0A 6E' is Akira, the game's protagonist. Then, you skip one more byte ahead, and this is where the actual string starts. 
- The strings are actually encoded in standard Shift-JIS, but the bytes are flipped.

For example, the Shift-JIS encoding for '自分' is 8E A9 95 AA, but in the .MES/.REA files, it appears as A9 8E AA 95.

Here’s a summary in C-style struct pseudocode for how the messages in the .MES/.REA files are structured, according to my understanding:

struct KoroshiMessage {
    uint8_t start_signature[2] = { 0x6D, 0x81 }; // 6D 81 signature
    uint8_t unknown_byte_1;
    uint8_t speaker_name[2]; // two bytes indicating the character/entity speaking the message
    uint8_t unknown_byte_2;
    uint8_t sjis_message_with_flipped_bytes[]; // the actual message string that you'd see in-game, but each SJIS byte is flipped
};
