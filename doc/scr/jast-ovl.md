JAST/Tiare OVL files
--------------------

Unless otherwise specified, character strings in OVL files are null-terminated,
encoded as Shift-JIS, with both single-byte and double-byte characters.
Shift-JIS incorporates the first 128 bytes of ASCII as single-byte characters,
so filenames are easily readable. Actual game text and selection verbs and subjects,
however, are double-byte Shift-JIS in the PC-98 versions.

Words are stored in x86-native, least-significant byte first, format.


##### OVL Header

OVL files begin with an array of word pointers, each delineating different sections
of the header. The first pointer importantly marks the end of the initial pointer
array, and start of the first header section.

There will be a different number of pointers depending on engine version. You can
normally determine the type by looking at the first word pointer:
- 0x0006 or 0x0008: old OVL type, not entirely understood
- 0x000A: OVL v3 type file
- 0x000C: OVL v2 type file
- Anything else: there may not be a header at all, this script is plain bytecode

Header layout outline:

```
+---------------------------------------------------------+
|  Word pointers to header sections: @H0, @H1, @H2, ...   |
+---------------------------------------------------------+
|  H0: Word pointers to choice combination results @CCR   |
+---------------------------------------------------------+
|  H1: Word pointer to bytecode start, end of header @BC  |
+---------------------------------------------------------+
|  H2: Word pointer to option lists @OL                   |
+---------------------------------------------------------+
|  H3: Word pointer to graphics lists @GL                 |
+---------------------------------------------------------+
|  H4...: Word pointers to nothing                        |
+---------------------------------------------------------+
|  GL: Graphics list, ID + null-terminated strings        |
+---------------------------------------------------------+
|  OL: Option list array, verb+noun pair pointers         |
+---------------------------------------------------------+
|  Verbs and nouns, ID + null-terminated strings...       |
+---------------------------------------------------------+
|  CCR: Choice result array, verb+noun pair pointers,     |
|       tracking variable numbers, jump addresses         |
+---------------------------------------------------------+
|  BC: Script bytecode start                              |
+---------------------------------------------------------+
```

##### CHOICE COMBINATION RESULTS: first word array

In most OVL files, but not all, each word in the array points to the
beginning of a variable-size choice record; each record ends where the next
starts. However, some OVL files only point to the first choice record even
though multiple records are present, so the decompiler needs to determine for
itself where each record ends.

If the first word array has a length of 0 words, or if the first offset is
invalid (for example 0000 or FFFF), then there are 0 valid records, and the
script does not offer choices to the user.

To read the results correctly, start from the first record and keep reading
up to the offset where bytecode starts.

Each record consists of:
- pointer to an ID byte + verb string, null-terminated : word
- pointer to an ID byte + subject string, null-terminated : word
- variable number attached to the choice : word
- jump addresses : array of words

The first two words must point into the header's data section. If either
points anywhere outside the data section, it must be ignored. The verb must
be valid, but an invalid subject just means the verb has no subjects and is
selected directly. The verb and subject strings are preceded by an ID number,
which can probably be ignored.

The jump addresses must all point into the bytecode segment, and there must
be at least one valid address. You should allow for at least 32 jump
addresses for each verb-subject combination. To determine how many addresses
each record has, keep reading jump addresses until you come to the start of
bytecode, or encounter an address that points into the header's data section
instead of to bytecode. If an address points into the data section, but is
not zero, it is the first pointer of the next record. If the address is zero,
it just marks the end of that bunch of addresses.


##### BYTECODE SCRIPT ENTRYPOINT: second word array

After dealing with the entire header, jump to this address and start
processing the script logic. This address also marks the end of the header's
data section.


##### OPTION LISTS: third word array

Option lists outline the verbs and subjects that are available, and the order
in which they should appear. Commands to show or hide verbs or subjects also use
these option list indexes.

Each option list is an array of words, terminating with FFFF:
- pointer to an ID byte + verb string, null-terminated : word
- pointer to an ID byte + subject string, null-terminated : array of words
- terminating FFFF : word

If a pointer to a verb or a subject has an address outside the data section
of the header, treat it as FFFF and consider that option list concluded.
Again, the ID byte values before strings can be ignored. The options should
be stored in memory in the order they were defined, not by their ID values.

Example from Season of the Sakura:

CS101.OVL has the following third array: `002C, 0030, 0034, 003A`

This means there are four verbs, each of which has 0 or more subjects.

At each address we find an option list:

- 002C:  004A, FFFF (has 0 subjects)
- 0030:  0051, FFFF (has 0 subjects)
- 0034:  0062, 006F, FFFF (has 1 subject)
- 003A:  0068, 006F, 0079, 007F, 0087, 008F, 009F, FFFF (has 6 subjects)

Fetching the strings resolves to:
```
AVOID
DEFEND YOURSELF
TALK > NOBUYUKI
THINK > NOBUYUKI, SELF, MOTHER, FATHER, YAMAGAMI DENKI, HIDEMI
```

Now, when in the bytecode the game tells you to disable verbs 3 and 4, you
hide TALK and THINK. And when it tells you to enable verb 4, subject 3, you
show THINK > MOTHER.


##### GRAPHIC LIST: fourth word array

This is a list of graphics that the script is going to use. The array points
to records describing each graphic. If the first record points to a zero
word, then there are no graphics.
Each record consists of:
- transition style : byte
- graphic type : byte
- file name : string

The transition style byte tells which transition to use to make the image
visible whenever it is drawn. A list of transition styles is found below.

The graphic type may be:
- 0x50 - a fullscreen background, but black out the screen before showing this
- 0x4E - a fullscreen background
- 0x42 - a fullscreen image of an older sort?
- 0x38 - a sprite image that needs transparency
- 0x03 - a fullscreen image that's already there?

3sis-era engines draw graphics by a command that has an index number into
this graphics list as a data byte. The list indexes are 0-based. Sakura-era
engines, on the other hand, define the graphic's filename as a part of the
drawing command, and do not use graphic lists.

Furthermore, in 3sis-era engines, each blinking animation seems to be loaded
automatically whenever the corresponding body is drawn - the animation has
the sprite's name plus A0, and almost every sprite has one. If a sprite has
two people, there could be an A1 as well, likewise automatically loaded.
There might not be an A0 at all, in which case there is no animation.

In newer-engine games, they sometimes forget to start a blinking animation.
It might actually be best to ignore the animation script commands and always
draw the associated blinkies when its related sprite is shown.

Note, that although in most games only type 0x38 sprites may have animations,
in at least Yukineko and Tenkousei background/event images may have
automatically loaded animations as well.


##### Engine differences

Tasogare no Kyoukai has different code or headers in two files: TA_00DG and
OP_M2. For details, see tasogare.md.

For Maririn DX engine oddities, see maririn.md.

Deep scripts usually start with 0x0008, and have the following header:

1st word array - the addresses point to variable-length click actions. Each
action begins at the given address and continues until the next address.
Conveniently the first address of the next array is right after the last
action, so you can always read up to the next word address.

2nd word array - bytecode addresses for jumping. The first address is used
when entering the script, and it also terminates the click actions from the
first array. This uses 1-based indexing.

3rd word array - zero?

4th word array - points to a string of bytes, possibly mapping each click
action to specific coordinates given in MAS files... or maybe it's a list
of which local variables will be used and should be zeroed out at init?
This array terminates at the first address of the first array.

Not sure where the script-specific images are referenced, although something
is certainly at 0x1DDAB in DEEP.EXE.
Battle image names are hardcoded in MAIN.COM.

If the script starts with 0x0006, then the first word array is skipped, and
the first present array is actually the scripts array.
If the script starts with 0x0000 (for example S40501b), skip the first word
and read the four array pointers normally from the second word onward.

The game's interface is point-and-click, and the cursor changes to indicate
action on clickable areas. MS_CUR contains the cursors. The clickable areas
are probably in the MAS files.
```
00:00 - Pointer arrow
00:01 - Look
00:02 - Go
00:03 - Talk
00:04 - Hit (pink fist)
00:05 - Push (smacking palm)
00:06 - Open
00:07 - xxx touch
00:08 - xxx tongue
00:09 - xxx lips
00:10 - xxx drill? screw?
```

Example: S30101B.OVL is run when you select the blonde chick, then go in the
first map square for the second time.

The game will display background DB_01_01, and over that the sprite DT_01_08
containing two characters who don't blink. It executes code at 003A, and upon
exit, retains the screen while letting the player click different areas.
There are two areas to 03-Talk at, and three areas to 01-Look at.
Right-clicking brings up a menu with one option, return back to the map.

```
1st word array @ 0008 = 0029 002C 002F 0032 0036
2nd word array @ 0012 = 003A 00CB 029D 0342 03B1 04E3 053F 05D9
3rd word array @ 0022 = 0000
4th word array @ 0024 = 0102030405
actions = 01-01:02    (look at guy, local var 1, code block 2)
          01-02:03    (look at chick, local var 2, code block 3)
          01-03:04    (look at bkg, local var 3, code block 4)
          03-04:05,06 (talk to guy, local var 4, code blocks 5 then 6)
          03-05:07,08 (talk to chick, local var 5, code blocks 7 then 8)
```

Other DEEP stuff:

OP_xx are the openings, and the original uses timed waits instead of waitkey.

The character abbreviations are:
- J for Jesus - the purple-haired magical girl from heaven
- K
- M
- S

It seems the music files played are different depending on character.
First script sets v257, v497, v498 = 1; A, B and C are available
First B script sets v258, v499 = 2; D becomes available
Third script sets v259, v499 = 1; nothing new becomes available
Fourth script sets v260, v500 = 1; E becomes available
Fifth script sets v261, v501, v502 = 1; F and G become available
Sixth script sets v262, v264 = 1; nothing becomes available
Seventh script sets v262, v263, v503 = 1; H becomes available


##### SCRIPT BYTECODE

The original engine appears to keep a stack of scripts, allowing jumping to
a new script, and later returning back to the previous one in the same state
it was left. Probably best implement a script tracker that remembers local
variables, choice strings and visibility, and script execution offset; but
graphics need not be remembered.

At the initialisation of a new script, do the following:
- All local variables 0..255 must be zeroed out. (For more on variables, and
  an example on choice jumps, see after the bytecode list.)
- All freshly defined choice verbs and subjects must also be set to be
  visible by default. Hiding or showing a verb does not change the visibility
  state of the subjects it contains, although obviously any subjects under
  a hidden verb will be inaccessible.
- 3sis-era engines automatically clear all graphics from the screen and draw
  and transition in the first item on the script's graphics list, which is
  usually a background picture.

The script itself is a series of command bytes, each followed by 0 or more
data bytes. The various engine versions have a number of differences in their
command codes, but some codes also remain constant throughout.


##### Bytecode list

`00 [00]`
All

End of bytecode section. A few different things can happen here:
- If anything has been printed, wait for a keypress before anything else.
- If choice verbs have been defined, display those to the user and keep going
from wherever the choice leads.
- If verbs have not been defined, return execution to the previous script, to
the offset just past the most recent runscript command. You must also restore
the local variables of that previous script, as well as its choice
definitions if any, and possibly also the return offsets for 03 jump calls.

Since choices are always defined in the header (except in Tasogare), you
could treat 00 commands in choice-containing scripts as an explicit "display
choices" command, while in choiceless scripts it has to be a "pop script".

This bytecode often shows up as a pair 00 00, in which case just ignore the
second 00. In fact, keep skipping until a non-00 byte is encountered. Also,
if 00 is the last command of the script, and it was preceded by a runscript
command, you can pretty safely omit the 00 command. Likewise, if 00 is ever
preceded by an 03 return or an 0B-36 unconditional jump, you may as well omit
the 00.

`01`
All

Wait for a keypress, then clear the textbox.
In Sakura, no graphics are actually displayed until a transition command is
encountered. For consistency, you may want to apply the same rule to 3sis,
and add an implicit transition right before the first waitkey after any
graphics loading commands, rather than transitioning immediately upon the
load command.

`02 xx`
Tenshitachi no Gogo Collection 1, Tenkousei, Yukineko

Jump to script xx. Scripts have a game-specific name with a number appended.

`02 xx`
Maririn

Execute hardcoded command sequence number xx. See maririn.md for details.

`02 xx`
Deep

Unknown... perhaps unlock movement board squares, or go to h01_xx?
- [E02j] 02-01 close to end of script
- [H01_01] 02-02 just before last exit
- [H01_02] 02-03

`02 xx yy zz`
Tasogare

Enters the first-person view dungeon. xx = dungeon file to use (MAP1.DAT or
MAP2.DAT), yy and zz are entry coordinates with values ranging 1..100.
yy = y coordinate, zz = x coordinate.

For more on the dungeon, see tasogare.md.

`03`
Sakura

Return from jump command within the same script; set script execution back to
right after the last place jumped from. Jump/returns must stack a few times.
Only ever used after 0B-39 case jump.

Used in: CS502, CS507_1, CSC01_A, CSC01_C, CSC01_F

For compatibility with the other games, it might be best to treat 0B-39 as
a script call rather than a goto. Then 03 can be a script pop in Sakura too.
0B-39 is used in CS501_1 and CSC01_B in addition to the above. This hack will
work correctly in all those script, as long as the choices inited in that
script are preserved.

`03`
Deep, Hohoemi, Vanish

Probably an explicit pop script. Used a lot, sometimes in inaccessible code.

`03`
Tasogare

Exit to the previous script, or immediate return to the previous game mode -
either the clickable map or the dungeon. The engine needs to see if there's
anything printed in the textbox, in which case wait for a keypress before
delving further.

`04 string`
3sis, Eden, FromH, Majokko, Runaway, Sakura, Tasogare

Go to a new script, name follows the code as a zero-terminated string.

`04 xx yy zz`
Deep

Probably jump to script xx yy zz?

`05 xx`
Maririn

Unknown...

`05 xx yy`
Deep

- If yy = 0, then run the script "Exx".
- If yy = 1, then run a character-specific script "Exx_", where presumably the
  last letter is either a global variable or hardcoded. Can be J, K, M or S.

`05 string`
Hohoemi, Parfait

Run a new script probably, zero-terminated name follows the code.

`05 string`
Tasogare

Run a new script, zero-terminated name follows the code. Only used in
TA_00DG. This seems to indicate to remain in the dungeon view, while
a regular runscript switches back to the VN interface...

`06 xx`
Eden, FromH, Hohoemi, Majokko, Sakura

Play song number xx, 1-based indexing, or stop playing if xx = 0 or FF.

`06 xx`
3sis, Runaway, Tenshitachi Collection 1, Tenshitachi Collection 2,
Tenkousei, Vanish, Yukineko

Draw a graphic from the graphics array, index xx. Implicitly draw all its
existing animations as well. The new graphics are transitioned in immediately
using the transition number listed for the loaded graphic in the graphics array.

Special note: This command has an implicit delayed function. Upon the next
graphic loading command, 06-xx or 11-02-xx, all sprites should be cleared
automatically to make room for the new ones. Except in Tenkousei, 11-02-xx
doesn't exist, so 06-xx also displays sprites persistently.

`06 xx yy`
Parfait

Unknown...

`07 xx`
3sis, Deep, Runaway, Tenshitachi Collection 1, Yukineko

Play song number xx. Most games have all songs named with a game-specific
prefix followed by a number. However, in Yukineko the files are not numbered.
Look in SETU.EXE at address 0x1466F. The 22 zero-terminated strings found
there are the songs in the right order.

If song number is FF, the music fades out.

`07 xx`
Maririn

Unknown...

`07 xx`
FromH, Tasogare

Prints the value of variable xx.

`07`
Hohoemi

Unknown... only found in *_PRE scripts, always between two Shift-JIS strings
in the form text-20-07-01-20-text.

`08`
Hohoemi, Majokko, Sakura

Pause in text output until a key is pressed, do not clear the textbox.

`08 xx`
Deep

Probably starts a fight against opponent xx.
xx = 01: generic security guy
xx = 02: airplane stewardess who kills you dead

`08 xx`
Tenkousei, Vanish, Yukineko

Sound effect number xx.

Tenkousei:
- 00: echoed hi-hat hit
- 01: snare-like bash, stereotypical 8-bit sound for hitting something.
- 02: echoed snare-like bash
- 03: soft snare with comedic "squeeze-squeeze" sound

Yukineko:
- 01: a feline FM moan
- 02: echoed melodic bass ka-POW
- 03: snare-like bash
- 04: plink! not used

Vanish:
- 00..0F: a variety of sounds, but Anex86 doesn't like the game

`09`
3sis

Pause in text output until a key is pressed.

`09`
Eden

Single-byte command in the middle of a dialogue title, no discernible effect.
Ignore it, probably a typo or something.

`09 xx`
Hohoemi, Majokko, Parfait

Sleep for xx desiseconds, interruptable. In Majokko, used to show lyrics for
a song halfway through the game.

`09 xx`
Deep

Unknown, but probably sleep for xx desisecs too.

`09 string`
FromH

Very strange, only appears in MT_012. Each is followed by a bunch of
single-byte ascii symbols, not terminated with zeroes. The occurrences are:
[09-"B_O" 09-"B_FADE" 09 09-";BLACK"]

`09 xx yy zz`
Tenkousei

Jump to script number zzyy. Xx seems to be always zero. This highly redundant
command is a stopgap measure used only in this engine, since some script
numbers go above 255, even though the total amount of scripts is below 200.

`0A`
All

Print a linebreak in the textbox.

`0B xx ...`
All

These are various function calls dealing with local variables and jumps. In
most games the variables and direct values are all 8-bit.
In Parfait, and probably other Windows-based games, they are 16-bit.

`0B 01 yy zz`

Increase var yy by shortint(zz).

`0B 02 yy zz`

Increase var yy by var zz.

`0B 03 yy zz`

Decrease var yy by var zz.

`0B 04 yy zz`

Let var yy := var zz.

`0B 05 yy zz`

Let var yy := byte(zz).

`0B 06 yy zz aa`

Let var yy := var zz - var aa.

`0B 07 yy zz aa`

Let var yy := var zz - byte(aa).

`0B 08 yy zz aa`

Let var yy := var zz AND var aa.

`0B 09 yy zz aa`

Let var yy := var zz OR var aa.

`0B 0A yy`

If var yy := 0, then carry out command `00 00`.

`0B 0B yy zz [yy bytes]`

Let var zz := (var zz) AND (var aa) AND (var bb) AND ...

`0B 0C yy zz [yy bytes]`

Let var zz := (var zz) OR (var aa) OR (var bb) OR ...

`0B 0F yy zz [zz bytes]`

Unknown, maybe var yy := var aa something var bb something...

`0B 10 yy zz [zz bytes]`

Unknown, maybe var yy := var aa something var bb something...

`0B 14 yy zz`

Let var yy := random number from 0 to (byte(zz) - 1).

`0B 15 yy`

Play song number (var yy), zero-based indexing.

`0B 1D yy zz aa`

Unknown... seems to do something to vars 30..34? Check which endings
have been completed? There are 5 endings in Parfait...
- MS_MAK6.OVL: [0B 1D 0000 001E 0045]
- TITLE.OVL:   [0B 1D 0000 0000 03E7] <-- not valid goto within title.ovl
-              [0B 1D 0000 001E 0045] <-- same

`0B 32 yy zz aa`

If var yy > 0, jump to @zzaa.

`0B 33 yy zz aa`

If var yy == 0, jump to @zzaa.

`0B 34 yy zz aa`

If var yy < 0, jump to @zzaa.

`0B 35 yy zz aa`

If var yy != 0, jump to @zzaa.

`0B 36 yy zz`

Unconditional jump to @zzyy.

`0B 37 yy zz aa ...`

Case-goto. There's a series of word addresses at aazz and onward. Jump to the one at
index [var yy]. The number of addresses is not given; bytecode may just continue without
any ending marker. One of the indexes often points to the first byte after the word array,
but not always. Sometimes the first bytecode word after the end of the array will point
outside the valid bytecode area, but this is not reliable.

Try this: Stick all jump offsets used in the script into a table. Since jump targets
must be valid code, you can stop parsing the array once you reach any such offset;
or an address outside the valid code area, which also must be bytecode instead of an
address. Hack remaining errors.

`0B 38 yy zz`

Unconditional call to @zzyy.

`0B 39 yy zz aa ... FFFF`

Case-call. Var yy is the index to use; aazz and onward is a series of word call
addresses, terminating with 0xFFFF.

Since this is a call, not a jump, you have to return execution to the byte after the
terminating 0xFFFF when exit code 00 or return 03 is encountered in the called code.

`0B 46 yy zz aa bb ...`

Immediate multiple choice with custom choice strings.
- The choices appear in a separate single-column choice box on the right side,
  not in the main text box.
- The user's chosen index is saved in variable yy.
- There are zz choice strings, and thus zz word addresses from bbaa onward.
- Each address points to a byte ID directly followed by a null-terminated string.
- The ID doesn't seem important...
- At least in Hohoemi, you can cancel out of the multichoice, which saves -1 in var yy

`0B 47 yy zz aa bb ...`

Same as 0B-46? But the choices go in the main text box, in single column.

`0B 48 yy`

Get a game-specific graphical user choice into var yy. In Hohoemi, this is the set of
buttons on the right side of the viewframe. 0 = study, 1 = talk.

`0B 49 yy zz aa bb cc dd ...`

Immediate multiple choice with custom choice strings, choices conditionally masked.
- The user's chosen index is saved in variable yy. The index must correctly skip over
  masked choices! Eg. if there are choices 0..2 and the first two are masked, the
  result must still be index 2.
- There are zz choice strings, and thus zz word addresses from ddcc onward.
- Each word address points to a byte ID directly followed by a null-terminated string.
- Var aa is always set to 0 before calling, and never referenced afterward.
  Something used for internal accounting? Possibly safe to ignore.
- Var bb has the choice mask value. Counting from the lowest bit, show each choice
  index only if the corresponding bit is set.

`0B 4A yy zz aa bb ...`

Exact same as 0B-46? Choices in separate box or main text box?

`0B 4B yy zz aa bb cc ...`

Immediate multiple choice and jump, with custom choice strings.
- The choices appear in the main text box, in a single column.
- There are yy choices, and this yy word address pairs from aazz onward.
- In each address pair, the first points to a byte ID directly followed by
  a null-terminated string. The second address is the jump address.
- Display all given choice strings, let the user choose, and jump to the
  address paired with that choice.
- When parsing, try filling the option strings pointed to with zeroes after
  processing the command, so the strings aren't misinterpreted as code once
  you parse that far.

`0C xx yy zz`
All

Global variable interaction - see further down for more on variables.
- xx = 01: Let local var yy := global var zz.
- xx = 02: Let global var yy := local var zz.
- xx = 03: Let global var yy := byte(zz).

`0D xx ...`
Deep

Unknown? Tends to be followed by 00-00 exit.
- [S41010b] 0D-01-01-05
- [S40505] 0D-01-02-0A
- [S40307] 0D-01-02-0A
- [H01_01] 0D-01-05-0A, 0D-01-01-06, 0D-01-02-07

`0D xx ...`
Parfait

Seems to be a bunch of different commands.
- xx = 02: unknown, four data words?
- xx = 09: unknown, one data byte; maybe an explicit transition command?
- xx = 0A: draw an animating sprite, with following arguments:
  * filename, zero-terminated
  * one word: unknown... animation speed?
  * two words: location x/y, can be FFFF in which case don't draw it?
  * two words: frame size x/y
  * many words: sequence of frame indexes to display, FFFF terminates
- xx = 0B: unknown, no further data bytes; maybe new clearallbutbkg?

```
EDIT.OVL:    [0D 08 02 00 08 00 EDIT 00]
TITLE.OVL:   [0D 02 005B 0190 01CA 40 00]
             [0D 02 005B 0190 01CA 40 00]
MS_MAK6.OVL: [0D 02 005B 0190 01CA 40 00]
             [0D 09 01]
             [0D 09 32]
             [0D 0A "PUSH2" 00C8 loc -1,-1 size 16x16, frames]
             [0D 0A "key"   018F loc 551,331 size 85x45, frames]
```

`0E xx`
Majokko, Tenkousei, Tenshitachi Collection 1, Tenshitachi Collection 2

Pan across a big image. The data byte describes direction of
movement. Complicating things, the big image is divided in two
separate images. Tenkousei and Angels2 display the first image, which
is marked scrollable by the transition style 2 or 3, then use the 0E
command to slide the other image with the same style in the gfxlist
into view.

Majokko displays one half as a background and the other as a sprite...

I just composite the images by manual definition, since there aren't
that many of them. The data direction means, as best as I can tell:
- 1, 2, 3: hard to say, but all vertical panning...
- 4: pan upward
- 5: pan downward
- 6: pan left
- 7: pan right
- A: (only once in Majokko) pan downward slowly? re-pan upward quickly?

First such pan when Aya transforms:
```
13 04 07 0A KE_039_1 00 gfx.show KE_039_1 bkg
13 03 07 0A KE_039_2 00 gfx.show KE_039_2 (sprite)
15 04 gfx.removeanims
13 02 05 07 0A 0E 32 03 00 gfx.transition instant, sleep
sound effect text!
06 1B actual sound effect!
0E 04 pan upward! Takes about 2.5 seconds
16 03
```

'0E xx`
Deep

Unknown... probably image pan.

`0E xxyy`
Parfait

Unknown... perhaps a swipe or transition.
Data values encountered: 0000, 0007, 0009, 000A

`0F`
3sis, Tenkousei, Vanish, Yukineko

Jump to an ending script, or just return to the main menu.

`0F xx`
Hohoemi, Sakura

Move to good ending number xx.

`0F [sequence of graphics]`
Tasogare

The command is followed by a sequence of special command bytes. At first,
fade the whole screen to black. All images are faded in for 3 seconds, stay
for 8 seconds, and fade out for 3 more.
- 01: new viewport-sized image, graphic name follows as a zero-terminated string.
- 02: new full-screen image, graphic name follows as a zero-terminated string;
  these could be treated as an overlay imposed over the viewport-sized
  images, since these are mainly credit text.
- 03: display lots of dialogue with interspersed 01 bytes, ends with 00 00.
  Each 01 is meant to pause for a few seconds to let the text be read,
  instead of being a waitkey command.
  If the ending 00 00 is followed by 03, that's the end of the sequence.
- 04: unknown, no extra data after this; possibly indicates that the next 03
  text segment will not be ending the 0F sequence yet.

`0F [graphic]`
FromH

Followed by a zero-terminated string, specifying an ending picture... roll
credits over it?

`10`
All?

Jump to a bad ending

`11 xx`

A wide variety of odd function calls...

`11 02 yy`
3sis, Runaway

Display graphic yy, and its animations if they exist. Unlike graphics loaded
with 06-xx, this does not have the implicit delayed function of clearing all
loaded sprites upon the next load command. This is used when more than one
sprite is needed on-screen at the same time. The graphics should only be
transitioned after a following 06-xx.

`11 02 yy`
Vanish

Initiate battle type yy. The types themselves are probably hardcoded, and
need to be figured out by seeing what exactly the original game does.

`11 03 yy`
Vanish

Either a return or jump to a new script. Ending? H-scene? Battle? Go to map location?
```
VAN_512 [11-03-03] not reachable? preceded by runscript EB019_02.
EB033 [11-03-06] only bytecode in the whole file, then verbless exit
EB032 [11-03-07]
EB031 [11-03-04]
EB030 [11-03-05]
EB029_00 [11-03-01]
EB019_02 [11-03-03]
EB011_00 [11-03-02]
```

`11 04 yy zz`
Vanish

Variable yy interacts with value zz? Check EB027_01, _02, maybe can figure
out the logic from a decompiled script with dummy 11-04. Those appear to be
the scripts where the player gets to pick three girls to join his party.
Var 50 is used to count number of girls chosen. 11-04-yy-zz might then be
used to tell the engine that party member number [var yy] shall be girl [zz]?

`11 04`
Tasogare

Asks the user for a slot, then saves the game!

`11 05`
FromH, Tasogare

Bring up a name entry screen, put user's choice into string 001.

`11 05`
Vanish

Might be a Quit Game command, dump user right into DOS.

`11 05`
Hohoemi

Unknown, only found at end of CG script, perhaps return to main menu.

`11 06 yy`
FromH, Tasogare

Mark graphic ID yy as seen, for later CG viewing. SuperSakura handles graphic
memory internally, so this can be dummied.

`11 06`
Hohoemi

Save the game.

`11 06 yy`
Eden

Launch a mouse-pointable map of the mansion. There are four times in the game
when the map can be called, and therefore four sets of scripts that the map
can lead to. The data byte yy defines which set to use. The map used is image
MAP2, and selectable areas are highlighted upon mouseover. No extra text appears
on highlight. Clickable areas do not change between sets, but the target scripts do,
as charted below.

The map graphic has location names embedded in the image in Japanese. The outside:
- 霊廟 (mausoleum, shrine, crypt)
- 花壇 (flowerbed, garden?)
- 噴水のある庭 (fountain in garden)

桐生院邸1階 (1st floor of Kiryuuin Manor):
- 浴室 (bathroom, not visitable)
- 音楽 (music room)
- 空室 (empty room)
- 研究 (study)
- 宗時 (Touyama, the butler)
- 絵里菜 or 絵里奈 (Erina, the maid) different kanji used on map vs in dialogue
- 徳富 (Syu, the cook)
- 加藤 (Katou, the solicitor)
- 小原 (Ohara, the doctor)
- 厨房 (kitchen)
- ダイニング (dining room)
- 玄関ホール (entrance hall)
- 応接 (front room)

桐生院邸2階 (2nd floor of Kiryuuin Manor):
- 榊 (Sakaki, the youngest son)
- 梓 (Azusa, the second daughter)
- 空室 (empty room x 2)
- 柾 (Masaki, the eldest son)
- 頼政 (Yorimasa, the client?..)
- 頼重 (Yorishige, the younger brother)
- 登紀子 (Tokiko, Yorishige's wife?)
- 空室 (empty room)
- 優里 (Yuri, Yorimasa's wife)
- 桜 (Sakura, the eldest daughter)
- 岩波 (Iwanami, the illegitimate son)
- 丈 (Joe, the detective)

The main text box says "どこ行こうか?" (Where shall I go?)

```
Set 1: JO2102x
+---+  +-----------------+  +-----------------+
| O |  |   | | L | K | G |  | 5 |  2 | 7  | 6 |
+---+  |   | +---+---+---|  |   +-+--+--+-+   |
       |---+             |  |---+ |  9  | +---|
+---+  | D | +---+---+---|  | 4 | +--+--+ | 3 |
| P |  |   | | E | F | H |  |   | | R| R| |   |
+---+  |---+ |   |   |   |  |---+ +--+--+ +---|
       | R | +---+---+---|  | R |         | 8 |
+---+  |   | |     I     |  |   |         |   |
|   |  |---+ +-------+---|  |---+         +---|
| N |  | C |         | J |  | Q |         | 1 |
|   |  |   |    M    |   |  |   |         |   |
+---+  +-----------------+  +-----------------+

Set 2: JO2116x
+---+  +-----------------+  +-----------------+
| T |  |   | | P | O | J |  | 5 |  2 | 9  | 7 |
+---+  |   | +---+---+---|  |   +-+--+--+-+   |
       |---+             |  |---+ |  C  | +---|
+---+  | G | +---+---+---|  | 4 | +--+--+ | 3 |
| U |  |   | | H | I | L |  |   | | B| B| |   |
+---+  |---+ |   |   |   |  |---+ +--+--+ +---|
       | B | +---+---+---|  | B |         | A |
+---+  |   | |     M     |  |   |         |   |
|   |  |---+ +-------+---|  |---+         +---|
| S |  | F |         | N |  | 8 |         | 1 |
|   |  |   |    R    |   |  |   |         |   |
+---+  +-----------------+  +-----------------+

Set 3: JO3404x
+---+  +-----------------+  +-----------------+
| T |  |   | | P | O | J |  | 5 |  2 | 9  | 7 |
+---+  |   | +---+---+---|  |   +-+--+--+-+   |
       |---+             |  |---+ |  C  | +---|
+---+  | G | +---+---+---|  | 4 | +--+--+ | 3 |
| U |  |   | | H | I | L |  |   | | B| B| |   |
+---+  |---+ |   |   |   |  |---+ +--+--+ +---|
       | B | +---+---+---|  | B |         | A |
+---+  |   | |     M     |  |   |         |   |
|   |  |---+ +-------+---|  |---+         +---|
| S |  | F |         | N |  | 8 |         | 1 |
|   |  |   |    R    |   |  |   |         |   |
+---+  +-----------------+  +-----------------+

Set 4: JO4102x
+---+  +-----------------+  +-----------------+
| M |  |   | | J | I | E |  | 5 |  2 | O  | 6 |
+---+  |   | +---+---+---|  |   +-+--+--+-+   |
       |---+             |  |---+ |  7  | +---|
+---+  | B | +---+---+---|  | 4 | +--+--+ | 3 |
| N |  |   | | C | D | F |  |   | | R| R| |   |
+---+  |---+ |   |   |   |  |---+ +--+--+ +---|
       | R | +---+---+---|  | R |         | P |
+---+  |   | |     G     |  |   |         |   |
|   |  |---+ +-------+---|  |---+         +---|
| L |  | A |         | H |  | Q |         | 1 |
|   |  |   |    K    |   |  |   |         |   |
+---+  +-----------------+  +-----------------+
```

`11 07 yy zz`
FromH, Tasogare

Two data bytes signifying two variables. Only in CG.OVL. Checks if the
graphic number in variable zz has been marked as seen. If seen, sets variable
yy to non-zero; if not seen, sets it to zero.

`11 07 yy`
Hohoemi

Show the stats for girl yy.

`11 08 yy`
Tasogare

Appears six times in TA_00DG. Data byte can be 0x0A or 0x14. Seems to be
a quick fade to black and back. The data byte controls the length of effect?

`11 09 addresses`
Tasogare

Launch a mouse-pointable location map. 11-09 is followed by 13 word
addresses, each pointing to a byte ID + null-terminated string, followed
immediately by bytecode. If the ID is 0xFF, then there is a string, but no
code, and that destination is not available. The string is displayed as
a mouseover legend, and there is no other mouseover highlighting. Clicking on
an active destination starts executing the following bytecode. The map
graphic is CHIZU. The actual map area coordinates are probably hardcoded.

```
Tasogare map:         0 - Oni no Amado ruins ("Demon's Heavenly Gate")
+------------------+  1 - big, friendly two-story house; Ichinose Ryokan
|      1  A&       |  2 - police station
|         &        |  3 - walled compound, rich guy Suemori's mansion
|        &  3      |  4 - river-crossing, roadside Jizou shrine
| 9    8 &7   2    |  5 - windmill-looking Furusawa shrine
|    6  &        &&|  6 - local health clinic
|  C   4       &&  |  7 - pair of houses; Melanie's home, Matsuno manor
|     &   &&B&&  0 |  8 - school and library
|   &&&&&&         |  9 - lone house, blue-roofed; Esaka home
| &&        5      |  A - hound stone
+------------------+  B - marsh, tiny island in river
                      C - stone pillar, execution site
```

`11 09 yy addresses`
FromH

Fade to black and launch a mouse-pointable location map. The data byte yy
defines the main character's costume; the mouse cursor changes to a chibi-guy
walking in place. The animations are in the image MS_CUR, and are numbered
0 to 5.

The costume byte is followed by 12 word addresses. Each address is either
zero, in which case that destination shall not be available, or points to
a null-terminated location description string followed by the code to execute
if the player chooses that location.

The map displayed is FROMAP. It has 12 possible destinations and a save
statue in the bottom corner that is always available; active destinations are
drawn highlighted from FROMAP2. The location coordinates are probably
hardcoded.

```
FromH map:
+------------+
|          2 |
|6     4     |
|   3      B |
|      5  C  |
|            |
| 7  9  A    |
|8        1 x|
+------------+
```

`11 09`
Hohoemi

Refreshes all custom elements in the viewframe.

`11 0A`
Hohoemi

Randomises the schedule for the week. Global variables DA..E5 are set randomly to 0-3,
where each number is one of four girls. There are three slots per day, and four days,
for a total of 12 slots per week. The same girl index can't appear more than once on
the same day.

`11 0B`
Hohoemi

Draws the upcoming week's calendar in the right half of the viewframe, and a summary
of all girls' stats on the left half.

`11 0B`
Tasogare

Probably a command to clear all current sprites in first-person view. This is
separate from the normal clearallbutbkg because the first-person view itself
is rendered inside the viewframe...

`11 0C`
Tasogare

Push player back to square they came from?
Appears 8 times in TA_4831 and 3 times in TA_00DG.

`11 0C`
Hohoemi

Refresh hours and minutes in the viewframe.

`11 0D yy`
Hohoemi

Fade everything to black, including the viewframe. Refresh custom elements in the
viewframe. Perhaps wait yy desiseconds? Then fade back to normal, revealing the new
viewframe values. The fades are quite fast.

`11 0E yy`
Hohoemi, Tasogare

Marks graphic ID yy as seen. SuperSakura handles the CG memory internally, so
this can be dummied.

`11 0F yy`
Hohoemi

Unknown...

`11 10 yy zz aa`
Tasogare

Modifies cell zz,yy of the current dungeon map to value aa.
Appears in TA_00DG, data bytes [08 3F C0] and [46 10 C0]. C0 means a wall.

`11 10 yy zz`
Hohoemi

Unknown... something to do with CG gallery. yy tends to be C8, zz = FA or 00.
C8 could be single-byte katakana "ne"? FA could be an action, and 00 exit...

`11 11 yy zz string`
Tasogare

Draw a sprite in first-person view mode, viewport size 280x280. Followed by
two location bytes. YO_21 in TA_1103 gets 5,0, which is drawn at 40,0 in the
dungeon view. All other pictures are always 0,0. Maybe fix the values to be
always centered?

The string is zero-terminated, specifying the YO-file to draw. If the
filename ends with an X, that means to draw the body (filename without the
last two letters), then the face (filename with the X replaced by A0).

`11 12 yy zz string`
Tasogare

Only used 2 times in TA_4848. Looks identical to 11-11, but the original game
doesn't actually display the graphic or do anything apparent. The sprites in
question do belong to people who have dialogue at those moments, so might as
well draw them.

`11 12`
Hohoemi

The user choice invoking this is "Load, Quit", whatever that exactly means.
Probably return to title screen.

`11 13 yy zz aa bb`
Hohoemi

Unknown... possibly var yy = var zz + var aa + var bb

`11 14`
Hohoemi

Unknown... related to ending scripts? Only in SYU_19, followed by 1E 01 01 01.

`11 15`
Hohoemi

Unknown... in ending scripts.

`11 16`
Hohoemi

Unknown... in ending scripts.

`12 xx yy [zz]`
All

- xx = 01: make verb yy invalid (1-based indexing)
- xx = 02: make verb yy valid
- xx = 04: make subject yy in verb zz invalid
- xx = 05: make subject yy in verb zz valid

The verb and subject validity are not tied. Disabling a verb makes it and any
of its subjects inaccessible, but if a subject is specifically disabled, then
merely re-enabling the verb won't make that subject available.

`13 xx`
3sis

Flash/smash with xx intensity.

`13 xx [...] 00`
Eden, FromH, Majokko, Sakura

A set of graphics functions. The command 13 is followed either by 0F, 10, or
a string of subcommands.

There is a kind of graphic stash, which allows saving the current visible
graphics, adding more graphics on top, then returning to the saved state.
Although it looks like the stash can technically persist across scripts, in
practice it's only ever used briefly in individual scripts.

(Presumably the original implementation uses double buffering, where some
commands draw into one buffer, others into the second, and you can flip
which buffer is active. Full low-level emulation of this would be tricky.
High-level emulation is tricky too, but allows more flexibility.)

The easiest way for SuperSakura to handle this is by using gob adoption,
where every new graphic becomes a child of any graphic it is on top of, so
animations are owned by their sprites, and the sprites by the topmost
background. A state save is accomplished by inserting the TB_008 transparent
graphic as an overlay over everything; following graphics get adopted by the
overlay; and a restore state just requires removing the overlay's kids.

Explicitly loading a new background image causes the graphic stash to be
cleared. This usually happens near the top of every script, which is why the
stash doesn't carry over in practice.

Scripts often request a restore state even without a stash. In these cases,
remove everything above the normal background at the next transition.

Due to the way animations are handled in the original engine, they have to be
repeatedly removed and restored, and are not part of the stash system. In
order to preserve animations across a stash pop, the script has to explicitly
restart required animations; not every script remembers to do this, which is
why characters sometimes stop blinking upon a stash pop.

13-10 is the graphic save stash command. It needs to preserve all
non-animating sprites.

13-0F and 13-04 without a filename are used to restore from the stash. All
current graphics need to be replaced with the stashed state upon the next
transition. If the stash is empty, redraw just the background and remove all
other graphics upon the next transition.

The real stack operations are used only in these scripts:
- Eden: JO1103, JO1106, JO2215, JO2216, JO3101, JO3103, JO3104, JO5103, JO5303
- Tasogare: TA_1105, TA_4252 (not really important here)
- Sakura: CS208, CS211, CS609, CS824, CSA01, CSA10, CSA11

JO3103 may not work with this?

If the byte following 13 is not 10 or 0F, there will instead be a string of
subcommands, processed one byte at a time, terminating with a 00 byte. The
string may contain ASCII characters, 0x20..7E, which form a filename.
- 01: Load graphic as an overlay over everything currently visible.
- 02: Display newly loaded graphics with a transition. This usually directly
  follows the graphics load commands, but rarely (Eden jo21026) may be
  significantly delayed, in which case newly loaded graphics must remain
  hidden until this 13-02 or 13-0E is actually executed.
- 03: Load graphic as a sprite over everything currently visible. If there
  is no filename included, remember the other given parameters for when
  the next graphic is loaded.
- 04: Load graphic as a background. No effect on any visible sprites; the
  existing sprites should be redrawn over the new background.
  As long as a filename IS included, this clears the graphic stash.
  If a filename is NOT included, this acts as a stash restore, the same
  as 13-0F. The only difference is that the subcommand string may
  contain an immediate transition, which 13-0F cannot.
- 0D xx yy: Draw graphic at horizontal offset xxyy*8.
- 0E xx: Blitz all new graphics on the screen right after loading this,
  using transition xx.
- 11: Seems to be a command to draw the first frame of the indicated
  filename's "A0" animation, as a sprite. Try this: draw the frame in
  a non-animating state, then when the actual animation is loaded,
  simply detect if the animation was already loaded as a sprite, and
  replace it.

Other, less useful subcommands, that can safely be ignored:
- 05: Draw sprite first, delay a moment, then fill the back with color 8
- 06: Same as 05?
- 07: Effect color 8 palette
- 08: Don't effect color 8 palette
- 09: Consider color 8 transparent
- 0A: No transparency?
- 0B: unknown? exists in Tenshitachi no Hohoemi and Parfait
- 0C: unknown? found in Hohoemi. Always after stopanims, before 11-0E-xx?
  And the image given is a special event image.
  Also in Parfait. Seems to be followed by image size words xx,yy.

`13 xx [...]`
Parfait

This resembles the above, but with differences.
Note that the char sprites are all 440 x 395 (01B8 x 018B).
Note that the viewframe is at 100,77 (64,4D), and is 440x304 (01B8 x 0130).
The first bkg drawn is MB_14.
The first sprite drawn is MT_02, at 100,0 from screen origin, ignoring the
background viewframe.

```
13 0D 0000 0000 0B 0064 0000 0C 018B 018B 0F 00 02
13 0D 0064 0000              0C 01B8 018B 01 10 00
13 0D 0000 0000 0B 0064 0000 0C 01B8 018B 0F 00 02
13 0D 0000 004D 0B 0000 0000 0C 01B8 0130 0F 00 01
13 0D 0000 0000              0C 0280 01E0 01 12 01 01 01 00
13 02 10 03 09 MS_OMAK1 00
13 02 10 02 09 FREFRE 00
13 02 10 01 09 MB_24 00
13 02 10 00 MT_07 00

xx = 0,1,10: followed by one byte, ends sequence
xx = 02: followed by two bytes
xx = 09: no data bytes
xx = 0B: followed by two words
xx = 0C: followed by two words and a byte
xx = 0D: followed by two words
xx = 12: followed by two bytes
xx = 21..7F: it's a filename string, zero-terminated, ends sequence
```

`14 xx`
Sakura, Eden, FromH, Hohoemi, Majokko, Tasogare

- xx = 01:
  followed by a length byte, then an array[length] of bytes. Quite
  likely a reset to zero of the listed global variables.
- xx = 03:
  followed by a style byte. Black out the screen immediately, using
  transition type yy. A list of transition types is found below. Some
  transitions are different when used with 14-03 black out.
  The black-out does not remove any existing graphics; upon the next
  drawing operation, the black-out is automatically removed and all
  graphics that it covered become visible again.
  Try this: display a black overlay, and swipe it into view
  immediately. Set a flag to remember you've done this. Whenever the
  next background, overlay, sprite or animation is loaded/shown, check
  this flag and if it is set, destroy the black overlay before
  loading/showing the graphic to be loaded/shown. The command to
  clear all graphics but the background should also clear this.
- xx = 05:
  unknown... yy is 32 or 39. Probably some kind of graphical effect.

`14 xx`
3sis

Pause for xx desiseconds.

`15 xx`
Eden, FromH, Hohoemi, Majokko, Sakura

Animation handling commands. Animations are slot-based. They are loaded into
slots, and can later be redrawn just by invoking the slot number. If an
animation is loaded into a slot that already has a graphic, and the previous
graphic is visible, the previous graphic must be implicitly removed before
drawing the new one. For example, Majokko does this with blinking animations.

- `15 01 yy [0D aa bb] filename 00`:
  load an animated graphic by the given name into slot yy; the optional bytes
  aa,bb are the image's location offset, so the graphic should be placed at
  (aa * 8, bb) within the viewframe.
- `15 02 yy 0D aa bb filename 00`:
  same as above, except the optional offset is relative to the whole game screen.
- `15 03 yy`:
  display the animation from slot yy.
- `15 04`:
  stop all animations (this occurs often more than once in a row, when
  several animations are present, so it probably is not intended to
  stop all animation with just the single command. However, at all
  occasions stopping all animations seems to be the desired end
  result, so in effect that is the simplest way to implement this.)
- `15 05`:
  probably stop all animations, then clear all animation slots.
- `15 06`:
  unknown...

`15 xx`
Tasogare

Same animation commands but with special handling, see tasogare.md.

`16 xx`
Eden, Hohoemi, Majokko, Sakura, Tasogare

Screen flashes xx times.

`16 xx ...`
Parfait

Unknown... something to do with graphics.
Note that the char sprites are all 440 x 395 (01B8 x 018B).
Note that the viewframe is at 100,77 (64,4D), and is 440x304 (01B8 x 0130).
The first bkg drawn is MB_14.
```
MS_MAK6.OVL: [16 0A 0000 004D 01B8 0130 01 01 01 0000]
             [16 0A 0000 004D 01B8 0130 FF FF FF 0000]
             [16 0A 0000 0000 0280 01E0 FF FF FF 0000]
TITLE.OVL:   [16 0A 0000 004D 01B8 0130 01 01 01 0000]
             [16 0A 0000 004D 01B8 0130 FF FF FF 0000]
             [16 0A 0000 0000 0280 01E0 FF FF FF 0000]
```

`17 xx`
Hohoemi, Majokko, Sakura, Tasogare

Pause for xx desiseconds.

`18 xx`
Hohoemi, Majokko, Sakura, Tasogare

Screen shakes vertically xx times.

`19`
All?

Clear the textbox immediately.

`1E xx`
Deep

Unknown... does something with var xx? Possibly junk code, almost always
after 0B-0A manipulating var 1E.

`20..EF`
All

It's a Shift-JIS character, maybe a string of them! Print it into a textbox!
Characters 0x20..0x7E are basically ASCII, so you could print those in a latin
font. But 0x81..9F and 0xE0..EF have an extra byte after each. Also, there are
linebreaks [0x0A], waitkeys [0x01, Saku 0x08, 3sis 0x09] and pauses [Saku 0x17]
liberally sprinkled in the middle of the text lines.

`F3 xx`
Deep

I have no idea whatsoever.


##### Transition styles:

3sis transitions (tested with the original engine):
- 0: Instant (or fill from top down if on a slow computer)
- 1: Instant, but drawn at 0,0 top corner rather than the game window!
  No transparency. Could be used to change skins on the fly...
- 2: Seems to also be instant at 0,0, but draws the following graphic
     instead! If the graphic in question is the last in the list, crashes...
- 3: Instant? Or a crash.
- 4: Box fill inward from edges. No transparency. Map to 3.
- 5: Spiral fill inward from edges. No transparency. Map to 3.
- 6: Box fill outward from center. Map to 1.
- 7: Sweep from left. Map to 1.
- 8: Sweep from center to top and bottom - opening eyes. Map to 3.
- 9: Noisy fade in. Map to 4.
- A: Interlaced sweep from top and bottom. Map to 3.
- B: Ragged uneven sweep from left. Map to 2.

Tenshitachi Collection 1 uses much the same as 3sis, except:
- 2: Current image drops out, new image drops in. Needs special handling.
- 3: Current image rises out, new image rises in. Needs special handling.
- 7: Sweep from center to left and right. Map to 1.

Season of the Sakura transitions:
- 32: instant, no transition.
- 33: no immediate transition, just a weird flash; upon the next transition
      command, does a swipe from top to bottom?
- 36: square fill from edges to center. Map to 3.
- 37: rectangular spiral swipe. Map to 3.
- 38: square fill from center to edges. Map to 1. Except, for 14-03 black
      out, actually same as 36 - a square fill from edges to center.
- 39: vertical line swipe from mid toward left and right, map to 1. Except
      14-03 black out, a vertical line swipe from left and right to middle.
- 3A: horizontal line swipe from mid toward top and bottom; except, for
      14-03 black out, from top and bottom to middle. Map both to 3.
- 3B: a noisy fade, map to 4. For 14-03 black out, instant.
- 3C: horizontal interlaced lines swipe in from top and bottom. Map to 3.
- 3D: ragged swipe left to right. Map to 2.

SuperSakura transitions:
(I'm only keeping the least tacky, cinematically useful transitions.)
- 0: instant.
- 1: wipe from left.
- 2: ragged wipe from left, old screen melts rightward.
- 3: interlaced wipe from top and bottom.
- 4: crossfade.


##### On variables:

Variables appear to be 16-bit signed integers. There are local and global
variables, 256 of each.

0B handles local variables, which work like asm registers for local
calculations and forgettable script progression flags. Local variables are
reset to 0 whenever initialising a new script.

0C handles global variables, which track the player's misdeeds throughout
the game. It may make sense to unify both kinds of variables in an engine
implementation, for example making global variables 256..511 so they don't
conflict.

The low-number local variables control branching from user choices.
Each choice combo has a variable by the same ID number; when the action is
picked, the game jumps to the address specified by the variable. If the
variable is zero, the first address is picked.

To clarify, an example from The Three Sisters' Story, SK_102.OVL:
- Action combination 3 is Talk + Yuki.
- It has four possible results, found at addresses 031F, 065F, 07C7 and 088A.
- When the player selects Talk + Yuki, local variable 3 starts at zero.
- The engine jumps to code address 031F. At the end of that segment, there is
  a command to set variable 3 to one. At next Talk + Yuki, the engine will jump
  to code address 065F.


##### Stuff that may need to be fixed manually:

[3sis]
SK_103 has a result-goto into the middle of a string.
SK_406 tries to draw a non-existent graphic at script offset 0x055F. This only
appears to happen in the English version.

[Sakura]
CS305 and CS306 have the scene of Seia sleepwalking. The music used is SK_01.
However, SK_01.M appears to not exist, and is replaced with SK_20 in all
English versions. The Memorial Pack version has the first song as an MP3, and
it is apparently meant to be Seia's theme. Now, SK_20 is the scary tune, and
while it works for sudden sleepwalking, Seia's theme is used at another
moment in the game as well (CS803_4), and replacing that with the scary music
is just wrong. So, best rip SK_01 from the PC-98 version.

[AngelsCollection2] S2_007 at @02FA has a strange character (Shift-JIS 8757).
The original engine draws that as a weird double-slash; it seems like one of
those things where a character namedrops a brand but replaces a syllable or
two with a censorship symbol.

[Eden] JO2101 defines two options: 誰かと話す (Talk to someone) and
街に出る (go to town). There's no indication of anything hidden or restricted,
but the game only shows the first option. It doesn't seem like there's any
choice you can make until this point in the game to make the choice appear.
The second choice is attached to v2, but that's a local variable reset to 0 on
script entry. v2 is used in jo1105a briefly, but whether it gets poked there
or not makes no difference here.
Must add hack to hide this choice in the converted script.

JO3404x repeat an oddity; ET03 is drawn, then she is removed, and at
the next 13-03 command without a filename, she is mysteriously restored.
This is not how the other games, and Eden at other points, handle graphic
stack pushing and popping...

[Majokko] MP8702, MP8503D and MP8102 have character 0x14 in the middle of
a graphic filename. Doesn't make sense except as a scripting error.
Also, MP0001 has graphic file names without a loading command. The context
suggests they should be drawn anyway...

[Tenkousei] TEN_S108 tries to draw graphic index 2, whose definition is in
the header, but the graphics list forgets to point to its definition.
Moreover, the graphic is TT_06, a palette-squeezed version of TT_02, with
an incorrect animation. Best to make the script draw TT_02 instead.

Also, the game should start with an introduction sequence before a fortune
teller. The script for this is in TK.EXE from 0x15347. It's mostly text, with
a few commands in between. The sequence music is TEN020.
- 0: return from call or exit
- 1: waitkey
- 2: draw OP_2 and go over 10 dual-choice questions, from 0x14FC6 onward. After
  all questions are answered, resume main.
- 3: depending on the user's answers, call one of three addresses: 0x151C8,
  0x15241, or 0x152C4. These are the fortune-teller's creepy prediction.
- 6: draw an image. There are four of these commands, and in order they should
  draw: TB_000, OP_1, OP_1, TB_000

The player's responses go in global variables 1..10. The left response is
value 0; the right is value 1.
