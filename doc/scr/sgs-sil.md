Silence/Sogna SGS SIL bytecode
------------------------------

The SIL variant used in Guynarock 2 is already well-described by Alpharobo:
https://alpharobo.neocities.org/gyna2_data

Words are x86-native little-endian. Script variables are 8 bits wide.

Looking at the various if-then-goto statements in SIL bytecode, it feels like the
scripting language must have had high-level if-then blocks and full case-switch
statements. Even though the compiled bytecode looks like a spaghetti mess of jumps,
the pre-compiled script was probably quite user-friendly.


### Bytecode commands

The correct interpretation will vary slightly between engine versions.
This is currently targeting Lime 1, SGS 386 1.00.

`00 vv xx options...`

Define option strings, print them, and wait for the user to choose one.
The selected option's identifying number goes in variable vv.
The number of first-order choices is xx.

Identifying number = first-order index * 10, plus sub-choice index if any.
Both indexes are 1-based.

For each first-order choice, there's an option structure:
```
byte - number of sub-choices for this, plus 1
byte - associated variable number; if variable != 0 and its value == 0, hide this choice
char[] - choice text as a JIS string, null-terminated
byte[] - all sub-choices for this first-order choice
```

For each sub-choice, there's this:
```
byte - associated variable number; if variable != 0 and its value == 0, hide this choice
char[] - sub-choice text as a JIS string, null-terminated
```

Note that some games like the Lime series primarily use a graphical method of user options,
which uses a complex combination of drawing commands and 3E user input.

`01 xx yy hh ww`

Set the menu location on screen.

`02 string 00`

Print text, null-terminated string. The text is JIS-encoded, not Shift-JIS.

ASCII-like escape codes may appear in the strings:
- `!0`: Clear textbox
- `!o`: Linebreak
- `!t`: Pause for an unknown length of time? Used between dialogue lines...
- `!p#X`: If X == "0", prints text at normal slow speed; else prints instantly
- `!s#X`: Print string variable X
- `!w#X`: Set text color to X

`03 xx yy ww hh`

Set the main text box location.

`04 xxxx`

Set text skipping speed, smaller number is faster?

`05 aaaa`

Jump to offset aaaa in the current script.

`06 string 00`

Load the named SIL script file. And execute it?

`07 xx string 00`

Load the named SZH file, usually a graphic, in slot xx, but not displayed yet.
The filename may start with a drive letter and colon.

`08 xx`

Draw the graphic from slot xx.

`09 xx ff`

Transition to or from the graphic from slot xx. Effect type is ff:
- 0: fade from black
- 1: fade to black
- 2: fade to white
- 3: fade from white

`0A xx`

Unknown, something to do with transitions.

`0B xx`

Unknown.

`0C xx ff tttt cc ...`

Palette effect, starting from the palette used by the graphic in slot xx.
Effect type is ff:
- 0: instant
- 1: fade
- 2: flash
- 9: instant
- 10: keep cycling palette between normal and new while the script continues
The effect duration is tttt frames, where each is about 20 msec.
The number of colors being modified is cc.
This is followed by 4 bytes for each cc: index, green, red, blue.

`0D xxxx`

Set animation frame delay in frames, each about 20 msec.

`0E`

Stop music.

`0F string 00`

Guynarock2: Load the named PMD music file, but don't play it yet.
Lime: Load the named music file and play it right away.

`10`

Play the loaded music.

`11 xx`

Play sound effect xx.

`12 vv xx aaaa`

If variable vv == value xx, jump to aaaa in this script.

`13 vv xx aaaa`

If variable vv != value xx, jump to aaaa in this script.

`14 vv`

Let accumulator := value of variable vv.

`15 xx aaaa`

If accumulator != value xx, jump to aaaa in this script.

`16 vv xx`

Let variable vv := value xx.

`17 aaaa`

On user interrupt, jump to aaaa in this script.

`18`

Wait for a keypress.

`19 tttt`

Sleep for tttt frames, where each frame is about 20 msec.

`1A`

Unknown.

`1B xx string 00`

Error message...

`1C xxxx`

Set screen position for following graphics. This is the coordinate for the top left corner
at 640x400 resolution.
- `X = (xxxx MOD 80) * 8`
- `Y = xxxx DIV 80`

`1D xxxx`

Set palette transition delay, counted as frames, each about 20 msec.

`1E xxxx ww hhhh`

Clear an area of the screen. Multiply width by 8.

`1F xx ff hhhh tttt`

Display image from slot xx and pan vertically, using pan type ff.
Pan direction is `ff & 1`, where 0 = pan from bottom of image to top, 1 = top to bottom.
Pan step size in pixel rows is `(ff >> 1) + 1`. Larger step size pans faster.
Pan window height is hhhh. Delay per pan step is tttt frames, each about 20 msec.
Total pan duration is `(graphic height - pan window height) / step size * delay per step`.

`20 string 00`

Load specified save file.

`21 string 00`

Save game as specified file.

`22 xx yy`

Bitwise OR variable xx with value yy.

`23 aaaa`

Call offset aaaa in the current script.

`24`

Return from call.

`25`

Remove the menu.

`26 xx string 00`

Set string variable A + `xx` to the specified JIS string, usually a character name.
This may contain escape codes such as color changes that need to be applied whenever
the string is printed out.

`27 xxxx`

Set text display delay, in frames, each about 20 msec.

`28 xx`

Unknown.

`2A xx ff zzzz`

Draws frame ff from the graphic in slot xx. Implicit delay after drawing the frame
may be zzzz, in units of about 20 msec.

`2B gg rr bb`

Reset all palette colors to this color?

`2C xx`

Guynarock 2: Show animation from slot xx, keep looping it while the script continues?

`2C xx aa <frames...>`

Lime 1: Show animation from slot xx. Animation type is aa:
- 0 = direct one-off, show specified sequence of frames, stop script execution until done
- 1 = loop sequence continuously, while script continues

The animation sequence is given as an array of bytes. Each array item has a byte for
the frame index; if this is 0, that's the end, and there is no second byte.
Otherwise display that frame index from the graphic in slot xx, read another byte,
and delay for that many frames (about 20ms each).

Example: `2C 0F 00: 01 04, 02 04, 01 09, 02 00, 00`

This shows the graphic from slot 0F, type 00 = one-off animation.
- Draw the 1st frame, wait 4*20 = 80 msecs
- Draw the 2nd frame, wait 4*20 = 80 msecs
- Draw the 1st frame again, wait 9*20 = 180 msecs
- Draw the 2nd frame again, no wait
- The next frame index is 0, so that's all.

`2D`

Unknown.

`2E`

Restore normal palette or stop palette cycling.

`31 xxxx ww hhhh cc`

Draw a rectangle on screen with color cc, typically for colored flashes.

`39`

Wait for the current animation sequence to end, if it's still ongoing.

`3E vv`

Get user input key press, save key code in variable vv.
- Down key: 4
- Up key: 8
- Esc: 32
- Space/Enter: 128
For this purpose, a mouse cursor movement down or up produces a down or up keypress.

`40 vv ww`

Probably let variable vv := variable ww.

`41 xx`

Unknown.

`42`

Unknown.

`43 xx yy aaaa`

Unknown. Some conditional jump to offset aaaa in this script?

`44 xx yy aaaa`

Conditional jump to offset aaaa in this script. Unknown condition...

`46 xx`

Increment variable xx by one.

`47`

Reset all tracking variables to 0? These are variables 0x64 to 0x6E, maybe more.

`4C`

Unknown.

`4D aaaa`

Conditional jump to aaaa under some unknown condition.

`4E xx`

Unknown.

`4F`

Wait for a keypress, fullscreen?..

`50 xx vv zz aa`

Draws a frame from graphic slot xx, 1-based frame index indicated by variable vv.
Unknown what zz and aa are for, always 0?

`51 vv`

Decrement variable vv by 1.

`52 vv xx`

Add xx to variable vv.

`54 xx`

Something to do with animation frames or screen buffer flipping...
Flip between the last two drawn frames, delay xx 20msec frames between flips?
The effect keeps going while text is displayed until a 54-00 command.

`55`

Unknown.

`56 string 00`

Possibly loading a completion state?

`57 string 00`

Something to do with saving the game, maybe end of game completion save?

`58 xxxx`

Slow dissolve transition to the graphic named just previously. The parameter is duration?..

`5A xx`

Unknown.
