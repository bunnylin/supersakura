Fairy Dust SCP bytecode
-----------------------

These files use a simple encryption: XOR every byte with 0xAA.
Thank to Alatalo for discovering this!

After decrypting, these are plaintext scripts, with abbreviated and symbolic commands,
indentation, and curly brace blocks.


##### Variables

These are specified with `%` percent-signs and an index number, eg. `%123`.

A string variable is prefixed with a `$` dollar-sign, eg. `$321`.

There is a `%Z` variable as well, which has the selected multiple choice index.

There is a `%F` variable, which might hold true if a queried floppy index is available.

A double-percent is some kind of a different variable, eg. `%%1`. Double-deref?

Variables can be set with `&` ampersand, and used in `?` conditionals.

Variable numbers can go above 255, but probably not far into the thousands.


##### Expressions

Expressions can be defined using numerals, variables, and operators `+ - * /`.

For example: `& %68 = %68 + 1` to add 1 to variable 68.

Conditional comparisons seem to treat 0 as true, non-zero for false.

For example: `? %20 - 5 { }` - if variable 20 = 5 then do something.


##### Commands

It seems you can only have one command on each line. If a line doesn't start with
any of the below, it's probably a string to be printed. Each file ends with 0x1A.

`!<script>`

Jump to a new script. If script is undefined, return from call.

`!!<script>`

Probably call a new script, returnable?

`#CDb`

Unknown.

`#CL<graphic>`

Load the specified character sprite.

`#CSb`

Unknown.

`#EG`

Unknown.

`#EM`

Unknown.

`#G<graphic>`

Load the specified large graphic.

`#HC<filename>`

Check for a file presence. Set %F to 0 if found, non-zero if not.
This is used to check if a disk ID file is found, or if an `end.dat` exists,
and see if any savegames exist.

`#I<number>`

Unknown. Transition of some sort for fresh graphics?

`#ID<letter>`

Unknown.

`#INIT`

Probably resets the engine state.

`#K<number>`

Unknown, in vicinity of floppy change request. Wait for keypress?

`#KC0`

Wait for a keypress.

`KN<number>`

Probably wait a given number of centiseconds, but stop waiting if the user presses a key.

`#L<graphic>`

Load the specified in-viewframe graphic.

`#MAP<number>`

Unknown.

`#N<number>`

Unknown.

`#ND`

Ends a multiple choice?..

`#ND0`

Begins a multiple choice?

`#O<number>`

Unknown, used in the title sequence.

`#OP`

Unknown, used in the title sequence.

`#P<song>`

Play the specified music file.

`#SB<number>`

Unknown.

`#SE<number>`

Wait for the player to select one of so many printed choices.
The selected index is saved in variable %Z.

`#T<number>`

Unknown.

`#WC`

Unknown.

`#WO`

Unknown.

`$<index>`

Print a string variable.

`& <variable> = <expression>`

Set variable to the value of the expression, eg. `&%3 = %3 - 1` or `&$3 = "Banana"`.

`? <expression> { <statements> } ? { <statements> }`

If the expression is 0 = true, execute the following block of statements.
If there's an extra question mark with no expression after the then-block,
it marks an else-block, to be executed if the expression wasn't true.

`?? <variable> { :<value> { <statements> } ... }`

Case switch using the value of the given variable. 

`[<number>`

Perhaps a for-loop? There's a concluding square bracket later.

`><label>`

The greater-than sign is a jump command, to the given label.

`<alphanumerics>:`

Indicates the start of a label. As plain ASCII, this is distinct from printable Shift-JIS.
