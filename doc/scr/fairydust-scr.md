Fairy Dust SCR bytecode
-----------------------

This script format has been thoroughly investigated by Alatalo. Thank you!
Variations of this are used in Koukan Nikki 1 and 2, and Ma Doll.

Ma Doll's Windows port uses the same scripting format as the PC98 version.

File layout:
- Header
- Script commands section
- Multiple choice definitions section
- Strings section

Header:
```
char[4] - "SCR0" or "SCR1"; omitted in Koukan Nikki 1
uint16 - number of commands in this file
uint16 - number of multiple choice definitions in this file; omitted in MaDoll SCR0 files
```

The script commands section follows immediately, where every command is always
6 bytes long, however many of those bytes are actually used by the command's parameters.
Total section size = number of commands * 6.

The multiple choice definitions section follows immediately, except in MaDoll where it
is omitted. The size of each choice definition depends on the game and file format:
- Koukan Nikki 1: 6 bytes each, except 12 bytes each in `meta.scr`
- Koukan Nikki 2: 6 bytes each if it's SCR1, or 10 bytes each if it's SCR0
- MaDoll: 6 bytes if it's SCR1, not present if it's SCR0

The strings section follows immediately, and runs to the end of the file. This starts
with a uint16, the byte size of the section. The rest is a series of null-terminated
strings, all encrypted with XOR 0x7F.

The strings are generally in Shift-JIS, but, if the string is being printed in a textbox,
some single-byte characters need to be fed through an extra lookup table. (If the string
is a filename or something, then don't do this mapping.) The table has single-byte
mappings for the most common double-byte characters. The lookup table can be extracted
from the game executable. For reference, it starts with `81 40 81 49 22 00 81 94`.

- MaDoll: `mdmain.exe`
  * Input bytes 0x20..7F are found at 0xA05B..A11A
  * Input bytes 0xA1..DE are found at 0xA125..A1A0
- MaDoll_w: `madou32.exe`
  * Input bytes 0x20..7F are found at 0x2C424..2C4E3
  * Input bytes 0xA1..DE are found at 0x2C526..2C5A1

If the second byte of the looked-up value is 0, then just use the first byte as
a single-byte shift-jis character after all.

There's a further table of constant strings, typically character names, which can
be printed as game text with a short special code. These are also found in the exe.

- MaDoll: `mdmain.exe` from 0x9E59, 27 null-terminated strings
- MaDoll_w: `madou32.exe` from 0x2CC10, 27 null-terminated strings

Additional special characters:
- 0x01: Wait for a keypress
- 0x02 aa: Print constant string <aa>
- 0x0A: Line break
- 0xEB AC: Heart emoticon
- 0xEB AE: Sweat drop
- 0xEB AF: Double exclamation mark
- 0xEC 8B: Musical note

Graphics transition rules:
- If whiteout or blackout is in effect, no other transition applies
- 10-xx applies an immediate transition
- 09-xx applies an immediate fade to black and back if drawing slot 0,
  and the currently visible graphics show only slot 0

Bytecode commands:

`00 vv xxxx`

Set variable vv to value xxxx.

`01 vv xxxx`

Increase variable vv by xxxx.

`02 cc xx gg aaaa`

Prints a string from the string table, at offset aaaa from the start of the table.
cc is the character id, xx the animation id, gg the graphic id?

`03`

Clear textbox?

`04 00 aa bb`

Enable all animations attached to current graphics. The animation set is referenced by
the index number aa. If bb == 0, the animation doesn't loop.

`05 00 aa bb`

Seems to do the same as 04. Perhaps this applies an extra 2-3 second delay before
the animation sequence wraps around?

`06 00 aa`

Stop animation set with index number aa.

`07 00 aa`

Play song index aa. The number is 1-based, but aa == 0 can also happen, some special value?

`08`

Fade out and stop music?

`09 ii aaaa`

Load this new graphic in slot ii, clearing any graphic that was there before.
The graphic name is a null-terminated string found at address aaaa.
Typically slot 0 is the background; possible sprites or overlays use a non-zero slot.

`0A`

Disable whiteout on the next section change, or remove immediately?
Fades from white back to normal view.

`0B`

Enable whiteout on the next section change. (Or maybe, immediately?)
This fades the game view to white, hiding graphic changes until the whiteout is removed.

`0E`

Disable forced fade to black for the next shown graphic?

`0F`

Enable forced fade to black for the next shown graphic?

`10 ii`

Clear graphics from slot ii, apply fade transition. But, for slot 0, remove all graphics.
Also, if there's no graphic in said slot, remove all graphics too?

`11 00 xx`

Pause for xx seconds.

`13 00 aaaa`

Jump to new section, address aaaa. Automatically clears existing graphics,
and usually fades to black before the next section's new graphics get shown.
The exact conditions of when it should do a full fade to black and when
just a crossfade are not understood well...

`14 vv xxxx aaaa`

If variable vv == xxxx, jump to aaaa.

`15 vv xxxx aaaa`

If variable vv != xxxx, jump to aaaa.

`16 vv xxxx aaaa`

If variable vv > xxxx, jump to aaaa.

`17 vv xxxx aaaa`

If variable vv < xxxx, jump to aaaa.

`1B 00 iiii cccc`

Show multiple choices and let the player pick one. The choices are in the
script's multiple choice definitions block. Display choices from choice
index iiii to (iiii + cccc - 1). Each choice has an associated jump address.
Jump directly to the address when the user has selected a choice.

Additionally, it seems every available choice can only be selected once,
and this is tracked implicitly. If a previously-selected choice is offered
to the player again, it must be greyed-out and unselectable.

`1D 00 ssss`

Returnable call to another script. The script name is a null-terminated string
at address ssss.

`1E`

Screen bash, a quick single bump upward of about 16 pixels.

`1F`

Flash white briefly?

`20`

Special effect: shrink game window to letterbox on a character closeup's eyes.
