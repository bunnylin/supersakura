MDR scripts
-----------

Used in some Panda House/Melody games.


### Night Slave MDR

These are already thoroughly researched by ValleyBell:
https://github.com/ValleyBell/PC98VNResearch/tree/master/panda_max

Since these are only used for relatively brief cutscenes, the script format
uses very straightforward bytecode.

Commands: (words are x86-native little-endian)

`0000`

Exit script.

`0001 <filename> 00`

Load the specified image.

`0002 xxxx`

Execute a visual effect, where xxxx is:
- 1: palette thing?
- 2: fade from black to show the current loaded image
- 7: restore stashed buffer?
- 8: display loaded image instantly
- 9: stash the current image buffer?
- 10: fade from black
- 11: fade to black

`0003 <string> 00 00`

Print the string in the main text box, wait for keypress, clear the box.
The string must have an even byte length, even if it uses single-byte characters,
so there may be an extra null byte at the end for padding, plus the double-null.

`0004`

Wait for a keypress.

`0005 <filename> 00`

Load and play the specified song.

`0006`

Fade out music.

`0007 xxxx`

Wait xxxx time units, maybe 20ms each?

`0008 string`

Another text printer?


CMF scripts
-----------

Used in Night Slave for mission scripting.

Already well-documented by ValleyBell:
https://github.com/ValleyBell/PC98VNResearch/blob/master/panda_max/NightSlave-CMF.txt

There are two different kinds of bytecode in CMF files... Each CMF file begins with
the first kind of bytecodes, but it can call further into the file, where a second kind
of bytecode is used.

Header: (words are x86-native little-endian)
```
uint16 - byte size of primary bytecode
byte[] - primary bytecode
```


##### Commands, first type:

`0000`

Exit script.

`0001 xxxx yyyy`

Set position to xxxx,yyyy.

`0002 xxxx yyyy`

Move position by xxxx,yyyy.

`0003 iiii jjjj aaaa`

If jjjj = 0, set jjjj := iiii;
decrement jjjj;
if jjjj = 0, jump to aaaa;
otherwise fall through?

`0007 <filename> 00 iiii`

Load the specified HDF graphic in slot iiii.

`0008 iiii`

Release graphic slot iiii.

`0009 iiii aaaa xxxx yyyy bbbb`

Show a sprite from slot iiii at xxxx,yyyy?

`000A aaaa`

Print text, null-terminated string from address aaaa in this file.

`000C xxxx yyyy aaaa`

Show an enemy sprite?

`000D <filename> 00`

Call the specified MDF script.

`000E`

Show cockpit?

`000F`

Init level?

`0010`

Unload all graphics.

`0011 <filename> 00`..`0013 <filename> 00`

Load a graphic file, can be SDF, RDF, or TDF.

`0014 <filename> 00`

Load and play the specified song.

`0015`

Unknown.

`0016 xxxx yyyy`

Set position?

`0017 aaaa`

Unknown. Aaaa is not an address in this file, at least.

`0018 aaaa`

Unknown.

`0019 aaaa bbbb`

Unknown. The parameters are not addresses in this file, and can be equal.

`001A aaaa bbbb cccc dddd`

Unknown.

`001B aaaa`

Fade to black? There are 48 uint16's at the address aaaa in the same file, presumably
palette data.

`001C aaaa`

Returnable call to address aaaa in this file. The bytecode there is of the second type.

`001D aaaa`

Load a palette?

`0020 <filename> 00`

Call the specified executable file.

`0021 aaaa`

Show the mission end screen.

`0022`

Unknown.

`0023 aaaa`

Unknown. Aaaa is a low number, not an address in this file.

`0024 aaaa`

Set shield state?


##### Commands, second type:

`0003 aaaa`

Jump to aaaa in this file.

`0005 dddd`

Delay for dddd time units, maybe 20 msec each?

`0008 aaaa`

Select sprite frames from aaaa in this file.

`000A ...`

Some kind of jump array of addresses?

`0010 xxxx yyyy`

Move map by xxxx,yyyy.

`0011 xxxx yyyy`

Set some sprite position?

`0022 dddd`

Delay for dddd time units again?

`0026 dddd`

Delay for dddd time units again?

`002B iiii`

Set object to use sprite from slot iiii.

`003A dddd <string> 00 00`

Display the null-terminated string, then delay for dddd time units. The string must have
an even number of bytes, so if there are single-byte characters, it may need an extra
padding null at the end.

`0043 iiii dddd`

Jump randomly at iiii percent probability? To what address?..

`0047 xxxx yyyy`

Move object by xxxx,yyyy.

`004F aaaa <string> 00 00`

Display the null-terminated string, freeze gameplay until the user presses a key.
The string must have an even number of bytes as usual, so may beed an extra null at the end.

`005B <filename> 00`

Load and play the specified song.
