EVE engine
----------

This was an early Windows engine that was licensed to a few different developers.
The engine was supposedly a Multimedia Script Processor by the Neural company.

Windows games using this:
- Tenshitachi no Hohoemi (JAST)
- Majokko Paradise (Tiare)
- Cat's Part 1 (Cat's Pro)
- Nova (Cat's Pro)
- Kurayami (Melody)

Some information on this engine can be found at the Kurayami fan translation site:
https://www.kurayamigame.com/

