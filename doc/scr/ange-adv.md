Ange ADV bytecode
-----------------

These are uncompressed bytecode, but include text encryption.

Whenever you encounter an encrypted string, which start with the char "X" 0x58, decrypt:
```
key := 0x58
Repeat
	value := (next input byte) XOR key
	key := (key + value) & 0xFF
	Output value
Until value == 0
```

Special escape codes can appear in strings:
- `\0g`
- `\f0`
- `\k`: Wait for a keypress
- `\n`: Line break
- `\pxx,yyy;`: Print start offset?
- `\w0;`
- `\z#;`: Print emoji, 0 = double-exclamation mark
- `\*`: Clear textbox
- `\]`

Trying to use that algorithm from every byte at the start of School Festival
s001.adv reveals the first lines printed in the game. There's nothing
recognisably encrypted the same way before those lines, unless ":OF" at 0x22
is somehow meaningful. More likely it's unencrypted bytecode.

School Festival has a `grp_tbl.sys`, which looks like a list of all graphic
filenames, each followed by a single byte indicating which disk the file is on.
Might be index-referenced from bytecode to avoid writing filenames?
Other `sys` files for each resource type contain a similar list of disk lookups.
Except `plt_tbl.sys` contains an array of 48-byte palettes.

The bytecode feels stack-based; there are repeated 82-xx commands in various
groups, which could be pushes into a stack. Eventually a single-opcode command
pops things from the stack and acts on them?

There's an unreasonable amount of opcodes compared to what actually happens in-game.
In the opening scenes of School Festival, s001, from the text print after which the
school bell rings, to the text print after the bell sound concludes, there are
18 single-byte codes, 28 multi-byte codes, and 9 jumps including 3 that appear
recursive. There's no visual change in that period other than the "bing bong" print.

ADVIZ 2.00 in Kaiketsu Nikki looks much the same as ADVIZ 3.00 in School Festival.
Nikki uses the sequence `C1-FFFC, C2-xx, 84-16, 1C, 88 > F2ED` between text prints
to draw character portrait index xx. It's redrawn between every print even if the
same portrait remains on screen. But if there's a longer bunch of code between
prints, the portrait may change but there's no C1 or C2.

ADVIZ 1.10 in Like seems to work on the same basis, but there are a lot of 00 bytes
in the bytecode, implying different parameter handling, possibly with more wasteful
always-word argument sizes. The text encryption as described above is also used here,
but it's tied to bytecode 0x30 instead of 0x58.

Commands: (words are x86-native little-endian)
```
10 - reset something? 84 jump landing point
1A - something to do with execution pointer or stack?
1C - push script execution pointer + a few bytes on stack, so 1E can return to it?
1E - return?
30 - ?
34 - ?
35 - ?
36 - wait for player to select an icon? preceded by 6 immediates for solo protag icon
37 - ?
3A - ?
44 - ?
4F - ?
55 - ?
58 <encrypted string> - text output
5A - begin multiple icon choice definition?
5F - ?
64 - ?
6A - ?
6E - ?
71 - clear textbox or remove "next msg" icon? found at start of scripts, also after prints
73 - ?
80 xxxx - immediate 16-bit signed value?
81 xxxx - immediate 16-bit signed value?
82 xx - immediate 8-bit value?
84 04 - ?
84 06 - ?
84 09 - ?
84 0F aaaa - pop imm value, pop var ref; if var value > imm, jump to aaaa
	but it can also be preceded by two immediates in stack?..
84 16 - pop imm value, pop var ref; var := imm value
88 aaaa - unconditional jump or call to aaaa
90 vvvv - ?
97 - ?
A0 xxxx - immediate 16-bit signed value?
A6 - ?
C0 xxxx - immediate 16-bit unsigned value? or maybe OR by xxxx?
C1 xxxx - immediate 16-bit signed value? or maybe AND by xxxx?
C2 xx - push to stack: 8-bit immediate value
C4 xx - immediate 8-bit value?
D0 vvvv - push to stack: reference to variable vvvv
E0 xxxx - immediate 16-bit signed value? or AND by xxxx?
E1 - ?
FF - ? last command in s001
```

First icon choice:
@053F:
5A
36 (0, 8, $FF, $D, 0, $3E)
88 > 0552

@0552:
34

@0CFF:
1C
88 > 3BC7
10
84-0F (1, $FFFC) > 0D33
64
...
1A
88 > 100C

@0FDB:
10
84-0F ($8000, $FFFC) > 100C
64
...
1A
88 > 2E17

@100C: choice outcomes

s001 start:
```
71
82 17, 90 (39 01), D0 (40 01), C2 [17], 84 16, 44
82 02, 82 00, 90 (3B 01), 6A
82 00, 82 0A, 82 FF, 44
82 00, 82 00, 90 (3B 01), 3A
82 0A, 73
82 06, 90 (3B 01), 44
82 03, 82 01, 90 (3B 01), 73
82 0E, 90 (32 01), 1C
88 [8E 37], 10
D0 (4A 01), C2 [00], 84 0F [DD 01], 1C
88 [C7 3B]
58 encrypted text ...
```
