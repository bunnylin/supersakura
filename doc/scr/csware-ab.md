AB bytecode
-----------

Used by C's Ware and Aaru.

On PC98, the AB scripts are compressed. Standard LZSS, 17-byte offset.
On Windows, the AB scripts come without compression, bundled in an FL1 archive.

The commands are little-endian uint16 values, which don't have to be word-aligned.

`00 string`

Print the specified string as normal game text. Wait for a keypress.
The text may have `\N` newlines, usually but not exclusively after a dialogue title;
the rest of the string gets linewrapped at runtime.
There may be a `\Gxxx.` command, which immediately draws the specified graphic.
This is used to draw dialogue portraits. The graphic name string can be empty,
which presumably clears the portrait.

`01 aaaa xxxx string`

Define a multiple-choice option, using the given string for the option text, and
aaaa as the jump address if the user selects it. Xxxx is unknown, often but not
always the text's byte length.
In the Windows version of the engine, the jump address may be a dword `aaaaxxxx`.

`02 aaaa`

Unconditional jump to script offset aaaa.
In the Windows version of the engine, the jump address may be a dword `aaaaaaaa`.

`03 cccc vvvv zzzz`

Set multiple flags?

`04 vvvv op xxxx`

Use value xxxx on variable vvvv, depending on operator character:
- `=` let variable vvvv := xxxx
- `+` add xxxx to variable vvvv
- `-` subtract xxxx from variable vvvv

`05`

Wait for a keypress.

`06`

Begin choice definitions. Reset choices?

`07`

Let the user choose from defined options, jump to the associated choice address.

`0A`

Unknown, choice-related?

`0E xxxx yyyy` or maybe no arguments at all?

Unknown.

`0F xxxx yyyy`

Unknown. Section marker?

`10 vvvv op xxxx aaaa`

Conditional jump to offset aaaa, if variable vvvv compares truly to value xxxx.
Comparison operator characters: `=` for equals, `{` for less than, `}` for greater than.
In the Windows version of the engine, the jump address may be a dword `aaaaaaaa`.

`11 tttt`

Sleep for tttt centiseconds?

`13 string`

Show the specified graphic. Could be a viewframe, could be a normal background.
If it's a background, clear any currently visible sprites?

`14 xxxx`

Flush graphics to screen? 01 = blinds transition, 03 = interlaced, 04 = fade in

`15 xxxx`

Some graphics thing, always after a sprite show command? Xxxx is always 0?

`16 string`

Show the specified graphic as a sprite.

`17 xxxx yyyy`

Fade to black, or maybe fade to color xxxx. Transition type yyyy, same values as `14`.

`18 string`

Jump to the start of the specified script file.

`19`

Game over, back to main menu.

`1E string`

Maybe play the specified song?

`20 xxxx`

Unknown.

`24 xxxx`

Some graphics thing, always before a graphics show command. If it'll be a background,
xxxx is 0 or 0x07FE; if it's a sprite, 0xF801.

`26 string`

Play the specified music file.

`27`

Unknown, something with music?

`28`

Stop music quickly.

`29`

Begin 5-second music fadeout, non-blocking.

`2D xxxx`

Something to do with viewframe and fullscreen mode switching.
The parameter is a word in ZestFan_w, but only a byte in ZestFan98!

`30 xxxx yyyy aaaa bbbb`

The values are screen coordinates, some kind of visual effect, screen fill?
This appears a couple times in RoseBlood_w and ZestFan_w, each time followed by
graphics in a 640x480 context, either the end credits or title sequence.
In ZestFan98, this only has 3 parameter words. If they are `0, 0x5000, 0x190`
then the game is switching to a fullscreen mode.

`31 xxxx`

Unlock asset xxxx? Or just ending xxxx.
Note that in the PC98 engine this is a byte argument `xx`, not uint16.
ZestFan: this triggers a game end; x = 0 for sad end, x = 1 for happy end.
Both endings display the SR graphic, and after waitkey return to the main menu.

`32`

Stop using the night palette, resume normal palette.

`33`

Switch to the night palette. On PC98, this applies to the graphic shown immediately
prior to this command. On Windows, this applies to the next displayed graphic.
- RoseBlood98: palette colors 10..14 are set to colors ranging RGB 223 to RGB 557,
  which means blue is halved, red and green reduced by 2/3. Full white remains white.
- RoseBlood_w: every pixel has red and green values cut by 1/3, blue unchanged.
- ZestFan98: all colors have red reduced by 1/3, green and blue unchanged.
- ZestFan_w: every pixel has red and green reduced by 1/4, blue unchanged.

`46 string`

ZestFan_w: Draw the specified graphic as a viewframe.

`47 string`

Show the specified graphic in-viewframe, probably a background or event picture.
Clear any currently visible sprites?

`48 string`

Show the specified graphic in-viewframe, probably an event picture.

`49`

Show a title sequence.
- RoseBlood98: not used
- RoseBlood_w: rapidly show each letter of "ROSEBLOOD" in isolation in the center of the
  screen accompanied by a gunshot sound for each letter; play the `title2` animation
  and play song `rose001`, wait for a keypress.

Notes:
- RoseBlood98 has a broken string in Rose_05; unclear if my error, corrupted file, or
  script bug. There's a `00 87` at 0x354; should be `83 87`.
- RoseBlood98 broken jump to 0xD3 in Rose_06, should be 0x1D3.
- RoseBlood98 broken jump in Rose_10B, `E2 00 00 00` at 0x2C9; should be `E2 02 01 00`.
- The Windows port of Rose Blood seems to have fixed all these.
