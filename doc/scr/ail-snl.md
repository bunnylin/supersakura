AIL SNL bytecode
----------------

These are found in SALL*.snl files. After LZSS-unpacking, these start with
four 0-bytes, followed by uint16 section sizes.

File layout:
- Header, always 10 bytes
- Jump table, a variable-size array of uint16's
- Script code
- String table

Header:
```
uint32 - always 0
uint16 - jump table byte size (divide by 2 to get number of jump entries)
uint16 - code byte size
uint16 - string table byte size
```

Commands:
00 01 FF 00 aaaa xxxx FF 01 bbbb yyyy - print text; dialogue title from
	string table offset aaaa, and main text from bbbb; xxxx and yyyy
	could set indentation?
00 0A ... - something to do with choices
00 0C FF xx - show background xx, maybe with transition
00 0D FF xx FF 01 00 01 - fade to black, show event graphic xx, fade in
00 0E FF ii FF xx FF yy FF zz - show sprite ii, screen position xx/yy/zz?
00 17 FF xx - fade in?
00 18 FF xx - fade to black?
00 22 FF xx - play song xx (one-based numbering)
00 2E FF xx - show viewframe

Kyouhaku sall_0.snl:
// screen is all black, only message box visible
04 01, 7F FF, 80 00,
00 18, FF 02,
00 16, FF 00,
00 22, FF 02, // play 2nd song from soundtrack
00 17, FF 02,
00 01,
FF 00, 00 00, 00 00, "Ryosuke" // first string printed in-game
FF 01, 00 0A, 00 05,
00 0D, FF 4C, FF 01, // show gall1a.12, fade in
00 01,
FF 00, 00 38, 00 00, "Asuka"
FF 01, 00 44, 00 05,
00 0D, FF 4D, FF 01, // fade to black, show gall1a.13, fade in
00 01,
FF 00, 00 00, 00 00, "Ryosuke"
FF 01, 00 4E, 00 05,
00 0D, FF 4C, FF 01, // fade to black, show gall1a.12, fade in
00 01,
FF 00, 00 38, 00 00, "Asuka"
FF 01, 00 7E, 00 05,
00 01,
FF 00, 00 00, 00 00, "Ryosuke"
FF 01, 00 92, 00 05,
00 01,
FF 00, 00 38, 00 00, "Asuka"
FF 01, 00 9E, 00 05,
00 01,
FF 00, 00 00, 00 00, "Ryosuke"
FF 01, 00 BA, 00 05,
00 01,
FF 00, 00 38, 00 00, "Asuka"
FF 01, 00 E0, 00 05,
00 01
FF 00, 00 00, 00 00, "Ryosuke"
FF 01, 00 FE, 00 05,
00 01
FF 00, 00 38, 00 00, "Asuka"
FF 01, 01 16, 00 05,
00 01
FF 00, 00 00, 00 00, "Ryosuke"
FF 01, 01 2C, 00 05,
00 01
FF 00, 00 38, 00 00, "Asuka"
FF 01, 01 62, 00 05,
00 18, FF 02, // fade to black
00 2E, FF 00 // show viewframe
00 0C, FF 12, FF 00, // show gall0b.2 background, fade in
00 22, FF 06, // play 6th song
00 17, FF 02, // fade in?
00 01,
FF 00, 00 38, 00 00, "Asuka"
FF 01, 01 6A, 00 05,
00 01
FF 00, 00 38, 00 00, "Asuka"
FF 01, 01 A0, 00 05,
00 0C, FF 0D, FF 02, // interlaced transition to gall0a.13 background
00 01,
FF 00, 00 38, 00 00, "Asuka"
FF 01, 01 C4, 00 05,
00 03, // hide textbox?
00 0E, FF 06, FF 00, FF 00, FF 01, // instant show sprite gall0a.6, X offset nearly left edge
00 01,
FF 00, 01 D2, 00 00, "Mom"
FF 01, 01 DA, 00 05,
// begin choice display, two options plus save/load
00 0A, 04 09, 00 00, 00 00, 02 09, F0 00, 03 20, 00 01, 4D 02, 09 0F, 00 03, 02 00, 01 4D, 0D FF, 02 00, 00 FF,
00 01, F0 02, 09 0F,
00 03, 02 00, 01 70, 05 19, 01 F0, 09 FF, 01 FF, 00 FF, 00 FF, 00 FF, 00 01, F6 08, 01 87, 00 00, FF 01, 02 0A, 05 19, 01 F0, 09 FF, 02 FF, 00 FF, 00 FF, 00 FF, 00 01, F6 09, 19 01, F0 02, 78 02, 00 01, 94 01, 01 EA, 09 09, F0 00, 01 E7, 03 00, 01 A4, 01 01, B9 FF, 01 D6,
00 01,
FF 00, 00 38, 00 00, "Asuka"
FF 01, 02 10, 00 05,
06 09, 10 00, 08 01, E7 00, 01 FF, 00 00, 38 00, 00 FF, 01 02, 2C 00
05 00, 00 FF, 01 02, BA 00, 05 06, 09 10, 00 08, 01 E7,
00 01,
FF 00, 00 38, 00 00,
FF 01, 03 38, 00 05,
08 01, E7 08, 02 78, 09 09, 0F 00, 02 75, 02 00, 01 F7, 01 02, 1A 00, 01 FF, 00 00, 38 00, 00 FF, 01 03, 74 00, 05 00, 01 FF, 00 01, D2 00, 00 FF, 01 03, D0 00, 05 06, 09 01, 00 08, 02 75, 00 01, FF 00, 01 D2, 00 00, FF 01, 04 5C, 00 05, 00 01, FF 00, 00 38, 00 00, FF 01, 04 7A, 00 05, 00 01, FF 00, 01 D2, 00 00, FF 01, 04 92, 00 05,
00 01,
FF 00, 00 38, 00 00,
FF 01, 04 A6, 00 05,
00 01,
FF 00, 01 D2, 00 00,
FF 01, 04 B4, 00 05,
00 01,
FF 00, 00 38, 00 00,
FF 01, 04 EC, 00 05,
06 09, 01 00, 08 02, 75 08, 02 78, 0D FF, 01 00, 01 FF, 00 00, 38 00, 00 FF, 01 05, 00 00, 05 00, 01 FF, 00 05, 58 00, 00 FF, 01 05, 62 00, 05 00, 18 FF, 01 00, 03 00, 0E FF, 02 FF, 02 FF, 00 FF, 02 00, 0F FF, 24 FE, 01 B0, FF 70, FF 00, FF 03, 00 17, FF 01,
00 01,
FF 00, 05 58, 00 00,
FF 01, 05 78, 00 05
