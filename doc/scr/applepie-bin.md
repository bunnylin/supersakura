Apple Pie BIN bytecode
----------------------

##### Lakers 1

These BIN files don't have a header or anything, just straight bytecode.
Commands can reference files by indicating an index number to look up in `file.def`.


`00 vv aaaa ww`

If variable vv = signed int value aaaa, jump to address in variable ww.

`06 vv`

Unconditional jump to address held in variable vv.

`07 vv aaaa`

Poke signed int value aaaa into variable vv.

`0F string`

Printed into standard textbox. These contain escape codes:
- \CL = clear box?
- \CR = newline?
- \E = end string
- \HA = waitkey?

`10 xx yy 00 cc nn strings`

Print nn null-terminated Shift-JIS strings at xx*8,yy*8. Color index cc.

`11 xx yy 00 ww hh 00`

Draw a window box for choices at xx*8,yy*8, size ww*16,hh*16. Color index 00...
Must be followed immediately by 10 to print box content? And at the first 07 14
after the 10 it implicitly gets the user choice into $v0 before resuming?

`12 xx`

Unknown.

`13 ffff aaaa 00`

Draw graphic file ffff in a standard story segment window.

`14 xx yy 00 ffff bb`

Show face file ffff. Draw location, incl. 8px window frame on all sides: xx*8,yy*8.
Face sprite size is always 96x96, or 112x112 with frame.

`15 xx`

Unknown, something to do with 12 xx?

`16 xx ...`

Define xx choice rectangles for choice boxes. Each rect: `xxxx*8 yyyy wwww*8 hhhh`.
xx is normally 5, even if less choices are available; extra rects have xxxx*8 = 640.
Immediately gets choice to var 0?
But choice rects aren't necessarily defined, the first choice level in a script can
appear without explicit rects.

`1A ffff`

Play music file ffff.

`1B`

Stop music.

`1C xx`

Play sound effect xx.

`1E ffff`

Jump to script file ffff.

`1F`

Roll up animation (close) standard graphics window?

`20 xx yy ww hh 00 00 00`

Fill rect at xx*8, yy*8, size ww*8, hh*8, with palette index 0.

`21 ffff gggg`

Show large graphic file ffff, using palette file gggg. Palette can be -1 for n/a.

`22`

Unknown.

`23 xx aa bb cc`

Unknown, maybe set palette xx to aabbcc?

`24 ffff 00 00 ww hh xx yy FF`

Load graphic file ffff. Size ww,hh. Target loc xx,yy.

`25 ffff aa tttt`

Load palette file ffff.
- aa = 0: Apply instantly
- aa = 1: Fade from black to palette over tttt desiseconds
- aa = 2: Fade from palette to black over tttt desiseconds
- aa = 3: Fade from white to black over tttt desisec

`26 tttt`

Sleep for tttt centiseconds. If FFFF, wait for keypress.

`28`

Unknown, instantly draw standard empty text box?

`29`

Unknown, instantly clear and remove standard text box?

`2A ffff sx sy tx ty ww hh`
Draw part of graphic file ffff. Source rect at sx*8,sy*8. Target rect tx*8,ty*8.
Draw size is ww*16,hh*8.
Maybe also the following [C0 00 00 01 00]
pan to source ofs 0,0 over 4 seconds.

`2E`

Summon the load game dialog.

`2F xx`

Unknown. Play lightning strike 4-frame animation?

`31`

Check if game has been completed, extras available.

`FF`

Exit script, return? Quit the game.
