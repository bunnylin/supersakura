JAST SCM bytecode
-----------------

These were used in Jast's older titles:
- Tenshitachi no Gogo 5
- Tenshitachi no Gogo Special 2
- Tenshitachi no Gogo 3 Bangaihen Hanseiban

An SCM file begins with a header block, followed by a whole lot of Shift-JIS.

 Offset | Type    | Description
:------:|:-------:|:------------
0       | byte[8] | Target noun indexes for LOOK, unused set to FF
8       | byte[8] | Target noun indexes for SPEAK, unused set to FF
0x10    | byte[8] | Target noun indexes for ACTION, unused set to FF
0x18    | byte    | Size of the rest of the header from 0x1B
0x19    | byte    | Choice combo count, equal to non-FF bytes in the target noun indexes
0x1A    | byte    | Number of script sections in this file
0x1B    | byte[]  | Choice outcomes data

The header is immediately followed by all script sections. Each section ends with
a `00 00` marker. Sections are called by their index number within the script, so
you have to count `00 00` markers to find the desired section to execute.

Section indexes are counted from 1 upward. On entry to a script, script section 1
is immediately executed. This is found at 0x1B + (size byte at 0x18). The other
sections from 2 onward are invoked as a response to user choices.

The script sections mostly consist of Shift-JIS and `00 00` end markers. There are
also `0A` linebreaks, and lines may wrap around the textbox implicitly.
Infrequently a `01` is encountered, typically at the start of a new section.

Each script file is treated as a single scene, and thus has a single scene graphic
that is transitioned in immediately on script entry, right before script section 1
is automatically executed. The graphic file references are found in a separate file:
- Tenshitachi no Gogo 5: `seentbl.bin`
- Tenshitachi no Gogo Special 2: `spseetbl.bin`
- Tenshitachi no Gogo 3 Bangaihen Hanseiban: `haseetbl.bin`

Choice verbs in this game are hardcoded to always and only be `LOOK`, `SPEAK`,
`ACTION`, and `SYSTEM`, all written in English. However, each verb goes to a submenu
with Japanese strings for target nouns.

The target nouns strings are defined in `ntbl.bin`, a series of 256 null-terminated
Shift-JIS strings. All strings are padded to 8 double-width characters with `8140`
doublewidth spaces or regular ascii spaces, and unused strings are just all spaces.


##### Choice outcome data

The choice outcomes data is a stream of variable-size structures. The basic form is:
```
byte - action code
byte - verb index, 1 = look, 2 = speak, 3 = action
byte - target noun index, referencing ntbl.bin
byte[] - action parameters
```

The codes are:

`00 (aa xx yy)`

Unconditionally execute script section aa. Then, if xx != FF, jump to script xx.
Or, if yy != FF, set flag yy to true (triggered).

`02 (yy aa bb)`

If flag yy is not set, execute script section aa. If yy is set, execute section bb.
Unlike the `08` command, `02` is a shorter way to just call two unconditional script
sections when neither outcome needs to set a flag or jump to another script.

`08 (yy a1 a2 { action if false } { action if true })`

Conditional execution: If flag yy is not set, execute the first action, otherwise
the second. The parameters a1 and a2 just indicate which action codes would be used
in either action, but the code is also found at the start of the actions, so a1 and a2
are redundant.

`09 (vv aa bb cc dd xx yy)`

Use local tracking variable vv, which is inited to 0 and gets +1 each time this
choice is selected. Use the value of vv to select which script section to execute,
from aa to dd. FF is used for undefined sections. When the last value or an undefined
section is reached, keep showing the last defined section.

After executing the appropriate section, if xx != FF, jump to script xx.
Or, if yy != FF, and the last valid script section was just executed,
set flag yy to true (triggered).


###### Example from Special 2, c02.scm

The outcome data has 5 entries. The first one:
```
08 01-3C: 02 09 00 { 09: 0A, 02 03 FF FF, FF FF }; { 00: 04, FF FF }
```

This is for choice 01-3C, which is the LOOK verb, targeting ディスプレイ (monitor).
This is conditional, so execute the first action if flag 02 is not set yet, or the
second action if the flag has been set. The action codes 09 and 00 right after the
flag number 02 can be ignored, since they are given inside the actions.

The first action is a tracked section caller. It uses local tracking variable 0A to
call script section 2 the first time, then script section 3 on any subsequent try.
The remaining parameters are FF, so nothing else happens in this action.

The second action is an unconditional call to section 4. So, once flag 2 is set,
"looking" at the "monitor" only returns the response in section 4.

```
09 01-4B: 0B, 05 06 07 08, FF FF
```

The second outcome is for looking at パソコン (the computer). This is a tracked
section caller, using local tracking variable 0B. When selected, this choice runs
sections 5 through 8 on subsequent tries, and repeats 8 if you keep trying.

```
00 02-52: 09 FF 02
```

The third outcome is for speaking to ひとりごと (musing to yourself). This only ever
invokes script section 9, but also sets flag 02.

```
08 03-91: 02 00 09 { 00 0D FF FF }; { 09: 0D, 0E 0F 10 FF FF 03 }
```

The fourth outcome is for the action 調べる (research). Another conditional, checking
flag 02. If it's not set yet, always run section D. If the flag is set, run sections
E to 10 on subsequent attempts. Furthermore, after running 10, set flag 03 to true.

```
08 03-75: 03 00 00 { 00 11 FF FF }; { 00 12 03 FF }
```

The final outcome is for the action 行く (get going). Conditional, checking flag 03.
If not set, always run section 11. If set, run section 12, and go to the next game
script, `c03.scm`.


##### Scene graphics

###### Tenshitachi no Gogo 3 Bangaihen Hanseiban, and Special 2

For these two games, the scene graphics are specified in `haseetbl.bin` and `spseetbl.bin`
respectively. Both use the same structure, an array of scene data entries of 34 bytes each:

```
char[3] - script filename without suffix
char[3] - scene graphic filename without suffix
byte - transition style
byte - number of animated things in scene, can be from 0 to 3
char[3] - animated graphic filename without suffix, or nulls if not used
byte - animated graphic total pixel width, divided by 8
byte - animated graphic total pixel height, divided by 8
byte[7] - data for animation 1, or nulls
byte[7] - data for animation 2, or nulls
byte[7] - data for animation 3, or nulls
```

For the animation data structure, see (doc/gfx/jast-gra-brge-img.md)[../gfx/jast-gra-brge-img.md].

When a new script is run, before executing script section 1, check the scene table.
Find the entry for the script's filename, then draw the associated scene graphic and
start the animations, if any.


###### Tenshitachi no Gogo 5

The scene graphics are specified in `seentbl.bin`. Scene data entries, x bytes each:

```
char[3] - script filename without suffix
char[3] - graphic filename without suffix
byte - transition style
byte - null, unknown
byte[3*5] - weird repeated series 00 00 00 "xy", presumably garbage
```


##### Analysis

Remaining mysteries:
- What's used to select the music index? Music changes on entry to c04, and its first
  script does start with `01`... but there's no index value after that. "Next track"?
  But subsequent sections also start with a `01` while the music remains the same.
- What triggers an ending?

TenGoSpecial2 c01.scm:

Playing the game:
- On game start, it immediately displays a room graphic and plays a song
- Prints section 0, two box pagefuls, then waits for user choice
- Look has 4 options:
  部屋 (0x59 room), パソコン (0x4B computer), テレビ (0x3E TV), ポスター (0x5A poster)
- Speak has 3 options: スピーカー (speaker), 親父 (dad), パソコン (computer)
- Action has 2 options: パソコンを使う (use computer), ポスターを取る (take poster)
- Look:0 prints sections 2, 3, 4, and keeps repeating 4
- Look:1 prints sections 5, 6, 7, and repeats 7
- Look:2 prints sections 8, 9, and repeats 9
- Look:3 prints sections 10, 11, and repeats 11
- Speak:0 prints sections 12, 13, and repeats 13
- Speak:1 prints sections 14, 15, 16, and repeats 16
- Speak:2 prints sections 17, 18, and repeats 18
- Action:0 prints section 19 repeatedly; if you have looked at computer three times,
  then it prints section 20, then a waitkey, and moves to the next script!
- Action:1 prints section 23, 24, and repeats 25

Possibly unused sections: (the protagonist fancies himself a cat burglar...)
- 21: "Another stack of bills is coming, but I only remember the number Q2, not good."
- 22: "Again, I'm no longer a rank amateur thief. I want to be the best gentleman thief!"


Outcomes stuff from 0x1B onward:
09 01-59: 01, 02 03 04 FF, FF FF
09 01-4B: 02, 05 06 07 FF, FF 01
09 01-3E: 03, 08 09 FF FF, FF FF
09 01-5A: 04, 0A 0B FF FF, FF FF
09 02-2A: 05, 0C 0D FF FF, FF FF
09 02-0C: 06, 0E 0F 10 FF, FF FF
09 02-4B: 07, 11 12 FF FF, FF FF
08 03-C2: 01 00 00 { 00 13 FF FF }; { 00 14 02 FF }
09 03-B2: 09, 17 18 FF FF, FF FF

Same for c02.scm:
08 01-3C: 02 09 00 { 09: 0A, 02 03 FF FF, FF FF }; { 00 04 FF FF }
09 01-4B: 0B, 05 06 07 08, FF FF
00 02-52: 09 FF 02
08 03-91: 02 00 09 { 00 0D FF FF }; { 09: 0D, 0E 0F 10 FF FF 03 }
08 03-75: 03 00 00 { 00 11 FF FF }; { 00 12 03 FF }

c03.scm:
09 01-60: 0E, 02 03 04 FF, FF FF
09 01-64: 0F, 05 06 FF FF, FF FF
09 01-34: 10, 07 08 09 FF, FF FF
09 02-60: 11, 0A 0B 0C FF, FF 04
09 03-A6: 12, 0D 0E 0F 10, FF FF
09 03-B0: 13, 11 12 13 FF, FF FF
08 03-82: 04 00 09 { 00 14, FF FF }; { 09 14 15 16 17 18, FF 05 }
08 03-80: 05 00 09 { 00 1A, FF FF }; { 09 15 1B 1C FF FF, 04 FF }

c04.scm:
09 01-20: 16, 02 03 04 05, FF FF
08 02-20: 06 09 09 { 09: 17 06 07 08 09, FF FF }; { 09: 18 0A FF FF FF, 05 FF }
09 03-B0: 19, 0B 0C 0D 0E, FF FF
09 03-82: 1A, 0F 10 11 12, FF 06

c05.scm:
08 01-C3: 07 09 00 { 09: 1B, 02 03 FF FF, FF FF }; { 00: 04, FF FF }
08 01-30: 07 09 00 { 09: 1C, 05 06 FF FF, FF FF }; { 00: 07, FF FF }
08 01-14: 07 09 00 { 09: 1D, 08 09 FF FF, FF FF }; { 00: 0A, 19 FF }
02 02-30: 07 0B 0C
08 03-A0: 07 00 09 { 00: 0D, FF FF }; { 09: 1E, 0E 0F 10 11, FF FF }
09 03-B0: 1F, 12 13 14 FF, FF FF
09 03-8E: 20, 15 16 FF FF, FF 07
08 03-A1: 07 00 09 { 00: 17, FF FF }; { 09: 21, 18 19 FF FF, 06 FF }

c06.scm:
08 01-14: 08 00 09 { 00: 02, FF FF }; { 09: 22, 03 04 FF FF, FF FF }
09 01-1E: 23 05 06 FF FF FF FF
09 01-45: 24 07 08 FF FF FF FF
09 01-22: 25 09 0A FF FF FF FF
09 02-52: 26 0B 0C FF FF FF FF
09 03-AB: 27 0D 0E FF FF FF FF
09 03-C4: 28 0F 10 FF FF FF 08
08 03-C5: 08 00 09 { 00 11 FF FF }; { 09: 29 12 13 FF FF 07 FF }

c07.scm:
02 01-2F: 09 02 03
02 01-04: 09 04 05
08 01-3A: 09 09 01 { 09: 2A, 06 07 FF FF, FF FF }; { 00 08 FF FF }
09 01-12: 2B, 09 0A 0B 0C, FF FF
08 01-59: 09 00 00 { 00: 0D, FF FF }; { 00: 0E, FF 0A }
09 01-4C: 2D, 0F 10 FF FF, FF FF
09 02-52: 2E, 11 12 FF FF, FF FF
02 03-80: 09 13 14
09 03-A7: 2F, 15 16 FF FF, FF FF
00 03-74: 17, FF 09
00 03-92: 18, 08 FF <-- can't burgle in the dark, why not turn on the lights ^^;
08 03-C6: 0A 00 09 { 00: 19, FF FF }; { 09: 30. 1A 1B FF FF. 0A FF }

c08.scm:
09 01-2F: 30, 02 03 04 FF, FF FF
09 01-04: 31, 05 06 FF FF, FF FF
09 01-3A: 32, 07 08 FF FF, FF FF
00 01-4C: 09, FF FF
09 02-63: 33, 0A 0B FF FF, FF FF
09 03-80: 34, 0C 0D FF FF, FF 0B
08 03-C6: 0B 00 09 { 00: 0E, FF FF }; { 09: 35, 0F 10 FF FF 09 FF }
09 03-78: EB, 11 12 13 14, 09 0B

c09.scm:
Target nouns are:
25 26 27 1F 02 28 29 FF
25 FF FF FF FF FF FF FF
5E 6F 2B E2 FF FF FF FF
But in-game LOOK has 4 entries, SPEAK has 3, ACTION has 2. Unexpected!?
Also, the 09-outcomes below are shorter than usual. How can this be?

09 01-25: 43, 02 03 FF FF, FF
09 01-26: 44, 04 05 FF FF, FF
09 01-27: 45, 06 07 FF FF, FF
09 01-1F: 46, 08 09 FF FF, FF
09 01-02: 47, 0A 0B FF FF, FF
09 01-28: 48, 0C 0D FF FF, FF
09 01-29: 49, 0E 0F FF FF, FF
07 02-25: 01
08 03-5E: 51 00 00 { 00 14 FF 51 }; { 00 15 FF FF }
08 03-6F: 51 00 00 { 00 16 FF FF }; { 00 17 FF 52 }
07 03-2B: 02
09 03-E2: 4E, 1A 1B FF FF, FF
