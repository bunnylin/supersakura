Tasogare no Kyoukai
===================

Opening script
==============
The opening script OP_M2 is interpreted wholly differently than the other OVL
files. The music played during it is TK_19.

  01 image
The image name is given as a zero-terminated string. Displays the given image
immediately.

  02
Clears the screen immediately.

  03 xx
Waits xx 10ths of seconds.

  04
Rapidly flashes the screen white 2-3 times.

  05 xx strings
Prints the given text block on the screen immediately, in a red font.
The data byte is the number of text rows, and each row follows as
a zero-terminated string.

  06 xx strings
Fades in the given text block on the screen, over about 6 seconds. Waits
a second, then slowly fades the text out.
The data byte is the number of text rows, and each row follows as
a zero-terminated string.

  07 image
Draws the given image while flashing the background a slight bit brighter for
a second, a barely noticeable palette effect.

  0A
Flashes the screen a dozen times and ends the sequence.


Variables
=========
Setting some global variables to non-zero gives the player items, which
become visible on the right side of the viewframe.
Variable 5: a scroll in the first slot?
Variable 4: treasure hunter contract? Has a number on the cover. First slot.
Both v4 and v5 can be set simultaneously, and it uses a combined image.
Variable 3: an eyeball in the second slot
Variable 1: a lantern in the third slot
Variable 0?
Variable 11: a necklace in fourth slot
Variable 10: controls the date indicator and moon phase?
  - 0 = "New moon", 1st moon phase displayed
  - 1 = "day two", 1st moon phase (new moon)
  - 2 = "day three", 2nd moon phase (waxing sliver)
  - 3 = "day four", 3rd moon phase (waxing further)
  - 4 = "day five", still 3rd moon phase
  - 5 = "day six", still 3rd moon phase
  - 6 = "day seven", 4th moon phase (waxing almost half)
  - 7 = "day nine", 5th moon phase (waxing half)
  - 8 = "10 days left"? 6th moon phase (waxing past half)
  - 9 = "day twelve", 7th moon phase (waxing quarter to full)
  - A = "nearly full moon"? 8th moon phase (waxing nearly full)
  - B = "day fourteen", still 8th moon phase
  - C = "full moon", 9th moon phase (full)
  - D = "evening of day sixteen?", 10th moon phase (slightly waning)

Furthermore, variable 2 tracks the lantern's remaining fuel...
When you grab the lantern, v2 is set to 502. Also, some events in dungeon
will refresh the light up to 506, and the protagonist goes "Huh? My lantern
shines more brightly."
When in the dungeon, the lantern check is done every 100 steps, turning
doesn't count, and on the 3rd check the light level is reduced. On the 4th
check, it's reduced further. 5th check kills ya?

Eyeballs are found in numerous locations, and they can be used in some events
and to defeat some monsters to reach new areas.


Dungeon
=======
There are two maps, MAP1.DAT and MAP2.DAT. Both can be read as 100x100 raw
bitmaps. For convenience, both are here in PNG format. The game treats the
coordinates as 1..100 on both axles.

- When you run into a wall, the engine calls TA_00DG, noarray.
- When you face a $41 ceiling rock pillar, call array2:0.
- When you stand in front of a $42 "MER", call array2:1.
- When you stand in front of a $43 stone coffin, call array2:2.
- As v2 counts down step by step, call TA_00DG every 100 steps. Also, there's
  a lantern graphic in the viewframe, and it has five frames with different
  flame intensities. Start with frame 0.
   * v2 = 400, call array6:0, switch to frame 1
   * v2 = 300, call array6:1, switch to frame 2
   * v2 = 200, call array6:2, switch to frame 3, cut minimap to 3x3
   * v2 = 100, call array6:3, switch to frame 4, cut minimap to 1x1
   * v2 = 0, call array6:4, game over.

Interpreting the map bytes:

  Empty space:
00 - Black.
  Events:
01 - Entrance point! Light pastel blue. array4:0
02 - Entrance to map2! Bottommost point. Light pastel green. array4:1
03 - Another ladder in map2, top left, center of room.
04 - Same as 03, but on a smaller version of the same map!
05 - Entrance to map1 final area!
06 - Monster attack event, in several spots. Dark red. array4:5
07 - Singular event, map2 middle, center of sort of room.
08..28 - Events
33 - When stepped on, rotates the player 90 degrees right.
34 - When stepped on, rotates the player 90 degrees left.
35 - When stepped into, rotates the player 180 degrees right.
37 - Propels player 2 squares forward!
38 - 3 squares forward? Not used, I think.
  Impassable features:
41 - Rock pillar in ceiling. White.
42 - Wall with MER scribble. Greyish red.
43 - Event? Altar? In map1 bottom right corner, center of room.
44 - Event? Altar? In map1 top left corner, kind of center of room.
45 - Event? Altar? In map1 bottom left corner, center of room.
     Also present in map2, second from mid right, center of room.
46 - Event? Altar? In map2 top left, center of a blob.
47 - Event? Altar? In map2 mid right, center of room.
48 - Event? Altar? In map2 far left, center of room.
49 - Event? Altar? In map2 bottom left, top room.
4A - Event? Altar? In map2 top right, kind of center of room.
56 - Event? Altar? In map1 top right corner, center of room.
  Wall types:
C0 - Solid wall. Dark grey.
C1 - Fake wall, passable from any direction. Bright yellow.
C3 - Fake wall, passable only from top. Dark cyan.
C4 - Fake wall, passable only from bottom. Cyan.
C5 - Fake wall, passable only from left. Pink.
C6 - Fake wall, passable only from right. Bright blue.


Furthermore
===========
TA_00DG.OVL contains dungeon-related code, and breaks convention with the
other OVL files.

Starts with 0008. At address $8 there is valid script code up to $12C, which
terminates with command 03, return to dungeon.

Second word is 012D. At this address there is an array of 10 addresses:
0141 ... [05] TA_4841.OVL [00] [03]
014F ... lots of valid code, ends with [03]
028A ... [0C-01 C0 C0] [0B-35 C0 033A] [0C-03 C0 01]
         [05] TA_4848.OVL [00] [01] [03]
02A6 ... [0C-01 C3 C3] [0B-35 C3 033A] [0C-03 C3 01]
         [05] TA_4848.OVL [00] [01] [03]
02C2 above pattern repeats...
02DE
02FA
0316 ... [0B-36 033A]
031A
0336 ... [0B-36 033A]
And terminates upon encountering address $141.
At the popular $33A address, we find a bunch of code ending with [03].

Third word is 0387. An array of 40 addresses:
03D7 ... draw sprite YO_012, [05] TA_1100.OVL [00] [03]
03F0 ... draw YO_012, text, [01] [03]
etc
098A ... if v$47 AND (v$47 - 2) = 0 then jump to 09FB
         text
         $9F8: [11-0C-03]
         $9FB: [03]

Fourth word is 09FC. An array of 5 addresses:
0A06 ... text [03]
0A6C ... text [03]
0ABB ... casejump v$A9 to 0AC8, 0DAA, 0E29
         $AC8: text [03]
0B62 ... casejump v$A9 to 0B6F, 0F15, 0F9F
         $B6F: [11-08-0A] text [03]
0BDE ... casejump v$A9 to 0BEB, 106B, 136A
         $BEB: [11-08-14] lots of text [sleep 3 sec] [bad ending?]

File size is $1601 (5633) bytes.
