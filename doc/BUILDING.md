How to build
------------

### Requirements

- [The Free Pascal compiler](https://www.freepascal.org/)
- [SDL2 and SDL2_ttf](https://libsdl.org/) libraries/dlls
- [The SDL2 Pascal headers](https://github.com/PascalGameDevelopment/SDL2-for-Pascal) (already embedded under "lib")
- [Various moonlibs](https://gitlab.com/bunnylin/moonlibs) (already embedded under "lib")
- [Pascal headers for FluidSynth](https://gitlab.com/bunnylin/pasfluidsynth) (already embedded under "lib")


### Compiling

First, get the SuperSakura source code:

    git clone https://gitlab.com/bunnylin/supersakura

Dev-branch has less stable code, so you may want to use the release-branch:

    git checkout release

Make sure you have the Free Pascal Compiler, preferably the latest release.
Also get the SDL2 and SDL2_ttf runtime binaries.
On Linux, you may need the SDL2 development libraries as well, but only to provide
a symlink to the latest `.so` file. Also make sure you have GNU libc if on Linux.

Arch Linux:

    pacman -S fpc sdl2 sdl2_ttf

Ubuntu:

    apt install fpc libsdl2-dev libsdl2-ttf-dev

Fedora:

    dnf install fpc glibc-devel SDL2-devel SDL2_ttf-devel

The SDL2 Pascal headers and moonlibs are statically linked units or
libraries; they are needed to compile the engine and tools, but afterward
are not needed to run them. The sources for both are included with
SuperSakura under the "lib" directory, so you already have them.

To compile a program or unit, you can use the included `comp.bat` or
`comp.sh` commands which include friendly compiler flags, or invoke the
Free Pascal compiler directly with `fpc`.

FPC will automatically build any units needed by the program being compiled.
As long as the compiler output doesn't say "Fatal:" or "Error:" at the end,
it probably worked. The created unit or executable will be in the same
directory as the source.

To build the whole engine and its tools, on Linux:

    ./comp.sh supersakura
    ./comp.sh supersakura-con
    ./comp.sh sakutool

On Windows:

	comp.bat supersakura
	comp.bat supersakura-con
	comp.bat sakutool


### Setting up games

The resource decompiler tool takes individual files from original games, and
saves them under SuperSakura's data directory in converted standard file
formats. Note, that although some PC-98 games are supported, sakutool cannot
yet extract the individual data files from .HDI or .FDI images, so if you
keep your PC-98 games in those, you will have to first extract the files
from the disk image manually. Possible tools for this:

* Ryo-Cokey's [FIVEC](https://www.pc98.org/project/fivec.html)
(Windows, GUI)
* [EditDisk](https://hp.vector.co.jp/authors/VA013937/editdisk/index_e.html)
(Windows, GUI)
* My own [98ripper](https://gitlab.com/bunnylin/98ripper) (Linux, commandline)

To convert resources:

    ./sakutool x <filename or directory>

To compile resources into a usable SuperSakura data file:

    ./sakutool make <projectname>

To run any compiled game through a friendly frontend:

    ./supersakura

To summarise by example, you could convert and run the DOS version of The Three
Sisters' Story with commands like:

    ./sakutool x /mygames/threesistersstory/
    ./sakutool make 3sis
    ./supersakura 3sis


### Desktop entry and icons

If needed, various sizes of the SuperSakura icon are found in
[/etc](../etc). There's also a basic `.desktop` file to make a Linux desktop shortcut.

For a user-specific Linux installation, copy `supersakura.svg` to
`~/.local/share/icons/hicolor/scalable/apps/`, and `supersakura.desktop` to
`~/.local/share/applications/`. Edit the `.desktop` file to point at wherever you put
the supersakura and sakutool executables.
