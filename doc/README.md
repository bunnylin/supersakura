This directory contains documentation pertaining to the SuperSakura engine
and reverse-engineered specifications and notes on many old file formats.


##### SuperSakura engine notes

- [Build instructions](BUILDING.md)
- [Miscellaneous musings](engine_notes.md)
- [Old reverse-dithering algorithm notes](beautify.md)
- [Old renderer design notes](renderer.md)
- [Out of date SakuraScript manual and reference](sakurascript/ssmanual.md)
- [Out of date SakuraScript commands reference](sakurascript/ssref.md)
- [String handling and automatic translation notes](strings.md)
- [To-do list](todo.md)
- [User interface design notes](uidesign.md)


##### Individual game notes that probably should be stored differently

- [Tenshitachi no Hohoemi](hohoemi.md)
- [Maririn DX](maririn.md)
- [Tasogare no Kyoukai](tasogare.md)
- [Winterquest](winterq.txt)
