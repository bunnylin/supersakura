Cuisse
------

A minor company that only released More and More for the PC98.
But the game's supposed to be pretty good.

Graphics used:
- CP2

Music used:
- MOB files

Scripts used:
- SIN script files


### SIN scripts

These use straightforward XOR encryption, and after that they are plaintext scripts!
The encryption was described by Alatalo. Thank you!

To decrypt:
```
x := 0
For each byte in the script file:
	script byte := script byte XOR x
	x := (x + 1) & 0xFF
```
