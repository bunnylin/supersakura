Silence and Sogna
-----------------

Silence and its subsidiaries Sogna and Technobrain are known for relatively
lavishly animated games, primarily the Jewel BEM Hunter Lime and Viper series.

Alpharobo has already documented much of this:
https://alpharobo.neocities.org/gyna2_data

- PC98 graphics formats: MSV, SCR, ANM
- PC98 music: FMX, PMD files.
- PC98 scripts: SIL
- Windows graphics formats: ANM, PCM? or is that audio...
- Windows archives: SGS.DAT

Their PC98 games apply an extra layer of generic compression on most files: SZH.

MLD can view most of those graphics, although only in their SZH-packed form.
For more details, see individual file format documents under doc/gfx.


### Engine versions

The common executable is `sgs.com`. Strings found in different versions of the file:
- Guyna-Rock 2: SILENS GAME SYSTEM Ver1.00CopyRight 1992/9 K.Ohshima
- Lime 1: SILENS GAME SYSTEM 386 Ver1.00 CopyRight 1993/4 K.Ohshima
- Viper V6: SILENS GAME SYSTEM 386 Ver1.01 CopyRight 1993/5 K.Ohshima
- Animahjong V3: SILENS GAME SYSTEM Ver2.00CopyRight 1993/1 K.Ohshima
- Lime 7: SILENS GAME SYSTEM 386 Ver2.13CopyRight 1993/5 K.Ohshima
- Viper V6 Turbo: SILENCE GAME SYSTEM 386 Ver 2.31 CopyRight 1993/5 K.Ohshima
- Lime 12: SILENCE GAME SYSTEM 386 Ver 3.00 CopyRight 1993/5 K.Ohshima
- Viper V6 RS: SUPER GAME SYSTEM 386 Ver 3.40RS CopyRight 1993/5 K.Ohshima
- Viper V16: SUPER GAME SYSTEM 386 Ver 3.42 V16 CopyRight 1993/5 K.Ohshima
- Exciting Milk: SUPER GAME SYSTEM 386 Ver 3.50 MILK CopyRight 1993/5 K.Ohshima

Val-Kaizer has an `acsex.exe`, which doesn't have an obvious copyright string, but
does have some references to BASIC commands. Is the game written in basic?
Could be the same deal in Guynarock's `ngai` executables, and Guynarock Minimum.


##### SZH compression

This is basic LZSS compression, except the compressed data is treated as a bitstream
rather than byte-aligned values. Bits are read top-down from 0x80 to 0x01.

Header:
```
char[3] - uncompressed file suffix (MSV, SCR, ANM, SIL, RGB)
```

Decompression pseudocode:
```
While more bits in input stream:
	b := Read next bit
	If b is set:
		copy_length := (Read the next 4 bits) + 1
		copy_distance := Read the next 12 bits
		Output copy_length bytes from copy_distance bytes ago
	If b is not set:
		Output the next 8 bits as a literal byte
```

As a special case, if trying to copy bytes from before the start of the output buffer,
output zeroes for that part? Viper V6 game_sys.msv exhibits this but looks broken...
However, that file appears to be identical in V6 Turbo and V6 RS, so the copy must be
valid, and it's copying something more than just zeroes...


##### PCM files

Maybe sound, or maybe graphics? The data looks like it's been SZH-compressed,
although without the 3-char header.
