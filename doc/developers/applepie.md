Apple Pie
---------

Apple Pie was a developer and publisher from 1992 through 2005. Their most notable
creation may be the Sei Shoujo Sentai Lakers tactical RPG series.


### Lakers 1

Uses OZM graphics, documented separately. Palette index 7 is used for transparency
for some images. There's no indication in the OZM file itself if transparency should
be used, you just have to guess from context.

Any image that has a matching RGB file should use that for its palette. But there are
many special-purpose RGB files.
B-RGB is presumably default for battles. V-RGB is nearly identical, with slightly
darker colors, possibly for story segments. BTL#.RGB are all the same except for
color 15, which is a wholly different color each time. Blue, green, orange, and
a redder orange. Text or some UI element color?..
Change#.RGB move palette index 7 from pink to bright danger red.
Probably use B-RGB as default for anything without a custom palette. Need to figure
out what the color 15 changes are used for. I don't see when you would ever use V-RGB.

Face.ozm is a frame for a character face sprite.

Kabegami.ozm is the fullscreen graphic over which the title logo is drawn.

Kabe_2.ozm is a noisy pattern background, such as used behind face sprites.

Kao##.ozm are individual face sprites. 51 different ones!

Message.ozm is the bottom message box.

Switch.ozm is a sprite sheet for clickable buttons: Move, Attack, System, Option.

Window.ozm is a generic dynamically-sizable text box etc frame.

Yazirusi.ozm has an Attack!! and a Defense triangle button sprite.

Yosumi.ozm is a fullscreen battle frame, with switch buttons along the bottom.

Fin.ozm is a cursive "Fin" text to show at the end.

S#_cut##.ozm are cutscene graphics, which don't use transparency.

MAP files: first two bytes are battle field X and Y size, the rest is a tilemap.

The unit0#.ozm files are the unit sprite sheet for each battle, with our heroes
appearing repeatedly in most sheets.

The chip0#.ozm files are the sprite sheet for terrain tiles. You can probably
only select one per battle.

Allmap.ozm is the battle field frame.

Bat_wind.ozm looks like an action popup showing a character's attack wind-up.

Btl##.ozm are those attack wind-up closeups.

Bth##.ozm are similar closeups except the character has been defeated...

Btl##.bin describe the battle parameters.

Ending.bin, ending2.bin, evn##.bin, memory.bin, openning.bin, s0#_0#.bin
are raw script bytecode files.

Char.bin contains plenty of Shift-JIS, probably regular script file.

File.def is an index for which disk each file is on. There's an array of data
offsets first, then a series of file info. Each file info is the disk number
as a byte, then the null-terminated filename. This is also used when the game
script loads any file - graphics, music, script files are referenced by index.

Init.def is a bat file for launching the game with the PMD driver.

Rei_se.efc presumably describes FM sound effects.

Lakers 1 scripts are in BIN files.


### Lakers 2

Uses OZM and OLH graphics, documented separately.

ApplePie.ozm is the pink-colored logo that's shown on startup. ApplePie.olh is
a light red version of same, presumably displayed at some other time.

L2op_anm.olh is a multiframe image for the fancy opening logo. The angled lines
images are flipped rapidly, then the logo transitions in piecemeal.

L2op_001.olh is a multiframe image for character introductions, again with
some fancy transition effects applied at runtime.
