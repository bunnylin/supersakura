JAST, Tiare
-----------

One of the very oldest VN houses, JAST (Japan System Technology) published games from
1985 to 1998, their most famous creation being the long-running Tenshitachi no Gogo
series. However, in the West they are best known for The Three Sisters' Story,
Season of the Sakura, and Runaway City, among the earliest English VN ports.
Those were published by JAST USA, a separate localisation and publishing operation
still going strong as of 2024.

JAST spawned a few notable spinoff companies, the most interesting of which for this
document is Tiare. Formed by JAST staff, Tiare used JAST's game engine through the
remaining PC98 era and for a few windows games, from 1995 to 1998, apparently
working closely with their parent company. They even kept sharing various background
graphics, implying their games take place in a shared universe. Tiare presumably shut
down together with JAST in 2000.


### Engine versions used

There are a few Jast/Tiare engine versions. Some of the really old ones are
probably just all hardcoded binaries.

- Gomen ne Angel and earlier use custom filesystems, not normally extractable.
  The format suffixes are guesses based on stray strings found in the disk images.
- Maririn DX uses partially hardcoded scripts.
- Deep uses a slightly different variant of this script format.
- Hohoemi has some extra script commands for handling its special interface.

| Game (PC98)                                |  Graphics   |  Music      |  Scripting  |
|:-------------------------------------------|:-----------:|:-----------:|:-----------:|
| Tenshitachi no Gogo                        | All hardcoded?                          |
| Tenshitachi no Gogo 2 Minako               | All hardcoded?                          |
| Tenshitachi no Gogo 2 Bangaihen            | All hardcoded?                          |
| Derringer                                  | ?           | ?           | ?           |
| Cosmos Club                                | IMG v1      | ?           | SCD.BIN     |
| Tenshitachi no Gogo 3 Ribbon               | IMG v1, ANI | ?           | ?           |
| Yoru no Tenshitachi                        | IMG v2      | ?           | ?           |
| Tenshitachi no Gogo 3 Bangaihen            | IMG v2, ANI | ?           | ?           |
| Tenshitachi no Gogo 4                      | IMG v2, ANI | ?           | SCM.BIN     |
| Gomen ne Angel                             | IMG v2, ANI | ?           | ?           |
| Tenshitachi no Gogo 5                      | BRGE        | MLO         | SCM         |
| Tenshitachi no Gogo 3 Bangaihen Hanseiban  | BRGE        | MLO         | SCM         |
| Tenshitachi no Gogo Special 2              | BRGE        | MLO         | SCM         |
| Tenshitachi no Gogo 6                      | MAG v2      | MLO         | SCM         |
| Totsugeki! Bakkon Street                   | Pi          | MLO         | MSG         |
| Maririn DX                                 | Pi          | O           | OVL v2 +    |
| Totsugeki Bakkon Street 2 Hunting Roulette | Pi          | O           | OVL v2, MSA |
| Deep                                       | Pi          | O and SC5   | OVL v2 +    |
| Tenshitachi no Gogo Collection 1           | Pi          | O and SC5   | OVL v2      |
| Yukineko                                   | Pi          | HMD and SC5 | OVL v2      |
| Tenshitachi no Gogo Tenkousei              | Pi          | HMD and SC5 | OVL v2      |
| Vanishing Point                            | Pi          | HMD and SC5 | OVL v2      |
| Tenshitachi no Gogo Collection 2           | Pi          | O and SC5   | OVL v2      |
| Meisou Toshi (Runaway City)                | Pi          | O and SC5   | OVL v2      |
| San Shimai (The Three Sisters' Story)      | Pi          | O and SC5   | OVL v2      |
| Sakura no Kisetsu (Season of the Sakura)   | Pi          | O and SC5   | OVL v3      |
| Eden no Kaori                              | Pi          | O and SC5   | OVL v3      |
| Majokko Paradise                           | Pi          | O and SC5   | OVL v3      |
| Tenshitachi no Hohoemi                     | Pi          | O and SC5   | OVL v3 +    |
| Tasogare no Kyoukai                        | Pi          | O and SC5   | OVL v3      |
| Shyuukan From H                            | Pi          | O and SC5   | OVL v3      |

| Game (Windows)            |  Data archive  |  Graphics   |  Music      |  Scripting  |
|:--------------------------|:--------------:|:-----------:|:-----------:|:-----------:|
| Tenshitachi no Hohoemi    | EVE archive                                              |
| Majokko Paradise          | EVE archive                                              |
| Tales of the Nights       | DAT/HED        | Pi graphics |             | OVL v4?     |
| Eroden                    | DAT/HED        | MAG         | wave audio  | OVL v4?     |
| Pretty Parfait            | DAT/HED        | MAG         |             | OVL v4?     |
| Sexy Parfait              | DAT/HED        | PIC         |             | OVL v4      |
| Monmon Gakuen Tenkousei   | a Flash game...                                          |
| Izayoi                    | DAT/HED        | PIC         |             | OVL v4?     |
| Tenshi Ningyou            | DAT/HED        | PIC         |             | OVL v4?     |
| You & I                   | Ikura GDL                                                |
| Justice Slave             | Ikura GDL                                                |


##### Engine versions overview

Detailed documentation: (doc/scr/jast-ovl.md)[../scr/jast-ovl.md]

The scripting before OVL v2 is not understood yet, so can't comment on those.

OVL v2 originally appeared in Maririn DX, though seemingly offloading parts of
script execution to possibly hardcoded hooks in the engine binary. With Deep, the
scripting engine had become well-rounded enough to allow pretty much the whole game
to be implemented in the scripting language.

OVL v2 scripts are fairly simple. Each script file begins with a header section
containing user choice strings and graphic filenames, if any are used by the script,
then the rest is simple imperative bytecode. There's no complex arithmetic evaluation
stack or direct rendering tricks. The script can only display an image with a specified
transition effect, but can't dynamically override the image position.

OVL v3 rearranges some of the bytecodes and has redesigned graphics handling.
Graphics are now named in-line in bytecode rather than in the script header, they can
be dynamically drawn at any screen position, and there's some kind of an off-screen
graphics stash to easily return to the previous displayed state.

OVL v4 changes some bytecode parameters to be uint16's instead of bytes, and makes
some further changes to graphic handling.


### Music

There are plenty of different music files:
- .M and .M2 are Professional Music Driver files.
- .O and .HMD are other similar FM music files...
- .MLO is an FM music file...
- .SC5, .RCP, and .MMD are Recomposer midi files.

See the `mus` directory for notes on specific formats.


### Graphics

See [doc/gfx/jast-gra-brge-img.md](../gfx/jast-gra-brge-img.md).


### Season of the Sakura girl.000 files

These are saved games. Normally the game engine writes saves in your C:\ root
with the file extension `000`. Copy one of the `girl.000` files over a save file
and load it to get straight to the relevant special event. Not much point
reverse-engineering these, although it would probably be simple enough.


### Jast Memorial Pack

This comes with a launcher and all three classic games. The PCK archive format
is fairly straightforward, and documented by vilevn.

However, the game resources have all been converted to be different from the
originals. Images are now uncompressed BMPs, with extra-blurry copies of all
large images saved separately. The music is in native MIDs and MP3s, except
there are no midis for Season of the Sakura. The game scripts are all new ASM
files, a completely different format from OVLs. Some parts of the game text
have been changed, not always for the better, and some typos remain unfixed.


### JAST/Tiare Windows titles

Jast developed 2 Windows titles and Tiare 4, using an updated version of
their classic engine. After this, they gave up on their custom engine and
briefly switched to licensed engines - Jast tried Neural's EVE multimedia
script processor and then Flash, while Tiare moved to Ikura GDL.

The Windows port of their classic engine stores game resources in `data.dat`.
The index for it is `data.hed`.

`Data.hed` begins with what looks like a short header of 17 bytes. It has the
associated data file's name, null-padded, plus 8 more useless bytes.
Examples:
- `data.dat [00 00 00 00] [00 00] [AC 21 02]`
- `data.dat [00] [02 8B 00 10] [FE 12 2C 03]`
(the garbage is not the data file's size, and it's not the header file's size.
Is it the count of items in the header? Or just meaningless bytes...)

The file index follows immediately from offset 0x11. Each entry is 20 bytes:
(words are x86-native little-endian)
```
char[16] - file name including dot-suffix, null-terminated, padded with garbage
uint32 - start offset in the data file
```

Each file runs from its starting offset to the next entry's starting offset.
The final file in the list is a virtual `end` which gives the final closing
offset, which should be equal to the data file's end.


### Custom disk format in older games

Used in JAST games from Tenshitachi no Gogo 3 to Gomen ne Angel.

These look like custom format variations. Each uses a numeric index for game
resources, no file names at all. The exact location of each index varies by game.

Each index entry is four bytes, encoded as `DD SS ls LL`, where:
```
DD - disk number, 01 or 02
sSS - sector index where the file starts
LLl - file length in sectors
```

Example: `02 0A 23 01` means file is on disk 02, from sector 30A, length 012 sectors.
In bytes, that's 0x4800 bytes from offset 0xC2800. The actual file data may end before
the last sector is full; the rest of the sector is garbage.

Each of these games comes on two disks, and the file indexes for both are on the first
disk only, inconvenient for extracting.


##### Gomen ne Angel

The first sector starts with a two-byte jump EB 0D, then a signature
`JAST CO.,LTD.` and the rest of the bootloader. Sector size is 0x400 bytes.

Some curious values at 0x3F2 onward: a sequence from 0x0A20 to 0x0A1A, can't be
offsets. These are at the very end of the boot sector...

This is followed by a bunch of `00` and `01` bytes in sector 1. A resource map
of some sort, or it might be a graphic bitmap, although the values don't form
an obvious image. It looks a bit like something if placed in rows of 7 bytes...
00 01 01 01 01 01 00
01 01 01 01 01 01 01
01 00 01 01 01 00 01
01 00 00 00 00 00 00
00 00 00 00 00 00 00
00 00 01 01 01 01 01
01 01 00 01 00 01 01
01 01 01 01 00 00 00
00 00 00 00 00 00 00
00 00 00 01 01 01 01
01 01 01 00 00 01 01
01 01 01 01 01 01 01
01 01 01 01 01 01 01
01 01 01 01 01 01 01
01 01 01 01 01 01 01
01 01 00 00 00 01 00
01 01 01 01 00 01 01
01 01 01 01 01 00 00
00 01 01 01 01 01 01
00 00 00 00 00 00 00

There's a stray 0x0C at 0x500, and stray 0x06s at 0x900, 0xD00, 0x1100, 0x1500, 0x1900,
and 0x1D00.

There are a few reverse words at 0x2000: 0010, 0020, 0024, 0028. Not file counts...

The resource arrays are at:
- 0x2412: 86 entries (0x56), most on 2nd disk, graphics?
  The first disk ones go from sector 0x12C to 0x1CC.
  The second disk runs from sector 0x001 to 0x442 and a bit, basically the whole disk.
- 0x2612: 88 entries (0x58), all on 1st disk, animations?
  From sector 0x258 to 0x3CD (from 0x96000 to 0xF3400 and a bit)
- 0x2816: 80 entries (0x50), all on 1st disk, scripts?
  From sector 0x64 to 0xF8 inclusive (from 0x19000 to 0x3E000 and a bit)
- 0x2A12: 18 entries (0x12), all on 1st disk, music?
  From sector 0x46 to 0x5E inclusive (from 0x11800 to 0x17BFF).

At 0x2C12 is a series of 4 filenames: GDIR.BIN, ADIR.BIN, SDIR.BIN, MDIR.BIN; each is
followed by two nulls. Presumably these are the names of the previous four arrays.

At 0x2C52 is a series of 16 dwords, the game's static palette, in index-GRB order.
Note that the games use only 8 colors, so the full 16-color palette needed by
the PC98 fills the palette with the same 8 colors twice.

From 0x2C92 there's something that could be code constants?.. This includes the
string "AKIE", preceded by a uint16 length value of 4.


##### Tenshitachi no Gogo 4 Yuko

The first sector starts with a two-byte jump EB 0D, then a signature
`JAST CO.,LTD.` and the rest of the bootloader. Sector size is 0x400 bytes.

There are a few potentially meaningful numbers at 0x2000. Not file counts...

Minimal file indexes can be found on disk 1 at 0x2412, 0x2612, 0x2816, 0x2A12.
Their counts: 348, 148, 328, 76 bytes (87, 37, 82, 19 dwords) (57, 25, 52, 13 in hex)
Each index entry is four bytes, encoded as `DD SS ls LL`, where:
```
DD - disk number, 01 or 02
sSS - sector index where the file starts
LLl - file length in sectors
```

Example: `02 0A 23 01` means file is on disk 02, from sector 30A, length 012 sectors.

The file index for both disks is on the first disk only, inconvenient for extracting.

The first file index lists the second disk's content. Probably all image files.
Start sectors range from 001 to 4A7, the last file being 008 sectors in length -> 4AF.
Sector 4AF stops at image byte 0x12BC00; viewed in a hex editor, the useful data
indeed ends there.

The second file index runs from sector 0x2BC to 0x30A; byte offset 0xAF000 onward.
This looks like image data. Animations, probably.

The third file index runs from sector 0x230 to 0x2B2; byte offset 0x8C000 onward.
This looks like some executable stuff, then script bytecode consisting mostly
of unencrypted, uncompressed Shift-JIS.

The fourth file index runs from sector 0x208 to 0x220; byte offset 0x82000 onward.
This looks like instrument data and note data.

These indexes don't specify what data is before sector 0x208; bytes below 0x82000...
However, all of the space on disk 1 from 0xC800 to 0x80000 is actually unused.

The data from sector 0x200 (0x80000) has all choice verbs and objects, Shift-JIS.
Each takes 16 bytes, running from 0x80006 to 0x81376, or thereabouts; 311 entries,
some of them invalid.

The data in sectors 0x10 (0x4000) to 0x31 (0xC400) must be the game engine code.

The constant region at 0x2C92 onward has the string "YUKO".


##### Tenshitachi no Gogo 3 Bangai-hen

Looks like the same format as above.

Curious values from 0x3F2 onward: a sequence from 0x0A20 to 0x0A1A, can't be
offsets. These are at the very end of the boot sector...

This is followed by a bunch of `00` and `01` bytes in sector 1.

There are a few reverse words at 0x2000: 0010, 0020, 0024, 0028. Not file counts...

Minimal resource indexes can be found on disk 1 at 0x2412, 0x2612, 0x2812, 0x2A12.

At 0x2C12 is a series of 4 filenames: GDIR.BIN, ADIR.BIN, SDIR.BIN, MDIR.BIN; each is
followed by two nulls. Presumably names of the previous four arrays.

The game's static palette starts from 0x2C4C, 16 dwords.

The constant region at 0x2C8C onward has the string "RURI".


##### Yoru no Tenshitachi

Different format than the above games. No recognisable signature in the first disk's
boot sector.

Resource indexes are on disk 1 at 0x400A, 0x420A, and 0x440A. Graphics, scripts, music.

At 0x4A0A is a series of 3 filenames: GDIR.BIN, SDIR.BIN, and MDIR.BIN; each is followed
by two nulls. These are the names of the previous arrays.

The constant region at 0x5CEA onward has some system messages containing ANSI codes,
and the string "YUI" at the end.

Curious values from 0x9D2E onward: Long sequence of words from 0x0A20 downward, but not
strictly decreasing. There is an FFFF and some FEFE's. Is it a binary tree?.. Some of
this data is evidently compressed.

From 0x10000, there's a list of 16-byte strings. This is the game's list of verbs and
nouns. These are encoded as pure double-byte JIS, rather than Shift-JIS. For details,
see the wikipedia article on JIS X 0208.

A conversion to Shift-JIS requires some finetuning, since it's not a linear mapping.
Example codes:
	2121 = 8140 whitespace
	2221 = 819F black diamond
	2330 = 824F latin 0
	2421 = 829F small hiragana a

Notably, ascii characters can be read almost normally in a hex dump, since each is
paired with a # hash but otherwise retains its ascii value.

Here's a small Python script, `conv.py`, for converting plain JIS to Shift-JIS,
then using Python's built-in Shift-JIS decoding to print it as modern UTF-8.

```python
import sys

if len(sys.argv) <= 1:
	print("Usage: conv.py <JIS text file>")
	exit(0)

with open(sys.argv[1], 'rb') as f: data = f.read()
offset = 0
while offset + 1 < len(data):
	i1 = data[offset + 1]
	i2 = data[offset]
	offset += 2
	if i1 <= 32 or i1 >= 127: raise Exception("Invalid first byte: " + str(i1))
	o1 = 112 + ((i1 + 1) >> 1)
	if i1 > 94: o1 += 64
	o2 = i2 + 126 if i1 & 1 == 0 else i2 + 31 + i2 // 96
	print(bytes([o1, o2]).decode("shift_jis"), end='')
```


##### Tenshitachi no Gogo 3 Ribbon

No recognisable signature in the first disk's boot sector.

Resource indexes are on disk 1 at 0x200A and 0x220A.

At 0x280A is a single filename: DIRBLK.BIN.

Extra resource index appears to be at 0x2B7E.

Curious values from 0x92D8 onward: Long sequence of words from 0x0A20 downard, but not
strictly decreasing. There are two FFFF's and several FEFE's.

From 0xC000, there's a list of 16-byte strings, the game's verb list in plain JIS.


##### Cosmos Club

No recognisable signature in the first disk's boot sector.

There's a recource index at 0x40C, but different from later games. 8 bytes per entry,
x86-native little-endian byte order:
```
uint16 - disk index 01 or 02
uint16 - start sector (sector size 0x100 bytes)
uint32 - data length in sectors
```

Example: `01 00 2C 01 45 00 00 00` is a file on the first disk, from sector 0x12C,
data length 0x45 sectors. That is, byte range 0x12C00 to 0x170FF.

At 0xA0C is a series of 3 filenames: DIRBLK.BIN, SCD.BIN, and MSG.BIN. Null-terminated,
and an optional extra null to pad to an even byte offset.

There's some system text including ANSI codes at 0xC2C.

Curious values from 0x4A76: Long sequence of words from 0x0A20 downward, not strictly
decreasing.

At 0x98C5, is the string CSCD.BIN, then more ANSI-flavored text.

At 0x9990, there's another resource index?

From 0x9C00, there's a block of interesting system messages, referencing DIRBLK.BIN,
GRP files, and a `MSG DATA CONVERTER` Ver 2.10.

From 0xAB00, there's a list of 16-byte strings, the game's verb list in plain JIS.
This is followed by a whole lot more JIS text.


### Otaku/G-Collections archive

Used for various Windows localisations, these are recognisable as suffixless
files that begin with a simple file index. Typical file names are MIDI, MRS,
WAP, MA, MO, MGF. Other suffixless archive formats exist, so must evaluate the
header to determine type.

First word: index list size in bytes.
Index list follows, where each entry is a 12 character filename, then offset dword.
Files run from the given offset to the next file's offset.
An empty filename terminates the list.
