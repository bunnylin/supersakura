Crowd game resources
--------------------

They used CWL, CWP, and ZBM graphic formats. Please see `gfx/cwl-cwp-zbm.md`.

##### BDT scripts

XOR the whole file with 0xFF to see the uncompressed bytecode.
