AIL
---

AIL started by publishing the RPG's Maha Barata (1993) and Skirmish (1994),
then switching to visual novels. They kept up a steady stream of releases
well into the 2010's. Their early games are notable for particularly good music.

Graphics formats used:
- ACHv1 (Maha Barata, Skirmish)
- ACHv2

Music formats used:
- PMD
- MMD (Ruriiro no Yuki)

Script formats used:
- SNL

Other:
- MAP, SNB, MDT, CHR (Skirmish)


### AIL archives

These have various normal-sounding file suffixes, but actually contain multiple
files and are only recognisable by a minimal header.

- ANM
- M
- MMD
- GRA, GRP, DAT

Header:
```
uint16 - number of files in archive
uint16[] - byte size for each file; if 0, ignore file
```

These are simple bundles of multiple files with a header of file sizes, then
all file data appended without particular byte alignment. The first file data
begins at offset `(number of files + 1) * 2`. The last specified file size is
typically out of bounds and can be ignored.

The individual files are almost all compressed. The file's first byte is the
compression type, the second is usually 0. Except, gall0a.14 is a 48-byte
palette-onlu file, completely uncompressed.

The rest after the first two bytes is compressed using one of two LZSS variants:
- Compression type 0: LZSS with 3-byte repeats and 257-byte offset
- Compression type 1: LZSS with 2-byte repeats and 17-byte offset
- Compression type 2: ? in Dual Soul
- Compression type 3: ? in Dual Soul

For both algorithms, the flag byte's bits are reversed compared to standard LZSS;
clear bit for a literal byte and set bit for a copy from before.

Flag bits are read from lowest 0x01 to highest 0x80, same as standard LZSS.

Type 0 repeats use 3 bytes: AB CD EF, where
- Copy length is CEF+3 bytes
- Read distance is (current write offset - 257 - DAB) & 0xFFF

Type 1 repeats use 2 bytes: AB CD, where
- Copy length is D+3 bytes
- Read distance is (current write offset - 17 - CAB) & 0xFFF


##### M music bundles

Various games have a single `.m` file which contains multiple PMD songs appended
together. There is no lookup index in the file itself, but the individual song
offsets and sizes can be found in `adv.exe`. In these uint16 arrays, every song
has a uint16 pair, for start offset and byte size.

- Skirmish: 15 songs from 0x2637F onward in `rpg.exe`, starting with `00 00 56 0B`
- Maha Barata: the PMD files are already cleanly separate
- Dual Soul: 15 songs from 0x13FC9 onward in `adv.exe`, starting with `00 00 2A 08`
- Inma Seifuku Gari: 10 songs from 0x15CC9 onward, starting with `00 00 8C 07`
- Kyouhaku: 16 songs from 0xCB81 onward, starting with `00 00 82 08`
- Majogari no Yoru ni: 9 songs from 0xE565 onward, starting with `00 00 90 07`
- Ruriiro no Yuki: these are AIL archives and do have a file index
