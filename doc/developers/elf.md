Elf games
---------

One of the most famous old companies, Elf descended from Fairytale and published
dozens of games between 1988 and 2015. Elf is fondly remembered for the Dragon Knight
series and Doukyuusei, among many other classics.

There is some limited file format overlap between Fairytale's and Elf's early games,
but Elf's technology quickly evolved along its own branch. The later Elf spinoffs
Mink and Silky's also retained some Elf file formats.

- There's some information on Silky's already at:
  https://anivisual.net/blog/2020-05-27-633

- There's a MES script reader for Silky's:
  https://github.com/TesterTesterov/mesScriptAsseAndDisassembler

- Silky's and AI5/AI6 archives:
  https://github.com/TesterTesterov/SilkyArcTool

- Another tool with Elf archive support:
  https://github.com/niksaak/ae/blob/master/src/format/archives/AA_elf.pas

- Maybe good commandline tool for MES compile/decompile:
  https://github.com/tomyun/juice/tree/main/mes

- More Elf/Silky's tools, and even an AI5Win engine reimplementation somewhere in there:
  https://github.com/nunuhara/elf-tools


##### Engine names

- Adv?
- AI1 (Angel Hearts, Pinky Ponkies, Run Run Kyousoukyoku, DK1, Foxy 1, De-Ja 1)
- AI2 (Ray-Gun, DK2)
- AI4 (Foxy 2, Elle, Shangrlia)
- AI5 (DK3 through Yu-No)
- AI5Win (various games from Isaku 1997 onward)

There are apparently no games using an AI3 engine.


##### Archive formats

- Doukyuusei 2 has an `elfsys.dat`; some kind of LZSS is involved, obvious at the end
- Isaku has multiple `isaku_*` files, probably the same format...
- Kakyuusei has `kayusei.arc`, different format; obviously encrypted
- Yu-No has multiple `yuno_*` files...


##### Music formats

Described in [doc/mus/elfm.md](../mus/elfm.md).

- MU
   * Angel Hearts
   * Pinky Ponky 1, 2, 3
   * Run Run Kyousoukyoku
- M
   * Dragon Knight 1, 2, 3
   * Foxy 1, 2
   * RayGun
   * Deja 1, 2
   * Elle
   * Shangrlia 1
   * Tenshin Ranma
   * Jan Jaka Jan
   * Doukyuusei 1
   * Curse of Castle
   * Metal Eye 1
- MDT
   * Shangrlia 2
- MUS (looks the same as MDT?)
   * Words Worth
- M (new, looks the same as MDT/MUS except without the useless 256-byte preamble)
   * Dragon Knight 4
   * Metal Eye 2


##### Graphics formats

Described in [doc/gfx/pr6-pr7-pd8.md](../gfx/pr6-pr7-pd8.md).
MLD can view all of these. Grapholic can view PD8 and GP4.

Source code for reading all these is in [/inc/gfx/ada.pas](../../inc/gfx/ada.pas).

- PR6 v2 + ANI
   * Angel Hearts
   * Pinky Ponky 1, 2, 3
   * Run Run Kyousoukyoku
   * Dragon Knight 1
   * Foxy 1
- PR7 + ANI
   * RayGun
   * Dragon Knight 2
- PR6 v3 + ANI
   * DeJa 1
- PD7 v1 + S4
   * Foxy 2
- PD7 v2 / PD8 + S4 + A4 / A5 / A6
   * Elle (S4, A4)
   * Dragon Knight 3
   * Shangrlia 1
   * Tenshin Ranma
   * DeJa 2 (S4, A5)
   * Jan Jaka Jan
   * Doukyuusei (S4, A6)
   * Curse of Castle
- GP4 + S4 + A6
   * Metal Eye 1, 2
   * Words Worth (there are also ADA files, not same as the old kind)
   * Shangrlia 2
   * Dragon Knight 4


##### Dragon Knight 1

The dungeon/tower is 6 floors of 18x18. Monsters appear in limited pre-defined
patterns; new monsters will join certain patterns, always on the same battle turn.
Each floor has its own monster patterns, and some areas of a floor may have
different patterns. Encounters can happen as often as every step, but can also go
a good fifty steps without anything. Perhaps something like a 1/25 chance on each
step? Unclear if location or events change encounter rate.

The dungeon walls are paper-thin. There are doors that can be passed through and
make a door noise. Some doors remain impassable. Some doors become passable under
some condition, such as finding a key. Individual event tiles trigger when stepped
on, and are not visually distinct; lawn mowering may be required to find some
events, although most are placed in deadends or immediately behind doors. An event
tile can push the player backward under some conditions, such as not knowing the
right password. There are lightning trap tiles that do about 10-16 points of
damage. There are illusionary walls that can be passed through.

The town locations get new dialogue based on quest flags. The temple heals one HP
for one gold, and one MP for one gold, and only offers to heal all HP or all MP.
Bought equipment directly increases strength and defense. Each equipment level
increase is about as helpful as a single level-up at most, although a few upgrades
only give a measly single-point improvement, which could be an error or just the
game trolling the player.

Strength and defense seem to somewhat directly count as the attack and defense values,
where the target's defense is subtracted from the attacker's attack. If the result is
0 or less, the attack misses, otherwise does so much damage. Attacks over multiple
rounds have some variation in damage output, but not much, perhaps 10% variance.
Except some low-level mooks on the first floor can still deal a few points of damage
even after the player has 100+ defense, so it's not just a direct attack less defense.
Another weird special case is that a monster doing consistent low damage may suddenly
switch to unpredictable damage, effectively sometimes doing more damage than they used
to when you had lower defense; surely a flaw in the attack calculation. Unless the
intent is that when such a monster's normal attack becomes ineffective, it starts
doing from 0 to (2 * floor + 1) unblockable damage, to retain some level of threat.
Boss monsters particularly deal totally consistent damage, the same every turn, so you
know exactly when is the last moment to heal. Some monsters of a given type may be
weaker or stronger when encountered in a boss pattern.

Monster don't have special attacks; everything just bonks once for some damage, except
boss dragons, which attack multiple times, equal to the dragon's floor number.
The player always acts first in a combat turn, then the monsters in a fixed order.
On the PC98, you can just hold down enter to bash through a battle as quickly as
possible, if the monsters aren't threatening, which makes grinding more tolerable.
However, monster patterns typically place the weakest monster up front, so you can't
hold enter without suffering extra damage from lower-positioned monsters unless you
are strong enough to take 0 damage from everyone.

The player has a magic attack which costs maybe (2 * player level) + 1 MP? The attack
hits every enemy with the player's normal attack value plus about 10% extra. The player
has a healing spell which costs (player level / 2) + 1 MP? which normally always heals
up to maximum HP. But in some situations it may only heal about 90% of maximum HP?
The player can flee from battle with about 50% success rate; on failure, all monsters
get a free attack. Escaping just exits the battle and doesn't move the player.

The encounters are balanced so that after clearing each floor, the player should
grind about one extra level and one extra piece of equipment to be able to advance
through the next floor without being very quickly wiped out. Attack and defense
advancement is somewhat dramatic, as a single extra level can mean the difference
between taking 20 damage each attack, or only 8, a massive difference in survivability.
Also, at a level or two less than intended, you may get wiped in a single combat turn.
Monsters yield fixed rewards? The level cap is 25, reached at 60000 experience.


###### DK1 MON files

These encode monster information, one file for each tower floor, another for boss
group encounters, and another for dragon encounters.

Each entry is 50 bytes:
```
char[32] - monster name, padded with nulls or garbage; terminates with 0x7D in the
        original; in the English patched version, the names are in double-quotes,
	so ignore 0x7D
char[8] - monster graphic filename, including dot-suffix, padded with nulls
uint16 - unknown, but bigger is more powerful
uint16 - unknown
uint16 - unknown
uint16 - unknown
uint16 - unknown
```

From 0x1F4 onward there's something else, perhaps group and action data...


###### DK2 MON files

The monsters now have some special moves, so the data file is more complicated.

mon1.dat:
- uint16: 0x0008, start offset of first monster info
- uint16: 0x014A, start offset of second monster info
- uint16: 0x0400, start offset of third monster info
- uint16: 0x027E, start offset of fourth monster info

Monster info:
```
char[6] - Basename of basic defeat picture, padded with spaces, no suffix
char[18] - Monster name, terminates with 7D00, padded with 8140 spaces
uint16 - unknown, 0x0001 or 0x0019; not an offset
uint16 - unknown, 0x000b or 0x00d3, not an offset
uint16 - unknown, 0x000b or 0x00d3, not an offset
uint16 - unknown, 0
uint16 - unknown, 0
uint16 - unknown, 0x0001 or 0x015e, not an offset
uint16 - unknown, 0x0008 or 0x01aa, not an offset
uint16 - unknown, 0x0006 or 0x00f5, not an offset
uint16 - unknown, 0
uint16 - unknown, 0x0003 or 0x0226, not an offset
uint16 - unknown, 0
uint16 - unknown, 0
uint16 - first action offset 0x0048
uint16 - second action offset 0x006b
uint16 - third action offset 0x0094
uint16 - fourth action offset 0x00bb
uint16 - fifth action offset 0x00de
uint16 - sixth action offset 0x00ff
uint16 - seventh action offset 0x011c
uint16 - eighth action offset 0x0133
char[] - action description strings, shift-jis, terminates with a single 0x7D
```

###### DK1 FLOORx.dat files

The first 18x18 bytes are the map for each floor, one byte per tile. Bitfield:
- 01: wall on South
- 02: door to South
- 04: wall on West
- 08: door to West
- 10: wall on North
- 20: door to North
- 40: wall on East
- 80: door to East

If a direction has both a wall and door bit, it's a passable illusionary wall.
Since each tile defines its own walls, it's possible to have a door or illusionary
wall that only goes one way and has a solid wall if you try to return. This is
seen in the southeast corner of the 1st floor, for example, which is where the
trap stairs drop you from the 5th floor.

The next 18x18 bytes are the event map, one byte per tile. If the value is not 0,
stepping on that tile triggers said event index. For example, the southwest corner
of the first floor has event 1, which offers an exit back to town. Event indexes
start from 1 on each floor.

It's unclear what the remaining 100 bytes are. These have some FFFF_FFFF values,
suggesting each entry is four bytes for a total of 25 entries, with some unused.
But these can't be script offsets for events, since that would match clearly
invalid values with some events. Perhaps an encounter table?


###### DK1 ITEM.dat

This contains PR6 images for equipment icons. Each is 252 bytes, padded with nulls,
and terminating with a single 1A eof.


### Doukyuusei research by Hyperkwu

There's a bunch of research on Doukyuusei here: https://hyperkwu.tripod.com/class.html

Since that site hasn't been updated for years, and is on Tripod, there's a risk the
research will be lost. So for safekeeping, here's a copy of the research, translated.

Doukyuusei room

In this room, we will analyze the PC-98 version of "Doukyuusei" for me!

If you have any information that would be useful for this section, please submit it here.

--------

Part 1: Data

In this first part, we will discuss data. Data here refers to files other than program
files. There are indeed more than 1000 files in Doukyuusei.

Data Files:
- MES: Message File
- PD8: Graphic File
- S4: Animation Data
- PAL: Palette File
- A6: Mouse Data
- C5: Map Data??
- MP: Map Data File
- M: Music File
- MOUSE.DAT: Mouse Cursor Data
- FLAG0: Saved Data File

The easiest to analyze is the PAL file, which contains the palette information as is.
PD8 is being analyzed, but only the coordinates and palette information are understood.

MES files are the core of the game, and are tough to analyze. I understand the first 2 bytes.
Usually, the message is 6x, but if you add 20 to every byte, it appears like solid SJIS,
but that's not enough.

S4 I've found out is animation data with a certain loader, but the internal details
remain unknown.

M files can be played in a shell by typing `PLAY5 XXX.M`. This shows it is a music file.
The format can't be understood without disassembling the program.

The format of A6 wasn't obvious, but I found it contains mouse information. Details will
be explained in part 6. MOUSE.DAT is mouse cursor information by its name, and contains
cursor shape data.

MP is probably map information.

There is a C5, but since the files share names with MP files, they could be MP movement
data (for example, moving a train).

FLAG0 is save data.

--------

Part 2: Programs

There are only four programs, including the installer.

Program Files:
- AI5.EXE: Main program
- AMD.COM: Animation Mouse Driver
- PLAY5.COM: BGM Player TSR
- INSTALL.EXE: Install program

AI5.EXE is the main program of Elf games, and AMD.COM and PLAY5.COM are normal TSRs.
It doesn't matter whether you have install.exe, if you already installed the game, so we
can ignore that in this part.
AI5.EXE could not be disassembled well by disassembler, and it is hard to follow it
by DEBUG.EXE because it is PACKED.

--------

Part 3: Save data

"+xx" means an offset address.

This is the most troublesome save data analysis. I don't know why I chose the hardest
save data as the first one. Once you know this, you don't even have to play the game.
It's laborious to save, exit, and check the file all the time, so I used the super famous
resident memory editor "STAND-DX" to examine the game's memory.

For more information on STAND-DX, please refer to the NON-STANDARD homepage.
HyperEGNet has also created a modification corner using STAND-DX. By the way, the
password near the save data is ID=AI5 PASSWD=0ECD0A.

The first few bytes (10 bytes) are the active MES file at the time of saving. In fact,
the current MES file is included in the game, but unless there is something unusual
(e.g., you saved with a trick save function), the saved data should be myroom.mes.

Next is 00. It seems file names are separated by this 00 in the game. Moving on,
there are flags, which are set when you click with the mouse. For example, when you click
on the desk in your room, the message changes between the first and second clicks.
That is because the first click changes the flag. I haven't studied this flag in detail,
but I think it is between +25H and +51H. This flag changes each time the scene changes,
so I haven't really looked into it too much.

The days of the week are 00 for Sunday, 01 for Monday, and 02 for Tuesday...
The time and date are a bit special: August 10 is 810, or 32A in hexadecimal.
The time is 1030 for 10:30 a.m., 406 in hexadecimal, and 22:30 for 10:30 p.m.,
recorded as 2230, which is 8B6 in hexadecimal, and so on.

Note: This is saved in LSB format, so the bytes will be in reverse: B6 08.

```
Offset  Data size  Content
+00     0AH        Fixed text myroom.mes
+0C     01H        Fixed character $
+25     2CH        Fixed 00 bytes
+52     08H        Likeability (love points?)
+228    02H        Money held
+22A    02H        Day of the week
+230    01H        Day of the month
+248    02H        Time
+250    02H        Mouse X + 8
+252    02H        Mouse Y + 8
+26A    02H        Mouse X
+26C    02H        Mouse Y
+31D    08H        Name (SJIS)
+BF9    07H        Fixed text TAM・TAM (halfwidth)
```

Data is saved in LSB byte order.

Likeability for each person:
- +53 high: 斎藤 亜子 Ako Saitou
- +53 low:  鈴木 美穂 Miho Suzuki
- +54 high: 黒川 さとみ Satomi Kurokawa
- +55 high: 成瀬 かおり Kaori Naruse
- +55 low: 田町 ひろみ Hiromi Tamachi
- +56 high: 正樹 夏子 Natsuko Masaki
- +56 low: 斎藤 真子 Mako Saitou
- +57 high: 桜木 舞 Mai Sakuragi
- +57 low: 芹沢 よし子 Yoshiko Serizawa
- +58 high: 田中 美沙 Misa Tanaka
- +58 low: 仁科 くるみ Kurumi Nishina
- +59 high: 真行寺 麗子 Reiko Shingyouji
- +59 low: 佐久間 ちはる Chiharu Sakuma
- +5A low: 草薙 やよい Yayoi Kusanagi

The space next to "Satomi Kurokawa" is empty, but it is assumed that this used to be
"Kyoko Sakuragi," which was rejected. The graphic is still present.

The following is a list of flags that indicate whether a person has appeared.
If this flag is set, you can see them on your computer, but please note that there
are other flags as well.

```
PC Flag           +77 10
Miho Suzuki       +75 01
Ako Saitou        +75 10
Satomi Kurokawa   +78 10
Hiromi Tamachi    +7B 10
Mako Saitou       +8E 10
Misa Tanaka       +8F 10
Natsuko Masaki    +95 11
Yoshiko Serizawa  +97 10
Mai Sakuragi      +9C 01
Kurumi Nishina    +9D 01
Kaori Naruse      +8C 10
Chiharu Sakuma    +107 10
Reiko Shingyouji  +135 01
Yayoi Kusanagi    +14B 01
```

Order of addresses & order in which they appear on the computer.

The flags in the game going forward are complicated and I will explain them together
with the MES file.

--------

Part 4: Palette file

A palette is a *.PAL file. There are five of them.

- ASA.PAL: morning
- HIRU.PAL: daytime
- YUGA.PAL: evening
- YORU.PAL: night
- PAL.PAL: ???

They are 48 bytes long, but they're not normal RGB files. The palette is in BRG order.
In fact, only 9 bytes from +1B to +24 are different. If you rearrange them into palette
numbers, only 3 of them (9 to B) have differences. However, I don't know where PAL.PAL
is used, so I'll update it when it's understood. The following is a program that reads
this palette and changes the colors on the screen. However, it is for PC-98, uses the
C language, and is not very easy to use. The executable file LP.EXE is here, 6950 bytes.

```
/*  LP.EXE  */
#include <stdio.h>
#include <stdlib.h>

#ifndef outportb
#define outportb(p, c)\
    _asm_c("\n\tOUT\tDX,AL", (char)(c), _asm_c, _asm_c, (unsigned)(p))
#endif

FILE *fp;

main(int argc, char **argv)
{
        char i=16;
        if(!argc) exit(0);
        *++argv;
        outportb(0x6A,0x01);
        outportb(0xA2,0x0D);
        if((fp = fopen(*argv,"rb")) == NULL) exit(-1);
        while(--i){
                outportb(0xA8,15-i);
                outportb(0xAE,fgetc(fp));
                outportb(0xAC,fgetc(fp));
                outportb(0xAA,fgetc(fp));
        }
        fclose(fp);
        exit(0);
}
```

Now you can run `A:> lp asa.pal` on the command line, and the palette will load nicely.
Some loaders do not load this PAL file, so you may want to use it after loading the graphics.

--------

Part 5: Mouse Cursor

The shape of the mouse cursor is in MOUSE.DAT. I would like to upload the graphic, but
I don't want to break the copyright. The format of this data is to read 4 bytes (32 bits)
at a time, and if the bit is 1, output a white dot; if the bit is 0, output a black dot;
and the bitmap will appear. The mouse cursor size is 32x32. 

```
Data (hexadecimal)   Cursor bitmap (binary)
00 00 00 00          --------------------------------
00 00 00 00          --------------------------------
20 00 00 00          --0-----------------------------
18 00 00 00          ---00---------------------------
1E 00 00 00          ---0000-------------------------
0F 80 00 00          ----00000-----------------------
0F E0 00 00          ----0000000---------------------
07 F8 00 00          -----00000000-------------------
```

This is a partial cursor, very easy to understand when viewed in binary.
I attached a sample program. As a sample program, it doesn't work as is, but the
executable file MOUSE.EXE is here, 9360 bytes.

```
#include <stdio.h>
#include <stdlib.h>

FILE *fp
/*  pset(unsigned int x, unsigned int y, char color);  */
main(){
    int c,i,j,k;
    if(NULL == (fp = fopen("mouse.dat","rb"))) exit(-1);

    // (...omitted...)

    for(i=0;i<32;i++){
        for(j=1;j<5;j++){
            c = fgetc(fp);
            for(k=0;k<8;k++){
                pset(j*8-k,i,(c & 1)*7);
                c = c >> 1;
            }
        }
    }
    getch();

    // (...omitted...)
}
```

Cursor bitmaps are in the order: normal 1, mask 1, normal 2, mask 2...
In the game, the mask data is displayed with AND and the normal data with OR.
Since these are animated cursors, there are two frame bitmaps, and when combined
with the masks, there is a total of four bitmaps grouped together.

--------

Part 6: A6 file

Speaking of A6, I had no idea what it was used for until recently, but I finally figured
it out. It is mouse position information. Normally, the mouse cursor is an arrow, but the
shape changes depending on location. A6 contains information about the locations.
This is hard to explain in text, so I tried to make a bullet list.

1. The first 2 bytes represent an ID number.
2. The next 2 bytes each are "upper left X", "upper left Y", "upper right X", "upper right Y".
3. If the next 2 bytes are the same as the previous ID, continue with that ID.
4. If not, the next coordinates are for the next object.
5. When FF is encountered, the file ends. 

Further explanation: An object is a clickable object on the screen. It can be a face or
an eye. An ID number is given to the object, abstractly. If the object is a rectangle,
it's easy to give it a single set of coordinates, but if it's a circle or something,
the object must be divided into several pieces, and each piece gets its own coordinates.
However, since it's a single object divided into several rectangles, they must still all
return the same message, so they have the same ID. The figure below illustrates this.

```
  ,-----,
 /       \
| +-+ +-+ |
| |3| |3| |
| +-+ +-+ |
|  +---+  |
|  | 1 |  |
 \ +---+ /
  '-----'
+---+-+---+
| / | | \ |
|2| | | | |
| | |2| | |
| | | | |2|
| \ | | / |
+---+-+---+
```

In this case, object 1 is singular, so we only need to specify coordinates for one rectangle.
Object 2 is divided into three parts, so we need to give the coordinates three times.
To give the coordinates three times, we have to give the coordinates of number 3 three times
in succession, as shown in step 3. Object 3 must be specified twice as well.

If you want to know more details, please e-mail me. (^^;;

--------

Part 7: S4 File for Animation

S4 contains coordinates and other data necessary for animation. Only the coordinates are
understood.

1. Find 53H in the file.
2. The next 1 byte x8 is the X coordinate of the upper left corner of the copy source.
3. The next 1 byte is the Y coordinate of the upper left corner of the copy source.
4. The next byte is unknown.
5. The next 1 byte x8 is the X coordinate of the lower right of the copy source.

However, as you can see when loading an image with a graphic loader, the graphic and
the animated graphic may overlap if both are displayed on the same screen
(e.g., L.PD8 and LAN.PD8). In that case, Step 1 is a little different. You have to
instead find 12H in the file.

--------

The information in this section is based on my own personal research,
so please do not contact elf. Please understand that I cannot be held responsible
for any mistakes that may have been made.

(end of Doukyuusei research by Hyperkwu)
