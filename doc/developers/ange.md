Ange
----

A slow and steady game developer, Ange started releasing about one PC98 game
each year from 1991, then picked up the pace, averaging three Windows games
each year 1997-2001. Ange is notable for having a strange penchant for
four-letter game names.

Ange also owned Sorciere, active 1996-2000. Sorciere made the Strange World
games, and a few others, using the same engine as Ange.

The first two Ange games Rose and Kiss are floppy-boot only, and don't have
a recognisable usual file system. Supporting these requires figuring out
how to extract the game data from the disk images in the first place.

Game engines used:
- ADVIZ 1.0 (Rose?, Kiss)
- ADVIZ 1.10 DOS type (Like)
- ADVIZ 2.00 (Kaiketsu Nikki)
- ADVIZ 3.00 (School Festival, S-Express, Arbeit, Leap, Coin, all Sorciere games)

The game and music engines contain text identifying H.Izumino of Syscom as
all their creator.

Graphics formats used:
- GIZ (Rose)
- GIZ2 (Kiss, Like, Kaiketsu Nikki, School Festival, Arbeit, Leap)
- GIZ3 (S-Express, Coin, Geo Slave, Persona, Strange World 1 and 2)

Music formats used:
- MIZ (Rose)
- MIZ2 (all other games)

Script formats used:
- ADV (Like, Kaiketsu Nikki, School Festival)
- PRS (S-Express, Arbeit, Leap, Coin, Geo Slave, Persona, Strange World 1 and 2)


### Rose

Examining the first disk FDI image, there is a minimal file index at 0x4C00.
Signature: `ROSE   1`, then a null dword, then an unknown dword, and finally
a list of file locations, where each file is a pair of uint16's:
the starting sector, and length in sectors.

The recognisable signature "MIZ1" appears at the start of some of these files,
for example at 0xD2400. The contents of these files are very suggestive of
a music markup language, so these are probably the game music.

There's also plenty of recognisable Shift-JIS on the disk, for example in
the sector starting from 0x6C00.

The first non-Shift-JIS sector starts at 0x3C400. The next after this is
at 0x3E800, then 0x40C00, then 0x43400. These are obviously the graphic files.
Rose graphics usually use a size of 416x272.

Header:
```
char[2] - signature "GI"
uint16 - unknown, 0001
uint8 - unknown, 00
uint8 - image pixel width / 8
uint16 - image pixel height
```

The compressed image stream presumably follows immediately.


### Kiss

Examining the first disk FDI image, there is a minimal file index at 0x2C00.
Signature: `KISS   1`, otherwise the same as Rose's index.

There's a readable note at the start of the file:
`KISS by Ange, IZBIOS ver.1.01 ADVIZ  ver.1.00`
And a few sectors further, more text:
`IZBIOS  Version 1.02 Copyright (c) 1992 H.Izumino`

Music files are now marked with the signature "MIZ2". Images are "GIZ2".

Every file is preceded by 4 extra bytes. It seems they're all compressed.
The first uint16 is the uncompressed size in bytes, the second uint16 is
the compressed size. The compression scheme looks like some kind of LZ
with 16-bit flag words.
