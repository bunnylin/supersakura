Discovery
---------

##### BT1 and MI_ files

Injuu Dateshi pre-installation has some MI_ files, which look simple:
First dword is compressed data size, second dword is uncompressed size,
then compressed data from byte 9 to end of file. The compression looks
like standard LZ, possibly LZSS. These are obviously midi files.

The BT1 files are compressed the same way but are standard RIFF WAVs.
LZ compression does very little for these.


##### LST and DAT files

LST is the file lookup list, where each item is:

```
char[14] - Filename including suffix, padded with zeroes
uint32 - Start offset
uint32 - File size
```

DAT contains the file data.


##### TAG files

Some kind of lookup index, 16 bytes per item. Injuu Dateshi has a TAG file
with 581 entries. There are also 581 entries in the LST index.
It's unclear what the relationship is.


##### GMD

A modified midi format. See ValleyBell's research:
https://github.com/ValleyBell/MidiConverters/blob/master/gmd2mid.c
