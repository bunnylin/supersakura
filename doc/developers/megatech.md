Megatech ports
--------------

Megatech was an early Japanese game localiser and porter. They took some
liberties with the game mechanics while at it. They managed to release four
games before inevitably going out of business:
- Cobra Mission (by Inos)
- Metal & Lace (Ningyou Tsukai by Forest)
- Knights of Xentar (Dragon Knight 3 by Elf)
- Power Dolls (by Kogado)

Because these games are all from different companies, they don't necessarily share
file formats with each other.

- Game data files are packed in VOL archives. These are already somewhat documented
  by BlackStar at:
  https://moddingwiki.shikadi.net/wiki/MegaTech_VOL_Format

- BlackStar also has some preliminary file parsing code here:
  https://github.com/BlackStar-EoP/cobra-mission-writer

- dascandy made a graphics converter:
  https://github.com/dascandy/cobra


### VOL archives

Just a simple array of uint32 start offsets, strictly ascending order, may include
empty files. There are no filenames provided for the contained files, and file types
must be inferred from the file data or archive name.

- If file data begins with "GC", it's a Cobra Mission GC graphic
- If data begins with "GPH", it's a GPH graphic
- If data begins with "EM", it's an EMI music file
- If data begins with "MD", it's a map data file
- If data begins with "Creative Voice File", it's a standard old VOC file.
- If data begins with "IC", it's... script?
- If data begins with "CX", it's... script? or text?
