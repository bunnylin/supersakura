Excellents games
----------------

Excellents was the parent company of Apricot (95-99), Desire (94-2000),
M' (97-99), Pearl Soft (95-99), Pis-Ton Soft (97-98), and Seal Staff (97-99).
Furthermore, Fournine (92-97) was owned by Apricot, somehow.
Of these, Apricot is known for Nocturnal Illusion, and Desire for May Club.
Fournine famously focused on anthropomorphic animal characters.

Some of this is already documented by VileVN:
https://vilevn.sourceforge.net/doxygen/classEngineWindy.html

A great deal of additional information and tools by ValleyBell:
https://github.com/ValleyBell/PC98VNResearch/tree/master/four-nine_system98


### Engine names

On English Windows ports, the files `may_sub.s` and `mug_sub.s` begin with the
signature `WindySub`, suggesting this might be called the "Windy" engine.
However, the PC98 versions start with just `Sub:`, so Windy probably refers
to Windows, leaving the base engine name unknown.

There's more than one engine at play across these companies. Looking at the
executables, some messages can be found (some LZ-compressed, so a bit unclear):
- Lixus (Fournine, 1992) `sys98a.exe`: "MUSIC LALF" Copyright TOKUMASHOTEN INTERMEDIA INC.
  1991 "ANNEX" Copyright 1987,88,89,90,91 by Ana. SYSTEM98 TYPE-A Version 1.00
  Program By.F.Ohtsuki
- Neko Manma EX (Fournine, 1993) `sys98b.exe`: System98 TYPE-B Version 0.E
  Copyright (c) 1993 By F.Ohtsuki
- Night Shifter (Fournine, 1993) `sys98c.com`: SYSTEM-98 TYPE-C full assembler version 1.1
- GaoGao 1 (FourNine, 1994) `sys98cpp.com`: SYSTEM-98 TYPE-C++ Assembler Version 1.0
- GaoGao 2 (FourNine, 1994) `sys98sx2.com`: SYSTEM-98 TYPE-C++ Assembler Version 1.1
- GaoGao 3 (FourNine, 1994) `sys98fm.exe`: System-98 version 3.00 (c) Copyright 1993,1994
  fournine/Izuho Saruta.
- H+ (Desire, 1994) `des98.com`: Des 98 Special Version.. Copyright 1993,1994
  by desire.... Programed and modified by Madam Jolly   in 1994.
- Wakuwaku Mahjong Panic (Fournine, 1995) `naval.exe`: LZ91-compressed, after unzip:
  MAIN SYSTEM Ver. 1.00
- Mugen Yasoukyoku (Apricot, 1995) `sys98.exe`: System-98 version 3.10 (c)
  Copyright 1993,1994 fournine/Izuho Saruta.
- Mayclub (Desire, 1995) `sys98.exe`: desire-system version 3.12 Copyright (c) 1993-1995
- Lilith (Fournine, 1995) `sys98.exe`: System-98 version 3.12 Copyright (c) four-nine 1993-95
- Angel Night (Fournine, 1996) `dshell.exe`: LZ91-compressed, after unzip: PC-98 D-SHELL
  v0.92 Copyright 1995,96 by Yuki Narita/fournine
- Yuugiri (Desire, 1996) `d2.exe`: LZ91-compressed, no identifying text even after unzip
- Saint Diary (Desire, 1996) `d2.exe`: same
- Wakuwaku Mahjong Panic 2 (Fournine, 1996) `sonnet.exe`: WYf sonnet (C)1994-6
  DAIMA Factory inc.
- Yatsu no Na wa Diamond (Pis-Ton soft, 1997) `dd1.exe`: many "Kasumi" copyrights,
  no engine name apparent
- GaoGao 4 (Fournine, 1997) `sys98.exe`: LZ91-compressed, after unzip: System-98
  version 3.10 (c) Copyright 1993,1994 fournine/Izuho Saruta.

- Cyber Illusion (Pearl Soft, 1995) `ae.exe`: Auto Expander pLib 1994 fournine;
  `ps1sys.exe`: MS Run-Time Library - Copyright (c) 1992, Microsoft Corp
  Copyright (C) 1995 by PEARL SOFT/Eagle Eyes

Therefore, presumed engine names:
- System-98 (old) by F.Ohtsuki
- System-98 (new) by Izuho Saruta, shared with other Excellents companies
- Des 98 could be the same, or a separate engine...
- D-Shell and D-Shell 2
- Multi-exe used by the Wakuwaku Mahjong Panic games, let's call it "Diana"
- Retro multi-com thing used in all Pearl Soft games: let's call it "Pearlcom"
- Kasumi's engine used by Pis-Ton Soft


### Pearl Soft

Made the games Cyber Illusion, Magical Panic, Watashi, Sweet Days.

All Pearl Soft games on the PC98 appear to use the same engine. The game scripts are
in com files, music is in fm and md files, and the graphics are in pl4 format. The
images may be packed in data0x.ary archives.


### File formats

General-purpose suffix, not obvious what file contains without trying to decode:
- Suffixless (GaoGao 2, HPlus)
- FN1 (Neko Manma EX)
- LSP (Night Shifter, GaoGao 1, 2, 3, and 4, Wakuwaku2)

- If the first 3 bytes are `00 1A 00`, it's a PMD music file.
- If the words at offset 2, 6, and 10 are strictly ascending and less than the file
  size, it's probably an MMD music file.
- If the first 256 bytes are 0, it's a script file.
- If the first 2 words are non-zero but <= 640 and 400, and bytes 7 to 9 are zero,
  it's probably a Pi graphic.

Scripts formats used:
- SDT/PAT (Lixus)
- Unknown format (Neko Manma EX)
- System-98 script (all PC98 titles from Night Shifter and GaoGao 1 onward?)

Graphic formats used:
- CGL (Lixus)
- EFD (Wakuwaku1)
- DPC/P (Yuugiri, Saint Diary, Angel Night, Wakuwaku2)
- G/Pi (All the rest of them)

Music formats used:
- MLO (Lixus)
- M/PMD (all PC98 titles except the above)
- MMD midi (GaoGao 3 and 4, Lilith)
- HMM midi (Yuugiri, Saint Diary)
- OPI/OVI (Yuugiri, Saint Diary)

Archive formats used:
- ARY (Pearl Soft games)
- FN1 (Neko Manma EX)
- DAC/ARC (Yuugiri, Saint diary, Wakuwaku2)
- DLB (Angel Night)
- DAT (Wakuwaku2)
- CAT+LIB (Nocturn, MayClub, GaoGao 3 and 4, Lilith)
- DAT+LST (Nocturn and MayClub on Windows)


### May Club

The .DAT contains many graphics whose filenames have no suffix. Just treat
them as .G files. There are a few suffixless files also outside the .DAT;
except for SVS and SVX that I see no point for, they appear to be identical
to the graphics inside the .DAT, and could be ignored.

The PC98 version has some .grp and .gld files, which I suppose are also
graphics, but I don't recognise the compression method, nor does Grapholic.

Three graphics are not regular .G files. They are full Pi files instead, and
can be identified by a "PiUser" signature.


### Nocturnal Illusion Renewal

There was a remake in Japan: Nocturnal Illusion Renewal "Voice Version"!
It has beautifully improved graphics, remixed music, and voice acting.
However, the graphics are mosaiced at key points. Standard Ikura GDL engine?

- BGW: music
- GRP: contains an index and .GGD graphics
- OTHERS: cursors
- SE: sfx
- SNR: scripts
- V1 and V2: voice (only on second cd)


### May Club DX

There was a remake in Japan: May Club DX. It has improved graphics, music,
and voice acting, as well as an extra character and some script changes.

Graphics are in GRAPHIC.PAK, which contains an index table of resources,
followed by concatenated .BET files. .BET files appear to use SZDD
compression. TEXTURE.PAK seems to have more graphics, perhaps animations?

Sound effects are in SE.PAK, same index format, standard RIFF waves. Voice is
found in VOICE.PAK, same index. Terribly wasteful.

Game script is in SCENARIO.PAK, again with an index first. S21-files follow.
The script files appear to be unobfuscated, uncompressed bytecode. Commands
seem to appear as zero-terminated ascii strings...
