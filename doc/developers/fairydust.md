Fairy Dust
----------

Fairy Dust was an adult anime publisher in the 80's. In addition to
merchandising, they branched into computer games, many based on their
existing animes. The first was a Cream Lemon episode adaptation in 1986,
then a larger variety from 1993 onward. Since Fairy Dust was primarily
a publisher, about half of their games were actually developed by
different companies. As a result, there's wild variance in the engines
and file formats used.

Graphics formats on PC98:
- CCG, CTL (Lemon Angel)
- L+RGB (Ami Kaze Tachinu)
- OT (Koukan Nikki 1 and 2, Ma Doll)
- RAL+ANI (Rall 3)
- SGV (Akai Suishou no Hitomi, Uki Uki Island)
- VGR (5 Jikanme no Venus)
- YTG (Virgin 2)

Graphics formats on Windows:
- OT (Koukan Nikki DX)
- MOT (Koukan Nikki DX, Ma Doll)
- MGO (Escalation Hardcore)
- I24 (Ami Shoushin no Tenshi)

Movie formats on Windows:
- MDT (Ami Shoushin no Tenshi)

Music formats on PC98:
- KMM midi files (Koukan Nikki 1)
- KMD FM files (Koukan Nikki 1)
- Standard MID files (Koukan Nikki 2, Ma Doll, Virgin 2)
- M PMD files (Ami Kaze Tachinu, Rall 3, 5 Jikanme no Venus, Akai Suishou no Hitomi,
  Uki Uki Island)
- MMD midi files (Rall 3, Akai Suishou no Hitomi, Uki Uki Island)
- FMX FM files (Lemon Angel)
- MMX midi files (Lemon Angel)

Sound formats on Windows:
- MID midi files (Koukan Nikki DX, Ami Shoushin no Tenshi)
- WAV music and speech files (Koukan Nikki DX, Ma Doll, Escalation Hardcore)

Script formats on PC98:
- CLA (Lemon Angel)
- SCP (5 Jikanme no Venus, Akai Suishou no Hitomi, Uki Uki Island)
- SCR (Koukan Nikki 1 and 2, Ma Doll)
- SRT (Virgin 2)
- TDT (Ami Kaze Tachinu)
- VI2 (Rall 3)

Script formats on Windows:
- SCR (Koukan Nikki DX, Ma Doll, Ami Shoushin no Tenshi)
- MSO (Escalation Hardcore)

Archive formats on PC98:
- CHM+HED (Star Trap)
- PCK (Escalation '95)


### SGV graphics

These are standard MAG v2, with a modified header.

Header:
```
char[4] - signature "SGVx", where x is F (fullscreen), W (windowed), or P (transparent sprite)
uint16 - X position of image's left edge
uint16 - Y position of image's top edge
uint16 - X position of image's right edge (X ofs + width - 1)
uint16 - Y position of image's bottom edge (Y ofs + height - 1)
uint32 - Start offset of "flag A" section
uint32 - Start offset of "flag B" section
uint32 - Byte size of "flag B" section
uint32 - Start offset of "color index" section
uint32 - Byte size of "color index" section
byte - Palette lookup index, read 16 GRB triplets from pal.dat at [index * 48]
```

The compressed image data follows immediately, which any MAGv2 converter can handle.
If the image is a transparent sprite (SGVP), palette index 2 is probably fully transparent.
