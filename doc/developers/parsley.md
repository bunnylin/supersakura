Software House Parsley
----------------------

Best known for True Love.

They had some connection to DO, ZyX, and Foster? True Love Windows port uses
MRS/VRS graphics, also used by those other developers.

Various resource files are listed below, but none of them contain the game script logic.
I suspect the rather large `tlove.exe` (228kb) probably has hardcoded scripting.
That'll be a challenge to extract...


##### CGA

Graphics files used on Parsley's PC98 games. Words are x86-native little-endian.

.CGA files start with an array of dword offsets. Add 0x200 to each, then they
point to sections starting with the signature FGC. The last dword offset
points to the end of the file.

Each FGC graphic starts with a 16-byte header:
```
char[3] - signature "FGC"
byte - unknown, always 0x0F?
uint16 - pixel width x8
uint16 - pixel height
uint16 - start offset for 1st bitplane data, always 0x0010
uint16 - start for 2nd bitplane
uint16 - start for 3rd bitplane
uint16 - start for 4th bitplane
```

The compressed first bitplane at 0x0010 should immediately follow the header.
Don't know how exactly it is compressed yet...


##### APB

.APB files start with a series of dword offsets. Each points to an array
of 16 byte triplets. Looks like a palette. These aren't directly referenced by
the CGA files, but rather are applied by the game script.


##### FMB

.FMB files start with an array of uint32 offsets, pointing at individual files.
Some files may begin with an FLZ0 signature, indicating LZSS-compression, same as TXB.
These are the FM music of the game.


##### TXB

TXB files are text bundles - each contains a series of compressed string tables.
The file starts with an array of uint32 offsets, strictly ascending. Each points
at the start of a compressed string table chunk. The size of each chunk is the
difference from its uint32 offset to the next. The final uint32 offset points at
the end of the file, indicating no more chunks.

Each compressed string table begins with this header:
```
char[4] - signature "FLZ0"
uint16 - uncompressed size
uint16 - compressed size
```

A standard LZSS-compressed (18-offset) stream follows immediately. To decompress:

```
While uncompressed data not completed:
	flag_byte := Read next byte from input
	For each bit in flag_byte, from 0x01 to 0x80:

		If bit is set:
			Copy one literal byte from input to output

		If bit is not set:
			copy_dist := Read next byte from input
			copy_len := Read next byte from input
			copy_dist := copy_dist + ((copy_len & 0xF0) << 4)
			copy_len := (copy_len & 0x0F) + 3

			copy_dist := ((current_output_offset - 18) & 0xFFF) - copy_dist + 1
			If copy_dist < 0:
				copy_dist := copy_dist + 0x1000

			Output copy_len bytes from copy_dist bytes ago in output
```

The decompressed string table starts with a uint16 string count, then an array of
that many uint16 offsets, strictly ascending. Each points at a single null-terminated
string.


##### APP

.APP files start with dword value 8, followed by an array of offsets.
Add 0x24 to each offset, and the pointed address contains three word values
followed by probably image frames for animation. The first word is always 0x4,
the second often 0xA but sometimes 0x32, and the last could be 0x50 or 0x88...


##### FPD

True Love has a window1 and window2 FPD file. The contents look like raw bitmap data.
