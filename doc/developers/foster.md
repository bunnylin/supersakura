Foster
------

Foster (1994-2003) was part of the same group as Forest and Ucom during the PC98
and early Windows eras. Then they got rolled into the Will conglomerate.
Foster is remembered for Paradise Heights and Time-Stripper Mako-chan, but they
produced quite a few games besides, on average publishing more than 2 per year.
Forest made only a few games under their own name, but that included Ningyou Tsukai,
which was famously localised by Megatech as Metal & Lace. Ucom used a different
engine so their games are not listed here.

Foster's earliest Windows titles (original Hana no Kioku ports) I'm uncertain of,
but after those they mostly used the same engine as DO and ZyX, mysteriously.
Their PC98 graphics format AS2 is also the same Himeya used.

The English localisations of Koko wa Rakuensou and TS Mako use the same resources
and engine as the Japanese versions.

At some point, Foster switched to the widely popular Shiina Rio engine.
Some tools for that:
- https://github.com/MishaIac/Shiina-Rio-TL-Tools
- https://github.com/Inori/FuckGalEngine/tree/master/RioShiina


Archive formats:
- FA1 on all PC98 titles (Hop Step Jump, Kuro no Ken, Marginal Storys, Ningyou Tsukai 2,
  KokoRaku 1 and 2, Rinkan Gakkou, Hana no Kioku 1 and 2, Kousoku Chojin,
  Time-Stripper Mako-chan, Sayonara no Mukougawa)
- Suffixless archives which I'll tentatively call OtakuArc on Windows titles
  (KokoRaku 1 and 2, Rinkan Gakkou, Kousoku Chojin, TSMako, Sayonara no Mukougawa)
- FA2 on Windows titles (Maigo no Kimochi, KokoRaku 3, Mania na Onna 1 and 2,
  Kokoro no Kakera, Akogare)
- WAR RioArc (Hana no Kioku 1+2+3 and 4+5+6 and 7, Calendar Girl, Kanawanu Koi no Monogatari,
  Shikaeshi, Tantei Shounen A, Stage)

Resource formats used on all PC98 titles:
- Graphics:
  * AS2
  * DA1 (Ningyou Tsukai 1 only)
- Scripts: BIN
- Music: PAI

Resource formats used on Windows DO/ZyX/Foster engine titles:
- Graphics:
  * MRS
  * MGF (Mukougawa)
- Scripts:
  * MDT (KokoRaku 1 and 2, Rinkan Gakkou, Kousoku Chojin, Sayonara no Mukougawa)
  * ZSS (TSMako)
- Music:
  * Standard MID
  * MMD (Mukougawa)
- Sound: Standard WAV

Resource formats used on Windows Foster engine titles:
- Graphics:
  * C24 (Maigo no Kimochi, KokoRaku 3, Mania na Onna 1)
  * C25 (Kokoro na Kakera, Mania na Onna 2, Akogare)
- Scripts:
  * BIN
  * SCN (Akogare)
- Music: Standard MID
- Sound:
  * Standard WAV
  * PAD (Akogare)
