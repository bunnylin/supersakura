Micro Cabin
-----------

One of the oldest Japanese game publishers, Micro Cabin and its development arm
Arrow Soft were primarily active from 1982 until the end of the century, although
they moved away from adventure games by the end of the 80's. Their games include
some fine licensed manga adaptations like Maison Ikkoku and Urusei Yatsura, and
they made the Xak action RPG series.


### Kimagure Orange Road

This game comes on two disks, but the second disk is not extractable by normal means.
Even though it has a FAT file system, all files on the disk have been marked as
"deleted", as a form of obfuscation.

What can be read:
- 108 PRS filenames, all of them are graphic files
- The start cluster for each file, not necessarily all correct
- The byte size of each file

What can not be read:
- Each filename's first character, which are replaced with the FAT delete marker 0xE5
- The cluster links in the file allocation table, which have been zeroed out

On this disk, the sector size is 0x400 bytes, and cluster size is 1 sector, the same.
The total 1.2M disk has 0x4D0 clusters, minus the first few sectors. The data section
starts from 0x2C00; that is, cluster 2 is at 0x2C00. (Clusters 0 and 1 are invalid.)

Examining the directory listing of deleted files suggests each file is in a contiguous
array of clusters, so the cluster links aren't strictly needed. However, the highest
cluster in the directory list is 0x29E, a two-cluster file, which means almost half
the disk has data not defined by this list.

Note also that file #57 `?-3R.PRS` looks correct starting from cluster 00B6, but file
#58 `?-1P.PRS` starting from 01F7 does not. #41 from 0129 is fine. #92 from 0177 is
not fine. It seems the start clusters are incorrect for anything above 0x129...
The next file would run from cluster 012F (0x4E000), which does look like the start
of some kind of image data, but the header looks shorter, so maybe an animation
rather than a normal image?

Probing the second disk, there are what look like additional file entries at 0x54E00,
listing some SHT files.

Looking at the first disk, it also has a few deleted files, whose names are probably
`PIYO2.LDY`, `PIYO2.LBC`, `S00001.TBL`, `S00001.TBY`, `PIYO2.ASM`, `MMD.SYS`, `CONFIG.SYS`.
There are also `S00000` table and tabby files, The tabby file looks like the game script.
The table file has some Shift-JIS (with reversed bytes), but also series of increasing
word values that could be disk offsets.

The deleted `S00001` files don't have anything that looks like disk offsets, just more
Shift-JIS. Same for `PIYO2.LBC` and `PIYO2.LDY`.

The first disk's highest-cluster file in the directory list is deleted `PIYO2.ASM`, which
runs from cluster 0198 to 01D7 inclusive. This again leaves the second half of the disk
undefined.

There's a `PAT.COM` on the first disk, which on inspection is not an executable file
at all, but rather a list of probable file offsets and sizes. Each is 3 bytes:
```
uint16 - start cluster?
byte - data length in clusters
```

But since these run from cluster 01A4 upward, these don't obviously indicate files on
the second disk. They could point at the first disk, if the large deleted `PIYO2.ASM`
is indeed invalid and parts of its data are overwritten with other game resources.
The big ASM file has some interesting strings in it, including that the game engine
may be called `LADY`, and an `OPTIMIZER` exists for it. However, most of the file
seems to be a ton of `DW` statements, and `DB` ones at the end.

Conclusion: I don't know what the second half of the second disk contains, and not too
sure about the second half of the first disk either.

However, firing up a debugger, I see the first image of the game is probably at 0x6A400
on the second disk. Cluster 01A0. Runs up to 0x6C400, 8 clusters. It's loaded into memory
at 2a0f:0000.
