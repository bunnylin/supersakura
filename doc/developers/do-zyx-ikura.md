Ikura GDL engine
----------------

A very popular engine used by multiple companies, most notably DO and ZyX.
This started out as Digital Romance System with DRS script files, then evolved
to ISF script files.

Since this is a widely used engine, plenty of documentation already exists:
- https://github.com/Redfoxymoon/ikura_tools
- https://github.com/mnakamura1337/engine_ikura
- http://vilevn.sourceforge.net/doxygen/annotated.html


### SM2MPX10 archive

Suffixless files, but with a recognisable signature. Typical file names are
GGD and ISF, which contain a bunch of graphic files and script files respectively.

Header (little-endian):
- 8 bytes: signature "SM2MPX10".
- dword: number of files in archive
- dword: end offset of file list
- 12 bytes: containing directory name for all subsequent files?
- dword: start offset of file list (always 0x00000020?)

File list:
- 12 bytes: filename including dot-suffix, padded with zeroes
- dword: start offset
- dword: file length

File data has some byte alignment, so there's a bit of empty space between files.
