Leaf
----

This developer only made a few PC98 games, but made many more for Windows.
Kinda famous for popularising the "sound novel" game format and calling
them "visual novels". Besides those, Leaf also made the RPGs Filsnown and
Legam (published by their parent U-Office).

There is a free software engine xlvns that supports some of their games:
https://github.com/catmirrors/xlvns


### Shizuku

The mus subdirectory has `fm` and `md` files for the FM and midi music respectively.
There is a `pmd.com` and `mmdr.com`, implying the classic PMD format is used.

`LFG` files take up most of the space, found under `bck` (backgrounds?), `chr`
(character sprites?), `hvs` ("special scenes"?), and `vis` (different events?..).
That leaves the `sce` subdirectory with `DAT` files. I would presume these contain
the scene scripts, which may or may not include all game text. However, the `DAT`
files don't have recognisable Shift-JIS.

There are also `VD` files in the game root, whose function is unclear. Any kind of
large-scale "Video" animation is out of the question, considering the small file sizes.
Perhaps these are the brief, simple animations during the intro sequence.

There's a `knj_all.kcd` file in the game root that's quite large. A bitmap kanji font?
This doesn't necessarily have to follow Shift-JIS encoding, so perhaps the game
scripts use a custom encoding (or a non-shiftjis encoding).

Shizuku was published at start of 1996, PC98 first, then Windows version half a year
later (per VNDB). It's not impossible that the developers might have switched to
Windows 95 quite early and used its native codepage 932 or something for all text...
but I've run scn002.dat through a few different encoding possibilities and got
nothing but mojibake. (on Linux, iconv -f <encoding> -t UTF8 <file> -c)

The Shizuku music files can be found in the Hoot archive. Comparing those with the
files from the game reveals that they are encrypted with a basic XOR 0xFF on every byte,
and it looks like LZSS compression. The music files particularly start with a uint32
that is not XORred, which has the uncompressed file size. Then the XORred, compressed
stream follows.

The scene files don't start with the uncompressed file size indicator; they have some
other header, 20 bytes long. But after that, it looks like the same xor FF and LZSS for
these too.

The compression used is standard LZSS, except flag bits are read from 0x80 to 0x01
(top-down) rather than from 0x01 upward. Also, for repeat commands, normally the length
byte is first, but here the distance byte is first.


##### Scene DAT files

These are split in two blocks, compressed separately. Layout:
```
uint16 - first data block starts at this offset * 16, always 0001
uint16 - second data block starts at this offset * 16
byte[12] - unused, padding to 16-byte boundary

first block:
uint32 - unpacked byte size
byte[] - data, encrypted and compressed; runs until start of second block

second block:
uint32 - unpacked byte size
byte[] - data, encrypted and compressed; runs to end of file
```
