Great
-----

##### Foreigner

- PAC images, documented separately.
- PAR files? mostly identical.
- SD scripts
- SW files? 128 bytes, all 0's. Related to SD scripts...
- O music, documented separately.
- TMAP.DAT?
- MAP.DAT? 735 byte files, all identical?..
- MONSTER.DAT: Monster stats
- ARM.DAT: Equipment stats
- FRMAIN.EXE: the game executable, has many hardcoded strings from 0x19B16 onward.

###### Arm.dat

Starts with a 17-byte "F-1 Arm Data V1.0", then 4 bytes each - 76 entries in total.

```
uint8 - index number
uint8 - 00
uint8 - value 1? to-hit?
uint8 - value 2? damage?
```

Weapon names are in FRMAIN.EXE from 0x19C83 onward, but may be tricky to extract,
since the strings aren't strictly null-terminated and there's other data interspersed.
Try reading valid Shift-JIS byte pairs, and when invalid byte encountered, end string.
When next valid byte pair appears, begin new string.

A few weapon names:
- 0 素手 Bare hands
- 1 ショートソード Short Sword
- 2 カトラス Cutlass
- 3 フォールション Falchion
- 4 ブロードソード Broad Sword
- 5 バイキングソード Viking Sword
- 6 ロングソード Long Sword
- 7 バスタードソード Bastard Sword

Some example data blocks:
```
01 00 0A 0A
02 00 0C 0D
03 00 0E 10
04 00 10 13
05 00 12 16
06 00 14 19
0B 00 05 0F
0C 00 07 12
```

###### Monster.dat

Starts with 24-byte "F-1 Monster Data V1.03" 00 00, then 0x9D bytes each...

```
0: char[32] - monster name in Shift-JIS, padded with zeroes
36,38: uint16 - HP and Max HP?
69: uint8 - monster index number, 1-based
```

Probably describes not only stats, but also what actions it can take.

Some example data blocks:
```
83 6F 83 43 83 70 81 5B   00 00 00 00 00 00 00 00  - 0
00 00 00 00 00 00 00 00   00 00 00 00 00 00 00 00  - 16
00 00 00 00 28 00 28 00   00 00 00 00 00 00 00 00  - 32
00 00 00 00 00 00 00 00   06 02 05 00 03 03 0A 65  - 48
00 00 00 00 00 01 00 00   00 00 00 00 00 00 00 00  - 64
00 00 00 00 00 00 00 00   00 00 0D 08 0D 06 08 0B  - 80
00 64 00 00 00 00 00 00   00 00 00 00 00 00 00 00  - 96
00 00 00 00 00 00 00 00   00 00 00 00 00 00 00 00  - 112
00 00 00 00 64 00 00 00   00 00 00 0A 14 28 00 1E  - 128
00 00 00 00 00 00 00 00   00 00 00 00 00           - 144

83 41 83 8B 83 5A 83 43   83 66 83 58 00 00 00 00  - 0
00 00 00 00 00 00 00 00   00 00 00 00 00 00 00 00  - 16
00 00 00 00 32 00 32 00   00 00 00 00 02 00 00 00  - 32
03 00 00 00 00 00 00 00   05 08 03 09 07 07 0A 1F  - 48
00 00 00 00 00 05 67 08   00 00 00 00 00 00 00 00  - 64
00 00 00 00 00 00 00 00   00 00 13 0C 09 06 07 0D  - 80
00 64 00 00 00 00 00 00   00 00 00 00 00 00 00 00  - 96
00 00 00 00 00 00 00 00   00 00 00 00 00 00 00 00  - 112
00 00 00 00 3C 1E 09 00   00 00 01 1E 28 1E 00 00  - 128
00 00 00 28 00 3C 00 64   00 00 00 00 00           - 144

Bandit: (2x2 yields 128 exp)
93 90 91 AF 00 00 00 00   00 00 00 00 00 00 00 00  - 0
00 00 00 00 00 00 00 00   00 00 00 00 00 00 00 00  - 16
00 00 00 00 1E 00 1E 00   00 00 00 00 00 00 00 00  - 32
00 00 00 00 00 00 00 00   05 01 05 01 05 06 05 01  - 48
00 00 00 00 00 0E 00 00   00 00 00 00 00 00 00 00  - 64
00 00 00 00 00 00 00 00   00 00 0C 07 0B 07 05 08  - 80
00 64 00 00 00 00 00 00   00 00 00 00 00 00 00 00  - 96
00 00 00 00 00 00 00 00   00 00 00 00 00 00 00 00  - 112
00 00 00 00 64 00 00 00   00 00 00 32 28 0A 00 00  - 128
00 00 00 00 00 00 00 00   00 00 00 00 00           - 144
```

Bandit fight data:
(For best results, should hack a game script to run an "arena" where
you can fight monsters at will easily. Record dozens of fights in
a spreadsheet and see how changing PC stats affect outcomes.)

- Klaus stabs bandit 1 with short sword: 18 damage (18 dmg)
- Bayse stabs bandit 1 with bastard sword: 24 damage, defeated (42 dmg)
- Klaus punches bandit 3: 17 damage (17 dmg)
- Bandit stabs Klaus: 4 damage
- Bandit stabs Bayse: 3 damage
- Miria casts Fireball 1 on bandit 2: 2 damage (2 dmg)
- Bayse misses bandit 2 with a scary hit
- Miria throws dagger at bandit 2: 13 damage, knocked out (only 15 dmg, instakill?)
- Klaus blocks bandit 3 with shield
- Klaus evades bandit 4
- Miria punches bandit 3: 14 damage, defeated (31 dmg)
- Klaus stabs bandit 4 with short sword: 15 damage (15 dmg)
- Bayse stabs bandit 4 with bastard sword: 26 damage, defeated (41 dmg)

Round 2! Stabs only
- Klaus stabs bandit 1 with short sword: 20 damage (20 dmg)
- Miria throws a dagger at bandit 1: 14 damage, defeated (34 dmg)
- Bayse stabs bandit 2 with bastard sword: 27 damage (27 dmg)
- Bandit stabs Klaus with short sword: 10 damage
- Bandit stabs Bayse with short sword: 5 damage
- Miria punches bandit 2, bandit evades
- Klaus stabs bandit 2 with short sword: 18 damage, defeated (45 dmg)
- Miria punches bandit 3: 5 damage (5 dmg)
- Klaus stabs bandit 3 with short sword: 22 damage (27 dmg)
- Bandit stabs Klaus with short sword, but Klaus evades
- Bayse stabs bandit 3 with bastard sword: 28 damage, defeated (55 dmg)
- Miria punches bandit 4: 5 damage (5 dmg)
- Klaus stabs bandit 4 with short sword: 22 damage (27 dmg)
- Miria punches bandit 4: 6 damage, defeated (33 dmg)

Round 3! Slashes only
- Bandit stabs Klaus with short sword: dodge
- Bandit stabs Klaus with short sword: block
- Bandit stabs Miria with short sword: 6 damage
- Klaus slashes bandit 1 with short sword: 29 damage
- Bayse slashes bandit 1 with bastard sword: 40 damage, defeated
- Miria slashes bandit 2 with dagger: 21 damage
- Klaus slashes bandit 2 with short sword: 26 damage, defeated
- Bandit slashes Klaus with short sword: 8 damage, Klaus faints!
- Bandit slashes Bayse with short sword: 5 damage
- Bandit stabs Bayse with short sword: no damage
- Bandit stabs Bayse with short sword: 6 damage
- Bayse slashes bandit 3 with bastard sword: dodge
- Miria slashes bandit 3 with dagger: 17 damage
- Bandit stabs Miria with short sword: 5 damage
- Bandit stabs Miria with short sword: 3 damage
- Bayse slashes bandit 3 with bastard sword: 35 damage, defeated
- Miria slashes bandit 4 with dagger: 17 damage
- Bandit stabs Miria with short sword: 7 damage
- Bandit stabs Miria with short sword: 5 damage
- Bayse slashes bandit 4 with bastard sword: 38 damage, defeated

Conclusions:
- Klaus stabs bandit with short sword: 18, 15, 20, 18, 22, 22 damage
- Bayse stabs bandit with bastard sword: 24, 26, 27, 28 damage
- Klaus punches bandit: 17 damage
- Miria throw dagger at bandit: 13, 14 damage
- Miria punches bandit: 14, 5, 5, 6
- Miria uses fireball 1: 2 damage
- Bandit stabs Klaus with short sword: 4, block, dodge, 10, dodge, dodge, block
- Bandit stabs Bayse with short sword: 3, 5 damage
- Bandit stabs Miria with short sword: 6
- Bandit HP: 29-31

###### Foreigner stats and combat

The characters start at level 1, 0 EXP, with 600 required for next level.

Character stats screen:
```
                                      GOLD     0
<name>           武装

LEVEL        1   武器  ショートソード    攻撃力    18
EXP          0                           打撃力    17
HP/MAX   35/35         素手              攻撃力    14
Vitality     8                           打撃力    12
Intellect    7   鎧    レザーメイル
Might        7   兜    レザーヘルム
Magic        8   靴    グラスブーツ      防御力    13
Mental       7   楯    バックラー        楯回避    16
Dexterity   10                           通常回避  13
Agility      8   指輪  なし
```
```
                                      GOLD     0
<player>         Equipment

LEVEL        1   Weapon Short Sword      Attack power   18 (to-hit?)
EXP          0                           Impact power   17 (damage?)
HP/MAX   35/35          Bare hands       Attack power   14
Vitality     8                           Impact power   12
Intellect    7   Armor  Leather Mail
Might        7   Helmet Leather Helm
Magic        8   Boots  Grass Boots      Defense power  13
Mental       7   Shield Buckler          Shield evasion 16
Dexterity   10                           Normal evasion 13
Agility      8   Ring   None

                                      GOLD     0
Bayse            Equipment

LEVEL        1   Weapon Bastard Sword    Attack power   12
EXP          0                           Impact power   24
HP/MAX   40/40          Bare hands       Attack power   12
Vitality    10                           Impact power   12
Intellect    5   Armor  Leather Mail
Might        9   Helmet Leather Helm
Magic        0   Boots  Grass Boots      Defense power  15
Mental       8   Shield Buckler          Shield evasion 13
Dexterity    7                           Normal evasion 13
Agility     10   Ring   None

                                      GOLD     0
Miria            Equipment

LEVEL        1   Weapon Dagger           Attack power   20
EXP          0                           Impact power   10
HP/MAX   30/30          Bare hands       Attack power   9
Vitality     6                           Impact power   8
Intellect   10   Armor  Leather Mail
Might        5   Helmet Leather Helm
Magic        8   Boots  Grass Boots      Defense power  11
Mental       8   Shield Buckler          Shield evasion 12
Dexterity    6                           Normal evasion  9
Agility      6   Ring   None
```

On the second page of stats, there's also アイテム名 Item name, and
数 number. This should show what items the character has.

Third page:
```
<name>       魔法

火術系魔法         Lv    雷術系魔法      Lv
F・ボール           0    R・アロー        0
F・エンチャント     0    R・ボルト        0
ガード・オブ・F     0    R・ブレード      0
F・ストーム         0    パラライズ       0

水術系魔法               幻術系魔法
ヒール・W           1    ブラインド       1
W・ウォール         0    フォグ           0
W・アーマー         1    バーサーク       0
キュア・W           0    イリュージョン   0
```
```
<player>     Magic

Fire magic         Lv    Lightning magic Lv
F. Ball             0    L. Arrow         0
F. Enchanment       0    L. Bolt          0
Guard of F.         0    L. Blade         0
F. Storm            0    Paralyze         0

Water magic              Illusion magic
Heal W.             1    Blind            1
W. Wall             0    Fog              0
W. Armor            1    Berserk          0
Cure W.             0    Illusion         0

Miria        Magic

Fire magic         Lv    Lightning magic Lv
F. Ball             1    L. Arrow         0
F. Enchanment       1    L. Bolt          0
Guard of F.         1    L. Blade         0
F. Storm            0    Paralyze         0

Water magic              Illusion magic
Heal W.             0    Blind            0
W. Wall             0    Fog              0
W. Armor            0    Berserk          0
Cure W.             0    Illusion         0
```

Party stats screen:
```
<player>            Bayse               Miria
LEVEL         1     LEVEL         1     LEVEL         1
EXP           0     EXP           0     EXP           0
HP/MAX    35/35     HP/MAX    40/40     HP/MAX    30/30
Short Sword         Bastard Sword       Dagger
Attack Power 18     Attack Power 12     Attack Power 20
Impact Power 17     Impact Power 24     Impact Power 10
Bare hands          Bare hands          Bare hands
Attack Power 14     Attack Power 12     Attack Power  9
Impact Power 12     Impact Power 12     Impact Power  8
Defense Powr 13     Defense Powr 15     Defense Powr 11
Shield Evasn 16     Shield Evasn 13     Shield Evasn 12
Normal Evasn 13     Normal Evasn 13     Normal Evasn  9
```

Combat starts with a welcome message for each monster stack:
"<monster>が現れた。" or "<monster> has appeared."

You select an action for each character.
- 攻撃 Attack
- 魔法 Magic
- アイテム Item
- 隙をつく Counter (Take advantage of an opening)
- かばう Cover
- 防御 Defend
- 逃げる Run

Attack lets you choose a wielded weapon, attack style, and target.
Magic lets you choose an element, specific spell, and spell power?..
Bigger power takes longer to cast, probably.
Counter lets you choose a wielded weapon, powerfully parries and counters the next enemy attack.
Cover and Defend make the character hard to damage. Unsure what the difference is.

The enemy sprite flashes white when attacked physically. If attacked with magic,
flashes with the appropriate color.
The battle screen flashes all white when a player character gets attacked.
After being hit, a character may faint. If an enemy faints, it's out of the battle.
If a player character faints, they wake after a while.
If a player character reaches 0 hit points, they are out for the rest of the fight,
but come back to 1 HP on fight end. Won't gain EXP for the fight.

After acting, the attacking character must select a new action immediately.
Characters act according to their speed statistic, and may act more than once
before a slow character gets to move. If a queued action was targeting an enemy
that is defeated, the queued action is cancelled and you must select a new action
for the character immediately.

Every enemy acts separately. With 2x2 opponents, there are 4 of them attacking you.

Characters can equip one item per hand? If you "Throw" a dagger, it is gone.
On victory, a brief fanfare plays, then a longer victory music, much like FF.
Everyone who's not knocked out gets an equal amount of EXP.

Attack:
- Short sword
- Bastard sword
- ダガー Dagger
- 素手 Bare hands

Short sword:
- 突く Stab, fast weak poke
- 斬る Slash, slower stronger strike
- 急所を狙う Target vital point

Bastard sword:
- Stab, slash
- 叩きつける Slam

Dagger:
- 投げる Throw (you'll lose the dagger)
- 斬る Slash
- 急所を狙う Target vital point

Bare hands:
- 殴る Hit
- 怖当たり Scary hit
- 急所突き Jab vital point

Magic:
- 火術系魔法 Fire magic
- 水術系魔法 Water magic
- 雷術系魔法 Lightning magic
- 幻術系魔法 Illusion magic

Magic > no levels in element's spells: その種類の魔法は、使えません You don't know such magic
Counter > too small weapon: ダガーでは、隙をつけません! Not a chance with a dagger!
Item > no items available: 使用で章るアイテムは有りません! ! No items to use!
Run > forbidden: う逃げられない! Can't run away!

- 盗賊は、ショートソードをかまえた。 The bandit holds a short sword. (or, brandishes?)
- クラウスは、盗賊に、殴りかかった!! Klaus hits the bandit!!
- クラウスは、盗賊をショ-トソ-ドで突いた!! Klaus stabs the bandit with a short sword!!
- べイスは、盗賊をパスタ-ドソ-ドで突いた!! Bayse stabs the bandit with a bastard sword!!
- ミリアは、盗賊に、 ダガーを投げつけた!! Miria throws a dagger at the bandit!!
- ベイスは、盗賊に対して体当たり!! Bayse slams the bandit!!
- 盗賊は、ベイスのこうげきをひらりとかわした。 The bandit evaded Bayse's attack.
- 盗賊に、18のダメ-ジを与えた!! The bandit takes 18 damage!!
- 盗賊は、クラウスをショートソードで突いた!! The bandit stabbed Klaus with a short sword!!
- 盗賊は、クラウスに、ショートソードで斬りかかった!! The bandit slashed Klaus with a short sword!!
- 盗賊は、クラウスの急所にショートソードで、 斬りつけた! The bandit slashed Klaus in the vitals
  with a short sword!
- クラウスは、4のダメージを受けた!! Klaus took 4 damage!!
- ベイスは、ダメージを受けなかった! Bayse did not take damage!
- クラウスは、盗賊のこうげきを楯で受けた。 Klaus blocked the bandit's attack with a shield.
- クラウスは、盗賊のこうげきをひらりとかわした。 Klaus evaded the bandit's attack.
- クラウスは、盗賊の隙をついてショートソードで反撃した! Klaus took advantage of the bandit's
  opening and countered with his short sword!
- ファイヤー・ボールだ!! Fire Ball!!
- ブラインドだ!! 盗賊は、幻覚により周囲が見ずらくなった。 Blind! The bandit can't see the
  surroundings well because of illusions.
- ガード・オブ・ファイヤーだ!! クラウスの周りに火の壁ができた。 Guard of Fire! A fire wall was
  created around Klaus.
- クラウスには、 ガード・オブ・ファイヤーがかかっていた! 炎が盗賊に襲いかかる!! The Guard of
  Fire was on Klaus! Flames attack the bandit!! (only procs if hit wasn't evaded or blocked?)
- 盗賊を倒した。 Defeated the bandit.
- 盗賊は、気絶してしまった。 The bandit has fainted.
- ミリアは、 気がついた。 Milia recovered.
- ミリアは倒された。 Milia was defeated.
- クラウス達は、戦いに勝利した! Klaus and friends have won the battle!
- Klaus gains 128 XP.

Levelling up:
- クラウスは､レベル2になった。 Klaus is now level 2.
- ヒットポイントが11上がった。(46) Hit points up by 11. (46)
- 体力が、1上がった。(9) Vitality up by 1. (9)
- 知力が、2上がった。(9) Intelligence up by 2. (9)
- 腕力が、1上がった。(8) Might up by 1. (8)
- 魔力が、1上がった。(9) Magic up by 1. (9)
- 精神力が、2上がった。(9) Mental up by 2. (9)
- 器用さが、3上がった。(13) Dexterity up by 3. (13)
- 素早さが、2上がった。(10) Agility up by 2. (10)
- ヒール・Wのレベルが2になった。 Heal W. is now level 2.
- W・アーマーのレベルが2になった。 W. Armor is now level 2.
- ブラインドのレベルが2になった。 Blind is now level 2.


A re-implemented combat system might look like this:
- Enemies are in the top half of the screen, with name and count underneath.
  Perhaps a centered area, leave some empty space on the sides.
- Flash effects are non-blocking.
- The player party's head icons and HP/MP are along the bottom.
   * Grab head icons from FOR01_3, maybe FOR07_3 for Miria.
- Action menu could be in the bottom left, occupying a similar rectangle as characters.
- Description text can be between the enemy and player characters.
- Reduce description verbosity.
- Select target by using sideways keys, available along choice in menu.
- All spells in a single table, no element selection

A re-implemented main game:
- Since the graphics are rather low-res, maybe a permanent viewframe is fine,
but bigger than the original, and center the graphic. Textbox below the graphic.
Dialogue titles could still go in their own box, but on the left side of the
main box. The choice menu should be a separate single-column thing, appearing
on the left side probably over the viewframe and graphic.
- The metamenu should be co-opted to bring up a DQ-style menu with the various
stats, inventory, equipment options, along with an extra System option to bring
up the standard metamenu. While the metamenu is up, show a summary of the party's
stats in boxes along the top, again like DQ.

###### Foreigner intro

Run the START script. This is the game's main menu, which ought to be reimplemented.

It prints "Foreigner" in double-quotes in the main view, and populates the
multiple choice section with "Start from the beginning" and "Start from the middle".
Select the first: fades to black, ends the script. Player name entry screen fades in.

The default player name is クラウス, Klaus. The other two are ベイス, Bayse, and
ミリア, Miria.

The title screen appears: all black, then a red horizontal line grows from the middle
to both edges over about 3 seconds. The line expands vertically into a red band across
the middle of the screen. The stylised FOREIGNER logo fades in from the center outward.
(This is the TITLE graphic.) The Fuorinaa katakana text fades in. Music begins playing.
The supertitle across the top appears from left to right. The Plum logo appears
instantly at the bottom. Finally, the text "CLICK THE MOUSE TO START" begins blinking
under the "ORE" below the red band.

On click, fade to black over 1 second, stop the music. Execute script OPEN.

After the first battle, the menu's items are:
- Look/Examine
- Talk/Ask
- Take/Touch
- Think
- Data: Personal data, List, Change equipment, Reorder Party
   * Personal data (individual statistics): Klaus, Bayse, Miria
   * List: Party stats, Equipment, Items
- System:
   * Message (msg speed? 1-5)
