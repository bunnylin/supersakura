ScooP
-----

A latecomer to the PC98, ScooP operated from about 1995 to 2003. In the west
they're best known for the Immoral Study series of short animated VNs.

File types used by ScooP games:
- PC98
  * BIN: game scripting, usually embeds game text
  * TLK: game text separate from game logic, Shift-JIS string table
  * APX: image
  * APG: maybe just a single image, maybe a scoopx archive of images
  * APH, API: image
  * AGX: scoopx archive of APX image files
  * MCG: scoopx archive of music files, similar but not same as PMD
  * BIX: scoopx archive of game scripts
- Windows
  * same BIN, AGX, APG
  * WAV: single RIFF wave audio file
  * WAX, SE: scoopx archive of RIFF wave audio files
  * SIX, BGM: scoopx archive of RIFF midi files
  * BMX: scoopx archive of BMP images
  * AVI: standard video clip
  * FX: parrot archive of BIN scripts
  * GX: parrot archive of BMP images
  * MX: parrot archive of MDS midi files
  * VX: parrot archive of WAV audio files

Parrot is used in Delicious Lunch Pack Neo, Success, Chaos Baby Yoshimi 1 and 2,
Yuuwaku 1 and 2, Chaos Queen Ryouko 4.
