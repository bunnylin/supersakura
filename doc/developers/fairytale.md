Fairytale
---------

As one of the old-timers of the industry, Fairytale published a lot of games,
and their engine was used in even more by the various companies under the
ownership of Kirara/IDES, which eventually became F&C in the Windows era.

Associated labels and brand names (mostly per VNDB):
- Fairytale Hardcover
- Fairytale X-Shitei
- Fairytale Red-Zone
- Cocktail Soft
- Elf
- Game Technopolis
- FMC
- Santa Fe
- Cat's Pro (only the games Cat's part 1 and Nova on PC98)
- Dez Climax (they must have been a Cocktail Soft spinoff)

Fairytale's most famous works use the ADV98V engine.

Graphics formats on PC98:
- ADA/MDA
- PR6
- PRS
- GPC/GPA
- WSB

Graphics formats used on Windows:
- IPF
- Maybe others buried in their custom archive formats

MLD can view most of those graphics.
For more details, see individual file format documents under doc/gfx.

Music formats used:
- Elf-style M
- MCO
- USO

Archive formats used:
- MRG
- PAK
- EVE PCK

### Dead of the Brain 1

SlowBeef describes some of the scripting here:
https://github.com/slowbeef/dotb-romhack

And a MES converter:
https://github.com/drasticactions/MesExtractAndInject

- TCM files are probably multiframe images or something
- SE files must be FM sound effects
- USO files contain the music, some kind of basic MML as usual. Numbers below 0x80 are notes.
- MES files contain game text
- MEC files are probably compressed game text?
- GPC and GPA files are graphics; see gfx/gpc-gpa.md
- CLM and CMD would have to be the game scripts
