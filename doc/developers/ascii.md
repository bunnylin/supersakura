ASCII Soft/Corporation
----------------------

An old-timer of the industry, ASCII were a notable publisher who ported some
western games for the Japanese market, most notably the Wizardry series, and
subsequently went on to create the RPG Maker tool. They published a variety
of other stuff besides, from sports to arcade action, even a few adventure games.


### Giten Megami Tensei - Tokyo Mokushiroku

A rare PC title developed by Atlus. There's a high-quality English patch by Sneikkimies.

All game data files use the `.bin` suffix, but a one- or two-letter prefix identifies
the file type.

- `a000?.bin`: unknown, highly patterned...
- `ca*.bin`: unknown
- `et*.bin`: item and enemy lists, simple XOR encryption, all loaded on startup
- `fc*.bin`: graphics
- `id*.bin`: unknown, but the first word points to a series of 00 and 02 bytes...
- `m0*.bin`: map data, XOR encryption
- `ms*.bin`: event scripts, XOR encryption
- `p*.bin`: enemy data, XOR encryption
- `sb*.bin`: FM music files, starts with 6 word offsets, one for each track
- `se000.bin`: unknown, various repeating patterns in file, starts with a list of offsets
- `sm*.bin`: standard midi files

XOR encryption used by the scenario files: Just XOR each byte with the previous byte.


##### FC files

These are divided into chunks, where each chunk contains a complete multi-frame graphic.
Each graphic is split into a single tilesheet, and a tilemap for each graphic frame that
constructs the frame from the defined tiles.

Source code for reading these is in [/inc/gfx/atlusfc.pas](../../inc/gfx/atlusfc.pas).

Each chunk begins with this:
```
uint32 - chunk byte size, not incl this dword
uint32 - pointer to tile sheet, incl this dword, add 2
uint16 - frame count
uint16[] - frame start offsets, decrement by 2; relative to frame count field
```

Each frame contains this:
```
sint16 - X offset in 8x8 px blocks
sint16 - Y offset in 8x8 px blocks
uint16 - frame width in 8x8 px blocks
uint16 - frame height in 8x8 px blocks
uint16[] - tile map data...
```

The tile map is an uncompressed array of width * height uint16's, each referencing a tile
index. The tiles are specified in columns, left to right. So, the first tile is drawn at
the top left corner, the next tile below that.

The top bits of the tile reference uint16 have a special meaning:
- 0x1000 = ?
- 0x2000 = ?
- 0x4000 = flip tile vertically
- 0x8000 = flip tile horizontally

Examining the graphic FC3003, it has 0x273 tiles, and the tilemap has indeed references to
tile indexes 0 to 0x272. But it also has 0x1000..0x1086, 0x2000..0x2013, and 0x3000..0x3001.
For the 0x1000+ range, it's not just a modification of the low byte tile; those tiles
typically look completely wrong for the context. Rather, I suspect the 0x1000+ range is
fetching tiles from a different tile sheet, in some different file, but which?..

The tile sheet contains one or more palettes, presumably for easy monster palette swapping.
The sheet itself is a series of compressed tiles, split into 4 bitplanes. I'll call the
planes 1, 2, 4, and 8; they appear in the compressed data in that order for each tile.
To turn each tile into a bitmap, combine 1 bit at a time from each bitplane to output
a 4-bit pixel.

Tile sheet header:
```
uint16 - size of header after this uint16 to end of palette
uint16 - number of palette copies, could be one for every frame
uint16 - bitmask for which palette entries to apply; 0x8000 = color 0, 0x0001 = color 15
uint16[][16] - for each palette copy, 32 bytes; byte order RB 0G
uint32 - byte size of tile sheet, not incl. this dword
uint16 - unknown, usually 0000, can be 0001
uint16 - tile count; tile 0 always fully transparent, not in list
byte[15] - unknown, usually all 00
byte[] - tile data...
```

The tile data is compressed using special byte commands. The first byte's high nibble
specifies which bitplanes have explicit data:
- 8x: bitplane 1 has something
- 4x: bitplane 2 has something
- 2x: bitplane 4 has something
- 1x: bitplane 8 has something

The first byte's low nibble is the compression mode for this tile. The low bit 0x01 always
has the same meaning:
- 0: Fill every bitplane that doesn't have something with 00
- 1: Fill every bitplane that doesn't have something with FF

The modes are:
- x0, x1: full bitplanes without bitmasks
- x2, x5: bitplanes with bitmasks, zero out masked, no cascading
- x3, x4: bitplanes with bitmasks, all FF on masked, no cascading
- x6, x7: import bitplane contents, plus overrides without bitmasks
- x8, xB: import, plus overrides with bitmasks, zero out masked
- x9, xA: import, plus overrides with bitmasks, all FF on masked
- xC, xD: bitplanes with bitmasks, cascade masked

If importing bitplane contents (modes 6 to B), an extra byte follows the mode byte, which
specifies which bitplanes use it. There are 2 bits for each bitplane, allowing each to
import from the first 3 bitplanes, or to skip importing.

The import byte, bitwise:
- 11xxxxxx: do not import bitplane 1
- xx00xxxx: import bitplane 2 from 1
- xx01xxxx: import bitplane 2 from 2 (not useful)
- xx10xxxx: import bitplane 2 from 4 (not useful)
- xx11xxxx: do not import bitplane 2
- xxxx00xx: import bitplane 4 from 1
- xxxx01xx: import bitplane 4 from 2
- xxxx10xx: import bitplane 4 from 4 (not useful)
- xxxx11xx: do not import bitplane 4
- xxxxxx00: import bitplane 8 from 1
- xxxxxx01: import bitplane 8 from 2
- xxxxxx10: import bitplane 8 from 4
- xxxxxx11: do not import bitplane 8

More detail on how each mode works:
- Full bitplanes without bitmasks
  * For every plane that "has something", read and output 8 bytes directly
- Bitplanes with bitmasks
  * For every plane that "has something", read a mask byte, then for each bit in that
    from 0x80 to 0x01: if the bit is set, read a byte and output it directly;
    if the bit is not set, output 00 (if mode 2/5), or output FF (if mode 3/4)
- Bitplanes with bitmasks and cascading
  * For every plane that "has something", handle a mask byte like above, but when the
    mask bit is not set, repeat the previous value; if the first byte has mask bit not set,
    output 00 if mode's low bit is 0, or output FF if mode's low bit is 1
- Importing with or without bitmasks
  * Same as above, except if a mask bit is not set, and the current bitplane is importing
    from an earlier bitplane, copy the byte at the same X,Y position from the import
    source bitplane; if the current bitplane is not importing anything, then output 00
    or FF just like above

Implementation notes:
- In bitmasks, 0x80 is the leftmost pixel on a row, 0x01 the rightmost
- If bitplane is empty but also imports from another plane, the import overwrites the fill
- If bitplane imports from another plane, that's applied to each masked row
- If tile uses importing but not for this particular bitplane, masked rows are 00 or FF
- Import can only be applied after source plane is complete
- Import from a bitplane happens before cascade applied on that bitplane!
- Rows to be cascaded are imported as if they use the initial 00 or FF

After all 4 bitplanes are somehow complete, a further 8 bytes follow. This is an alpha mask,
and every tile has a full uncompressed one. A set bit means opaque, a clear bit transparent.

Sounds confusing, I know! Here's an example tile:

```
E8 F5
60 28 28
FE 7C D6 D6 FE C6 EE 7C 00
0C FE FE
7C FE FE FE FE FE 7C 00
```

Here's how it breaks down. `E8` means bitplanes 1, 2, and 4 are explicitly specified, and
we're using mode 8, importing stuff. The `F5` means bitplanes 1 and 2 don't import
anything, 4 and 8 are importing whatever's in bitplane 2.

So we start with bitplane 1. `60` means there will be data for the 2nd and 3rd row, while
all other rows are set to a base fill of 00, since the mode's low bit is 0.

The next two bytes are `28`, so the 3rd and 5th bit on the row are set. That's all for
bitplane 1. Here's how it looks:

	0 0 0 0 0 0 0 0
	0 0 1 0 1 0 0 0
	0 0 1 0 1 0 0 0
	0 0 0 0 0 0 0 0
	0 0 0 0 0 0 0 0
	0 0 0 0 0 0 0 0
	0 0 0 0 0 0 0 0
	0 0 0 0 0 0 0 0

For bitplane 2, the `FE` byte means we have a literal for every row but the last.
This is how it ends up looking:

	0 1 1 1 1 1 0 0
	1 1 0 1 0 1 1 0
	1 1 0 1 0 1 1 0
	1 1 1 1 1 1 1 0
	1 1 0 0 0 1 1 0
	1 1 1 0 1 1 1 0
	0 1 1 1 1 1 0 0
	0 0 0 0 0 0 0 0

For bitplane 4, we start by copying the previous bitplane. The first byte `0C` indicates
only rows 5 and 6 need overriding. The literal bytes for both are `FE`. This is the result:

	0 1 1 1 1 1 0 0
	1 1 0 1 0 1 1 0
	1 1 0 1 0 1 1 0
	1 1 1 1 1 1 1 0
	1 1 1 1 1 1 1 0
	1 1 1 1 1 1 1 0
	0 1 1 1 1 1 0 0
	0 0 0 0 0 0 0 0

Bitplane 8 only imports from bitplane 2, so there's no mask byte or anything. All that's
left is the 8-byte alpha mask. After applying that, the combined bitmap is this:

	. E E E E E . .
	E E 1 E 1 E E .
	E E 1 E 1 E E .
	E E E E E E E .
	E E 4 4 4 E E .
	E E E 4 E E E .
	. E E E E E . .
	. . . . . . . .
