Aaru
----

Aaru was a spinoff from Himeya and C's Ware, publishing about two games a year (1996-2005).
They brought some file formats along from Himeya.

The company name, by the way, is a transliteration of how the English letter "R" is
pronounced. Aaru's credit screens say as much - "Presented by r."
The R probably stands for something, but I don't know what.

Graphics formats used:
- PC98:
  * DA1 (Rose Blood, M Hard, Zest to Fantasy)
- Windows:
  * BMP (Rose Blood, M Hard, Zest to Fantasy, Rabid Helix, Lofty Form, LAG, Kokoro 1,
    Guardian, Enomoto, Koutei Heika)
  * ALP/MON (LAG)

Music formats used:
- PC98:
  * MUF FM music (Rose Blood, M Hard, Zest to Fantasy)
- Windows:
  * MID (Rose Blood, M Hard, Zest to Fantasy, Rabid helix, Lofty Form, LAG, Kokoro 1,
    Guardian, Enomoto)

Sound formats used:
- WAV
- RL1 (Kokoro 1 Voiced)

Script formats used:
- AB (Rose Blood, M Hard, Zest to Fantasy, Rabid Helix, Lofty Form, LAG, Kokoro 1,
  Guardian, Enomoto, Koutei Heika)

Archive formats used:
- FL1 (Rose Blood, M Hard, Zest to Fantasy, Rabid Helix, Lofty Form, LAG, Kokoro 1)
- FL2 (Kokoro 1 Voiced, Guardian, Enomoto, Koutei Heika)
- FL3 (Akairo no Utage, Senryaku Musume)
- FL4 (Kokoro 2 Voiced, Flowers, Kokoro 0)

Missing: Kokoro 2 basic, D.i.G., Naked Edge


### BMP images

These are exactly the same as C's Ware's LZSS-compressed BMP files, except the
file starts with "PD" instead of "LZ".


### ALP and MON images

These are also the same kind of compressed BMP, but for different purposes.
Used in LAG only.

MON are 1bpp alpha masks for sprites. ALP ought to be that too, but is somehow
different and I'm not sure what's going on there...
