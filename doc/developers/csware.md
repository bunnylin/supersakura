Himeya Soft and C's Ware
------------------------

Himeya aren't remembered much for their own PC98 games (1992-1995), but they facilitated
the distribution of their spinoff C's Ware's games, particularly in the west. C's lucked
out with a few very talented staff, and developed such titles as Desire, Xenon, and
Eve Burst Error. Himeya's most notable games might be Bacta 1 and 2, and their
large-animation-focused Zenith and De.FaNa.

Himeya was founded by a few people splitting from Elf, but they didn't bring over any
game tech; Himeya's first game Bacta doesn't share any file formats with Elf games.
When C's Ware split off two years later, they kept the graphics and music formats but
made a new script format. (Another two years later a Himeya founder and a few others
and some C's artists formed Aaru; they carried over the script and graphics formats.)

Graphic formats used:
- PC98:
  * DA1/GDT (Bacta 1, Phobos, D'Ark, D'Ark Gaiden, YES, Kinketsu, Etsugaku, Ash, Desire,
    Zenith, Xenon, Nanaei 1 and 2, Amy, YES-HG, DeFaNa, Eve BE, Gloria, Rabyni)
  * AS2 (Bacta 2)
- Windows:
  * EW (Kinketsu)
  * BMP (Etsugaku, Nana Eiyuu Monogatari, Xenon, Amy, Gloria, Divi-Dead, Re-Leaf)
  * B2 (Desire, Eve BE)
  * B3, B4 (Re-Leaf, Adam DF)
  * GRP/SPR (Ai ga Mie Hajimetara)

Script formats used:
- PC98:
  * COM (Bacta 1, Phobos)
  * MSG (D'Ark, D'Ark Gaiden, YES)
  * BIN (Bacta 2, Zenith, DeFaNa)
  * B (Ash)
  * CMD (Kinketsu)
  * CC (Etsugaku, Desire, Xenon, Amy, Eve BE)
  * BIN/AB (Nanaei 1 and 2)
  * BL (YES-HG)
  * AB (Gloria)
  * BIN (Rabyni)
- Windows:
  * SCR, same as AB? (Kinketsu)
  * SCO, same as AB? (Etsugaku, Xenon, Amy, Eve BE)
  * Hardcoded in exe? (Nana Eiyuu Monogatari, Gloria)
  * $$$/AN, same as AB? (Desire)
  * BIN? (Adam DF)
  * SCN (Ai ga Mie Hajimetara)
  * AB (Divi-Dead)
  * EV/SC? (Re-Leaf)

Music formats used:
- PC98:
  * MLO FM (Bacta 1, Phobos, YES, Kinketsu, Etsugaku)
  * PMD FM (D'Ark, D'Ark Gaiden, Bacta 2, Ash, Desire, Zenith, Xenon, Amy, DeFaNa,
    Eve BE, Nanaei 2, Gloria, Rabyni)
  * MMD midi (D'Ark Gaiden, YES, Bacta 2, Ash, Zenith, Amy, DeFaNa)
  * ML/MDL? (Nanaei 1, YES-HG)
- Windows:
  * MDS (Etsugaku, Nana Eiyuu Monogatari, Amy, Gloria)
  * MID (Kinketsu, Desire, Xenon, Eve BE, Ai ga Mie Hajimetara, Divi-Dead)

Sound formats used:
- WAV
- LDP voice (Desire, Xenon, Amy, Eve BE)
- T2M voice (Adam DF)
- AF2 effects? (Re-Leaf, Adam DF)
- VIC voice (Ai ga Mie Hajimetara)

Video formats used:
- VID (Desire, Xenon, Eve BE)
- AVI (Divi-Dead, Luv wave)

Archive formats used:
- PC98:
  * FA1 (Bacta 2, Zenith, DeFaNa, Rabyni)
  * MIO (Ash, Nanaei 1)
  * DL1 (YES-HG, Nanaei 2)
- Windows:
  * AR (Kinketsu)
  * ARC2 (Etsugaku, Nana Eiyuu Monogatari, Amy, Gloria)
  * ARC3 (Desire, Xenon, Eve BE, Re-Leaf)
  * DL1 (Divi-Dead)
  * ARC, EMC (Chiruhana, Heart & Blade, Sho-Ki, Vist)
  * PCS (Chirin)
  * PGD (Love Producer)
  * DAT (Fugue)
  * SVC container (Luv wave)


##### BMP (Divi-Dead)

These are LZSS-compressed BMP files. Standard LZSS decompression, with a 17-byte offset.

Header:
```
char[2] - signature "LZ"
uint32 - compressed size
uint32 - size when uncompressed
```

The compressed datastream follows the header immediately, and unpacks to a standard BMP.


### ADT

Used in Amy, Eve, Xenon.

These are fullscreen-type short animation files for action events.


### IAF

Graphic files used in Maid Monogatari, CD Girls.
Each appears to contain the signature "BM", but it's not just a Windows BMP.


### SD

Script file used by CD Girls.


### EMP

Used in Vist. "EMSAC-Moving Picture-2" video file.


### PMG

Game script files, used in Love Producer. Encrypted or compressed somehow.
Can also be bitmap files of some kind.


### BMP

Used in Ai ga Mie Hajimetara, a non-standard BMP. Instead of "BM", the sig
is "GR". The rest of the header looks like a BMP, except compression is
0x80000000, which is some custom method.


### BPC

Used in Shinobu - Tsukikage. Single compressed bitmap, looks a lot like
simple Windows DIBs without a BMPFILEHEADER, but with a custom compression method.

Header:
```
uint32 - header size, always 0x28
uint32 - image pixel width
uint32 - image pixel height
uint16 - BMP color planes, always 0x0001
uint16 - BMP bits per pixel, always 0x0008
uint32 - field for BMP compression type, in these files always 0
uint32 - uncompressed number of pixels (width * height)
uint32 - BMP horizontal resolution ppm, usually 0, presumably ignorable
uint32 - BMP vertical resolution ppm, same
uint32 - palette size
uint32 - number of "important" colors, ignore
```

This is directly followed by the palette, an array of uint32's in BGRA order.
The compressed image stream immediately follows the palette.

Could probably figure out the compression scheme easily by looking at the
"logo" and "title" files, compared to the very first graphics in the game...


### BPA

Used in Shinobu - Tsukikage. Mildly compressed set of animation frame bitmaps,
24-bit color. Modified version of BPC, presumably.

Header:
```
uint32 - unknown, seen values are around the range 0A..DC
uint32 - unknown, usually 1
uint8 - compression or type flag? usually 0; if the previous is 9, then this is 1
uint32 - always 0xFFFFFF?
uint32 - header size from this point on? often 0x28
uint32 - total pixel width
uint32 - total pixel height
uint16 - BMP color planes, always 1
uint16 - BMP bits per pixel, always 8
uint32 - BMP compression type, in these files always 0
and so on
```


### SVC

Used in the more recent Windows title: LuvWave.
Starts with a game sig "LuvWave" 0x1A, and at 0x10 a resource type ID.
These can contain audio "SVRAC-Sound-1" and bitmaps "SVRAC-Image-1", at least.

Images begin with a metadata chunk at 0x40.

```
char[16] - "ImageInf4", the rest padded with nulls
uint32 - unknown, 0x100?
uint32 - unknown, 0x20?
uint32 - image pixel width
uint32 - image pixel height
uint32 - unknown, 8?
uint32 - unknown, 0x101010?
uint32 - unknown, 4?
uint32 - unknown, 0?
sint32 - unknown, 0xFFFF F92C = -6D4 = -1748?
uint32 - unknown, 0x7777?
uint32 - unknown, 0xFFFF0000?
uint32 - unknown, 1?
uint32 - unknown, 1?
```

If a palette is present, it's after the info chunk.

```
char[8] - "Palette8"
uint32 - palette data byte size, counting from the first palette item to chunk end
uint32 - unknown, 0?
array of uint32 - palette entries, probably BGRX order, ignore the 4th byte
```

The images appear to support 24-bit images as well as palettised 8-bit ones.
There's an "Image-24" or "Image-08" chunk identifier.
The image compression format could be an LZSS again, but hard to say.
It feels like a bit stream - the image data always ends with a bunch of 0-bits.


### EW images

Used only in the Windows port of Fatal Relations. Little-endian words.

```
uint16 - signature "EW"
uint16 - unknown, always 0001?
uint16 - image width
uint16 - image height
uint16 - 0004 or 0008, perhaps indicates 4/8 bits per pixel
uint16 - palette size, usually 16, can be up to 256
uint8 - encryption key 1
uint8 - encryption key 2
```

The palette starts is at offset 0x18. 4 bytes per color, byte order RGB0.
For 16-color images, the low nibble for each color component is set to F,
but should actually be a copy of the high nibble to correctly match the
original PC98 palette.

The non-compressed encrypted bitmap begins at 0x58. Same decryption
process as with the AR archive. The bitmap is upside-down, like
normal Windows DIBs.


##### Eve BE startup

Purple logo: C fades in, s slides from below, then red bit fades in, then play
a cutting sound effect, then slide in final text below.
Main menu appears, with "Game Start" available. Selecting it makes a "dululi dululi" sound.
Route selection appears, with "Kojiroh" and "Marina" available.
Kojiroh: Run A01 script from the top.
December date fades in at middle of screen, brief tension music eve_29.
Date is in C's purple. Top left corner of date is at about 224,160. (E0,A0 or 1C,A0)
After 4 seconds or so, date fades out. Wait until music done.
Fade in Kojiroh's dump, 001. No viewframe? But yes textbox at bottom.
"Ka-domp" sound effect. Text "ドテッ。" Wait for keypress.
Start background music eve_21. "【小次郎】んくっ‥‥ん‥‥。" Wait for keypress.
Eventually 45 and FB 11 00: ice spell sound effect "ガシャーン！"
After more sitcom, photographer girl appears and we get our first choices.

@1580:
"見る・調べる" FF-01-00 (Look/Examine)
"移動する" FF-02-00 (Move)
"話す" FF-03-00 (Talk)
"聞く" FF-04-00 (Listen)
"依頼のこと" FF-28-00 (Request)

Look/Examine -> brings up @5B45 "あたりの様子" (Around) "茜" (Akane) "カメラ" (Camera)
Move -> brings up option "外に出る" (Go outside); string is found at 7256 and 85AD.
Talk -> brings up @3061 "茜" (Akane)

@74B1: end of scene choices
"見る・調べる" FF-01-00
"移動する" FF-02-00
"考える" FF-03-00 (Think)
"セーブ・ロード" FF-04-00 (Save/Load)
"セーブ" FF-28-00
"ロード" FF-29-00
"絵画捜索の依頼" FF-1E-00 (Request to find painting)
"孔という人物" FF-1F-00 (The person named Ko)

@7E35: end of scene Look objects
"あたりの様子" FF-0A-00 (Around)
"電灯" FF-0B-00 (Lights)
"テーブル" FF-0C-00 (Table)
"仕事机" FF-0D-00 (Workbench)

Look:Around -> @622D
Look:Around again repeats -> @61AE
Look:Akane -> @711F
Look:Akane again repeats -> @7098
Look:Camera -> @5CCF
Look:Camera again repeats -> @5C33
Go outside -> @7308
Go outside again -> @72C8
Talk:Akane -> @5895
Talk:Akane again -> @53EA
Talk:Akane again -> @504B
Talk:Akane again -> @45B7
Talk:Akane again -> @4154, this unlocks a new option: Listen
Talk:Akane again repeats -> @4050
Listen:Request -> @2B84
Listen:Request again -> @2499
Listen:Request again -> @1AE3
Listen:Request again -> @162A
Listen:Request again repeats -> @15F6
Talk:Akane now -> @3E07, Akane departs and Listen is removed
Talk:Akane again repeats -> @3D9C
Look:Camera now repeats -> @5B7F
Look:Akane now -> @6F0F
Look:Akane again -> @6D04
Look:Akane again -> @69A9
Look:Akane again -> @63EF, Akane reappears!?
Look:Akane again repeats -> @634F
Talk:Akane now -> @3A1D
Talk:Akane again -> @3079, eventually jumps to @7453, end of scene options
Go outside now -> @8653
Go outside again repeats -> @860D
Think:Request -> @76E4
Think:Request again repeats -> @75A2
Think:Ko -> @7CA2
Think:Ko again repeats -> @7C31


### Kindan no Ketsuzoku / Fatal Relations, Windows port

These use the AR format for file bundles.

The script files use a different format than the PC98 originals.
Looks like uncompiled script code, so commands are actual keywords,
and the script file is plaintext, with linebreaks (0D 0A) and tab indents.
One command per line. If a line is in double-quotes, it's printed.
Printed lines can have escape codes, like \N for newline.
If there's a comma and another quoted string on the same line, that's
treated as a voice file to be played immediately.

```
G_CLS n - Clear screen to black with transition n
G_LOAD "xxx" - Load named graphic
G_PUT_SCAN n - Screen transition to just loaded graphics
JMP nnn - Jump to start of scene/page nnn
MOV "xxx" - Load new script file, jump to start of first scene/page
MUSIC_LOAD "xxx" - Prepare named music, don't play
MUSIC_START
MUSIC_STOP
PAGE nnn - Scene index number, should be unique within a script file
PEND - End scene/page
T_CLS - Clear something?
WAVEPLAY "xxx",n - Play named sound effect, uncertain what number n is
```


### SCO scripts, Amy/Xenon Windows ports

These are in ARC2 format archives.

The scripts still have some plaintext commands, but mostly it's 16-bit bytecode.
Words are little-endian.

The commands appear to have a lot of unnecessary zeroes interspersed.
Additionally, there's some kind of macro system in use, where plaintext commands
appear by their full name the first time they're used, but then are referenced
by a number if invoked again. The number changes from script to script, most
obviously on text printing commands. Presumably the numbers are assigned in the
order that the commands appear for the first time in each script file...


### Game start sequences

##### Amy startup

Purple logo: C fades in, s slides from below, then red bit fades in, then play
a blululu sound effect, then slide in final text below.
Show "menu" fullscreen graphic with "menu_m" animation. Fade in from black.
Timings: 3 seconds of open eyes, then alternately two blinks or one blink.
Each blink is quite quick, frame 1 -> frame 2 -> frame 1 -> frame 0, where
the opening side feels slightly quicker. Less than 0.5 sec for one blink.
Play opening music.
Press a key to bring up the menu on the right half of the screen.
Options: New game, Load Game, Gallery.
New Game -> Chapter choice 1, 2, or 3 (a01, b01, c01 scripts)
Load Game -> script L
Gallery -> Ladies Room (K) and Music Room (M).


##### Desire startup

Purple logo...
Show menu1, with options:
- "最初から" From the beginning
- "途中から" From the middle
- "おまけ" Omake

From the beginning shows menu3 or menu4, with the options "アルバート・マクドガル"
Albert McDougal and "マコト・イズミ" Makoto Izumi. There's also the secret "?" entry.
These lead to scripts a01, b01, and c01 respectively.

Omake contains "Ladies' room" and "Music room". (menu2)


##### Desire SFX

	15 airplane passenger notification signal, pingggg


##### Xenon startup

Purple logo, same as Eve.
Show "menu1", with Game Start, Data Load, and Special Mode.
Game Start -> run s00.

##### Xenon endings

There are a total of 7 "Dreams". At first, with no Dreams seen, a new game starts
script s00. When you reach the end of s0111, the first dream is complete and you
return to the main menu.

With Dream 1 seen, a new game starts script s00b. This allows access to Dreams
2 and 3A through 3D depending on your first actions in-game. If you hide, you
get Dream 2; if you stay still, you get Dream 3.

With any one of Dreams 2 or 3 being seen, a new game starts script s00c. This has
all paths available. If you leave the room, you get the brief Infinity Dream path.
If you try to call a nurse, you get Dream 1. If you investigate the room, you get
the path for Dreams 2 and 3.

With all 7 endings seen, some kind of bonus mode appears. This probably is the
"Special Room", which contains the "Lady's Room" gallery, and "Music Mode" song player.


##### Kinketsu startup

Show title1 and title2 graphics, play some song, menu box.
Title1 fades in in the middle of the screen, slides up, then the rest appear.
Menu items appear to be New Game, Load Game, and Special (music+gallery).
New Game -> fade out music, fade to black, run s-01 script.


##### Etsugaku startup

Purple logo, sound effect this time is a gentle high gong.
Show "back" graphic. There is no music. Some intro text scrolls past.
The text comes from `op.gdt`, which is a plain shift-jis text file, not a GDT graphic.

Show main menu (menu1), play hs0702.
- "初めから" From the beginning
- "途中から" From the middle
- "おまけ" Omake

Omake contains "回想" Reminiscence, "音楽" Music, and "名前変更" Rename.
The rename option was removed from the voiced Windows versions.

New Game -> run s01 script.

The ending credits for the game are in `st.gdt`, again a plain text file.
