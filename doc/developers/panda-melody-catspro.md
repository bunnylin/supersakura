Panda House, Melody, Cat's Pro
------------------------------

After splitting from IDES (Fairytale etc), Panda House published five PC98 titles
under the Cat's Pro label, and another four under Melody, from 1993 to 1996.
The company went on to be quite productive in the Windows era, putting out about
two games a year until 2007.

They got a headstart by carrying over the Fairytale game engine and reusing it
for the first two Cat's Pro games, Cat's Part 1 and Nova. After this, they
switched to various custom engines.

Engines used:
- Fairytale ADV98V (Cat's Part 1, Nova)
- MAX (Heart Heat Girls, Feti)
- MAXG (Ikenie, Escalation95, Mesuneko Hishoshitsu, Kurayami)
- Some kind of hacked MAX (Night Slave)
- EVE (Windows port: Kurayami, Cat's Part 1, Nova)
- PAS Panda House Adventure System v? (Mesuneko Hishoshitsu?, Melody?)
- PAS Panda House Adventure System v1.30 or 1.50 (Collector)
- Neural developer station? (Kurayami 2)
- Script engine Monochrome by Ikegami Ryuuichi 'Hurry Rabbit+' (Love Lesson)

ValleyBell has some excellent tools and research on the MAX engines:
https://github.com/ValleyBell/PC98VNResearch/tree/master/panda_max

Archive formats used:
- PCK (Escalation95, Mesuneko Hishoshitsu, Night Slave, Kurayami)

Music formats used:
- USO
- MFD

Script formats used:
- MES (Cat's Part 1, Nova)
- MDR (the other PC98 titles)
- CMF? Action-section script? (Night Slave)

Graphic formats used:
- GPC/GPA (Cat's Part 1, Nova)
- VDF (the other PC98 titles)
- HDF sprites (Night Slave)

- CDF? WDF?
- SDF? RDF? TDF? Stage maps or something (Night Slave)
