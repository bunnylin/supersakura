Renderer design notes
---------------------

SuperSakura has an outputBuffy which is the full game window bitmap.
Game graphics are drawn using virtual viewports, with dirty rectangle tracking.

The SDL renderer has a Renderer object and a single main texture, which is
another full game window bitmap. When SuperSakura has finished updating
outputBuffy, the dirty rectangles are copied into the main texture, and the
entire main texture is pushed into the Renderer pipeline as a ready frame.
After sleeping a bit for frame rate purposes, SuperSakura signals the
Renderer to present the next pipelined frame.

This is based on:
wiki.libsdl.org/MigrationGuide#If_your_game_just_wants_to_get_fully-rendered_frames_to_the_screen

There is a fair bit of buffer copying, but modern pipelines may force it.
For some special effects, outputBuffy should be readable, so it must be in
RAM. It could be an SDL surface, but it's easier to just have a manually-managed
memory block, as there's no difference when copying into the texture. Also,
the console port must not depend on SDL, so our own buffer is a must.

The main texture is hopefully in VRAM, so shouldn't be read, and is copied into
further VRAM buffers managed by the SDL Renderer. These are periodically exposed
to the OS window manager one by one, which blits them into one of 2-3 display
buffers, which is eventually flipped into the user's view.


### Scaling, subpixel position, viewports

The original games tend to run in 640x400, but most graphics are inside
a viewframe, so the effective resolution is often less. Viewframes are not
necessary anymore, so they should be optional; the contents of a viewframe
thus should be scaled up to fullscreen. These games also have lots of textboxes,
which ought to be customisable and rendered directly at the best final size,
rather than using blurry upscaling from 640x400 or something.

One complication presented by this is that viewframes use non-standard aspect
ratios, like 480:296, which is not quite 16:10. As a result, going up to
fullscreen means a non-integer scaling multiplier. Some graphics, like blinking
animations, must be aligned pixel-perfectly on their parent sprites, which is
not possible with non-integer multipliers without allowing for sub-pixel
alignment.

There are various ways to handle subpixel positioning, but no perfect way that
would meet all requirements: no visual artifacts, no unnecessary CPU use, minimal
RAM use, while remaining a software renderer.

- Render into an original-resolution buffer with pixel precision, then scale that
  up to final buffer with bilinear: needs more than one original-res buffer for
  framed and frameless game views, extra buffers use more RAM, extra buffer copy
  uses more CPU.
- Use integer-multiple intermediate buffers, scale up to final buffer with bilinear:
  same problem.
- Allow only an integer-multiple final buffer, applying letterboxing for the remainder: 
  no extra buffers needed, but letterboxing gets extreme, like base resolution 480x296
  which in full HD would be only 1440x888, an unsatisfying fullscreen experience.
- Use a hardware renderer which handles subpixel positioning automatically as part of
  antialiasing: Fast but completely different rendering paradigm, I prefer software.
- Shift loaded graphics' subpixel position: Imperfect if done at loadtime, since the
  graphic can move at runtime; uses much CPU if done at runtime.
- Render into a super-resolution intermediate buffer, scale down to final with bilinear:
  Even more RAM and CPU used.
- Most graphics don't need subpixel precision, so only render affected things into
  an integer-multiple intermediate buffer, then scale up to final with bilinear:
  more complex to implement, but might represent a tolerable balance...

Eye blinking probably has to be composited over the base body at original resolution,
separately for every frame! Then nx-scaled, then bilinear-scaled to final buffer.
Basically, a pixel-perfect blinking animation ends up having to incorporate the full
sprite body in every frame, very wasteful. Compared to that, rendering into an
integer-multiple buffer is more efficient, but then you can't apply nx-scaling on the
sprite+eyes composite as a single unit, so sprite pixels can melt into background pixels
or eye edges have a noticeable discontinuity with sprite base.

Virtual viewports that are scaled up by an integer multiplier, which allows using
existing scaling nx algorithms at loadtime. All the complex bitmap layering stuff
goes in these. Modified parts can then be copied to the output buffer with fractional
scaling applied.

The SuperSakura renderer then tracks gobs, boxes, and viewport effects needing
redraw. Gobs are composited into viewport buffers, effects modify those buffers,
and all dirty rectangles are blitted to outputBuffy. Each box is relative to
a viewport, and renders in ascending index order immediately after the viewport,
with further viewports on top.


### Dynamic graphic objects

For the most part, graphics come from image files. These are turned into TGraphicFile
objects that carry descriptive metadata, and on load become TGraphicObjects bearing
the actual bitmap. Those are provided on the asset loader side; the SuperSakura
engine has a further gob type that describes a graphical thing's screen location and
any applicable special effects. A gob typically carries a TGraphicObject reference,
used by the renderer.

If a very simple graphic is needed, like a flat rectangle, it's more efficient to
use a direct runtime render fill, not keeping a bitmap in memory at all. In this
case a gob has a special name and no TGraphicObject reference. The renderer detects
this and fills the gob's screen position directly rather than blitting a bitmap.

The special name format for runtime flat graphics is: `|cccc`, a pipe character
followed by 1 to 4 hexadecimal digits representing an RGBA color.

An extra-special case is a gob with no source object name at all. This is treated
as a runtime flat rectangle that is fully transparent, and as such not drawn at all.
Such virtual gobs are useful for designating particular screen rectangles for
mouseovers or for location snapping spacers.

There are two further kinds of dynamic graphic objects: gradient rectangles, and
altered copies of existing bitmaps. Both of these need to be backed by a bitmap in
memory, but have no corresponding image file on disk.

When a gob like this is created, the script may specify a 32k size for the gob.
In case of gradient rectangles, this is mandatory, since it's the only size indication.
Altered bitmaps can derive their size from the source bitmap, so the gob can be created
without specifying a size. Either way, the 32k size must be known right after gob
creation. This is later converted to a viewport-relative pixel size for fetching the
final bitmap.

Gradients ought to be defined somewhat similarly to CSS gradients, since those are
already well-defined and well-understood. However, at least initially, only the
simplest gradient types will be implemented. Support for more complex types can be
added later if something actually needs them.

Bitmap alterations are typically needed when dealing with sprite sheets or tilemaps.
Tilemaps typically contain many regular-sized frames, but sometimes may have frames
from multiple animations, where not every frame is the same size. Since SuperSakura
only accepts invariable-size frames, those frames need to be separated so each bitmap
contains only frames of the same size. The separation is done at loadtime as part of
general frame rearranging, and must be specified in the inf metadata file if needed.
After this, any number of frames can be lifted into a dynamic graphic with a single
rectangle copy.

For best efficiency, any source frames being copied to a new dynamic graphic should be
resized while copying, to avoid doing an extra normal-sized copy.

Also for efficiency, the command string should be quick to parse and not parsed more
often than needed. For a copy command, this means the source rect size should be in
the first parameters.

The special name format has the following forms:
- `>flat <RGBA>` to generate a bitmap of a flat color, 1-4 hexadecimals
- `>linear-gradient [direction] <color-stop> <color-stop> ...` to generate a gradient
  bitmap similar to what CSS would produce
  * direction can be "to <location> [location]" or "<number>deg" or "<number>.<number>"
    fraction; locations are "left", "right", "top", and "bottom", indicating the edge
    or corner where the gradient's last color goes; default is 180deg or 0.5, from top
    "to bottom"; angles loop around, and negative values work
  * a straight vertical and horizontal gradient could use a faster codepath
  * color stops must have an RGBA color of up to 4 hex digits (named colors not
    supported); there must be at least two colors, with no soft upper limit
  * by default colors are placed at even distances, but you can add one or two
    stop values after a color definition to select the color position in the gradient;
    the stop values can be "<number>%" or "<number.number>" fraction
- `>copy <source_image> <widthp> <frameheightp> [src x = 0] [src y = 0] [framecount = 1]`
  to copy an existing graphic in part or completely; if the rectangle contains multiple
  frames, that should be specified as the optional final argument
- Command parts are separated by whitespace; commas are treated as whitespace

Probably stuff like beveling and texture tiling ought to go through this system too?
Then again, there is some efficiency in rendering those more directly in box buffers.
What if the base buffer is scrapped and a generated graphic used directly?


### Transitions and other effects

Normal rendering flow is to render all gobs and boxes in refresh rects directly into
outputBuffy, which gets sent to SDL for display.

Some full-viewport effects, most notably transitions, would benefit from switching to
an indirect approach. The game view is rendered into a workBuffy instead of
outputBuffy, then a transformation effect is applied, the result going in outputBuffy.
When the effect begins, outputBuffy is renamed to workBuffy to retain existing
graphics, and a new fresh outputBuffy is created. In case of a transition effect,
workBuffy is immediately copied to a stashBuffy. Each frame, the outgoing image in
stashBuffy is drawn with a transition effect over workBuffy with the ongoing game view.

The benefit is that the game view itself carries over between frames, so there's no
need to re-blit every gob and box, if those haven't changed. This is probably the most
efficient way to implement it. The downside is added complexity in needing a new
specialised pipeline. And any boxes that should be exempt from the transition effect
would have to have an extra rendering phase after the transition is applied.

A more flexible alternative is to treat a transformation as a dynamic gob rather than
a buffer. In this case, the single pipeline continues rendering directly to
outputBuffy. When a transition begins, the outgoing view is copied to a new dynamic
gob, which is drawn at a very high Z-level. As the transition progresses, less of the
gob is drawn, revealing the new ongoing game view. The cost is that all other gobs
and boxes end up redrawn every frame as well. The benefit is that, with gobs and boxes
unified into TElements, the same shared transition effect can be used for gob and box
transitions, not just screen transitions.

Assuming the renderer is efficient, and not all transitions necessarily force the
entire viewport to refresh, I feel the extra power and flexibility of the second
approach is irresistible.

Beside transitions, other full-viewport effects to consider: simple flash (create
a flat dynamic gob and alpha-fade it), lightning flash (again a flat dynamic gob,
but different render method that leaves blacks in place for max contrast),
godrays (probably have to have a render hack, keeping a bitmap for this would be
wasteful), color adjust for lightness/sepia/warmth (flat dynamic gob with
a special pixel shading render method), horizontal blur for dizziness (another
special pixel shading render method).


### Messy notes

[jump index should be built using only numbers that are
multiples of four, makes it easier to jump]

gob actual a = inherited gob nominal a from all parents, multiplied 
together
graphic pixels get premultiplied by px alpha
accumulated value starts at 0
available a starts at 100%

for each output pixel where gob's actual a < 100% (DrawRGBA32Alpha):
inc(accumulated value, gob px value * gob actual a * available a)
dec(available a, px alpha * gob actual a * available a)

if available a = 0 then skip to post-processing special effects
if there are no gob actual a < 100% elements below this, and px a = 100%; 
skip the rest of the elements, go straight to post-processing special 
effects
if there are gob actual a < 100% elements below this, and px a = 100%; 
skip to the next non-descendant of this element's parent


for each output pixel where px alpha may be < 100% (DrawRGBA32):
inc(accumulated value, gob px value * available a)
dec(available a, px alpha * available a)
if available a = 0 then skip to post-processing special effects

for each output pixel otherwise (DrawRGB24):
inc(accumulated value, gob px value * available a)
available a := 0
skip to post-processing special effects


   sprite: actual a=60%, nominal a=100%, px a=100%, child of
  sprite: actual a=60%, nominal a=100%, px a=100%, child of
 sprite: actual a=60%, nominal a=60%, px a=100%, child of
bkg: actual a=100%, nominal a=100%, px a=100%

