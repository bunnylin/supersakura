TO DO
=====

### Stuff that should be done

- TTextbox should derive from TElement
- May have to separate text element from box element so hilite can be between them
- Possibly box base should be a gob in that case?
  * Decorations should definitely be gobs relative to parent textbox
- Fix C's Arc3 extractor
- Fix box growshrink effect, probably should promote these into proper Effects
- If loading game saved during effect, fiber may hang in WaitFx? Nocturn first event fades
- When Emi pummels you, flash and bash are out of synch
- Flatsnake
- Change tfileloader refs in decomp from loader to src; speed, size diff?
- Use a double-width X when calculating ex-width of box using Japanese language
- Blog pages should have atom link in nav bar
- Site should be a bit wider; paragraphs are fine, but add h-padding, wider glass


### Upcoming game resource conversions

- Integrate Koukan Nikki script converter
- Integrate DO/ZyX script converter
- Integrate C's AB script converter
- MegaTech resource conversions
- Integrated HDI/FDI extraction
- Old Jast game resource extraction


### Upcoming user-facing features

- Persistent game var memory [0.96]
  * Allows gradually unlocking progress
  * Unlock Desire final route when first two completed
  * Unlock Xenon progress on route completions
  * Route switching for Eve, unlock final route when first two completed
  * Probably a "persist" flag for variables? Save in game's global save file,
    don't wipe during varmon reset otherwise

- Count time played for each game, save in game's global save file [0.96]
  * If there's no player input for 2 minutes, pause the timer
  * Show the value in the frontend, bottom left corner, for highlighted game
  * Try to discount fast-forward time doubling; maybe add to play time on
    any input with tick comparison to last input, capped at 2 min

- Clickable map for Foreigner [0.96]

- Improve game text transcription [0.96]
  * Should clean up escapes and not print choice stuff

- Intros for the prime three games [0.98]

- Polished ending scripts for the prime three games [0.98]

- Viewframe option for the prime three games [0.98]

- Character bios for Sakura [0.98]

- Dramatis personae for Eden [0.98]

- Clickable map for FromH [0.98]

- Friendly game data converter button on front end [0.98]
  * Shell out to sakutool from supersakura
  * Source dir selection may require a separate UI, current choicebox
    design doesn't really work with directory tree traversal; perhaps
    just put in a text field and tell the user to type the directory...
    Vertical-only choicebox could forward left/right presses to area
    events, allowing a file list and actions at the same time?
  * If possible, accept drag and drop of a folder to start conversion

- Bunnysound beyond basic midi [0.98]
  * Use shiny, new SDL_Sound for file format decoding
  * Port FLAC encoder Flake to Pascal: pasflake unit
  * When ripping games, depending on original engine, either use pasflake,
    or try to shell out to oggenc, then fall back to pasflake; older noisy
    waves are best flac'd, newer games with hifi bgm/speech waves can use
    ogg without bad loss of quality; if ogg encoder is not locally provided,
    that's the user's problem, but the game still runs using flac files
  * Accept any format that SDL_Sound can convert; if the user wants to convert
    game sounds to a more exotic format, that's still covered
  * Wave sounds in a dat can be marked in inf as "resident", in which case
    they are decompressed to raw waves at dat load and remain in memory for
    the dat's lifetime; this is for sound effects that need to play without
    delays; music and speech can tolerate some latency so they are loaded
    into memory on demand and converted to raw wave while being played;
    BunnySound needs a "nonresident" property for waves, so such will be
    unloaded from memory when the wave playback is complete

- Silver star on frontend for completed games, gold star for 100% graphics [1.00]
  * Graphics completion should only count backgrounds and overlays, no sprites

- Warning on frontend for games converted with older tools [1.00]
  * Put sakutool version in data.inf on extraction
  * If version in data.inf, save it in dat
  * When loading a dat, if dat format version is not same as engine, only load
    metadata for dat and show on frontend as incompatible, needs reconversion
  * If sakutool version in dat is different in the first 5 chars, try to load it
    normally but show on frontend as outdated, could benefit from reconversion

- Save/load dialog [1.00]
  * Access individual gamename.xx.sav files

- Autosave [1.00]
  * 2+ slots, use a timer and save once every two minutes on the next label
    change, but only if saves allowed; slot count and period configurable

- Review and improve accessibility [1.00]
  * See gameaccessibilityguidelines.com
  * Option to use a gentle pulse rather than bright flash effect, gentle by default
  * Light and dark text boxes for each game

- Settings dialog [1.00]
  * Video and audio config
  * Needs a simple widget system of our own; existing ones would probably
    work well except I'd like gamepad support; most likely layout will be
    something like PPSSPP menus, a vertical list that scrolls, with the item
    names on the left and the values on the right

- Automatic translation [1.00]
  * Shell out to translate-shell, add a button in-engine for auto-translating
    a game; needs a progress bar with eta


### Upcoming internal features

- Sakutool "clean" command for one or all game resources [0.96]
  * Leaves built dat in place, can be called when ripping from front end so it
    removes the work files; does not touch override directory

- Sakutool command to dump string table from a dat [0.96]

- Allow labels with empty script, "@.dothing:" [0.96]
  * When using goto or call label without script specified, it first tries to find the
    label in the calling script, but then it should also check empty script
  * Where the "not a word of power" error is generated, convert that to an implicit
    call to a label; call should take dynamic parameters that on label start can be
    placed in variables; this serves as a hacky proc/func/macro system

- Gobs and textboxes must be unified so a textbox is a graphic provider [0.96]
  * The textbox and viewport classes will have to derive from gobs
  * Box decors ought to be gobs too, child of box gob; remove current decor setup;
    may need a new scaling parameter to leave scaled bitmap aspect ratio intact
  * The box gob generates its own bitmap, so renderer must know not to ask mcsassm
    for a gob's content if it's a TTextBox

- Dynamic graphics basics [0.96]
  * Use a prefix to indicate a dynamic request, then either the filename or another
    prefix to generate at will; further colon-separated commands, applied in order:
    `?CUBERT1:resize 8192` or `?CHAR_M:crop 80,0,240,280` or `??black:size 32768,32768`
    or `??rgbaF00F` or `??rgba F00F,B00B,8888,0000`
  * Not a question mark, looks awful. Alternatives: lt, gt, pipe, asterisk
    `|F123` `>F123` `<F123` `*F123`
    The lt doesn't look good, asterisk looks confusingly like C deref,
    let's use gt for loadtime and pipe for runtime
  * DONE: Runtime ones are plain flat rectangles of a single color; no point holding a bitmap
    in memory at all, just have a special fill function when one of these is drawn
  * DONE: If a loadtime one is a modification over a runtime one, the runtime must be first
    converted in place to a loadtime bitmap
  * Another special case: in GetGraphicObject, if a file doesn't exist but said graphic
    does at another size, just resize from the previous. If the engine provided an image
    without a backing file, this can scale it automatically.
  * Loadtime ones can be more involved, a string of commands that mcsassm uses to
    generate a bitmap when the graphic is requested; eg. a 4-color gradient

- Link SDL2 dynamically [0.96]
  * First make a small test program with define-blocked static vs dynamic SDL
    function references, verify that dynamic results in a smaller binary
  * Make a new SDL2 pascal unit with only the stuff I need; organization must
    be compatible with the community header port so it's a drop-in replacement
  * Could reduce binary size by 20%, but I'll be happy for 10%

- Save state fiber pointer verification [0.96]
  * If the underlying scripts have changed, a save state may be broken
  * It would be good to autodetect and warn the user about this
  * For each callstack level of each fiber, save the next dword from the execution offset
  * If it's at the end of a label, save 127-esc's for the out of bounds part
  * When loading, check if the dwords still match; if not, notify the user the save
    may be broken
  * It could be possible to scan a kilobyte backward and ahead to see if that dword can
    be found somewhere, and automatically adjust the execution offset there; if not found,
    move execution offset to the start of that label; if no such label, then to the start
    of that script; if no such script, then delete that callstack item and hope for the best

- Improve conversion speed [0.96]
  * Try faster bitgroup reads, see ryg's Reading bits in far too many ways
  * Optimise script compiler; tight inbuf read loops for string-read states may up speed?
    Reading label, comment, decnum, dotnum, hexnum, quostr, ministr, minitrue, param;
    perhaps using goto to jump between parser states would be useful, and nostalgic

- Reduce binary size [0.96]
  * Replace "process" unit (half a meg!) with a "miniexec"
  * Replace some UTF8strings with shortstrings or pchars
  * Write a blog post about modern binary space usage

- Use deflate+fse for script and string table compression [0.96]

- PNGX and other image optimisation tooling [0.96]
  * mcgloder should allow quick compression at level 1~3 or so, but also slow
    optimising compression at highest level and testing the best row filters for
    the next two rows before compressing each row; might juggle dictionaries to
    plot the fastest sequence of filters; don't even try average filter if indexed;
    don't use Z_DEFAULT_COMPRESSION (6), neither fast or good
  * Try to offer deflate dictionary support; sakutool build could generate a dictionary
    from the banner image, then use that to compress everything including the banner
    image again; better yet, indicate in inf a representative training image so the
    banner is only used as fallback; dictionary is only used in built dat, not allowed
    with loose files
  * Add -x switch to sakutool extract to generate PNGXs; since PNGX is non-standard,
    there's no tooling for it, so extracted graphics must be vanilla by default;
    using sakutool rip on a PNGX can be used to convert back to PNG and vice versa;
    also needs a switch for producing image with extra hacks
  * sakutool extract by default produces standard PNGs with fast compression only;
    on request can produce a slow-optimised PNGX without extra hacks
  * sakutool build needs an -optimise switch, by default off on commandline but
    included via frontend, which attempts to recompress all images as PNGX plus hacks;
    without the switch, all images are copied into the dat verbatim
  * Try to xor every frame of multiframe images with the previous frame; should be
    done before expand to 8bpp for speed; compare pngout smallest result with or
    without this; if strategy is beneficial, add to sakutool build optimisations,
    only used when building an optimised dat

- PNGX format [0.96]
  * mcgloder should support deflate-fse codec in images instead of deflate; must set
    compression type to 1 in IHDR
  * deflate-fse needs a paszlib2 unit
  * Try adding an up two rows filter and an extra flip nibbles on this row bit;
    only flips nibbles if indexed 4bpp; flips bit quartets or pairs at 2 or 1 bpp
    respectively, helping with dithering; either hack only used when building an
    optimised dat, and needs to set filtering method 1 in IHDR; verify that optimal
    filter selector does use them occasionally
  * Nibble flip may do much the same as simple XOR by above or XOR by left...
  * Try adding a median adaptive filter, possibly better Paeth variant:
    P = (if NW > max(N, W)) min(N, W), (if NW < min(N, W)) max(N, W), (else) N + W - NW
  * Double-filtering? The filter byte's top nibble could select a filter too, to
    be run after the first, or default 0 for no second filter; would have to adapt
    encoder and decoder since second filter uses values from first filter results
    for predictions so have to keep previous row or two of first filter results;
    this can logically only ever improve compression ratio, but by how much relative
    to the nearly double encoding time?
  * Try leaving out the filter byte and instead start with filter 0, then for each
    pixel in sequence use same filter as previous pixel if prediction was correct
    for previous pixel, otherwise check every filter for previous pixel and carry
    over whichever would have predicted most correctly; variant - remember the error
    from each filter for previous 4 bytes, for next byte use whichever brought the
    lowest total error; could use any small n bytes without performance impact using
    a sliding window
  * It would be better to keep filter bytes separate from the image data, to avoid
    polluting the data stream's compressability; and you could decompress the image
    straight into a bitmap buffer and apply the filtering in place, gaining a notable
    speed boost for filter 0 rows; see Nigel Tao's fastest, safest png decoder;
    but this would break PNG format design pretty badly, and how much better would it
    really be? Try it and find out! Pack filter bytes in reverse order at end of the
    bitmap, so the total bitmap buffer remains the same size
  * For indexed color images being well-optimised, try to sort palette indexes so the
    most used are in the bottom? This may minimise encodable difference between
    predicted and actual pixel; ECT has some version of this
  * Using a move-to-front transform like Pi as row filter may be even superior to above
  * Include color depth stuff from cpng proposal
  * Save alpha channel separately for truecolor+alpha, that data has a different modality
    so putting it in a separate chunk should improve compression
  * When encoding, look for lowest diff from 0 when deciding which filter to use,
    or do a mini-compress speculatively for stronger compression level
  * Only need to look at first half, quarter, or eighth (but at minimum image width) to
    make a reasonable guess for the rest of the row on best filter type
  * Decide on new filter only every x rows, same filter probably works fine for several
    rows even if not optimal, for greater compression speed
  * When building an optimised dat, instead of saving flipped truecolor RGB in one
    chunk and alpha values in another, save the native BGRA dword for each palette
    index in a new chunk: PALA
  * When building an optimised dat, don't calculate the CRC, just save a 0 dword
  * Set up a benchmarker for speed/size, then optimise mcgloder in general

- Replace animation definitions [0.98]
  * Run code snippets in EffectHub
  * Anim snippets should be a list of their own, and gobs and box decors point to
    list items; solution should support JRPG-style sprites too...
  * Snippet is a sakurascript labelful, which gets the current seq and frame as params
    and returns the modified ones likewise, hopefully efficient

- If graphic not found, try graphic.frame automatically [0.98]
  * Some multiframe graphics are not a good fit for a single bitmap, so they have to go in
    separate PNG files, eg. abc.0.png, abc.1.png... In this case, `show abc frame:1` would
    see that there is no abc.png, and would show the single frame of abc.1.png
  * Individual frames each have a different hard offset
  * Might be necessary for some Himeya, Fairytale, Sogna games; have to see case by case

- Unify conversion hack definitions [0.98]
  * These should go in the inf files?
  * Various source byte operations, defined as game, filename, offset,
    expected byte series, replacement/insertion string
  * Should hardcoded graphic metadata go there too? Palette tweaks, alpha
    presence expectation...
  * Post-proc compositing should perhaps always use the C's approach of
    loading the original base file instead of reloading PNGs at the end

- Allow pre- and post-label injection [0.98]
  * Use "@[script.]label@before.[script.]label:" and "@[script.]label@after.[script.]label:"
  * This allows extending existing labels instead of overwriting entirely, useful
    for modifying metascripts or doing other game mods
  * Does not alter how existing labels or fibers work, so not a breaking change
  * @after is placed in nextLabel, which is usually already defined; the existing
    nextLabel is adopted into this new script
  * Must parse this special case @, probably ignore whitespace if followed by the second @
  * A label is entered either by falling through or directly jumping
  * Both cases must execute a "before" label once, then continue with the named label
  * Must know that "before" was triggered and not trigger it again for the same entry
  * Can't have a preface without an extra flag to prevent named label entry from preface
    triggering preface again. With multiple prefaces, gets messy, so no preface field.
  * Can't directly append/prefix to label code at buildtime since label code is not
    available if doing a mod
  * Append/prefix to label code at loadtime? Would still have to renumber unique strings
    for one or the other script, no good.
  * Can't do a preface call since then couldn't cancel named label with immediate return
  * Label pre-emption may be the only option!
  * Can't pre-empt label with @before because a subsequent @after applies to a wrong label!
  * If a game is language-patched, label offsets may have changed, so can't pre-empt label
    for original game and expect to work for language-patched too!
  * Delayed compilation, only compile at loadtime so unique strings have proper indexes?
  * Track all befores and afters at loadtime. Each label is first loaded by its unique name.
    After all dats loaded, check that each before and after has an existing target label.
    Warn loudly if one hasn't. For appending, just insert into the fallthrough order.
    For prefixing, encase new label in uniqueofs xxx and 0 commands, and append its strings
    to the named label's strings. Change unique string fetcher to add the ofs, which is
    reset on new label entry.
    Therefore, the before label still exists by its unique name, but a copy of it was
    placed in the named label. The after label isn't copied, its fallthrough next label
    just gets renamed.

- Converge aspects of visual effects toward CSS naming conventions [0.98]
  * Timing types could use "ease-in" etc

- Viewport-oriented rendering [0.98]
  * Each viewport uses an integer size multiple, auto or user-configurable
  * Graphics are cached at integer multiples and drawn into viewports; this allows
    using any existing nx scaling algorithms
  * Viewport contents are blitted into framebuffy with runtime resize
  * Z-values now only apply within viewport, viewports are drawn lowest to highest
  * Colorisation effect to tweak RGB and HSL or warmth...

- Text outlines [0.98]
  * RGBA color, thickness, offset, and the option to fade alpha to transparent
    toward the outline edge; the thickness and offset must be 32k values relative
    to fontheight
  * Precalculate a 2-dimensional outline kernel, can carry over any number of prints
  * Then for each output pixel find closest opaque and grab outline from the kernel;
    really it's an x/y-vector to closest; but there will be a lot of conditionals
    which will wreck jump prediction so speed may suck
  * If moving in scanline order, the half of pixels to the left of current retains
    its closest pixel as it moves away, the only possible new closest are whatever
    pixels cross to left of center column; almost halves amount of comparisons
  * Saving may be possible on right side, considering if closest pixel is right of
    center column then it remains closest at the next step unless the rightmost
    column entering the kernel has a closer pixel; but if closest is in the center
    column then any other may be next closest as the center crosses leftward
  * Track opaque pixels in an array? Add any that appear on the right of kernel,
    drop any that are positioned so that they logically can never be closer than
    another opaque pixel, and drop any that have moved across the center column;
    but still the short loops and many conditionals may be very heavy
  * Particular order of pixels spiralling out from center of kernel? Stop at the
    first opaque, and that can use the precalculated table flattened to 1 dimension;
    good for densely opaque areas but wasteful on transparent areas
  * Some kind of bit field, where each possible meaningful distance from center of
    kernel has a precalculated bit magnitude? For each kernel cell, AND with the
    input pixel's opaqueness, and OR every kernel cell together; highest set bit
    indicates distance to closest opaque pixel; but semi-transparent pixels require
    mangling which complicates things; could check close neighborhood first and
    exit early; minimises conditionals, but needs more than 32 bits in field

- New dependenciless native pixel scaler unit [0.98]
  * Must support BGRX and BGRA separately, although some BGRX can forward to BGRA
  * Must have nearest-neighbor 2x, 3x, 4x; 6x and 8x can forward to a pair of those
  * Must have Scale2x and Scale3x; 4x, 6x, and 8x can forward to those
  * Maybe the MMPX 2x scaler
  * Perhaps a CUT3 scaler
  * Remove scalers from MCGLoder, mcg_bitmap.Resize calls a new more generic fast,
    fractional bilinear scaler only; mcsassm can call scaler unit for the special
    scalers on asset load; viewport renderer then uses fast bilinear on dirty
    rectangles to get the perfect final size to blit
  * Try out a bibloom scaler; operate in linear energy space, diffusing pixel energy
    up to 4px sideways, maybe 0-4px vertically; for scaling, bilinearry interpolate
    a spot in the diffused energy field; then convert back to sRGB; maybe start
    with a non-diffused variant, see how it compares to basic bilinear

- Make fibercmds self-documenting [1.00]
  * The exact param order and default values, and textual description of each
    sakurascript command are best documented in fibercmds.pas right beside the
    implementation
  * Doxygen or PasDoc are geared toward the project's own source, but this is for
    describing the script language implemented in this project, so needs a custom tool
  * Double-slash and forward angle for the command sample: //> gfx.bop param [param=1]
  * Multiple double-slash and colon for description, after that: //: Makes the gfx bop!
  * etc/makedoc.py to generate script commands reference doc/sakurascript/cmdref.md
  * Also generate doc/sakurascript/readme.md from parsertest for intro + syntax
  * Separate syntax from command functionality, new md with thorough descriptions of
    engine components and how to use them with example commands
  * Run makedoc on git submit, only gen if src timestamp newer than md, or md not found
  * Only upload generated docs on release, users can gen in-dev docs themselves
  * It should also check that doc sub-readmes have complete lists for contained items
  * Change debug console interactive help to pull descriptions from the cmdref.md

- Refactor and modularise Beautify into a unit, re-enable in sakutool [1.00]
  * Must allow beautified and non-beautified assets to exist at the same time so
    user can flip between them at runtime; script-controllable engine variable, which
    gets saved in save state
  * Front end ripper needs a radio button for Original graphics only vs Original
    and beautified graphics (slow); in-game settings needs a button for Generate
    beautified graphics or Re-generate beautified graphics if already done;
    determine if beautified by checking if beautified version of banner image exists;
    beautified image gets @ suffix
  * Runtime generate must reload graphics only so they become immediately available
    without messing script positions
  * Engine shells out to sakutool for this, gets back progress messages; must offer
    cancel button; banner image is always last to be beautified, and if it's not
    beautified yet then any image for which a beautified version exists is not
    re-processed
  * Beautified during ripping go in gfx directory and get built in dat; during runtime
    go in override directory
  * newdata.inf must have beautifiable flag to enable in-engine beautification, only
    set for known old games that need it, but not for eg. the front end or winterq

- Decomp input file prioritisation [1.00]
  * Handle cases where multiple different versions of the same file exist in one game
  * See decomp_core "prioritisation" for plan

- FLACX [1.00]
  * FLAC's design is a bit overengineered, but not stupidly complicated, and it has
    space in the headers for defining a new residual encoding method
  * Try using FSE for residual encoding instead of Rice encoding; may require larger
    blocks for best compression?.. Might yield 5-10% improvement
  * Perhaps allow reusing previous FSE section header if skipping the header saves
    more space than would be gained from writing a header with optimal parameters;
    but this would make each block dependent on the previous block, bad for seeking
- While at it, evaluate wavpack; its integer math preference claim is appealing,
  but can it match flac's compression and efficiency? Let's find out...

- Sakutool command for decompressing a savefile in place [1.00]

- Skip seen text [1.00]
  * Currently skips all text, seen or not
  * Must be implemented per-string, unique strings only; to spare SSDs only
    dump state to disk on each save, autosave, or exit

- Dynamic graphics advanced [1.00]
  * Possible additional effects over generated and loaded bitmaps
  * Also allow a "dynamic" item in inf files, which makes the engine load the given
    dynamic string instead of the graphic; this implies if the graphic file exists,
    it is ignored when building DATs; this can be used to replace some unnecessary
    repetitive images, and to extract irregular frames from imagemaps


### Build, test, distribution

- Compatibility table on website [0.96]
  * Needs to come from a single definitive source, easily editable
  * Writing them in games' inf files no good, too spread out
  * Could have a markdown table file, very easy to parse and nicely viewable;
    gidtable.inc could be generated from this file automatically
  * Table should have: shortname, displayname, crc, engine name, support level,
    developer/publisher, comment, vndb id, review score and link
  * Gidtable only needs shortname, displayname, crc, and support level
  * Considerations: maintainability, viewability, speed, size
  * Gidtable is mostly random-accessed via the shortname enum, otherwise linear access;
    perhaps the displaynames can be entropy-encoded, use static Huff per displayname;
    if that can avoid the 4/8-byte alignment, even better; ensure ECompat is saved as byte

- Improve the main site, needs better download section at least; move
  progress page to be part of main page to make space?

- Add test script, don't want to upload a broken release [0.98]; Partially in place...
  * Compress an 8x8 4-color checkerboard in every supported graphics format,
    put them in a testpics.zip file in doc/gfx; a verification step should
    decomp that zip, which goes into unknown/gfx, and confirm that all produced
    png files are identical; delete unknown/gfx/testpic-*.png before and after

- Add a simple makefile for Linux deployment [0.98]

- Add flatpak packaging; supposedly preferred installation method on steamdecks [0.98]

- Improved autoplay mechanism [1.00]
  * Should allow playthrough for each game
  * Maybe even a python or other shellable script that accepts log prints from the game
    and returns actions in stdout? Could change mode depending on which script/label
    currently active; random brute, systematic brute, or specific sequence of actions
  * This allows serious CI for ensuring games remain completable, and playthrough
    scripts could be contributed by anyone
  * Perhaps easier to implement: an int command to switch autoplay strategy, and a cmdline
    option to import a particular override script, which contains pre-label etc hooks
    solely with those int commands and a sys.quit at the appropriate place
  * This CI-exclusive stuff should perhaps not be in the main repo
  * Also needs some file list thing with a minimal set of files defined for each game that
    should be extracted for a smoke test; clean, extract files matching list, make, play;
    could be annoying pulling specific files from various multifile archives


### Other random stuff for later

- General purpose extractor for resources from Windows exe files, there are
  at least icons and stuff embedded in some of those
- Convert CUR and ANI files used for custom cursors
- Try a BSD again, maybe usable enough to officially support
- SDL_HINT_GAMECONTROLLERCONFIG_FILE could be used to customise bindings
- Clipboard support for text entry, SDL has functions for that
  * Ctrl-C on its own copies the current mainbox content to clipboard
  * The transcript and debug console ought to allow click and hold text selection,
    with Ctrl-C and Ctrl-V (and Ctrl-Insert and Shift-Insert); also just click to
    select the pointed word only; clicking over the dropdown box is always swallowed
    in any case so main box won't advance
- Make debug console usable while paused
  * Console should appear or vanish instantly on ctrl-D while paused
  * Typed input should also go in, and scroll commands
  * Hitting enter on a command spawns the fiber and enters single-step mode
- Save transcript log in savestates, so when you load a game, you can check
  what happened recently
- Automatic furigana generation when using Japanese? Option for hiragana or romaji...
  * Maybe can dynamically link to mecab and a suitable dictionary
  * Freetype doesn't appear to have furigana support as such, so probably have to
    render and position the snippets ourselves
  * Furigana can be stored as escape codes in the string, or implicit generation at
    runtime which adds those escapes on print; horizontal positioning for long
    sequences covering more than one kanji may be annoying to escape-annotate
  * Default to 1/3 font height, added to line height; have a configurable to vary
    that between 1/4 and 1/2
- Reimplement precipitation effect as script code
- Eye candy
  * Pixelisation effect, dynamically slidable pixel size?
  * Imitate snowfall effect from Legend of Heroes 3 intro
  * Nice wind for sakura petals and snowfall
  * Sunlight effect; perhaps full starburst with loc/speed vars
  * Rain effect
  * Horizontal blur effect, doubled vision (use in sakura CS704)
- Dithered graphics mode for many-color games
  * Takes 256-color or true/fullcolor graphics, turns them to 16-color dithered
  * Conversion takes some effort so must be done between rip and build stages
  * Shell out to buncomp from sakutool
  * Each individual image can't be converted by itself, or they'll have unique
    sets of 16 colors, making the total exceed the limitation when more than
    one image shown at the same time; must build histogram from every game graphic
    and then build a palette based on that; then use the single shared palette to
    convert every graphic
  * Palette must use preserve contrast and favor flat colors; furthermore, must
    favor flats in sprites more than in backgrounds since character sprites tend
    to have flat skin and fabric tones
  * Use only checkerboard dithering for sprites? 4x4 for everything else
  * A sprite is any graphic that includes transparency
  * Any pixel with 50%+ transparency is considered fully transparent, anything less
    counts as fully opaque; transparent pixels don't count toward palette limit,
    so the histogram contains only opaque colors
  * Buncomp can be restricted to 4bpp output depth since that's all the PC98 could
    do, makes the conversion a lot faster
  * Throw in a 4-color CGA mode while at it, why not
  * Dithering could be done after the initial nx scaler, allows super-resolution
    dither which preserves more detail; but at high n, too fine resolution? Perhaps
    best would be a separate dither+scale nx step before normal nx scale
- AI-powered truecolor background generation tool? Train model on background images
  from rippable games; either modern truecolor or old 256-color ones that are
  stylistically still close to PC98 art; perhaps use reverse dithering on finest
  PC98 art (and touch up manually) and train with those too; convert them to proper
  dithered graphics using the dithered graphics mode described above
- Add graphic scaling at run-time in scriptcode? Scalesizex/y? A rescale
  cache used solely by the Renderer routine, so when a gob of unusual size
  is requested, it's rescaled and cached? Smooth size slides done this way
  might be pretty heavy...
- E-ink mode! Render to black and white with extra dithering, replace crossfade
  transition with a dissolve
- Viewports could have a move/size effect too, slides viewframe on/off
- Textboxes:
  * Box drop shadow
  * Improve bevel (maybe via dynamic gfx gen: "??black:bevel 0 128")
  * More bevelling parameters, to allow shaped bevels etc
  * Allow setting normal/light font hinting as a user preference
  * Fallback flat color should be autogenerated from gradient or texture
  * In addition to a primary text color, allow for a color gradient?
- Make fresh textboxes (box idea: a raised decoration on the left going
  higher than rest of box, leaving a snug corner/slot for a title box to
  fit in, title box can have diagonal far edge)
- Automated testing:
  * Save every button press and non-move mouse event incl. mouseovers
    in an internal buffer that needs to be dumped in a text file in case
    of abnormal program exit; add a commandline switch/debug option to
    play back virtual input from a file like this. The text file dump
    should also have remarks to clarify current script location (offset?
    label?) and the strings of choices selected.
  * Set up Vagrant? configs for the new VMs
- Sakurascript efficiency optimisation
  * Some of the subproc calls in mayclub caused a noticeable engine slowdown
  * Put some commands in loops of 10000 and see what's optimisable
- Perhaps an offline machine translation engine can be bundled, with a model trained
  on the targeted VN games; this would provide a legal free open source offline way
  to offer good translations with minimal user effort; the "moses" translator might
  work, or argos/libre translate, though any such will require extremely heavy setup
  and a potentially huge dictionary file to be distributed with the engine
  * This model could be given context for strings, something not readily possible
    with general-purpose online translators; for every string, include a pre and
    post context of the previous and next two strings from the same label, while
    output is still only for the section being translated; should provide a good
    translation quality bump, Japanese being a high-context language
- Extra content as optional mods!
  * Sad face for sakura/Hidemi-okaasan
  * Import the shrine background from FromH, insert as brief extra scene
    in runaway once or twice, to foreshadow and remind about the tree
  * Import the church at night from Parfait, insert in Saku/Seia's story
  * Runaway is the most linear of the classics; add choices for bypassing
    some H-scenes, resulting in mildly different epilogue segments; add
    an extra textual ending or two based on how Hiroaki comes to terms
    with his power?
  * 3sis has too much exposition at the beginning, streamline it
  * Runaway needs a clickable map when moving around downtown
  * 3sis needs a clickable map when moving around the school
  * Expanded soundtrack mod, subtle variations on existing themes?
  * Eye-blinkies for event graphics wherever appropriate
  * Add ambient soundscapes where appropriate
  * Insert other new special effects and sound effects as appropriate
- Improved game support:
  * Figure out the .O music format (have some docs)
  * Figure out the Elf .M music format
  * Figure out the .EMI music format
  * Figure out the .MLO music format
  * Figure out the .USO music format
  * Figure out and document the .PIC graphic format
  * Add a tilemap mode? for top-down jrpg adventures like Vanishing Point
  * Tasogare needs a wolf3d-style dungeon, and an automap
  * Hohoemi title extract, print into main
  * Hohoemi blackout should just remove bkg (unless in event?)
  * Nocturn98 ending sequence, need to see it in original first
- Check the broken Deep graphics in the original, just keep jumping ahead
  in scripts until find one or two; if they still look corrupt, write
  a brute force fixer to guess at what the corrupt bytes should be
- A new minitool for calculating diffs for all graphics of one game
  * Compare every graphic to every other graphic of the same dimensions and
    same bitmapformat; count the number of pixels that differ
  * Build a sorted list of all image relations, cutoff at maybe 25% difference
  * From the most similar image pair, traverse the list and print a delta
    string for each; can only use an image as a base if that image is not
    itself already delta'd against something else
  * User can pipe printed delta strings into a delta.inf
  * Recomp uses delta.inf to subtract the base image from the image being
    saved while saving it, and sets a dynamic loader string in the image's
    metadata; mcsassm uses the dynamic string to merge the images at loadtime
- Build a library of custom chip sound effects (use some from Alleycat for
  inspiration), use those in the games as appropriate
- Audio file mean square amplitude calculator, used on a small subset of audio
  samples at dat buildtime; determines approximate loudness level of the game's
  wave files, and can save an implicit volume multiplier in the dat that can
  be used by the engine to force a baseline loudness; the user can adjust that
  multiplicatively with the effects/voice volume sliders
- Oomph parameter for synth: carriers use waveforms with n% mix of expected
  wave and same but one octave below
- Check how well multiple displays and hi-DPI are working
- Check how well touch controls work if at all (SDL_HINT_MOUSE_TOUCH_EVENTS)
- Console port should use -p for pure text mode, stdout multiple choices only
- Commandline option to dump the game transcript into a file as you play
  * Eliminate unnecessary repetition in the transcript
- Specific global vars with known uses in games should be named
- Use v512 for endings in 3sis SK_737, SK_738 and SK_743
- Port 98ripper to Decomp so disk image files can be converted easily
- Multithreaded processing for sakutool extract
