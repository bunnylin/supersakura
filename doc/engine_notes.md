Scattered SuperSakura engine notes
----------------------------------

See also: renderer.md, uidesign.md


### Why the name SuperSakura?

The project originally started as a way to test a reverse-dithering algorithm idea
on scenes in Season of the Sakura. When the game resources were eventually figured out,
it seemed natural to make a new engine to run the game natively on modern systems,
with usability and cosmetic upgrades. Hence, a better way to run Sakura.

The name is a bit long to type, and carries a still unfortunate abbreviation, so
perhaps a name change would be in order. But to what? The engine could be described as
a Virtual Machine, much like ScummVM, so something with VM?

- SakuraVM is already used by another party
- SakuVM sounds too similar to ScummVM and SakuraVM, which is a trademark confusion risk
- BunnyVM would lose the "sakura" reference which feels culturally important to keep
- UsagiVM is too hard to pronounce
- Sakura Visual Novel System, or anything with VN, doesn't take into account this engine
  is not only for visual novels
- SuperSakuraSuite is too indulgent and too long anyway
- SakuraXL (excellence) incorrectly implies relation to DarkXL and DaggerXL
- SakuraXG, GS, XP, NT are all already used abbreviations elsewhere
- SakuraNG (nextgen) is actually an existing person's name
- SakuraGame, SakuGe, OpenSakura, FreeSakura all imply just a single game
- SakuraRE (runtime environment) might be reasonable...
- Sakuraesque, Sakuroid, Sakurate, Sakuboo, Sakubox, Sakuchan, Sakutan... this is getting silly

The project has gone by SuperSakura for over 10 years at this point, and none of the
listed alternatives seem clearly better. Best keep what little name recognition it has
and remain SuperSakura.


### Should TViewports be TElements?

Maybe. Graphic objects and textboxes are both displayable elements with parameters
relating to their screen position. Viewports aren't displayable in the same sense, but
they do have parameters relating to screen position.

Pros:
- Can share some codepaths with TElement, slightly reducing code size
- Can use any element-affecting script commands on viewports
- Can refer to viewports by name, more user-friendly to develop
- Element positioning relative to other elements can be used instead of special casing
  positioning relative to a viewport

Cons:
- Complicates the class hierarchy by adding a subclass, greater lateral dependency
- When would you actually want to use element-affecting script commands on a viewport?
  Mouseover events maybe, but those are easily handled with a full-port virtual gob
- Referring to viewports by name requires string matching when asking for a particular
  viewport, rather than instant reference by array index
- The current implementation has a lot of viewport references, which would all have to
  be switched to a new style, not a task to take on without meaningful gains

Possibility:
- If viewports are TElements, there's no reason not to retain an indexed array of
  viewport objects for fast access, which cancels the last 2 cons

This does seem to weigh in favor of the pros, so this could be implemented at some point.


### How to protect against build failures?

Git has a number of script hooks that can be triggered in response to various repo
actions. The script `.git/hooks/pre-push`:

```
cd /<path>/sakutest
if [[ $? -ne 0 ]]
then
exit 2
fi
git pull

trybuild()
{
fpc $1
result=$?
if [[ $result -ne 0 ]]
then
echo $1 failed with $result
exit 1
fi
}

tryrip()
{
./sakutool $1
result=$?
if [[ $result -ne 0 ]]
then
echo $1 failed with $result
exit 1
fi
}

trybuild "supersakura"
trybuild "supersakura-con"
trybuild "sakutool"
trybuild "decomp"
trybuild "recomp"
tryrip "/<path>/pix/test"
echo Go ahead!
exit 0
```

Whenever a `git push` is attempted, this goes over to a `sakutest` directory which has
been configured to pull directly from the local development directory. It gets the commits
currently being pushed, and builds all project executables. It also rips a bunch of test
images in supported formats to verify the graphics converter at least should be working.
If there's even a single error, it exits with a non-zero code, causing `git push` to bail out.

This has caught a build break a few times...

Some additional smoke tests would be a good idea, but the whole process should remain
fast, ideally well under a minute.


### Should FM music be pre-rendered and distributed as OGGs?

Most PC98 games use FM music, and may or may not have midi versions available.
Early Windows games all use midi, with poorly-looping uncompressed CD audio tracks
quickly becoming the norm. From about 2000 onward, games largely switched to somewhat
compressed pre-rendered wave files, some able to loop properly.

Since supporting CD audio tracks is clearly in scope for SuperSakura, it's somewhat
appealing to manually convert everything from the PC98 era into a similar format and
just use wave streaming across the board, like all modern games do. However, this is
not going to work.

Rendering an FM song into a wave track is non-trivial and requires using a suitable
FM player like Hoot, which isn't readily integratable into SuperSakura's tooling, so
music would have to be converted manually, and - this is the deal-breaker - the new
tracks would have to be distributed with SuperSakura, a clear copyright infringement.
It would also dramatically increase the engine download size, never desirable.

Furthermore, just like CD audio tracks, pre-rendered FM songs would struggle to loop
cleanly, unless prepared very carefully. Original FM tracks and midi have no problem
at all with looping.

The only possible way to support FM music out of the box is to include an FM synth
in SuperSakura. On the plus side, this enables applying a bit of subtle reverb and
equalisation to the already delicious FM sound as a runtime user option.


### Can SuperSakura use more hardware acceleration?

Using the current design, not really. The engine has been designed from the start
around software rendering, which has the following consequences:

- Pixel-perfect output buffer control in native code
- Pixel-perfect control over screen and sprite effects in native code
- The entire render pipeline except the final buffer push runs on the CPU
- The output buffer can be rendered as colorful ASCII for the console port

A proper hardware-rendered design would have these characteristics:

- As much blitting as possible of all graphic elements runs on the GPU
- To blit graphics on hardware, all graphics should be loaded into video memory before use
- Must avoid reading pixels from video memory, which is slow and discouraged
- Effects on graphic elements can't be done in normal code but should be shaders instead
- Perfect subpixel sprite positioning
- Basically free graphic element resizing, rotation, and other transforms
- More possibilities around 3D scene rendering for first-person-view dungeon games

Pushing pixels around in a GPU is typically more efficient than doing the same using
the CPU, so a hardware renderer usually saves energy. However, sprite-based visual
novel and JRPG games do very little pixel pushing to begin with. Furthermore, I have
optimised the software renderer quite well, so practical gains would be minimal.

Some of the functional benefits offered by hardware rendering are enticing, but that
has to be balanced against the maintainability loss of introducing shader programming
to an otherwise native-code project, not to mention the cost of writing a new renderer.

Finally, and most decisively, I've always found software rendering intriguing.
It requires a low-level hacker mindset, making the program design a challenging
nitty-gritty puzzle of intricate moving parts, which I like for some reason.
So, technically, more hardware acceleration is possible, but doesn't seem worth it to me.


### How to implement custom graphics clipping?

It's not a common use case, but a few games clip graphics against a context that is not
the full screen or the viewframe. For example, a graphic may be scrolling within
a reduced rectangle smaller than the viewframe, such as the intro/credits roll in Lime.
Another case is having a special effect or animation that is masked to only appear inside
an irregular foreground graphic, typically the game title logo.

For the latter, the only option is an alpha mask. Probably need to support 8-bit bitmaps
for that purpose. The mask image could be generated at conversion time, but in some cases
could also at runtime grab and invert the alpha channel of the containing graphic.

For the former, an alpha mask would work as well, but could be overkill, since clipping
against a rectangle is more efficient than per-pixel occlusion. Viewports already provide
clipping against arbitrary rectangles, but have the side effect of scaling any contained
graphic to their resolution, so viewports are not really meant to be custom clippers.
Perhaps a runtime dynamic element, which is always a rectangle, could use a shortcut
clipping path?

There's a gotcha here with the current renderer, since it determines which gob to draw
bottommost by finding the lowest opaque gob within a render rectangle. But if that gob
happens to be custom-clipped, it might not be fully in the render rectangle, so beware.

For the moment, only Silence games are seen to really make use of this, and a full-port
clip is tolerable for those. Eventually, advanced clipping will need to be implemented.
