Stringmatic
-----------

This is part of the mcsassm unit.

Complexities that need handling:
- Frontend is in English, games can come with English or Japanese script, or more than
  one language. Scripts with different canonical languages must coexist.
- Some short strings (eg. choice verbs) are often repeated and should be always
  presented consistently when translated.
- Must be able to switch active language at runtime, or even display two languages
  side by side for educational purposes.
- Must provide automatic translation into English as an opt-in feature. The option is
  engine-wide, can be toggled in settings, defaults to off.
- The autotranslator can be toggled at runtime, so must be able to run at runtime.
- Must provide an integrity check in case the script has changed and string IDs are
  different than before, to avoid showing the wrong translation for a given ID.
- Fiber execution is blocked deep in the stack when waiting for a translation, and
  there's no guarantee the external translator ever returns; blocks the main thread.
- Fiber execution is synchronous in the main loop with other components.
- The quit system event must be synchronous with script running since the confirm quit
  box does an immediate metastate change.
- Therefore fiber execution must run in the main thread but also must not block,
  at least not for very long.

##### String types

LL is a label's local language, stored separately for each label. When compiling a label
into bytecode, all encountered strings must be in the same language, as defined in the
most recently applied metadata inf, and that active language is stored in the label's header.

- Hardcoded: used for script logic, not display to user; saved as a single string
  directly in script bytecode. Not translated, not in string table. Always load as [LL].
- Global: used for display to user, but likely repeated a lot; saved as a single string
  directly in script bytecode and added to global table if not yet present. Always load
  as [LL], and fetch other available language indexes. Autotranslatable.
- Unique: used for display to user, likely unique; saved as a table index number in
  script bytecode and added to unique table. Always load as [LL], and fetch other
  available language indexes. Autotranslatable.
- Empty: as a special case, any empty string is always saved hardcoded. Not translated.

##### Global string table

A lookup key-value dictionary is the way to go. The canonical string is the key (string ID)
regardless of source language, and translated strings go in their respective columns.
The canonical string is not repeated in its language column.

The global table is sorted by key for fast binary searching. It is stored in label[0],
which is otherwise invalid or used for debug commands. Unlike unique string tables,
where the first column contains the first language, the global table's first column is
the key string, and translations are pushed up by +1 index in memory.

Global strings are never deleted from the string table on load. If a global string has
changed, that's just a new global string and the previous one and its translations
remain in place.

When a global string is encountered in bytecode, it is the full canonical string in the
label's local language. Request the complete bunch from Stringmatic, which looks up
this index in the global stringtable, provisions a translation for language 0 (English)
if empty, copies the canonical string to any other empty language, and returns the bunch.

##### Unique string tables

Each label has its own unique string table. Unsorted, strings are saved in the order
they are encountered in the source code with unique index numbers. Although some unique
strings may be duplicates, they have to be kept separate, since the English translation
may need to be different depending on context.

It's possible for the index numbers to change if the game script changes, for example
due to re-ripping the game with an improved sakutool. So as not to throw away the
entire string table, a string ID should combine the index within the label with a CRC
value. This CRC is minimal - XOR every byte in the string. The string ID in an
exported table becomes `script.label.index.crc`. If a string has changed, its CRC also
usually becomes different.

When a unique string is encountered in bytecode, it is specified solely as an index number;
since you know which script.label you're in, you can request the content of this
string.label.index from Stringmatic, which looks up this index in the unique stringtable,
provisions a translation for language 0 (English) if empty, copies the canonical string to
any other empty language, and returns the bunch. The CRC doesn't come into play at this stage.

##### Memory structure

- Store a database of strings
- Strings are seekable by script.label.index; global strings by text match binary search
- More than one language string linked together, easy to select which language
- Seek/read time is most important, but load time is also important, save time least

Having to reserve an array and multiple strings on heap for each canonical string is
quite a lot of overhead, but it may be the most practical solution.

Table structure: `scriptObjects[labelindex].stringTable[stringindex].txt[languageindex]`.
Although a CRC code is associated with every label.string, that is only used at load time
and not kept in memory here.

```
type TStringEntry = record
  txt : TStringBunch;
end;

type TScriptObject = class
  ...
  stringTable : array of TStringEntry;
  labelLanguage : byte;
end;
```

Any sakurascript string values being manipulated are always kept as a stringbunch of
languageList length, even if all strings in the bunch are empty. But in memory, any
stringTable may have fewer than languageList length entries, if the string was loaded
before an extra language appeared. The string fetcher will fill in missing columns.

##### TSV files

String tables are serialised into a TSV stream. They can be dumped for user perusal as
a plaintext file, and are easy to compress into a block in a DAT file. The first column
is for string IDs, and there's a further column for each language present in the table.
Empty cells signify a missing or untranslated string. Missing cells on the right side
of a row are treated as empty. Empty rows are ignored. Repeated global strings or
unique IDs overwrite the existing string with all non-empty cells. This allows merging
a translation via overlaying. The global string ID already covers the canonical language,
so the canonical column should be empty, but if it isn't, the new value is returned
instead of the canonical.

The language columns can be in any order, and different TSV files can have different
column order. On deserialisation, languages are matched to LanguageList indexes on
a first come basis.

First row: empty cell, then language name for each column.

Second row onward: global strings and unique strings. These may exist in any order,
but for efficiency, global strings are written first in byte-sorted order, then all
labels in undefined order but with each one's indexes in reverse order. This means
only one memory allocation is needed when deserialising.

The first column has the string ID. The global string ID is the actual string
itself, case-sensitive. A canonical global can't be empty. The unique string ID
takes the case-insensitive form `@script.label.index.crc`. Script name and label
name can both be empty. CRC is always two hexadecimal characters; index is
1+ decimal characters.

When loading string tables, the relevant labels must already exist in memory. When
importing a unique string, the canonical language must always be imported before
(or at the same time with) non-canonicals.
- Importing a canonical string or compiling a script: all non-canonicals get wiped.
- Importing non-canonicals at the same time as canonical: put them all in.
- Importing non-canonicals on their own without the canonical in the table: error.
- Importing non-canonicals after canonical in table: JIT CRC check, discard if error.
- Keep a local list of bools for each label: any CRC fail in label sets this.
  At end of import, wipe all non-canonicals from label on the assumption the CRC just
  didn't catch the rest.

Keep track of the total count of strings, and count of strings where language 0 is
missing. If this is more than 0, there are strings that still need translating.

##### DAT files

Each metadata inf file can contain a language value. The value in the most recently read
inf file becomes the new labelLanguage or LL for any scripts being compiled afterward.
As each label is compiled into bytecode, the bytecode header also stores the LL index.
Each unique language is added to LanguageList.

When the label is saved, the LL index is expanded to the full language string. When the
label is loaded from disk, its language string is again ensured to exist in LanguageList,
and LL becomes an index pointing to it.

When sakutool is building a DAT file, it first loads all loose files in the project.
Files types can be loaded in any order, so a string table may appear before the
relevant scripts. It is up to the project creator to ensure a directory layout that
doesn't cause strings to be loaded in incorrect order.

Before the engine loads a DAT file, it will try to load all its parent DAT files.
After a DAT file is loaded, the engine automatically tries to load a TSV file with the
same shortname, looking in the same directories where DATs are. The shortname must be
the whole basename, or may have a hyphen and more stuff. If multiple are present, it
loads the one that sorts first bytewise. After this, the engine tries to load
everything in the project's override directory, possibly including more TSV files or
new scripts containing additional or modified strings.

If a TSV file is next to the DAT instead of in the project directory, any canonical
language strings in it are ignored. It's convenient to keep the canonical strings in
the file to aid translation, but they may become out of sync and can't be allowed to
revert game strings to an earlier state. If you actually want to override canonical
game strings, the TSV must be under the project directory.

##### Voices and sound effects

Not stored in the string table. Rather, there's a separate sfx.play command with the
filename as hardcoded string, and a "sound" parameter at the end of the print command
which does the same thing.

##### Autotranslator

Any request for a global or unique string must go through a shared fetcher, namely
GetStringGlobal and GetStringUnique in stringmatic. If the autotranslator is off or
the bunch is already translated, the call returns immediately.

Otherwise it needs to lodge a priority translation request with the translator thread,
and put the game in a holding metastate until the translation is done. (Hit esc to
cancel the metastate and use whatever's available.) While waiting, the engine polls
once per frame if the translation has appeared.

The translator thread is the asset manager's background worker thread, shared with any
other async asset requests. Whenever new DATs need to be loaded, the worker thread
needs to shut down, and erase the job queue.

Whenever a set of DATs has been loaded completely, if the untranslated string count is
more than 0, the main thread kicks off a translation preprocessor job and moves on.

The autotranslator keeps track of total number of strings in string tables, and how
many strings remain untranslated for language 0. This is calculated during TSV import,
then each translator action modifies the value. SuperSakura can access these translated
and total counts to show to the user, as well as the translator status message.
The status message can show an ETA if actively translating, calculated from completion
at thread start up to now, knowing how long the thread's been running, and extrapolating
to 100%. The ETA should be in hours unless < 2h, then show minutes until < 2m, then show
seconds.

If any new string has been generated into the string tables from the translator,
an autosave flag is set. On the next save state or engine exit, dump the string table
in a TSV file beside the DAT. Only dump non-canonical (translated) unique strings,
but since the canonical string is the ID for global strings, dump those too. Use safe
save-by-copy to avoid accidents.

###### Internal preprocessor

The preprocessor job keeps a pointer to the current label and string index being scanned,
starting from label 0. When a string has been found with an empty language 0, the current
job preprocesses the source string, spawns a new translation job, and exits. The scanning
pointers are kept by the new job. When the new job completes, it spawns a new preprocessor
job with the pointers before exiting itself.

Preprocessing converts parts of canonical strings that don't need translating, like
punctuation at the ends of the string, and converting double-width latin characters
directly. This includes exclamation and question marks, which conversion doesn't appear
to harm common machine translations, if question marks are kept in the dispatched string.
Such conversions slightly reduce the load on the external translator, and for ubiquitous
"....." strings may even convert the whole string immediately.

There's special handling for dialogue titles. When part of a unique string, it is
extracted and looked up in the global string table, then saved in the result prefix.
If the global string table lacks this entry, a new entry is added. (Must add lock around
global table access, since inserting a new entry in a sorted table changes many indexes.)
If there's no translation in the global string table, then if the internal translator is
enabled, check for a matching snippet (less \:) and apply it. Or, if the external
translator is configured and enabled, execute its commandline to acquire the translation.
This dialogue title scanning is done as the first step.

The job keeps a source string, and a result prefix and suffix. If, after preprocessing,
the whole source string has been consumed, the result prefix is the entire result and no
further work is needed. The string is scanned first from the start for alternately
characters and snippets, then from the end for the same. The scan stops when no more
characters or snippets are recognised, leaving a middle section needing translation.

Recognisable characters:
- Ascii question mark when reading from right: leave in place and stop
- Other ascii characters, extract from source
- Shift-jis question mark when reading from right: replace with ascii in place and stop
- Shift-jis whitespace or punctuation: extract and convert to ascii
- Shift-jis double-width latin characters: replace all in a row with ascii in place and stop

Recognisable snippets are defined in metadata inf files, consisting primarily of choice
verbs and nouns to ensure consistency, character names, and simple grunts and common
expressions. Game-specific inf files might add special cases to this list.
Each defined replacement string should specify the source and target languages, to
ensure they are not attempted if the active language pair is unexpected.

Some of the replacements can be marked as common transliterations. This detects and
preserves honorifics and titles like oneechan, kouhai, or sensei, and expressions like
itadakimasu or kanpai. These are on by default, but can be disabled in Settings. These
replacements are done in the whole source string in place after the initial character
and snippet scans finish to ensure such strings are correct even inside Japanese text.

The snippet replacements are only attempted if the internal translator setting is enabled.
Character replacements are done regardless. The internal can be disabled using a config
file line, which defaults to auto, which enables it if the external is also enabled.

If there is still an untranslated section and the external translator is configured and
enabled, its commandline is executed.

###### External translator

The engine has a configurable shell command string that is used to invoke any external
commandline translation tool. Defaults to none. There's also a rate limiter for number
of bytes per 5 seconds and msec delay between sending requests. Default 4000 bytes,
1500 msec. Consuming more than allowed bytes causes a longer wait before next send,
calculated when a new translation request is being prepared.

The shelled tool must return only the translated string in STDOUT, and exit code 0.
Failure imposes a delay, 8 seconds for first, 1 minute for a second successive, then
10 minutes, then 64 minutes, then 200 minutes, then autotranslate is toggled off
with an error status message which is also shown in a message box.

Additionally, each failure temporarily bumps the msec delay between requests by 100ms
for the next 100 requests; cumulative bumps restart the 100 request counter. After
the counter reaches 0, reduce 100ms and count from 100 requests again.

If the shell string is empty, the engine will try to autodetect the presence of
a known translation tool, primarily Translate Shell. The autodetect is done at first
translation request after program launch, in the background thread. Translate Shell
detection tries "trans -V" and expects "Translate Shell<whitespace><version>" in the
output. If failed, autodetect is not attempted again and external autotranslation
remains unavailable. If successful, use `trans -b ja:en "<text>"` as shell string.
Double-quotes and backslashes will be escaped.

After translating a unique string and saving the result, the external translator scans
for duplicate uniques, before taking on the next request. Since machine translation
doesn't understand context, each duplicated string would always be translated the same
anyway, so may as well re-use the translation. Checking every string in the game would
be too much, but check every string in every label of the same script. Any identical
canonical string without a translation gets immediately filled in.

##### User interface

The settings dialog has a switch for Automatic translation enabled or disabled. This
controls the external translator. With the internal defaulting to auto, and the
shell string empty by default, enabling the external will hopefully provide a good
total experience.

Beside the switch, there's an edit field for the shell string, displaying (autodetect)
if empty. Beside that, there's a "Test translation" button, which produces a textbox
containing either the translator status if failed, or "Input: こんにちは、世界" and
"Result: <result>". Additionally, display the total strings in table vs translated
nearby.

The total vs translated strings count should also be discreetly displayed on the custom
title screen of each game, along with the autotranslator status message. This allows
the user to know how to long to wait before an uninterrupted experience is possible.

If the game engine must request an untranslated string for immediate translation, the
game must be put in a holding metastate meanwhile. Dim the game view, print "Translating"
in a centered message box.

There's no UI for requesting a retranslation, but this can be easily done by removing
the generated TSV file, or editing it to clear strings needing a new translation.

Sakutool has a trans command, with trans -t for test, trans shortname for status for
that DAT, trans -d shortname to dump the entire string table, and trans -x shortname
to try to translate everything untranslated. This option starts with the test string
first, so any error is obvious up front. It will keep printing the status message as
it goes, and exits when the autotranslator disables itself or runs out of things to do.
It should autodump the string table once every 200 strings.

There's an engine-wide default displayLanguage preference, defaulting to empty, which
is equal to original language. This can be changed in the ini file. On game start,
the callbacked variable _displayLanguage is connected to the sysvar of same name,
which is inited to the displayLanguage preference. Once in game, the variable is
validated by the main script and then only changes via script commands, primarily in
the metamenu, since a language change requires changing textbox characteristics,
which is under the script's purview.

The metamenu has the options "Original" and "English", with possibly a third, "Both",
beside them. Textbox contents can't readily change after something's printed in them,
so it's necessary to have two copies of each textbox, one set to each language, and
use print forwarding to add all game text to both. The inactive language boxes are
hidden, so no time is spent flowing or rendering them. Changing languages simply
changes which boxes are hidden, and may have to reactivate choicematic.

Showing both languages side by side is also possible, but does require repositioning
the boxes, and repositioning again when switching away.
