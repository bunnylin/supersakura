Tenshitachi no Hohoemi
----------------------

Intro sequence: 
Prepare to print white on black text, play T_15, call OPENING.

The opening script uses custom encoding:
- If next bytes are 81 77 or 82 B1, it's a title text in Shift-JIS, terminates with 00 00.
  Clear all graphics, print this text centered on the screen.
- If next byte is 0x80..0xE4, it's a Shift-JIS string, terminated with 00 00. Print it
  in a standard main textbox.
  * The string may be 01 bytes, which pause text output for about 3 seconds, then
    clear the textbox and continue printing stuff. Best translated as a waitkey.
- If next byte is 0x41..0x56, it's a graphic name, terminated with 00 00. Dissolve or
  square-fill it in. It can be a background or a sprite. If the sprite has an
  associated eyeblink animation, it is loaded implicitly.

On return from the OPENING script, play T_17 and fade everything to white, then fade to
black with the text gone. The title sequence plays:
- KT_S
- The top left two characters from OP_003, centered
- YT_S
- The top right two characters and first from bottom left from OP_003, centered
- MT_S
- The last three characters from OP_003, centered
- AT_S
- Full OP_003, with the middle stripe blacked out
- A ragged swipe transition uncovers the middle stripe
- Fade to black, fade in OP_000
- Wait a moment, then add OP_000_ in the top left corner and show the main menu options

Select New Game, and you can enter your name. The top left legend is "名前を入れて下さい",
"Please enter your name". The default name is "孝友", Takatomo.

The game starts in the script SYU_01.

The game loads its main interface, which is the viewframe NORMAL_F. This is a bit unusual
in that each background includes the top viewframe piece, which allows character sprites
to pop out, the top of their head overlapping the viewframe. Each background change then
redraws that top bar, covering the sprite fully. 448x316 at 96,0.

There's a separate larger viewframe for special events, EVENT_F: 496x328 at 72,8.

Other elements of the main interface:
- Top left corner says "なまえ" ("name") in red, and your chosen name under it in white
- Your four stats are along the left, the seven-segment digits taken from the graphic 123;
  the starting values are 100 power, 50 S.A., 50 teaching, 40 love chemistry
- The top right shows the current date and time; the time uses seven-segment digits in
  a 24-hour format; the date has two lines: Month Day, and Weekday, with the month and
  weekday names using frames directly from 123, while the day is built from rounded
  digits in the same graphic
- On the middle right is a list of four actions, highlighted on mouseover when available;
  these are used to select what to do when you arrive at a tutoring session; they are
  unavailable otherwise
- On the bottom right is a space for a tutoring animation, A_*A0 graphics.

The bytecode command 11-0B shows the girls' stats and the week's calendar. The stats block
for a girl is a wooden frame rectangle from 123, where their name is at the top, the stat
names are baked into the image, and their values are on the right.

The bytecode command 11-07-yy shows the stats only for girl yy, in a single frame rect
on the left side of the viewport, while the girl remains in the middle.

The girls are:
- 彩 Aya
- 遊 Yuu
- めぐみ Megumi
- 琴乃 Kotono

Their stats are:
- 理解力 Comprehension
- 読解力 Reading ability
- 信頼度 Trust (or reliability?)
- ストレス Stress
- There's also an extra hidden stat, probably love points

Global variables:
- 6C, 72, 78, 7E, 85, 86, 87, 88, 96: start at 0
- D2: current day, 1 = Oct 2nd, a Monday
- DA..E5: these have the week's schedule, set randomly when 11-0A is called;
  each girl is met three times during the week, and no more than once per day;
  the value is 0-3 for tutoring one of the girls, or 4 for a special date or empty!?
  * DA, DB, DC: Monday slots: early evening, late evening, the night is still young
  * DD, DE, DF: Tuesday slots
  * E0, E1, E2: Thursday slots
  * E3, E4, E5: Friday slots
- E6: your power
- E7: your S.A.
- E8: your teaching ability
- E9: your love chemistry
- EA: Aya's comprehension
- EB: Aya's reading
- EC: Aya's trust
- ED: Aya's stress
- EE: Aya's love points?
- EF: Yuu's comprehension
- F0: Yuu's reading
- F1: Yuu's trust
- F2: Yuu's stress
- F3: Yuu's love points?
- F4: Megumi's comprehension
- F5: Megumi's reading
- F6: Megumi's trust
- F7: Megumi's stress
- F8: Megumi's love points?
- F9: Kotono's comprehension
- FA: Kotono's reading
- FB: Kotono's trust
- FC: Kotono's stress
- FD: Kotono's love points?
- FE: current time of day in 10-minute increments; 1 = 00:10, 102 = 17:00, 104 = 17:20
