MUAP98/NAX O files
------------------

These are played by NAX.COM, and the macro language used is apparently
MUAP98, by Packen Software; @PackenSoftware on xitter. This is supposed to
have been roughly about as capable as PMD and was used around the same time.

The official Japanese site used to be at: http://homepage3.nifty.com/y_ohta/packen/emu/

Possibly now: http://ohta.music.coocan.jp/packen/emu/index.htm

Among other things, there is a PDF manual of some sort, and assembler sources
for a MUAP98 player. I have transcribed the manual to a markdown format, in Japanese:
https://mooncore.eu/bunny/txt/muap98doc.txt

This was notably used in most PC98 games published by JAST, but various other
companies used this format as well. There are a few dozen games in the Hoot collection
that have .O files.

The files start with 17 word addresses pointing to tracks. Track offsets can be
expected to be listed in strictly ascending order. There are typically some zeroes
and possible garbage (definitely not always valid offsets) after the end of the array
and the start of the first track. The first track often starts with a DB FF identifying
string, followed by music data as on other tracks.

The tracks evidently are directed to different output modes:
- 0, 1, 2, 6, 7, 8, 11..16 are FM-OPN
- 3..5 are SSG (Software-controlled Sound Generator)
- 9, 10 are for rhythm and Pulse-Code Modulation (wave output)

### List of commands

`00..3F xx`

Note

`CE`

"set last tone, volume, pan"

`CF xx`

Send channel change

`D0 xx`

"SSG/PCM mode"

`D1`

Fade out

`D2`

"Play stack +1"

`D3 xx yy zz aa`

"@if exit"

`D4 xx yy zz aa`

"PCM address set"

`D5 xx yy`

SSG: start vol/attack rate, Rhythm: PCM play

`D6 xx`

volume sub

`D7 xx`

volume add

`D8 xx`

"LFO start(pmd,amd)/stop"

`D9 + 6 bytes`

LFO parameter set

`DA xx yy zz`

"X value set"

`DB FF xx yy`

followed by a text string of length yy

`DC xx yy zz`

"init skip_data"

`DD xx`

set comment length

`DE xx`

ratio only change

`DF`

FM, SSG: "Slur", Rhythm: PCM Repeat

`E0`

Loop counter clear

`E1`

"Tie"

`E2 xx`

volume data change

`E3 xx yy zz aa`

"@if call"

`E4 xx yy zz aa`

"@if jump"

`E5`

"play stack initialize"

`E6 + 26 bytes`

"set USR tone parameter"

`E7 xx yy`

source line symbolic information

`E8`

"@ret"

`E9 xx yy`

"@call"

`EA xx yy`

"@jump"

`EB xx`

FM: tone number change, SSG: mixer mode chg, Rhythm: PCM tone chg

`EC xx`

"key display mask on/off & color"

`ED xx`

FM: "3-channel 4-harm mode", SSG: "envelope type"

`EE xx yy`

FM: "hard LFO AMD,PMD,AMon", SSG: "env speed", Rhythm: pan/volume

`EF xx`

FM: "hard LFO speed", SSG: "envelope mode set, reset", Rhythm: "dump"

`F0 xx`

FM, SSG: Global detuning by xx?, Rhythm: Rhythm key on

`F1 xx yy`

"register data set"

`F2 xx yy zz`

SSG: "start decay data set", Rhythm: "DSP mode, level, delay"

`F3`

"wait all channel"

`F4 xx yy`

length/ratio change

`F5 xx yy`

timer-A tempo, probably global

`F6 xx`

FM: set panning to xx, SSG: noise freq change, Rhythm: PCM pan set

`F7 xx yy zz`

loop number of times

`F8 xx yy zz`

add frequency

`F9`

FM, SSG: same frequency play, Rhythm: "rhythm command end"

`FA + 8 bytes`

"3-channel 4-harm play"

`FB`

"wait on '"

`FC`

End of track? "this channel skip_play"

`FD`

End (break)

`FE`

End (loop)

`FF`

"Rest music"

START1.O

track0:
F5 F4 01 - timer-A tempo = 1F4
EB C8 - FM tone number change to C8
F4 0C 01 - lengthratio 0C/01
DE 00 - ratio only change 00
E2 69 - volume data change 69
F4 06 00 1B 0B - lengthratio 06/00, note 1B
F4 0C 00 1B 9E - lengthratio 0C/00, note 1B
E2 64 - volume data change 64
F4 60 00 22 B6 - lengthratio 60/00, note 22

track1:
F4 04 00 FF - lengthratio 04/00, rest
EB D3 - FM tone D3
F4 0C 01 - lengthratio 0C/01
E2 64 - volume 64
F4 06 00 23 0B - lengthratio 06/00, note 23
F4 0C 01 23 9E - lengthratio 0C/01, note 23
E2 5F - volume 5F
F4 60 0C 2A B6 - lengthratio 60/0C, note 2A

track2:
F4 08 01 FF - lengthratio 08/01, rest
EB D5 - FM tone D5
F4 0C 01 - lengthratio 0C/01
E2 69 - volume 69
F4 06 00 1B 0B - lengthratio 06/00, note 1B
F4 0C 01 1B 9E - lengthratio 0C/01, note 1B
E2 64 - volume 64
F4 60 0C 22 B6 - lengthratio 60/0C, note 22

START2.O
track0:
F5 C1 03 - tempo 3C1
EB 9A - FM tone 9A
F4 30 06
DE 00 E2 6E - ratio only 00, volume 6E
F4 90 00 DF 1B 39 - lengthratio 90/00, slur, note 1B
F4 0C 00 E1 1B 39 - lengthratio 0C/00, tie, note 1B
F4 30 00 DF 22 6A - lengthratio 30/00, slur, note 22
F4 C0 00 E1 22 6A - lengthratio C0/00, tie, note 22

track1:
F4 02 00 FF
F5 08 02 - tempo 208
EB 9A - FM tone 9A
F4 30 06
DE 00 E2 6E - ratio only 00, volume 6E
F4 90 00 DF 23 0B - lengthratio 90/00, slur, note 23
F4 0C 00 E1 23 0B - lengthratio 0C/00, tie, note 23
F4 30 00 DF 23 0B - lengthratio 30/00, slur, note 23
F4 C0 00 E1 23 0B - lengthratio C0/00, tie, note 23

track2:
F4 01 00 FF
F5 08 02 - tempo 208
EB 9A - FM tone 9A
F4 30 06
DE 00 E2 6E
F4 9C 00 FF - lengthratio 9C/00, rest
F4 30 00 DF 23 9E - lengthratio 30/00, slur, note 23
F4 C0 00 E1 23 9E - lengthratio C0/00, tie, note 23
FD - break
