FMP music
---------

The FMP music format is perhaps best remembered for the FMP music disks, collections
of apparently fan-made etc songs, mostly covers of video game music, originally just
distributed at comiket.

The original format was developed by Guu-san, @GuuFMP on xitter, who has a site
in Japanese only at: http://guu.fmp.jp/

FMP music files can use the `OPI` or `OVI` suffix, for OPN or OPNA compatibility
respectively. `OZI` might also be possible, indicating PPZ8 usage.
Playable by FMPMD2000, also 98fmplayer. Guu-san has made available a closed-source
Windows-only FMP7 player and FMC7 compiler, but I haven't tried it since it's not open.

Open-source-friendly implementation details by takamichih:
https://github.com/myon98/98fmplayer/tree/master/fmdriver

The FMP music macro language (MML) is officially documented:
http://fmpdoc.fmp.jp/

File structure, maybe: (words are x86-native little-endian)
```
uint16 - some kinda offset within the file, should point to "FMC" and a version byte?

for version <= 0x29:
	uint16[3] - FM tracks 1-3 offsets
	uint16[3] - SSG tracks 1-3 offsets
	uint16[3] - FM tracks 1-3 loop offsets
	uint16[3] - SSG trakcs 1-3 loop offsets
	byte - "bar"?
	byte - flags?
	byte[] - FM instrument definitions

or, version <= 0x49:
	uint16[6] - FM tracks 1-6 offsets
	uint16[3] - SSG tracks 1-3 offsets
	uint16 - rhythm track offset
	uint16 - ADPCM track offset
	uint16[6] - FM tracks 1-6 loop offsets
	uint16[3] - SSG tracks 1-3 loop offsets
	uint16 - rhythm track loop offset
	uint16 - ADPCM track loop offset
	byte - "bar"?
	byte - flags?
	uint16 - ADPCM data offset
	byte[] - FM instrument definitions

or, version <= 0x69:
	uint16[6] - FM tracks 1-6 offsets
	uint16[3] - SSG tracks 1-3 offsets
	uint16 - rhythm track offset
	uint16 - ADPCM track offset
	uint16[3] - FM extended tracks 1-3 offsets
	uint16[6] - FM tracks 1-6 loop offsets
	uint16[3] - SSG tracks 1-3 loop offsets
	uint16 - rhythm track loop offset
	uint16 - ADPCM track loop offset
	uint16[3] - FM extended tracks 1-3 offsets
	byte - "bar"?
	byte - flags?
	uint16 - ADPCM data offset
	byte[] - FM instrument definitions
```

Alternatively, if the signature is "ELF" instead of "FMC", the header is the same
as version <= 0x49 except with the ADPCM offsets omitted. However, an invalid
ADPCM data offset may still exist right before the FM instrument definitions?

The available tracks are, as expected:
- 3 FM tracks
- 3 extended FM tracks
- 3 more FM tracks (4-6)
- 3 SSG tracks
- One ADPCM track
- 8 PPZ tracks

FM instrument definitions contain:
- Tone number 0-255
- Attack rate 0-31
- Decay rate 0-31
- Sustain rate 0-31
- Release rate 0-15
- Sustain level 0-15
- Total level 0-127
- Keyscale 0-3
- Multiplier 0-15
- Detune 0-7
- AMS enable 0-1, not necessarily present
- "Connection" 0-7
- Feedback 0-7

Commands:

`62` set tempo

`63` mix or volume "d" for FM/SSG/PPZ8

`64` loop

`65` loop end

`66` tie

`67` q? Something to do with "gates"

`68` pitch bend

`69` set FM/SSG/ADPCM/PPZ8 volume

`6A` decrease FM/SSG/ADPCM volume

`6B` increase FM/SSG/ADPCM volume

`6C` key-on delay

`6D` detune

`6E` poke

`6F` sync

`70` wait

`71` envreset SSG, or set FM or ADPCM tone (instrument?)

`72` deflen

`73` set relative volume for FM or ADPCM, or set noise for SSG

`74` loop

`75` LFO

`76` LFO_P

`77` LFO_Q

`78` LFO_R

`79` set SSG envelope, or FM LFO A

`7A` tie

`7B` transpose

`7C` pan FM, or set SSG instrument, or pan ADPCM?
	start volume, attack rate, decay rate, sustain level, sustain rate, release rate

`7D` sync

`7E` loop or detune?

`7F` LFO delay

A rhythm track possibly uses different command numbers.
