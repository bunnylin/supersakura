MLO music files
---------------

These were mostly used in various Himeya games. There's no obvious player executable,
so the player must be hardcoded.

Header:
```
uint16 - always 0001? version number?
uint16 - offset within file, points to last part of file; instrument definitions?
uint16 - same offset minus 4
```

Track data begins immediately after the header. Each track starts with a uint16
giving the track length in bytes, counted from the start of the length value itself.
A length of 0000 ends the track array, and should line up with the instrument
definition start offset found in the header.

Track commands:

`0A`

unknown

`10..7F`

The range might be wrong. Trigger a note using the current duration.

`B1 xx`

unknown, but xx seems like a note-like length

`B9`

Probably a tie/slur between notes.

`C0 xx`

unknown

`D0 xx`

unknown, maybe a rest? set volume?

`D2 xx`

unknown

`D3 xx`

unknown, often directly before D0

`D4 xxxx`

unknown, could be a jump or loop to the given offset, relative to track's uint16 length

`D5 xxxx`

unknown, could be a jump or loop to the given offset, relative to track's uint16 length

`D6 xxxx`

Probably the main track loop, jump back to absolute offset xxxx.

`D7`

End of track.

`D8 xx`

unknown, often directly before E5; xx is always 0F or less?

`DD aa bb cc dd ee ff`

Unknown, volume envelope adjust or LFO setup?

`DF aa bb cc dd`

Unknown, volume envelope or LFO setup?

`E0 xx`

unknown

`E2 aa bb cc dd ee ff`

Change SSG parameters? Volume envelope, LFO setup?

`E3 xx`

unknown

`E4 xx`

unknown

`E5 xx`

unknown

`E9`

unknown


### YOK18.MLO, the catchiest Kinketsu song

Track 0:
D0 18
E0 B6, D8 0D, E5 06, D2 00, E9
D0 18, D3 04, D0 18
A2 48 AA A2
60 62 48 6A 62 60
D4 0D 00
52 48 5A 52 60 62 48 6A 62 60
D3 02, D0 18, D3 02, D0 18
02 48 0A 02 60
D5 0A 00
71 48 79 71 60
D4 10 00
51 48 59 59 51 30 59
D4 1E 00
D3 02, D0 18
A1 48 A9 A1
60 61 48 69 61 60
D4 0D 00
D3 02, D0 18, D3 02, D0 18
02 48 0A 02 60
D5 0A 00
81 48 89 81 60
D4 10 00
81 48 89 89 81 30 89
D4 1E 00
D6 74 00
D7

Track 1:
D0 18
E5 06, E9, D0 18, D8 0D, D2 01, D3 04, D0 18, D3 02, D0 18
AC 5C 1D 5C
D4 07 00
D3 02, D0 18
AC 6C 1D 6C
D4 07 00
D4 19 00
D3 02, D0 18, AB 5B 1C 5B
D4 07 00
D3 02, D0 18, AB 6B 1C 6B
D4 07 00
D8 0C, D2 02, D3 02, D0 18
01 C0 70 C0 01 C0 50 C0
D4 0B 00
D8 0D, D2 03, D3 02, D0 18, D3 02, D0 18, AC 5C 1D 5C, D4 07 00, D3 02, D0 18, AC 6C 1D 6C
D4 07 00
D4 19 00
D2 01 D3 04 D0 18 D3 02 D0 18 0C 7B 3C 7B D4 07 00 D3 02 D0 18 8B 3B 0C 3B D4 07 00 D4 19 00 D6 8E 00 D7

Track 2:
13 01
D0 18, D8 0F, E9, D0 18, D3 02, D0 18
DD 0E 01 07 00 08 00
D3 02, D0 18, D2 04, E5 06
DF AC 0D 1D 3D D2 05
E5 02
DE 55 30 A5 30 95 30 A5 30
D5 09 00
65 30 35 30
D4 20 00
DD 20 01 09 00 08 00, 65 60, D4 37 00, E5 06, D8 09, D2 01, C0 24, D3 02, D0 18, AB 5B 1C 5B
D4 07 00
D3 02, D0 18, AB 6B, D5 07 00, 1C 6B, D4 0A 00, 14 0C, D8 0F, D2 04
DD 0E 01 07 00 08 00, D3 02, D0 18, D3 02, D0 18, E5 06
DF 0D 2D 3D 5D, E5 04
DE 75 30 A5 30 95 30, D5 0B 00, 55 30 7D 5D 35 30, D4 1C 00, A5 30 9D 7D 5D 9D, D4 29 00
DD 0E 01 07 00 08 00, D3 02, D0 18, D2 04, E5 06
DF AC 0D 1D 3D, D2 05, E5 02
DE 55 30 A5 30, D5 0D 00, 95 30 A5 30 65 30 45 30, D4 20 00, 06 30 A5 30
DD 20 01 09 00 08 00, 95 60
DD 0E 01 07 00 08 00, D3 02, D0 18, D3 02, D0 18, D2 04, E5 06
DF 0D 2D 3D 5D, D2 05, E5 02
DE 75 30 06 30, D5 0D 00, B5 30 06 30 8D 7D 65 30, D4 20 00, 26 30 06 30 B5 60
D4 2D 00
D6 0A 01
D7

Track 3:
D0 18, D8 0D, E5 03, E9, D0 18
E2 FF FF 0B C8 01 05, E3 01, D3 02, D0 18
DD 0E 01 10 00 08 00, D3 02 D0 18 DF AB 0C 1C 3C DE 54 2A C0 06 A4 2A C0 06 94 2A C0 06 A4 2A C0 06 D5 0D 00 64 2A C0 06 34 2A C0 06 D4 24 00 DD 20 01 16 00 08 00 64 60 D4 3B 00 C0 C0 C0 C0 DD 0E 01 10 00 08 00 D3 02 D0 18 D3 02 D0 18 E5 06 DF 0C 2C 3C 5C DE 74 30 A4 30 9C C8 D5 0B 00 54 30 7C 5C 34 30 D4 1A 00 A4 30 9C 7C 5C 9C D4 27 00 DD 0E 01 10 00 08 00 D3 02 D0 18 DF AB 0C 1C 3C DE 54 30 A4 30 D5 0D 00 94 30 A4 30 64 30 44 30 D4 18 00 05 30 A4 30 DD 20 01 16 00 08 00 64 60 E2 C3 C3 FF E1 01 0A E3 01 D3 02 D0 18 D3 02 D0 18 DF DD 0E 01 10 00 08 00 0C 2C 3C 5C DE 74 30 05 30 D5 0D 00 B4 30 05 30 8C 7C 64 30 D4 1F 00 25 30 05 30 DD 20 01 16 00 08 00 B4 60 D4 33 00 D6 F2 00 D7

Track 4:
D0 18
D8 09 E5 03 C0 24 E9 D0 18 E2 FF FF 0B C8 01 05 E3 01 D3 02 D0 18 DD 0E 01 10 00 08 00 D3 02 D0 18 DF AB 0C 1C 3C DE 54 2A C0 06 A4 2A C0 06 94 2A C0 06 A4 2A C0 06 D5 0D 00 64 2A C0 06 34 2A C0 06 D4 24 00 DD 20 01 16 00 08 00 64 60 D4 3B 00 C0 C0 C0 C0 DD 0E 01 10 00 08 00 D3 02 D0 18 D3 02 D0 18 E5 06 DF 0C 2C 3C 5C DE 74 30 A4 30 9C C8 D5 0B 00 54 30 7C 5C 34 30 D4 1A 00 A4 30 9C 7C 5C 9C D4 27 00 DD 0E 01 10 00 08 00 D3 02 D0 18 DF AB 0C 1C 3C DE 54 30 A4 30 D5 0D 00 94 30 A4 30 64 30 44 30 D4 18 00 05 30 A4 30 DD 20 01 16 00 08 00 94 60 E2 C3 C3 FF E1 01 0A E3 01 D3 02 D0 18 D3 02 D0 18 DF DD 0E 01 10 00 08 00 0C 2C 3C 5C DE 74 30 05 30 D5 0D 00 B4 30 05 30 8C 7C 64 30 D4 1F 00 25 30 05 30 DD 20 01 16 00 08 00 B4 60 D4 33 00 D6 F2 00 D7

Track 5:
E0 D9
D0 18
D8 0B, E9, D0 18, D3 02, D0 18, D3 02, D0 18, D3 03, D0 18
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B, 0B
E2 FF B9 0F A0 E1 BE, E3 02, E4 0A, 0B
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
D4 28 00
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
E2 FF AF 06 87 FF BE, E3 02, E4 04, 0B
E2 FF B9 0F A0 E1 BE, E3 02, E4 0A, 0B
D5 11 00
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
D4 62 00, 0B
D4 6A 00
D3 03, D0 18
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B, 0B
E2 FF B9 0F A0 E1 BE, E3 02, E4 0A, 0B
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
D4 28 00
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
E2 FF B9 0F A0 E1 BE, E3 02, E4 0A, 0B, 0B, 0B, D3 02, D0 18, D3 02, D0 18, D3 03, D0 18
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B, 0B
E2 FF B9 0F A0 E1 BE, E3 02, E4 0A, 0B
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
D4 28 00
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
E2 FF AF 06 87 FF BE, E3 02, E4 04, 0B
E2 FF B9 0F A0 E1 BE, E3 02, E4 0A, 0B
D5 11 00
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
D4 62 00, 0B, D4 6A 00, D3 07, D0 18
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B, 0B
E2 FF B9 0F A0 E1 BE, E3 02, E4 0A, 0B
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
D4 28 00
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
E2 FF AF 06 87 FF BE, E3 02, E4 04, 0B
E2 FF B9 0F A0 E1 BE, E3 02, E4 0A, 0B, 0B
D3 02, D0 18, D3 02, D0 18, D3 03, D0 18
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B, 0B
E2 FF B9 0F A0 E1 BE, E3 02, E4 0A, 0B
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
D4 28 00
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B, 0B
E2 FF B9 0F A0 E1 BE, E3 02, E4 0A, 0B
D5 11 00
E2 FF FF 22 02 FF 23, E3 02, E4 00, 0B
D4 57 00, 0B, D4 5F 00
D6 DA 01
D7
