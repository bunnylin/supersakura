MUF music files
---------------

FM music files used in Aaru games. This format supports the standard 3 FM and 3 SSG channels
of the YM2203, and the 3 extra channels on YM2608 (the "9801-86" version). The extra
channels may go unused.

There's a `snd.dat` file which may contain the FM/SSG instrument definitions.
The first 256 instruments are each 36 bytes. Then another 256 instruments of 10 bytes each.
In each FM instrument, bytes 4-19 are probably the four operators' ADSR values, which in PMD
have a 4-5 bit range. The last 8 bytes seem to be all 0, perhaps not used, leaving 28 bytes
of meaningful parameters for each FM instrument.

Header: (words are x86-native little-endian)
```
uint16[9] - an array of track start offsets, the first is always 0x0012
```

Track commands:

`82 xx` - unknown, can be more than once per track; not volume; instrument select?

`85 aa bb cc dd` - lot of these on all tracks, must be related to noteons; cc must be length,
        since frequent values are 0C, 18, 30, and 60; aa and bb could be note and velocity,
        but in which order? dd then may be delay before next note?

`8A xx` - always at start of first track? tempo?

`9B 00` - just before each end of track, loop end?

`9C` - unknown, only once per track, loop start?

`9D xx` - unknown, only once per track, not on all tracks; stereo panning?..

`FE` - end of track
