HMD files
---------

These are FM music files, played with Music Driver HHD by Hideki Higuchi.
These were used in three games published by Jast (Tenkousei, Setsujuu,
Vanishing Point), and supposedly in a couple other games including Metal & Lace 2.
This driver apparently can't play files from the commandline, only via interrupts.

There may be a midi player too by the same author, Music Driver HMM.

There is no documentation for this music format I can find, so have to reverse
engineer it the hard way.

Starts with [00 07 C0 00 00] followed by 12 word addresses. The first one
points to a metadata track. Some may be zeroes, in which case the track does
not exist.

List of commands:
FFFF - track end marker

00..7B - notes, followed by duration byte
0F xx - pause of xx ticks
F2 xx - unknown
F4 xx - unknown
F9 xx yy zz aa - xx yy aa always 18 05 01?
FA - unknown
FB - unknown
FC xx - Volume adjustment by shortint(xx)?
FD xx yy zz aa - Loop, xx or yy times (always same value?), jump backwards
                 aazz bytes from the next byte after end of this command.

[F0 E0]
[FE 1E 00]
[FE 1F 00]
[FF 06 01]
[FF 09 02]
[FF 09 08]
[FF 0A]
[FF 0D 47 C0 49 C0 60]
