Non-standard MIDI file types
----------------------------

The standard MIDI file (SMF) didn't really become well-established until Windows with
built-in MIDI file support became the norm in operating systems.
Until then, DOS and PC-98 games tended to use custom MIDI file formats, or had only
FM music support. This document describes how such custom formats map to standard MIDI.

Source code for converting these is in [/inc/aud/midi.pas](../../inc/aud/midi.pas).


### Recomposer MCP, RCP, SC5, R36, G36

For further notes, see [Shingo45endo's RCP converter](https://shingo45endo.github.io/rcm2smf).
[Timidity++](http://timidity.sourceforge.net/) may be able to play these as well.
There's also [STed](http://sted2.sourceforge.net/).

There are a few different versions of this format, determined by the header signature.

- "M1<null><null>" or "MC<null><null>" for version 1
- "RCM-PC98V2.0(C)COME ON MUSIC"
- "COME ON MUSIC RECOMPOSER RCP3.0"

Additionally, an extra control file (MTD, CM6, GSD) may be present for initialising the
sound system. If applicable, you could send it as SysEx to your sound module.


##### RCP v1

Haven't found any yet... these may be designed for the MT-32 specifically.


##### RCP v2

```
Header:
000 - char[32] - signature
020 - char[64] - song name
060 - 12 lines of char[28] - memo text
1B0 - char[16] - reserved or unused?
1C0 - byte - time base low byte (ticks per quarter note, normally 48)
	1E7 - byte - time base high byte
1C1 - byte - tempo
1C2 - byte - time signature, numerator
1C3 - byte - time signature, denominator
1C4 - byte - key signature
1C5 - sint8 - global key transpose or "play bias"
1C6 - char[12] - CM6 control file name, if any
1D2 - char[4] - reserved or unused
1D6 - char[12] - GSD control file name, if any
1E2 - char[4] - reserved or unused
1E6 - byte - number of tracks (use 18 if this value is 0 or otherwise weird)
...
206 - 32 times byte[16] - instrument names and data?
406 - 8 times byte[48] - SysEx data? Each is 24 chars description + 24 bytes data
586 - track data begins here
```

The tempo value here is one byte. Midi files need microseconds per beat, which is
calculated as 60 million divided by this tempo. That value can be directly output as
0xFF midi meta-event 0x51, followed by the calculated value in three bytes.

Each track begins with its own header:

```
00 - track size : uint16
02 - track number : byte
03 - rhythm flag : byte
04 - midi channel : byte
05 - track key adjustment : sint8
06 - track time adjustment, all events are shifted by this many ticks : sint8
07 - mode, uncertain what this does : byte
08 - track name or "memo" string : 36 chars, padded with nulls or spaces
0x2C - event data begins here
```

All events on a track use the given midi channel. There are of course only 16 midi channels,
so the upper nibble is actually the midi port to use, almost always 0, sometimes 1. If the
channel value is 0x80 or 0xFF, the track is probably unused and empty.

Key adjustment is added directly to every note on the track, transposing them. Both
global and track key adjustment apply together. However, if track key adjustment has the
high bit set, ignore it? The adjustment value is effectively 7-bit signed...

Event data is saved as byte quartets. Even if a particular event doesn't use all four bytes,
four bytes are always read anyway. The first byte is the event type, and the meaning of the
other bytes depends on the first. The second is the number of ticks to wait after this
event (but only if event is < 0xF0); this is directly usable as a midi delta time.

```
00..7F - Note on, using the event byte as the midi note, second byte as delay after
	the note, third byte as the duration (insert an implicit noteoff this many ticks
	hence; if the same note is retriggered meanwhile, probably overwrite the pending
	duration and only send one note off anyway), and the fourth byte as velocity.
	If duration or velocity is 0, this is actually a rest, not a note on.
90..97 - User sysex message index, maybe ignorable.
98 - Track sysex message. Followed by a series of 0xF7 events, whose third and fourth
	bytes need to be concatenated to build the complete sysex data. The sysex data
	is finished when the next event is not 0xF7.
C0..DF - Manufacturer-specific commands, maybe ignorable.
E2 - Instrument and bank change. Use the fourth byte as the new bank, the third byte as
	the new midi instrument.
E6 - Channel change. Use the third byte as the new channel (1-16). If 0, mute track.
E7 - Tempo change. New midi tempo = `60 million / (third byte * header.tempo / 64)`.
EA - Channel aftertouch. Send the third byte as a new channel aftertouch value.
EB - Controller change. The third byte is the midi controller number, the fourth byte
	its new value.
EC - Instrument change. The third byte is the new midi instrument number.
ED - Note aftertouch (polyphonic). The third byte should be the note number, the fourth
	byte the new aftertouch value. However, many of these appear to duplicate the
	aftertouch value in both bytes, which suggests either a recomposer bug or that
	the command should be treated as a channel aftertouch after all...
EE - Pitch bend. New channel bend value = (3rd byte + (4th byte << 7)).
F6 - Comment. Concatenate the two data bytes of all F7 events following this in sequence,
	and output the result as a general text midi event.
F8 - Loop end, see below.
F9 - Loop begin, see below.
FC - "Same measure", a goto command or compression mechanism, see below.
FD - "Measure end", see below.
FE - End of track, immediately, don't even read the other three bytes.
```

The loop commands allow marking a section of song that should be repeated on that track
only, a certain number of times. F9 loop begin's event data bytes don't have useful
values, and F8 loop end's third and fourth event data bytes are also ignored.
The second data byte of an F8 loop end is the target loop count, or number of times
you're supposed to loop back here. If the target loop count is 0 or 0xFE or 0xFF,
it's an infinite loop.

Loops can be nested inside each other, up to perhaps 16 levels. A loop can be visited
more than once if it's nested, and needs to repeat the expected number of times again.
An F8 loop end without an F9 loop begin does nothing.

Thanks to this looping feature, tracks run effectively asynchronously. You'll need to
allow for this in any reader implementation. For a midi file conversion, it's often
necessary to unroll infinite loops a few times until all tracks have reached either
track end or an infinite loop.

The "Same measure" feature also repeats a part of the song, but rather than being coded
as a loop, it's a goto and a return, used solely to compress the music data. When an
FC same measure command is encountered, the new read offset = 3rd byte + (4th byte << 8).
This offset is relative to the track header start. Remember the read offset being jumped
from (the next event after the FC command). Continue reading events from the new offset.
When an FD measure end is encountered, return to the remembered offset.

These measures do not nest. If an FC goto offset points directly to another FC command,
jump immediately again as directed. However, the return offset remains right after the
first FC command.

But if an FC command is encountered later than the immediate first event within a same
measure call, then it is treated as really an FD measure end.

If an F9 loop begin is encountered while in same measure, make note of it in your state
tracker. When the F8 loop end is encountered and you loop back to the F9, restore the
same measure state and return offset that were in effect when the F9 was initialised.
(Occasionally there's a "same measure, begin loop, play a note, end measure, end loop"
sequence, where the same end measure must run multiple times without breaking the loop.)


##### RCP v3

Haven't found any yet, but expecting something like below...

```
Header:
000 - signature : 32 bytes
020 - song name : 128 chars
0A0 - memo text : 12 lines of 30 chars each
208 - number of tracks : byte
209 - reserved or unused : byte
20A - time base : uint16 (ticks per quarter note, normally 48)
20C - tempo : uint16
20E - time signature, numerator : byte
20F - time signature, denominator : byte
210 - key signature : byte
211 - global key transpose or "play bias" : sint8
212 - reserved or unused : 134 bytes
298 - First CM6/GSD control file name, if any : 12 chars
2A4 - reserved or unused : 4 chars
2A8 - Second CM6/GSD control file name, if any : 12 chars
2B4 - reserved or unused : 4 chars
2B8 - Third CM6/GSD control file name, if any : 12 chars
2C4 - reserved or unused : 84 bytes
318 - instrument names and data? : 128 bytes
398 - SysEx data? : 8 times (24 chars description + 24 bytes data)
518 - track data begins here
```

RCPv3 Track header:

```
00 - track size : uint32
04 - track number : byte
05 - rhythm flag : byte
06 - midi channel : byte
07 - track key adjustment : sint8
08 - start shift? initial tick count at start of track? : byte
09 - mode : byte
0A - track name or "memo" string : 36 chars, padded with nulls or spaces
0x2E - event data begins here
```


### MMD files

There is more than one format using the MMD extension.


##### MMD used by Fairy Dust, Gainax, Himeya

This is a more efficient variant of RCP v2. There's no recognisable signature.
Some existing conversion code: (https://kurohane.net/silverhirame/soft_mus/index.html).

```
Header:
00 - tempo : byte
01 - unknown : byte
02 - array of 18 track info entries:
	track start offset in file : uint16
	key adjustment on track : sint8
	channel : byte
4A - another track info entry, or 4 bytes of garbage?
4E - two unknown bytes
50 - song title etc : null-terminated Shift-JIS string, or several, up to first track
```

All tracks follow the header immediately in order. If the first track's beginning offset
is in 4A..50, then the song title is omitted and the track just starts there.

The tempo value here is the same as RCP v2. The track key adjustment and midi channel
also work the same as RCP v2.

The 0x98 track sysex command works differently. Rather than being followed by a series of
0xF7 events, it is followed by a string of single bytes that terminates with 0xF7.

Each track is a sequence of events similar to RCP v2. Each event consists of 4 bytes.
You'll need to remember the most recently seen event. To process:

```
Read a byte from track data
If value & 0xF0 is 0x80, this is re-using the most recent event bytes, with:
	If value & 8 is not 0, event[0] = read new byte
	If value & 4 is not 0, event[1] = read new byte
	If value & 2 is not 0, event[2] = read new byte
	If value & 1 is not 0, event[3] = read new byte
Else if value is 0xFE, end track immediately
Else this is a normal RCP v2 event:
	event[0] = value
	event[1], [2], and [3] = read new byte for each
Handle the event[] as a normal RCP v2 event
```

Note that when jumping backward due to the loop end command, it's important to restore
the four event bytes to what they were immediately after the loop start event. The next
event right after a loop start may be an 0x80 shorthand.


##### MMD used by Fuga Systems

These appear as MMD and GMD files. This is another RCP variant with a stripped header and
some XOR encryption. Fuga's MD files are FM music, a different format.

ValleyBell documented this: (https://github.com/ValleyBell/MidiConverters/blob/master/fuga2rcp.c)

First, XOR the whole file with 0xA5. You can pretty reliably identify these by checking if
at least two of the bytes 0x03-0x05 are 0xA5.

Header:
```
byte - tempo
byte - time signature, numerator
byte - time signature, denominator
byte - key signature
sint8 - global key transpose or "play bias"
byte - unknown
```

Normal RCP track data follows immediately. You'll need to figure out individual track start
offsets by adding up track byte sizes in each track's header. There may be extra garbage at
the end of the file after track 18, ignore it.

Fuga MMD's don't use the 0x8F shorthand that other MMD's use.


##### HMM

Evidently Hideki Higuchi's MIDI format. Seems like an RCP variant.

Begins with two uint16's, probably tempo and something else.

After this is an array of track info, where each is:
```
byte - unknown, often 0, sometimes 0x0C or 0x80, could be a mute or transpose...
byte - midi channel, typically from 0 to 7 and 9 (rhythm)
uint16 - track size?
```

Memo text is found from offset 0x58 onward. This looks LZSS-compressed, even though the
rest of the file isn't, kind of a waste of effort.
