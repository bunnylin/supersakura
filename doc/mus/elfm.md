Elf MU files
------------

FM music file. Used in Run Run Kyousoukyoku, the Pinky Ponkies, Angel Hearts.
These appear quite minimal. Probably instrument definitions at the start, then
track data. Notes look to be in the 0x50-0x70 general range.

The player binary is `mu.com`. Considering how small that is, it shouldn't be too
difficult to figure out the format...

There's also an even older .M format used in Private School and Dokidoki Shutter Chance,
but the file contents look like MU files, so the two formats must be similar.


Elf M files
-----------

FM music file. Used from Dragon Knight 1 onward. Not the same as PMD, despite the
same .M suffix. Also not an EMI file.

The Project ANISE engine documents some aspects of Elf games: https://tomyun.pe.kr/projectanise/
But their code for reading M files looks for a "MUSIC DRV" or "OPL3 DATA" signature at
the start of the file, suggesting they're describing a different file format. Their
implementation also hard-targets an emulated YMF262, which is OPL-based, not OPN.

The games themselves come with a `play3.com`, `play4.com`, `play5.com`, or `elfmus98.com`,
which can be run in a PC98 emulator with a commandline argument to play .M files.
Press ESC to stop. Extremely useful for reverse-engineering.

The play binaries have a text message right after the initial execution jump.
Play3 is supposedly by Elf, but play4 and play5 are by "TAM¥TAM". Tam-Tam was a person
of some importance associated with Elf, I guess.

Words are in x86-native little-endian order.

Header:
```
uint16 - offset for first data block, always 0006
uint16 - offset for second data block, always 0006 or bigger
uint16 - offset for third data block, points to uint16 in the 0x300-0x600 range, which is
         preceded by a uint16 often 0000
```

First data block:
- The first block runs until the start of the second. Can be 0 bytes long.
- Length is always a multiple of 32, so it's a sequence of 32-byte entries.
- FM instrument definitions? PMD FM instruments on OPN have 30 parameters + ID byte...
Examples:
- 04 30 70 3A 7B 00 00 12 00 08 08 0B 04 04 04 06 06 05 06 03 03 01 01 1F 1F F0 00 80 09 00 40 08
- 11 60 30 22 03 09 1A 0D 0C 08 12 0A 11 03 04 02 03 00 00 00 00 0B 19 3F 0D F0 00 08 08 00 F0 05
- 1C 76 76 34 34 1F 1A 0D 19 94 94 99 99 08 08 0C 0C 00 00 05 05 35 35 47 47 F0 00 20 2F 00 07 02
- 28 03 77 0F 71 0A 18 19 35 1F 1F 13 17 04 06 0C 0C 01 01 01 02 1B 1B 2D 1F F0 00 40 03 00 50 07
- 28 73 07 7F 01 0A 18 19 19 1F 1F 13 17 04 06 0C 0C 01 01 01 02 1B 1B 2D 1F F0 00 20 2F 08 78 1F
- 2C 32 74 38 74 16 17 12 00 10 1F 1C 12 00 00 0A 0A 00 00 06 06 01 01 17 1D F0 00 80 09 08 14 08
- 2C 71 32 74 31 16 17 7B 1D 10 1F 1C 12 00 00 0A 0A 00 00 06 06 01 01 1D 1D F0 00 10 20 00 10 0B
- 2C 71 32 74 31 18 18 26 07 13 18 19 12 00 00 0A 0A 00 00 07 07 01 01 2D 2D F0 00 28 06 00 78 16
- 2C 72 34 78 34 16 17 2D 0C 10 1F 1C 12 00 00 0A 0A 00 00 05 05 01 01 1D 1D F0 00 08 08 00 F0 05
- 35 01 02 01 01 18 0F 2D 0C 18 1F 1C 12 04 00 00 00 00 00 00 00 1B 2B 1D 2D F0 00 08 08 00 F0 05
- 35 A1 52 A1 51 18 22 26 00 18 1F 1C 12 04 00 00 00 00 00 00 00 1B 2B 1D 2D F0 00 A8 04 08 78 1C
- 3C 74 72 32 32 21 20 04 0F 1B 18 1F 1F 15 15 0A 0A 0B 0B 05 05 55 55 49 49 F0 00 E0 15 00 48 04

Second data block:
- An array of 8-byte entries.
- Data often starts with EF, can start with F0, always with 7 more bytes.
- Data can start with F0F0 for small files, but that doesn't end the block.
- EF in x86 asm is output value to a port. Is this machine code? But F0 would be
  the LOCK command, not sensible.
- The second block runs until the start of the third. Can be 0 bytes long.
- SSG instrument definitions? PMD SSG instruments on OPN have 6 parameters...
  (attack rate, decay rate, sustain rate, release rate, sustain level, initial attack level)
  It would make sense for initial level to always be 0, the last byte in these examples;
  Don't know what to make of the first two bytes; none of the PMD SSG values go that high.

Examples:
- EF 02 0A 08 01 01 00 00, EF 02 0B 04 02 01 00 00, EF 02 0B 06 02 01 00 00
- EF 24 00 05 26 02 02 00, F0 00 00 00 00 02 08 00, F0 F0 00 3F 00 01 02 00, F0 F0 0A 0A 02 01 00 00
- F0 F0 00 3F 00 01 02 00, F0 F0 08 28 07 01 00 00, F0 F0 0A 0A 02 01 00 00
- F0 F0 00 00 00 02 1B 00

Third data block:
- First word is often in 0x300-0x600 range. Not an offset, can be bigger than file size.
- The third block runs until end of file.
- This must be the actual track data.
- There are always six tracks, presumably 3 FM and 3 SSG.
- Each track ends with F0, but note that F0 might be a valid parameter value for
  a track command, so have to parse byte by byte from the start.

Examples:
37 04 02 00 00 1B 2B 1D 14 00 2B 00 45 00 51 00 8F 00 CF 00 B0 AA CF D0 89 32 36 38 42 36 38 3A 44 32 36 3A 46 E0 83 4A 80 4A F0
8D A0 0E B1 A8 CF D0 89 32 36 38 42 36 38 3A 44 32 36 3A 46 E0 83 4A 80 4A F0
B2 AA CF D0 83 0A 0C E0 80 10 10 F0
B0 AE CF D0 8C 20 24 28 2A 32 34 36 38 1C 22 24 28 2C 30 32 34 1C 20 24 26 2A 30 32 36 E0 83 3A 3A 8F 3A AD 3A AC 3A AB 3A AA 3A A9 3A A8 3A A7 3A A6 3A A5 3A A4 3A A3 3A A2 3A 8D 0E F0
8D A0 0E B1 AC CF D0 8C 20 24 28 2A 32 34 36 38 1C 22 24 28 2C 30 32 34 1C 20 24 26 2A 30 32 36 E0 83 3A 3A 8F 3A AB 3A AA 3A A9 3A A8 3A A7 3A A6 3A A5 3A A4 3A A3 3A A2 3A A1 3A 3A 8D 0E F0
B0 AE CF D0 8C 1A 20 24 26 2C 30 32 34 18 1C 20 24 28 2A 2C 30 18 1A 20 22 26 2A 2C 32 E0 83 36 36 8F 36 AD 36 AC 36 AB 36 AA 36 A9 36 A8 36 A7 36 A6 36 A5 36 A4 36 A3 36 A2 36 8D 0E F0

32 04 02 55 1F 0A 0C 0B 14 00 50 00 8B 00 C4 00 E4 00 E6 00 B0 AC CF D0 87 12 10 AB 86 12 AC E0 83 08 AB E0 8F 08 E0 08 AA E0 08 E0 08 A9 E0 08 E0 08 A8 E0 08 E0 08 A7 E0 08 E0 08 A6 E0 08 E0 08 A5 E0 08 A4 E0 08 A3 E0 08 A2 92 08 A0 08 F0
8D A0 0E B1 AA CF D0 87 12 10 A9 86 12 AA E0 08 A9 E0 8F 08 E0 08 A8 E0 08 E0 08 A7 E0 08 E0 08 A6 E0 08 E0 08 A5 E0 08 E0 08 A4 E0 08 E0 08 A3 E0 08 A2 E0 08 A1 E0 08 A0 08 F0
B1 AB CF D0 87 22 20 AA 86 22 AB E0 83 1A AA E0 8F 1A E0 1A A9 E0 1A E0 1A A8 E0 1A E0 1A A7 E0 1A E0 1A A6 E0 1A E0 1A A5 E0 1A E0 1A A4 E0 1A A3 E0 1A A2 E0 1A A0 1A F0
B0 AE CF D0 87 12 10 AD 86 12 AE E0 84 00 AD 8F 00 AC 00 AB 00 AA 00 A9 00 A8 00 A7 00 A0 00 F0
A0 F0
A0 F0

3C 04 02 E0 00 E0 08 E0 14 00 95 00 97 00 99 00 E9 00 EB 00 8F A0 0E B0 AF CF D0 E0 8D 00 E0 89 00 AC E0 8F 00 AD E0 02 AE E0 08 AF E0 12 AC E0 00 AD E0 02 AE E0 08 AF E0 12 AC E0 00 AD E0 02 AE E0 08 AF E0 12 AE E0 0A E0 14 E0 0A E0 14 E0 0A E0 14 E0 0A E0 14 E0 0C E0 16 E0 0C E0 16 AF E0 0C E0 16 E0 0C E0 16 E0 18 E0 1C E0 26 E0 83 04 AD E0 8F 04 E0 04 AC E0 04 AB E0 04 AA E0 04 A9 E0 04 A8 E0 04 A7 E0 04 A6 E0 04 A5 E0 04 A4 E0 04 A0 04 F0
A0 F0
A0 F0
80 A0 0E B0 A8 CF D0 8F 00 B1 A9 00 B2 AA 00 B3 AB 00 B4 AC 00 B5 AD 00 B6 AE 00 B7 00 B8 00 B9 AF 00 B8 00 B7 AE 00 B6 00 B5 00 B4 00 B3 00 B2 00 B1 00 B0 00 00 00 00 00 AD 00 AC 00 AB 00 AA 00 A9 00 A8 00 A7 00 A6 00 A5 00 A4 00 A0 00 F0
A0 F0
A0 F0

click4:
50 04 02 EF 3F 02 05 00 14 00 16 00 18 00 1A 00 69 00 6B 00 A0 F0 A0 F0 A0 F0 B0 AE CF D0 92 00 A0 00 00 00 00 00 00 00 00 00 AE 00 A0 00 00 00 00 00 00 00 00 00 AE 00 A0 00 00 00 00 00 00 00 00 00 00 00 AE 00 A0 00 00 00 00 00 00 00 00 00 AE 00 A0 00 00 00 00 00 00 00 00 00 AE 00 A0 00 00 00 00 00 00 00 00 00 F0 A0 F0 A0 F0

keekai:

- 1st block:
38 70 50 64 01 10 18 0E 0C 19 1F 15 1F 05 08 06 03 05 09 07 06 89 29 6F 1D F0 00 08 08 08 F0 27
3C 35 73 74 34 02 00 00 00 1E 1D 1F 1F 0F 10 0F 0A 02 09 09 0B F8 F8 0B 0B F0 00 01 3F 00 53 3F
28 01 75 0F 71 0A 18 19 0F 0A 0D 0A 0D 0F 0F 0C 0C 0F 0C 01 02 1B 1B 2D 1F F0 00 E0 15 08 78 12

- 3rd block:
3C 04 02 F0
A0 F0
A0 F0
14 00 30 00 32 00 34 00 7D 00 C5 00 B0 AD CD D0 8F 0C B1 CE 0C B0 CD 0C B1 CF 64 66 64 B2 AE E0 83 0C E0 8A 0C 83 0C F0
A0 F0
A0 F0
86 A0 0E 92 0E B0 AC CF D0 8F 2C 2A 24 1A 2C 2A 24 1A 2C 2A 24 1A 2C 2A 24 1A 2C 2A 24 1A 2C 2A 24 1A AB 2C 2A AA 24 1A A9 2C 2A A8 24 1A A7 2C 2A A6 24 1A A5 2C 2A A4 24 1A A3 2C 2A A2 24 1A A1 2C 2A A0 24 1A 2C 2A F0
86 A0 0E B0 AD CF D0 8F 1C 1A 14 0A 1C 1A 14 0A 1C 1A 14 0A 1C 1A 14 0A 1C 1A 14 0A 1C 1A 14 0A AC 1C 1A AB 14 0A AA 1C 1A A9 14 0A A8 1C 1A A7 14 0A A6 1C 1A A5 14 0A A4 1C 1A A3 14 0A A2 1C 1A A1 14 0A A0 1C 1A F0
86 A0 0E B1 AD CF D0 8F 2C 2A 24 1A 2C 2A 24 1A 2C 2A 24 1A 2C 2A 24 1A 2C 2A 24 1A 2C 2A 24 1A AC 2C 2A AB 24 1A AA 2C 2A A9 24 1A A8 2C 2A A7 24 1A A6 2C 2A A5 24 1A A4 2C 2A A3 24 1A A2 2C 2A A1 24 1A A0 2C 2A F0

levup:

- 1st block:
04 72 32 06 04 0E 02 05 00 12 0D 12 13 09 08 09 07 08 07 05 09 C3 34 65 65 F0 00 80 09 08 8B 07

- 3rd block:
5F 04 02 3F 00 01 00 00 14 00 21 00 2E 00 30 00 32 00 34 00 B0 AB CF D0 8C 38 0E 0E 38 AA 87 38 F0
B0 A8 CF D0 8C 34 0E 0E 34 A7 87 34 F0
A0 F0
A0 F0
A0 F0
A0 F0


Elf MDT files
-------------

Used in Shangrlia 2. Possibly same as MUS used in Words Worth, and new M used in
Dragon Knight 4 and Metal Eye 2.

These begin with a 256-byte block that is almost entirely zeroed out. The new M
format leaves out this 256-byte block but otherwise looks the same.

The second byte of actual useful data appears to always be 0xC2...

Notes look to be in the 0x00..0x50 general range.

The player binary might be `music.bin`. It looks like valid machine code, and
contains the header text `__MUSIC DRIVER__  Version 0.38  By Pirorin/Witch`.
VNDB notes that Witch was a music group (or music circle?) who produced music
on contract mostly for Elf and Silky's. So presumably Pirorin was a programmer
in that group.

A very simple file like elf.m in DK4, an opening jingle, is big enough to suggest
the instrument data is still embedded in the music files and not in a shared
external file.
