.EMI files
----------

FM sound files, used in Cobra Mission and Knights of Xentar DOS ports.
Also in the Ayayo games and a few others, according to the Hoot collection.

Already partially documented by BlackStar:
https://moddingwiki.shikadi.net/wiki/MegaTech_VOL_Format

Start with a signature, [45 4D 00 06]. Then two bytes of useful data? Then
word addresses. First address points to a bunch of text. Second address seems
to point to start of music data. The next two point to end of file?

Header: (words are x86-native little-endian)
```
char[4] - signature "EM" 00 06
uint16 - unknown
uint16 - song title offset, usually 0x1C
uint16 - song data start offset
uint16 - same again?
uint16 - song data byte size
uint16 - unknown
uint16[] - six track start offsets
```

Commands:
`00` - rest
`01..7F` - note, from low C# upward
`81..90` - speed or delay of some sort?
`CD` - unknown
`CE` - unknown
`CF` - unknown
`DC` - unknown
`DD` - unknown
`E4 xx` - unknown
`E7 xx` - unknown
`E8 xx` - unknown
`F8` - loop?
`F7 xx` - unknown
`FA xx` - unknown
`FC xx` - unknown
`FD xx` - select instrument?
`FE` - end of track?
