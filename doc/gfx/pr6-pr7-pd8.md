PR6, PR7, PD7, PD8 images
-------------------------

Elf kept tweaking this format quite often, so there are mutually incompatible
revisions that can be hard to tell apart without trying and failing to
decompress in each format.

Source code for reading these is in [/inc/gfx/ada.pas](../../inc/gfx/ada.pas).

Encoders are also available:
- [makepr6.py](https://mooncore.eu/filus/makepr6.py)
- [makepd8.py](https://mooncore.eu/filus/makepd8.py)


### PR6 v2 (old)

This was used in early Elf games and Dragoon Armor by Fairytale.

- 8px columns, 8-color BRG
- Only allows the default max-contrast 8-color GRB palette
- Pixels are double-height (640x200 resolution)
- No obvious transparency indicator in the file
- Columns unpacked in normal vertical order
- Bitplanes are grouped at each column row
- Repeats don't cross columns
- Very simple but effective

Header:
```
uint8 - X offset, x8
uint8 - Y offset
uint8 - image width, x8
uint8 - image height
```

The compressed image follows the header immediately.

To decompress:
```
While image not done:
	aa, bb, cc := 3 next input bytes
	If (aa == 0xE9) and (bb != 0xE9):
		copy (cc & 0x3F)+1 rows from row bb onward;
		If cc >= 0x40 then copying from previous column
	Else:
		If aa == 0xE9:
			bb := cc; cc := next input byte
		Output aa,bb,cc as a literal
```

The row being copied from is relative to the first image row.
Exception: in Angel Hearts, it's relative to the full screen height,
so you have to subtract the image's Y coordinate.


### PR6 v1 (even older)

These use the ADA file suffix, but are a PR6 variant. Used in Lipstick Adventure 1.

Same header as above. Almost the same algorithm. Repeats can cross columns, both
for the copy-from and write-to pointers.

To decompress:
```
While image not done:
	aa, bb, cc := 3 next input bytes
	If (aa == 0x93) and (bb != 0x93):
		dd := next input byte
		copy dd rows from column bb, row cc onward; the columns and rows
		must take the graphic's own offset in account!
	Else:
		If aa == 0x93:
			bb := cc; cc := next input byte
		Output aa,bb,cc as a literal
```


### PR6 ANI

Elf uses .ANI files in various old games, which is a multiframe wrapper around the
PR6 v2 (old) format.

Header:
```
uint8 - number of frames
uint16 - end offset of final frame
uint16[] - array of start offsets for each frame, and the final end offset again
```

The array of start offsets should end right where the first frame data begins.
Each frame is a full PR6 image, header and all. After the final frame there may be
a bunch of extra unused space (disk sector leftovers), which can be ignored.
Remember that any repeat commands in a frame will be relative to that frame, not
to the first frame in the ANI file.

For file type verification, it may be a good idea to confirm that a fully extracted
frame consumed all the input data intended for that frame, and not a byte more.

Example file:
```
03 <final frame end>
<frame 0 start: 0x0B> <frame 1 start> <frame 2 start> <final frame end>
<frame 0 data> <frame 1 data> <frame 2 data> <garbage>
```


### PR7

Used in Elf games RayGun, Dragon Knight 2.

- 8px columns, hybrid 16-color/8-color BRG
- Always embeds a 16-color palette
- Pixels are double-height (640x200 resolution)
- No obvious transparency indicator in the file
- Columns unpacked in normal vertical order
- Bitplanes are grouped at each column row
- Repeats can cross columns
- Commands are a bit wastefully encoded due to the hybrid design

Words are little-endian, native x86.

Header:
```
uint16 - image X offset, x8
uint16 - image Y offset
uint16 - image width, x8
uint16 - image height
uint16[] - array of 16 palette entries, each a uint16, order 0GRB
uint8[] - array of 16 bytes, special codes
```

The special codes are typically in the range C8..FF, and are not necessarily
in order. Each special code must be unique. When reading the compressed data,
the first byte will either match one of these special commands, or is a literal.
However! Like with PR6, if the same special command byte immediately follows
the first, drop the first one and treat the second as the start of a literal.
This does guarantee any bit combination can be output as a literal, but applying
this double-byte exception to every special command no longer feels as elegant
as it did in PR6's simpler case. The original encoder does select the special
codes so as to minimise surprise literals, but some images will have them.

The compressed image follows the header immediately. The image is unpacked one
8px-wide column at a time, and each 8px row in that column is split in 4 bitplanes
in the order 8, 1, 2, 4 (intensity, blue, red, green). However, in this hybrid
format, intensity values always default to 0 except when specifically outputting
a single 4bpp literal (or copying that row later).

To uncompress:

```
While image not complete:
	i := next input byte
	special_index := index of special code that matches i, if any

	If the next input byte would be i again:
		Skip the repeated input byte, treat this as a literal, not
		a special code even though it matched

	If i is a special code, then Case special_index of:
		0:
			aa, bb := 2 next input bytes
			copy (bb & 0x3F) + 1 rows from row aa in this column,
			or from previous column if bb has bit 0x80 set
		1: aa := next input byte; copy 1 row from row aa in this column
		2: aa := next input byte; copy 2 rows from row aa in this column
		3: aa := next input byte; copy 3 rows from row aa in this column
		4: aa := next input byte; copy 4 rows from row aa in this column
		5: aa := next input byte; copy 1 row from row aa in previous column
		6: aa := next input byte; copy 2 rows from row aa in previous column
		7: aa := next input byte; copy 3 rows from row aa in previous column
		8: aa := next input byte; copy 4 rows from row aa in previous column
		9: Do this twice:
			bb, rr, gg := 3 next input bytes
			output (bb, rr, gg, 0) as a literal
		A: Do this twice:
			xx gg := 2 next input bytes
			output (xx, xx, gg, 0) as a literal
		B: Do this twice:
			xx bb := 2 next input bytes
			output (bb, xx, xx, 0) as a literal
		C: Do this twice:
			xx rr := 2 next input bytes
			output (xx, rr, xx, 0) as a literal
		D: Do this twice:
			rr gg := 2 next input bytes
			output (0, rr, gg, 0) as a literal
		E: Do this twice:
			bb gg := 2 next input bytes
			output (bb, 0, gg, 0) as a literal
		F: Do this twice:
			bb rr := 2 next input bytes
			output (bb, rr, 0, 0) as a literal
	Else:
		bb, rr, gg := 3 next input bytes
		output (bb, rr, gg, i) as a literal
```

Example: input `01 12 34 56`

Output bits:
- `intensity = 0000 0001`
- `blue   12 = 0001 0010`
- `red    34 = 0011 0100`
- `green  56 = 0101 0110`

Output pixels: `0427 0658`


### PR7 ANI

Games that use PR7 graphics probably also have PR7 in their .ANI multiframe wrappers,
which works exactly like the PR6 ANI files.

Header:
```
uint8 - number of frames
uint16 - end offset of final frame
uint16[] - array of start offsets for each frame, and the final end offset again
```

The array of start offsets should end right where the first frame data begins.
Each frame is a full PR7 image, header and all. This is somewhat wasteful, since
every header embeds a copy of the same palette, and the full special code dictionary,
which may be different per frame but would have been more efficient to only have one
per file.


### PR6 v3 (new)

Used in De-Ja 1. This is a variation on PR7, but the file suffix is PR6.
The algorithm only has minor differences, improving 8-color image compression slightly
while making it impossible to save 16-color images. The embedded palette still has
16 entries regardless.

To decompress, follow the PR7 code, except:
- Literals are only 3 bytes, so `rr, gg := 2 next input bytes` and `(output i, rr, gg, 0)`
- Special code 9 is no longer for outputting a pair of literals. Instead do this once:
  * `xx := next input byte`
  * `output (xx, xx, xx, 0) as a single literal`


### PD7 v1

Used in Foxy 2. This is a further modification of PR7, hacking in support for 640x400
resolution images, encoded as two 640x200 images on top of each other. Presumably the
reason for this is that there was a byte variable somewhere in the decompressing code
that was too hard to change to a word, so they couldn't easily unpack more than 255
rows at once without significant refactoring; splitting the image into two halves, both
less than 255 rows tall, was the quick fix.

- 8px columns, hybrid 16-color/8-color BRG
- Always embeds a 16-color palette
- Images can be up to 640x200, but a second image in the same file can double that
- No obvious transparency indicator in the file
- Columns unpacked in normal vertical order
- Bitplanes are grouped at each column row
- Repeats can not cross columns?
- Even less efficient than PR7 due to hybrid design and image split

Header:
```
uint16 - second image start offset, 0000 for not present
uint16 - image X offset, x8
uint16 - image Y offset
uint16 - image width, x8
uint16 - image height, clamp to 200 if above that
uint16[] - array of 16 palette entries, each a uint16, order 0GRB
uint8[] - array of 16 bytes, special codes
```

The second image is an entire new image complete with coordinates, size, palette,
and a new set of special codes (ie. the second image has a full header except for
the start offset word). The second image, if present, will be positioned directly
below the first. The second image's X offset and width should be identical to the
first image's. The Y offset should be the first image's Y offset plus 200.

When a second image half is present, the first half's pixel height is always 200,
regardless of what the header says. The second half's height should be what the
second header says, so the total image height is 200 + the second half's height.
However, the original engines appear to expect the first half's declared height
to be the full image height, or they may fail to show the image.

The decompression algorithm is otherwise identical to PR7. Where special codes
require copying from a specific row, that means the row in the current image half.


### PD7 v2 and PD8

Used in Elle with a PD7 suffix, and in Dragon Knight 3 and later with PD8.
Both are the same format. This is just like PD7 v1, except the number of special
codes is increased from 16 to 24.

Header:
```
uint16 - second image start offset, 0000 for not present
uint16 - image X offset, x8
uint16 - image Y offset
uint16 - image width, x8
uint16 - image height, clamp to 200 if above that
uint16[] - array of 16 palette entries, each a uint16, order 0GRB
uint8[] - array of 24 bytes, special codes
```

Because special codes are always 0xC8 or higher, it should be possible to distinguish
PD7 v2 from v1 by trying to read 24 special codes in the header. If any byte in that
array from 17..24 is < 0xC8 or a duplicate of a prior code, then it's the v1 format with
an array of only 16 codes.

Special codes 0..8 in PD7 v2 are the same as in previous formats; copy bytes from earlier.
Codes 9 and above have some new actions, while old actions are assigned to different codes.
Can repeat commands cross columns? Codes 0B (output two masked bytes) and 0C..15
(output two shorthand literals) can definitely cross columns.

Command handling differs from PD7 v1 thus:
```
function OutputMaskedByte(value_byte):
	bb, rr, gg, ii := 0
	If (mask_byte & 1) == 0: bb := value_byte
	If (mask_byte & 2) == 0: rr := value_byte
	If (mask_byte & 4) == 0: gg := value_byte
	If (mask_byte & 8) == 0: ii := value_byte
	value_byte := value_byte XOR 0xFF (negate value)
	If (mask_byte & 16) == 0: bb := (bb | value_byte)
	If (mask_byte & 32) == 0: rr := (rr | value_byte)
	If (mask_byte & 64) == 0: gg := (gg | value_byte)
	If (mask_byte & 128) == 0: ii := (ii | value_byte)
	output (bb, rr, gg, ii) as a literal

...
If i is a special code, then Case special_index of:
	9:
		mask_byte := next input byte
		OutputMaskedByte(next input byte)
	A:
		OutputMaskedByte(next input byte)
	B: Do this twice:
		OutputMaskedByte(next input byte)
	C: Do this twice:
		xx, gg, ii := 3 next input bytes
		output (xx, xx, gg, ii) as a literal
	D: Do this twice:
		xx, rr, ii := 3 next input bytes
		output (xx, rr, xx, ii) as a literal
	E: Do this twice:
		xx, rr, gg := 3 next input bytes
		output (xx, rr, gg, xx) as a literal
	F: Do this twice:
		bb, xx, ii := 3 next input bytes
		output (bb, xx, xx, ii) as a literal
	10: Do this twice:
		bb, xx, gg := 3 next input bytes
		output (bb, xx, gg, xx) as a literal
	11: Do this twice:
		bb, rr, xx := 3 next input bytes
		output (bb, rr, xx, xx) as a literal
	12: Do this twice:
		rr, gg, ii := 3 next input bytes
		output (0, rr, gg, ii) as a literal
	13: Do this twice:
		bb, gg, ii := 3 next input bytes
		output (bb, 0, gg, ii) as a literal
	14: Do this twice:
		bb, rr, ii := 3 next input bytes
		output (bb, rr, 0, ii) as a literal
	15: Do this twice:
		bb, rr, gg := 3 next input bytes
		output (bb, rr, gg, 0) as a literal
	16:
		aa := next input byte
		Copy previous row, but shift all pixels right by aa, extending the leftmost;
		for example "1234ABCD" SHR 2 == "111234AB"
		(if implemented with a native shift, change byte order to big-endian first)
	17:
		aa := next input byte
		Copy previous row, but shift all pixels left by aa, extending the rightmost;
		for example "1234ABCD" SHL 3 == "4ABCDDDD"
Else:
	Fall back to whatever PR7 does
```
