PAC image format
----------------

These are found in Great's Foreigner and Quintia Road 2.

Mission also has .pac files, but they are different...

Source code for reading these is in [/inc/gfx/greatpac.pas](../../inc/gfx/greatpac.pas).


##### Header

Words are in big-endian MSB-first order.

```
48 bytes - palette, 3 bytes per color, 16 colors, RGB order.
2 bytes - 00 00, unknown
word - image pixel width x8
word - image pixel height
```

The compressed image follows the header immediately.

In Quintia Road 2, `ba000.pac` doesn't have a header?.. The image data looks corrupted.


##### Data stream

The image is split into four bitplanes, each saved separately.
The first plane is bit 0 (what would be the blue plane in classic EGA colors).
Each plane is saved one 8px column at a time, left to right.
When all planes have been output, they need to be merged into the final image.

The compressed image is a series of command bytes, each with possible data bytes.
The action is `command & 0xE0`, and `command byte & 0x1F` is the number of repetitions
for this action. For example, `31 69` means command `2x` 17 times, which would output
17 `69` bytes.

```
0x aa bb cc ... - output x literals, which follow
2x aa - output "aa" x times
4x aa bb - output "aa bb" x times
6x aa bb cc dd - output "aa bb cc dd" x times
8x - copy x from same position on first bitplane
Ax - copy x from same position on second bitplane
Cx - output x 00 bytes
Ex - output x FF bytes
```
