C24 and C25 graphics
--------------------

These were used in Foster's native Windows engine titles.
- Pixels have byte order BGR
- Transparency is supported
- Multiple images can be in the same file

Both formats are already implemented at GarBro:
https://github.com/morkt/GARbro/blob/master/ArcFormats/Foster/ImageC24.cs

Source code for reading both of these is in [/inc/gfx/bmp.pas](../../inc/gfx/bmp.pas).


### C24

24bpp uncompressed pixel bitmap, plus possible RLE-encoded alpha.

Header:
```
char[4] - signature "C24" null
uint32 - number of image frames in file, usually 1
uint32[] - array of start offsets for each image frame
```

Frames in the same file can have different sizes. Some frames may be omitted,
in which case their start offset is 0. The frame offset coordinates can be negative.

Each image frame starts with its own header:
```
uint32 - frame pixel width
uint32 - frame pixel height
sint32 - frame offset X
sint32 - frame offset Y
uint32[] - array of image row start offsets, one value for each row
```

To unpack an image frame, loop through its array of row start offsets, and
unpack each image row from the given offset, like this:

```
For each row in image:
	Seek to the start of this row's data in the file
	run_code := 0xFF
	While row not complete:
		run_length := Read next input byte
		If run_length == run_code:
			run_length := Read next input uint16

		If run_code == 0:
			Output next run_length * 3 bytes from input as opaque pixels
		Else:
			Output run_length transparent pixels

		run_code := run_code XOR 0xFF
```


### C25

Same as C24, except signature is "C25" + null now, all alpha values are possible, and
decompression is different:

```
For each row in image:
	Seek to the start of this row's data in the file
	While row not complete:
		run_length := Read next input byte
		If run_length >= 0xF0:
			run_length := run_length & 0x0F;
			If run_length == 0:
				run_length := Read next input uint16
			Output next run_length * 4 bytes from input as BGRA pixels

		Else if run_length >= 0x80:
			run_length := run_length & 0x7F;
			If run_length == 0:
				run_length := Read next input uint16
			Output next run_length * 3 bytes from input as BGR pixels with A := 0xFF

		Else run_length < 0x80:
			If run_length == 0:
				run_length := Read next input uint16
			Output run_length fully transparent pixels
```
