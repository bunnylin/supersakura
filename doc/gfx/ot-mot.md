OT graphics
-----------

These were used in Fairy Dust's Koukan Nikki 1 and 2, and MaDoll.
This image format is kind of messily designed, so it's tricky to implement.

MLD can open these files.

- It's a bitstream, bits in each byte read from 0x80 downward.
- The image is saved as 2x2 pixel quads; scanline order, but two rows at the same time!
- Single bit prefixes appear, where 0 = output 2x2 literals, 1 = copy from before.
- Pixel literals are encoded using a delta table, like the Pi format, but the table row
  is selected based on neighboring pixels to take advantage of horizontal and vertical
  similarity.
- Repeats are arbitrary-length copies from a limited set of relative positions.
- Copy-from positions and lengths are saved as variable-width bit codes, and the bit
  codes are defined chaotically so you have to hardcode every branch. Repeat lengths
  of 64 and above are inefficiently encoded.
- Additional animation frames may follow the main image data.


##### Header

```
char[5] - signature "OT!", 0x20, 0x1A
uint16 - image X offset
uint16 - image Y offset
uint16 - image pixel width
uint16 - image pixel height
uint16 - unknown, always 0x0030? palette length?
uint16 - 0 = single-frame image, 1 = multi-frame image
uint16 - unknown, 0
uint16 - unknown, 0
byte[48] - palette, 3 normal bytes per color, GRB order
```

Note: The second and third bits of palette indexes are swapped. The 16 palette
entries appear in the order: `0145236789CDABEF`. In other words, normal order,
except swap (2 and 4), (3 and 5), (A and C), and (B and D).

The compressed image immediately follows the header. If this is a multi-frame image,
additional animation frames may appear after the main image data.


### Decompression

To unpack the image:
```
Initialise delta_table, 16 rows by 16 columns
While image not complete:
	i := Read next bit from input
	If i is clear:
		Output a 2x2 quad of literals, see below
	If i is set:
		Copy 2x2 quads from before, see below
```


##### Delta table literals

This is a form of entropy coding called Move-to-Front Transform. It's a lookup
table that moves the most recently fetched item to the front of the table so the
next reference to that value will use a short-width code.

Code points don't correspond with fixed outputs, but the first literals at the start
of an image start using a cleanly ordered delta row, `123456789abcdef0`, so the first
input code is always only off by one from the output.

Codes:
- 0: [11]
- 1: [101]
- 2: [100 00]
- 3: [100 10]
- 4: [100 010]
- 5: [100 110]
- 6: [100 0110]
- 7: [100 1110]
- 8: [100 01110]
- 9: [100 11110]
- A: [100 011110]
- B: [100 011111]
- C: [100 111111]
- D: [100 1111100]
- E: [100 1111101]
- F: [0]

Literal code points appear in groups of four, outputting a 2x2 pixel area.
The order is: top left, top right, bottom left, bottom right.

Initial delta table, where row 0 is at the top, and low indexes are to the left:
```
[123456789abcdef0]
[23456789abcdef01]
[3456789abcdef012]
[456789abcdef0123]
[56789abcdef01234]
[6789abcdef012345]
[789abcdef0123456]
[89abcdef01234567]
[9abcdef012345678]
[abcdef0123456789]
[bcdef0123456789a]
[cdef0123456789ab]
[def0123456789abc]
[ef0123456789abcd]
[f0123456789abcde]
[0123456789abcdef]
```

Any fetched value gets moved to the frontmost code point of its row, except the
rightmost value, which always remains at the right end of the row (code point F).
The last code point has the shortest encoding length, because the algorithm tries
to predict each pixel based on its neighbors, so the shortest bit width is used
if it predicted right.

To output a literal:
```
p := Predict what the next pixel will be, based on its neighbors
i := Read a literal code point from input
color := Fetch the value from delta_table[row p][column i]
If (i != 0xF):
	Move the fetched value in delta_table[row_p] to column 0,
	shifting other colors up by 1 to make space
Output color
```

To predict the next pixel:
```
prediction := the pixel at [0,-2] relative to pixel being predicted
If (the pixels at [0,-1] and [0,-2] are not the same)
And (the pixels [0,-2] and [-2,0] and [-2,-2] are not all the same):
	If (the pixel being predicted is the 1st or 3rd pixel in the 2x2 quad)
	And (the pixels [-2,0] and [-1,0] are the same),
	Or (the pixel being predicted is the 2nd or 4th pixel in the 2x2 quad)
	And (the pixels [-3,0] and [-1,0] are the same):
		prediction := the pixel at [-1,0]
	Else:
		prediction := the pixel at [-1,-1]
```

If any relative pixel location would be out of bounds, use a 0 for that pixel value.

Example, assuming only the first table row is being used, ie. all pixels predicted as 0:
- Output bits: `0 10010 11 101 11`
- 0: going to output a 2x2 quad of literals
- 10010: code "3", so output delta_table[0][3], which is "4"; shift "4" to front of row
- 11: code "0", so output delta_table[0][0], which is now "4"
- 101: code"1", so output delta_table[0][1], which is "1"; shift "1" to front of row
- 11: code"0", so output delta_table[0][0], which is now "1"


##### Copy 2x2 quads from before

The parameters are: a copy-from position code, and a variable-width copy length.

The copy-from code looks up the actual position from a hardcoded list of 27 possible
relative positions. The source position and copy length are counted in 2x2 pixel quads.
So repeat sequences are only possible where entire 2x2 quads are repeatable.

Variable-width binary codes for copy-from positions:
- 11: code 0, position [0,-1], the 2x2 quad directly above
- 011: code 1, position [-1,0], the 2x2 quad directly to the left
- 101: code 2, position [1,-1], right and above
- 0001: code 3, position [-1,-1], left and above
- 00000: code 4, position [2,-1]
- 00111: code 5, position [-2,0]
- 00101: code 6, position [-2,-1]
- 00110: code 7, position [0,-2]
- 01000: code 8, position [1,-2]
- 10011: code 9, position [-4,0]
- 01011: code 0xA, position [-1,-2]
- 10001: code 0xB, position [4,-1]
- 001001: code 0xC, position [-4,-1]
- 001000: code 0xD, position [-8,0]
- 000011: code 0xE, position [2,-2]
- 010101: code 0xF, position [-8,1]
- 010011: code 0x10, position [-2,-2]
- 010100: code 0x11, position [4,-2]
- 100001: code 0x12, position [-16,0]
- 100000: code 0x13, position [-4,-2]
- 0100100: code 0x14, position [-8,-1]
- 100101: code 0x15, position [8,-2]
- 0000101: code 0x16, position [16,-1]
- 0000100: code 0x17, position [-8,-2]
- 0100101: code 0x18, position [16,-2]
- 1001000: code 0x19, position [-16,-1]
- 1001001: code 0x1A, position [-16,-2]

Variable-width binary codes for copy lengths:
- 00110011100101: 0 quads, only occurs as a second code after an 0x40+ value
- 1: 1 quad
- 000: 2 quads
- 011: 3
- 0100: 4
- 00101: 5
- 01010: 6
- 001001: 7
- 001110: 8
- 010111: 9
- 0011000: 0xA
- 0011010: 0xB
- 0101100: 0xC
- 00100000: 0xD
- 00100011: 0xE
- 00110110: 0xF
- 00111100: 0x10
- 00111111: 0x11
- 001000010: 0x12
- 001000100: 0x13
- 001100100: 0x14
- 001101110: 0x15
- 001111010: 0x16
- 001111100: 0x17
- 001111101: 0x18
- 010110111: 0x19
- 0010001010: 0x1A
- 0011001011: 0x1B
- 0011001101: 0x1C
- 0011001010: 0x1D
- 0011011111: 0x1E
- 0011110111: 0x1F
- 0101101000: 0x20
- 0101101011: 0x21
- 0101101001: 0x22
- 0101101100: 0x23
- 00100001101: 0x24
- 00100001110: 0x25
- 00100010110: 0x26
- 00110011001: 0x27
- 00110011101: 0x28
- 00111101100: 0x29
- 01011010100: 0x2A
- 00110111100: 0x2B
- 00110111101: 0x2C
- 01011010101: 0x2D
- 01011011011: 0x2E
- 001000011110: 0x2F
- 001000011001: 0x30
- 001000101110: 0x31
- 001100111101: 0x32
- 001000101111: 0x33
- 001100110000: 0x34
- 001000011000: 0x35
- 001100110001: 0x36
- 001100111110: 0x37
- 001111011011: 0x38
- 001000011111: 0x39
- 001100111100: 0x3A
- 001100111111: 0x3B
- 001100111000: 0x3C
- 001111011010: 0x3D
- 010110110101: 0x3E
- 010110110100: 0x3F
- 00110011100100: 0x40, plus a second length code
- 00110011100111: 0x80, plus a second length code
- 001100111001101: 0xC0, plus a second length code
- 0011001110011000: 0x100, plus a second length code
- 0011001110011001: 0x140, plus a second length code

Note: The length codes 0x40 and above are immediately followed by an extra length code
whose value is added to the first value.

The copy operation can't cross rows, in either input or output. If the copy source span
is partially or completely out of bounds, output zeroes for the out of bounds section.

Example:
- On the first row, input bits `1 11 000`
- 1: going to copy 2x2 quads from before
- 11: copying from directly above, which is out of bounds...
- 000: copy length is 2 quads
- Output a pair of 2x2 quads of all 0, an easy black fill

Example:
- 42 quads away from the right edge, input bits `1 10001 00110011100100 000`
- 1: going to copy 2x2 quads from before
- 10001: copying from [4,-1], four quads ahead on the above row
- 00110011100100: copy length is 40 quads, plus...
- 000: 2 quads, for a total of 42 quads; this will complete the output row
- Output 38 quads copied from four quads ahead on the above row
- The remaining 4 quads would be out of bounds at the source, so output 4 quads of all 0


##### Animation data

If there are still dozens or thousands of bytes of input data left after the image
is fully unpacked, animation data may be present. Check for the "ANI" signature to verify.
The image header should also specify this is a multi-frame image.

Note: You'll have to discard any leftover bits in the current byte at the end of the
base image decompression, so the animation signature is nicely byte-aligned. But don't
discard input bits between animation frames, when unpacking.

Animation header:
```
char[4] - "ANI:"
uint16 - object count
uint16 - sequence count

array[object count]
	uint16 - frame count, can be 0 if the object is empty
	uint16 - object X offset
	uint16 - object Y offset
	uint16 - object pixel width
	uint16 - object pixel height

array[sequence count]
	uint16 - sequence length
	array[sequence length]
		8 x byte - object:frame pairs for all four objects, zeroes if undefined
		byte - delay in 40ths of a second after these frames, 0 for stop animating
```

An example hopefully makes it clearer. Ma Doll's 077:
```
"ANI:" signature
0003 - object count
0002 - sequence count

0004 - framecount for object 0
00D0, 006A - offset for object 0
0068, 007C - frame pixel size of object 0

0004 - framecount for object 1
0050, 0068 - offset for object 1
0080, 00BA - frame pixel size of object 1

0004 - framecount for object 2
0008, 00AA - offset for object 2
0048, 0064 - frame pixel size of object 2

0003 - length of sequence 0
01 01, 02 01, 03 01, 00 00, 08 - all 3 objects use their 2nd frame; wait 200ms before next
01 02, 02 02, 03 02, 00 00, 06 - all objects use their 3rd frame; wait 150ms before next
01 03, 02 03, 03 03, 00 00, 06 - all objects use their 4th frame; wait 150ms before next

0001 - length of sequence 1
01 00, 02 00, 03 00, 00 00, 00 - all objects use their 1st frame; time = 0 so don't animate
```

Each object is a separate animated rectangle over the base image. Different objects don't
have to have the same framecount, but normally they do. Objects' screen coordinates should
not have any overlap. The frames for each object are saved in a vertical sequence, so the
total bitmap height for each object is its frame height multiplied by its frame count.

Object indexes in the sequence definitions are off by 1, so that 0 can be used for
an empty definition.

Reset the delta table to its initial values before you start unpacking the animation frames.
The table is not reset between frames, however.

The decompression algorithm is the exact same for animation frames as for the base image.
The compressed bitstream begins immediately after the header.


MOT graphics
------------

An upgraded OT, used in the Windows ports of Ma Doll and Koukan Nikki DX.

##### Header

```
char[4] - signature "MOT "
uint16 - unknown, 0
uint16 - image X offset
uint16 - image Y offset
uint16 - image pixel width
uint16 - image pixel height
uint16 - 0 = single-frame image, 1 = multi-frame image
uint16 - unknown, 0
uint16 - unknown, 0
byte[256 * 3] - palette, 3 normal bytes per color, GRB order
```

The compressed image immediately follows the header, from 0x314 onward.

I suspect the compression algorithm is the same, just with a much bigger delta table...
