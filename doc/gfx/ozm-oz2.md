OZM graphics
------------

These are used in Apple Pie games Lakers 1 and 2, Chou Baku, and Dennou Garou.

Palette may be kept separate in .RGB files. There's no explicit association between
image files and palette files, so you just have to guess which to use together.
The palette is 16x3 bytes, low nibbles only, RGB byte order.

Source code for reading these is in [/inc/gfx/ozm.pas](../../inc/gfx/ozm.pas).

An [encoder](https://mooncore.eu/filus/makeoz.py) exists too.

Header: (little-endian words)
```
byte - version number? 0x03 for Lakers, 0x02 for Chou Baku and Dennou Garou
byte - another version number? palette flag? 0x01 in Lakers 2, 0x00 elsewhere
uint16 - image width x 8
uint16 - image height
uint16 - start offset for image data, 0x0039 or 0x003A
byte - palette flag? 0x01 if valid palette in header, 0x00 to use a file instead
byte[48] - palette in RGB order, all zeroed if palette flag zero
byte - only present if version is 0x03, end of header marker? 0x0F
```

Image data follows the header immediately. Drawn left to right in 8px columns, split
into bitplanes. Each bitplane is drawn for the whole image before the next starts.
There's no end column marker, byte repeat commands can cross columns, but not planes.
Also no end bitplane or end image marker.

The image is RLE-compressed. While reading non-zero bytes, those are literals:
output them as rows in the current 8px bitplane column. When 00 is encountered,
it is a special multi-byte command. After the command's bytes, resume reading
literals until the next non-zero byte.

```
00 01 - output one 00 byte
00 02 - output two 00 bytes
00 03 - output three 00 bytes
00 04 - output four 00 bytes
00 05 - output five 00 bytes
00 06 aa xxxx - output aa for xxxx bytes
00 07 aa bb xxxx - output aa, bb for xxxx bytes
aa - output aa as literal
```

OLH graphics
------------

"Ohishi cg Loader Hyper version - From team XYZA... ver.1.10 (1994/12/02) by H. Ohishi."
These are used in Lakers 2 and Metamorph Meena. These use an improved compression
algorithm, and the file may contain multiple OLH images appended together,
typically for rapid animation purposes.

Source code for reading these is in [/inc/gfx/ozm.pas](../../inc/gfx/ozm.pas).

An [encoder](https://mooncore.eu/filus/makeolh.py) exists too.

Header:
```
char[] - sig "OLH", and maybe other arbitrary text, ends with 1A 00
byte - flag: 0x4 = size is present, 0x8 = palette is present

If size is not present, use 640x400; otherwise read:
byte - width*8
uint16 - height

If palette is not present, keep previous palette; otherwise read:
byte[32] - 16 palette indexes, each taking two bytes, 0GRB order (47 05 -> 457 RGB)
```

Image data follows the header immediately. Drawn left to right in 8px columns, split
into bitplanes. Each bitplane is drawn for the whole image before the next starts.
There's no end column marker, some commands can cross columns, none cross planes.
Also no end bitplane or end image marker.

If multiple frames exist, a new "OLH" sig follows immediately after the end of the
previous frame's image data.

For any command except 0x, 1x, Fx, the repeat count is the nibble x.
But if x is 0, read another byte rr and...
- if rr = 0: repeat count := read next uint16 in big-endian order
- if rr >= 0x10: repeat count := rr
- else: repeat count := (rr << 8) + read next byte

Examples:
- 51 -> output one 0x00 byte
- 30 11 88 -> output 0x88 byte 0x11 times
- 90 01 23 -> copy 0x123 bytes from the previous column
- 60 00 7D 00 -> output 0xFF byte 0x7D00 times, filling up a whole 640x400 bitplane

```
0x, 1x, Fx - output this as a literal byte
2x ... - output x literal bytes
3x aa - output aa byte x times
4x ab - output aa,bb for x bytes
5x - output 00 byte for x bytes
6x - output FF byte for x bytes
7x - copy x bytes from 2 rows ago, can't cross columns
8x - copy x bytes from 4 rows ago, can't cross columns
9x - copy x bytes from previous column
Ax - copy x bytes from this location on bitplane 0
Bx - copy x bytes from this location on bitplane 0, inverted
Cx - copy x bytes from bitplane 1
Dx - copy x bytes from bitplane 1, inverted
Ex aa - copy x bytes from special location:
	00: bitplane 2
	01: bitplane 2, inverted
	02: bitplane 0 OR bitplane1
	03: bitplane 0 AND bitplane1
	04: bitplane 1 OR bitplane2
	05: bitplane 1 AND bitplane2
	06: bitplane 0 OR bitplane2
	07: bitplane 0 AND bitplane2
	08: 2 columns to the left
	09: 16 rows above, can't cross columns
```


OZ2 graphics
------------

These are used in the Apple Pie game Lakers 3. The PC98 OZ2 files use this
compression scheme. OZ2 files on Windows are a different thing, see below.


APR, OZ2 on Windows
-------------------

Used in the Apple Pie games Lakers 3, Fern.
OZ2 files on Windows are actually simple BMPs. There are also accompanying
MSK files, which are alpha masks. Grapholic calls both "bbm" files.

tbd
