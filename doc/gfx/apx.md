APG/APH/API/APX images
----------------------

These were used in most ScooP games.


### APG images

Recognisable by signature "API CG.03x".


### APH images

Signature "TKH Version 1.0", null-terminated.


### APX images

```
char[8] - signature "APX" and 5 nulls
uint16 - image pixel width
uint16 - image pixel height
uint16 - unknown, always 0?
byte[3*16] - palette, high nibbles only, RGB? order
```

The compressed image stream immediately follows the header.
