VDF images
----------

These were used by Panda House labels Cat's Pro and Melody.

- Single or multiple up to 4bpp bitmaps in each file
- Transparency supported via an inefficient alpha mask image
- Pixel data is saved in 8px 1bpp columns
- RLE-compressed, optionally LZSS-compressed on top of that
- Repeats can't carry across columns

Source code for reading these is in [/inc/gfx/vdf.pas](../../inc/gfx/vdf.pas).

There's also an encoder: [https://mooncore.eu/filus/makevdf.py]

The file can begin with one of a few possible three-character signatures:
- LDF and MDF make use of LZ compression
- VDF and LDF have a single bitmap in the file
- MDF has two bitmaps; the first is an alpha mask for the second normal image
- ADF contains multiple full VDF images, used as animation frames
- GDF seems to be the same as VDF?


##### Palette

In VDF, LDF, and MDF files, a palette immediately follows the signature, at offset 4.
The palette is stored as an array of 48 uint16's, dickishly shuffled and encrypted.
Seriously, the palette encryption is more complex than the actual image compression...

To unshuffle the palette words, reorganise the 48 uint16's such that:
- Color 0 starts from index 0
- Colors 1..7 start from indexes 25..31
- Color 8 starts from index 24
- Color 9..15 start from indexes 1..7

Each color has three components, which are at indexes +8 and +16 relative to
the color's first index.

In other words, given a palette of uint16[48]:
- Color 0 uses uint16 [0, 8, 16]
- Color 1 uses uint16 [25, 33, 41]
- Color 2 uses uint16 [26, 34, 42]
- ...
- Color 7 uses uint16 [31, 39, 47]
- Color 8 uses uint16 [24, 32, 40]
- Color 9 uses uint16 [1, 9, 17]
- Color 10 uses uint16 [2, 10, 18]
- ... and so on

To decrypt each palette color, use its uint16 components `c1`, `c2`, and `c3`:
(all division commands are integer division)
```
If c2 == 0:
	The color is monochrome: R, G, B are all (c3 * 3 + 10) div 20
Else:
	scheme := c1 / 0x3C
	c1 := (c1 modulo 0x3C) * 5 / 3
	c2 := (c2 * c3) / 0x64
	x := c3 - c2
	y := c1 * c2 / 0x64

	If scheme == 0:
		c1 := c3
		c2 := x + y
		c3 := x
	If scheme == 1:
		c1 := c3 - y
		c2 := c3
		c3 := x
	If scheme == 2:
		c1 := x
		c2 := c3
		c3 := x + y
	If scheme == 3:
		c1 := x
		c2 := c3 - y
		c3 is unchanged
	If scheme == 4:
		c1 := x + y
		c2 := x
		c3 is unchanged
	If scheme == 5:
		c1 := c3
		c2 := x
		c3 := c3 - y

	R := (c1 * 3 + 10) div 20
	G := (c2 * 3 + 10) div 20
	B := (c3 * 3 + 10) div 20

Extend R, G, B from the range 0..F to the full 0..FF now, if you prefer.
Return R, G, B
```

Examples:
- Encrypted components 0149 001d 005d resolve to E,A,C
- Encrypted components 0020 001c 002e resolve to 7,6,5
- Encrypted components 0020 0013 0042 resolve to A,9,8

The next section from offset 0x64 is either image data (VDF), or one or more LZ-compressed
image sections (LDF or MDF).


##### LDF/MDF decompression

LDF and MDF files have a compressed section beginning at offset 0x64. MDF files have
an additional compressed section directly after the first.

```
uint32 - compressed data size
uint32 - uncompressed data size
byte[] - compressed data
```

Standard LZSS algorithm with an 18-byte offset, and a pre-initialised repeat buffer.

```
Initialise the repeat buffer, 0x1000 bytes:
	For i := 0 to 0xFF:
		Write i as a byte 13 times
	Write the 256-byte sequence 0..0xFF
	Write the 256-byte sequence 0xFF..0
	Write 128 bytes of 0
	Write 110 bytes of 0x20
	Write 18 bytes of 0

While stream not done:
	flag_byte := Read next byte from stream
	For each bit in flag_byte from 0x01 to 0x80:
		If the bit is set:
			Read the next byte from the stream and output as a literal
		If the bit is clear:
			AB, CD := Read the next two bytes from the stream
			read_distance := CAB
			copy_len := D + 3
			read_distance := (current_output_offset - 18 - read_distance) & 0xFFF
			Output copy_len bytes from read_distance bytes ago
			If copying from before start of buffer, output from the pre-initialised
				section instead, copy_len bytes from buffer index CAB onward
```

With the data decompressed, put it through VDF decoding to get the bitmap.


##### VDF decoding

VDF data starts with a short header:
```
uint8 - image x offset, multiplied by 8
uint16 - image y offset
uint8 - image pixel width, multiplied by 8
uint16 - image pixel height
uint8 - bitflag; the low nibble indicates which bit planes are present
```

The image is RLE-encoded. Each 8px column is presented in a group of up to four columns,
one for each bitplane that's present. The bitflag's low nibble is usually 0xF, indicating
each column appears four times for the full 4bpp result.

The decoding rules are simple:
- `03 cc xx`: output `xx` as a literal `cc` times; if `cc` = 0, output 256 times
- `04 xx`: output literal `xx` once, where `xx` is an otherwise special value 03, 04, or 05
- `05 cc xx yy`: output literals `xx`,`yy` as a pair `cc` times
- `xx`: output literal `xx` once

Decoding pseudocode:
```
While image not complete:
	b := Read next byte from input

	If b == 3, then output a repeat of a single byte:
		repeat_count := Read next byte from input
		If repeat_count == 0, then repeat_count := 256
		repeat_value := Read next byte from input
		Output repeat_value for repeat_count times
	If b == 4, then output an explicit literal byte:
		b := Read next byte from input
		Output b as a literal
	If b == 5, then output a repeat of two bytes:
		repeat_count := Read next byte from input
		first_value := Read next byte from input
		second_value := Read next byte from input
		Output first_value and second_value for repeat_count pairs
	Else:
		Output b as a literal
```

With the image decoded, the output buffer has a layout like this:

```
+-------------------------+
| 1st column, bitplane 0  |
+-------------------------+
| 1st column, bitplane 1  |
+-------------------------+
| 1st column, bitplane 2  |
+-------------------------+
| 1st column, bitplane 3  |
+-------------------------+
| 2nd column, bitplane 0  |
+-------------------------+
| 2nd column, bitplane 1  |
+-------------------------+
| ...                     |

```

Merge the 1bpp bitplanes for each column together to get a full 4bpp column.
Flip the buffer so each column is displayed vertically as an actual 8px-wide column,
and the image is finished.


##### MDF alpha-masking

MDF images have contain two LZ-compressed sections. After decompressing and unpacking,
the first is a 4bpp alpha mask image, and the second is the normal 4bpp image.
Although the alpha mask has four planes, all four are identical. Each 0xF nibble in
the mask means the corresponding color pixel is fully transparent; a 0x0 mask value
means a fully opaque pixel.

There are some images where the mask is just a copy of the normal image. I'm not sure
what the best way is to handle these... are they perhaps unused graphics?

The mask bitmap and normal bitmap are rarely not the same size. I'm not sure how the
original engine handles that...


##### ADF multiple frames

These are different from the other VDF subtypes in that an ADF is a container for
multiple full VDF images.

Header:
```
char[4] - signature "ADF" + null
uint16 - number of frames
uint16[] - byte size for each frame
uint16 - an extra copy of the last byte size?
uint16 - 0xFFFF terminator
```

The rest of the first file sector (0x800 bytes) is unused. Next, from offset 0x800,
there's a list of internal filenames, one for each frame. Each string is terminated
by `00 0A`. After the final filename, there's an `FF 00`. The rest of the sector is
again unused.

The first VDF file begins at 0x1000. Additional frames begin after the given byte
sizes. Unpack them like normal VDF images. The different frames can have different
palettes and different pixel sizes.


HDF images
----------

Uncompressed sprite bitmaps with multiple frames. There is no palette included.

Source code for reading these is in [/inc/gfx/vdf.pas](../../inc/gfx/vdf.pas).

And there's an encoder: [https://mooncore.eu/filus/makehdf.py]

Each HDF file begins with an array of big-endian uint32's. It's a strictly increasing
sequence pointing to frame starts. The array runs up to the first specified offset,
so if the first offset is 4, it's a single-frame image. The first offset must be
a multiple of 4; subsequent offsets are also often but not always multiples of 4.
If an offset points exactly at the end of the file, treat that and any subsequent
offsets as empty frames.

Each frame is saved with a header and alpha mask, then another header and the 4bpp bitmap.
Rarely, the alpha mask and its header are omitted, in which case the sprite is all opaque.
It is also rarely possible to have a 1bpp color bitmap, in which case the alpha bitplane
value is 0001 and the color bitmap bitplane value is also 0001.

The header: (words are x86-native little-endian)
```
uint16 - bitplanes: 0001 for 1bpp alpha mask or bitmap, 000F for 4-plane bitmap
uint16 - width, x8
uint16 - height
```

The mask and all bitplanes are stored scanline-wise; that is, one row at a time,
8 pixels per byte.

In the alpha mask, a 1-bit is opaque, a 0-bit is transparent.

In both the alpha mask and color bitmap, in each byte, the topmost bit 0x80 is the
leftmost pixel.

Frame dimensions don't have to be the same for every frame. However, the dimensions
for the alpha mask should always be the same as its associated bitmap. A frame with
zero width or height is possible, in which case only the two 6-byte headers are
present but otherwise it's an empty frame. Frame pixel widths must always be a multiple
of 16, and the width specified in the frame header is the pixel width divided by 8.

Cautions: (Night Slave)
- mis2.hdf: the last 4 of 16 frames are broken
- missile1.hdf: the last frame is cropped out and the second to last frame from
  0x608 only has the alpha mask, no color bitmap
- smoke.hdf: only 1bpp color bitmaps, no alpha masks
