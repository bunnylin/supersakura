GDT, DA1, DAD graphic files
---------------------------
Used by C's Ware, Himeya Soft, Forest.
GDT and DA1 are the same thing. DAD is a bundle of multiple images appended together.

Source code for reading these is in [/inc/gfx/da1.pas](../../inc/gfx/da1.pas).

An [encoder](https://mooncore.eu/filus/makeda1.py) exists too.


##### Header

Words are in x86-native little-endian order.

```
dword - 44 41 31 00 "DA1" signature
dword - file size from start of sig to end of bitstream
byte - horizontal offset x8
byte - image pixel width x8
word - vertical offset
word - image pixel height
byte - 00, unknown
byte - bitflag, always has 0x0F? $80: palette is present; $40: 8px column mode
24 bytes - (only if bitflag has $80) palette: 3 nibbles per color, 16 colors, BRG order.
4 words - byte size for all four following sections
```

For example, section sizes could be $299D, $1418, $23EA, $142E. Section start offsets
are therefore $0030, $29BD, $3DE5, $61CF.

The bytestream for the first section starts after the fourth section size word.
Each section corresponds to a bitplane, so to get the final 4 bpp bitmap you have
to merge the four planes. The first plane is bit $01, the fourth plane is bit $08.
(You can think of them as the blue, green, red, and intensity plane, like the
EGA color palette, although of course the palette here can be different.)
Each section contains an entire bitplane of the image, and subsequent sections may
reference earlier planes to copy bit patterns, so they must be unpacked in order.

Use the appropriate 8px/16px column mode to uncompress all sections. In 8px mode,
section data is read one byte at a time. In 16px mode, it is read one word at a time.
The image is packed one column at a time, top-down, then left-to-right, each column
explicitly terminated with an 0xFF command.

DA1 compression consists RLE repeats, literals, as well as fine-grained sequence copies
from earlier in the current image or from a base image.


##### 8px column mode (bitflag $40 set)

```
00 xx: clear all bits for next xx rows
0x: clear all bits for next x rows (up to $1F, clear 31 rows)
20 xx: set all bits for next xx rows
2x: set all bits for next x rows (up to $3F, set 31 rows)
4x/40 xx: import all bits for next x or xx rows from plane 0 blue
6x/60 xx: import all bits for next x or xx rows from plane 1 red
8x/80 xx: import all bits for next x or xx rows from plane 2 green
Ax/A0 xx: import all bits for next x or xx rows from 16 pixels above
Bx/B0 xx: import all bits for next x or xx rows from 8 pixels above
Cx/C0 xx: import all bits for next x or xx rows from 4 pixels above
Dx/D0 xx: import all bits for next x or xx rows from 2 pixels above
Ex/E0 xx: import all bits for next x or xx rows from 16 pixels left
F1-F8 ...: apply specified bytes for the next 1-8 rows, one byte per row, MSB is leftmost
F0 xx ...: apply specified bytes for the next xx rows, one byte per row
F9 xx: skip next xx rows
FA xx yy: apply byte yy for the next xx rows
FB 0x: import all bits for next x rows from plane 0 blue, inverted
FB 8x: import all bits for next x rows from plane 1 red, inverted
FC 0x: import all bits for next x rows from plane 2 green, inverted
FC 8x A5: produces x repeats of 2-row pattern, 55/AA
FD 0x yy zz: produces xx repeats of 2-row pattern: yy/zz
FD 8x 21: produces x repeats of 4-row pattern, 11/22/44/88 (rotate by two on second pair)
FD Cx 12 48: produces x repeats of 4-row pattern 22/11/88/44
FE 4x: import all bits for next x rows using bitwise AND: plane 0 & plane 1
FE 8x: import all bits for next x rows using bitwise AND: plane 0 & plane 2
FE Cx: import all bits for next x rows using bitwise AND: plane 1 & plane 2
FE xx aa bb cc dd: apply xx repeats of 4-row pattern aa/bb/cc/dd
FF: column break
```

Note the many "import" commands. These copy bits from other bitplanes, with
a possible bitwise tweak. Since the stream is processed in order, these imports
won't try to fetch bits that haven't yet been unpacked from the current stream;
plane 0 won't try to copy the current image's plane 2, for example.

However, some images are drawn on top of other images, in which case any planes
in the base image can be copied at any time. The "skip" commands likewise
leave the base image visible, in effect importing all bits from the base.
There's no indication in the image files of which images are drawn on top of
which other ones - you just have to hardcode appropriate image pairs.
For example, if you have an "H23B" image without a palette, it probably goes
over an "H23" base image.

The original implementation presumably maintained a 640x400 screen buffer and
image decompression (and bit imports) were acting directly on the buffer.
A modern decompressor may want to keep such a working buffer as well and unpack
images on top of each other without clearing the buffer.


##### 16px column mode (bitflag $40 not set)

Since 16px images may need to be drawn on top of 8px images, it may be best
to unpack these 16px columns as two adjacent 8px columns.

The bytes are actually reversed when reading data. These are listed here flipped
because it's the second byte (high byte in a little-endian word) that defines the
output action.

```
00 FF: end of plane?
0x ab: x repeats of 2-row pattern bb-bb aa-aa (up to $7F, 127 repeats)
8x ab: x repeats of 4-row rotated pattern, eg. A1 -> 1111 AAAA 4444 AAAA (up to $BF)
Cx yy: yy rows of xx-xx
D0 yy: yy direct byte pairs (up to $D1-FF for 511 byte pairs)
D2 yy: skip yy rows (show base image here)
D3 yy: copy yy rows from 16 rows ago
D4 yy: copy yy rows from 12 rows ago
D5 yy: copy yy rows from 8 rows ago
D6 yy: copy yy rows from 4 rows ago
D7 yy: copy yy rows from 2 rows ago
D8 yy: copy yy rows from 1 row ago
D9 yy: copy yy rows from previous column, 8 rows ago
DA yy: copy yy rows from previous column, 4 rows ago
DB yy: copy yy rows from previous column, 2 rows ago
DC yy: copy yy rows from previous column, 1 row ago
DD yy: copy yy rows from previous column
DE yy: copy yy rows from previous column, 1 row ahead
DF yy: copy yy rows from previous column, 2 rows ahead
E0 yy: copy yy rows from previous column, 4 rows ahead
E1 yy: copy yy rows from previous column, 8 rows ahead
E2 yy: copy yy rows from 2 columns ago, 8 rows ago
E3 yy: copy yy rows from 2 columns ago, 4 rows ago
E4 yy: copy yy rows from 2 columns ago, 2 rows ago
E5 yy: copy yy rows from 2 columns ago, 1 row ago
E6 yy: copy yy rows from 2 columns ago
E7 yy: copy yy rows from 2 columns ago, 1 row ahead
E8 yy: copy yy rows from 2 columns ago, 2 rows ahead
E9 yy: copy yy rows from 2 columns ago, 4 rows ahead
EA yy: copy yy rows from 2 columns ago, 8 rows ahead
EB yy: copy yy rows from 3 columns ago, 4 rows ago
EC yy: copy yy rows from 3 columns ago, 2 rows ago
ED yy: copy yy rows from 3 columns ago, 1 row ago
EE yy: copy yy rows from 3 columns ago
EF yy: copy yy rows from 3 columns ago, 1 row ahead
F0 yy: copy yy rows from 3 columns ago, 2 rows ahead
F1 yy: copy yy rows from 3 columns ago, 4 rows ahead
F2 yy: copy yy rows from 4 columns ago
F3 yy: copy yy rows from bitplane 0 (blue)
F4 yy: copy yy rows from bitplane 1
F5 yy: copy yy rows from bitplane 2
F6 yy: copy yy rows from plane 0, inverted
F7 yy: copy from plane 1, inverted
F8 yy: copy from plane 2, inverted
F9 yy: plane 0 AND plane 1
FA yy: plane 0 AND plane 2
FB yy: plane 1 AND plane 2
FC yy ab-cd: yy rows of ab-cd
FD 0x aa-bb: x repeats of 2-row pattern aaaa bbbb
FD 8x ab-cd ef-gh: x repeats of 2-row pattern ab-cd ef-gh
FE 4x ab-cd: x repeats of 4-row pattern bbbb aaaa dddd cccc
FE Cx ab-cd ef-gh ij-kl mn-op: x repeats of 4-row pattern ab-cd ef-gh ij-kl mn-op
FF-xx: end column, ignore xx
```

Additional cautions about the 16px column mode...

The image display offset is still a multiple of 8. If the value is not a multiple
of 16, then there's a single 8px mode column before doing the rest in 16px mode.
Likewise, if the image's right edge isn't at a 16px boundary, the last column is
in 8px mode. If an image in 16px mode only has a width of 8 or 16 pixels, then
it's even possible the whole thing is in one or two 8px mode columns anyway.

All commands in this mode are byte pairs, and must be word-aligned. So if a column
begins at an odd byte offset, skip the first byte to become word-aligned. This can
happen after an 8px first or last column.


### DAD archive

First dword: sig "DAD" and a null. Then a series of concatenated DA1 files, each
starting with the "DA1" sig, a null byte, and a size dword.

Can also contain several "BRG" (plus null byte) entries, which are always 24 bytes
after the sig and don't have a size dword. This is a palette, stored the same way
as DA1's palette - 3 nibbles per index and BRG order. Unsure when these are used...

Rabyni mystery: D.FA1 038.DAD has several DA1's with further data past image end,
going by given frame size, why?
