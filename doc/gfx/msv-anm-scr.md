MSV, ANM, SCR images
--------------------

Image formats used by Silence/Sogna. On the PC98, these are typically further
compressed into SZH files, but the first Guyna-Rock game for example uses plain
MSV files.

The ANM variant used on Sogna's Windows games is described after the other formats.

Source code for reading these is in [/inc/gfx/msv.pas](../../inc/gfx/msv.pas).

An encoder is also available at [https://mooncore.eu/filus/makemsv.py].


### Footer

These PC98 image formats have a recognisable footer that should be read first.
Note that the stored palette omits colors 0 (always black) and 15 (always white)
to save a few bytes. The footer normally only appears once, but there's at least
one file where it appears twice. It's probably fine to just read the final one.

Words are in x86 little-endian order.

Footer (last 46 bytes in file):
```
byte - image format type, see below
byte[14*3] - palette values for color 1 to 14, in RGB order, low nibbles only
char[3] - "RGB"
```

The compression algorithm is broadly identified by the file suffix, but more precisely
by the format byte from the footer:

- 0 = MSV
- 1 = ANM
- 2 = ANM, different somehow?
- 3 = SCR
- 4 = SCR, single-bitplane 1bpp
- 5 = ANM, same as 1, but frames may be larger than the viewport (scrollable)
- 6 = ANM, same as 1, but with transparency mask and extra-wide frames?
- 7 = ANM, same as 6, but extra-tall frames?

All frames in the file will be in the same format.


### Header

There are two possible headers - single-frame and multi-frame. Usually MSV and SCR
images have a single-frame header, and ANM a multi-frame one, but sometimes there's
a multi-frame header even though the file only has one frame defined in there.
It's probably safest to try to load the file assuming a multi-frame header first,
and fall back to the single-frame header if needed.


##### Single-frame header

```
uint16 - image pixel width, x8
uint16 - image pixel height
```

If pixel width is 0, it's actually an alternative extended header:
```
uint16 - false pixel width, 0, indicates alt header
uint16 - frame start offset, where x (column) = offset % 80, y (row) = offset / 80
uint16 - true frame pixel width, x8
uint16 - frame pixel height
```

The compressed image data directly follows the header.


##### Multi-frame header

```
byte[128] - array of frame indexes, 1-based; the largest number found is the count of
	frames in this file; padded with 0's after the last valid index
uint32[framecount] - array of frame start offsets
```

The array of frame indexes is also the animation sequence, but it's usually overridden
by the game script, so you only need to pick up the highest index number. The index
numbers are usually presented in strictly ascending order, but in a few files they can
repeat and descend.

From file offset 0x80, there's a 4-byte frame data start offset for each frame in the
file. The first frame start offset should be right after this array. The offsets are
encoded in segment:offset style, a uint16 pair:

	file offset = (first uint16) * 16 + (second uint16)

Each frame start offset points to a single-frame header, directly followed by that
frame's compressed image data.


### Format 0

- 8px columns, 16-color
- Columns unpacked in normal vertical order
- Columns appear in groups of 4, one for each bitplane
- XOR output by previous byte, RLE-encoded runs
- Repeats can cross columns
- Transparency is not supported

Decompression:
```
For each 8px column in the image:
	For each bitplane:
		Output next input byte as a literal, the first output byte of any column
		(unless in the middle of a repeat, in which case just continue repeating)

		While bitplane column not complete:
			b := next input byte
			If b == previous input byte:
				repeat_count := next input byte
				If repeat_count == 0:
					repeat_count := 256 + next input byte
				For repeat_count times:
					Output (b XOR previous output byte)
					If reached end of column in this bitplane,
					go to top of next column and keep repeating

			Else, b != previous input byte:
				Output (b XOR previous output byte) as a literal
```

While outputting repeats, the value is repeatedly XORred. Therefore, any 2-row
pixel pattern can be run-length-repeated. Example checkerboard pattern:

- Input stream: 55 FF FF 02
- 1st byte is output as a literal: output 55 (0101 0101)
- 2nd byte FF, xor with 55: output AA (1010 1010)
- 3rd byte FF starts a repeat for 02 rows, xor with FF each time: 55, AA


### Format 2

- 8px columns, monochrome, only a single bitplane
- Columns unpacked in normal vertical order
- XOR output by previous byte, RLE-encoded runs

The decompression algorithm is the same as format 0, except instead of having groups
of four 1bpp columns to make each final 4bpp 8px column, every decoded column now
goes straight to a final 1bpp result.


### Format 3 and 4

- 8px columns, 16-color
- Uncompressed bitmap rows in normal scanline order
- Each row is repeated once for each bitplane, 4 times in format 3
- Format 4 is a directly displayable 1bpp bitmap
- No transparency

That is, the row layout in format 3 (bitplanes b, g, r, i):
```
+-----------------
| b0 b0 b0 b0 ...  1st row, 1st bitplane
+-----------------
| g0 g0 g0 g0 ...
+-----------------
| r0 r0 r0 r0 ...
+-----------------
| i0 i0 i0 i0 ...
+-----------------
| b1 b1 b1 b1 ...  2nd row, 1st bitplane
+-----------------
| ...
```

Decompression:
```
For each row in the image:
	For each bitplane:
		Read image pixel width / 8 bytes, output those directly
	If there was more than 1 bitplane, combine the bitplane rows to a full 4bpp row
```


### Format 1 and 5

Typically used in multi-frame images. The animation speed and whether it loops are
controlled by the game script, not specified in the file. The screen position for the
graphic is also controlled by the script, but each frame can come with an extended
header that adds a location offset for that frame.

The compression goes back to 8px columns, but instead of one full column for one
bitplane at a time, all bitplanes are now interlaced in 4-byte values. That is, the
first 4 bytes are the topmost 8px row in the first column; the second 4 bytes are
the second 8px row in the first column, and so on.

For example:
- Input value: `11030509`
- Input in binary: `00010001 00000011 00000101 00001001`
- The first byte is 8 bits in the first bitplane, the second byte 8 bits in the second, etc
- Take the first bit of each bitplane to make the first full pixel, then the second bit etc
- Full 8 pixels, binary (least-significant bit first): `0000 0000 0000 1000 0001 0010 0100 1111`
- Full 8 pixels 4bpp output: `0 0 0 1 8 4 2 F`

If the same input value is immediately repeated, that indicates an RLE run: the next
input byte specifies how many rows to fill with this input value. Example:
- Input values: `12341234 12341234 07`
- This will output the 8px 4bpp equivalent of `12341234` a total of 1+7 times

The exception to that is that the first input value in a column is always a single
literal, even if identical to the previous input value. Also, if repeat count is 0,
read another byte and add 256 to that to get the true repeat count. If a third
identical input value follows, that doesn't trigger a repeat; a fourth one again does.

Decompressing:
```
While image not complete:
	current_value := Read next 4 input bytes
	If current_value != previous_value, or this is the first row of a column:
		Output current_value as a literal 8 pixels
	Else:
		repeat_count := Read next input byte
		If repeat_count == 0:
			repeat_count := 256 + next input byte
		Output current_value repeat_count times
	previous_value := current_value
```


##### Format 6 and 7

This introduces transparency masking into the algorithm. This is mainly used for
character sprites appearing over a background.

The algorithm is much the same as format 1, except there are now 5 input bytes
for each 8px row. The fifth byte is an alpha mask, where a set bit is opaque,
clear bit is transparent. 0x80 applies to the leftmost pixel in the row, 0x01
to the rightmost.

As before, if the same input value is repeated (all 5 bytes!), there's an RLE
repeat byte or two.

Formats 6 and 7 look identical, but possibly one is meant for particularly
wide images, the other for tall images.

Note: Sometimes the bitplane bytes have 0xFF values even though the alpha mask is 0.
This is irrelevant if you just want to convert the image to a raw bitmap, but the
extra values appear to be required by the original engine. If you want to encode
these ANM files, you may have to do this:
```
current_value (uint32) = Read next 8-pixel row in the current column
current_mask (byte) = One bit for each 8 pixels in the row; 1=opaque, 0=transparent
For each transparent bit in current_mask:
	Set the corresponding bit in some or all bytes of current_value to 1
```

It's not quite clear which bytes should get masked; sometimes it's just the 3rd
and 4th byte, sometimes all of them. Look at what the original file did and try
to mask in the same way, I guess.


### Exceptions

##### Guyna-Rock 1

- The MSV files have a footer but omit the format byte; these are always format 0
- Images that begin with M have an unspecified number of frames inside but no
  list of start offsets; after decompressing a frame, there may be an additional
  frame if there's still plenty of file data remaining and
  * the next two words look like a valid frame size, or
  * the frame's palette immediately follows, terminating with "RGB", and then the
    next two words look like a valid frame size, or
  * the two words at the next 16-byte boundary look like a valid frame size
- Frames can come in different sizes in the same file
- The ending credit graphics ED*.MSV don't have a footer at all; they probably
  share the palette with MED.MSV, orange text on pink background
- The frame image GNM also doesn't have a footer
- Palette 15 is only white in the MOP* images; in the rest it's a bright red,
  used for the viewframe corner title

##### Minimum Guyna-Rock

The same exceptions as above can still apply in this game. Everything still in format 0.


##### Cyber Arms Val-Kaizer

Same as above, all still in format 0. Although now palette 15 is a light grey, and 0
is a dark grey, both used in the viewframe.


ANM on Windows
--------------

These are used in Sogna's Windows games, which is mostly the Viper series.
Grapholic can view these easily.

Source code for reading these: Decomp_SognaANM in [/inc/gfx/msv.pas](../../inc/gfx/msv.pas).

Header:
```
byte[768] - palette, 256 byte triplets, BGR order
uint32 - framecount, must be 1 or more
uint32[] - start byte offset for each frame, relative to 0x304
```

For some reason, the first 33 bytes in the palette always seem to be zeroes.
That's the first 11 palette indexes. Perhaps they're fixed interface colors.
If the image has a transparent color, it is likely palette index 12.

Each frame:
```
uint16 - image X offset
uint16 - image Y offset
uint16 - image pixel width, must be a multiple of 4?
uint16 - image pixel height
byte[] - compressed data
```

The image data is comprises literals and basic RLE. It's a 256-color indexed format,
so there is one byte per pixel.

The data is stored in 4px columns, so 4 bytes at a time. The entire leftmost 4px column
is stored first, then the next 4px column, and so on.

Pixels are output as literals by default, but if the same pixel quad is immediately
repeated, that signals an RLE repeat. RLE runs can't cross column boundaries, and can't
start from the topmost row of the image. After an RLE run, the next 4 bytes are always
a literal, even if they are a repeat of the previous 4 bytes.

Decompression pseudocode:
```
While image not complete:
	q := Read next 4 bytes from input stream
	If q == previous 4 bytes from input stream,
	And this is not the first row of a column,
	And the previous 4 bytes were output as a literal, not as a repeat:
		copy_length := Read next byte from input stream
		If copy_length == 0:
			copy_length := 256 + Read next byte from input stream
		Output q copy_length times
	Else:
		Output q as a literal 4px group
```

When multiple frames are present, they are usually standalone pieces that get composited
together, but not necessarily all for the same animated thing. In a few cases, frames are
saved as a pure delta from the previous frame, like a GIF animation. Different frames in
the same file can have wildly different sizes and offsets.
