MRS/VRS graphics
----------------

These are used in DO, Foster, and Zyx games, and Parsley's True Love.

Source code for reading these is in [/inc/gfx/mrs.pas](../../inc/gfx/mrs.pas).
@MindrustUK helped figure these out, thank you!

Header: (little-endian words)
```
uint16 - signature "CD" or "DO"
uint16 - unused 0000
uint16 - image width
uint16 - image height
uint32 - unknown
byte[768] - 256-color palette in BRG order
```

Image data follows the header immediately, at offset 780 (0x30C).
Standard scanline pixel order, compressed using RLE and backward reference copy.

To decompress:

```
While image not complete:
	i := next input byte
	case (i & 0xC0) of
	0x00:
		if i == 0 then len := next input byte + 64, else len := i
		output len input bytes as literals
	0x40:
		len := i & 0x3F
		if len == 0 then len := next input byte + 64
		repeat the last output byte (len + 1) times
	else:
		dist := ((i & 0x0F) << 8) + next input byte + 1
		len := (i >> 4) & 0x07
		if len == 0 then len := next input byte + 8
		copy and output (len + 2) bytes from dist bytes ago
```


XMG graphics
------------

These are used in ZyX games Gakuen Bakuretsu Tenkousei, Ring Out, Twilight Hotel,
Let's Pirates Trouble Vacance.

These are the same as MRS, except the header, which is encrypted, and after
decrypting the header signature is just 0000, or "XX" (5858).

To decrypt the 780 header bytes:
```
key := 0
For each byte:
	header byte := ((header byte - key) & 0xFF) xor 0xF3
	key = (key + 7) & 0xFF
```
