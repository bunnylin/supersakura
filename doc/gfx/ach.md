ACH graphics
------------

Used by AIL.

The GALL*.dat files are graphics. MLD can show these if extracted individually,
and told to use -ach or -ach2 format.

Source code for reading these is in [/inc/gfx/bmp.pas](../../inc/gfx/bmp.pas).

After LZSS-unpacking, these graphics have a small header, followed by optional
palette data, then the uncompressed image bitmap.

Header: (words are x86-native little-endian)
```
uint16 - image pixel width
uint16 - image pixel height
```

The sprites in this game are drawn over the viewframe, effectively popping out
over the framed background. So all sprites are in a 400px-high context, and
their X offset is probably controlled by script.

A palette is included for all non-sprite images, but it's not quite clear from
the header whether an image is a sprite or not. Guesstimate from pixel width?

If the palette is present, it follows immediately after the image pixel height.
There is one nibble per color component, in the order `GR xB xx` and `xx Rx BG`.
The total 16-color palette is packed into 24 bytes.
Example: `12 34 56` would be the RGB colors `214` and `365`.

The palette is also directly followed by another 24 bytes, some kind of palette mask.

After this the uncompressed image data follows in scanline order. Each full row is
stored as four 1bpp rows, one for each bitplane.

Note: There are some files like Kyouhaku gall2a.6 where the file doesn't look big
enough to accommodate an image of the right size. LZSS problem or something else?..
