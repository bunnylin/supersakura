JAST graphics
-------------

### GRA files

These are mostly in the Pi format, a few in the MAG format.
My specifications for both:
- https://mooncore.eu/bunny/txt/makichan.htm
- https://mooncore.eu/bunny/txt/pi-pic.htm

Japanese specification for Maki version 1 A and B:
http://www.jisyo.com/viewer/faq/maki_tech.htm

Japanese specification for MAG version 2:
http://www.jisyo.com/viewer/faq/mag_tech.htm

In Tenshitachi no Gogo collection 1, there are actually three different
versions of .GRA files. Most are Pi, some are MAG, and the remaining few
are sad, uncompressed TIFF bitmaps.

The MAG files in question are T2_59 to T2_85, T3_67 to T3_74, and A17 to A40.
The images are all either identical copies or lower quality versions of other
images already stored in the advanced format. Why are they included at all?
The packaging for this game must have been an unusually quick dash job.

Each GRA file that uses the Pi format starts with a two-byte signature, then
a meta-data string, which terminates with the byte 0x1A. The metadata is mostly
junk, except for animation data stored there by OVL v3 games. The signature is
either "Pi", or 00 00 in an attempt to mask the filetype.

All animation data in OVL v3 .GRA files only uses the top nibbles of the bytes,
to avoid accidentally printing 0x1A. To put together values in the string:
read bytes in pairs, shift the second byte right by four bits, and OR them.

OVL v2 games keep animation data hardcoded inside the executable, using
whole bytes, so no nibble concatenation is needed. For example:
- 3sis (DOS) J_ADV1.EXE: look in address 0x15AD0 for the first, ST01A0.
- Runaway (DOS) MEI.EXE: check out 0x162B0 for op_013a0.
- Vanish (PC98) MAIN.COM: starts at 0x2804 with VT_001A0.

Keep reading animation records until you encounter zeroes instead of a proper
sequence length or file name.

Even older engine versions may not have easily accessible animation data anywhere.
Then the frames must be generated automagically or by hand.

The animation
```
Offset Type              Description
------ ----------------  -----------
0 .... uint16 .........  Length of animation sequence,
                         max 32 [frame number:delay] pairs
2 .... char[36] .......  Zero-padded filename without the .GRA extension,
                         sometimes incorrectly spelled...
38 ... uint16 .........  The entire graphic's width divided by eight;
                         The real width is this * 8.
40 ... uint16 .........  The entire graphic's height
42 ... uint16 .........  The animation's screen location, X coordinate,
                         divided by eight
44 ... uint16 .........  The animation's screen location, Y coordinate
46 ... uint16 .........  Width of the animation frames, divided by eight
48 ... uint16 .........  Height of the animation frames
50 ... uint16[32] .....  The animation's frame sequence. Frames beyond
                         sequence length [from 00] are garbage. While
                         showing the animation, once you reach the
                         sequence length, loop back to zero.
114 .. uint16[32] .....  The animation's timing. Each uint16 is the number
                         of ticks to wait before drawing the
                         corresponding frame. There are about 80 ticks
                         to a second.
------ ----------------  -----------
Total size: 178 bytes
```

Yukineko, Tenkousei, AngelsC1 have animation data embedded in the executable,
but in a mildly different way. The file name char[36] array at offset 2 is
instead replaced by a uint[32] array. The first word is an offset from
a baseline address, pointing to the animation's zero-terminated filename.
The rest of the word array is probably garbage. This pushes the rest of the
header forward, so the width at offset 38 is now at 66 (total structure length 206).

The baseline address for animation names is a dword of zeroes just before
a Borland C++ copyright notice. The names are all stored after the end of the
animation records. You could keep reading records until the sequence length
gets bigger than 0xFF or is 0, or the name pointed to is empty; and then read
all the names sequentially.

- Yukineko: the animation records start at 0x15816; there are 52 records.
- Tenkousei: TK.EXE the animation records start at 0x159CC; about 70 records.
- AngelsC1: TEN_S.EXE has animation records from 0x129A0, 0x13680, and 0x14360.

All frames are stored beside and on top of each other in the image. The
sequence pointer indicates which to draw. The frames are usually arranged in
a vertical sequence, but occasionally there are also frames side by side.
The correct frame enumeration is:
```
+-----+-----+-----+    +-----+-----+-----+
|  0  |  3  |  6  | or |  0  |  1  |  2  |
+-----+-----+-----+    +-----+-----+-----+
|  1  |  4  |  7  |
+-----+-----+-----+
|  2  |  5  |  8  |
+-----+-----+-----+
```

After the byte 1A concludes the metadata string, next is the actual header.
There is often a zero-terminated string of additional junk between the 1A
marker and the start of the header block.

To recap: Parse animation data if it exists, up to a 1A marker; then find the
first 00 that follows, which is the first byte of the header.

The Pi format uses a header like this:

```
Offset  Type      Description
------  --------  -----------
0       byte      Start of header, always 00.
1       byte      Mode byte. Almost always 00, but can be FF; ignore.
2       byte      Screen ratio X, always 00.
3       byte      Screen ratio Y, always 00.
4       byte      Bitdepth. Usually 4 or FF for 16 colors, could be 8 for 256.
5       char[4]   Compressor model, eg. JAST, Magd, PC98 or "MPS "...
9       uint16    Length of model-specific data, MSB first. Almost always 0,
                  except in [Eden] OP_TT and [Yukineko] SH_26A0.
11      uint16    Image width, MSB first, [01 E0] = 480
13      uint16    Image height, MSB first, [01 28] = 296
15      byte[48]  Palette: 16 byte triplets, RGB. See below.
63                Compressed image data starts here
------  --------  -----------
```

Although the palette is presented as 3 bytes per index, how this is
interpreted depends on the system. PC98 systems of this era could use
a palette of 16 colors from 4096 possible, or 4 bits per channel, so only the
top nibble actually counts. Anex86 confirms this, drawing the graphics in
each game with the bottom nibble as a copy of the top nibble, xor 1.
(I don't know what the xor 1 is for, Anex bug?)

The localised ports target VGA graphics or better, where mode 12 can do
640x400 with 16 colors from 262144 possible, or 6 bits per channel. I think
the last loaded graphic defines the entire screen's palette, but I haven't
run into any deliberate instances of this. The relevant Dos and Windows
engines however screw up the lower nibble, either always zeroing the whole
nibble or zeroing bits 2 and 3, so what should be FF becomes F0 or F3.
(Or this could be a Dosbox bug.)

Since the PC98 versions are original, the palette should always be
interpreted on that basis, with the bottom nibble set equal to the top.

There appears to be no definition of transparency in the .GRA files. Instead,
graphic loading commands in bytecode specify whether to treat each image as
transparent or not. Transparent images probably need to be named in the
decoder by hand, assuming the graphics decoder is kept separate from the
script decompiler.

If transparency is used, in Jast/Tiare games it is nearly always mapped to
color 8, sometimes 15.

Again, a full algorithm description for Pi graphics is separately on my site.

[Majokko] MT05KF appears to have an unremovable solid background. Floodfill?
However, it is never actually used in the game, so no one ever noticed.
A pity, she looks cute here... ^^;


##### GRA files that are actually TIFF

You find some of these in Tenshitachi no Gogo Collection 1, files `A1` to `A16`.
These are slightly non-standard TIFF files and aren't used by the game anyway.
Most of the rather robust header is not relevant for our purposes...

```
Offset  Type     Description
------  -------  -----------
0       char[4]  Signature "II*" and a null (actually a byte-order marker and version)
30      dword    Image width
42      dword    Image height
------  -------  -----------
```

The palette is stored from offset 0x100. Color values are saved as uint16's, first
16 red values, then 16 green values, then 16 blue values.

At 0x200 begins a sequence of nibbles that looks like an uncompressed 4bpp bitmap.
For each byte, the low nibble is the left pixel, the high nibble the right.


##### Remaining problems:

[Deep]
DB_04_09 image corrupted
DB_05_03 image corrupted
DB_05_08 breaks the decoder...
DB_05_09 image corrupted
DB_06_05 image corrupted
DB_06_08 image totally corrupted
Interestingly, these appear equally broken in Grapholic. A bad rip?

There seems to be an unreasonably big repetition code which may have special
significance. A reset of some kind, maybe. To fix, play the game in an emu
until you find one of those images and see if it's still broken.

If it's a bad rip, the long string of 1's may be a bad sector or something.
In that case, the data will have to be reconstructed with brute force...

[Yukineko]
Some images are effectively empty. The graphic files themselves are valid and
don't have image data. But these graphics are used in places where you'd
expect to see something. Censorship? Visual override? Unfinished art? Check
the original engine.
```
SB_18 not used
SB_37 not used
SH_06 in SMG_S028
SH_32 in SMG_S075
SH_33 in SMG_S076
SH_34 in SMG_S077
SH_37 in SMG_S080
```


### IMG images

These were used in games from Cosmos Club to Gomen ne Angel. There are two variants
of this format, and the later BRGE format could be seen as a third variant.


##### IMG v1

This was only used in Cosmos Club and Tenshitachi no Gogo 3.

- Default 8-color palette
- Pixels are double-height (640x200 resolution)
- Image can be saved in either scanline or 8px column order
- Rows in columns can be interlaced, but not in a useful way
- Basic RLE compression
- No transparency

The image is saved as 3 bitplanes, in the normal order (BRG).

Header:


##### IMG v2

This was used in Yoru no Tenshitachi, Tenshitachi no Gogo 3 Bangaihen, Tenshitachi
no Gogo 4, and Gomen ne Angel. Basically the same as IMG v1, but updated to
theoretically support 16-color images, even though in practice they all still use
the default 8-color palette.

This has a different header:


### BRGE images

These were used in Tenshitachi no Gogo 3 Hanseiban, 5, and Special 2.
Decompression code for these is in (/inc/gfx/jast-img-brge.pas)[../../inc/gfx/jast-img-brge.pas].

- 16 colors, but palette not defined in files?
- Scanline order
- Image dimensions are not stored in the files either!?
- Images are split into 4 separate files, one for each bitplane
- Basic RLE compression, same algorithm as the above IMG formats

If a bitplane is stored uncompressed, the file suffixes are `.B`, `.R`, `.G`, and `.E`.
If they are compressed, the suffixes are `.BC`, `.RC`, `.GC`, and `.EC`.

Animated image dimensions can be read from specific `bin` files.

For non-animated images, image dimensions can be reliably derived from the total pixel
count or byte size, since these games only have a few different image sizes. The
uncompressed bitplane files should all be exactly the same size, but in practice some
may have up to a couple hundred bytes of garbage at the tail end, so allow some leeway.

Here's a list of dimensions depending on the uncompressed blue plane byte size:
- 2408 bytes: 344x56
- 2944 bytes: 368x64
- 3072 bytes: 128x192
- 3264 bytes: 408x64
- 3968 bytes: 128x248
- 4160 bytes: 520x64
- 4400 bytes: 400x88
- 8160 bytes: 480x136
- 8664 bytes: 456x152
- 16800 bytes: 480x280
- 32000 bytes: 640x400

Exception: Angels3H s18 may have a ton of garbage, but loads fine as a 480x280.


###### Tenshitachi no Gogo 3 Bangaihen Hanseiban, and Special 2

For these two games, the scene graphics are specified in `haseetbl.bin` and `spseetbl.bin`
respectively. Both use the same structure.

```
char[3] - script filename without suffix
char[3] - scene graphic filename without suffix
byte - transition style
byte - number of animated things in scene, can be from 0 to 3
char[3] - animated graphic filename without suffix, or nulls if not used
byte - animated graphic total pixel width, divided by 8
byte - animated graphic total pixel height, divided by 8
byte[7] - data for animation 1, or nulls
byte[7] - data for animation 2, or nulls
byte[7] - data for animation 3, or nulls
```

Animation data structure:
```
byte - screen position x, divided by 8
byte - screen position y, divided by 8
byte - frame pixel width, divided by 8
byte - frame pixel height, divided by 8
byte[3] - frame delays
```

Basically all animations are either eyes or a mouth being half-open, closed, and fully open.
All animations have 3 equal-sized frames, stored horizontally in the graphic. If the
graphic has more than one animation, subsequent ones form a new row of frames each.
When displayed, the frame order ping-pongs between open and closed, always in the order
0, 1, 0, 2, with typically the longest delay after frame 2, eyes open for a while.


###### Tenshitachi no Gogo 5

The graphics for each scene are specified in `seentbl.bin`. However, this game has no
animations, so there's no relevant information for image conversion here.


##### BRGE palette

The palette is always fixed with these constant colors and game-specific colors:
- 0: black
- F: white


###### Tenshitachi no Gogo 3 Bangaihen Hanseiban palette

- 1: #05F blue
- 2: #F03 red
- 3: #A4F violet
- 4: #6FB bright green-blue
- 5: #5EF bright turquoise
- 6: #FF5 yellow
- 7: #F80 orange
- 8: #777 dark grey
- 9: #00A dark blue
- A: #930 redwood
- B: #F6D hot pink
- C: #0B5 green
- D: #FA9 peach
- E: #FCB light skin

###### Tenshitachi no Gogo 5 palette

- 1: #009 dark blue
- 2: #F00 red
- 3: #C4F violet
- 4: #2F2 neon green
- 5: #0FF bright turquoise
- 6: #FF0 yellow
- 7: #4CD cyan
- 8: #777 dark grey
- 9: #00D medium blue
- A: #A00 dark red
- B: #F0E fuchsia
- C: #171 pine green
- D: #FA9 peach
- E: #FCB light skin

###### Tenshitachi no Gogo Special 2 palette

- 1: #06E blue
- 2: #A8F dark lilac
- 3: #0A9 dark cyan
- 4: #788 dark grey
- 5: #0FF bright turquoise
- 6: #FAF pink
- 7: #FF4 yellow
- 8: #E05 red
- 9: #A50 brown
- A: #F98 pale strawberry
- B: #FBA peach
- C: #FDC light skin
- D: #5D8 blue-green
- E: #50A deep purple


### ANI

Uncompressed multiframe images.
