Excellents graphics formats
---------------------------

Except for the CGL format, studios affiliated with Excellents stuck with Pi variants.
Pi compression is documented on my site: https://mooncore.eu/bunny/txt/pi-pic.htm


### CGL images

These use mostly RLE compression, with some custom codes for common pixel patterns.
Source code for reading these is in [/inc/gfx/cgl.pas](../../inc/gfx/cgl.pas).

Header: (words are in x86-native little-endian order)
```
byte[2] - signature: 0F 04, no obvious meaning to these specific values
uint8[24] - palette, one nibble each, order RGB; bitwise rrrrgggg bbbbrrrr ggggbbbb...
byte - image width in 8px columns
uint16 - image pixel height
```

Lixus doesn't appear to use transparency in graphics, so all colors are always opaque.

The compressed image data immediately follows the header.

The image is compressed as a long series of byte codes. The image is split into four
bitplanes, in the standard order `1 2 4 8`. Each bitplane in turn is encoded as 8px-wide
columns, so each output byte represents 8 1-bit pixels side by side, then the next byte
represents the next 8 1-bit pixels under that, and so on.

- Repeat codes can cross columns, but can't cross bitplanes.
- There is no end-of-column marker but there is an end-of-plane marker.
- It's possible to copy from the previous column, but not from further back than that.

Codes:

`00`

End of plane marker, no output.

`20`

Unclear what exactly this does, best seen in CG123, but occurs in a few other files,
always before the terminator. Seems safe to ignore, or output a single 0 byte.

`21..3F ...`

Output the next x bytes as literals. X is this code & 0x1F.

`40 00 xxyy aa`

Output `aa` for `xxyy` times.

`40 xx aa`

Output `aa` for `xx` times.

`4x aa`

Output `aa` for `x` times.

`50 xx ab cd ...`

Output nibble literals for `xx` bytes. Each input nibble is output as the top nibble,
with the bottom nibble zeroed out. Each input byte has up to 2 nibbles, so if `xx` is 5,
there are 3 input bytes, with the bottom nibble of the last byte ignored.

Example: `50 07 AB CD EF 69`

This would output: `A0 B0 C0 D0 E0 F0 60`

`51..53 ab ...`

Same as above, but only output 1-3 nibbles (code & 0x03).

`54..57 ...`

Same effect as `50..53`, but all bottom nibbles are output as F instead of 0.

Example: `57 AB CD`

This would output: `AF BF CF`

`58..5B ...`

Same effect as `50..53`, but each input nibble is output as the bottom nibble, with
output top nibbles zeroed out.

Example: `59 AB`

This would output: `0A`

`5C..5F ...`

Same effect as `58..5B`, but all top nibbles are output as F instead of 0.

`60`

Copy 1 byte from this row in the previous column.

`64`

Copy 1 byte from 2 rows ago in the current column.

`68`

Copy 1 byte from 1 row ahead in the previous column.

`6F`

Copy 1 byte from 1 row ago in the previous column.

`70 xx`

Copy `xx` bytes from the previous column following the current row.

`7x`

Copy `x` bytes from the previous column following the current row.

`80 aa bb xx`

Output `aa bb` byte pair for `xx` times.

`9a xx`

Output patterned byte pair for `xx` times. The byte pair is built bitwise from `a`,
repeating its first 2 bits for the first byte, and its second 2 bits for the second byte.
For example, `99 04` outputs `AA 55` for 4 times. (9 = 1001; 10101010 = AA, 01010101 = 55)

`A0 xx aa`

Output rotated byte pair for `xx` times. The byte pair is `aa` and `aa` rotated right by 1.
For example, `A0 05 33` outputs `33 81` for 5 times.

`A8 xx aa`

Same as `A0`, but rotate right by 2.

`Ax aa`

Same as `A0` and `A8`, but only output the byte pair `x & 0x07` times. For codes `A1..A7`,
rotate right by 1, for codes above `A8` rotate right by 2.

`B0..BF ...`

Same as `A0..AF`, but rotate left.

`C0..C3 aa xx`

Output byte pair `xx` times. The byte pair depends on the code:
- `C0` outputs `aa` and `aa XOR 0x01`
- `C1` outputs `aa` and `aa XOR 0x02`
- `C2` outputs `aa` and `aa XOR 0x40`
- `C3` outputs `aa` and `aa XOR 0x80`

`D0..D3 xx`

Output a constant literal byte pair `xx` times. The byte pair depends on the code:
- `D0` outputs `00 01`
- `D1` outputs `00 02`
- `D2` outputs `00 40`
- `D3` outputs `00 80`

`DC..DF xx`

Output a constant literal byte pair `xx` times. The byte pair depends on the code:
- `DC` outputs `FF FE`
- `DD` outputs `FF FD`
- `DE` outputs `FF BF`
- `DF` outputs `FF 7F`


##### CGL complications

Sometimes bitplanes are left one byte short. This only occurs when the plane ends with
a run of literals. Presumably this is an encoder bug. The final byte just doesn't exist
in the image file! In the majority of cases, outputting an extra 00 byte is fine, but
there are some graphics where the bottom right corner looks noticeably wrong.

Files that may need patching:
- cg07
- cg58
- cg68
- cg92*
- cg95
- cg109
- cg111
- cg112
- cg121
- cg124
- cg125
- jekao3
- mon_28c
- netsen1

Sakutool handles this by copying pixels from above the missing corner, slightly offset
to maintain any possible checkerboard dithering.

CG14 and CG42 look like they could have some mild compression errors too, but that
could also just be an artistic oversight. CG106 may also look severely broken, in which
case the file is actually damaged; a better disk image will fix that.

Various graphics have what looks like a 32-pixel spacer, placed outside the meaningful
picture area. A leftover from the art process, perhaps safe to crop out...


### DPC images

These are found in Yuugiri.

Header: (words are in x86-native little-endian order)
```
uint16[16] - palette
uint16 - X pixel offset
uint16 - Y pixel offset
uint16 - pixel width
uint16 - pixel height
```

The palette is stored bitpacked similarly as Elf's GP4. The binary color layout in the
uint16 is `gggg0rrr r0bbbb0a`. If the `a` bit is set, the color is transparent.

Pi-compressed image data follows the header immediately.


### P images

These are found in Angel Night and Waku Waku Mahjong Panic 2.
Same as DPC images, but X offset and width are in 8px columns, not pixels.


### G images

These are found in the Gao Gao series, Mayclub, and Nocturnal Illusion.

Header: (words are in MSB-first order!)
```
uint16 - image pixel width
uint16 - image pixel height
bytes [4..31] = palette, 16 RGB values
```

Pi-compressed image data follows the header immediately.


##### G complications

There are some images in the Gao Gao series that are split across 2-3 files. These are
unusually tall images, so splitting them gets around 16-bit memory allocation limits.
However, these files have misleading dimensions in the header. The given width and height
are correct for decompressing the bitmap, but the bitmap looks wrong when rendered at
the given dimensions. Without moving any bitmap contents, redefine the width as 480px
instead of 640, and the height as the number of complete rows the same buffer now yields.

For example, a 640x400 image should be displayed as a 480x533 image. The extra partial
scanline should be discarded.

This solution fixes the following images:
- GaoGao1: RS023H, RS037H/L, RS037AH/L, RS073H/L, RS103H/L
- GaoGao2: PW004H/L, PW021H/L, PW056H/L, PW060H/L, PW104H/L
- GaoGao4: BG12H/L, E020H/L, E029H/L, E033H/L, E050H/L, E29BH/L,
  S028H/L, S030H/L, S034H/L, S040H/L

Gao Gao 3 doesn't appear to have problematic files like this for some reason.

Unfortunately there are 2 graphics in Gao Gao 2 that are an exception: PW042 and PW045.
These are split in 3 files each, and the partial bottom scanline of the top file must be
preserved, as the middle file continues that scanline directly.

To handle this, read and decompress the top and middle files of the graphic at the sizes
specified in their headers, then concatenate both bitmap buffers, and treat the result as
a 480x533 image. (Which makes this 3-way split rather pointless, since they clearly could
save an image of that size in a single file already.)

The bottom file in these triples works the same way as the others, starting with a full
scanline, and any extra at the bottom can be discarded.
