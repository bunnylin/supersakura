EFD images
----------

Used in Waku Waku Mahjong Panic! Words are x86-native little-endian.

These may be wrapped in basic LZSS compression. If so, the header is:
```
char[4] - signature "eLZ0"
uint32 - uncompressed size
```

The compressed data follows immediately.

If not compressed, or after uncompressing, the header is:
```
char[8] - signature "_eFD01" 00 0x1A
uint16 - image width in 8px columns
uint16 - image pixel height
byte[12] - unknown stuff
byte[16 * 3] - palette
```

The image data follows immediately from offset 0x48.

How is it compressed?..
