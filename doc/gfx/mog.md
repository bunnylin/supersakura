MOG image format
----------------

Used in ZyX games such as Hercequary, Gakuen Bakuretsu Tenkousei, Nekketsu,
Lightning Warrior Raidy, Ring Out, Twilight Hotel.

Grapholic calls these "gf" images. 16 colors.
