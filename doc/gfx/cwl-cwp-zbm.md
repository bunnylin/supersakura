ZBM, CWL, CWP graphics
----------------------

Used by Crowd.


##### ZBM graphics

Used in X-Change 1 and Tokimeki Check-In.
Already documented at https://github.com/niksaak/ae

These use generic Microsoft SZDD compression, LZSS with 16-byte window offset.
(Any installable files with names like DATA.DA_ probably use this algorithm.)

Header:

```
uint32 - sig "SZDD" 53 5A 44 44
uint32 - more sig? 88 F0 27 33
uint8 - compression mode, must be always 0x41
char - stashed last character in suffix, if any
uint32 - size when uncompressed
```

After LZ-decompressing, XOR 0xFF the first 100 bytes, and you have a standard BMP.


##### CWL graphics

Used by Doushin Same Heart, X-Change 2.
Already documented at:
https://github.com/morkt/GARbro/blob/master/ArcFormats/Crowd/ImageCWL.cs

Same SZDD compression as above. Then, the decompressed header:

```
0: char[28] - signature "cwd format  - version 1.00 -"
28: uint32[4] - unknown
44: sint32 - image width, encrypted
48: sint32 - image height, encrypted
52: uint8 - encryption key
```

To decrypt width and height: `value := value + encryption_key + 0x259A`

The 16bpp raw bitmap data follows immediately from offset 0x38.


##### CWP graphics

Used by Yin-Yang X-Change Alternative.
Already documented at:
http://vilevn.sourceforge.net/doxygen/ccrowd_8cpp_source.html
https://github.com/morkt/GARbro/blob/master/ArcFormats/Crowd/ImageCWP.cs
https://github.com/niksaak/ae/blob/master/src/format/graphics/AG_Crowd.pas

These are standard PNG files with some chunk and metadata cropped out.

Source code for reading these is in [/inc/gfx/png.pas](../../inc/gfx/png.pas).

Header:
```
0: uint32 - sig "CWDP" 43 57 44 50
4: uint32 - image width, big-endian
8: uint32 - image height, big-endian
12: uint8 - bitdepth
13: uint8 - colortype (true color, indexed, etc PNG types)
14: uint8 - compression method
15: uint8 - filter method
16: uint8 - interlace method
17: uint32 - CRC
```

The header bytes 4..20 are a standard PNG IHDR chunk, just without the
chunk length and signature dwords.

At 0x15, there's the first IDAT chunk's length as uint32, directly usable.

At 0x19, begin IDAT chunks until end of file, just without the first IDAT sig.
There will possibly be multiple IDAT chunks.

You'll need to round out the IHDR, insert the first IDAT sig, and append an IEND
chunk, to make a standard PNG.

Note that even though the image may claim to be true color with alpha, the alpha
channel is likely to be bogus and should be scrapped or ignored. Also, the red
and blue color channels may be the wrong way around and need correcting.
