PRS images
----------

Older Fairytale, Cocktail Soft, Game Technopolis image format.
There's a different PRS format by Micro Cabin described further below.

Source code for reading these is in [/inc/gfx/prs.pas](../../inc/gfx/prs.pas).

- Single 3bpp bitmap in each file
- Only allows the default max-contrast 8-color GRB palette
- Pixels are double-height (640x200 resolution)
- Supports transparency
- Pixel data is saved in 8px 1bpp columns
- Columns appear in groups of three, one for each bitplane

Standard palette colors 0..7:
- Black, blue, red, violet, green, cyan, yellow, white

File layout:
- Header
- Data bytes
- Command codes

Header:
```
0x00: uint8 - image pixel width x8
0x01: uint8 - image pixel height (double-height!)
0x02: uint8 - image X offset x8
0x03: uint8 - image Y offset (double-height!)
0x04: uint8 - low nibble: bitplane mask, high nibble: transparent color
0x05: uint16 - this + 8 is the start offset for command codes
0x07: uint8 - default pattern byte, often but not always both nibbles are equal
```

The transparent color value isn't always reliable. If it's 1..7, it's probably
correct; if it's 0, then color 0 may be transparent, or the game may decide
which color is transparent based on its script code.


##### Decompression

You'll need to read command codes from the last part of the file, and output
bytes accordingly, consuming data bytes as needed. Command codes have variable
bitlengths, and are read one bit at a time until a valid code is complete.
Bits are read from 0x01 upward. Valid codes are 0, 10, 110, 1110, and 1111.

Keep a 256-entry ring array of most recently used literals. Commands 0 and 1110 add
a single item to the array each time; other commands don't write to the array.
The array loops around. It must be inited to all 0, then start saving literals from
index 0 upward.

```
While image not complete:
	cmd := next command bits
	If cmd == bit 0:
		Output next data byte as a literal, also save into literal history
	If cmd == bits 10:
		xx := next data byte
		yy := next data byte
		If xx >= 0xF0:
			Copy ((xx - 1) & 0x0F + 1) bytes from the
			literal history array, absolute index yy and upward;
			the read loops around if reaches index 256.
		Else:
			Output yy byte xx times
	If cmd == bits 110:
		xx := next data byte
		yy := next data byte
		zz := next data byte
		Output alternately yy or zz, for xx bytes
	If cmd == bits 1110:
		Output single 00 byte, also save into literal history
	If cmd == bits 1111:
		xx := next data byte
		Output default pattern byte xx times
```

Multibyte output commands are not expected to cross column boundaries,
but boundary checking is always the safe choice.


##### Building the final image

The decompressed data is arranged in 1bpp 8px columns, left to right. Data for all
bitplanes appears concatenated for each column, so the order is:
- Bitplane 0 (blue) for the first column
- Bitplane 1 (red) for the first column
- Bitplane 2 (green) for the first column
- Bitplane 0 (blue) for the second column ...

You can merge the 1bpp column planes to create a final 3bpp column for output.

However, some images may not have all 3 bitplanes. The bitplane mask byte in the
header has a bit set for each present bitplane. For example, mask 0x03 will have
the blue and red bitplanes for each column, but the unpacked data goes from
bitplane 1 straight to the next column's bitplane 0. The skipped bitplane is zeroed.

Example 16x2 decompressed data as bytes:
```
55 55 33 33 0F 0F 00 33 0F 00 0F 00
```

Same, organised and shown as binary:
```
   -- Column 1 - Column 2 --
Blue  01010101   00000000
Blue  01010101   00110011

Red   00110011   00001111
Red   00110011   00000000

Green 00001111   00001111
Green 00001111   00000000
```

After merging, the 3bpp 16x2 image has these binary pixels:
```
000 001 010 011 100 101 110 111   000 000 000 000 110 110 110 110
000 001 010 011 100 101 110 111   000 000 001 001 000 000 001 001
```

Or, in decimals:
```
0 1 2 3 4 5 6 7  0 0 0 0 6 6 6 6
0 1 2 3 4 5 6 7  0 0 1 1 0 0 1 1
```


##### Remaining issues

- dc8, dc15, dc60, dc63 (Dragon City) appear to be corrupted


MDA multiframe images
---------------------

These were used for animations in older Fairytale/Cocktail Soft etc titles.

- Multiple 3bpp bitmap frames in each file
- Only allows the default max-contrast 8-color GRB palette
- Pixels are double-height (640x200 resolution)
- No transparency
- Pixel data is saved in 8px 1bpp columns
- Each output byte comes in groups of three, one for each bitplane
- The algorithm uses only literals and repeats of hardcoded patterns

Header:
```
uint8 - number of uint16 words to follow
uint16[] - array of start offsets for each frame, plus possible array terminator
```

Normally the last uint16 in the array will be a terminator with the invalid value
0xF0zz, where zz is the offset of the first frame. The first frame begins immediately
after the header. So expect to see something like:

`04 0009 0089 0145 F00B <first frame header> ...`

However, if the first frame header's offset is reached while still reading the
uint16 array, consider the array implicitly terminated and start reading frames
normally.

Each frame starts with a frame header:
```
uint16 - destination offset in screen buffer, reduce by 0xC000
uint8 - frame width x8
uint8 - frame height
```

The destination top left corner's coordinates and buffer offset are convertable:

```
ofsx == ((offset - 0xC000) MOD 80) * 8
ofsy == (offset - 0xC000) / 80
offset == ofsy * 80 + ofsx / 8
```

So the header `C084 05 0F` means a 40x15 pixel image, top left corner at (32, 1).

The compressed frame bitmap immediately follows the frame header. For each command
bit encountered, you either output three bytes (an 8px row) of literals, or repeat
one of 125 hardcoded byte pairs as a dithering pattern. The first byte has the blue
pixels for the row, the second byte the red, the third byte the green.

The image is unpacked in 8px columns, one 8px row at a time. Columns are processed
alternately from the top, then from the bottom - a serpentine pattern. So, at the
bottom of the first column, start outputting the second column from the bottom;
when at the top of the second column, start the third from the top again.

The dithering patterns are always row-aligned. The low byte of a uint16 pattern
appears on even-numbered rows, the high byte on odd-numbered. Make sure to track
row oddness correctly when stepping to the next column; that's a sideways movement,
so the row doesn't change.

```
function GetPattern(input byte i):
	patterns := [0000, 8822, aa55, 77dd, ffff]
	g := patterns[i MOD 5]
	i := i DIV 5
	r := patterns[i MOD 5]
	b := patterns[i DIV 5]
	return [b, r, g]

While input data remaining:
	cmd := next input byte
	For each cmd bit from 0x80 downward:
		If cmd bit is set:
			Read and output three literal bytes (BRG order)
		Else:
			pattern := GetPattern(next input byte)
			count := next input byte
			direction := down
			If output destination Y coordinate is odd:
				swap top and bottom byte in each pattern word

			For i := 0 to count - 1:
				output bottom byte for each pattern color (BRG order)
				swap top and bottom byte in each pattern word
				If direction == down:
					output destination := next row down
				Else:
					output destination := next row up
				If image complete: exit
				If current column is fully output:
					output destination := next column, remain at same row
					flip direction
```


##### Remaining issues

- CT_A.MDA in Dragoon Armor is invalid; looks like it's extracted data from cluster 0 or
  thereabouts, maybe a disk copy error or bad file in the original game.


ADA images
----------

The ADA suffix is a general-purpose name for Fairytale's old non-animated graphics.

The actual algorithm depends on the game:
- Lipstick Adv 1, Dragoon Armor: PR6 format
- Koroshi no Dress 1, 2: this ADA format
- Lipstick Adv 2 or newer: PRS format

Notes on the ADA format:
- Similar to the MDA format, except this is output in 4px columns rather than 8px.
- The dither pattern generator has the same patterns as in MDA, except the middle value
  aa55 is flipped to 5a. `patterns := [00, 82, 5a, 7d, ff]`
- Uses a serpentine pattern, first down one 4px column, then up the next 4px column
- The original encoder prefers to produce a pattern repeat command if even a single row
  matches, even though that takes 1+6+7=14 bits; a literal would have taken only 13 bits
  for the same end result... and because RLE is only available for aligned dither
  patterns, it fails to take advantage of obvious opportunities like straight vertical
  lines, instead producing a lot of literals and single-row repeats.

Header:
```
uint16 - destination offset in screen buffer, reduce by 0xC000
uint8 - frame width x4
uint8 - frame height
```

The compressed image follows the header directly.

```
While image not complete:
	cmd := read next bit
	If cmd bit is set:
		b := read next 4 bits
		r := read next 4 bits
		g := read next 4 bits
		output 4px row
		move to next row
	Else cmd bit is not set:
		pattern := read next 7 bits
		count := read next 6 bits
		output GetPattern(pattern) for next count rows
```


Micro Cabin PRS images
----------------------

Used in Kimagure Orange Road at least.

Source code for reading these is in [/inc/gfx/prs.pas](../../inc/gfx/prs.pas).

- Scanline order
- 8-color BRG, default palette
- 8px at a time, bitplanes in interleaved bytes
- Pixels are double-height (640x200 resolution)
- Constant pattern dictionary with RLE

Header: (words are x86-native little-endian)
```
uint16 - unknown, but always a low value, 0029 or less, never 0... framecount?
uint16 - image pixel width, multiply by 8
uint16 - image pixel height
uint16 - image start offset in the output buffer, ofsy * width + ofsx?
byte[3 * 63] - pattern dictionary, 63 blue bytes, then 63 red, then 63 green
```

The compressed image stream immediately follows the pattern dictionary, from 0xC5.
The dictionary is fixed, so it acts as a shorthand for the 63 most common byte triplets
in this image.

To decompress:
```
While image not complete:
	cmd := Read next input byte
	repeat_count := cmd & 3
	If repeat_count == 0, then:
		repeat_count := Read next input byte
		If repeat_count == 0, then repeat_count := 256
	pattern := cmd >> 2

	If pattern == 63:
		Output the next (repeat_count * 3) bytes from input as literals
	Else:
		value := dictionary[pattern], dictionary[pattern + 63], dictionary[pattern + 126]
		Output the 3-byte value repeat_count times
```

After decompressing, the bitplanes need to be deinterlaced from each output byte triplet.

For each set of 3 bytes `76543210 hgfedcba HGFEDCBA`,
output 8 3bpp pixels: `7hH 6gG 5fF 4eE 3dD 2cC 1bB 0aA`.
