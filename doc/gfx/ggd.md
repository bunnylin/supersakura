GGD image format
----------------

Used in Ikura GDL engine games, such as used by newer ZyX games among others.
Grapholic calls these "VGD" images.
Already documented by [VileVN](http://vilevn.sourceforge.net/doxygen/cikura_8cpp_source.html).
Also at [GarBRO](https://github.com/morkt/GARbro/tree/master/ArcFormats/Ikura).

Source code for reading these is in [/inc/gfx/ggd.pas](../../inc/gfx/ggd.pas).

- GGD can be a 24-bit or 8-bit image
- GGA (GG0/GG1/GG2/GG3) is 32-bit RGBA with transparency
- GAN is a 24-bit RGB multiframe animation
- GAD is an 8/24-bit RGB multiframe image
- GGP is a xor-encrypted PNG file that could be of any bitness and can have transparency


##### 24-bit GGD image

Header:
```
uint32 - signature B9 AA B3 B3
uint16 - image width
uint16 - image height
```

Image width must be a multiple of 2. (Or maybe 4?) If not, increase it to the next
such multiple for image decompression. Afterward, the extra pixels can be cropped.

Compressed datastream follows directly. Pseudocode:

```
While more data remaining:
	cmd := next input byte

	// repeat last pixel
	if cmd == 0:
		i := next input byte
		Repeat the last three output bytes i times

	// copy from up to 255 pixels ago
	if cmd == 1:
		i := (next input byte) * 3
		distance := (next input byte) * 3
		Copy i bytes from distance bytes ago

	// copy from up to 65535 pixels ago
	if cmd == 2:
		i := (next input byte) * 3
		distance := (next little-endian input word) * 3
		Copy i bytes from distance bytes ago

	// copy single pixel from up to 255 pixels ago
	if cmd == 3:
		distance := (next input byte) * 3
		Copy 3 bytes from distance bytes ago

	// copy single pixel from up to 65535 pixels ago
	if cmd == 4:
		distance := (next little-endian input word) * 3
		Copy 3 bytes from distance bytes ago

	// output literals
	if cmd >= 5:
		i := (cmd - 4) * 3
		Output i input bytes
```


##### 8-bit GGD image

Header:
```
0: uint32 - signature CD CA C9 B8
4: uint32 - header size after signature, including this dword, always 0x28?
8: uint32 - image width
12: sint32 - image height, use absolute value if negative
```

Then, at 0x2C, the palette follows. An array of 256 x uint32, BGRA byte order.

After the palette there's an extra unknown dword.

The compressed datastream follows, using the 1992 Softdisk LZW algorithm.
Pseudocode:

```
While more data remaining:
	cmd := next input byte
	for each 8 bits in cmd, from lowest:
		if cmd bit is set:
			Output 1 byte from input
		else:
			// 12-bit distance, 4-bit length
			distance := (next input byte) + (next input byte & 0xF0 << 4)
			length := (that second input byte) & 0x0F + 3

			distance := (output ofs - distance - 19) & 0xFFF + 1
			Copy length bytes from distance bytes ago in output
			Except!! If copying from before start of output buffer,
			then any such underflow bytes are copied as zeroes.
```


##### GGA, GG0, GG1, GG2, GG3 image

32-bit image with an alpha channel. It's possible to have a GGD and GGA by the same name,
for example BA_WIN.GGD and GG2 in Horny Bunnies 1; the GGD uses RGB #000 for the
transparent color, while the GG2 has a full alpha channel. I guess there was some OS
display configuration or internal engine requirement (speed?) to use one or the other.

Although alpha values appear for every pixel, particularly GG0 images may appear with
a fully transparent (all 0) alpha channel. In this case, treat the image as fully opaque.

Header:
```
0: uint64 - signature "GGA00000" 47 47 41 30 30 30 30 30
8: uint16 - image width
10: uint16 - image height
12: uint16 - unknown, always 0000?
14: uint16 - unknown, GG0=1020, GG1=3020, GG2=1120, GG3=3120
16: uint32 - total header size
20: uint32 - image data size
24: uint32 - header second part start ofs
28: uint32 - header second part length
```

Unknown what the second header part is for. Image datastream starts directly after
total header size. The pixel colors are in BGRA order.

Pseudocode:

```
While more data remaining:
	cmd := next input byte

	// repeat last pixel up to 255 times
	if cmd == 0:
		i := next input byte
		Repeat the last four output bytes i times

	// repeat last pixel up to 65535 times
	if cmd == 1:
		i := next little-endian input word
		Repeat the last four output bytes i times

	// copy single pixel from up to 255 pixels ago
	if cmd == 2:
		distance := (next input byte) * 4
		Copy 4 bytes from distance bytes ago

	// copy single pixel from up to 65535 pixels ago
	if cmd == 3:
		distance := (next little-endian input word) * 4
		Copy 3 bytes from distance bytes ago

	// copy up to 255 pixels from up to 255 pixels ago
	if cmd == 4:
		distance := (next input byte) * 4
		i := (next input byte) * 4
		Copy i bytes from distance bytes ago

	// copy up to 65535 pixels from up to 255 pixels ago
	if cmd == 5:
		distance := (next input byte) * 4
		i := (next little-endian input word) * 4
		Copy i bytes from distance bytes ago

	// copy up to 255 pixels from up to 65535 pixels ago
	if cmd == 6:
		distance := (next little-endian input word) * 4
		i := (next input byte) * 4
		Copy i bytes from distance bytes ago

	// copy up to 65535 pixels from up to 65535 pixels ago
	if cmd == 7:
		distance := (next little-endian input word) * 4
		i := (next little-endian input word) * 4
		Copy i bytes from distance bytes ago

	// repeat last pixel once
	if cmd == 8:
		Repeat the last four output bytes once

	// copy pixel from above
	if cmd == 9:
		Copy 4 bytes from (image width * 4) bytes ago

	// copy pixel from above and behind
	if cmd == 10:
		Copy 4 bytes from (image width * 4 + 4) bytes ago

	// copy pixel from above and ahead
	if cmd == 11:
		Copy 4 bytes from (image width * 4 - 4) bytes ago

	// output literals
	if cmd >= 12:
		i := (cmd - 11) * 4
		Output i input bytes
```


##### GAD image

A single image containing multiple frames. The actual format can probably be any
of the above.

Header:
```
0: uint32 - signature "GAD " 47 41 44 20
4: uint32 - header size including signature
8: uint16 - animation sequence count
10 ... - animation sequences
```

Animation sequence:
```
uint16 - sequence index number
uint16 - sequence length
uint16 - draw animation at screen position X
uint16 - draw animation at screen position Y
uint16 - frame width
uint16 - frame height
... - array[0..seqlength - 1] of framedata
```

Frame data:
```
uint16 - frame source position X
uint16 - frame source position Y
uint32 - delay associated with this frame, centiseconds?
```

Expect a single GGD image to begin immediately after the header and last
animation sequence. This is recognisable by a valid GGD/GGA signature.
All individual animation frames are within this image. They are selected
using the specified frame width/height and frame source position.


##### GAN image

Container for multiple image frames, always 640x480 image size, where each frame
can be an efficient delta on top of an earlier frame. Like an animated GIF, kind of.
Except the actual compression is RLE, not great for truecolor fullscreen images.
Byte order BGR.

Header:
```
0: char[8] - signature "GANM0100"
8: uint32 - sequence length
12: uint32 - number of frames
16: uint32[] - animation sequence of the given length
```

The animation sequence specifies in what order the frames should be displayed.
Where the uint32 value is a valid frame index, display that frame, then wait
some specific constant time (200ms? Probably set by script). If the value is
not a valid frame index, it's probably a delay for so many centiseconds.

From 0x2010 onward is an array of image frame headers, where each entry is 16 bytes:
```
0: uint32 - frame index, 1-based; normally ascending, may end with repeat of 1st frame
4: uint32 - delta base index, draw this on top of the base image unless index is 0
8: uint32 - image data start offset (the first is always from 0x2810)
12: uint32 - image data byte length
```

For frames with a 0 delta index, the image data is a basic RLE truecolor image in
a single stream; ie. only a pixel section, there is no span section length.

If the delta index is not 0, the image data begins with a span lengths section,
then the pixel section. Span lengths determine how many pixels to output each time;
the pixel source alternates after every span, either unpacking from the current
image's pixel section, or copying from the base image,

```
uint32 - span section length, including this uint32
byte[] - span section
byte[] - pixel section, up to end of image data specified in the frame header
```

Length values are encoded as follows:
- 0..0x7F: single byte
- 0x80..0x7FFE: big-endian double-byte with the top bit set
- else: two throwaway bytes FF FF, then the true value as a little-endian uint32

Example: span length bytes `01 05 AF 44 FF FF 08 00 09 00`
- Output 1 pixel from the pixel section
- Copy 5 pixels from the base image
- Output 0x2F44 pixels from the pixel section
- Copy 0x00090008 pixels from the base image

Pixel values use 3 bytes each, in BGR order, and are RLE-encoded:
- Read next pixel bytes, output them immediately
- If this was not the first pixel, and was identical to the previous pixel,
  a variable length value appears instead of the next pixel bytes; the length
  is the total number of identical pixels that appear here, including the two
  pixels already output
- Repeat the last pixel until the given length is reached
- An RLE repeat might cover more than one length span; if an RLE repeat is not
  finished when the length span ends, keep the repeat counter and resume the
  repeats after copying however many pixels from the base image

Pseudocode for decompressing one image frame:
```
function ReadLength(source is span section or pixel section):
	len := next source byte
	If len >= 0x80:
		secondbyte := next source byte
		If (len == 0xFF) && (secondbyte == 0xFF):
			len := next source uint32
		Else:
			len := ((len & 0x7F) << 8) + secondbyte
	return len

Create a 24bpp 640x480 target buffer for this frame
If delta base != 0:
	Extracting over a delta base...
	span_section_length := next input uint32
	span_section := current input position
	pixel_section := input position + span_section_length - 4
	RLE_len := 0

	While image data bytes remain:
		span_len := ReadLength(span_section)
		While span_len > 0:
			span_len := span_len - 1
			If RLE_len > 0:
				Output prev_literal
				RLE_len := RLE_len - 1
			Else:
				literal := next 3 input bytes
				Output the literal
				If this was not the first pixel, and literal == prev_literal:
					RLE_len := ReadLength(pixel_section) - 2
				prev_literal := literal

		span_len := ReadLength(span_section)
		Copy span_len pixels from the base image
Else:
	Extracting a solid image, no delta...
	While image data bytes remain:
		literal := next 3 input bytes
		Output the literal
		If this was not the first pixel, and literal == prev_literal:
			RLE_len := ReadLength(current input position) - 2
			Output the literal RLE_len times more
		prev_literal := literal
```


##### GGP images

Standard PNG file with a brief extra header and simple octabyte XOR encryption.

Header:
```
char[8] - signature "GGPFAIKE"
uint32 - unknown, always 0?
byte[8] - encryption seed
uint32 - data start offset
uint32 - data byte size
byte[8] - unknown, always 0?
```

To decrypt:
```
key := header signature XOR encryption seed
key_index := 0
For each byte in encrypted data:
	Output (next input byte XOR key[key_index])
	key_index := (key_index + 1) AND 7
```

Then just feed the decrypted data into your standard PNG loader.
