AIC image format
----------------

Used in Slayers, developed by AIC Spirits, published by Banpresto.
- Single 4bpp bitmap in each file
- Transparency is normally palette index 2, a maximum green
- Huffman-encoded, single byte copy commands but no run-length copies
- Binary tree tables follow the header directly, then the compressed image as bitstream
- Image is stored scanline-wise, left to right and top to bottom
- Each pixel pair is saved as a byte with interlaced bits that must be separated

MLD can view these.

Source code for reading these is in [/inc/gfx/aic.pas](../../inc/gfx/aic.pas).
There's an [encoder](https://mooncore.eu/filus/makeaic.py) as well.

Header: (words are x86-native little-endian)
```
uint16 - header byte size, always 0x40
uint16 - X pixel offset, probably must be a multiple of 8
uint16 - Y pixel offset
uint16 - image pixel width, probably must be a multiple of 8
uint16 - image pixel height
char[6] - signature, "AIC110"
byte[48] - palette, 16 byte triplets, low nibbles only, order RGB
```

The header is followed by 3 sections at fixed offsets:
- 0x40 to 0x83 (68 bytes) - binary tree branch indicator bitstream
- 0x84 to 0x2A3 (512+32 bytes) - uint16 values, binary tree node outcomes
- 0x2A4 to eof - image data bitstream

The first two sections are typically only partially filled, then padded with zeroes.
The bitstreams are read one byte at a time; bits are read from 0x01 to 0x80.


##### Building the binary tree

The first two data sections are used to construct a binary tree, which can be used
to convert variable-width bit values to output pixel pairs. The tree is built node
by node, where each node has two outcomes - false/left/0 and true/right/1 - which can
be either a leaf (output some value) or a branch (move to a different node).

The first section indicates which nodes are leaves (0) and which are branches (1).
The second section has the values to be placed in each leaf node. Although the second
section is stored as uint16 values, the largest allowed value is only 256 + 28 = 0x11C.

The tree is built from node 0, and the false/left/0 outcome is always defined first.

Pseudocode for building the tree:
```
node type = structure of:
	leafOnLeft, leafOnRight, valueLeft, valueRight, parent
tree = array of nodes

current_node := 0

While any node outcome still needs to be defined:
	i := Read the next bit from the first section

	If i == 1:
		Add a branch to tree[current_node]:
			Extend the tree array by adding a new_node on top
			If the left outcome was not defined yet:
				leafOnLeft := FALSE
				valueLeft := new_node
			Else:
				leafOnRight := FALSE
				valueRight := new_node
		Set parent of tree[new_node] := current_node
		Move immediately to the new branch: current_node := new_node

	If i == 0:
		Add a leaf to tree[current_node]:
			value := Read the next uint16 from the second section
			If the left outcome was not defined yet:
				leafOnLeft := TRUE
				valueLeft := value
			Else:
				leafOnRight := TRUE
				valueRight := value
				Both the left and right outcomes are now defined for this node!
				Repeat:
					If current_node == 0:
						Break! The tree is complete
					current_node := parent of tree[current_node]
				Until tree[current_node] has an undefined outcome
```


###### Small example binary tree

First section: single byte `26`

First section as bits (flipped to read from 0x01 upward): `01100100`

Second section: `0005 0008 0045 0099 0102`

The binary tree starts with an initial node, with both outcomes undefined.
Read a bit from the first header section. This is `0`, so the left outcome will be
a leaf. Read a value from the second section and save as the left leaf value, `0005`.
The binary tree still has only one node. It looks like this:

```
leaf
0005      undefined
 \          /
  +--------+
  | node 0 |
  +--------+
```

Read the next bit from the first section. This is `1`, so the undefined outcome becomes
a branch to a new node. The next bit is again `1`, so the left outcome of the new node
becomes a branch to yet another new node.

```
undefined  undefined
 \          /
  +--------+
  | node 2 |
  +--------+    undefined
       \          /
        +--------+
leaf    | node 1 |
0005    +--------+
 \          /
  +--------+
  | node 0 |
  +--------+
```

The next two bits are `0` and `0`, so node 2 gets two leaves, values `0008` and `0045`.
There is nowhere to go from node 2 except backward to node 1, which still has an
undefined right outcome. The next bit `1` creates a new branch there, to a new node.
The remaining two bits are `0` and `0`, so the new node immediately gets two leaves,
with values `0099` and `0102`.

```
leaf    leaf  leaf     leaf
0008    0045  0099     0102
 \        /    \        /
 +--------+    +--------+
 | node 2 |    | node 3 |
 +--------+    +--------+
        \        /
        +--------+
leaf    | node 1 |
0005    +--------+
  \        /
  +--------+
  | node 0 |
  +--------+
```

You can tell the tree is complete since there are no more undefined nodes. Any leftover
data in sections 1 or 2 can be ignored.

To use this tree for decoding values, always start from node 0. Read input bits; on a `0`,
take the left outcome, on a `1` take the right, until you find a leaf.

For this example tree, the input code `101` would return the value `0045`, whereas the
input code `0` returns `0005`.


##### Decoding the image

With the binary tree built, the final data section from 0x2A4 can be read. Read it as
a bitstream, one byte at a time, again from bit 0x01 to bit 0x80. Feed the bits into the
binary tree to get back values.

If the value is in the range 0..255, output it as a literal byte.

If the value is >= 256, it's a command to copy a single byte from earlier in the output.
Deduct 256 from the value and divide it by 2, then use the below copy distance table.

When the output buffer has enough bytes for the whole image (width / 2 * height bytes),
you need to de-interlace the bytes. Every byte has two 4-bit pixels, but the bits are
in the wrong order. Go over every byte and shuffle its bits into the correct order.

Raw bit order: `76543210`

Corrected bit order: `1357 0246`

Here's a possible way to do the bit shuffle for a single byte `x`:
```
x = (x & 0x24) | ((x & 0x90) >> 3) | ((x & 9) << 3) | ((x & 0x40) >> 6) | ((x & 2) << 6)
```


##### Copy distance table

A hardcoded table for copying one byte from earlier in the image.
Horizontal difference must not read from a different scanline, only the explicit
"x rows ago" are allowed to read from another scanline. Eg. copying from 1 byte ago
when in the first pixel of a scanline is not allowed.

	0: invalid
	1: copy from one byte ago
	2: copy from 2 bytes ago
	3: copy from 4 bytes ago
	4: copy from 8 bytes ago
	5: copy from 1 row ago, 1 byte ahead
	6: copy from 1 row ago
	7: copy from 1 row ago, 1 byte ago
	8: copy from 1 row ago, 4 bytes ago
	9: copy from 2 rows ago, 1 byte ahead
	A: copy from 2 rows ago
	B: copy from 2 rows ago, 1 byte ago
	C: copy from 4 rows ago
	D: copy from 4 rows ago, 2 bytes ago
	E: copy from 8 rows ago
	F: copy from 8 rows ago, 4 bytes ago
