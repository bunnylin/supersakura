AS2 image format
----------------

Used in Himeya's Bacta 2, and various Forest/Foster games.

- Single 4bpp bitmap in each file
- No transparency, but bitmaps can be overlaid to replace a rectangular area in a base image
- Compressed into a bitstream; uses simple copy commands and literals, Huffman-encoded
- Huffman tables follow the header directly, then the compressed image until end of file
- Bitmap is saved one 2px column at a time, rather than scanline order
- Each 2px value is saved as a pair of nibbles with interlaced bits that must be separated

Source code for reading these is in [/inc/gfx/as2.pas](../../inc/gfx/as2.pas).

##### Header

	dword sig [AS2 ]
	dword filesize from start of sig to past end of data
	byte x offset x8
	byte width x8
	word y offset
	word height
	byte flag1 - 0x80: palette present, 0x40: unknown, ignore?
	byte flag2 - 0x00 or 0x01 only? Determines which variable length variant to use
	16 x 3 nibbles = 24 bytes palette, BRG order

From this point on, the data is a bitstream with no further byte boundary alignment.
Data is read one little-endian 16-bit word at a time, then bits in that word are
consumed from the top.

eg. `51 0F 37 88` will produce the bits:

`0000 1111 0101 0001 : 1000 1000 0011 0111`

##### Copy distance table

This is a hardcoded table for copying bytes from earlier in the image.

	00: 0 (no copy, output a literal)
	01: copy from previous column
	02: copy from 2 columns ago
	03: copy from 3 columns ago
	04: copy from 4 columns ago
	05: copy from 8 columns ago
	06: copy from 1 row ago
	07: copy from previous column, 1 row ago
	08: copy from previous column, 1 row ahead
	09: copy from 2 rows ago
	0A: copy from previous column, 2 rows ago
	0B: copy from 2 column ago, 2 rows ago
	0C: copy from 4 columns ago, 2 rows ago
	0D: copy from previous column, 2 rows ahead
	0E: copy from 2 columns ago, 2 rows ahead
	0F: copy from 4 columns ago, 2 rows ahead
	10: copy from 4 rows ago
	11: copy from 2 columns ago, 4 rows ago
	12: copy from 4 columns ago, 4 rows ago
	13: copy from 2 columns ago, 4 rows ahead
	14: copy from 4 columns ago, 4 rows ahead
	15: copy from 8 rows ago
	16: copy from 2 columns ago, 8 rows ago
	17: copy from 4 columns ago, 8 rows ago
	18: copy from 2 columns ago, 8 rows ahead
	19: copy from 4 columns ago, 8 rows ahead
	1A: copy from 16 rows ago
	1B: copy from 2 columns ago, 1 row ago
	1C: copy from 2 columns ago, 1 row ahead
	1D: copy from 3 rows ago
	1E: copy from previous column, 3 rows ago
	1F: copy from previous column, 3 rows ahead

##### Huffman tables

These will mostly be saved as nibbles and quintets (4-bit and 5-bit values).

First there is a link table, which has between 1 and 32 entries, each being an index
into the hardcoded copy distance table. Most images use all copy commands, but some
don't need them all. (Leaving out unused distance values allows more efficient encoding,
and perhaps the most frequently used distances end up with shorter codes.)

	Read a quintet, then read that many quintets plus 1 into your link table.

Now comes a series of quintets used to build the repetition code binary tree.
The tree has up to 32 nodes, where each node has two outcomes depending on the next read
bit, and each outcome can either lead to a further node, or return a final value.
The tree is built and traversed from node 0 upward. While building, keep track of which
branches for each node have not been defined yet.

	Read a quintet; this is how many final values the tree will have.
	While all final values have not been placed:
		Repeat:
			Add a branch from current node to a new node
			(FALSE branch first if not used yet, else TRUE branch)
			Read the next quintet
		Until the read quintet is 0
		Repeat:
			Read the next quintet; this is a link table index
			Add the read quintet as a final value in this node
			(FALSE outcome first if not used yet, else TRUE outcome)
			If both outcomes have been defined for this node:
				If all final values have been placed, exit
				Move back through parent nodes until an undefined branch is found
			Read the next quintet
		Until the read quintet is not 0

Next is a series of nibbles used to build the literals binary tree. The logic is the same
as above, except:
- Read nibbles instead of quintets
- The outcome nibble (which is a link table index in the previous tree) is saved after
converting to a literal byte: `n := (n & 3) | ((n & 12) << 2);
result := (n & 17) | ((n & 34) << 1)`
- The tree may have 0 final values, in which case skip the tree building, and instead
whenever the tree is needed, just return a fixed value. Pseudocode:

```
Read a nibble; this is how many final values the tree has
If it's 0:
	Read a nibble
	n := (n & 1) + ((n & 2) << 1) + ((n & 4) << 2) + ((n & 8) << 3)
	Fixed result := n * 3
Else: build the tree normally
```

##### Uncompressing the image

For this, you need a memory buffer storing at least the 8 most recent columns, or all columns
to keep it simple. Each column is two nibbles wide and the image's pixel height tall, in other
words: one byte per pixel row.

When copying bytes, the copy size will be given as a variable length. It uses the value of
flag2 from the header, and works like this:

	i := flag2
	While i < 8:
		Read the next bit; if it is FALSE, increase i; if it is TRUE, break
	j := Read the next i bits, which will make a value of 0 to 8 bits
	variable length result := (1 << i) + j - flag2

Column uncompressing pseudocode:

	While not all columns completely filled in:
		r := Read a final value using the repetition code tree
		If r is 0:
			Read two final values using the literals tree
			Output (first value << 1) + (second value) as a literal byte
		Else:
			Read a variable length code
			Output that variable length of bytes, copied from r bytes ago

##### Building the final image

Assuming you used linear byte arrays for the columns, the image in memory now is
diagonally mirrored. Go through the columns and swap every byte's column and row
coordinates in whatever way is efficient.

While at it, you need to separate the interlaced nibbles in each byte. Take every
other bit to get one nibble, then the remaining bits to get the other.

	byte with bits 0123 4567 -> separate! -> byte with bits 0246 1357

The final 4bpp image is complete.
